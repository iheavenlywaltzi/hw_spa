'use strict';
var CACHE_NAME = 'cache-update-and-refresh-v1';
var urlCash = [
	'/offline.html'
];

//Установлен
self.addEventListener('install', (event)  => {
    console.log('Установлен');
	event.waitUntil( caches.open(CACHE_NAME).then((cache) => cache.addAll(urlCash) ));
});

//Активирован
self.addEventListener('activate', (event)  => {

});

//Происходит запрос на сервер
self.addEventListener('fetch', (event) => {
  const request = event.request;
  event.respondWith( 
    // Сначала попытка запросить его из Сети 
    fetch(request)
    .then( responseFromFetch => {
      return responseFromFetch;
    }) // конец fetch.then
    // Если не сработало, то...
    .catch( fetchError => {
      // пытаемся найти в кеше
      caches.match(request)
      .then( responseFromCache => {
        if (responseFromCache) {
         return responseFromCache;
       // если не сработало и...
       } else {
         // это запрос к веб-странице, то...
         if (request.headers.get('Accept').includes('text/html')) {
           // покажите вашу офлайн-страницу 
           return caches.match('/offline.html');
         } // 1конец if
       } // конец if/else
     }) // конец match.then
   }) // конец fetch.catch
  ); // конец respondWith
});
