"use strict";
var buttonsController = new function(){
	let G = this;
	
	G.start = function(){
		G.createNumber();
		G.createText();
		G.createSes();
		G.createSme();
		G.createTimetable();
		G.createLimit();
		//G.createHelper();
		G.createScroll();
		G.createSwitch();
		G.createSwitch2();
		
		G.checkbut();
		G.checkbox();
		
		//функции нажатия на элементы
	}
	
	document.addEventListener('click',function(e){
		let target = e.target;
		//if(target.localName != 'helper'){let hw; if(hw = document.querySelector('.helper-window')){hw.style.visibility = 'hidden';}}
		if(!target.classList.contains('line') && !target.classList.contains('btn-line')){
			Array.apply(null,document.querySelectorAll('scroll.active')).forEach(function(elem){elem.classList.remove('active');});
		}
	});
	
	G.createSwitch2 = function(){
		let allElem = G.getAllElem('switch2');
		let GS = {};
		let s = '';
			s+= '<div class="btn"></div>';
			s+= '<div class="btn"></div>';
			s+= '<div class="btn"></div>';
			s+= '<div class="switcher"></div>';
			s+= '<div class="click"></div>';
			
		for (let n in allElem){let elem = allElem[n];
			elem.innerHTML = s;
			elem.addEventListener('click',click);
			elem.classList.add('str');
			
			let icon = (elem.getAttribute('icon'))?elem.getAttribute('icon').split(','):false;
			if(icon) {elem.querySelector('.switcher').innerHTML = icon[0];}
		}
		
		function click(e){
			let elem = this;
			let x = e.offsetX;
			let col = elem.querySelector('.coloring');
			let swi = elem.querySelector('.switcher');
			let kof = elem.offsetWidth/3;
			let icon = (elem.getAttribute('icon'))?elem.getAttribute('icon').split(','):["","",""];
			
			if (x < kof){
				swi.innerHTML = icon[0] || "";
				elem.classList.add('str');
				elem.classList.remove('mid');
				elem.classList.remove('end');
			}
			if (x > kof){
				swi.innerHTML = icon[1] || "";
				elem.classList.remove('str');
				elem.classList.add('mid');
				elem.classList.remove('end');
			}
			if (x > (kof*2)){
				swi.innerHTML = icon[2] || "";
				elem.classList.remove('str');
				elem.classList.remove('mid');
				elem.classList.add('end');
			}
		}
	}
	
	G.createSwitch = function(){
		let allElem = G.getAllElem('switch');
		let GS = {};
		let s = '';
			s+= '<div class="btn"></div>';
			s+= '<div class="coloring"><div class="switcher"></div></div>';
			s+= '<div class="click"></div>';
			
		for (let n in allElem){let elem = allElem[n];
			elem.innerHTML = s;
			elem.addEventListener('click',click);
			elem.classList.add('str');
			
			let icon = (elem.getAttribute('icon'))?elem.getAttribute('icon').split(','):false;
			if(icon) {elem.querySelector('.switcher').innerHTML = icon[0];}
		}
		
		function click(e){
			let elem = this;
			let x = e.offsetX;
			let col = elem.querySelector('.coloring');
			let swi = elem.querySelector('.switcher');
			let kof = elem.offsetWidth/3;
			let icon = (elem.getAttribute('icon'))?elem.getAttribute('icon').split(','):["","",""];
			
			if (x < kof){
				swi.innerHTML = icon[0] || "";
				elem.classList.add('str');
				elem.classList.remove('mid');
				elem.classList.remove('end');
			}
			if (x > kof){
				swi.innerHTML = icon[1] || "";
				elem.classList.remove('str');
				elem.classList.add('mid');
				elem.classList.remove('end');
			}
			if (x > (kof*2)){
				swi.innerHTML = icon[2] || "";
				elem.classList.remove('str');
				elem.classList.remove('mid');
				elem.classList.add('end');
			}
		}
	}
	
	G.createScroll = function(){
		let allElem = G.getAllElem('scroll');
		let GS = {};
		for (let n in allElem){
			let elem = allElem[n];
			let da = {}
				da.min = parseInt(elem.getAttribute('min')) || 0;
				da.max = parseInt(elem.getAttribute('max')) || 100;
				da.start = parseFloat(elem.innerHTML) || da.max/2;
			elem.innerHTML = create(da);
			elem.querySelector('.line').addEventListener('click',click);
			elem.querySelector('.btn-line').addEventListener('pointerdown',down);
			elem.querySelector('.btn-line').addEventListener('pointerup',up);
			
			elem.querySelector('.btn-line').addEventListener('touchend',up);
			elem.querySelector('.btn-line').addEventListener('touchmove',move);
			
			document.addEventListener('mousemove',move);
			document.addEventListener('mouseup',up);
		}
		
		function create(data){
			let s = '', shtrih = '',line = '',iter = 0,posS=0; 
			data = data || {
				min:0,
				max:100,
				start:50
			}
			GS.step = (data.max - data.min)/10;
			GS.min = data.min;
			
			shtrih = '<div class="shtrih-temp">';
			while (iter <= 10){
				let numb = parseFloat((data.min + (iter*GS.step)).toFixed(2));
				shtrih += '<p>|<span>'+numb+'</span></p>';
				iter++;
			}
			shtrih += '</div>'
			
			posS = (data.start-data.min)/(GS.step/10);
			line = '<div class="scale-temp">';
			line += shtrih;
			line += '<span class="line"></span>';
			line += '<div class="target" style="left:'+posS+'%">';
			line += '<div class="arrow-temp"></div>';
			line += '<span class="btn-line">'+data.start+'</span>';
			line += '<span class="btn-line-info">'+data.start+'</span>';
			line += '</div>';
			s += line;
			
			return s;
		}
		
		function click(){
			let elem = this;
			let main = elem.parentElement.parentElement;
			let active = document.querySelector('scroll.active');
			if(main.classList.contains('active')){
				main.classList.remove('active')
				if (active){active.classList.remove('active');}
			}
			else {main.classList.add('active'); if (active){active.classList.remove('active');}}
		}
		
		function down(e){
			GS.start = true;
			GS.elem = this;
			GS.main = GS.elem.parentElement;
			GS.container = GS.main.parentElement;
			GS.elemInfo = GS.main.querySelector('.btn-line-info');
			GS.x = event.pageX;
			GS.sx = parseFloat(parseFloat(GS.main.style.left).toFixed(4));
			GS.kof = parseFloat(parseFloat((100/GS.container.offsetWidth).toFixed(4)));
		}
		function move(e){ if (GS.start){
			let x,y;
			x = event.pageX || event.targetTouches[0].pageX || 0; 
			x = ((x-GS.x)*GS.kof)+GS.sx;
			x = parseFloat(x.toFixed(4));
			if(x >= 100){x=100}; if(x <= 0){x=0}
			GS.main.style.left = x+'%';
			let numb = GS.min + ((x/10)*GS.step); numb = numb.toFixed(1);
			GS.val = numb;
			GS.elem.innerHTML = numb;
			GS.elemInfo.innerHTML = numb;
		}}
		function up(){if (GS.start){
			GS.start = false;
			let fcn = HWS.upElem(GS.elem,'scroll').dataset.fcn || false;
			if(fcn){HWS.buffer[fcn](GS)}
		}}
	}
	
	G.createHelper = function(){
		let allElem = G.getAllElem('helper');
		let helperTimerId = 0;
		for (let n in allElem){
			let elem = allElem[n];
			elem.title = elem.innerHTML;
			elem.innerHTML = "?";
			elem.addEventListener('click',click);
		}
		
		function click(e){
			let elem = this;
			let sms =  elem.title;
			let wind = document.querySelector('.helper-window');
			if (!wind){
				wind = document.createElement('div');
				wind.classList.add('helper-window');
				document.body.appendChild(wind);
				wind.addEventListener('click',function(){this.style.visibility = 'hidden';});
			}
			let xr = e.clientX, ww = document.documentElement.clientWidth;
			let yr = e.clientY, wh = document.documentElement.clientHeight;
			let wr = wind.offsetWidth, hr = wind.offsetHeight;
			
			if(xr+wr > ww){xr = (ww-wr)-10}
			if(yr+hr > wh){yr = wh-hr}
			
			wind.style.visibility = 'visible';
			wind.style.top = yr+'px';
			wind.style.left = xr+'px';
			wind.innerHTML = sms;
			
			clearTimeout(helperTimerId);
			helperTimerId = setTimeout(function(){
				wind.style.visibility = 'hidden';
				wind.style.top = '-300px';
				wind.style.left = '-300px';
			},4000);
		}
	};
	
	G.createTimetable = function(){
		let allElem = G.getAllElem('timetable');
		let Gdata = {size:25,line:['ПН','ВТ','СР','ЧТ','ПТ','СБ','ВС']}
		let allValue = [
			{name:'Значение - 1',color:'#222',value:'1'},
			{name:'Значение - 2',color:'#f22222',value:'2'},
			{name:'Значение - 3',color:'#ddd',value:'3'}
		]

		for (let n in allElem){
			let elem = allElem[n];
			allValue = create_data(elem);
			let section = document.createElement('section');
			let footer = document.createElement('footer');
			let data = elem.dataset.data || false;
			
			elem.innerHTML = '';
			section.innerHTML = create_table(elem,Gdata.line,24,{data:data});
			footer.innerHTML = create_bot_content();
			
			elem.appendChild(section);
			elem.appendChild(footer);
			
			section.querySelector('.window-table .table').addEventListener('mousedown',p_down);
			section.querySelector('.window-table .table').addEventListener('mousemove',p_move);
			section.querySelector('.window-table .table').addEventListener('mouseup',p_up);
			document.addEventListener('pointerup',p_up);
			document.addEventListener('touchend',p_up);
		}
		
		var specD = {}; specD.up = false;

		function p_down(e){
			let elem = this;
			specD.x = e.offsetX;
			specD.y = e.offsetY;
			specD.up = true;
			specD.size = parseInt(G.searchParent(elem,'section').parentNode.getAttribute('size')) || Gdata.size;
			specD.main = elem;
			specD.color = elem.dataset.color;
			specD.value = elem.dataset.value;
			specD.allElem = creater();
			
			specD.div = document.createElement('div');
			specD.div.classList.add('selector');
			specD.div.style.position = 'absolute';
			specD.div.style.left = specD.x+'px';
			specD.div.style.top = specD.y+'px';
			specD.div.style.background = elem.dataset.color;
			specD.div.style.opacity = '0.8';
			
			specD.x = parseInt(specD.x/specD.size)*specD.size;
			specD.y = parseInt(specD.y/specD.size)*specD.size;
			
			specD.ls = parseInt(specD.y/specD.size);
			specD.ns = parseInt(specD.x/specD.size);
			
			//elem.appendChild(specD.div);
			elem.insertBefore(specD.div,elem.querySelector('.controler'));

			function creater(){
				let allObj = {};
				Array.apply(null,elem.querySelectorAll('span')).forEach(function(elem){
					allObj[elem.classList[0]] = elem;
				});
				return allObj;
			}
		}
		function p_move(e){if(specD.up){
			let x = e.offsetX;
			let y = e.offsetY;
			let xm = x-specD.x;
			let ym = y-specD.y;
			let xe=0,ye=0;
			let topE=0,leftE=0;

			if(xm > 0 && ym > 0) {
				xe = (parseInt(xm/specD.size)*specD.size)+specD.size;
				ye = (parseInt(ym/specD.size)*specD.size)+specD.size;
				
				specD.div.style.left = specD.x+'px';
				specD.div.style.top = specD.y+'px';
				specD.div.style.width = xe+'px';
				specD.div.style.height = ye+'px';
			}
			if(xm < 0 && ym > 0){
				xe = (parseInt(xm/specD.size)*specD.size)-specD.size;
				ye = (parseInt(ym/specD.size)*specD.size)+specD.size;

				specD.div.style.left = (specD.x+xe)+'px';
				specD.div.style.top = specD.y+'px';
				specD.div.style.width = Math.abs(xe)+specD.size+'px';
				specD.div.style.height = ye+'px';
			}
			if(xm > 0 && ym < 0){
				xe = (parseInt(xm/specD.size)*specD.size)+specD.size;
				ye = (parseInt(ym/specD.size)*specD.size)-specD.size;
				
				specD.div.style.left = specD.x+'px';
				specD.div.style.top = (specD.y+ye)+'px';
				specD.div.style.width = xe+'px';
				specD.div.style.height = Math.abs(ye)+specD.size+'px';
			}
			if(xm < 0 && ym < 0){
				xe = (parseInt(xm/specD.size)*specD.size)-specD.size;
				ye = (parseInt(ym/specD.size)*specD.size)-specD.size;
				
				specD.div.style.left = (specD.x+xe)+'px';
				specD.div.style.width = Math.abs(xe)+specD.size+'px';
				specD.div.style.top = (specD.y+ye)+'px';
				specD.div.style.height = Math.abs(ye)+specD.size+'px';
			}
		}}
		function p_up(){if (specD.up){
			specD.up = false;
			let lastL=0,lastN=0;
			let linS = parseInt(specD.div.offsetTop/specD.size);
			let linE = parseInt((specD.div.offsetTop+specD.div.offsetHeight)/specD.size);
			let numS = parseInt(specD.div.offsetLeft/specD.size);
			let numE = parseInt((specD.div.offsetWidth+specD.div.offsetLeft)/specD.size);
			
			if (linS == linE) {linE = linE+1;}
			if (numS == numE) {numE = numE+1;}
			
			for (let l=linS;l<linE;l++){
				for (let n=numS;n<numE;n++){
					let clas = 'tile_'+l+'_'+n;
					specD.allElem[clas].style.background = specD.color;
					specD.allElem[clas].dataset.val = specD.value;
					lastN = n;
				} lastL = l;
			}
			Array.apply(null,specD.main.querySelectorAll('.selector')).forEach(function(elem){ elem.remove(); });
			
			let mBlock = HWS.upElem(specD.main,'timetable');
			let fcn = mBlock.dataset.fcn || '';
			specD.timetable = mBlock;
			if(fcn){HWS.buffer[fcn](specD)}
		}}

		function change_val(){
			let elem = this;
			let main = G.searchParent(elem,'header').parentNode;
			let header = main.querySelector('header');
			let table = main.querySelector('section .window-table .table');
			let D = {};
				D.name = elem.dataset.name;
				D.color = elem.dataset.color;
				D.value = elem.dataset.value;
				
			table.dataset.name = D.name;
			table.dataset.color = D.color;
			table.dataset.value = D.value;
			
			header.querySelector('div').style.background = D.color;
			header.querySelector('h4').innerHTML = D.name;
			
			main.classList.remove('active');
		}
		
		function create_table(el,line,num,data){
			data = data || {};
			line = line || ['ПН','ВТ','СР','ЧТ','ПТ','СБ','ВС'];
			let print = '';
			let d = {}
				d.size = el.getAttribute('size') || Gdata.size;
				d.type = el.getAttribute('type') || false // month;
				d.line = el.getAttribute('line') || false // month;
				d.step = el.getAttribute('step') || false // month;
				d.data = data.data || false;

			if (d.type == 'month') {
				line = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
			}
			
			if (d.type == 'custom') {
				if (d.line) {line = d.line.split(',');}
				if (d.step){
					if (d.step.indexOf(',') + 1){d.step = d.step.split(',');  num=d.step.length;}
					else {d.step = parseInt(d.step); num=d.step;}
				}
			}
			
			
			
			if (d.data) {
				let sD = {}
				d.data.split(',').forEach(function(die){ // data info element ( die ;)
					die = die.split('|');
					let color;
					allValue.forEach(function(Velem){ if (Velem.value == die[2]){color =  Velem.color || '#000';} });
					sD[die[0]+'_'+die[1]] = {val:die[2],color:color};
				});
				d.data = sD;
			}

			// main block
			let minW = (num*d.size);
			let valFirst = allValue[0];
			print += '<div class="window-table" style="min-width:'+minW+'px;">';
			
			// TOP
			print += '<div class="top-table">';
			if (d.step){
				if (typeof d.step == 'object'){ 
					d.step.forEach(function(elem){print += '<p style="width:'+d.size+'px;">'+elem+'</p>';});
				}
				if (typeof d.step == 'number'){ 
					for (let i=0;i<num;i++){print += '<p style="width:'+d.size+'px;">'+i+'</p>';}
				}
			}
			else {
				for (let i=0;i<(num/2);i++){
					let t = (i*2)+1;
					if(t < 10) {t = '0'+t;}
					print += '<p style="width:'+(d.size*2)+'px;">'+t+':00</p>';
				}
			}
			print += '</div>';
			
			// LEFT 
			print += '<div class="left-table">';
			for (let l in line){
				print += '<p style="height:'+d.size+'px; width:'+d.size+'px" >'+line[l]+'</p>';
			}
			print += '</div>';
			
			// Table 
			let numline = line.length;//line
			print += '<div class="tile-table" ><div class="table" data-color="'+valFirst.color+'" data-name="'+valFirst.name+'" data-value="'+valFirst.value+'">';
			print += '<div class="controler"></div>';
			for (let id=0;id<numline;id++){
				print += '<div>';
				for (let ih=0;ih<num;ih++){
					let back = '#eee';
					let val = 0;
					if(d.data){ 
						let st1 = id+'_'+ih; 
						if(d.data[st1]){back = d.data[st1].color; val = d.data[st1].val}
					}
					print += '<span '+
						'style="width:'+d.size+'px; height:'+d.size+'px; background:'+back+'"'+
						'class="tile_'+id+'_'+ih+' one-tile-table"'+ 
						'data-line="'+id+'"'+
						'data-step="'+ih+'"'+
						'data-val="'+val+'"'+
						'></span>';
				}
				print += '</div>';
			}
			print += '</div>';

			print += '</div></div>';
			
			return print;
		}
		
		function create_data(elem){
			let allE = Array.apply(null,elem.querySelectorAll('val'));
			let ret = [];
			allE.forEach(function(el){
				ret.push({name:el.innerHTML,color:el.getAttribute('color'),value:el.getAttribute('value')});
			});
			return ret;
		}
		
		function create_bot_content(){
			let print = '';
			let i = 0;
			let active = 'active';
			
			print += ''
			for (let e in allValue){
				i++; if (i != 1){active = '';}
				print += '<div class="check-value '+active+'" '+
					'data-color="'+allValue[e].color+'"'+
					'data-name="'+allValue[e].name+'"'+
					'data-value="'+allValue[e].value+'">'+
					'<span class="box-color" style="background:'+allValue[e].color+'"></span>'+
					'<p class="name">'+allValue[e].name+'</p>'+
					'<span class="click btn_timetable_check_value"></span>'+
				'</div>';
			}
			print += ''

			return print; 
		}
		
		HWS.on('click','btn_timetable_check_value',function(e,el){
			let block = HWS.upElem(el,'.check-value');
			let timetable = HWS.upElem(block,'timetable');
			let table = timetable.querySelector('section .tile-table .table');
			let footer = timetable.querySelector('footer');
			
			if(!block.classList.contains('active')){
				footer.querySelector('.check-value.active').classList.remove('active');
				block.classList.add('active');
				
				table.dataset.color = block.dataset.color;
				table.dataset.name = block.dataset.name;
				table.dataset.value = block.dataset.value;
			}
		})
	}
	
	G.updateTimetable = function (elem,data){
		elem = HWS.getElem(elem);
		let valColor = {},spData = {};
		// Цвета
		HWF.recount(elem.querySelectorAll('footer .check-value'),function(onel){
			valColor[onel.dataset.value] = onel.dataset.color;
		},'on')
		// создаём  спец дату
		HWF.recount(data.split(','),function(onel){
			let die = onel.split('|');
			if(!spData[die[0]]){spData[die[0]] = {}}
			spData[die[0]][die[1]] = valColor[die[2]];
		});
		// проход по всем объектам
		HWF.recount(elem.querySelectorAll('.tile-table .table .one-tile-table'),function(onel){
			onel.style.background = spData[onel.dataset.line][onel.dataset.step];
		},'on')
	}
	
	G.createrTimetableInArray = function(arr,stp){ stp = stp || 48;
		let Cstep = -1, step = 0, echo = '';
		stp = stp-1;
		arr.forEach(function(elm,key){
			if(Cstep < stp){Cstep++;} else{Cstep = 0; step++;}
			{echo += step+'|'+Cstep+'|'+elm+',';}
		});
		return echo;
	}
	
	G.createSme = function(){
		let allElem = G.getAllElem('sme');
		let printTop = '<li class="one-head">\
			<div>Тип</div>\
			<div>Имя</div>\
			<div>Данные</div>\
			<div>Порт</div>\
			<div><i></i></div>\
		</li>';
		
		function print_list(list){
			print = '';
			console.log(list);
			for (let e in list){
				let elemEqup = list[e] || false;
				if(elemEqup){
					let sys3xData = HWequip.normInfo(elemEqup);
					let et = sys3xData.et;
					let value,stateColor='',port='';
					value = sys3xData.stateName || sys3xData.val;
					stateColor = 'color:'+sys3xData.stateColor;
					port = sys3xData.portSpec;
					if(sys3xData.state == 'no_connect'){value = 'Нет связи';}
					
					print += '<li data-id="'+sys3xData.id+'" class="elem-equp sme_elem_'+sys3xData.id+'">'+
						'<div><span class="icon icon_equip_'+sys3xData.type+'"></span></div>'+
						'<div class="name">'+sys3xData.name+'</div>'+
						'<div class="state" style="'+stateColor+'"><inf>'+value+'</inf></div>'+
						'<div class="port">'+port+'</div>'+
						'<div>'+
							'<a href="#'+HWD.page.equp+'/'+elemEqup.id+'" class="icon p16 icon-settings hover block_mr_8"></a>'+
							'<span class="icon p20 icon-delete hover btn_delete_elem"></span>'+
						'</div>'+
					'</li>';
				}
			}
			return print;
		}
		
		for (let n in allElem){
			let elem  = allElem[n];
				elem.id = 'sme_'+n;
			let span = document.createElement('span');
			let bottom = document.createElement('div');
				bottom.classList.add('block_w_100');
				bottom.classList.add('block_pt_20');
			let ul = document.createElement('ul');
				ul.classList.add('sme-list-equp');
			let id = elem.dataset.id || false;
			let buf = elem.dataset.buf || false;
			let print = '';

			if(buf && buf != 'null' && buf != 'undefined' && HWS.buffer.equps[buf]){
				print = print_list(HWS.buffer.equps[buf]);
			}
			if(id && id != 'null' && id != 'undefined'){
				id = id.split(',');
				let idSystem = HWS.buffer.elem.id_system;
				print = '<li><p></p>'+HWhtml.loader()+'<p></p></li>';
				
				let dataP = {token:HWS.pageAccess,list_start:0,list_amount:1000,local_id:id,id_system:idSystem}
				HWF.post({url:HWD.api.gets.list,data:dataP,fcn:function(inf){
					let list = inf.list;
					print = print_list(list);
				}});
			}
			
			bottom.innerHTML = '<span class="btn_text grey sme_btn_add_elem block_p_8">Добавить</span>';
			ul.innerHTML = printTop+print;
			span.innerHTML = '+';
			elem.appendChild(ul);
			elem.appendChild(bottom);
		}
		// Кнопка выбора оборудования
		HWS.on('click','sme_btn_add_elem',function(e,el){
			let list = HWS.upElem(el,'sme').querySelector('.sme-list-equp');
			let Bl = HWS.upElem(el,'sme');
			let filter = Bl.dataset.filter || 'of' ;
			let idSystem = HWS.buffer.elem.id_system;
			let print = '<div id="group-list-modal" class="modal_list block_w_100 block_h_80"></div>';
				print += '<div class="modal_btn_add_equp style_btn orange block_w_200p">Добавить</div>';
			
			HWS.data.apiGet.listSensorDO.data = {"id_program":HWS.buffer.elem.id}
			HWF.modal.print('Выберите оборудование',print);
			HWF.print.listSensorDO('#group-list-modal',{view:'list'});
			
			HWS.on('click','btn-one-equip',function(e,el){
				let liElem = HWS.upElem(el,'.one-equipment');
				let idLi = liElem.dataset.id;
				let check = el.parentElement.querySelector('.equ_b .checkbox');
				if(check.classList.contains('active')){
					check.classList.remove('active');
				}
				else {
					check.classList.add('active');
				}
			});
			
			HWS.on('click','modal_btn_add_equp',function(e,el){
				let specD = {};
				let modalBlock = HWS.upElem(el,'.modal-section');
				let newList = modalBlock.querySelectorAll('.list-equipments .one-equipment .equ_b .one_equipment_check.active')
				let frag = document.createDocumentFragment();
					specD.listId = []
				
				if(newList.length >0){
					HWF.recount(newList,function(elem){
						let li = HWS.upElem(elem,'.one-equipment');
						let data = {
							id:li.dataset.id,
							icon:li.querySelector('.equ_n .icon').classList[1],
							name:li.querySelector('.equ_n .text_name').innerHTML,
							color:'color:'+window.getComputedStyle(li.querySelector('.equ_d')).color,
							val:li.querySelector('.equ_d .data').innerHTML,
						}
						specD.listId.push(data.id);
						let newLi = document.createElement('li');
						newLi.classList.add('elem-equp');
						newLi.classList.add('sme_elem_'+data.id);
						newLi.dataset.id = data.id;
						newLi.innerHTML = ''+
							'<div><span class="icon '+data.icon+'"></span></div>'+
							'<div class="name">'+data.name+'</div>'+
							'<div class="state" style="'+data.color+'"><inf>'+data.val+'</inf></div>'+
							'<div>'+
								'<a href="#'+HWD.page.equp+'/'+data.id+'" class="icon p16 icon-settings hover block_mr_8"></a>'+
								'<span class="icon p20 icon-delete hover btn_delete_elem"></span>'+
							'</div>'+
						'';
						frag.appendChild(newLi);
					},'on');
					list.appendChild(frag);
					
					let fcn = Bl.dataset.fcn || '';
					if(fcn){HWS.buffer[fcn](specD)}
					HWF.modal.of();
					
				}
			});
		});
		
		// Нажатие на кнопку удалить
		HWS.on('click','btn_delete_elem',function(e,el){
			HWS.buffer.deleteLiSme = HWS.upElem(el,'.elem-equp');
			let name = HWS.buffer.deleteLiSme.querySelector('.name').innerHTML || 'устройство'
			HWF.modal.confirm('Удалить устройство?','Вы действительно хотите удалить <b>'+name+'</b> из этой программы?','acept_delete_equp');
		});
		// Подтверждение удаления
		HWS.on('click','acept_delete_equp',function(e,el){
			let Bl = HWS.upElem(HWS.buffer.deleteLiSme,'sme');
			let fcn = Bl.dataset.fcnd || '';
			let idd = HWS.buffer.deleteLiSme.dataset.id || '';
			if(fcn){HWS.buffer[fcn](idd)}
			HWS.buffer.deleteLiSme.remove();
		});
	}
	G.updateSme = function(elem,buf){
		elem = HWS.getElem(elem);
		let BList = elem.querySelector('.sme-list-equp');
		let allLi = BList.querySelectorAll('.elem-equp');
		let SpecControlData = {}
		if(buf && HWS.buffer.equps[buf]){ print_ses(HWS.buffer.equps[buf]);} 
		else {print_ses()} 
		
		function print_ses(newElem){
			if(newElem && newElem.length > 0){ 
				HWF.recount(newElem,function(onel){
					let nInf = HWequip.normInfo(onel);
					let elemLi = BList.querySelector('.sme_elem_'+nInf.id);
					let value,stateColor='';
					value = nInf.stateName || nInf.val;
					stateColor = 'color:'+nInf.stateColor;
					if(nInf.state == 'no_connect'){value = 'Нет связи';}
					
					if(elemLi){
						elemLi.querySelector('.name').innerHTML = nInf.name;
						elemLi.querySelector('.state').style.color = nInf.stateColor;
						elemLi.querySelector('.state inf').innerHTML = value;
					}
					else{
						let newLi = document.createElement('li');
						newLi.classList.add('elem-equp');
						newLi.classList.add('sme_elem_'+nInf.id);
						newLi.dataset.id = nInf.id;
						newLi.innerHTML = ''+
							'<div><span class="icon icon_equip_'+nInf.type+'"></span></div>'+
							'<div class="name">'+nInf.name+'</div>'+
							'<div class="state" style="'+stateColor+'"><inf>'+value+'</inf></div>'+
							'<div>'+
								'<a href="#'+HWD.page.equp+'/'+nInf.id+'" class="icon p16  icon-change hover block_mr_8"></a>'+
								'<span class="icon p20 icon-delete hover btn_delete_elem"></span>'+
							'</div>'+
						'';
						BList.appendChild(newLi);
					}
					SpecControlData[nInf.id] = 1;
				}); 
				// Удоление лишних
				if(allLi && allLi.length > 0){ HWF.recount(allLi,function(onli){
					if(!SpecControlData[onli.dataset.id]){onli.remove();}
				},'on');}
			}
			else {
				BList.innerHTML = '\
				<li class="one-head">\
					<div>Тип</div>\
					<div>Имя</div>\
					<div>Данные</div>\
					<div><i></i></div>\
				</li>';
			}
			
		}
	}
	
	G.createSes = function(){
		let allElem = G.getAllElem('ses');
		
		for (let n in allElem){
			let elem  = allElem[n], elemG = elem; 
			let nameElem = elem.innerHTML || 'Устройство управления'; elem.innerHTML = '';
			let span = document.createElement('span');
			let div = document.createElement('div');
			let id = elem.dataset.id || false
			let printTop = '<p class="ts_14p tc_black name">'+nameElem+'</p><p class="btn-return"></p>';
			let liE = document.createElement('li');
				liE.classList.add('elem-equp');
			let ul = document.createElement('ul');
			let liH = document.createElement('li');
				liH.classList.add('one-head');
				
			liE.innerHTML = '<div class="block_ml_6">Добавить</div>\
				<div class="ta_right block_mr_6">+</div>\
				<p class="click btn_add_elem_ses"></p>\
			';
			
			if(id && id != 'null' && id != 'undefined') {
				let idSystem = HWS.buffer.elem.id_system;
				liE.innerHTML = '<p></p>'+HWhtml.miniLoader()+'<p></p>';
				printTop = '<p class="ts_14p tc_black name">'+nameElem+'</p><p class="btn-return"><span class="btn_add_elem_ses prog-btn-add">↺</span></p>';
				
				if(HWS.buffer.equps && HWS.buffer.equps[id]){
					print_ses(HWS.buffer.equps[id],liE);
				} else {
					let dataP = {token:HWS.pageAccess,local_id:id,id_system:idSystem}
					HWF.post({url:HWD.api.gets.objectLocalId,data:dataP,fcn:function(getElem){
						print_ses(getElem,liE);
						if(elemG.dataset.load){HWS.buffer[elemG.dataset.load](getElem)}
					}});
				}
			} else {elemG.dataset.val = 'HWempty';}
			
			liH.innerHTML = printTop;
			ul.appendChild(liH);
			ul.appendChild(liE);
			elem.appendChild(ul);
		}
		
		function print_ses(elem,liE){
			if(elem.exceptions){
				liE.innerHTML = '\
					<p class="block_ml_6">Оборудование недоступно</p>\
				';
			}
			else {
				let value,stateColor='';
				let sys3xData = HWequip.normInfo(elem);
				value = sys3xData.stateName || sys3xData.val;
				stateColor = (sys3xData.stateColor == HWD.color.grey)? HWD.color.grey2 : sys3xData.stateColor;
				stateColor = 'color:'+stateColor;
				liE.innerHTML = ''+
					'<div class="ic"><a href="#equipment/'+elem.id+'"><span class="icon icon_equip_'+sys3xData.type+'"></span></a></div>'+
					'<div class="name ta_left"><a href="#equipment/'+elem.id+'">'+elem.config.name+'</a></div>'+
					'<div class="state" style="'+stateColor+'"><inf>'+value+'</inf></div>'+
					'<div>'+
						'<span></span>'+
						'<span class="icon p20 icon-delete hover btn_delete_elem_ses"></span>'+
					'</div>'+
				'';
			}
		}
			
		// Кнопка выбора оборудования 
		HWS.on('click','btn_add_elem_ses',function(e,el){
			let Bl = HWS.upElem(el,'ses');
			let filter = Bl.dataset.filter || 'relay' ;
			let idSystem = HWS.buffer.elem.id_system;
			let type = HWS.buffer.elem.info.type.type;
			
			HWF.modal.print('Выберите оборудование','<div id="ses-modal-wiev-list" class="list-equp-no-select block_w_100"></div>');

			if(filter == 'sensor'){
				console.log(HWS.buffer.elem);
				HWS.data.apiGet.listSensorDO.data = {"id_program":HWS.buffer.elem.id}
				console.log(HWS.data.apiGet.listSensorDO.data);
				HWF.print.listSensorDO('#ses-modal-wiev-list',{view:'list'});
			}
			if(filter == 'relay'){
				HWS.data.apiGet.listAutoDO.data = {"id_system":idSystem,'for_object_type':type}
				HWF.print.listAutoDO('#ses-modal-wiev-list',{view:'list'});
			}
			if(filter == 'pump'){
				HWS.data.apiGet.listPumpDO.data = {"id_system":idSystem}
				console.log(HWS.data.apiGet.listPumpDO);
				HWF.print.listPumpDO('#ses-modal-wiev-list',{view:'list'});
			}

			HWS.on('click','btn-one-equip',function(e,el){
				let li = Bl.querySelector('.elem-equp');
				let header = Bl.querySelector('.one-head');
				let nameBlock = Bl.querySelector('.one-head .name').innerHTML;
				let elem = HWS.parentSearchClass(el,'one-equipment');
				let icon = elem.querySelector('.equ_n .icon').classList[1];
				let name = elem.querySelector('.equ_n .text_name').innerHTML;
				let Scolor = elem.querySelector('.equ_d').classList[1];
				let val = elem.querySelector('.equ_d .data').innerHTML;
				let data = elem.dataset.val || '';
				let type = elem.dataset.typetype || '';
				let id = elem.dataset.id || '';
				let rElem = ''; let fcn = Bl.dataset.fcn || '';
				
				if(!li){
					Bl.querySelector('.one-head .prog-btn-add').innerHTML = '↺';
					li = document.createElement('li'); 
					li.classList.add('elem-equp'); 
					Bl.querySelector('ul').appendChild(li);
				}

				Bl.dataset.val = data || 0;
				
				header.innerHTML = '<p class="ts_14p tc_black name">'+nameBlock+'</p><p class="btn-return"><span class="btn_add_elem_ses prog-btn-add">↺</span></p>';
				li.innerHTML = ''+
					'<div><a href="#equipment/'+id+'"><span class="icon '+icon+'"></span></a></div>'+
					'<div class="ta_left name"><a href="#equipment/'+id+'">'+name+'</a></div>'+
					'<div class="'+Scolor+' state"><inf>'+val+'</inf></div>'+
					'<div>'+
						'<span></span>'+
						'<span class="icon p20 icon-delete hover btn_delete_elem_ses"></span>'+
					'</div>'+
				'';
				HWF.modal.of();

				if(fcn){
					HWF.recount(HWS.buffer.equpsList,function(elList){if (id == elList.id){rElem = elList}});
					HWS.buffer[fcn](rElem)
				}
			});
		});
		
		// Нажатие на кнопку удалить
		HWS.on('click','btn_delete_elem_ses',function(e,el){
			HWS.buffer.deleteLiSes = HWS.upElem(el,'.elem-equp');
			let name = HWS.buffer.deleteLiSes.querySelector('.name a').innerHTML || 'устройство'
			HWF.modal.confirm('Удалить устройство?','Вы действительно хотите удалить <b>'+name+'</b> из этой программы?','acept_delete_equp_ses');
		});
		// Подтверждение удаления
		HWS.on('click','acept_delete_equp_ses',function(e,el){
			let Bl = HWS.upElem(HWS.buffer.deleteLiSes,'ses');
			let header = Bl.querySelector('.one-head');
			let nameElem = Bl.querySelector('.one-head .name').innerHTML;
			let elem = Bl.querySelector('.elem-equp');
			
			header.innerHTML = '<p class="ts_14p tc_black name">'+nameElem+'</p><p class="btn-return"></p>';
			elem.innerHTML = '<div class="block_ml_6">Добавить</div>\
				<div class="ta_right block_mr_6">+</div>\
				<p class="click btn_add_elem_ses"></p>\
			';
			Bl.dataset.val = 'HWempty';
			let fcn = Bl.dataset.fcnd || '';
			if(fcn){HWS.buffer[fcn]()}
			else{
				let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.elem.id,id_automatic_device:'delete'}
				HWF.post({url:HWD.api.gets.taskShceduleProgram3xSystem,data:dataP,fcnE:HWF.getServMesErr});
			}
		});
	}
	G.updateSes = function(elem,id){
		elem = HWS.getElem(elem);
		if(id){
			id = parseInt(id);
			if(HWS.buffer.equps[id]){;
				print_ses(HWS.buffer.equps[id]);
			} else {
				let idSystem = HWS.buffer.elem.id_system; idSystem = parseInt(idSystem);
				let dataP = {token:HWS.pageAccess,local_id:id,id_system:idSystem}
				HWF.post({url:HWD.api.gets.objectLocalId,data:dataP,fcn:print_ses});
			}
		} else {print_ses()} 
		
		function print_ses(newElem){
			let printTop = '';
			let Btop = elem.querySelector('.one-head');
			let Bcont = elem.querySelector('.elem-equp');
			if(newElem && !newElem.exceptions){
				let sys3xData = HWequip.normInfo(newElem);
				let value='',stateColor='',stateColorD='';
				value = sys3xData.val;
				value = sys3xData.stateName || sys3xData.val;
				stateColor = (sys3xData.stateColor == HWD.color.grey)? HWD.color.grey2 : sys3xData.stateColor;
				stateColor = 'color:'+stateColor;
				stateColorD = (sys3xData.stateColor == HWD.color.grey)? HWD.color.grey2 : sys3xData.stateColor;
				if(sys3xData.state == 'no_connect'){value = 'Нет связи';}
				
				if(!Bcont.querySelector('.btn_add_elem_ses') && id == newElem.local_id){
					Bcont.querySelector('.name').innerHTML = newElem.config.name;
					Bcont.querySelector('.state').style.color = stateColorD;
					Bcont.querySelector('.state inf').innerHTML = value;
				} else {
					Btop.querySelector('.btn-return').innerHTML = '<span class="btn_add_elem_ses prog-btn-add">↺</span>';
					Bcont.innerHTML = ''+
						'<div class="ic"><a href="#equipment/'+newElem.id+'"><span class="icon icon_equip_'+newElem.info.type.type+'"></span></a></div>'+
						'<div class="name ta_left"><a href="#equipment/'+newElem.id+'">'+newElem.config.name+'</a></div>'+
						'<div class="state" style="'+stateColor+'"><inf>'+value+'</inf></div>'+
						'<div>'+
							'<span></span>'+
							'<span class="icon p20 icon-delete hover btn_delete_elem_ses"></span>'+
						'</div>'+
					'';
				}
			} else {
				if(!Bcont.querySelector('.btn_add_elem_ses')){
					Btop.querySelector('.btn-return').innerHTML = '';
					Bcont.innerHTML = '\
						<div class="block_ml_6">Добавить</div>\
						<div class="ta_right block_mr_6">+</div>\
						<p class="click btn_add_elem_ses"></p>\
					';
				}
			}
		}
	}
	
	G.createNumber = function(){
		let allElem = G.getAllElem('number');
		
		for (let n in allElem){
			let val = parseInt(allElem[n].innerHTML) || '0';
			let elems = createElement();
				elems.input.value = val;
			allElem[n].innerHTML = '';
			allElem[n].appendChild(elems.span1);
			allElem[n].appendChild(elems.input);
			allElem[n].appendChild(elems.span2);
		}
		
		function createElement(){
			let allE = {};
			allE.span1 = document.createElement('span');
			allE.span1.classList.add('number_minus');
			allE.input = document.createElement('input');
			allE.input.type = "text";
			allE.span2 = document.createElement('span');
			allE.span2.classList.add('number_plus');
			
			allE.span1.addEventListener('click',function(){
				let fcn = this.parentElement.dataset.fcn || '';
				let main = allE.span1.parentNode;
				let input = main.querySelector('input');
				let val = parseInt(input.value) || 0;
				
				val--;
				input.value = val;
				if(fcn){HWS.buffer[fcn](val)}
			});
			
			allE.input.addEventListener('change',function(){
				let fcn = this.parentElement.dataset.fcn || '';
				let val = this.value;
				if(fcn){HWS.buffer[fcn](val)}
			});
			
			allE.span2.addEventListener('click',function(){
				let fcn = this.parentElement.dataset.fcn || '';
				let main = allE.span1.parentNode;
				let input = main.querySelector('input');
				let val = parseInt(input.value) || 0;
				
				val++;
				input.value = val;
				if(fcn){HWS.buffer[fcn](val)}
			});
			
			return(allE)
		}
	}
	
	G.createText = function(){
		let allElem = G.getAllElem('text');
		
		for (let n in allElem){
			let val = allElem[n].innerHTML || '';
			let elems = createElement();
				elems.input.value = val;
			allElem[n].innerHTML = '';
			allElem[n].appendChild(elems.span);
			allElem[n].appendChild(elems.input);
		}
		
		function createElement(){
			let allE = {};
			allE.span = document.createElement('span');
			allE.span.classList.add('text_save');
			allE.input = document.createElement('input');
			allE.input.type = "text";
			
			return(allE)
		}
	}
	
	G.checkbut = function(fcnON,fcnOF){
		fcnON = fcnON || function(){}
		fcnOF = fcnOF || function(){}
		let allElem = G.getAllElem('checkbut');
		
		for (let k in allElem){ allElem[k].addEventListener('click',click); }
		
		function click(e){
			let elem = e.target;
			if (elem.classList.contains('active')){elem.classList.remove('active'); fcnOF(elem);}
			else {elem.classList.add('active'); fcnON(elem);}
		}
	}
	
	G.checkbox = function(){
		let allElem = G.getAllElem('checkbox');
		
		for (let k in allElem){ allElem[k].addEventListener('click',click); }
		
		function click(e){
			let elem = e.target;
			let fcn = this.dataset.fcn || false;
			let testClassNoOf = elem.classList.contains('no_of');
			
			if(testClassNoOf && elem.classList.contains('active')){}
			else{
				if(elem.getAttribute("name")){
					let testClass = elem.classList.contains('active');
					let allcheckbutCD = document.querySelectorAll('.checkbox[name="'+elem.getAttribute("name")+'"]');
					let allcheckbutED = document.querySelectorAll('checkbox[name="'+elem.getAttribute("name")+'"]');
					let allElemD = []; allElemD.push.apply(allElemD,allcheckbutCD); allElemD.push.apply(allElemD,allcheckbutED);
					
					for (let d in allElemD){ allElemD[d].classList.remove('active');}
					if (!testClass) {elem.classList.add('active');  (fcn)?HWS.buffer[fcn]('on',elem):'';} 
					else {(fcn)?HWS.buffer[fcn]('of',elem):'';}
				}
				else {
					if (elem.classList.contains('active')){elem.classList.remove('active'); (fcn)?HWS.buffer[fcn]('of',elem):'';}
					else {elem.classList.add('active'); (fcn)?HWS.buffer[fcn]('on',elem):'';}
				}
			}
		}
	}
	
	G.createLimit = function (){
		let allElem = G.getAllElem('limit');
		let data = {
			type:"norm",
			min:-40,
			max:100,
			war:true,
			ale:true,
			warMin:false,
			warMax:false,
			aleMin:false,
			aleMax:false,
			aleTitle:false,
			warTitle:false
		}
		for (let k in allElem){
			let elem = allElem[k];
			if(elem.getAttribute("type")){data.type = elem.getAttribute("type")} else {data.type = 'norm'};
			if(elem.getAttribute("war") && elem.getAttribute("war") == 'false'){data.war = false} else {data.war = true};
			if(elem.getAttribute("ale") && elem.getAttribute("ale") == 'false'){data.ale = false} else {data.ale = true};
			if(elem.getAttribute("min")){data.min = parseInt(elem.getAttribute("min"))} else {data.min = -40};
			if(elem.getAttribute("max")){data.max = parseInt(elem.getAttribute("max"))} else {data.max = 100};
			if(elem.getAttribute("warMin")){data.warMin = parseInt(elem.getAttribute("warMin"))} else {data.warMin = false};
			if(elem.getAttribute("warMax")){data.warMax = parseInt(elem.getAttribute("warMax"))} else {data.warMax = false};
			if(elem.getAttribute("aleMin")){data.aleMin = parseInt(elem.getAttribute("aleMin"))} else {data.aleMin = false};
			if(elem.getAttribute("aleMax")){data.aleMax = parseInt(elem.getAttribute("aleMax"))} else {data.aleMax = false};
			if(elem.getAttribute("warTitle")){data.warTitle = elem.getAttribute("warTitle")} else {data.warTitle = false};
			if(elem.getAttribute("aleTitle")){data.aleTitle = elem.getAttribute("aleTitle")} else {data.aleTitle = false};
			elem.innerHTML = G.onelimit(data); 
			G.limit(elem.querySelector('.predel-scale-p'));
		}
	}
	// шаблон скрола пределов
	G.onelimit = function(data){
		data = data || {
			type:"norm",
			min:-40,
			max:100,
			war:true,
			ale:true,
		}
		

		let s = '',numberStep='',shtrih='';
		let min = data.min || 0;
		let max = data.max || 100;
		let ale = (data.ale)?'active':'';
		let war = (data.war)?'active':'';
		let warMin = data.warMin || min+10;
		let warMax = data.warMax || max-10;
		let aleMin = data.aleMin || min+5;
		let aleMax = data.aleMax || max-5;
		let aleTitle = data.aleTitle || 'Тревога';
		let warTitle = data.warTitle || 'Внимание';
		
		let iter = min;
		while (iter <= max){
			numberStep += '<p>'+iter+'</p>';
			shtrih += '<p>|</p>';
			iter = iter+10;
		}
		let specKof = 100/(max-min);
		let btnWarMin = parseFloat((warMin-min)*specKof).toFixed(4);
		let btnWarMax = parseFloat((max-warMax)*specKof).toFixed(4);
		let btnAleMin = parseFloat((aleMin-min)*specKof).toFixed(4);
		let btnAleMax = parseFloat((max-aleMax)*specKof).toFixed(4);
		
		let header = '\
		<header>\
			<div class="warning-min-max '+war+'">\
				<p class="flex_between">\
					<span>'+warTitle+'</span>\
					<span class="btn-active on-off warning-on-off '+war+'" data-targetplus="warning-min-max"></span>\
				</p>\
				<div class="min-max">\
					<number class="predel-control-warning-min">\
						<span class="btn_war_min_minus number_minus"></span>\
						<input class="text-war-min" type="text" value="'+warMin+'">\
						<span class="btn_war_min_plus number_plus"></span>\
					</number>\
					<h6>min</h6>\
				</div>\
				<div class="min-max">\
					<number class="predel-control-warning-max">\
						<span class="btn_war_max_minus number_minus"></span>\
						<input class="text-war-max" type="text" value="'+warMax+'">\
						<span class="btn_war_max_plus number_plus"></span>\
					</number>\
					<h6>max</h6>\
				</div>\
			</div>\
			<div class="alert-min-max '+ale+'">\
				<p class="flex_between">\
					<span>'+aleTitle+'</span>\
					<span class="btn-active on-off alert-on-off '+ale+'" data-targetplus="alert-min-max"></span>\
				</p>\
				<div class="min-max predel-control-alert-min">\
					<number class="predel-control-alert-min">\
						<span class="btn_ale_min_minus number_minus"></span>\
						<input class="text-ale-min" type="text" value="'+aleMin+'">\
						<span class="btn_ale_min_plus number_plus"></span>\
					</number>\
					<h6>min</h6>\
				</div>\
				<div class="min-max">\
					<number class="predel-control-alert-max">\
						<span class="btn_ale_max_minus number_minus"></span>\
						<input class="text-ale-max" type="text" value="'+aleMax+'">\
						<span class="btn_ale_max_plus number_plus"></span>\
					</number>\
					<h6>max</h6>\
				</div>\
			</div>\
		</header>\
		';
		let mid = '\
		<section>\
			<div class="number-temp">'+numberStep+'</div>\
			<div class="scale-temp">\
				<span class="shtrih select-none">'+shtrih+'</span>\
				<span class="dop-left"></span>\
				<span class="dop-right"></span>\
				<div class="line line-war-min '+war+'" style="width:'+btnWarMin+'%;"><span class="btn-line btn-war-min">min</span></div>\
				<div class="line line-alt-min '+ale+' red-left-scale" style="width:'+btnAleMin+'%;"><span class="btn-line btn-alt-min">min</span></div>\
				<div class="line line-war-max '+war+' orange-righ-scale" style="width:'+btnWarMax+'%;"><span class="btn-line btn-war-max">max</span></div>\
				<div class="line line-alt-max '+ale+' red-righ-scale" style="width:'+btnAleMax+'%;"><span class="btn-line btn-alt-max">max</span></div>\
			</div>\
		</section>\
		';
		
		let dopClass = '';
		if(data.type == "notop"){dopClass = 'notop';}
		
		s = '<div class="predel-scale-p '+dopClass+'" data-min="'+min+'" data-max="'+max+'">'+header+mid+'</div>';
		
		return s;
	}
	
	G.limit = function(elem){
		let GL = {}; GL.min = 0; GL.max = 0;
		let S = {};
		
		GL.click = function click(event){
			let el = this;
			let line = el.parentNode;
			let container = el.parentNode.parentNode; //scale-temp
			
			S.elemClick = el;
			S.elem = line;
			S.input = '';
			S.x = event.pageX || event.targetTouches[0].pageX; 
			S.l = parseFloat(parseFloat(line.style.width).toFixed(4));
			S.kof = parseFloat((100/container.offsetWidth).toFixed(4));
			
			if(el.classList.contains('btn-war-min') ){
				S.input = GL.input.warMin;
				S.elemClick.addEventListener('mousemove',GL.move);
				S.elemClick.addEventListener('touchmove',GL.move);
			}
			
			if (el.classList.contains('btn-alt-min')) {
				S.input = GL.input.aleMin;
				S.elemClick.addEventListener('mousemove',GL.move);
				S.elemClick.addEventListener('touchmove',GL.move);
			}
			
			if (el.classList.contains('btn-war-max')){
				S.input = GL.input.warMax;
				S.elemClick.addEventListener('mousemove',GL.mover);
				S.elemClick.addEventListener('touchmove',GL.mover);
			}
			
			if (el.classList.contains('btn-alt-max')){
				S.input = GL.input.aleMax;
				S.elemClick.addEventListener('mousemove',GL.mover);
				S.elemClick.addEventListener('touchmove',GL.mover);
			}
			
			document.body.removeEventListener('pointerup',GL.end);
			document.body.addEventListener('pointerup',GL.end);
		}
		GL.move = function(event){
			let x,y;
			x = event.pageX || event.targetTouches[0].pageX; 
			x = (x-S.x)*S.kof+S.l;
			if(x >= 100){x=100}; if(x <= 0){x=0}
			S.elem.style.width = x+'%';
			GL.warMin = (x/GL.specKof)+GL.min;
			S.input.value = parseInt(GL.warMin);
		}
		GL.mover = function(event){
			let x,y;
			x = event.pageX || event.targetTouches[0].pageX; 
			x = (S.x-x)*S.kof+S.l;
			if(x >= 100){x=100}; if(x <= 0){x=0}
			S.elem.style.width = x+'%';
			GL.warMin = GL.max-(x/GL.specKof);
			S.input.value = parseInt(GL.warMin);
		}
		GL.end = function(e){
			S.elemClick.removeEventListener('mousemove',GL.move);
			S.elemClick.removeEventListener('touchmove',GL.move);
			S.elemClick.removeEventListener('mousemove',GL.mover);
			S.elemClick.removeEventListener('touchmove',GL.mover);
		}
		
		GL.plus = function(e){
			let elem = this;
			let block = elem.parentNode;
			let input = block.querySelector('input');
			let numb = parseInt(input.value);
			let btn ='',line='',t='max';
			numb++; if(numb >= GL.max){numb = GL.max} input.value = numb;
			
			if(elem.classList.contains('btn_war_min_plus')){line = GL.line.warMin;t='min'}
			if(elem.classList.contains('btn_ale_min_plus')){line = GL.line.altMin;t='min'}
			if(elem.classList.contains('btn_war_max_plus')){line = GL.line.warMax;}
			if(elem.classList.contains('btn_ale_max_plus')){line = GL.line.altMax;}
			line.style.width = line_position(numb,t)+'%';
		}
		
		GL.minus = function (e){
			let elem = this;
			let block = elem.parentNode;;
			let input = block.querySelector('input');
			let numb = parseInt(input.value);
			let btn = '',line='',t='max';
			numb--; if(numb <= GL.min){numb = GL.min} input.value = numb;
			
			if(elem.classList.contains('btn_war_min_minus')){line = GL.line.warMin;t='min'}
			if(elem.classList.contains('btn_ale_min_minus')){line = GL.line.altMin;t='min'}
			if(elem.classList.contains('btn_war_max_minus')){line = GL.line.warMax;}
			if(elem.classList.contains('btn_ale_max_minus')){line = GL.line.altMax;}
			line.style.width = line_position(numb,t)+'%';
		}

		GL.lineClick = function(e){
			let el = this;
			if(el.classList.contains('open')){el.classList.remove('open');}
			else {
				let active = G.searchParent(el,'.scale-temp').querySelector('.line.open');
				if(active){active.classList.remove('open')}
				el.classList.add('open');
			}
		}
		
		GL.warOnOff = function(e){
			let el = this;
			if(el.classList.contains('active')){
				GL.line.warMin.classList.add('active');
				GL.line.warMax.classList.add('active');
				GL.line.warMin.classList.remove('select-none');
				GL.line.warMax.classList.remove('select-none');
			}
			else {
				GL.line.warMin.classList.remove('active');
				GL.line.warMax.classList.remove('active');
				GL.line.warMin.classList.add('select-none');
				GL.line.warMax.classList.add('select-none');
			}
		}
		GL.aleOnOff = function(e){
			let el = this;
			if(el.classList.contains('active')){
				GL.line.altMin.classList.add('active');
				GL.line.altMax.classList.add('active');
				GL.line.altMin.classList.remove('select-none');
				GL.line.altMax.classList.remove('select-none');
			}
			else {
				GL.line.altMin.classList.remove('active');
				GL.line.altMax.classList.remove('active');
				GL.line.altMin.classList.add('select-none');
				GL.line.altMax.classList.add('select-none');
			}
		}
		
		GL.change = function(e){
			let el = this;
			let numb = parseInt(el.value);
			let btn = '',line='',t='max';
			if(numb <= GL.min){numb = GL.min} el.value = numb;
			if(numb >= GL.max){numb = GL.max} el.value = numb;
			
			if(el.classList.contains('text-war-min')){line = GL.line.warMin;t='min'}
			if(el.classList.contains('text-ale-min')){line = GL.line.altMin;t='min'}
			if(el.classList.contains('text-war-max')){line = GL.line.warMax;}
			if(el.classList.contains('text-ale-max')){line = GL.line.altMax;}
			line.style.width = line_position(numb,t)+'%';
		}
		
		GL.start = function(elem){
			GL.elem = elem; GL.line = {}; GL.input = {};
			GL.min = parseInt(elem.dataset.min) || 0; GL.max = parseInt(elem.dataset.max) || 0;

			GL.input.warMin = GL.elem.querySelector('header .predel-control-warning-min input');
			GL.input.warMax = GL.elem.querySelector('header .predel-control-warning-max input');
			GL.input.aleMin = GL.elem.querySelector('header .predel-control-alert-min input');
			GL.input.aleMax = GL.elem.querySelector('header .predel-control-alert-max input');
			
			GL.warMin = GL.input.warMin.value || 0;
			GL.warMax = GL.input.warMax.value || 0;
			GL.aleMin = GL.input.aleMin.value || 0;
			GL.aleMax = GL.input.aleMax.value || 0;
			
			GL.line.warMin = GL.elem.querySelector('.line-war-min');
			GL.line.warMax = GL.elem.querySelector('.line-war-max');
			GL.line.altMin = GL.elem.querySelector('.line-alt-min');
			GL.line.altMax = GL.elem.querySelector('.line-alt-max');
			
			GL.specKof = 100/(GL.max-GL.min);
			//on - off
			elem.querySelector('.warning-on-off').addEventListener('click',GL.warOnOff);
			elem.querySelector('.alert-on-off').addEventListener('click',GL.aleOnOff);
			//line
			Array.apply(null,elem.querySelectorAll('.line')).forEach(function(elem){
				elem.addEventListener('click',GL.lineClick);
			});
			//elem.querySelector('.line').addEventListener('click',GL.lineClick);
			// input onkeydown change
			elem.querySelector('.text-war-min').addEventListener('change',GL.change);
			elem.querySelector('.text-war-max').addEventListener('change',GL.change);
			elem.querySelector('.text-ale-min').addEventListener('change',GL.change);
			elem.querySelector('.text-ale-max').addEventListener('change',GL.change);
			//Warning (внимание)
			elem.querySelector('.btn_war_min_minus').addEventListener('click',GL.minus);
			elem.querySelector('.btn_war_min_plus').addEventListener('click',GL.plus);
			elem.querySelector('.btn_war_max_minus').addEventListener('click',GL.minus);
			elem.querySelector('.btn_war_max_plus').addEventListener('click',GL.plus);
			//Alert (Тревога)
			elem.querySelector('.btn_ale_min_minus').addEventListener('click',GL.minus);
			elem.querySelector('.btn_ale_min_plus').addEventListener('click',GL.plus);
			elem.querySelector('.btn_ale_max_minus').addEventListener('click',GL.minus);
			elem.querySelector('.btn_ale_max_plus').addEventListener('click',GL.plus);
			//Scrol
			elem.querySelector('.btn-war-min').addEventListener('pointerdown',GL.click);
			elem.querySelector('.btn-war-max').addEventListener('pointerdown',GL.click);
			elem.querySelector('.btn-alt-min').addEventListener('pointerdown',GL.click);
			elem.querySelector('.btn-alt-max').addEventListener('pointerdown',GL.click);
		}
		
		function line_position(numb,type){
			let ret = ''
			if (type == 'max'){ret = parseFloat((GL.max-numb)*GL.specKof).toFixed(4);}
			if (type == 'min'){ret = parseFloat((numb-GL.min)*GL.specKof).toFixed(4);}
			return ret;
		}
		
		GL.start(elem);
	}
	
	G.getAllElem = function(elem){
		let allcheckbutC = document.querySelectorAll('.'+elem);
		let allcheckbutE = document.querySelectorAll(elem);
		let allElem = []; allElem.push.apply(allElem,allcheckbutC); allElem.push.apply(allElem,allcheckbutE);
		
		return allElem;
	}
	
	G.searchParent = function(el,selector){
		let elem; while ((el = el.parentElement) && !(elem = el.querySelector(selector))); return elem;
	}
}
