"use strict";
var HWD = new function(){ //Глобальные данные для работы
	let G = this;
	let GL = {};
	// версии страниц шаблонов
	G.v = {
		"programs":1,
		"info":1,
		"settings":1
	}
	// страници
	G.page = {
		main:'main', // главная страница
		login:'login', // страница авторизации
		proj:'project', // страница с 1 проектом
		projAdd:'project-add', // добавить проект
		equp:'equipment', // страница с 1 оборудованием
		equps:'equipments', // страница с оборудованием
		prog:'program', // страница с 1 программой
		progs:'programs', // страница программами
		group:'group', // страница с 1 группой
		temps:'templates', // страница с Шаблонами
		temp:'template', // страница с 1 Шаблоном
	}
	// API
	G.api = {
		imgDir: 'uploads/', // папка ОТКУДА грузятся картинки
		url: serverAPIURL+'api/', // урл до самого сервера ( задаётся в отдельном файле )
		foreign_login: serverAPIURL+'foreign_login', // урл какойто магии с яндексом }
		queries:{
			projects:{url:'projects/get_projects',data:{}},
			equipments:{url:'objects/list',data:{local_id_object_type:[2,0],local_id_object_group:[0,1,2]}},  
			system:{url:'objects/list',data:{local_id_object_type:0,local_id_object_group:0}},
			templates:{url:'patterns/get_list',data:{}},
			programs:'programs',
			users:'users',
			groups:'groups/list',
			usersGroups:'groups/list_group_users',
			equpGroups:'groups/list_group_objects',
			listAutoDO:{url:'objects/list_3x_automatic_device_objects'},
			listPumpDO:{url:'objects/list_3x_automatic_device_objects_for_pump'},
			listSensorDO:{url:'objects/list_3x_sensor_not_in_program_objects'},
			listVijet:{url:'objects/list_objects_for_add_to_favorites',data:{local_id_object_type:'2'}},
			systemVijet:{url:'objects/list_systems_for_add_to_favorites',data:{}},
			listProjectsEqup:{url:'projects/list_objects_for_add_to_project',data:{object_to_add:'device'}},
			listProjectsProj:{url:'projects/list_objects_for_add_to_project',data:{object_to_add:'project'}}
			// по [ключу] необходимо сформировать функцию в "hw-html.js" для вывода этого списка
		},
		gets:{ // HWD.api.gets.
			object:'objects/get_object', //запрос объекта
			objectLocalId:'objects/get_object_by_local_id', //запрос объекта local ID
			list:'objects/list', //запрос листа объектов
			listMore:'objects/list_objects_with_more_info', //запрос листа объектов
			sendTask:'task/send_task', //отправить задание системе
			taskName3xDevice:'task/change_name_3x_device', // изменение имени устройства 3x
			taskFlagsToAutomatic3xDevice:'task/set_flags_to_automatic_3x_device', // изменение имени устройства 3x
			sendDeviceState:'task/send_device_state', //отправить задание системе на изменение статуса
			taskCommandTo3xSystem:'task/send_command_to_3x_system', //Отправление команды системе 3.х
			taskParametersTo3xSystem:'task/send_parameters_to_3x_system', //Изменние параметров системы 3.х
			taskGsmParametersTo3xSystem:'task/send_gsm_parameters_to_3x_system', //Изменние GSM параметров системы 3.х
			taskParametersTo3xSensor:'task/send_parameters_to_3x_sensor', //Изменние GSM параметров системы 3.х
			taskFlagsTo3xSensor:'task/set_flags_to_3x_system_sensor', //Изменние флагов датчика системы 3.х
			taskNotificationProgram3xSystem:'task/send_notification_program_3x_system', //Изменние параметров программы оповещения системы 3.х
			taskPhoneNotificationProgram3xSystem:'task/send_phone_data_notification_program_3x_system', //Изменние телефона программы оповещения 3.х
			taskDelPhoneNotificationProgram3xSystem:'task/delete_phone_notification_program_3x_system', //Изменние телефона программы оповещения 3.х
			taskDelProgram3xSystem:'task/delete_program_3x_system', //Изменние телефона программы оповещения 3.х
			taskShceduleProgram3xSystem:'task/send_shcedule_program_3x_system', //Изменние телефона программы оповещения 3.х
			taskROSProgram3xSystem:'task/send_reaction_on_sensors_program_3x_system', //Изменние параметров программы "Реакция на датчики" системы 3.х
			taskAddROSProgram3xSystem:'task/add_sensors_to_reaction_on_sensors_program_3x_system', //Добавление датчиков в программу"Реакция на датчики" системы 3.х
			taskDelROSProgram3xSystem:'task/delete_sensors_from_reaction_on_sensors_program_3x_system', //удаление из программы "Реакция на датчики" системы 3.х
			taskSecurityProgram3xSystem:'task/send_security_program_3x_system', // "Охрана" системы 3.х
			taskAddSecurityProgram3xSystem:'task/add_sensors_to_security_program_3x_system', // "Охрана" добавить сенсор системы 3.х
			taskDelSecurityProgram3xSystem:'task/delete_sensors_from_security_program_3x_system', // "Охрана" добавить удалить системы 3.х
			taskHeatingProgram3xSystem:'task/send_heating_program_3x_system', // параметры "Отопление" системы 3.х
			taskAddProg3xSystem:'task/add_program_3x_system', //Создание программ системе 3.х
			taskOpentherm3xsystem:'task/send_opentherm_program_3x_system', //Отправление параметров программе "Opentherm" системы 3.х
			taskSendSystemTime:'task/send_system_time', //Изменние параметров системы 3.х
			taskSendSystemName:'task/send_system_name', //Изменние имени системы 3.х
			taskSendParametersToCamera:'task/send_parameters_to_3x_camera', //Камера системы 3.х
			sendFor3xSystemSensorTypes:'task/send_for_3x_system_sensor_types', //Конфигурирование дачиков портов Д1-Д5 системы 3х
			chartValue:'history/chart_value_data', //отправить задание системе
			chartState:'history/chart_state_data', //отправить статус системе
			register3xSystem:'user/registrate_3x_system', //Регистрация новой системы
			objectsList3xSystem:'objects/list_systems', //Список систем пользователя
			changeMesag3xSystem:'objects/change_systems_alert', // Изменение оповещения для системы
			multyCartsData:'history/multy_charts_data', // Изменение оповещения для системы
			newsList:'news/list_news', // список новостей
			getNews:'news/get_news', // Получить 1 новость
			subscriptionSet:'objects/set_subscription', // Установка подписки MultiValue
			subscriptionGet:'objects/get_subscription_data', //Получение данных по подписке (если нет возможности установить связь по вебсокету)
			projectsGet:'projects/get_projects_from_id_project', //Запрос проектов
			userFriendGet:'user/get_friendly_users', //получит список друзей пользователя
			userFriendDel:'user/delete_friendly_user', //Удаление друга
			userFriendCon:'user/confirm_add_friendly_user', //Пинятие друга
			userFriendAvo:'user/avoidance_from_add_friendly_user', //Пинятие друга
			projChange:'projects/change_project_data', //Изменение данных проекта
			projSetUser:'projects/set_project_security_for_user', //Добавить пользователя к проекту 
			projDelUser:'projects/remove_project_from_user', //Удалить пользователя к проекту 
			projChangeName:'projects/change_project_name', //изменение Имени проекта
			projChangeType:'projects/change_project_type', //изменение Типа проекта
			projChangeMain:'projects/change_main_project', //Сделать проект главны
			projListUser:'projects/list_project_users', //список друзей в пректе
			objectsSecurity:'objects/set_objects_security_for_users', //Установка прав на проект
			objectsSecurityUserList:'objects/list_object_users', //Лист пользователй у которых есть доступ к данному объекту
			objectsSecurityDel:'objects/remove_objects_from_users', //Удаление прав с оборудования
			vijetAdd:'objects/add_favorite_objects', //Добавление плиток в раздел виджетов
			objectsDelete:'objects/delete_objects', //удалени объектов с ЛК
			eventsHistory:'history/events_history', // история по оборудованию
			eventsProgHistory:'history/program_events', // история по оборудованию
			historyСharts:'history/charts', //графики для ЛК
			setWebConfig:'objects/set_web_config', //графики для ЛК
		}
	}
	
	// Колориста цвета для всяких нужд
	G.color = {
		red:  '#FF0000',
		orange:'#FAB400',
		green:'#0BC40B',
		green2:'#039003',
		blue: '#569AD4',
		grey: '#D3D4D3',
		grey2: '#6C6D6F',
		black:'#000000',
		white:'#ffffff',
	} // G.color.red
	
	// колориста для реле
	GL.equpStateControlRelay = {
		0:{name:'Откл',color:G.color.grey}, 1:{name:'Вкл',color:G.color.blue},
		3:{name:'Откл',color:G.color.grey},
		254:{name:'Ошибка',color:G.color.red},255:{name:'Нет связи',color:G.color.grey},
	}
	GL.equpStateControlCrane = {
		0:{name:'Закрыт',color:G.color.grey}, 1:{name:'Открыт',color:G.color.blue},
		3:{name:'Откл',color:G.color.grey},
		254:{name:'Ошибка',color:G.color.red},255:{name:'Нет связи',color:G.color.grey},
	}
	// Типы устройств оборудования
	G.equp = {
		// system 4x
		1888:{title:'Датчик температуры',tmp:'analog4x',min:-40,max:100,system:40},
		
		416:{title:'Универсальный контактный датчик',tmp:'discret4x',normT:'Замкнут',alerT:'Разомкнут',norm:1,system:40},
		
		6944:{title:'Реле электромагнитное',tmp:'control_device4x',state:GL.equpStateControlRelay,btnName:'Включать',system:40},
		
		// system
		14719:{title:'ectoControl',tmp:'systemEmpty',system:32},
		14720:{title:'ectoControl v3.0',tmp:'system3x',system:30}, // system_info_dev = 48
		14721:{title:'ectoControl v3.1',tmp:'system3x',system:31}, // system_info_dev = 49
		14722:{title:'ectoControl v3.2',tmp:'system3x',system:32}, // system_info_dev = 50
		14723:{title:'ectoControl v3.3',tmp:'system3x',system:33}, // system_info_dev = 51
		14724:{title:'ectoControl v4.0',tmp:'system4x',system:40},
		
		// system 3x
		1:{title:'Адаптер OpenTherm',tmp:'adapter_boiler3x',system:32},
		2:{title:'Адаптер eBUS',tmp:'adapter_boiler3x',system:32},
		
		353:{title:'Радиобрелок',tmp:'radio_brelok3x',system:32},
		1633:{title:'Ключ ТМ',tmp:'key_tm3x',system:32},
		
		1569:{title:'Датчик влажности воздуха',tmp:'analog3x',min:0,max:100,system:32,pravka:{min:-100,max:100}},
		1889:{title:'Датчик температуры',tmp:'analog3x',min:-40,max:100,system:32},
		321:{title:'Резервная АКБ системы',tmp:'analog3x',min:0,max:100,system:32,pravka:{min:-100,max:100}},
		320:{title:'Резервная АКБ системы',tmp:'analog3x',min:0,max:100,system:32,pravka:{min:-100,max:100}},
		384:{title:'Датчик тока 4...20мА',tmp:'analog3x',min:0,max:100,system:32},
		385:{title:'Датчик тока 4...20мА',tmp:'analog3x',min:0,max:100,system:32},
		
		417:{title:'Универсальный контактный датчик',tmp:'discret3x',normT:'Замкнут',alerT:'Разомкнут',norm:1,system:32},
		737:{title:'Датчик наличия питания',tmp:'discret3x',normT:'Есть',alerT:'Нет',norm:1,system:32},
		738:{title:'Датчик напряжения 220В',tmp:'discret3x',normT:'Есть',alerT:'Нет',norm:0,system:32},
		1473:{title:'Датчик уровня',tmp:'discret3x',normT:'Норма',alerT:'Тревога',norm:1,system:32},
		1665:{title:'Датчик протечки',tmp:'discret3x',normT:'Норма',alerT:'Тревога',norm:1,system:32},
		1537:{title:'Датчик газа',tmp:'discret3x',normT:'Норма',alerT:'Тревога',norm:0,system:32},
		1857:{title:'Датчик дыма',tmp:'discret3x',normT:'Норма',alerT:'Тревога',norm:0,system:32},
		1697:{title:'Датчик движения',tmp:'discret3x',normT:'Норма',alerT:'Тревога',norm:0,system:32},
		1441:{title:'Датчик двери',tmp:'discret3x',normT:'Норма',alerT:'Тревога',norm:0,system:32},
		1761:{title:'Манометр с контактами',tmp:'discret3x',normT:'Норма',alerT:'Тревога',norm:1,system:32},
		
		6946:{title:'Реле электромагнитное',tmp:'control_device3x',system:32,state:GL.equpStateControlRelay,btnName:'Включать'},
		6753:{title:'Реле электромагнитное',tmp:'control_device3x',system:32,state:GL.equpStateControlRelay,btnName:'Включать'},
		7041:{title:'Привод крана',tmp:'control_device3x',system:32,state:GL.equpStateControlCrane,btnName:'Открывать',legend:{on:'Открыт',off:'Закрыт'}},
		
		1409:{title:'Фотокамера',tmp:'foto_camera3x',system:32},
		
		//Программы для 3x
		32770:{title:'Отопление',type:'prog',tmp:'heating',system:32,state:{
			0:{name:'Ожидание',colorBlamba:G.color.grey2,colorText:G.color.black}, // Ожидание
			1:{name:'Нагрев',colorBlamba:G.color.blue,colorText:G.color.black},
			2:{name:'Не настроена',colorBlamba:G.color.grey2,colorText:G.color.black}, // недоступна
			3:{name:'Аварийный',colorBlamba:G.color.red,colorText:G.color.black},
			4:{name:'Ошибка',colorBlamba:G.color.red,colorText:G.color.red},
		}},
		
		32771:{title:'Реакция на датчики',type:'prog',tmp:'alarm',system:32,state:{
			0:{name:'Отключено',colorBlamba:G.color.grey,colorText:G.color.black},
			1:{name:'Включено',colorBlamba:G.color.blue,colorText:G.color.black},
			2:{name:'Нет связи',colorBlamba:G.color.grey,colorText:G.color.black},
			3:{name:'Не настроена',colorBlamba:G.color.grey,colorText:G.color.black},
		}},
		
		32772:{title:'Расписание',type:'prog',tmp:'sheduler',system:32,state:{
			0:{name:'Отключено',colorBlamba:G.color.grey,colorText:G.color.black},
			1:{name:'Включено',colorBlamba:G.color.blue,colorText:G.color.black},
			2:{name:'Нет связи',colorBlamba:G.color.grey,colorText:G.color.black},
			3:{name:'Не настроена',colorBlamba:G.color.grey,colorText:G.color.black},
		}},
		
		32768:{title:'Оповещение',type:'prog',tmp:'notofications',system:32,state:{
			0:{name:'Запрещены',colorBlamba:G.color.grey,colorText:G.color.black},
			1:{name:'Разрешены',colorBlamba:G.color.green,colorText:G.color.black},
		}},
		
		32769:{title:'Охрана',type:'prog',tmp:'security',system:32,state:{
			0:{name:'Снято',colorBlamba:G.color.grey,colorText:G.color.black},
			1:{name:'На охране',colorBlamba:G.color.green,colorText:G.color.black},
		}},
		
		28501:{title:'Экземпляр пускателя программы',type:'prog',tmp:'iotps',system:40,state:{
			0:{name:'Снято',colorBlamba:G.color.grey,colorText:G.color.black},
			1:{name:'На охране',colorBlamba:G.color.green,colorText:G.color.black},
		}},

	}
	
	// Типы Шаблнов
	G.temp = {
		1: {tmp:'system',title:'Система'},
		2: {tmp:'equps',title:'Датчик'},
		3: {tmp:'equps_temp',title:'Датчик температуры'},
		4: {tmp:'equps_vater',title:'Датчик влажности'},
		5: {tmp:'equps_akb',title:'Датчик АКБ'},
		6: {tmp:'equps_cur',title:'Датчик тока'},
		7: {tmp:'equps_disc',title:'Дискретный датчик'},
		8: {tmp:'uu',title:'Устройство управлния'},
		9: {tmp:'prog_notofications',title:'Программа оповещения'},
		10:{tmp:'prog_security',title:'Программа охраны'},
		11:{tmp:'prog_alarm',title:'Программа реакция на датчики'},
		12:{tmp:'prog_heating',title:'Программа отопления'},
		13:{tmp:'prog_sheduler',title:'Программа по расписанию'},
	}
	
	// Типы подключения устройства
	G.iface = {
		group:{0:'Встроенный ',1:'Проводной ',2:'Беспроводной '},
		port:{
			0:'ВСТРОЕННЫЙ',
			1:'ПИТАНИЕ',
			2:'АКБ',
			3:'Д',
			4:'Т',
			5:'ТОК',
			6:'',
			7:'OK',
			8:'OpenTherm',
			9:'РЕЛЕ',
			10:'РАДИО',
			11:'LORA',
			12:'ДОП',
			13:'Розетка ~220В'
		}
	}
	//Типы и виды проектов
	G.typeProject = {
		1:'img',
		2:'map',
		3:'grid'
	}
	
	// branch формирует различие ветки (класса) устройства.
	G.branch = {
		0:'common',
		1:'metric_sensor',
		2:'discrete_sensor',
		3:'relay',
		4:'faucet_gear',
		5:'photo_camera',
		6:'open_therm',
		7:'touch_memory',
		8:'console '
	}
	
	// Шаблоны програм ( выводят допустимый список для добавления )
	G.typeTempProg = {
		'security':{
			state:{
				0:{name:'Снят с охраны',color:'#d4d4d4'},
				1:{name:'На охране',color:'#1ab148'},
			}
		}, 
		'notofications':{
			state:{
				0:{name:'Отключено',color:'#d4d4d4'},
				1:{name:'Разрешено',color:'#ffae00'},
			}
		}, 
		'heating':{
			state:{
				0:{name:'Ожидание',color:'#d4d4d4'},
				1:{name:'Нагрев',color:'#58aeff'},
				2:{name:'Ошибка',color:'#ca3434'},
			}
		}, 
		'alarm':{
			state:{
				0:{name:'Тревога',color:'#ca3434'},
				1:{name:'Норма',color:'#1ab148'},
				2:{name:'Ошибка',color:'#ca3434'},
			}
		}, 
		'sheduler':{
			state:{
				0:{name:'ОТКЛ',color:'#d4d4d4'},
				1:{name:'ВКЛ',color:'#58aeff'},
				2:{name:'Ошибка',color:'#ca3434'},
			}
		},
	}
	
	G.reg = {}
	G.reg.name = "^[а-яА-Яa-zA-Z0-9-_()!№# ]+$" ;
	
	// цвета статусов программ
	G.state = {
		prog:{
			11:{colorBlamba:G.color.red,colorText:G.color.black},
			12:{colorBlamba:G.color.green,colorText:G.color.black},
			15:{colorBlamba:G.color.blue,colorText:G.color.black},
			16:{colorBlamba:G.color.grey,colorText:G.color.grey},
			17:{colorBlamba:G.color.red,colorText:G.color.black},
			
			111:{colorBlamba:G.color.grey,colorText:G.color.black},
			112:{colorBlamba:G.color.red,colorText:G.color.black},
			113:{colorBlamba:G.color.grey,colorText:G.color.black},
			114:{colorBlamba:G.color.green,colorText:G.color.black},
			115:{colorBlamba:G.color.grey,colorText:G.color.black},
			116:{colorBlamba:G.color.green,colorText:G.color.black},
			117:{colorBlamba:G.color.grey,colorText:G.color.black},
			118:{colorBlamba:G.color.blue,colorText:G.color.black},
			119:{colorBlamba:G.color.red,colorText:G.color.black},
			120:{colorBlamba:G.color.grey,colorText:G.color.grey},
			121:{colorBlamba:G.color.grey,colorText:G.color.black},
			122:{colorBlamba:G.color.blue,colorText:G.color.black},
			123:{colorBlamba:G.color.red,colorText:G.color.black},
			124:{colorBlamba:G.color.grey,colorText:G.color.grey},
			125:{colorBlamba:G.color.grey,colorText:G.color.black},
			126:{colorBlamba:G.color.blue,colorText:G.color.black},
			127:{colorBlamba:G.color.red,colorText:G.color.black},
			128:{colorBlamba:G.color.grey,colorText:G.color.grey},
		},
		equp:{}
	}
	
	G.timezone = {
		'-44':'(UTC-11) Апиа',
		'-40':'(UTC-10) Гонолулу 						',
		'-36':'(UTC-09) Джуно							',
		'-32':'(UTC-08) Ричмонд, Санта-Фе				',
		'-28':'(UTC-07) Солт-Лейк-Сити, Чихуахуа		',
		'-24':'(UTC-06) Мехико, Гватемала				',
		'-20':'(UTC-05) Вашингтон, Нью-Йорк			',
		'-16':'(UTC-04) Квебек, Сантьяго				',
		'-12':'(UTC-03) Бразилиа, Буэнос-Айрес		',
		'-8':'(UTC-02) Ресифи						',
		'-4':'(UTC-01) Канарские острова				',
		'0':'(UTC+00) Лондон, Лиссабон				',
		'4':'(UTC+01) Берлин, Рим					',
		'8':'(UTC+02) Калининград					',
		'12':'(UTC+03) Москва, Санкт-Петербург		',
		'16':'(UTC+04) Самара, Волгоград				',
		'20':'(UTC+05) Екатеринбург, Челябинск		',
		'24':'(UTC+06) Омск							',
		'28':'(UTC+07) Новосибирск, Красноярск		',
		'32':'(UTC+08) Иркутск, Улан-Удэ				',
		'36':'(UTC+09) Чита, Иркутск					',
		'40':'(UTC+10) Хабаровск, Владивосток		',
		'44':'(UTC+11) Веллингтон, Хониара			',
		'48':'(UTC+12) Петропавловск-Камчатский, Сува',
	}

	// Разные надписи и тексты
	G.globalText = {
		used_by:'Используется',
		no_connect:'Нет связи',
		yes_connect:'На связи',
		port_7_1:'СВЕТ',
		port_7_2:'ЗВУК',
	}
	
	G.regRegulations = '<div class="modal-regulations-text">\
		<p>1.1 В соответствии с настоящими Правилами Компания «Эктострой» (в дальнейшем – Исполнитель) предоставляет пользователю систем EctoControl (в дальнейшем – Заказчик) право регистрации и дальнейшего просмотра и управления информацией, содержащейся в Личном кабинете.</p>\
		<br>\
		<p>1.2 Регистрация в Личном кабинете осуществляется путем регистрации Заказчика на сайте : my.ectostroy.ru .</p>\
		<br>\
		<p>1.3 При регистрации указываются: - Логин - Пароль - Уникальный номер системы EctoControl (IMEI), указанный в гарантийном талоне и на обратной стороне центрального блока системы - Основной номер СИМ-карты, установленной в центральный блок</p>\
		<br>\
		<p>1.4 Осуществляя регистрацию в Личном кабинете, Заказчик автоматически дает свое согласие на передачу персональной информации в соответствии с Законодательством РФ об обработке персональных данных.</p>\
		<br>\
		<p>1.5 После осуществления регистрации и первой успешной аутентификации Заказчика или его Уполномоченного представителя в Личном кабинете дальнейшая работа в Личном кабинете осуществляется Заказчиком в соответствии с теми сервисами, которые предусмотрены Личным кабинетом.</p>\
		<br>\
		<p>1.6 Заказчику известно, что заключая Соглашение на условиях настоящих Правил, он выражает свое согласие с тем, что для использования отдельных сервисов Личного кабинета Заказчику будет необходимо предоставить дополнительную информацию или соблюсти дополнительные условия, которые прямо не указаны в настоящем Соглашении. О наличии каких-либо дополнительных условий и о необходимости предоставления дополнительной информации Заказчику будет предоставлена соответствующая информация в Личном кабинете перед началом осуществления операций.</p>\
		<br>\
		<p>1.7 За использование Личного кабинета плата Исполнителем с Заказчика не взимается.</p>\
		<br>\
		<p>Исполнитель имеет право:</p>\
		<p>2.1 В одностороннем порядке вносить изменения в настройки системы, структуру и оформление Личного кабинета.</p>\
		<br>\
		<p>2.2. В одностороннем порядке изменять условия настоящих Правил, размещая информацию об изменениях в Личном кабинете или на сайте Исполнителя.</p>\
		<br>\
		<p>2.3. В одностороннем порядке без применения каких-либо мер ответственности приостановить, ограничить или прекратить пользование Заказчиком Личным кабинетом или отдельными его сервисами в случае нарушения Заказчиком условий настоящих Правил, а также по техническим, организационным и/ или иным причинам.</p>\
		<br>\
		<p>2.5.1 Привлекать третьих лиц для организации работы Личного кабинета, которые в целях законодательства о защите персональных данных рассматриваются Заказчиком как штатные работники Исполнителя.</p>\
		<br>\
		<p>2.5.2 По требованию Заказчика временно ограничить или заблокировать доступ Заказчику к Личному кабинету.</p>\
		<br>\
		<p>2.6 Стороны признают согласием на внесение изменений Правил первый вход в Личный кабинет в рамках основной или дополнительной учетной записи после того, как правила были изменены.</p>\
		<br>\
		<p>Заказчик имеет право:</p>\
		<p>3.1 Осуществлять действия, связанные с просмотром и управлением информацией, содержащейся в Личном кабинете.</p>\
		<br>\
		<p>3.2 Направлять Исполнителю запросы, касающиеся работы Личного кабинета, условий его использования.</p>\
		<br>\
		<p>3.3. В любое время отказаться от использования Личного кабинета.</p>\
		<br>\
		<p>3.4 Передавать информацию о своей учетной записи (номере пользователя и пароле) третьему лицу, однако в этом случае Заказчик несет всю ответственность за действия этого лица от имени Заказчика и за все возникшие последствия такой передачи.</p>\
		<br>\
		<p>3.5 Заказчик обязуется осуществить своими силами и за свой счет все необходимые действия, связанные с обеспечением защиты от несанкционированного доступа к Личному кабинету и содержащейся в нем информации или использования своего программного оборудования, а также предпринимать все необходимые меры реагирования при обнаружении случаев такого доступа либо использования.</p>\
		<br>\
		<p>3.6 Заказчик обязан использовать и регулярно обновлять средства антивирусной защиты, использовать только лицензионное программное обеспечение, не проходить процедуру аутентификации на необеспеченных надлежащей защитой технических устройствах</p>\
		<br>\
		<p>3.7 Заказчик самостоятельно несет риск сохранности пароля и последствий, которые могут возникнуть при несанкционированном использовании пароля другими лицами для доступа в личный кабинет и содержащейся в нем информации.</p>\
		<br>\
		<p>3.8 Заказчик обязуется незамедлительно сообщать Исполнителю по телефону или посредством электронной почты о несанкционированном доступе к Личному кабинету со стороны третьих лиц.</p>\
		<br>\
		<p>3.9 Заказчик обязуется воздерживаться от действий, влияющих на нормальное функционирование Личного кабинета и его сервисов, а также нарушающих права и законные интересы иных лиц, в том числе посредством совершения действий, направленных на получение доступа к Личному кабинету иных лиц без их на то согласия.</p>\
		<br>\
		<p>4.1 Помимо случаев, прямо указанных в тексте настоящих Правил, Исполнитель также не несет ответственности за последствия несанкционированного доступа и использования Личного кабинета, в том числе содержащейся в нем информации, третьими лицами, если такие доступ или использование произошли не по вине Исполнителя. К таким обстоятельствам в любом случае относятся технические неполадки в сетях, серверах или программном обеспечении, которые находятся вне контроля Исполнителя.</p>\
		<br>\
		<p>4.2. Исполнитель не несет ответственности за негативные последствия, в том числе убытки, понесенные Заказчиком в случае перерыва, задержек, сбоев в работе программного обеспечения и/или оборудования, а также иных ошибок, влияющих на пользование Личным кабинетом, произошедших не по вине Исполнителя.</p>\
		<br>\
		<p>4.3. Исполнитель не несет ответственности за последствия, вызванные обстоятельствами непреодолимой силы, распространением вирусных программ, а также противоправными действиями третьих лиц.\
		</p>\
		<br>\
	</div>';
	
	G.helpText = {}
	 // текстовая информация ОБОРУДОВАНИЯ
	G.helpText.equp = {
		btnHelpActive:'Разрешить датчику формировать уведомления о тревоге и влиять на программы.',
		btnHelpJurnal:'Разрешить датчику запись данных в историю событий.',
		btnHelpVviget:'Добавить устройство на страницу виджеты или удалить с нее.',
		btnHelpDelayNorm:'Время нахождение датчика в состоянии нормы по окончанию тревоги, по истечению которого будет выполнено оповещение о возврате показаний в норму.',
		btnHelpDelayAlert:'Время нахождение датчика в состоянии тревоги, по истечению которого будет выполнено оповещение или сработает программа.',
		btnHelpNameNorm:'Отображаемое состояние датчика, если тревоги нет.',
		btnHelpNameAlert:'Отображаемое состояние датчика, находящегося в тревоге.',
		btnHelpGraphTime:'Выбор периода отображения графика. Вне зависимости от реальной частоты измерений точки на графике отображаются либо при смене значения, либо каждые 10 минут, если значение не изменилось. Если шаг между точками составляет более 20 минут, скорее всего, у устройства или ее системы были сбои со связью.',
		btnHelpGraphType:'СSV–простой текстовый формат, совместимый с MS Excel <br> PDF–документ для просмотра и печати.',
		btnHelpPorogDown:'Если показания опустятся ниже этого порога, датчик перейдет в тревогу по нижнему пределу.',
		btnHelpPorogUp:'Если показания превысят этот порог, датчик перейдет в тревогу по верхнему пределу.',
		btnHelpUUActive:'Разрешить датчику запись данных в историю событий.',
		btnHelpUUHold:'По команде «включить» устройство включится и останется в этом состояни.',
		btnHelpUUTime:'По команде «включить» устройство включится на указанное время и по его истечении отключится.',
		btnHelpUUHoldKrane:'По команде «открыть» кран откроется и останется в этом состоянии.',
		btnHelpUUTimeKrane:'По команде «открыть» кран откроется на указанное время и по его истечению закроется.',
		btnHelpboilerModel:'Для корректной расшифровки ошибок.',
		btnHelpboilerKontur1:'Разрешить работу основного контура отоплени.',
		btnHelpboilerKontur2:'Разрешить работу дополнительного контура отопления (для некоторых моделей).',
		btnHelpboilerPressureMin:'Нижний порог давления в контуре отопления.',
		btnHelpboilerPressureMax:'Верхний порог давления в контуре отопления.',
		btnHelpboilerModulGorelMax:'Максимально-возможная модуляция горелки котла, %.',
		btnHelpboilerPZA:'Кривая регулирования в режиме погодозависимой автоматики (при добавлении в программу отопления датчика уличной температуры).',
		btnHelpboilerPZApridel:' Котел будет стараться поддерживать температуру ГВС в этих пределах. Если заданная числовая уставка температуры в разделе "ГВС" работает некорректно, измените данные пределы до достижения оптимальной температуры ГВС на выходе котла. Не все котлы поддерживают эту настройку.',
		btnHelpboilerModulGorel:'Котел будет стараться поддерживать модуляцию горелки не выше заданной. Не все котлы поддерживают эту настройку.',
		btnHelpboilerGVS:'Разрешить нагрев контура горячей воды.',
		btnHelpboilerTemp:'Эту температуру будет поддерживать котел в контуре ГВС, если котел поддерживает ГВС и его работа разрешена. Котел самостоятельно регулирует эту температуру, поэтому текущая температура ГВС может несколько отличаться от уставки.',
		btnHelpboilerActive:'Разрешить запись в историю событий всех параметров котла.',
		btnHelpboilerTempCrash:'Требуемая температура контура, если неисправен или отсутствует термодатчик в программе отопления.',
		btnHelpboilerHeatСarrierMin:'Нижний порог температуры теплоносителя.',
		btnHelpboilerHeatСarrierMax:'Верхний порог температуры теплоносителя.',
		btnHelpboilerHeatСarrierMinKotel:'Минимальная температура ГВС, которую сможет обеспечить котел.',
		btnHelpboilerHeatСarrierMaxKotel:'Максимальная температура ГВС, которую сможет обеспечить котел.',
		btnHelpTmKey:'Мастер-ключ записывается в ячейку с номером 0 и позволяет не только активировать и деактивировать режим охраны, но и автономно записать другие ключи. Пользовательские ключи только управляют режимом охраны.',
		btnHelpRadioBrelocAction:'Отключение того пункта приведет к игнорированию системой сигнала с радиобрелока.',
		btnHelpRadioBrelocJurnal:'Отключение этого пункта позволит не вести журнал для данного радиобрелока.',
		btnHelpRadioBrelocControl:'При каждом нажатии дополнительной кнопки брелока выбранное устройство будет включаться и отключаться.',
		systemConnection:'Состояние связи системы с ЛК и приложением.',
		systemRadioBtn:'Если неактивно, радиоустройства будут недоступны, но время автономной работы системы повысится.',
		systemDOPBtn:'Если неактивно, устройства порта ДОП будут недоступны, но время автономной работы системы повысится.',
		systemMicrophoneBtn:'Если активно, при голосовом звонке на систему можно прослушать помещение (требуется подключенный микрофон).',
		systemAlertBtn:'Если активно, порт ТОК работает с тревожной кнопкой, иначе – с датчиком тока 4..20мА.',
		systemWiFi:'Имя беспроводной сети и пароль к ней (минимум 8 символов).',
		systemTelHelp:'Абонентский номер SIM-карты в системе. Вводится информативно, на работу системы никак не влияет.',
		systemBalanceHelp:'Символы USSD - запроса оператору связи для получения сообщения о состоянии лицевого счета абонента.',
		systemGsmIMEI:'Уникальный серийный номер модема. Идентифицирует систему в сети сотового оператора.',
		systemAccess:'Возможные каналы передачи данных в интернет и приоритеты их использования. Основной канал передает данные, резервный - только периодически проверяет связь, расходуя минимальный трафик. Если по основному каналу наступает сбой, данные начинают передаваться по резервному каналу, а основной периодически проверяется на возможность полноценного его использования после сбоя.',
		systemDataAndTime:'Дата и время в системе. Требуются для журнала событий.',
		systemBuiltInPO:'Версия встроенной в систему программы. Перед обновлением внимательно ознакомьтесь с описанием нововведений.',
		systemPassword:'4 символа пароля доступа к системе. Используется в SMS - командах и в голосовом меню.',
		systemReboutPass:'Настройки системы будут сброшены на заводские установки. Все подключенные устройства будут удалены, доступ в ЛК отключен. Вернуть систему в ЛК можно либо SMS-командой или локально через браузер смартфона.',
		systemReboutStstem:'Перезагрузка системы. Питание датчиков и реле будет временно отключено.',
		systemAddNewEqup:'Нажать для установки нового оборудования.',
		systemDelete:'Удаление системы, всех ее устройств со всей истории событий и графиками из данного аккаунта.',
		systemTimezone:'Для правильного отображения истории событий выберите тот часовой пояс, в котором установлена и работает эта система. Часовой пояс не будет изменен при автосинхронизации времени.',
		systemTimezoneAuto:'Если активен хотя бы один режим, система периодически будет подводить свои часы разрешенными ей способами.',
		systemTimezoneNTP:'Если активно, система будет синхронизировать время через интернет по протоколу NTP через Wi-Fi или GSM (у Wi-Fi высший приоритет).',
		systemTimezoneGSM:'Если активно, система будет синхронизировать время через сеть оператора сотовой связи, даже если услуга интернет (GPRS) недоступна. Будьте бдительны, если система располагается на географической границе часовых поясов страны или на границе со страной иного часового пояса.',
		photoCameraJurnal:'Включить или отключить запись событий в журнал.',
		photoCameraShotAlarm:'Включить или отключить режим автоматического получения снимка при перехода в тревогу датчиков программы "Охрана".',
		photoCameraShotReturn:'Активация переворота кадра на 180 градусов при выполнении снимка.',
		photoCameraShotProgram:'Активация режима автоматического получения снимка при включения устройства в заданной программе.',
		photoCameraSelectProgram:'Выбрать программу для отслеживания включения устройства, указанного в ней, для автоматического получения фотоснимка.',
		photoCameraShotEfect:'Режим изменения цветов, подходит для съемок в условиях нестандартного освещения.',
		photoCameraShotSize:'Размер снимка в пикселях, чем больше размер, тем более детальным будет снимок и тем дольше он будет загружаться.',
		photoCameraShotLight:'Режим освещенности. Влияет на цветовой оттенок снимка, подбирается под источник света для камеры.',
		photoCameraLight:'Режим работы подсветки при выполнении снимка. Влияет на яркость полученной картинки в условиях недостаточной внешней освещенности.',
		photoCameraBright:'Общая яркость снимка (выдержка фотокамеры).',
		photoCameraContrast:'Общая контрастность снимка.',
		photoCameraSaturation:'Насыщенность цветов снимка. Применяется при съемке множества объектов с неяркими цветами.',
		battareyHelper:'Активация отправки пользователям сообщений о низком разряде батареи беспроводного датчика. ',
		btnHelpAnalogPravka:'Это значение будет прибавлено к значению, измеренному датчиком. Как правило, используется для компенсации погрешности датчика.',
		
	}
	// текстовая информация СТРАНИЦ
	G.helpText.pages = { 
		equipmentsFilteEqup:'Выбор типа оборудования: датчики с устройствами управления или системы.',
		equipmentsFilteConnect:'Выбор типа подключения устройства к системе.',
		equipmentsFilteLink:'Наличие связи устройств с системой или связи системой с ЛК.',
		equipmentsFilteSustem:'Отображать оборудование,принадлежащее только одной системе или всем системам.',
		progectSelect:'Выбор типа проекта.',
		programsSelectProg:'Выбор типа программы.',
		programsSelectSystem:'Выбор системы, программы которой будут отображены.',
		communicationTime:'Период отображения графиков связи систем с ЛК.',
		communicationName:'Имя системы.',
		communicationID:'Серийный номер системы.',
		communicationConnection:'Состояние связи системы с ЛК.',
		communicationMesConnect:'Признаки уведомления пользователя об изменении состояния связи систем с ЛК.',
		communicationGraph:'График связи систем с ЛК за выбранный период.',
		userSets:'Данные пользователя.',
		userId:'Уникальный номер пользователя. Укажите его при обращениях в службу технической поддержки.',
		userMain:'Вид главной страницы по умолчанию.',
		messageSmsAndCool:'Настройки голосовых вызовов пользователям и отправки им SMS.',
		messageUserSmsAndCool:'Телефонные номера пользователей, которым будут совершаться голосовые вызовы и отправляться SMS.',
		messageMesageEmail:'Выбор событий, сообщения о которых будут отправляться на ваш e-mail.',
		shareStatusIconUp:'Вы поделились с кем-то этим оборудованием.',
		shareStatusIconDown:'Кто-то поделился с вами этим оборудованием.',
	}
	// текстовая информация ПРОГРАМ
	G.helpText.prog = { 
		manualMode:'В ручном режиме устройством управления можно управлять самостоятельно, программа не будет менять его состояние.',
		manualEgups:'Список датчиков. Если хотя бы один перейдет в состояние тревоги, устройство управления будет активировано в соответствии с настройками.',
		btnHelpVviget:'Добавить программу на страницу виджеты или удалить с нее.',
		typeWork:'Как активировать устройство управления после тревоги хотя бы одного датчика из списка.',
		timeWork:'На какое время активировать устройство управления после тревоги хотя бы одного датчика из списка.',
		shedulerManualMode:'В ручном режиме устройством управления можно управлять самостоятельно, программа не будет менять его состояние.',
		shedulerControl:'Устройство управления, которое будет включаться и отключаться в зависимости от текущего времени и настроек расписания.',
		heatingManualMode:'Если котлом управляет реле, в ручном режиме программа не будет управлять этим реле и реле насоса, это можно будет сделать вручную. Если котлом управляет адаптер Opentherm/eBus, им будет поддерживаться последняя выставленная температура, а насос так же будет управляться вручную.',
		heatingSupurtedTemp:'Температура поддержания.',
		heatingTakeTempTupe:'Выбор режима: нагрев до определенной температуры или по расписанию.',
		heatingTakeTemp:'Текущая температура.',
		heatingTakeTempState:'Текущее состояние нагревателя.',
		heatingPumpOnlyOn:'Выбор режима управления дополнительным циркуляционным насосом: включен постоянно или включен на время включения отопителя и дополнительно на указанное время после отключения отопителя.',
		heatingInverRelay:'Если активно, то нагреватель будет выключаться путем включения реле и наоборот. Это может быть необходимо для того, чтобы при выключенной системе и обесточенном реле нагреватель был включен во избежание заморозки помещения.',
		heatingGlisterezis:'Максимальное отклонение «вверх» и «вниз» текущей температуры от поддерживаемой, необходим для исключения частых включений и выключений нагревателя.',
		heatingPumpRun:'Циркуляционный насос будет работать в течение указанного времени после отключения отопителя.',
		securitySound:'Устройство будет включаться в выбранном режиме при срабатывании охраны на заданное время, короткими включениями может подтверждать постановку на охрану и снятие с нее. Как правило, это устройство должно управлять сиреной.',
		securityLight:'Устройство своим включением и отключением будет отображать состояние охраны, так же может подтверждать постановку на охрану и снятие с нее. Как правило, это устройство должно управлять световой сигнализацией.',
		securityRelay:'Устройство будет включаться при постановке на охрану и выключаться при снятии с охраны.',
		securityEqupment:'Если в состоянии \'на охране\' хотя бы один их этих датчиков перейдет в состояние тревоги, устройства \'Свет\' и \'Звук\' будут активированы, а также будут отправлены требуемые оповещения. В состоянии \'снят с охраны\' уведомления от этих датчиков поступать не будут, устройства \'Свет\' и \'Звук\' не будут активированы.',
		alarmDelayBtn:'Это значение времени применяется в двух ситуациях. <br> 1. После включения режима охраны фактически объект станет на охрану только по прошествии этого времени. <br> 2. При возникновении тревоги любого датчика при активном режиме охраны тревожные оповещения начнут производиться только по истечении такого же времени.',
	}
	
	// текстовая информация ПРОГРАМ
	G.helpText.vijets = {
		alarmManualMod:'Программа не управляет устройством. В ручном режиме это можно сделать вручную из виджета устройства или со страницы его настроек.',
		shedulerManualMod:'Программа не управляет устройством. В ручном режиме это можно сделать вручную из виджета устройства или со страницы его настроек.',
		heatingManualMod:'Программа не управляет отопителем и насосом. В ручном режиме это можно сделать вручную из виджетов этих устройств или со страниц их настроек.',
		system3xReboutSms:'Нажмите, чтобы обновить сообщение о состоянии баланса SIM-карты.',
	}
	//Тестовые данные для работы без сервера
};
