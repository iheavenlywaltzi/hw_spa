"use strict";
function hw_html(){
	let G = this;
	// список оборудования
	// type - переменная отвечающая за вид возврата таблици
	//		norm - ( нормальный ), 
	//		nohead - (без шапки агловления),
	//		head - (только шапка оглавления),
	//		list - (только список из (li))
	this.equipments = function(info,dop){
		console.log(info);
		let vspec = dop || {}
		let view = vspec.view || 'list';
		let S = {};
			S.ContA = vspec.a || 'of';
			S.type = vspec.type || '';
			S.click = vspec.click || 'btn-one-equip';
			S.check = vspec.check || [];
			S.sorIcon = vspec.sorIcon || 'on';
			S.sorIconPrint = '';
			S.list = '';
			S.print = '';
			
		if (view == 'list'){
			if(S.sorIcon == 'on'){S.sorIconPrint = HWfilter.btnSortAZ('equipments','.all-equipments','fcnSornName','fcnSornName');}
			
			S.ulOpen = '<ul class="list-equipments '+S.type+'">';
			S.ulClouse = '</ul>';
			S.head = '\
				<li class="one-head sticky">\
					<div class="equ_n"><span class="block_mr_30">Тип</span><span>Имя</span>'+S.sorIconPrint+'</div>\
					<div class="equ_d">Данные</div>\
					<div class="equ_p">Подключение</div>\
					<div class="equ_p">Порт : адрес</div>\
					<div class="equ_i">id : канал </div>\
					<div class="equ_b equp_select_all user-none">Всё</div>\
				</li>';
			for (let key in info){ key = parseInt(key);
				let elem = info[key]; elem.info = elem.info || {}; elem.info.iface = elem.info.iface || {}
				let nextElem = info[key+1] || false;
				let et = (elem.cloud_type)? HWD.equp[elem.cloud_type] || false : false;
				let name = elem.config.name || '';
				let value = '',valueVal = '', valueColor = '',ident = '',branch = '',unit='',state='',dopStep = '';
				let connect = HWD.iface.group[elem.info.iface.group] || 0;
				let port = '';
				let type = elem.type, verSystem = ''; 
				let system = et.system || 40;
				let normInfo = HWequip.normInfo(elem);

				let check = '';
				if(!elem.lk) {elem.lk = {}};
				let stateType = elem.lk.state || '';
				let share_status = elem.lk.share_status || '';
				let stateColor = HWF.getStateColor(stateType) || '';
				if(stateColor == HWD.color.grey) {stateColor = HWD.color.grey2;}
				value = elem.lk.state_name || '';
				
				// Глобальные
				if (S.ContA == 'on'){S.a = '<a href="#'+HWD.page.equp+'/'+elem.id+'" class="click"></a>';}
				else {S.a = '<div class="click '+S.click+'"></div>';}
				
				if (share_status &&  share_status  == 1){share_status = '';}
				if (share_status &&  share_status  == 2){share_status = '<span class="icon icon-share-down btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconDown+'"></span>';}
				if (share_status &&  share_status  == 3){share_status = '<span class="icon icon-share-up btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconUp+'"></span>';}
				
				if(S.check && S.check.length > 0){HWF.recount(S.check,function(idChek){ if(idChek == elem.id){check = 'active';} }); }
				
				if(HWS.buffer.btn_dop_sort_system){
					if(normInfo.tmp == 'system3x' || normInfo.tmp == 'system4x' || normInfo.tmp == 'systemEmpty'){dopStep = '';}
					else { 
						dopStep = '<div class="block_w_50p"> <span class="icon icon-t_line spec-icon-list-left"></span> </div>';
						if (nextElem) {
							let nextElemNorm = HWequip.normInfo(nextElem);
							if (nextElemNorm.tmp == 'system3x' || nextElemNorm.tmp == 'system4x' || nextElemNorm.tmp == 'systemEmpty'){
								dopStep = '<div class="block_w_50p"> <span class="icon icon-l_line spec-icon-list-left"></span> </div>';}
						} else {dopStep = '<div class="block_w_50p"> <span class="icon icon-l_line spec-icon-list-left"></span> </div>';}
						
					}
				}
				
				type = normInfo.type;
				unit = '';
				valueVal = normInfo.val || '';
				branch = normInfo.branch;
				port = normInfo.portSpec;
				//Для систем 4x	
				if(system == 40){
					if(normInfo.tmp == 'system4x'){ // если это система
						if(elem.system_connection_status){value = HWD.globalText.yes_connect;  }
						else {value =  HWD.globalText.no_connect;}
						ident = normInfo.ident;
						connect = elem.info.info_vermain;
					}
				}
				
				//Для систем 3х
				if(system == 30 || system == 31 || system == 32 || system == 33){
					if(normInfo.tmp == 'key_tm3x'){
						port = normInfo.channel_index;
						ident = normInfo.ident;
					}
					
					if(normInfo.tmp == 'system3x'){ // если это система
						ident = elem.info.ident;
						connect = elem.info.info_vermain;
					}
					
					if(normInfo.tmp == 'systemEmpty'){ // если это пустая система
						ident = elem.info.ident;
						connect = elem.info.info_vermain;
					}
					
					if(normInfo.port == 10 || normInfo.port == 12){
						let chanel = '';
						if(normInfo.channel_index){chanel = ':'+normInfo.channel_index}
						ident = normInfo.ident+chanel;
						port = normInfo.portName+' : '+(normInfo.portSymbol+normInfo.addr);
					}
				}

				S.data = {}
				S.data.id = (elem.id)?'data-id="'+elem.id+'"':'';
				S.data.localid = (elem.id)?'data-localid="'+elem.local_id+'"':'';
				S.data.typetype = (type)?'data-typetype="'+(type || '')+'"':'';
				S.data.type = (elem.type)?'data-type="'+(elem.type || 0)+'"':'';
				S.data.state = (state)?'data-state="'+state+'"':'';
				S.data.valueVal = (valueVal)?'data-val="'+valueVal+'"':'';
				S.data.system = (system)?'data-version="'+system+'"':'';
				S.data.branch = (branch)?'data-branch="'+branch+'"':'';
				S.data.idSystem = (elem.id_system)?'data-idsystem="'+elem.id_system+'"':'';
				S.data.group = ''; 
				if (elem.info && elem.info.iface){
					if (elem.info.iface.group) {S.data.group = 'data-group="'+elem.info.iface.group+'"'; }
					else {S.data.group = 'data-group="0"'; }
				}
				
				S.list += '<li class="one-equipment" '+
					S.data.id+' '+
					S.data.type+' '+
					S.data.typetype+' '+
					S.data.state+' '+
					S.data.valueVal+' '+
					S.data.system+' '+
					S.data.localid+' '+
					S.data.idSystem+' '+
					S.data.group+' '+
					S.data.branch+'" >\
					<div class="equ_n">'+dopStep+'\
						<span class="icon icon_equip_'+type+'"></span>\
						'+share_status+'\
						<span class="text_name vertical-middle">'+name+'</span>\
					</div>\
					<div class="equ_d" style="color:'+stateColor+'">\
						<span class="tw_700 data">'+value+' </span>'+
						unit+'\
					</div>\
					<div class="equ_p">'+connect+'</div>\
					<div class="equ_p">'+port+'</div>\
					<div class="equ_i">'+ident+'</div>\
					<div class="equ_b">\
						<div class="settings-butt">\
							<div class="btn-active checkbox one_equipment_check '+check+'"></div>\
						</div>\
					</div>\
					'+S.a+'\
				</li>';
			}
			if(info.length == 0){S.list += '<li class="one-equipment"><p></p><p>Данное оборудование в системе отсутствует</p><p></p><li>';}
			S.print = S.ulOpen + S.head + S.list + S.ulClouse
		}

		return S.print;
	}
	this.listVijet = function(info,dop){dop = dop || {}; dop.sorIcon = 'of'; let s = G.equipments(info,dop); return s;};
	this.listPumpDO = function(info,dop){dop = dop || {}; dop.sorIcon = 'of'; let s = G.equipments(info,dop); return s;};
	this.listAutoDO = function(info,dop){dop = dop || {}; dop.sorIcon = 'of'; let s = G.equipments(info,dop); return s;};
	this.listSensorDO = function(info,dop){dop = dop || {}; dop.sorIcon = 'of'; let s = G.equipments(info,dop); return s;};
	this.system = function(info,dop){dop = dop || {}; dop.sorIcon = 'of'; let s = G.equipments(info,dop); return s;};
	this.systemVijet = function(info,dop){dop = dop || {}; dop.sorIcon = 'of'; let s = G.equipments(info,dop); return s;};
	this.listProjectsEqup = function(info,dop){dop = dop || {}; dop.sorIcon = 'of'; let s = G.equipments(info,dop); return s;};
	
	// создание списка проектов
	this.projects = function(info,dop){
		let vspec = dop || {}
		let type = vspec.type || 'norm';
		let view = vspec.view || 'list';
		let S = {}; S.list = ''; S.print = '';
		if(info && info.length > 0){
			if (view == 'list'){
				S.check = '';
				S.checkH = '';
				S.ulOpen = '<ul class="list-projects">';
				S.ulClouse = '</ul>';
				if(type == 'checkbox'){S.checkH = '<div class="equ_check proj_select_all user-none btn_text">Всё</div>';}
				S.head = '\
				<li class="one-head sticky">\
					<div class="equ_n"><span class="block_mr_10 block_w_30p">Тип</span><span>Имя</span>'+HWfilter.btnSortAZ('projects','.all-projects','fcnSornName')+'</div>'
					+S.checkH+'\
					<div class="equ_b">Доп</div>\
				</li>';
				for (let key in info){
					let elem = info[key];
					let imgSrc = elem.src;
					if (elem.project_type == '1'){elem.type = '1';}
					if (elem.project_type == '2'){elem.type = '2';}
					if (elem.project_type == '3'){elem.type = '3';}
					if(type == 'checkbox'){S.check = '<div class="equ_check"><div class="btn-active checkbox"></div></div>'}
					
					let share_status = (elem.lk)?elem.lk.share_status || '':'';
					if (share_status &&  share_status  == 1){share_status = '';}
					if (share_status &&  share_status  == 2){share_status = '<span class="icon icon-share-down btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconDown+'"></span>';}
					if (share_status &&  share_status  == 3){share_status = '<span class="icon icon-share-up btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconUp+'"></span>';}
					
					S.list += '\
					<li class="one-project stat-'+elem.state+'" data-id="'+elem.id+'" data-state="'+elem.state+'" data-type="'+elem.type+'">\
						<div class="equ_n">\
							<span class="icon icon_projects_'+elem.type+' block_mr_10"></span>\
							'+share_status+'\
							<span class="text_name vertical-middle">'+elem.name+'</span>\
						</div>\
						<div class="pro_i"></div>'+
						S.check+'\
						<div class="equ_b">\
							<div class="settings-butt">\
								<span class="icon icon-settings hover btn-proj-change"></span>\
								<span class="icon icon-delete hover btn-proj-delete"></span>\
							</div>\
						</div>\
						<div class="click btn-one-proj"></div>\
					</li>';
				}
					
				if(type == 'norm'){S.print = S.ulOpen + S.head + S.list + S.ulClouse}
				if(type == 'checkbox'){S.print = S.ulOpen + S.head + S.list + S.ulClouse}
			}
			if (view == 'tile'){
				for (let key in info){
					let elem = info[key];
					let imgSrc = elem.src;
					if (!elem.type){elem.type = 'img'}
					if (elem.type == 'map'){imgSrc = 'img/fon-map.png';}
					if (elem.type == 'grid'){imgSrc = 'img/mmBack.jpg';}
					S.list += '<div class="one-tile one-project status-'+elem.state+'" data-id="'+elem.id+'" >\
						<header class="flex_space">\
							<span class="icon icon_projects_'+elem.type+'"></span>\
							<h3>'+elem.name+'</h3>\
							<div class="control-btn">\
								<div class="text_btn btn-proj-change">✎</div>\
								<div class="text_btn btn-proj-delete">✕</div>\
							</div>\
						</header>\
						<section><div class="tile-img"><img src="'+imgSrc+'" /></div></section>\
						<footer></footer>\
						<div class="click btn-one-proj"></div>\
					</div>';
				}
				if(type == 'norm'){S.print = S.list}
			}
		}else {
			S.print = '<p class="block_pt_20"><a href="#'+HWD.page.projAdd+'" class="style_btn">Создать проект ?</a></p>'
		}
		return S.print;
	}
	this.listProjectsProj = function(info,dop){dop = dop || {}; let s = G.projects(info,dop); return s;};
	
	// создание списка програм
	this.programs = function(info,dop){ console.log(info);
		let vspec = dop || {}
		let type = vspec.type || 'norm';
		let typeW = vspec.typeW || 'norm';
		let view = vspec.view || 'list';
		let S = {};
			S.ContA = dop.a || 'of';
			S.a = '<div class="click btn-one-prog"></div>';
			S.list = '';
			S.print = '';
			S.state = ''

		if (view == 'list'){
			S.topCheck = '<div class="equ_b"></div>';
			S.ulOpen = '<ul class="list-programs list">';
			S.ulClouse = '</ul>';
			if(typeW == 'checkbox'){S.topCheck = '<span class="equ_b prog_select_all user-none btn_text ta_center">Все</span>';}
			S.head = '\
			<li class="one-head sticky">\
				<div class="equ_n"><span class="equ_t">Тип</span><span class="name">Имя</span>'+HWfilter.btnSortAZ('programs','#all-programs','fcnSornName')+'</div>\
				<div class="equ_s">Состояние</div>\
				<div class="equ_s">Имя система</div>\
				<div class="equ_s">Cерийный номер</div>'+
				S.topCheck+'\
			</li>';
			for (let key in info){
				let elem = info[key];
				let stateName = elem.stateName;
				let stateType = elem.state;
				let stateColor = HWF.getStateColor(stateType) || '';
				if(stateColor == HWD.color.grey) {stateColor = HWD.color.grey2;}
				let ver = '';
				if (HWD.equp[elem.type]){ver = HWD.equp[elem.type].system;}
				
				if (S.ContA == 'on'){S.a = '<a href="#'+HWD.page.prog+'/'+elem.id+'" class="click btn-one-prog"></a>';}
				if(typeW == 'modal') {S.listDopBtn = '';}
				else if (typeW == 'checkbox'){
					S.listDopBtn = ''+
					'<div class="settings-butt">'+
						'<div class="btn-active checkbox one_prog_check"></div>'+
					'</div>';
				}
				else {
					S.listDopBtn = ''+
					'<div class="settings-butt">'+
						'<span class="icon icon-clouse hover btn_delete_programs"></span>'+
					'</div>';
				}
				
				let share_status = (elem.lk)?elem.lk.share_status || '':'';
				if (share_status &&  share_status  == 1){share_status = '';}
				if (share_status &&  share_status  == 2){share_status = '<span class="icon icon-share-down btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconDown+'"></span>';}
				if (share_status &&  share_status  == 3){share_status = '<span class="icon icon-share-up btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconUp+'"></span>';}
				
				S.list += ''+
				'<li class="one-program" \
					data-id="'+elem.id+'" \
					data-type="'+elem.type+'" \
					data-ver="'+ver+'">'+
					'<div class="equ_n">'+
						'<span class="equ_t"><span class="icon icon_equip_'+elem.type+'"></span></span>'+
						share_status+
						'<span class="name">'+elem.name+'</span>'+
					'</div>'+
					'<div class="equ_s tw_700" style="color:'+stateColor+'">'+stateName+'</div>'+
					'<div class="equ_s">'+elem.namesis+'</div>'+
					'<div class="equ_s">'+elem.idsis+'</div>'+
					'<div class="equ_b">'+S.listDopBtn+'</div>'+
					S.a+
				'</li>';
			}
			if(type == 'norm'){S.print = S.ulOpen + S.head + S.list + S.ulClouse}
		}
		
		return S.print;
	}
	//список Шаблонных программы
	this.templatePrograms = function(list,version){
		version = version || 32;
		let S = {};
		S.print = '<ul class="list block_w_100">';
		S.print += '<li class="one-tmp-program" data-type="thermostat" data-version="'+version+'">'+
			'<div>'+
				'<div class="icon icon_equip_32770 block_mr_10"></div>'+
				'<span class="vertical-middle">Отопление</span>'+
			'</div>'+
			'<div class="click clic_tmp_program"></div>'+
		'</li>';
		S.print += '<li class="one-tmp-program" data-type="reaction_on_sensors" data-version="'+version+'">'+
			'<div>'+
				'<div class="icon icon_equip_32771 block_mr_10"></div>'+
				'<span class="vertical-middle">Реакция на датчики</span>'+
			'</div>'+
			'<div class="click clic_tmp_program"></div>'+
		'</li>';
		S.print += '<li class="one-tmp-program" data-type="shcedule" data-version="'+version+'">'+
			'<div>'+
				'<div class="icon icon_equip_32772 block_mr_10"></div>'+
				'<span class="vertical-middle">Расписание</span>'+
			'</div>'+
			'<div class="click clic_tmp_program"></div>'+
		'</li>';
		S.print += '</ul>';
		S.print += '<div class="block_p_8"></div>';
		
		return S.print;
	}
	// создание списка пользывателей
	this.users = function(info,dop){
		let vspec = dop || {}
		let type = vspec.type || 'norm';
		let view = vspec.view || 'list';
		let group = vspec.group || {"1":"No Groups","2":"Yes Yes NO!"};
		let S = {};
			S.list = '';
			S.print = '';

		if (view == 'list'){
			S.ulOpen = '<ul class="list">';
			S.ulClouse = '</ul>';
			S.head = '\
				<li class="one-head sticky">\
					<div class="fio">Ф.И.О</div>\
					<div class="access flex_center">Доступ <a href="#access" class="text-icon btn_change_access">✎</a></div>\
					<div class="email">Email</div>\
					<div class="date">Заходил</div>\
					<div class="mesage"><div title="Количество систем" class="btn_helper icon icon-equip-system help"></div></div>\
					<div class="btn"></div>\
				</li>';
			for (let key in info){
				let elem = info[key];
				let obj = group;
					obj[elem.object.value] = {"name":elem.object.name,"selected":'selected'}
				let	pObj = G.select(obj,'user-access-object');
				let sustem = 0;
				if(elem.system){sustem = elem.system.length;}
				S.list += '\
				<li class="one-user" data-id="'+elem.id+'" >\
					<div class="fio"><span>'+elem.last_name+' '+elem.name.charAt(0)+'.'+elem.middle_name.charAt(0)+'</span><div class="click btn_one_user"></div>\</div>\
					<div class="access">'+pObj+'</div>\
					<div class="email">'+elem.email+'<div class="click btn_one_user"></div></div>\
					<div class="date">'+elem.last_date+'<div class="click btn_one_user"></div></div>\
					<div class="mesage">'+sustem+'</div>\
					<div class="btn">\
						<div class="settings-butt">\
							<a href="#user/'+elem.id+'" class="text-icon btn_one_user">✎</a>\
							<div class="text-icon btn_delete_user">✕</div>\
						</div>\
					</div>\
				</li>';
			}
			if(type == 'norm'){S.print = S.ulOpen + S.head + S.list + S.ulClouse}
		}
		return S.print;
	}
	
	// создание списка Групп
	this.groups = function(info,dop){
		let print = '',templat='';
		dop = dop || {}
		
		templat += '<li class="one-elem" data-id="{{id}}">';
		templat += '<div class="name">{{name}}</div>';
		templat += '<div class="security">{{security}}</div>';
		templat += '<div class="equps">{{id}}</div>';
		templat += '<div class="one_group one_elem click"></div>';
		templat += '</li>';
		
		if (dop.view == 'dopBtn'){
		templat = '';
		templat += '<li class="one-elem modal" data-id="{{id}}" data-load="#group_load_{{id}}">';
		templat += '<div class="block_w_100 flex_between main-info">';
		templat += '<div class="name">{{name}}</div>';
		templat += '<div class="security">{{security}}</div>';
		//templat += '<div class="equps">{{id}}</div>';
		templat += '<div class="equps elem-chec flex_around">\
				<div class="button_text_2 button-up-do btn-active open_dop_info_group_equp" data-targetplusg="#group_load_{{id}}">Оборудование</div>\
			</div>';
		templat += '<div class="one_group one_elem click"></div>';
		templat += '</div>';
		templat += '<div class="block_w_100 window-up-do" id="group_load_{{id}}">';
		templat += '</div>';
		templat += '</li>';
		}

		print += '<ul class="list list-groups"><li class="one-head">';
		print += '<div class="name">Имя</div>';
		print += '<div class="security">Доступ</div>';
		print += '<div class="equps">Оборудование</div>';
		print += '</li>';
		print += G.list(info,templat);
		print += '</ul>';
		
		return print;
	}
	// создание списка пользывателей текущей группы
	this.usersGroups = function(info,dop){
		let print = '';

		print += '<ul class="list user-group-list"><li class="one-head">';
		print += '<div class="mail">Емайл</div>';
		print += '<div class="name">Имя</div>';
		print += '<div class="but">Управление</div>';
		print += '</li>';
		
		for (let key in info){
			print += '<li class="one-user" data-id="'+info[key].id+'">';
			print += '<div class="mail">'+info[key].email+'</div>';
			print += '<div class="name">'+info[key].name+'</div>';
			if (!info[key].owner_status){
				let actW = '';
				let actX = '';
				if(info[key].security == 'r--'){}
				if(info[key].security == 'rw-'){actW = 'active';}
				if(info[key].security == 'rwx'){actW = 'active'; actX = 'active';}
			print += '<div class="but">\
				<div class="elem-chec flex_between">\
					<div class="acces_but_user acces_lvl_w checkbox-empty '+actW+'"  data-lvl="w">⚙</div>\
					<div class="acces_but_user acces_lvl_x checkbox-empty '+actX+'"  data-lvl="x">♕</div>\
					<div class="text-icon btn_delete_user block_mrl_4" title="Удалить?">✕</div>\
				</div>\
			</div>';} else {print += '<div class="but"></div>';}
			print += '</li>';
		}
		print += '</ul>'

		return print;
	}
	//список Шаблонов оборудования
	this.templates = function(info,dop){
		let list = info;
		let print = '';
		let S ={};
		S.ulOpen = '<ul class="list templates">';
		S.ulClouse = '</ul>';
		S.head = '\
			<li class="one-head sticky">\
				<div class="equ_n"><span class="block_w_46p"></span><span>Имя</span></div>\
				<div class="equ_t">Тип</div>\
				<div class="equ_b template_select_all user-none btn_text">Всё</div>\
			</li>';
		S.mid = ''; S.a = ''; S.tupe = '';
		S.ContA = dop.a || 'on';
		
		if(list.length > 0){ HWF.recount(list,function(elem){
			// Глобальные
			if (S.ContA == 'on'){S.a = '<a href="#'+HWD.page.temp+'/'+elem.id+'" class="click"></a>';}
			else {S.a = '<div class="click click_one_temp"></div>';}
			S.tupe = HWD.temp[elem.type].title;
			
			S.mid+= '<li class="one-elem" data-id="'+elem.id+'">';
			S.mid+= '	<div class="equ_n"><span class="icon icon_template_'+elem.type+'"></span><span class="name">'+elem.name+'</span></div>';
			S.mid+= '	<div class="equ_t">'+S.tupe+'</div>';
			S.mid+= '	<div class="equ_b equp_select_all user-none">\
							<div class="settings-butt">\
								<div class="btn-active checkbox one_template_check"></div>\
							</div>\
						</div>';
			S.mid+= 	S.a;
			S.mid+= '</li>';
		});}
		else {
			S.mid = '<li class="no-elems"><p><span class="block_plr_7"></span>Шаблоны отсутствуют</p></li>';
		}
		
		print = S.ulOpen+S.head+S.mid+S.ulClouse;
		return print;
	}
	
	
	
	
	// создание списка пользывателей текущей группы
	this.equpGroups = function(info,dop){
		let print = '';
		
		print += '<div class="table-mid flex_center_top"><ul class="list">';
		
		print += '<li class="one-head">';
		print += '<div class="name">Имя</div>';
		print += '<div class="id">id</div>';
		print += '<div class="but flex_between">'+
				'<div class="security flex_around">'+
					'<span class="button_text_2 vertical-middle equp_btn_all_r block_plr_8">r</span>'+
					'<span class="button_text_2 vertical-middle equp_btn_all_w block_plr_8">w</span>'+
					'<span class="button_text_2 vertical-middle equp_btn_all_x block_plr_8">x</span>'+
				'</div><div>'+
					'<span class="button_text_2 vertical-middle equp_select_all_g">Всё</span>'+
				'</div>'+
			'</div>';
		print += '</li>';
		
		for (let key in info){
			let elem = info[key];
			let name = elem.config.name;
			let id = elem.id;
			let actR = '', actW = '', actX = '';
			if(elem.security.indexOf('r') + 1){actR = 'active';}
			if(elem.security.indexOf('w') + 1){actW = 'active';}
			if(elem.security.indexOf('x') + 1){actX = 'active';}
			
			print += '<li class="one-equp" data-id="'+id+'">';
			print += '<div class="name"><a href="#equipment/'+id+'">'+name+'</a></div>';
			print += '<div class="id">'+id+'</div>';
			print += '<div class="but elem-chec flex_between">'+
					'<div class="security flex_between">'+
						'<div class="acces_but_equp acces_lvl_r btn-active checkbox-empty '+actR+'" data-lvl="r">r</div>'+
						'<div class="acces_but_equp acces_lvl_w btn-active checkbox-empty '+actW+'" data-lvl="w">w</div>'+
						'<div class="acces_but_equp acces_lvl_x btn-active checkbox-empty '+actX+'" data-lvl="x">x</div>'+
					'</div><div>'+
						'<div class="btn-active checkbox" data-id="'+id+'"></div>'+
					'</div>'+
				'</div>';
			print += '</li>';
		}
		
		
		print += '</ul></div>';
		
		return print;
	}
	
	//создаёт селектор выбора
	// obje = {"1":"name-1","2":"name-2","3":{"name":"name-3","selected":"selected"}} ;
	// selec = ключь объекта который нужно выбрать как "selected";
	this.select = function(obje,clas,selec){ 
		clas = clas || ''; selec = selec || false;
		let html = '<select size="1" class="'+clas+'">';
		for (let key in obje){
			let elem = obje[key];
			let val = elem.val || key;
			let selected = '',name = '';
			if (typeof elem == 'number'){name = elem;}
			if (typeof elem == 'string'){name = elem;}
			if (typeof elem == 'object'){name = elem.name; selected = elem.selected || '';}
			if (selec && selec == key){selected = 'selected';}
			html += '<option value="'+val+'" '+selected+'>'+name+'</option>'
		}
		html += '</select>';
		return html;
	}
	//создаёт таблицу
	// obje = ['test 1', 'test 2', 'test 3'] OR
	// obje = [{class:"clas1",html:"html text1"},{class:"clas1",html:"html text1"}]
	this.ul = function(obje,clas,dopD){ dopD = dopD || {};
		clas = clas || ''; 
		let clasLi = dopD.clasLi || '';
		let keyData = dopD.keyData || false;
		let ul = '<ul class="list '+clas+'">';
		for (let key in obje) {
			let elem = obje[key]; 
			let clas= clasLi || '',html='',data='',dataS='';
			if(keyData){dataS = 'data-key='+key}
			if(typeof elem == 'string'){html = elem || '';}
			if(typeof elem == 'object'){
				clas = elem.class || clasLi || '';
				html = elem.html || '';
				data = elem.data || '';
				if(data){for(let ke in data){dataS += 'data-'+ke+'="'+data[ke]+'"';}}
			}
			clas = 'class="'+clas+'"'
			ul += '<li '+clas+' '+dataS+'>'+html+'</li>';
		}
		return ul += '</ul>'
	}
	// вернёт Li для списка
	this.list = function(obj,str){
		let print = '';
		if(typeof obj == 'object' || typeof obj == 'array'){ for (let key in obj){ print += G.templateStr(obj[key],str)} }
		else {console.log('not object');}
		return print;
	}
	// функции котороя заменяет в строке все переменные если такие есть в объекте {{...}}
	this.templateStr = function(obj,str,data){ data = data || {};
		let print = str;
		let reg = /\{\{([a-zA-Z0-9_]+)\}\}/;
		let i = reg.exec(print);
		let up = data.up || '', du = data.du || '';
		while (i){
			let repl = obj[i[1]] || ""; 
			if(obj[i[1]] === 0){repl = '0';}
			repl =  up+repl+du;
			print = print.replace(i[0],repl);
			i = reg.exec(print);
		}
		return print
	}
	// создаёт пагинатор по параметрам
	// infou - объект со структурой
	// infou.np - номер страници
	// infou.allp - всего страниц
	// infou.view - посколько элементов отображается
	// infou.type - вид пагинатора
	this.pagination = function(infou){
		let info = infou || {};
		let ip = {};
			ip.target = info.target || '';
			ip.load = info.load || '';
			ip.tmp = info.tmp || ''; //шаблоны для распечатки
			ip.np = parseInt(info.number) || 1; // номер страници
			ip.allp = parseInt(info.all) || 0; // всего элементов
			ip.amount = parseInt(info.amount) || 10; // посколько элементов Отображается
			ip.type = info.type || 'norm'; // nowiev, list
			ip.left = parseInt(info.left) || 2; // Количество элементов с Лева
			ip.righ = parseInt(info.righ) || 2; // Количество элементов с Права
			ip.sum = info.sumPage || Math.ceil(ip.allp/ip.amount); // Общее количество страниц
			ip.start = parseInt(info.start) || false; // с какого элемента начинать амоут
			if(ip.start){ip.np = (ip.start/ip.amount)+1;}
		
		if (ip.allp > 0){
			let content = '<div data-all="'+ip.allp+'" data-target="'+ip.target+'" data-type="'+ip.type+'" data-load="'+ip.load+'" class="paginator">';
			let predel = false;
			let si = 1;
			let max = 5;

			if(ip.sum <= max){max=ip.sum;}
			else {
				if(ip.np + ip.righ <= ip.sum){max = ip.np + ip.righ} else{predel='max'}
				if(ip.np - ip.left >= ip.left){si = ip.np - ip.left} else {predel='min'}
				if(predel == 'max'){max = ip.sum; si = ip.sum-ip.left-ip.righ;}
				if(predel == 'min'){max = ip.left+ip.righ+1; si = 1;}
			}
			
			let pageList = '';
				for (let i=si;i <= max; i++){
					let clas = 'btn-paginator-page'; if (ip.np == i){clas='active'}
					pageList += '<span data-page="'+i+'" class="'+clas+'">'+i+'</span>';
				}
			let page = '\
				<div class="pagin-pages">\
					<span class="btn-paginator-left-end icon hover icon-pagin-left-end">‹‹</span>\
					<span class="btn-paginator-left icon hover icon-pagin-left">‹</span>\
					<div class="pagin-numbers">'+pageList+'</div>\
					<span class="btn-paginator-right icon hover icon-pagin-right"></span>\
					<span class="btn-paginator-right-end icon hover icon-pagin-right-end"></span>\
				</div>';

			let view = '<div class="pagin-amount"><select class="paginator-wiev-page block_w_70p" size="1">';
				for (let iopt=10; iopt <= 50; iopt=iopt+10){
					let optSelect = ''; if (ip.amount == iopt){optSelect = 'selected';}
					view += '<option '+optSelect+' value="'+iopt+'">По '+iopt+'</option>';
				}
				view +=	'</select></div>';
			
			let tmp = '';
			if (ip.tmp){
				tmp = '<div class="template">';
				if (ip.tmp.start){tmp += '<input type="textarea" class="tmp-start" value=\''+ip.tmp.start+'\'>';}
				if (ip.tmp.one){tmp += '<input type="textarea" class="tmp-one"   value=\''+ip.tmp.one+'\'>';}
				if (ip.tmp.end){tmp += '<input type="textarea" class="tmp-end"   value=\''+ip.tmp.end+'\'>';}
				if (ip.tmp.url){tmp += '<input type="textarea" class="tmp-url"   value=\''+ip.tmp.url+'\'>';}
				if (ip.tmp.dop){tmp += '<input type="textarea" class="tmp-dop"   value=\''+ip.tmp.dop+'\'>';}
				tmp += '</div>';
			}
			
			if (ip.type == 'norm'){content += page+view+tmp+'</div>';}
			if (ip.type == 'buffer'){content += page+view+tmp+'</div>';}
			if (ip.type == 'list'){content = pageList;}
			if (ip.type == 'templater'){content += page+view+tmp+'</div>';}
			return content;
		} else {return '';}
	}
	// создаёт окно для поиска
	this.search = function(clas,clasI,clasC,clasB){ 
		clas = clas || '';
		clasI = clasI || 'search_block_input';
		clasC = clasC || 'click_search_block';
		clasB = clasB || 'click_clear_search_block';
		let content = '\
		<div id="search-block" class="search '+clas+'">\
			<input class="'+clasI+'" type="text" value="" placeholder="Найти">\
			<span class="'+clasC+' search-block">⚲</span>\
			<span class="'+clasB+' clear-search-block tc_red" title="Сбросить">✕</span>\
		</div>';
		return content;
	}
	// фильтры для Груп
	this.groupsFilter = function(){	
		let print = 
			G.select(['Страна','Россия','Казахстан'])+
			G.select(['Область','Воронежская','Московская','Ростовская'])+
			G.select(['Город','Вороне','Москва','Ростов']);
		return print;
	}
	// поиск для Груп
	this.groupsSearch = function(){	
		let print = 
			G.search()+
			G.select(['По имени','Адрес','Примечание 1','Примечание 2'],'group-select');
		return print;
	}
	// создаёт фильтр по параметрам 
		//.target = цель фильтра
		//.apilist = переменная по которой брать из АПИ листа
		//.apiget = переменная запроса на серве
		//.view = вид распечатки ( невсе элементы его поддерживают )
		//.vlue = {value1:'вася1',valu2:'вася2'} - значение атрибутов option ( для выбора )
	this.filter = function(data){
		data = data || {};
		let s = '<select class="filter_page" size="1" data-apilist="'+data.apilist+'" data-apiget="'+data.apiget+'" data-view="'+data.view+'" data-target="'+target+'">';
		for (let key in data.vlue){
			let name = data.vlue[key];
			html += '<option value="'+key+'">'+name+'</option>'
		}
		s += '</select>'
		
		return s;
	}
	// фильтры для Проектов
	this.projectsFilter = function(target){
		let content = '';
		let data = HWS.getHash().get;
		let act = {};
		
		function create_date(d1,dFull){
			d1 = (d1)?d1.split(','):'';
			let d2={};
			if(d1 && d1.length > 0){ HWF.recount(d1,function(el){
				d2[el] = 'active';
			});} else { HWF.recount(dFull,function(el,key){
				d2[key] = 'active';
			});}
			return d2;
		}
		
		act.typeProj = create_date(data.type,{1:'',2:'',3:''});
		
		content += '<div id="filter-window" class="filter-window flex_between_top projects">';
		
		content += '<div class="colum">\
			<p class="title">Проекты '+HWhtml.helper(HWD.helpText.pages.progectSelect)+'</p>\
			<div><ul id="filter-typeProj-ul" data-type="type">\
				<li><span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				\
				<li class="filter-li '+(act.typeProj[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Картинка</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.typeProj[2] || '')+'" data-val="2">\
					<span class="checkbox"></span><span class="name">Карта</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.typeProj[3] || '')+'" data-val="3">\
					<span class="checkbox"></span><span class="name">Сетка</span><div class="click btn_one_filter"></div></li>\
			</ul></div>\
		</div>';
		
		content += '<div class="block_w_100">\
				<span class="style_btn grey btn_clear_equp_filter block_w_80p">Очистить</span>\
				<span class="block_pt_10"></span>\
				<span class="style_btn grey btn_accept_new_filter block_w_80p">Применить</span>\
		</div>';

		content += '</div>';
		
		return content;
	}
	// фильтры для оборудования
	this.equipFilter = function(target){
		target = target || '';
		let content = '';
		let data = HWS.getHash().get;
		let act = {};

		act.equpSustem = (data.equp_system == 2)?'':'active';
		act.type = (data.type)?data.type:'equp';
		act.typeOption = (data.type == 'syst')?'selected':'';
		act.idSystem = data.id_system || '';
		act.system = (act.idSystem)?'selected':'';
		act.systemSelect = (act.system)?'block':'';
		act.systemName = (act.idSystem)? HWhtml.miniLoader() :'Не выбрана';
		if(act.idSystem == 'no_select'){act.systemName = 'Не выбрана'}
			
		act.branch = create_date(data.branch,{1:'',2:'',3:'',5:'',6:'',7:'',8:''});
		act.ifaceGroup = create_date(data.iface_group,{0:'',1:'',2:''});
		act.connection = create_date(data.connection,{0:'',1:''});
		act.objectType = create_date(data.object_type,{14720:'',14721:'',14722:''});
		
		function create_date(d1,dFull){
			d1 = (d1)?d1.split(','):'';
			let d2={};
			if(d1 && d1.length > 0){ HWF.recount(d1,function(el){
				d2[el] = 'active';
			});} else { HWF.recount(dFull,function(el,key){
				d2[key] = 'active';
			});}
			return d2;
		}
		
		content += '<div id="filter-window" class="filter-window flex_between_top equipments '+act.type+'">';
		//content += '<div class="block_p_4"></div>';
		content += '<div class="colum">\
			<p class="title">Оборудование '+G.helper(HWD.helpText.pages.equipmentsFilteEqup)+'</p>\
			<div class="equp"><ul id="filter-equp-ul" data-type="branch">\
				<li>\
					<span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				<li id="system-check-filter" class="filter-li '+(act.equpSustem || '')+'" data-val="equp_system">\
					<span class="checkbox"></span><span class="name">Системы</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.branch[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Аналоговые датчики</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.branch[2] || '')+'" data-val="2">\
					<span class="checkbox"></span><span class="name">Дискретные датчики</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.branch[3] || '')+'" data-val="3">\
					<span class="checkbox"></span><span class="name">Устройства управления</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.branch[5] || '')+'" data-val="5">\
					<span class="checkbox"></span><span class="name">Фотокамеры</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.branch[6] || '')+'" data-val="6">\
					<span class="checkbox"></span><span class="name">Адаптеры котла</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.branch[7] || '')+'" data-val="7">\
					<span class="checkbox"></span><span class="name">Ключи ТМ</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.branch[8] || '')+'" data-val="8">\
					<span class="checkbox"></span><span class="name">Радиобрелоки</span><div class="click btn_one_filter"></div></li>\
			</ul></div>\
		</div>';
		
		content += '<div class="colum">\
			<p class="title">Подключение '+G.helper(HWD.helpText.pages.equipmentsFilteConnect)+'</p>\
			<div class="equp"><ul id="filter-type-connect-ul" data-type="iface_group">\
				<li>\
					<span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				<li class="filter-li '+(act.ifaceGroup[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Проводной</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.ifaceGroup[2] || '')+'" data-val="2">\
					<span class="checkbox"></span><span class="name">Беспроводной</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.ifaceGroup[0] || '')+'" data-val="0">\
					<span class="checkbox"></span><span class="name">Встроенный</span><div class="click btn_one_filter"></div></li>\
			</ul></div>\
		</div>';
		
		content += '<div class="colum">\
			<p class="title">Связь '+G.helper(HWD.helpText.pages.equipmentsFilteLink)+'</p>\
			<div><ul id="filter-connection-ul" data-type="connection">\
				<li>\
					<span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				<li class="filter-li '+(act.connection[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">'+HWD.globalText.yes_connect+'</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.connection[0] || '')+'" data-val="0">\
					<span class="checkbox"></span><span class="name">'+HWD.globalText.no_connect+'</span><div class="click btn_one_filter"></div></li>\
			</ul></div>\
		</div>';
		
		content += '<div class="colum flex_between_colum">\
			<div class="block_w_100"><div class="equp">\
				<select class="btn_filter_selector_all_or_one_sys tc_black" size="1">\
					<option value="all">Все системы</option>\
					<option value="select" '+act.system+'>По системе</option>\
				</select> '+G.helper(HWD.helpText.pages.equipmentsFilteSustem)+'\
				<div class="selector_blocl_system block_pt_45 block-onof '+act.systemSelect+'">\
					<p></p>\
					<p>Система: <span class="tw_700" id="filter-name-system-select" data-id="'+act.idSystem+'">'+act.systemName+'</span> <br>\
					<span class="btn_text_2 btn_filter_select_system block_pt_10">Выбрать</span></p>\
				</div>\
			</div></div>\
		</div>';
		
		content += '<div class="block_w_100">\
				<span class="style_btn grey btn_clear_equp_filter block_w_80p">Очистить</span>\
				<span class="block_pt_10"></span>\
				<span class="style_btn grey btn_accept_new_filter block_w_80p">Применить</span>\
		</div>';
		content += '</div>';
		
		return content;
	}
	// фильтры для програм
	this.programsFilter = function(target){
		let content = '';
		let data = HWS.getHash().get;
		let act = {};
		
		function create_date(d1,dFull){
			d1 = (d1)?d1.split(','):'';
			let d2={};
			if(d1 && d1.length > 0){ HWF.recount(d1,function(el){
				d2[el] = 'active';
			});} else { HWF.recount(dFull,function(el,key){
				d2[key] = 'active';
			});}
			return d2;
		}
		
		act.idSystem = data.id_system || '';
		act.system = (act.idSystem)?'selected':'';
		act.systemSelect = (act.system)?'block':'';
		act.systemName = (act.idSystem)? HWhtml.miniLoader() :'Не выбрана';
		if(act.idSystem == 'no_select'){act.systemName = 'Не выбрана'}
		act.typeProg = create_date(data.object_type,{32768:'',32769:'',32770:'',32771:'',32772:''});
		
		content += '<div id="filter-window" class="filter-window flex_between_top programs">';
		
		content += '<div class="colum">\
			<p class="title">Программы '+HWhtml.helper(HWD.helpText.pages.programsSelectProg)+'</p>\
			<div><ul id="filter-type-prog-ul" data-type="object_type">\
				<li><span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				\
				<!--li class="filter-li '+(act.typeProg[32768] || '')+'" data-val="32768">\
					<span class="checkbox"></span><span class="name">Оповещение</span><div class="click btn_one_filter"></div></li-->\
				<li class="filter-li '+(act.typeProg[32769] || '')+'" data-val="32769">\
					<span class="checkbox"></span><span class="name">Охрана</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.typeProg[32770] || '')+'" data-val="32770">\
					<span class="checkbox"></span><span class="name">Отопление</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.typeProg[32771] || '')+'" data-val="32771">\
					<span class="checkbox"></span><span class="name">По датчикам</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.typeProg[32772] || '')+'" data-val="32772">\
					<span class="checkbox"></span><span class="name">По расписанию</span><div class="click btn_one_filter"></div></li>\
			</ul></div>\
		</div>';
		
		content += '<div class="colum flex_between_colum">\
			<div class="block_w_100 block_p_8"><div class="equp">\
				<select class="btn_filter_selector_all_or_one_sys" size="1">\
					<option value="all">Все системы </option>\
					<option value="select" '+act.system+'>По системе</option>\
				</select> '+HWhtml.helper(HWD.helpText.pages.programsSelectSystem)+'\
				<div class="selector_blocl_system block_pt_45 block-onof '+act.systemSelect+'">\
					<p></p>\
					<p>Система: <span class="btn_text_2 btn_filter_select_system">Выбрать</span></p>\
					<p id="filter-name-system-select" class="block_pt_10" data-id="'+act.idSystem+'">'+act.systemName+'</p>\
				</div>\
			</div></div>\
		</div>';
		
		content += '<div class="colum"></div>';
		content += '<div class="colum"></div>';
		
		content += '<div class="block_w_100">\
				<span class="style_btn grey btn_clear_equp_filter block_w_80p">Очистить</span>\
				<span class="block_pt_10"></span>\
				<span class="style_btn grey btn_accept_new_filter block_w_80p">Применить</span>\
		</div>';
		content += '</div>';
		
		return content;
	}
	
	// фильтры для 
	this.templatesFilter = function(target){
		let content = '';
		let data = HWS.getHash().get;
		let act = {};
		
		function create_date(d1,dFull){
			d1 = (d1)?d1.split(','):'';
			let d2={};
			if(d1 && d1.length > 0){ HWF.recount(d1,function(el){
				d2[el] = 'active';
			});} else { HWF.recount(dFull,function(el,key){
				d2[key] = 'active';
			});}
			return d2;
		}
		
		act.sensors = create_date(data.sensors,{1:'',2:'',3:'',4:'',5:'',6:''});
		act.programs = create_date(data.programs,{1:'',2:'',3:'',4:'',5:''});
		act.random = create_date(data.random,{1:'',2:''});
		
		content += '<div id="filter-window" class="filter-window flex_between_top templates">';
		
		content += '<div class="colum">\
			<p class="title">Датчики</p>\
			<div><ul id="filter-sensors-ul" data-type="sensors">\
				<li><span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				\
				<li class="filter-li '+(act.sensors[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Температуры</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.sensors[2] || '')+'" data-val="2">\
					<span class="checkbox"></span><span class="name">Влажности</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.sensors[3] || '')+'" data-val="3">\
					<span class="checkbox"></span><span class="name">АКБ</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.sensors[4] || '')+'" data-val="4">\
					<span class="checkbox"></span><span class="name">Дискретный</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.sensors[5] || '')+'" data-val="5">\
					<span class="checkbox"></span><span class="name">Тока</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.sensors[6] || '')+'" data-val="6">\
					<span class="checkbox"></span><span class="name">Общий</span><div class="click btn_one_filter"></div></li>\
			</ul></div>\
		</div>';
		
		content += '<div class="colum">\
			<p class="title">Программы </p>\
			<div><ul id="filter-programs-ul" data-type="programs">\
				<li><span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				\
				<li class="filter-li '+(act.programs[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Отопление</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.programs[2] || '')+'" data-val="2">\
					<span class="checkbox"></span><span class="name">По датчикам</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.programs[3] || '')+'" data-val="3">\
					<span class="checkbox"></span><span class="name">По расписанию</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.programs[4] || '')+'" data-val="4">\
					<span class="checkbox"></span><span class="name">Оповещения</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.programs[5] || '')+'" data-val="5">\
					<span class="checkbox"></span><span class="name">Охрана</span><div class="click btn_one_filter"></div></li>\
			</ul></div>\
		</div>';
		
		content += '<div class="colum">\
			<p class="title">Прочее </p>\
			<div><ul id="filter-random-ul" data-type="random">\
				<li><span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				\
				<li class="filter-li '+(act.random[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Устройство управления</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.random[2] || '')+'" data-val="2">\
					<span class="checkbox"></span><span class="name">Система</span><div class="click btn_one_filter"></div></li>\
			</ul></div>\
		</div>';
		
		content += '<div class="block_w_100">\
				<span class="style_btn grey btn_clear_equp_filter block_w_80p">Очистить</span>\
				<span class="block_pt_10"></span>\
				<span class="style_btn grey btn_accept_new_filter block_w_80p">Применить</span>\
		</div>';
		content += '</div>';
		
		return content;
	}
	
	// Пин группы на карте
	this.pinGrup = function(id,name,num){
		let s = '\
		<a href="#'+HWD.page.group+'/'+id+'"><h4 class="tc_orange">'+name+'</h4></a>\
		<div class="pin group">\
			<div data-target="ecto-pin-map" class="click-pin click btn-active-one"></div>\
			<div class="menu-elem">\
				<div class="menu-btn-clouse"><div class="icon icon-delete" data-num="'+num+'"></div></div>\
				<div class="menu-btn-change"><a class="text-icon" href="#'+HWD.page.group+'/'+id+'">&#9998;</a></div>\
			</div>\
		</div>\
		<div class="marker-shadow"></div>\
		<div class="leaflet-popup-tip-container">\
			<div class="leaflet-popup-tip"></div>\
		</div>\
		';
		return s;
	}
	// Пин проекта на карте
	this.pin = function(id,name,num,sum){
		sum = sum || '';
		let s = '\
		<a href="#'+HWD.page.proj+'/'+id+'"><h4 class="tc_orange">'+name+'</h4></a>\
		<div class="pin">\
			<div data-target="ecto-pin-map" class="click-pin click btn-active-one"><div class="sum">'+sum+'</div></div>\
			<div class="menu-elem">\
				<div class="menu-btn-clouse"><div class="icon icon-delete p18 hover_red pin-clouse" data-num="'+num+'"></div></div>\
				<div class="menu-btn-change"><a class="icon icon-settings p18 hover" href="#'+HWD.page.proj+'/'+id+'"></a></div>\
			</div>\
		</div>\
		<div class="marker-shadow"></div>\
		<div class="leaflet-popup-tip-container">\
			<div class="leaflet-popup-tip"></div>\
		</div>\
		';
		return s;
	}
	this.pinAdmin = function(tyep){ tyep = tyep || '';
		let s = '\
		<div class="pin admin-'+tyep+'"></div>\
		<div class="marker-shadow"></div>';
		return s;
	}
	// Шаблоны для каждого даттчика (t)Шаблоны (e)Оборудования (m)главной (p)страници)
	this.TEMP = function(elem,type){
		let normInfo = '';
		let block = document.createElement('div');
			block.id = 'info-'+type+'-'+elem.id;
			block.classList.add('info-block');
			block.classList.add('type-'+type);
			block.classList.add('new-block');
			block.style.top = parseFloat(elem.y)+"%";
			block.style.left = parseFloat(elem.x)+"%";
			block.dataset.id = elem.id  || '';
			block.dataset.type = type || '';
			
		if(type == 'proj'){ //prog
			block.style.width = (elem.width || 12)+"%";
			block.style.height = (elem.height || 12)+"%";
			normInfo = {};
			normInfo.tmp = 'proj';
			normInfo.name = elem.name;
		} else {
			normInfo = HWequip.normInfo(elem);
		}
		
		let s = '';
		let content = '<div class="ib-content">';
		// меню кнопок
		let spec = '<div class="ib-spec">';
		let secBtn = '\
			<div class="menu-btn-clouse"><span class="icon icon-delete hover_red ib-clouse"></span></div>\
			<div class="menu-btn-dragdrop"><div class="ib-move click"></div><div class="icon hover icon-dragdrop"></div></div>\
		';
		
		//console.log(normInfo);
		
		// аналоговые датчики 
		if(normInfo.tmp == "analog3x"){
			let data = '';
			if(normInfo.state == "no_connect"){data = '<span class="icon icon-radio-0"></span>'}
			else{data = normInfo.val}
			s = '<div class="ib-center">\
				<div class="ib-name">'+normInfo.name+'</div>\
				<div class="ib-data" style="background:'+normInfo.stateColor+'">'+data+'</div>\
			</div>';
			
			secBtn += '<div class="menu-btn-change"><a href="#'+HWD.page.equp+'/'+normInfo.id+'" class="icon icon-settings hover"></a></div>';
		} 
		// дискретные датчики 
		else if(normInfo.tmp == "discret3x"){ 
			let view = '<span class="icon icon-check wite"></span>'; 
			if (normInfo.state == 'alert') {view = '<span class="icon icon-alarm wite"></span>'};
			if(normInfo.state == "no_connect"){view = '<span class="icon icon-radio-0"></span>'}
			s = '<div class="ib-center">\
				<div class="ib-name">'+normInfo.name+'</div>\
				<div class="ib-data" style="background:'+normInfo.stateColor+'">'+view+'</div>\
			</div>';
			
			secBtn += '<div class="menu-btn-change"><a href="#'+HWD.page.equp+'/'+normInfo.id+'" class="icon icon-settings hover"></a></div>';
		} 
		// устройства управления / реле  
		else if(normInfo.tmp == "control_device3x"){
			if(normInfo.val == 1){block.classList.add('state-on');}
			else {block.classList.add('state-off');}
			let data = '';
			if(normInfo.state == "no_connect"){data = '<span class="icon icon-radio-0"></span>'}
			else{data = '<span class="icon icon-btn"></span>'}
			let controlClick = '';
			if(normInfo.elem.lk && normInfo.elem.lk.control_locked) {block.classList.add('blocked');}
			
			s = '<div class="ib-center ib-relay">\
				<div class="ib-name">'+normInfo.name+'</div>\
				<div class="ib-data">'+data+'</div>\
			</div>';
			
			secBtn += '<div class="menu-btn-onoff"><div class="ib-onoff click"></div><div class="icon icon-btn"></div></div>';
			secBtn += '<div class="menu-btn-change"><a href="#'+HWD.page.equp+'/'+normInfo.id+'" class="icon icon-settings hover"></a></div>';
		}
		// Проекты
		else if(normInfo.tmp == "proj"){
			content = '<div class="ib-content type-project">';
			s = '<div class="ib-center"><h4>'+elem.name+'</h4></div>';
			secBtn += '<div class="menu-btn-resize"><div class="ib-resize click"></div><div class="icon icon-resize"></div></div>';
			secBtn += '<div class="menu-btn-change"><a href="#'+HWD.page.proj+'/'+elem.id+'" class="icon icon-settings hover"></a></div>';
		}
		// Система
		else if(normInfo.tmp == "system3x"){
			let color = HWD.color.red;
			if(elem.connection){color = HWD.color.green}
			s = '<div class="ib-center">\
				<div class="ib-name">'+normInfo.name+'</div>\
				<div class="ib-data" style="background:'+color+'"></div>\
			</div>';
			
			secBtn += '<div class="menu-btn-change"><a href="#'+HWD.page.equp+'/'+normInfo.id+'" class="icon icon-settings hover"></a></div>';
		}
		else {
			let data = '';
			if(normInfo.state == "no_connect"){data = '<span class="icon icon-radio-0"></span>'}
			
			s = '<div class="ib-center ib-relay">\
				<div class="ib-name">'+normInfo.name+'</div>\
				<div class="ib-data" style="background:#CCCCCC">'+data+'</div>\
			</div>';
		}
		
		spec += secBtn+'</div>';
		content += spec+s+'<div title="'+normInfo.name+'" class="ib-click click"></div></div>'
		block.innerHTML = content;
		
		return block;
	}
	// меню нажатия правой кнопки на картинки
	this.rMenuImg = function(){
		let s  = '\
			<div class="rm-add-obj"><span class="select-none">Проект</span><div class="icon icon-home select-none"></div></div>\
			<div class="rm-add-equip"><span class="select-none">Оборудование</span><div class="icon icon-equipment select-none"></div></div>\
			<!--div class="rm-add-prog"><span class="select-none">Программа</span><div class="icon icon-programs select-none"></div></div-->\
		';
		return s;
	}
	// меню нажатия правой кнопки на карте
	this.rMenuMap = function(){
		let s  = '\
			<div class="rm-add-objpin"><span class="select-none">Проект</span><div class="icon icon-home select-none"></div></div>';
		return s;
	}
	//Меню виджетов
	this.rMenuVijet = function(){
		let s  = '\
			<div class="vjet-add-obj"><span class="select-none">Проект</span><div class="icon icon-home select-none"></div></div>\
			<div class="vjet-add-equip"><span class="select-none">Устройства</span><div class="icon icon-equipment select-none"></div></div>\
			<div class="vjet-add-prog"><span class="select-none">Программа</span><div class="icon icon-programs select-none"></div></div>\
			<div class="vjet-add-system"><span class="select-none">Системы</span><div class="icon icon-equipment select-none"></div></div>\
		';
		return s;
	}
	// меню нажатия правой кнопки на карте
	this.rMenuProg = function(){
		let s  = '\
			<div class="prog-add-3">3.x</div>\
			<div class="prog-add-4">4.x</div>'
		return s;
	}
	// меню нажатия правой кнопки на картинки
	this.rMenuEqup = function(){
		let s  = '\
			<div class="rm-add-system"><span class="select-none">Система</span><div class="icon icon-home select-none"></div></div>\
			<div class="rm-add-equip"><span class="select-none">Устройства</span><div class="icon icon-equipment select-none"></div></div>\
		';
		return s;
	}
	// Загрузчик
	this.loader = function(){let s = '<div class="loading-content-2"> <svg class="circular" viewBox="25 25 50 50">\
    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>\
    </svg> </div>'; return s;}
	//this.loader = function(){let s = '<div class="loading-content"></div>'; return s;}
	this.miniLoader = function(clas){clas = clas || ''; let s = '<span class="loading-content-2 mini '+clas+'"> <svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>\
    </svg> </span>'; return s;}
	// вопросик с помощью
	this.helper = function(info){info = info || '' ;let s = '<span data-text="'+info+'" class="helper">?</span>'; return s;}	
	// Окно ( модально ) для дополнительного подтверждения действия
	this.mConfirmTitle = function(title){let s = title || 'Подтвердите действие'; return s;}
	this.mConfirmTitlePrint = function(title){let s = title || ''; return s;}
	this.mConfirm = function(title,dop,yes,no,remClas){
		dop = dop || ''; remClas = remClas || 'btn-active';
		let head = title || '<h4>Действительно это сделать !?</h4>'
		let s = '<div class="confirm-modal"><div class="confirm-modal-block">'+
			'<div class="confirm-top">'+head+'</div>'+
			'<div class="btns-modal-nocust">'+
				'<div data-target="modal-window" class="btn-no-modal btn-active style_btn grey">'+no+'</div>'+
				'<div data-target="modal-window" class="btn-yes-modal '+dop+' '+remClas+' style_btn grey">'+yes+'</div>'+
			'</div>'+
		'</div></div>';
		return s;
	}
	this.mConfirmCust = function(obj){
		let yesBtn = '';
		let CD = {}; // Control data
			CD.print = obj.print || '';
			CD.yes = obj.yes || 'Да';
			CD.no = obj.no || 'Отмена';
			CD.classYes = obj.classYes || '';
			CD.classNo = obj.classNo || '';
			CD.clouseYes = (obj.clouseYes)?'':'btn-active'; // автоматическое закрытие модального окна
			CD.clouseNo = (obj.clouseNo)?'':'btn-active';  // автоматическое закрытие модального окна
			CD.yesHref = obj.yesHref || false; // если существует то кнопка обрамляется в ссылку и перекидывает нас.
			
		if(CD.yesHref) {yesBtn = '<a class="btn-yes-modal style_btn grey" href="#'+CD.yesHref+'">'+CD.yes+'</a>';}
		else {yesBtn = '<div data-target="modal-window" class="btn-yes-modal '+CD.classYes+' '+CD.clouseYes+' style_btn grey">'+CD.yes+'</div>';}

		let s = '<div class="confirm-modal"><div class="confirm-modal-block">'+
			'<div class="confirm-top">'+CD.print+'</div>'+
			'<div class="btns-modal">'+
				'<div data-target="modal-window" class="btn-no-modal '+CD.classNo+' '+CD.clouseNo+' style_btn grey">'+CD.no+'</div>'+
				yesBtn+
			'</div>'+
		'</div></div>';
		return s;
	}
	this.mConfirmMessage = function(title){
		let head = title || '<h4>Сообщение</h4>'
		let s = '<div class="confirm-modal"><div class="confirm-modal-block">'+
			'<div class="confirm-top">'+head+'</div>'+
			'<div class="btns-modal"><div data-target="modal-window" class="btn-no-modal btn-active style_btn btn_confir_accept">OK</div></div>'+
		'</div></div>';
		return s;
	}
	// создание шаблона плитки или блока
	this.tile = function(clas){
		clas = clas || 'one-tile'
		clas = clas.split(' ');
		let tile = document.createElement('div');
		let top = document.createElement('header'); tile.appendChild(top);
		let mid = document.createElement('section'); tile.appendChild(mid);
		let bot = document.createElement('footer'); tile.appendChild(bot);
		
		clas.forEach(function(elClass){ tile.classList.add(elClass) });
		
		return {b:tile,top:top,mid:mid,bot:bot};
	}
	// Объект удалился!
	this.delete = new function(){
		this.ok = function(){
			let s  = '\
				<div class="one-block ob-center ob-column">\
					<h3 class="block_pb_10">Удалено!</h3>\
					<div class="style_btn btn_back_page">↩ Назад</div>\
				</div>';
			return s;
		}
		this.load = function(){
			let s = '\
				<div class="one-block ob-center">\
					<div class="loading-content"></div>\
					<p>Удаление...</p>\
				</div>';
			return s;
		}
	}
	// шаблоны кнопок
	this.btn = new function(){
		this.add = function(Dclas){Dclas = Dclas || ''; let s = '<div title="Добавить" class="icon icon-add hover '+Dclas+'">✛</div>'; return s;}
		this.back = function(){let s = '<div class="text-icon btn_back_page" title="Назад">↩</div>'; return s;}
		this.onof = function(act,clas){
			let activ = ''; clas = clas || '';
			if(act == 1 || act == 'active' || act == true){activ = 'active';}
			let s = '<span class="'+clas+' btn-active on-off '+activ+'"></span>'; return s;
		}
		this.numb = function(val,data){ val = val || 0; data = data || {};
			let dn = {}
			dn.clas = data.clas || ''; 
			dn.clase = data.clase || 'btn_int_click';
			dn.clasinp = data.clasinp || 'inp_num_val';
			dn.tofix = data.tofix || 0;
			dn.iter = data.iter || 1;
			dn.max = data.max || '';
			dn.min = (data.min == 0)?'null':data.min || '';
			dn.fcn = data.fcn || '';
			dn.step = data.step || 1;
			dn.id = data.id || '';
			let s = '<span id="'+dn.id+'" class="btn-int '+dn.clas+'" data-tofix="'+dn.tofix+'" data-iter="'+dn.iter+'" data-fcn="'+dn.fcn+'" data-max="'+dn.max+'" data-min="'+dn.min+'" data-step="'+dn.step+'">'+
			'<span class="'+dn.clase+'" data-cont="-"></span>'+
			'<input class="'+dn.clasinp+' inp_only_numb" type="text" value="'+val+'">'+
			'<span class="'+dn.clase+'" data-cont="+"></span>'+
			'</span>'; 
			return s;
		}
		this.scrol = function(val,data){ val = val || 0; data = data || {}
			let dc = {}, str = {};
			dc.type = "norm";
			dc.name = data.name || '';
			dc.min = data.min || 0;
			dc.max = data.max || 100;
			dc.step = data.step || 10;
			dc.vald = val || data.max/3;
			dc.stepk = (dc.max - dc.min)/dc.step;
			dc.posd = (dc.vald-dc.min)*(100/(dc.max - dc.min)); dc.posd = dc.posd.toFixed(4);
			dc.clas = data.clas || '';
			dc.tofix = data.tofix || 0;
			dc.iter = dc.tofix/10;
			dc.fcn = data.fcn || '';
			dc.unit = data.unit || '';
			dc.nameMax = data.nameMax || 'Максимум';			
			dc.helper = data.helper || 'Максимум';
			dc.id = data.id || '';
			str.shtrih = '';
			let iter = 0;

			while (iter <= dc.step){
				let numb = parseFloat((dc.min + (iter*dc.stepk)));
					numb = numb.toFixed(dc.tofix)
				str.shtrih += '<p><b>|</b><span>'+numb+dc.unit+'</span></p>';
				iter++;
			}
			let s = '<div id="'+dc.id+'" class="ecto-scrol duo '+dc.clas+'" data-min="'+dc.min+'" data-max="'+dc.max+'" data-tofix="'+dc.tofix+'" data-step="'+dc.step+'" data-fcn="'+dc.fcn+'">'+
				'<div class="scale-temp">'+
					'<div class="shtrih-temp">'+str.shtrih+'</div>'+
					'<span class="line btn_scrol_line_click"></span>'+
					'<span class="line-color"></span>'+
					'<span class="line-color-back"></span>'+
					'<span class="line-color-1" style="width:'+dc.pos+'%;"></span>'+
					'<span class="line-color-2" style="width:'+(100-dc.posd)+'%;"></span>'+
					'<div class="target val_2" style="left:'+dc.posd+'%;">'+
						'<div class="arrow-temp"></div>'+
						'<span class="btn-line top">'+dc.vald+'</span>'+
						'<span class="btn-line bot">'+dc.vald+'</span>'+
					'</div>'+
				'</div>'+
				'<div class="ltft"></div>'+
				'<div class="right scale-name">'+
					'<div class="colum val_max">'+
						'<p class="tc_black">'+dc.nameMax+' '+G.helper(dc.helper)+'</p>'+
						G.btn.numb(dc.vald,{clas:'val_2',clase:'btn_scrol_num_click',clasinp:'scrol_inp_num_val',tofix:dc.tofix,iter:dc.iter,min:dc.min,max:dc.max})+
					'</div>'+
				'</div>'+
			'</div>';
			return s;
		}
		this.scrolDuo = function(val,data){ val = val || [1,5]; data = data || {}
			let dc = {}, str = {};
			dc.type = "norm";
			dc.name = data.name || '';
			dc.min = data.min || 0;
			dc.max = data.max || 100;
			dc.step = data.step || 10;
			dc.val = val[0] || data.max/2;
			dc.vald = val[1] || data.max/3;
			dc.stepk = (dc.max - dc.min)/dc.step;
			dc.pos = (dc.val-dc.min)*(100/(dc.max - dc.min)); dc.pos = dc.pos.toFixed(4);
			dc.posd = (dc.vald-dc.min)*(100/(dc.max - dc.min)); dc.posd = dc.posd.toFixed(4);
			dc.clas = data.clas || '';
			dc.tofix = data.tofix || 0;
			dc.iter = dc.tofix/10;
			dc.fcn = data.fcn || '';
			dc.unit = data.unit || '';
			dc.nameMin = data.nameMin || 'Минимум';
			dc.nameMax = data.nameMax || 'Максимум';			
			dc.nameMinHelp = data.nameMinHelp || 'Минимум';
			dc.nameMaxHelp = data.nameMaxHelp || 'Максимум';
			dc.id = data.id || '';
			str.shtrih = '';
			let iter = 0;

			while (iter <= dc.step){
				let numb = parseFloat((dc.min + (iter*dc.stepk)));
					numb = numb.toFixed(dc.tofix)
				str.shtrih += '<p><b>|</b><span>'+numb+dc.unit+'</span></p>';
				iter++;
			}
			let s = '<div id="'+dc.id+'" class="ecto-scrol duo '+dc.clas+'" data-min="'+dc.min+'" data-max="'+dc.max+'" data-tofix="'+dc.tofix+'" data-step="'+dc.step+'" data-fcn="'+dc.fcn+'">'+
				'<div class="scale-temp">'+
					'<div class="shtrih-temp">'+str.shtrih+'</div>'+
					'<span class="line btn_scrol_line_click"></span>'+
					'<span class="line-color"></span>'+
					'<span class="line-color-back"></span>'+
					'<span class="line-color-1" style="width:'+dc.pos+'%;"></span>'+
					'<span class="line-color-2" style="width:'+(100-dc.posd)+'%;"></span>'+
					'<div class="target val_1" style="left:'+dc.pos+'%;">'+
						'<div class="arrow-temp"></div>'+
						'<span class="btn-line top">'+dc.val+'</span>'+
						'<span class="btn-line bot">'+dc.val+'</span>'+
					'</div>'+
					'<div class="target val_2" style="left:'+dc.posd+'%;">'+
						'<div class="arrow-temp"></div>'+
						'<span class="btn-line top">'+dc.vald+'</span>'+
						'<span class="btn-line bot">'+dc.vald+'</span>'+
					'</div>'+
				'</div>'+
				'<div class="ltft"></div>'+
				'<div class="right scale-name">'+
					'<div class="colum val_min">'+
						'<p class="tc_black">'+dc.nameMin+' '+G.helper(dc.nameMinHelp)+'</p>'+
						G.btn.numb(dc.val,{clas:'val_1',clase:'btn_scrol_num_click',clasinp:'scrol_inp_num_val',tofix:dc.tofix,iter:dc.iter,min:dc.min,max:dc.max})+
					'</div>'+
					'<div class="colum val_max">'+
						'<p class="tc_black">'+dc.nameMax+' '+G.helper(dc.nameMaxHelp)+'</p>'+
						G.btn.numb(dc.vald,{clas:'val_2',clase:'btn_scrol_num_click',clasinp:'scrol_inp_num_val',tofix:dc.tofix,iter:dc.iter,min:dc.min,max:dc.max})+
					'</div>'+
				'</div>'+
			'</div>';
			return s;
		}
		this.check=function(data){ data=data||{}
			let onof = 'data-onof="'+data.onof+'"' || ''
			let s = '<div class="btn-active checkbox" '+onof+'></div>'; 
			return s; 
		}
		this.selectEqup = function(){
			let btn = '↺' // +
			let s = '<div class="select-one-equp"><ul>'+
				'<li class="one-head"><div><span class="btn_add_elem">'+btn+'</span></div><div>Имя</div><div>Данные</div><div><i></i></div></li>'+
				'<li class="one-elem" id="1"><div><icon></icon></div><div>equp - bag</div><div>24</div><div><div></div><div class="button-text grey hover btn_seting_elem" title="Настройки">✎</div></div></li>'+
			'</ul></div>'; 
			return s;
		}
		this.sortAZ = function(nameList,target,fcn){
			let s = '';
			let view = HWS.buffer.dopFilter.name || 0;
			let icon = 'icon-sort-az';
			if(!view){icon = 'icon-sort-za';}
			s = '<span data-list="'+nameList+'" data-target="'+target+'" data-fcn="'+fcn+'" class="icon '+icon+' p24 grey icon_btn_sort block_ml_10 hover"></span>';
			return s;
		}
	}
	
	// шаблон скрола пределов
	this.scrol = function(data){
		
	}
	// шаблон лимита пределов
	this.limit = function(data){
		data = data || {}
		let s = '',numberStep='',shtrih='';
		let min = data.min || 0;
		let max = data.max || 100;
		let warMin = data.warMin || {};
		let warMax = data.warMax || {};
		let aleMin = data.aleMin || {};
		let aleMax = data.aleMax || {};
		let fcn = data.fcn || '';
		let clas = data.class || 'one-block ob-top one-segment-50';
		let id = data.id || 'limit-block';
		
		let iter = min;
		while (iter <= max){
			numberStep += '<p>'+iter+'</p>';
			shtrih += '<p>|</p>';
			iter = iter+10;
		}
		let specKof = 100/(max-min);
		
		if(warMin.act == 'of'){warMin.print = ''; warMin.printModul = ''}
		else {
			let btnWarMin = parseFloat((warMin.val-min)*specKof).toFixed(4);
			if(warMin.act == 'onlyVal'){warMin.active = 'active'}
			warMin.print = '<div class="line line_war_min '+warMin.active+' orange-left-scale" style="width:'+btnWarMin+'%;">\
				<span class="dop-left">\
				<span class="btn_line_move btn-line top left">'+warMin.val.toFixed(1)+'</span>\
				<span class="glista left">\<span class="ges-left"></span>\<span class="ges-right"></span>\</span>\
				<span class="btn_line_move btn-line bot left">min</span>\
			</div>';
			warMin.printModul = print_modul(Object.assign(warMin,{name:'Внимание мин',col:'#feca58',clas:'warning_min_predel'}))
		}
		
		if(warMax.act == 'of'){warMax.print = ''; warMax.printModul = '';}
		else {
			let btnWarMax = parseFloat((max-warMax.val)*specKof).toFixed(4);
			if(warMax.act == 'onlyVal'){warMax.active = 'active'}
			warMax.print = '<div class="line line_war_max '+warMax.active+' orange-righ-scale" style="width:'+btnWarMax+'%;">\
				<span class="btn_line_move btn-line top right">'+warMax.val.toFixed(1)+'</span>\
				<span class="glista right">\<span class="ges-left"></span>\<span class="ges-right"></span>\</span>\
				<span class="btn_line_move btn-line bot right">max</span>\
			</div>';
			warMax.printModul = print_modul(Object.assign(warMax,{name:'Внимание макс',col:'#feca58',clas:'warning_max_predel'}))
		}
		
		if(aleMin.act == 'of'){aleMin.print = ''; aleMin.printModul = '';}
		else {
			let btnAleMin = parseFloat((aleMin.val-min)*specKof).toFixed(4);
			if(aleMin.act == 'onlyVal'){aleMin.active = 'active'}
			aleMin.print = '<div class="line line_alt_min '+aleMin.active+' red-left-scale" style="width:'+btnAleMin+'%;">\
				<div class="dop-btn-open-line"></div>\
				<span class="btn_line_move btn-line top left">'+aleMin.val.toFixed(1)+'</span>\
				<span class="glista left">\<span class="ges-left"></span>\<span class="ges-right"></span>\</span>\
				<span class="btn_line_move btn-line bot left">min</span>\
			</div>';
			aleMin.printModul = print_modul(Object.assign(aleMin,{name:'Нижний порог '+G.helper(HWD.helpText.equp.btnHelpPorogDown),col:'#ff7778',clas:'alert_min_predel'}))
		}
		
		if(aleMax.act == 'of'){aleMax.print = ''; aleMax.printModul = '';}
		else {
			let btnAleMax = parseFloat((max-aleMax.val)*specKof).toFixed(4);
			if(aleMax.act == 'onlyVal'){aleMax.active = 'active'}
			aleMax.print = '<div class="line line_alt_max '+aleMax.active+' red-righ-scale" style="width:'+btnAleMax+'%;">\
				<div class="dop-btn-open-line"></div>\
				<span class="btn_line_move btn-line top right">'+aleMax.val.toFixed(1)+'</span>\
				<span class="glista right">\<span class="ges-left"></span>\<span class="ges-right"></span>\</span>\
				<span class="btn_line_move btn-line bot right">max</span>\
			</div>';
			aleMax.printModul = print_modul(Object.assign(aleMax,{name:'Верхний порог '+G.helper(HWD.helpText.equp.btnHelpPorogUp),col:'#ff7778',clas:'alert_max_predel'}))
		}
		
		let specClassAlert = '';
		if(!warMax.print && !aleMax.print) {specClassAlert = 'noMax'}
		
		s = '\
		<div class="'+clas+'">\
			<div class="title"><h4>Пороги</h4></div>\
			<div class="ltft"></div>\
			<div class="title">\
				<div id="'+id+'" class="predel-scale" data-min="'+min+'" data-max="'+max+'" data-fcn="'+fcn+'">\
					<section>\
						<div class="number-temp">'+numberStep+'</div>\
						<div class="scale-temp '+specClassAlert+'">\
							<span class="shtrih select-none">'+shtrih+'</span>\
							<span class="dop-left"></span>\
							<span class="dop-right"></span>'+
							warMin.print+
							aleMin.print+
							warMax.print+
							aleMax.print+
						'</div>\
					</section>\
					<section class="btn-controllers">'+
					'<div class="ltft"></div>'+
					aleMin.printModul+
					warMin.printModul+
					warMax.printModul+
					aleMax.printModul+
					'</section>\
				</div>\
			</div>\
		</div>\
		';
		return s;
		
		function print_modul(data){ data = data || {};
			let act = data.act || 'norm';
			let name = data.name || 'none';
			let title = data.title || 'none';
			let active =  data.active || '';
			//let value = data.val.toFixed(1) || 0.0;
			let value = data.val || 0;
			let color = data.col || '#fff';
			let clas = data.clas || '';
			let gestLeftVal = data.glv || 0;
			let gestRightVal = data.grv || 0;
			let gestLeftDelayVal = data.gldv || 0;
			let gestRightDelayVal = data.grdv || 0;
			let print = '';
			
			if(act == 'onlyVal'){
				let spMin = min;
				if(min === 0) {spMin = 'null'}
				
				print+='<div class="one-limit active '+clas+'">';
				
				print += '<div class="name-limit">'+
					'<span>'+name+'</span>'+
				'</div>';
				
				print += '<div class="value-limit">'+
					'<div class="min-max">'+
					G.btn.numb(value,{clas:'main-value',clase:'btn_int_minusplus',clasinp:'inp_main_val',min:spMin,max:max})+'\
					</div>'+
				'</div>';
				
				print += '</div>';
			}
			else{
				print+='<div class="one-limit '+active+' '+clas+'">';
				
				print += '<div class="name-limit flex_between">'+
					'<span>'+name+'</span>'+
					'<span class="btn-active on-off '+active+' active_limit"></span>'+
				'</div>';
				
				print += '<div class="value-limit flex_between">'+
					'<div class="min-max">\
						<div class="btn-int main-value">\
							<span class="btn_int_minusplus" data-tofix="1" data-cont="-">-</span>\
							<input class="inp_main_val" type="text" value="'+value+'">\
							<span class="btn_int_minusplus" data-tofix="1" data-cont="+">+</span>\
						</div>\
					</div>'+
					'<div class="icon icon-gear-open pointer hover open_dop_sets"></div>'+
				'</div>';
				
				print += '<div class="dop-sets-limit">'+
					'<div class="dop-sets-one-limit flex_between">'+
					'<p>Гистерезис</p>'+
					'<div class="min-max">'+
						'<div class="btn-int">\
							<span class="btn_int_minusplus" data-tofix="1" data-step="0.1" data-cont="-">-</span>\
							<input class="" type="text" value="'+gestLeftVal+'">\
							<span class="btn_int_minusplus" data-tofix="1" data-step="0.1" data-cont="+">+</span>\
						</div>\
						<span class="mini-info">Левый</span>'+
					'</div>'+
					'<div class="min-max">'+
						'<div class="btn-int">\
							<span class="btn_int_minusplus" data-tofix="1" data-step="0.1" data-cont="-" >-</span>\
							<input class="" type="text" value="'+gestRightVal+'">\
							<span class="btn_int_minusplus" data-tofix="1" data-step="0.1" data-cont="+">+</span>\
						</div>\
						<span class="mini-info">Правый</span>'+
					'</div></div>'+
					'<div class="dop-sets-one-limit flex_between">'+
					'<p>Задержка сек.</p>'+
					'<div class="min-max">'+
						'<div class="btn-int">\
							<span class="btn_int_minusplus" data-tofix="0" data-cont="-">-</span>\
							<input class="" type="text" value="'+gestLeftDelayVal+'">\
							<span class="btn_int_minusplus" data-tofix="0" data-cont="+">+</span>\
						</div>\
						<span class="mini-info">Левый</span>'+
					'</div>'+
					'<div class="min-max">'+
						'<div class="btn-int">\
							<span class="btn_int_minusplus" data-tofix="0" data-cont="-">-</span>\
							<input class="" type="text" value="'+gestRightDelayVal+'">\
							<span class="btn_int_minusplus" data-tofix="0" data-cont="+">+</span>\
						</div>\
						<span class="mini-info">Правый</span>'+
					'</div></div>'+
					'<div class="dop-sets-one-limit flex_between">'+
						'<p>Своё название</p>'+
						'<input class="predel-limit-dopname" type="text" placeholder="Своё" value="'+title+'">'+
					'</div>'+
				'</div>';
				
				print += '<div class="spec-color" style="background:'+color+'"><span></span></div>';
				print += '</div>';
			}
			return print;
		}
	}
	
	//Преобразует строку истории G.templateStr
	this.historiString = function(str,data){ 
		let spData = {},s = '';
		let up = '<b>', du = "</b>";
		for (let key in data){
			if(typeof data[key] == 'object'){
				spData[key] = data[key].name || data[key].val;
				if(data[key].unit){spData[key] += ''+ HWD.unit[data[key].unit];}
			}
			else {spData[key] = data[key]}
		}
		s = G.templateStr(spData,str,{up:up,du:du});
		return s;
	}
	
	//Шаблоны для страници оборудования
	this.equp = new function(){
		this.top = function(data){
			let pover = '<div class="icon icon-battery icon-p1"></div>';
			let connect = '<div class="icon icon-gsm"></div>';
			let ident = '', chanel='',addr='',port='',cell='',meterage='';
			let onof = data.onof || '';
			let template = data.template || 'on';
			let icon = data.icon || '0';
			let nameChange = data.nameChange || true;
			let blambaColor = data.blambaColor || false;
			let name = '',redactNameBtn = '',system = '';
			let bot = data.dataBot || '';
			data.stateClass = data.stateClass || ''
			data.dataClass = data.dataClass || '';
			data.dataColor = data.dataColor || '';
			
			if(data.power){pover = '<div class="icon icon-battery-'+data.power+'"></div>'}
			else {pover = ''}
			
			if(data.connect){connect = '<div>'+data.connect+'</div>'}
			else {connect = ''}
			
			if(data.ident){ident = '<p><span class="tc_black">id:</span> <span class="tc_black tf_fr500">'+data.ident+'</span></p>'}
			else {ident = ''}
			
			data.chanel = parseInt(data.chanel);
			if(data.chanel){chanel = '<p><span class="tc_black">Канал:</span> <span class="tc_black tf_fr500">'+data.chanel+'</span></p>'}
			else {chanel = ''}
			
			if(data.addr){addr = '<p><span class="tc_black">Адрес:</span> <span class="tc_black tf_fr500">'+data.addr+'</span></p>'}
			else {addr = ''}			
			
			if(data.port){port = '<p><span class="tc_black">Порт:</span> <span class="tc_black tf_fr500">'+data.port+'</span></p>'}
			else {port = ''}			
			
			if(data.cell){cell = '<p><span class="tc_black">Ячейка:</span> <span class="tc_black tf_fr500">'+data.cell+'</span></p>'}
			else {cell = ''}
			
			if(data.meterage){meterage = '<span>'+data.meterage+'</span>'}
			
			if(onof && onof.act){
				let onofBlocked = data.onofBlocked || false;
				let onoShadow = data.onoShadow || false;
				let activ = (onof.val == 1)?'active':'';
				if(onofBlocked) {onofBlocked = 'blocked'}
				if(onoShadow) {onoShadow = 'block-onof'}
				onof = '\
					<div class="'+onoShadow+' block_w_80p ta_center">\
						<div class="'+onofBlocked+' btn-active btn_onof_relay on-off on-off-b '+activ+'"></div>\
					</div>';
			}
			
			if(data.systemName || data.systemCN){
				system = '\
					<p>Принадлежит:</p>\
					<p><span id="name-system" class="tf_fr500">'+(data.systemName || '')+'</span></p>\
					<p><span class="tf_fr500">'+(data.systemCN || '')+'</span></p>\
				';
			}
			
			if(nameChange != 'of'){
				name =  '<input class="equp_name input-name-page" type="text" value="'+data.name+'" maxlength="16">';
				redactNameBtn = '<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>';
			}
			else {name =  '<span class="equp_name input-name-page">'+data.name+'</span>';}
			if(blambaColor) {blambaColor = 'background:'+blambaColor} else {blambaColor = '';}
			if(data.dataColor) {data.dataColor = 'color:'+data.dataColor} else {data.dataColor = '';}
			
			let s = '<div class="one-block ob-top page-equp-top">\
				<div class="name-page">\
					<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
					'+name+redactNameBtn+'\
				</div>\
				<div class="info-li block_w_100 flex_center_left">\
					<div id="blamba" class="b'+data.stateClass+'" style="'+blambaColor+'"></div>\
					<div class="page-top-dop-info block-title-name1colum">\
						<div class="data tt_capitalize '+data.dataClass+'" style="'+data.dataColor+'"><inf>'+data.data+'</inf>'+meterage+'</div>\
						'+onof+'\
					</div>\
					<div class="page-top-dop-info-spec block-title-colum">\
						'+port+addr+chanel+cell+ident+connect+pover+'\
					</div>\
					<div class="page-top-dop-info-title">\
						'+system+'\
					</div>\
					<div id="iconr" class="flex_center_left">\
						<span class="block_w_150p block_pr_10">'+data.title+'</span>\
						<span class="icon p35 icon_equip_'+icon+'"></span>\
					</div>\
				</div>\
				<div id="top-dop-info-page-bottom" class="block_w_100 flex_center_left">'+bot+'</div>\
			</div>\
			';
			return s;
		}
		
		this.sets = function(data){
			let onof = '';
			let journal = data.journal; if(journal){journal = 'active';}
			let norma = data.norma || '';
			let aler = data.alert || '';
			let period = data.period || '';
			let ontime = data.ontime || '';
			let custom = data.custom || '';
			let correction = '', delayA = '', delayN = '';
			let template = data.template || '';

			if (norma != ''){
				if(typeof norma != 'object'){norma={val:norma}; norma.title = 'Норма';}
				norma = '<div class="cn-block tc_green">\
					<p>'+norma.title+':</p>\
					<div><input class="tc_green btn_state_text_norm" type="text" value="'+norma.val+'" placeholder="Своё"></div>\
				</div>';
			}
			if (aler != ''){
				if(typeof aler != 'object'){aler={val:aler}; aler.title = 'Тревога';}
				aler = '<div class="cn-block tc_red">\
					<p>'+aler.title+':</p>\
					<div><input class="tc_red btn_state_text_aler" type="text" value="'+aler.val+'" placeholder="Своё"></div>\
				</div>';
			}
			if (period != ''){
				period = '<div class="cn-block delay-time">\
					<p>Период опроса</p>\
					<div><input type="text" value="'+period+'"> '+G.select(['сек'])+'</div>\
				</div>'
			}
			if(ontime != '' && ontime.act){
				if (ontime.val == 0){ontime.val = '';}
				ontime = '<div class="cn-block delay-time">\
					<p>Включать на:</p>\
					<div><input class="delay_time_ontime" type="text" value="'+ontime.val+'" placeholder="Постоянно"> '+G.select(['сек'])+'</div>\
				</div>';
			} else {ontime = '';}
			if(custom != '' && custom.act){
				let selectCustom = 	{
					0:HWD.unit[0],
					1:HWD.unit[1],
					2:HWD.unit[2],
					3:HWD.unit[3],
					4:HWD.unit[4],
					5:HWD.unit[5],
					6:HWD.unit[6],
					14:HWD.unit[14],
					15:HWD.unit[15],
					16:HWD.unit[16]
				}
				selectCustom[custom.unit] = {name:HWD.unit[custom.unit],selected:'selected'}
				custom = '<div class="cn-block">'+
					'<div class="display_inline ta_center block_pr_8">'+
						'</p>Мин:<p>'+
						'<input class="block_w_40p ta_center" type="text" value="'+custom.min+'" placeholder="Мин">'+
					'</div>'+
					'<div class="display_inline ta_center block_pr_8">'+
						'</p>Макс:<p>'+
						'<input class="block_w_40p ta_center" type="text" value="'+custom.max+'" placeholder="Макс">'+
					'</div>'+
					'<div class="display_inline ta_center">'+
						'</p>Ед:<p>'+
						G.select(selectCustom)+
					'</div>'+
				'</div>';
			}else {custom = ''}
			
			if(HWF.objKeyTest('delayA',data)){
				delayA = data.delayA;
				delayA = '<div class="cn-block delay-time-delayA">\
					<p>Задержка при <span class="tc_red">тревоге:</span></p>\
					<div>'+G.btn.numb(delayA,{fcn:'clickDelayAlarmSensor'})+'  сек</div>\
				</div>';
			} else {delayA = '';}
			
			if(HWF.objKeyTest('delayN',data)){
				delayN = data.delayN;
				delayN = '<div class="cn-block delay-time-delayA">\
					<p>Задержка при <span class="tc_green">норме:</span></p>\
					<div>'+G.btn.numb(delayN,{fcn:'clickDelayNormSensor'})+' сек</div>\
				</div>';
			} else {delayN = '';}
			
			if(HWF.objKeyTest('correction',data)){
				correction = data.correction;
				correction = '<div class="cn-block delay-correction">\
					<p>Поправка:</p>\
					<div>'+G.btn.numb(data.correction,{fcn:'clickPravkaSensor'})+' °C</div>\
				</div>';
			} else {correction = '';}
			
			if(HWF.objKeyTest('onof',data)){
				onof = data.onof; if(onof){onof = 'active';}
				onof = '<div class="cn-block cn-center">'+
					'<p>Активен</p>'+
					'<span id="equp-onof" class="btn-active on-off btn_active_inactive_equp '+onof+'" data-targetplus="warning-min-max"></span>'+
				'</div>';
			} else {onof = '';}
			
			let s = '\
			<div class="one-block block-setings">\
				<h4>Глобальные:</h4>'+
				onof+
				'<div class="cn-block cn-center">\
					<p>Журнал</p>\
					<span id="equp-jurnal" class="equp_jurnal btn-active on-off '+journal+'" data-targetplus="warning-min-max"></span>\
				</div>'+
				period+custom+ontime+norma+aler+correction+delayA+delayN+
				'<div class="block_w_100">'+template+'</div>'+
			'</div>';
			return s;
		}
		
		this.history = function(data,typ){ typ = typ || 'norm';
			let s = '',sl = '';
			data.list.forEach(function(elem){
				let time = HWF.unixTime(elem.timestamp);
				time = time.fullDate+' - '+time.fullTime;
				let events = HWD.events[elem.event_id] || '';
				sl+= '<p class="block_m_8"><i class="tc_grey">'+time+' : </i>'+G.historiString(events,elem.data)+'</p>';
			});
			if(typ == 'list'){s+= sl;}
			else {s+= '<div class="tc_grey8 history-list">'; s+= sl; s+= '</div>';}
			return s;
		}
			
		this.graph = function(H,fcn,fcnL,Dclass,dopHtmp){ dopHtmp = dopHtmp || '';
			let s = '<div id="equp-statistic-graf" class="one-block">'+G.timeControler(H,fcn,fcnL);
			s += '<div class="graph-windiw-statistic '+Dclass+'"> '+G.loader()+' </div>';
			s += dopHtmp;		
			s += '</div>';		
			return s;
		}
		
		this.pravka = function(data){
			let s = '<div id="equp-correctios-graf" class="one-block one-segment-50 ob-top mini_grahp">';
			let pravkaUl = '<ul><li class="tw_700">'+
				'<div><b>Имя</b></div>'+
				'<div class="tc_green"><p>°C</p></div>'+
				'<div class="tc_orange"><p>∆</p></div>'+
			'<div></div></li>';
			
			data.forEach(function(el){
				pravkaUl += '<li>'+
					'<div class="name_control block_pl_10"> '+el.num+' </div>'+
					'<div class="tc_green"><input class="tc_green" type="text" value="'+el.val+'"></div>'+
					'<div class="tc_orange"><input class="tc_orange" type="text" value="'+el.cor+'"></div>'+
					'<div class="btn_clouse block_pr_10">✕</div>'+
				'</li>';
			});
			
			s += '<h4 class="flex_between">'+
				'<span><span class="text-icon dop-but-add">+</span>Правка: </span>'+
				'<span class="icon icon-full-width hover pointer" title="Раскрыть на всю шерену"></span>'+
			'</h4>';
			s += '<div class="graph-windiw-modding"><canvas class="graf-control"></div>';
			s += pravkaUl+'</ul>';
			s += '</div>';
			return s;
		}
		
		this.openTherm = function(elem){
			let cfg = {};
			cfg.maxModBurner = elem.config.modulation_max; // Макс. модуляция горелки
			cfg.heating = elem.config.heater_cuircuit_liquid.flags.cuircuit_1_enable; // Отопление
			cfg.heating2 = elem.config.heater_cuircuit_liquid.flags.cuircuit_2_enable; // Второй контур отопления
			cfg.perCoolTemp = [ //Допустимая температура теплоносителя
				elem.config.heater_cuircuit_liquid.temperature_limits.min,
				elem.config.heater_cuircuit_liquid.temperature_limits.max
			];
			cfg.perCoolPressure = [//Допустимое давление теплоносителя 
				1.5,3.5
			];
			cfg.crashTemp = elem.config.heater_cuircuit_liquid.crash_temperature; // Температура контура отопления при аварии датчиков
			cfg.gvs = elem.config.domestic_hot_water[0].flags.enable; // ОТКЛ/ВКЛ (ГВС)
			cfg.tempLimit = [//Допустимая температура горячей воды (ГВС)
				elem.config.domestic_hot_water[0].temperature_limits.min,
				elem.config.domestic_hot_water[0].temperature_limits.max
			];
			cfg.hotWater = elem.config.domestic_hot_water[0].desired_temperature // Установка температуры горячей воды (ГВС)
			cfg.pzaGvs = elem.config.heater_cuircuit_liquid.wa_curve // Погодозависимая автоматика (ПЗА) (ГВС)
			
			let s = '<div class="one-block ob-top">';
			s += '<div class="block_w_100 flex_between">'+
				'<div>Марка и модель котла '+G.select(['Baxi'])+'</div>'+
				'</div>';
			s += G.btn.scrol(cfg.maxModBurner,{name:'Макс. модуляция горелки',tofix:0}); // modulation_max
			s += '</div>';
				
			s += '<div class="one-block ob-top">';
			s += '<h4>Параметры отопления</h4>';
			s += '<div class="block_w_100 flex_between">'+
				'<div class="cn-block cn-center"><p>Отопление</p>'+G.btn.onof(cfg.heating)+'</div>'+
				'<div class="cn-block cn-center"><p>Второй контур</p>'+G.btn.onof(cfg.heating2)+'</div>'+
			'</div>';
			s += '<div class="block_w_100 block_pt_30"></div>';
			s += G.btn.scrolDuo(cfg.perCoolTemp,{max:80,step:8,name:'Допустимая температура теплоносителя'});
			s += '<div class="block_w_100 block_pt_30"></div>';
			s += G.btn.scrolDuo(cfg.perCoolPressure,{max:5,min:1,step:8,name:'Допустимое давление теплоносителя',tofix:1}); // none
			s += '<div class="block_w_100 block_pt_30"></div>';
			s += G.btn.scrol(cfg.crashTemp,{max:80,step:8,name:'Температура контура отопления при аварии датчиков'}); 
			s += '</div>';
			
			s += '<div class="one-block ob-top">';
			s += '<h4>Горячее водоснабжение (ГВС)</h4>';
			s += '<div class="block_w_100 flex_between">'+
				'<div class="cn-block cn-center"><p> Откл/Вкл (ГВС)</p>'+G.btn.onof(cfg.gvs)+'</div>'+ 
				'</div>';
			s += G.btn.scrolDuo(cfg.tempLimit,{max:80,step:8,name:'Допустимая температура горячей воды (ГВС)'});
			s += '<div class="block_w_100 block_pt_30"></div>';
			s += G.btn.scrol(cfg.hotWater,{max:80,step:8,name:'Установка температуры горячей воды (ГВС)'});
			s += '</div>';
			
			s += '<div class="one-block ob-top">';
			s += '<h4>Погодозависимая автоматика (ПЗА) (ГВС)</h4>';
			//s += ''+G.select([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],)+''; //heater_cuircuit_liquid wa_curve
			s += '<div class="block_w_100"><object data-selel="'+cfg.pzaGvs+'" data="img/wac.svg" id="wac" onload="HWequip.openThermInitMap(this)" type="image/svg+xml"></object></div>';
			s += '</div>';
			return s;
		}
		this.system3 = function(elem,type){
			let s = {},di = {};
			
			function print_gsmwifi_icon(elemWG){
				let type = elemWG.type;
				let name = elemWG.title;
				let signal = elemWG.level;
				let clasStatus = elemWG.use_inet;
				let roam = (elemWG.roam)?'-roam':'';
				
				let helperText = '';
				if(clasStatus == 'connect'){clasStatus = 'green';}
				if(clasStatus == 'disconnect'){
					clasStatus = 'red';
					helperText = '<span class="btn_text grey btn_problem_conection" data-type="'+type+'">Проблема?</span>'
				}
				if(clasStatus == 'not_use'){clasStatus = 'of';}
			
				let print = '<p class="'+type+' flex_center_left">\
					<span class="width-45 vertical-middle">\
						<span class="select-none position_relative block_mr_10">\
							<span class="spec-circle-icon-state '+clasStatus+'"></span>\
							<span class="icon icon-'+type+roam+'-'+signal+'"></span>\
						</span>\
					</span>\
					<span class="name vertical-middle tf_fr500">'+(name || 'Отсутствует')+'</span>\
					'+helperText+'\
				</p>';
					
				return print;
			};
			
			di.Sdate = elem.info.info_vermain || '';
			di.Rdate = elem.info.info_verrf || '';
			
			di.wifi ={};
			di.wifi.m = elem.mod_wifi;
			if (JSON.stringify(di.wifi.m) != "{}"){
				di.wifi.state = di.wifi.m.state;
				di.wifi.name = di.wifi.m.config.cfg_wifi_ssid || 'Отсутствует';
				di.wifi.state_n = '';
				di.wifi.icon = di.wifi.m.state.signal_level || '0';
				di.wifi.helper = '';
				di.wifi.list = '';
				di.wifi.conectStr = '';
			} else {di.wifi = false;}
			
			di.gsm ={};
			di.gsm.m = elem.mod_gsm;
			di.gsm.name = di.gsm.m.info.info_operator || 'Отсутствует';
			di.gsm.state_n = '';
			di.gsm.rouming = (elem.state.roaming_status)?'R':'';
			di.gsm.icon = di.gsm.m.state.signal_level || '0';
			di.gsm.helper = '';
			di.gsm.ussdVal = di.gsm.m.config.cfg_balussd || '';
			di.gsm.ussdOnOf = '';
			di.gsm.ussd = {'100':'','102':'','105':'','custom':'selected'};
			di.gsm.info = di.gsm.m.info.info_bal || '';
			di.gsm.phone = di.gsm.m.config.phone_number;
			di.gsm.imei = ''; di.gsm.imeiTitl = '';
			if(elem.cloud_type == 14723){
				di.gsm.imeiTitl = '<br><p class="block_ptb_8">IMEI '+HWhtml.helper(HWD.helpText.equp.systemGsmIMEI)+'</p><br>';
				di.gsm.imei = '<br><p class="block_ptb_8">'+elem.info.imei+'</p><br>';
			}
			
			di.wc = 'block-onof';
			di.lastSysDateT = HWF.unixTime(elem.system_connection_time,true);
			di.lastSysDateText = elem.system_connection_delta || '';
			di.lastSysDateTime = di.lastSysDateT.hh+':'+di.lastSysDateT.mm+':'+di.lastSysDateT.ss;
			di.lastSysDate = di.lastSysDateT.fullDate;
			//console.log(di.lastSysDateT);
			
			di.sysDateF = HWF.unixTime(elem.info.info_time*1000);
			di.sysDate = di.sysDateF.dd+'.'+di.sysDateF.mt+'.'+di.sysDateF.yyyy;
			di.sysTimeH = di.sysDateF.hh;
			di.sysTimeM = di.sysDateF.mm;
			
			di.radiobtn = (elem.config.cfg_radio == 1)?'active':'';
			di.dopbtn = (elem.config.cfg_ext == 1)?'active':'';
			di.microbtn = (di.gsm.m.config.cfg_mphone == 1)?'active':'';
			di.alertbtn = (elem.config.cfg_button == 1)?'active':'';
			di.favorite = (elem.lk.favorite)?'active':'';
			
			di.cfgSelect = {'0':'','1':'','2':'','3':'','4':''}
			di.cfgSelect[elem.config.cfg_lk] = 'selected';

			// список wi-fi
			let specList = [];
			if(di.wifi){
				if(di.wifi.m.info.wifi_list){ for (let key in di.wifi.m.info.wifi_list) {
					specList.push({name:di.wifi.m.info.wifi_list[key].ssid,val:di.wifi.m.info.wifi_list[key].ssid});}
				}
				else if(di.wifi.m.config.cfg_wifi_ssid) {specList.push({name:di.wifi.m.config.cfg_wifi_ssid,val:di.wifi.m.config.cfg_wifi_ssid});}
				for (let key in specList) {if(specList[key].name == di.wifi.m.config.cfg_wifi_ssid) {specList[key].selected = 'selected';}}
				di.wifi.list = HWhtml.select(specList,'btn_select_wifi_networck');
			}
			
			if (di.gsm.ussdVal == '*105#') {di.gsm.ussd['105'] = 'selected'; di.gsm.ussd.custom = '';}
			if (di.gsm.ussdVal == '*102#') {di.gsm.ussd['102'] = 'selected'; di.gsm.ussd.custom = '';}
			if (di.gsm.ussdVal == '*100#') {di.gsm.ussd['100'] = 'selected'; di.gsm.ussd.custom = '';}
			
			if (di.gsm.ussd.custom == 'selected'){di.gsm.ussdOnOf = 'open'}
			
			let systemConnection = elem.system_connection;
			let printIcon = '';
			HWF.recount(systemConnection,function(eleC,key){
				let mesage1 = eleC.details || false;
				let discript = eleC.common_description || false;
				if (mesage1 && typeof mesage1 == 'string'){let spAr = []; spAr.push(mesage1); mesage1 = spAr}
				if (discript && typeof discript == 'string'){let spAr = []; spAr.push(discript); discript = spAr}
				
				if(di.wifi && eleC.type == "wifi"){
					HWS.buffer.mesageErrocConectionWIFImes = mesage1;
					HWS.buffer.mesageErrocConectionWIFIdes = discript;
					printIcon+= print_gsmwifi_icon(eleC);
					di.wifi.conectStr = eleC.use_inet_str || '';
					s.wifiUse = (eleC.use_inet == 'connect')?true:false;
				}
				if(eleC.type == "gsm"){
					HWS.buffer.mesageErrocConectionGSMmes = mesage1;
					HWS.buffer.mesageErrocConectionGSMdes = discript
					printIcon+= print_gsmwifi_icon(eleC);
					s.gsmUse = true;//(eleC.use_inet != 'not_use')?true:false;
				}	
				if(key == 0){printIcon+='<br>'}
			});
			
			
			if(type == 'top'){
				di.name = elem.config.name || elem.info.name || '';
				di.et = HWD.equp[elem.cloud_type];
				di.ver = di.et.title;
				di.imei = elem.info.ident;
				di.systemConect = elem.lk.state_name || '';
				di.systemBlamba = HWF.getStateColor(elem.lk.state);
				
				s.header = '\
				<div class="one-block ob-top page-equp-top">\
					<div class="name-page">\
						<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
						<input class="equp_name input-name-page" type="text" value="'+di.name+'"  maxlength="16">\
						<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
					</div>\
					<div class="info-li block_w_100 flex_center_left">\
						<div id="blamba" style="background:'+di.systemBlamba+'"></div>\
						<div id="iconr"><span class="icon x2 icon_equip_'+elem.cloud_type+'"></span></div>\
						<div id="top-info-block" class="page-top-dop-info block-title-name1colum">\
							<div class="data"><inf>'+di.systemConect+'</inf></div>\
						</div>\
						<div class="page-top-dop-info-spec block-title-colum">\
							<p><span class="tc_black">С/н: </span> <span class="tc_black tf_fr500">'+di.imei+'</span></p>\
						</div>\
						<div class="page-top-dop-info-title block-title-colum tf_fr500">\
							Система '+di.ver+'\
						</div>\
					</div>\
				</div>\
				';
								
				s.header+= '\
				<div class="one-block-col">\
					<div class="title"><h4>Связь '+HWhtml.helper(HWD.helpText.equp.systemConnection)+'</div>\
					<div class="ltft"></div>\
					<div class="right">\
						<div id="info-connections-wifi-gsm" class="colum pec-info-block">\
							'+printIcon+'\
						</div>\
						<div class="colum"></div>\
						<div class="colum">\
							<p>Последний сеанс связи</p>\
							<br>\
							<p>\
								<span id="top-info-time-sis-text"  class="tf_fr500">'+di.lastSysDateText+', </span>\
								<span id="top-info-time-sis" class="tf_fr500">'+di.lastSysDate+' в '+di.lastSysDateTime+'</span>\
							</p>\
						</div>\
					</div>\
				</div>';
				
				s.print = s.header;
			}
			
			else if(type == 'sets'){
				s.network = '';
				
				s.sets = '<div class="one-block-col">'+
					'<div class="title"><h4>Настройки</h4></div>'+
					'<div class="ltft"></div>'+
					'<div class="right">'+
						'<div id="system-radio-control" class="mcolum">'+
							'<p>Радиосеть '+HWhtml.helper(HWD.helpText.equp.systemRadioBtn)+'</p>'+
							'<span class="on-off btn-active btn_radio_system_onof '+di.radiobtn+'" data-targetplus="spec-info-block"></span>'+
						'</div>'+ 
						'<div id="system-port-dop" class="mcolum">'+
							'<p>ДОП '+HWhtml.helper(HWD.helpText.equp.systemDOPBtn)+'</p>'+
							'<span class="on-off btn-active btn_portdop_onof '+di.dopbtn+'" data-targetplus="spec-info-block"></span>'+
						'</div>'+ 
						'<div id="system-microphone" class="mcolum">'+
							'<p>Микрофон '+HWhtml.helper(HWD.helpText.equp.systemMicrophoneBtn)+'</p>'+
							'<span class="on-off btn-active btn_microphone_system_onof '+di.microbtn+'" data-targetplus="spec-info-block"></span>'+
						'</div>'+ 
						'<div id="system-alert-btn" class="mcolum">'+
							'<p>Тревожная кнопка '+HWhtml.helper(HWD.helpText.equp.systemAlertBtn)+'</p>'+
							'<span class="on-off btn-active btn_warninr_but_onof '+di.alertbtn+'" data-targetplus="spec-info-block"></span>'+
						'</div>'+
						'<div class="colum">'+
							'<p>В виджеты '+HWhtml.helper(HWD.helpText.equp.btnHelpVviget)+'</p>'+
							'<span id="equp-viget" class="equp_viget btn-active on-off '+di.favorite+'" ></span>'+
						'</div>'+
					'</div>'+
				'</div>';
				
				s.wifi = '<div class="one-block-col">'+
					'<div class="title"><h4>Wi-Fi '+HWhtml.helper(HWD.helpText.equp.systemWiFi)+'</h4></div>'+
					'<div class="ltft"></div>'+
					'<div class="right">'+
						'<div class="mcolum">'+
							'<p class="block_ptb_8">Сеть</p>'+
							'<br>'+
							'<p class="block_ptb_8">Пароль</p>'+
							'<br>'+
							'<p class="block_ptb_8">Состояние</p>'+
						'</div>'+
						'<div id="system-wifi-control" class="columx2">'+
							'<p>'+
								di.wifi.list+
								'<span class="block_p_4"></span>'+
								G.miniLoader('mini_load_wifi_rebout vertical-middle block-onof')+
								'<span class="btn_update_wifi_list btn_text grey block_ptb_8 block_ml_8" title="Обновит список доступных сетей для системы">Обновить список сетей</span>'+
							'</p>'+
							'<br>'+
							'<p><span class="spec-password">\
								<input class="pass-wifi" type="password" placeholder="пароль сети" maxlength="24" required>\
								<span class="btn_view_pass icon hover_orange icon-read"></span>\
							</span></p>'+
							'<br>'+
							'<p class="block_p_6 wifi-status-sistem">'+di.wifi.conectStr+'</p>'+ 
						'</div>'+
						'<div class="block_w_100 ta_right">'+
							'<span class="style_btn grey btn_acept_wifi_list">Сохранить настройки</span>'+
						'</div>'+
					'</div>'+
				'</div>';
				if(!di.wifi){s.wifi = '';}
				
				s.gsm = '<div class="one-block-col">'+
					'<div class="title"><h4>GSM</h4></div>'+
					'<div class="ltft"></div>'+
					'<div id="system-gsm-control" class="right">'+
						'<div class="mcolum">'+
							di.gsm.imeiTitl+
							'<p class="block_ptb_8">Телефон '+HWhtml.helper(HWD.helpText.equp.systemTelHelp)+'</p>'+
							'<br>'+
							'<p class="block_ptb_8">Баланс '+HWhtml.helper(HWD.helpText.equp.systemBalanceHelp)+'</p>'+
							'<p class="block_ptb_8 custom_text_query open-clouse '+di.gsm.ussdOnOf+'"><br>Запрос</p>'+
						'</div>'+
						'<div class="columx2">'+
							di.gsm.imei+
							'<div>'+
								'<span class="plus-from-input-tel">+</span>'+
								'<input type="text" value="'+di.gsm.phone+'" class="tel inp_only_numb btn_save_new_number" placeholder="7 (000) 000 00 00" maxlength="12">'+
							'</div>'+
							'<br>'+
							'<p>'+
								'<select size="1" class="btn_cange_text_get_balance">'+
									'<option '+di.gsm.ussd['105']+' value="*105#" data-of="#user-get-balanc-value">*105# (Tele2)</option>'+
									'<option '+di.gsm.ussd['102']+' value="*102#" data-of="#user-get-balanc-value">*102# (Beeline)</option>'+
									'<option '+di.gsm.ussd['100']+' value="*100#" data-of="#user-get-balanc-value">*100# (Megafon/MTC)</option>'+
									'<option '+di.gsm.ussd['custom']+' class="custom_user_balanc" value="custom" data-on="#user-get-balanc-value">Своё</option>'+
								'</select>'+
								'<span class="block_p_4"></span>'+
								G.miniLoader('mini_load_gsm_rebout vertical-middle block-onof')+
								'<span class="btn_get_new_balance btn_text grey block_ptb_8 block_ml_8" title="Обновить баланс">Обновить баланс</span>'+
							'</p>'+
							'<p class="open-clouse '+di.gsm.ussdOnOf+' custom_val_query">'+
								'<br>'+
								'<input type="text" id="custom-get-balance-system" class="custom_get_balance_system" value="'+di.gsm.ussdVal+'" placeholder="своё значение">'+
								'<span class="block_p_4"></span>'+
								G.miniLoader('mini_load_gsm_custom vertical-middle block-onof')+
							'</p>'+
						'</div>'+
						'<div class="mcolum">'+
							'<p class="">Последнее сообщение</p>'+
							'<br>'+
							'<p id="gsm-last-sms-text" class="tf_fr500">'+di.gsm.info+'</p>'+
						'</div>'+
					'</div>'+
				'</div>';
				
				s.lkList = '<option '+di.cfgSelect['0']+' value="0">ЛК отключен</option>'+
					'<option '+di.cfgSelect['1']+' value="1">только GSM</option>'+
					'<option '+di.cfgSelect['2']+' value="2">только Wi-Fi</option>'+
					'<option '+di.cfgSelect['3']+' value="3">GSM - основной, WiFi - резервный</option>'+
					'<option '+di.cfgSelect['4']+' value="4">WiFi - основной, GSM - резервный</option>';
					
				if(!di.wifi){s.lkList = '<option '+di.cfgSelect['0']+' value="0">ЛК отключен</option>'+
					'<option '+di.cfgSelect['1']+' value="1">только GSM</option>';}
				
				s.lk = '<div class="one-block-col">'+
					'<div class="title"><h4>Доступ в ЛК '+HWhtml.helper(HWD.helpText.equp.systemAccess)+'</h4></div>'+
					'<div class="ltft"></div>'+
					'<div class="right">'+
						'<div class="mcolum">'+
							'<p class="block_ptb_8">Приоритет доступа</p>'+
						'</div>'+
						'<div id="system-priority-control" class="columx2">'+
							'<p>'+
								'<select size="1" class="btn_priority_connections">'+
								s.lkList+
								'</select>'+
							'</p>'+
						'</div>'+
					'</div>'+
				'</div>';
				
				s.timezone = '';
				
				if(elem.cloud_type == 14723 && elem.config.time_source_lk){
					let timezoneVal = elem.config.timezone || 0;
					let ontionPrint = '', controlDopOpt = false;
					let timezoneNTP = (elem.config.time_source_ntp == 1)?'active':'';
					let timezoneGSM = (elem.config.time_source_gsm == 1)?'active':'';
					HWF.recount(HWD.timezone,function(val,key){
						let activeOpt = '';
						if(parseInt(key) == timezoneVal) {activeOpt='selected'; controlDopOpt=true;}
						ontionPrint+='<option '+activeOpt+' value="'+key+'">'+val+'</option>';
					});
					
					if(!controlDopOpt){ontionPrint+= '<option selected value="'+timezoneVal+'">(UTC '+(timezoneVal/4)+')</option>';}
					
					s.timezone+= '<div class="block_w_100">';
					s.timezone+= '<p>Часовой пояс '+HWhtml.helper(HWD.helpText.equp.systemTimezone)+'</p><br>';
					s.timezone+= '<select size="1" class="btn_timezone_selector block_w_auto">';
					s.timezone+= ontionPrint;
					s.timezone+= '</select><br><br><br>';
					s.timezone+= '<div class="colum"><div class="block_pt_15">Автосинхронизация времени '+HWhtml.helper(HWD.helpText.equp.systemTimezoneAuto)+'</div></div>';
					s.timezone+='<div class="mcolum">'+
									'<p>NTP '+HWhtml.helper(HWD.helpText.equp.systemTimezoneNTP)+'</p>'+
									'<span class="on-off btn-active btn_timezone_ntp '+timezoneNTP+'" data-targetplus="spec-info-block"></span>'+
								'</div>';
					s.timezone+='<div class="mcolum">'+
									'<p>GSM '+HWhtml.helper(HWD.helpText.equp.systemTimezoneGSM)+'</p>'+
									'<span class="on-off btn-active btn_timezone_gsm '+timezoneGSM+'" data-targetplus="spec-info-block"></span>'+
								'</div>';
					s.timezone+= '</div>';
				}
				
				s.time = '<div class="one-block-col">'+
					'<div class="title"><h4>Дата и время '+HWhtml.helper(HWD.helpText.equp.systemDataAndTime)+'</h4></div>'+
					'<div class="ltft"></div>'+
					'<div id="system-date-time" class="right">'+
						'<div id="data-picket-target" class="mcolum">'+
							'<p class="">Дата</p>'+
							'<br>'+
							'<p class="selector-date">'+
								'<input disabled type="text" value="'+di.sysDate+'" class="tf_fr500 time_val_data block_w_120p" onClick="xCal(this,{top:80, left:1})">'+
								'<span class="icon icon-setdate select-none of"></span>'+
							'</p>'+
						'</div>'+
						'<div class="mcolum">'+
							'<p class="">Время</p>'+
							'<br>'+
							'<p class="">'+
								'<input disabled class="tf_fr500 inp_time_text_hh_mm" type="time" value="'+di.sysTimeH+':'+di.sysTimeM+'">'+
							'</p>'+	
						'</div>'+
						'<div class="mcolum">'+
							'<span class="btn_text grey canche_data_system_btn">Изменить</span>'+
						'</div>'+
						s.timezone+
					'</div>'+
				'</div>';
				
				s.po = '<div class="one-block-col">'+
					'<div class="title"><h4>Встроенное ПО '+HWhtml.helper(HWD.helpText.equp.systemBuiltInPO)+'</h4></div>'+
					'<div class="ltft"></div>'+
					'<div class="right">'+
						'<div class="colum">'+
							'<p class="">Основное ПО</p>'+
							'<br>'+
							'<p id="system-text-data" class="tf_fr500">'+di.Sdate+'</p>'+
						'</div>'+
						'<div class="colum">'+
							'<p class="">ПО радиосети</p>'+
							'<br>'+
							'<p id="system-text-po-radio" class="tf_fr500">'+di.Rdate+'</p>'+
						'</div>'+
						'<div class="colum">'+
							'<!--p class="">Доступна новая прошивка</p>'+
							'<br>'+
							'<p class="tf_fr500">____</p>'+
							'<br>'+
							'<p class="btn_text grey">Подробнее</p>'+
							'<br-->'+
							'<p class="btn_text grey btn_update_new_version_system">Обновить</p>'+
						'</div>'+
					'</div>'+
				'</div>';
				
				s.pass = '<div class="one-block-col">'+
					'<div class="title"><h4>Пароль '+HWhtml.helper(HWD.helpText.equp.systemPassword)+'</h4></div>'+
					'<div class="ltft"></div>'+
					'<div id="system-new-pasword" class="right">'+
						'<div class="mcolum">'+
							'<p class="block_ptb_8">Текущий</p>'+
							'<br>'+
							'<p class="block_ptb_8">Новый</p>'+
						'</div>'+
						'<div class="colum">'+
							'<p class=""><span class="spec-password">\
								<input class="last_pasword inp_only_numb" type="password" maxlength="4" minlength="4" placeholder="Старый пароль" required>\
								<span class="btn_view_pass icon hover_orange icon-read"></span>\
							</span></p>'+
							'<br>'+
							'<p class=""><span class="spec-password">\
								<input class="new_pasword inp_only_numb" type="password" maxlength="4" minlength="4" placeholder="Новый пароль" required>\
								<span class="btn_view_pass icon hover_orange icon-read"></span>\
							</span></p>'+
							'<span class="error_mesage tc_red"></span>'+
						'</div>'+
						'<div class="block_w_100 ta_right">'+
							'<span class="style_btn grey btn_cange_pasword_system">Сохранить настройки</span>'+
						'</div>'+
					'</div>'+
				'</div>';
				
				s.dop = '<div class="one-block-col">'+
					'<div class="title"></div>'+
					'<div class="ltft"></div>'+
					'<div id="system-new-pasword" class="right flex_between">'+
						'<div class="mcolum">'+
							'<span class="btn_text grey btn_rebout_sets_system">Сброс настроек</span> '+
							HWhtml.helper(HWD.helpText.equp.systemReboutPass)+
						'</div>'+
						'<div class="mcolum">'+
							'<span class="btn_text grey btn_reset_system">Перезагрузка</span> '+
							HWhtml.helper(HWD.helpText.equp.systemReboutStstem)+
						'</div>'+
						'<div class="mcolum">'+
							'<span class="btn_text grey btn_ust_new_equp_system">Добавить устройства</span> '+
							HWhtml.helper(HWD.helpText.equp.systemAddNewEqup)+
						'</div>'+						
						'<div class="mcolum">'+
							'<span class="btn_text grey btn_delete_equp">Удалить систему</span> '+
							HWhtml.helper(HWD.helpText.equp.systemDelete)+
						'</div>'+
					'</div>'+
				'</div>';
					
				s.print = s.network + s.sets + s.wifi + s.lk + s.gsm + s.time + s.po + s.pass + s.dop;
			}
			else if(type == 'graph'){
				s.timeGraph = HWS.buffer.timeValueGraph || 1;
				s.networkG = '<div class="one-block ob-top mobile-around">\
					'+G.timeControler(s.timeGraph,'timeControlFCNgraph','',{load:'of'})+'\
					<div id="block-graf-analog-gsm" class="block_w_100 mobile-100 equp-info-graph">\
						<header class="flex_between block_w_100">\
						<h5>Сигнал сети GSM</h5>\
						</header>\
						<div class="graph">'+G.loader()+'</div>\
					</div>';
					
				if(s.wifiUse){
					s.networkG+='\
					<div id="block-graf-analog-wifi" class="block_w_100 mobile-100 equp-info-graph">\
						<header class="flex_between block_w_100">\
						<h5>Сигнал сети Wi-Fi</h5>\
						</header>\
						<div class="graph">'+G.loader()+'</div>\
					</div>';
				}
				
				s.networkG+='</div>';
				
				s.print = s.networkG;
			}
			else {s.print = '';}
			
			
			return s.print;
		}
	}
	
	G.templateProg = function(elem) {
		let tmp = elem.tmp;
		let ver = elem.ver;
		let typ = elem.type;
		let S = {};
		S.print = '';
		// Отопление
		if (tmp == 'heating'){
			S.valS = elem.suport_val;
			S.pump = elem.pump;
			S.typeTemp = {}; S.typeTemp[elem.temperature.active] = 'active';
			S.dataTemp = elem.temperature.type;
			S.glist = elem.glist;
			S.SCtimetable = (elem.temperature.active == 'timetable')?'open':'';
			S.OpenTerm = elem.OpenTerm || false;
			S.invers = (elem.invers)?'active':'';
			
			if(S.OpenTerm){
				S.typeW = {}; S.typeW[elem.type_work] = 'selected';

				S.dprint = '<div class="block-w-100 block-pb-10 block-pt-10">'; 
				S.dprint += '<h4>Режим работы котла:</h4>'; 
				S.dprint += '<div class="elements-left block-w-100">\
					<select>\
					<option '+S.typeW[1]+' value="1">Термостат</option>\
					<option '+S.typeW[2]+' value="2">Термостат. Релейный режим</option>\
					<option '+S.typeW[3]+' value="3">ПЗА - уличный термометр</option>\
					<option '+S.typeW[4]+' value="4">ПЗА - уличный и комнатный термометр</option>\
					<option '+S.typeW[5]+' value="5">ПЗА - уличный и комнатный термометр. Релейный режим.</option>\
					</select>\
				</div>'; 
				S.dprint += '</div>';
				
				S.dprintPza ='<div class="block-pb-20">';
				S.dprintPza +='<h4>Уличный датчик температуры ПЗА</h4>';
				S.dprintPza +='<div><ses class="list" data-equp="sensorPza"></ses></div>';
				S.dprintPza +='</div>';
				
				S.dprintCE = '<div class="block-p-10"></div>'; 
				S.dprintCE +='<div class="block-w-100 block-pb-10 block-pt-10">';
				S.dprintCE +='<h4>Сброс ошибок котла</h4>';
				S.dprintCE +='<button>Сброс ошибок</button>';
				S.dprintCE +='</div>';
			} else {S.dprint = ''; S.dprintPza = ''; S.dprintCE = '';}
			
			S.print += '<div class="block-w-100 block-pb-10 block-pt-10 elements-left">'; 
				S.print += '<div>';
				S.print += '<span id="heating-icon-flame" class="icon icon-flame size-x2 icon-p1"></span>';
				S.print += '</div>';
				
				S.print += '<div>'; 
				S.print += '<span>Tекущее значение: </span><span id="heating-state-val" class="text-orange"></span><br>'; 
				S.print += '<span>Cостояние: </span><span id="heating-state-info"></span><br>'; 
				S.print += '<span>Поддерживается: </span><span class="text-orange">'+S.valS+'</span>'; 
				S.print += '</div>';
			S.print += '</div>';
			
			S.print += S.dprint

			S.print += '<div class="block-w-100 block-pb-10 block-pt-10">'; 
			S.print += '<h4>Температура поддержания:</h4>'; 
			S.print += '<div class="block-w-100 elements-around">\
				<div class="inline block-pr-10">\
					<checkbox name="temp" class="blue btn_open_control '+S.typeTemp['eco']+'" data-of="#heating-schedule-timetable"></checkbox>\
					<span class="text-blue vertical-middle">Эконом</span><br>\
					<number>'+S.dataTemp['eco']+'</number>\
				</div>\
				<div class="inline block-pr-10">\
					<checkbox name="temp" class="orange btn_open_control '+S.typeTemp['standart']+'" data-of="#heating-schedule-timetable"></checkbox>\
					<span class="text-orange vertical-middle">Стандарт</span><br>\
					<number>'+S.dataTemp['standart']+'</number>\
				</div>\
				<div class="inline block-pr-10">\
					<checkbox name="temp" class="red btn_open_control '+S.typeTemp['komfort']+'" data-of="#heating-schedule-timetable"></checkbox>\
					<span class="text-red vertical-middle">Комфорт</span><br>\
					<number>'+S.dataTemp['komfort']+'</number>\
				</div>\
				<div class="inline block-pr-10">\
					<checkbox name="temp" class="black btn_open_control '+S.typeTemp['custom']+'" data-of="#heating-schedule-timetable"></checkbox>\
					Свое значение:<br>\
					<number>'+S.dataTemp['custom']+'</number>\
				</div>\
				<div class="inline">\
					<checkbox name="temp" class="'+S.typeTemp['timetable']+' btn_open_control" data-on="#heating-schedule-timetable"></checkbox>\
					<span class="vertical-middle">Расписание</span><br>\
					<div class="block-p-18"></div>\
				</div>\
			</div>'; 
			S.print += '</div>';

			S.print += '<div id="heating-schedule-timetable" class="block-w-100 block-pb-10 block-pt-10 block-onof '+S.SCtimetable+'">'; 
			S.print += '<h4>Расписание поддержания температуры:</h4>'; 
			S.print += '<div>\
				<timetable size="25" data-data="'+S.dataTemp['timetable']+'">\
				<val value="0" color="#007de3">Эконом</val>\
				<val value="1" color="#ffae00">Стандарт</val>\
				<val value="2" color="#f22222">Комфорт</val>\
				</timetable>\
			</div>'; 
			S.print += '</div>';
				S.print += '<div class="block-p-10"></div>';
			S.print += '<div class="block-w-100 block-pb-10 block-pt-10">'; 
			S.print += '<h4>Гистерезис <span title="Максимально возможное отклонение поддерживаемой температуры от заданной." class="helper">?</span></h4>'; 
			S.print += '<p>Внимание! Не меняйте эту настройку без необходимости!</p>'; 
			S.print += '<scroll min="0" max="5"> '+S.glist+' </scroll>'; 
			S.print += '</div>';
				S.print += '<div class="block-p-10"></div>';
			S.print +='<div class="block-w-100 block-pb-10 block-pt-10 elements-around">';
			
			S.print +='<div class="block-pb-20">';
			S.print +='<h4>Устройство управления отоплением</h4>';
			S.print +='<div><ses class="list" data-id="'+elem.uu_heater+'"></ses></div>';
			S.print +='</div>';
			
			S.print +='<div class="block-pb-20">';
			S.print +='<h4>Устройство управления насосом</h4>';
			S.print +='<div><ses class="list" data-id="'+elem.uu_pump+'"></ses></div>';
			S.print +='</div>';
			
			S.print +='<div class="block-pb-20">';
			S.print +='<h4>Датчик поддержания температуры</h4>';
			S.print +='<div><ses class="list" data-id="'+elem.sensor_temp+'"></ses></div>';
			S.print +='</div>'
			
			S.print += S.dprintPza;
			
			S.print +='</div>'
			
			S.print += S.dprintCE;
			
				S.print += '<div class="block-p-10"></div>'; 
			
			if(S.pump > 0){S.pumpS1 = ''; S.pumpS2 = 'selected'; S.pumpSO = 'open';}
			else {S.pumpS1 = 'selected'; S.pumpS2 = ''; S.pumpSO = '';}
			
			S.print +='<div class="block-w-100 block-pb-10 block-pt-10">'
			S.print += '<h4><checkbox class="'+S.invers+'"></checkbox> : Инверсный режим реле</h4>'; 
			S.print +='</div>'
			
			S.print +='<div class="block-w-100 block-pb-10 block-pt-10">'
			S.print += '<h4>Насос:</h4>'; 
			S.print +='<div>\
				<select class="open_control">\
				<option '+S.pumpS1+' data-of="#pump-control-timer" value="0">Не отключать</option>\
				<option '+S.pumpS2+' data-on="#pump-control-timer" value="-1">Отключить через</option>\
				</select>\
				<span id="pump-control-timer" class="open-clouse '+S.pumpSO+'"><number>'+S.pump+'</number> сек. после отопителя</span>\
			</div>'
			S.print +='</div>'
			
			S.printGraph = '';
			if(elem.graphs){
				let allGraph = elem.graphs;
				for (let key in allGraph){
					let graph = allGraph[key];
					S.printGraph += '<span class="btn-active block-p-8 pointer graph_control_heating" '+
					'data-id="'+key+'"'+
					'data-targetdown="checkbox"'+
					'style="color:'+graph.color+'">'+
						'<checkbox class="mini select-none '+graph.active+'"></checkbox> '+graph.name+
					'</span>';
				}
			}
			
				S.print += '<div class="block-p-10"></div>'; 
			S.print +='<h4>Статистика работы отопителя</h4>'
			S.print += '<div id="equp-statistic-graf" class="block-w-100">'+
				'<div class="flex_between block-w-100 block_pb_10">'+
					'<div>'+
						'<span class="vertical-middle">Показания за:</span>'+
						G.select({1:'1ч',3:'3ч',6:'6ч',12:'12ч',24:'1д',72:'3д'},'statistic_graf_time_wiev')+
						'<span id="loader-graph-program-select"  class="vertical-middle">'+G.miniLoader()+'</span>'+
					'</div>'+
					'<div>'+
						G.select({1:'JSON',2:'XML',3:'DOC',4:'SVG',5:'PDF'},'')+
						'<span class="icon icon-24 icon-download vertical-middle pointer"></span>'+
					'</div>'+
				'</div>'+
			'<div class="graph-window-heating block-w-100 block_h_300p"> '+G.loader()+' </div>'+
			'<div class="block-w-100 ">'+S.printGraph+'</div>'+
			'</div>';
		}
		return S.print;
	}
	G.admin = {};
	G.admin.systemListTop = function(load){load = load || 'on'
		let print = '<div class="one-system top-info">'+
			'<span>SN</span>'+
			'<span>Версия</span>'+
			'<span>Версия ПО</span>'+
			'<span>Связь</span>'+
			'<span>Последний сеанс</span>'+
			'<div class="btns"></div>'+
		'</div>';
		if(load == 'on'){print += '<li>'+G.loader()+'</li>';}
		return print;
	};
	G.admin.systemList = function(info){ console.log(info);
		let print = G.admin.systemListTop('of');
		let list = info.list;
		HWF.recount(list,function(sys){
			let ds = {} 
			ds.id = sys.id || '';
			ds.imei = sys.ident || '';
			ds.conect = (sys.connection)?'green':'';
			ds.po = sys.software_version || '';
			ds.on = (sys.connection)?'tc_blue':'';
			ds.onT = (sys.connection)?'Online':'OFFline';
			ds.ver = sys.version || '';
			ds.blocked = (sys.active)?'red':'';
			ds.coment = (sys.comments)?'black':'';
			ds.time = HWF.unixTime(sys.connection_time,'on');
			
			print += '<div class="one-system" data-id="'+ds.id+'">'+
				'<span title="'+ds.onT+'" class="'+ds.on+' name-elem">'+ds.imei+'</span>'+
				'<span>'+ds.ver+'</span>'+
				'<span>'+ds.po+'</span>'+
				'<span><span class="circle-icon-state p13 '+ds.conect+' "></span></span>'+
				'<span>'+ds.time.fullDate+' в '+ds.time.fullTime+'</span>'+
				'<div class="btns">'+
					'<span title="Заблокировать" class="text-icon blocked_system '+ds.blocked+'">⍉</span>'+
					'<span title="Стереть" class="text-icon clear_base_system">♻</span>'+
				'</div>'+
			'</div>';
		});
		if(list.length <= 0){print += '<p>Систем необнаружено</p>'}
		return print;
	}
	G.admin.userListTop = function(load){load = load || 'on'
		let print = '<li class="one-elem top-info user-info">'+
			'<span><span class="block_pl_40">Email</span></span>'+
			'<span>Имя</span>'+
			'<span>Телефон</span>'+
			'<span>Регистрация</span>'+
			'<span>Заходил</span>'+
			'<div class="btns"></div>'+
		'</li>';
		if(load == 'on'){print += '<li>'+G.loader()+'</li>';}
		return print;
	};
	G.admin.userList = function(info){
		let print = G.admin.userListTop('of');
		let users = info.list;
		HWF.recount(users,function(user){ if(user.email !='EctoGod@ectostroy.ru') {
			let D = {};
			D.id = user.id_user;
			D.mail = user.email || '';
			D.name = user.name || '';
			D.tel = user.phone || '';
			D.number = user.user_number || '';
			D.numberS = HWF.emptyStep4(D.number);
			D.coment = (user.comments)?'black':'';
			
			D.last = 'Не подтверждён';
			D.reg = HWF.unixTime(user.registration_time,'of').full;
			if(user.connection_time) {D.last = HWF.unixTime(user.connection_time,'of').full; }
			
			D.system = user.system;
			D.pSys = '';
			
			print += '<li class="one-elem" data-id="'+D.id+'" data-email="'+D.mail+'" data-numb="'+D.number+'">'+
				'<div class="user-info">'+
					'<span class="info">'+
						'<span title="войти" class="btn-login-user click_get_user_token">➚</span>'+
						'<span title="Системы акаунта" class="click-user name-elem">'+
							'<span class="select-none mail">'+D.mail+'</span> <br>'+
							'<span title="id" class="number-user select-none">id: '+D.numberS+'</span>'+
						'</span>'+
					'</span>'+
					'<span class="name">'+D.name+'</span>'+
					'<span class="tell">'+D.tel+'</span>'+
					'<span>'+D.reg+'</span>'+
					'<span>'+D.last+'</span>'+
					'<div class="btns">'+
						'<span title="Коментарий" class="icon icon-email view_coment_elem hover '+D.coment+'"></span>'+
						'<span title="Удалить" class="icon icon-clouse hover delete_user"></span>'+
					'</div>'+
				'</div>'+
				'<div class="user-system">'+
					'<div class="list_system">'+G.admin.systemListTop('of')+'</div>'+
					'<div class="pagin_system"></div>'
				'</div>'+
			'</li>';
		}});
		return print
	}
	G.admin.adminListTop = function(load){load = load || 'on'
		let print = '<li class="one-elem top-info user-info">'+
			'<span>Email</span>'+
			'<span>Доступ</span>'+
			'<div class="btns"></div>'+
		'</li>';
		if(load == 'on'){print += '<li>'+G.loader()+'</li>';}
		return print;
	};
	G.admin.adminList = function(info){
		let print = G.admin.adminListTop('of');
		let users = info.list;
		HWF.recount(users,function(user){
			let D = {}; 
			D.id = user.id_user;
			D.mail = user.email || '';
			D.root = user.root || '';
			
			print += '<li class="one-elem" data-id="'+D.id+'">'+
				'<span title="Системы акаунта" class="click-user name-elem">'+D.mail+'</span>'+
				'<span>Admin</span>'+
				'<a href="#god/admins/'+D.id+'" class="click"></a>'+
				'<div class="btns"><span title="Удалить" class="text-icon delete_user">✕</span></div>'+
			'</li>';
		});
		return print
	}
	G.admin.commentsList = function(list){
		let echo = '<ul class="list coment-elem-list">';
		HWF.recount(list,function(sms){
			sms.date = HWF.unixTime(sms.timestamp,true).full;
			echo += '<li class="one-coment" data-id="'+sms.id+'">'+
				'<div class="state-coment">'+
					'<span class="data-coment">'+sms.date+'</span>'+
					'<span class="info-coment">'+sms.text+'</span>'+
				'</div>'+
				'<span title="Удалить" class="icon icon-send-delete pointer hover btn_delete_coment"></span>'+
			'</li>';
		});
		echo += '</ul>';
		return echo
	}
	// выбор времени и периуда для разныш штук
	G.timeControler = function(H,fcn,fcnL,obj){
		obj = obj || {}; H = H || 1;
		obj.helpTime = obj.helpTime || G.helper(HWD.helpText.equp.btnHelpGraphTime); // 
		obj.helpLoad = obj.helpLoad || G.helper(HWD.helpText.equp.btnHelpGraphType); // 
		obj.load = obj.load || false;
		fcn = fcn || ''; fcnL = fcnL || '';
		let selector = {}; selector[H] = 'active';
		if(!H){selector[1] = 'active';}
		
		let loadPrint = '<div class="flex_between">\
				<span class="block_p_4">'+obj.helpLoad+'</span>\
				'+HWhtml.select({'CSV':'CSV','PDF':'PDF'},'select_type_format type-load-format')+'\
				<span class="icon hover icon-download block_ml_8 btn_load_info_format"></span>\
			</div>';
			
		if (obj.load){loadPrint = '<div></div>'}
		
		let print = '\
		<div class="block-control-graph" data-fcn="'+fcn+'" data-fcnl="'+fcnL+'">\
			<div>\
				<div class="statistic_time_wiev btn-select-time '+(selector[1] || '')+'" data-h="1">1 час</div>\
				<div class="statistic_time_wiev btn-select-time '+(selector[3] || '')+'" data-h="3">3 часа</div>\
				<div class="statistic_time_wiev btn-select-time '+(selector[12] || '')+'" data-h="12">12 часов</div>\
				<div class="statistic_time_wiev btn-select-time '+(selector[24] || '')+'" data-h="24">24 часа</div>\
				<div class="statistic_time_wiev btn-select-time '+(selector[168] || '')+'" data-h="168">7 дней</div>\
				<div class="statistic_time_wiev btn-select-time '+(selector[336] || '')+'" data-h="336">14 дней</div>\
				<span class="ts_13 tc_grey_fon vertical-middle block_ml_8 block_pt_2">'+obj.helpTime+'</span>\
			</div>\
			'+loadPrint+'\
		</div>';
		
		return print;
	}
	//
	this.vijetImgGraph = function(data){ //console.log(data);
		let print = '';
		let time = ''; HWF.unixTime(data.change_state_timestamp);
		let dataText = data.states_description || {} ; 
		let textL = data.state_from || 0;
		let textR = data.state_to || 0;
		let titleL = dataText[textL] || '';
		let titleR = dataText[textR] || '';
		let imageN = data.image_number || 0;
		if(data.change_state_timestamp) {
			time = HWF.unixTime(data.change_state_timestamp);
		} else {time = {fullDate:'',hh:'',mm:''}}
		
		
		print+= '<div class="discret-vijet-graph view_'+imageN+'">';
		print+= '<div class="left-line"><span class="title">'+titleL+'</span></div>';
		print+= '<div class="center-line"><span class="centr-img"></span><span class="centr-tringle"></span></div>';
		print+= '<div class="right-line">\
			<span class="title">'+titleR+'</span>\
			<span class="dop-text">'+time.fullDate+' в '+time.hh+':'+time.mm+'</span>\
		</div>';
		print+= '</div>';
		
		return print;
	}
}
var HWhtml = new hw_html();
