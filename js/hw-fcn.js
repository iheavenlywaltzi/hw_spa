"use strict"; 
// Глобальные
function globalF(){
	let g = this;
	// старт системы
	this.start = function(){
		let token = localStorage.getItem('PageAccess') || false;
		let GodToken = localStorage.getItem('PITn0Hxg') || false;
		if(GodToken){
			g.post({url:'user/check-user',data:{token:GodToken},fcn:ok,fcnE:g.exitToLogin});
			function ok(info){
				let tokneTest = info.success || false;				
				if(info.admin){
					document.body.classList.add('admin');
					if(token){g.setInfoTopMenu();}
				}else{localStorage.setItem('PITn0Hxg',''); document.body.classList.remove('admin');}
				
				if(!tokneTest){g.exitToLogin()}
			}
			
		}else if(token && !GodToken){ 
			g.post({url:'user/check-user',data:{token:token},fcn:ok,fcnE:g.exitToLogin});
			
			function ok(info){
				let tokneTest = info.success || false;
				if(!tokneTest){g.exitToLogin()}
				else{g.setInfoTopMenu();} 
			}
		}else {g.exitToLogin()}
	}
	// Вставит емаил и ИД пользователя в верхнее меню
	this.setInfoTopMenu = function(){
		let token = localStorage.getItem('PageAccess') || false;
		let uEmail = localStorage.getItem('UserEmail') || false;
		let uNumber = localStorage.getItem('UserNumber') || false;
		let menu = document.body.querySelector('#right-menu');
		if(!uEmail || !uNumber){
			
			g.post({url:'user/get_settings',data:{token:token},fcn:function(inf){
				let email = inf.email || '';
				let numder = inf.user_number || '';
				numder = numder.toString();
				numder = numder.replace(/(\d)(?=(\d{4})+(\D|$))/g, '$1 ');
				localStorage.setItem('UserEmail',email);
				localStorage.setItem('UserNumber',numder);
				menu.querySelector('.user-email-info').innerHTML = email;
				menu.querySelector('.user-id-info').innerHTML = 'id: '+numder;
			}});
		}
		else{
			uNumber = uNumber.toString();
			uNumber = uNumber.replace(/(\d)(?=(\d{4})+(\D|$))/g, '$1 ');
			menu.querySelector('.user-email-info').innerHTML = uEmail;
			menu.querySelector('.user-id-info').innerHTML = 'id: '+uNumber;
		}
	}
	this.exitToLogin = function(info){ info = info || false;
		if(HWS.hash[0] == 'foreign_login'){
			HWS.pageAccess = false;
			localStorage.setItem('PageAccess','');
			localStorage.setItem('PITn0Hxg','');
			document.body.classList.remove('full');
		}
		else if (HWS.hash[0] == 'login' && Boolean(HWS.get)){
			HWS.pageAccess = false;
			localStorage.setItem('PageAccess','');
			localStorage.setItem('PITn0Hxg','');
			document.body.classList.remove('full');
		}
		else {
			if (info) {console.log(info);}
			HWS.pageAccess = false;
			localStorage.setItem('PageAccess','');
			localStorage.setItem('PITn0Hxg','');
			HWS.go('login');
			document.body.classList.remove('full');
		}
	}
	// переход между страницами
	this.hashChange = function(hash){
		let token = localStorage.getItem('PageAccess') || false;
		let GodToken = localStorage.getItem('PITn0Hxg') || false
		let rmenu = document.body.querySelector('.right-click-menu');
		let lmenu = HWS.blocks.menul.elem;
		let lvlPage1 = hash.hash[0];

		// Работа с левым меню.
		if (lmenu.querySelector('a.active')) {lmenu.querySelector('a.active').classList.remove('active');}
		if(lvlPage1 == 'main' || !lvlPage1){lmenu.querySelector('a.main').classList.add('active');}
		if(lvlPage1 == 'projects' || lvlPage1 == 'project'){lmenu.querySelector('a.projects').classList.add('active');}
		if(lvlPage1 == 'equipments' || lvlPage1 == 'equipment'){lmenu.querySelector('a.equipments').classList.add('active');}
		if(lvlPage1 == 'programs' || lvlPage1 == 'program'){lmenu.querySelector('a.programs').classList.add('active');}
		if(lvlPage1 == 'history'){lmenu.querySelector('a.history').classList.add('active');}
		
		//if()
		// Работа с правым меню.
		if(rmenu){rmenu.remove();}
		if(GodToken){
			let menul = HWS.blocks.menul.elem;
			let menur = HWS.blocks.menur.elem;
			let btnAdmin = menul.querySelector('#adminbtnpage');
			
			if(!btnAdmin){
				let newA = document.createElement('a');
					newA.href = '#god'; newA.id = 'adminbtnpage'; newA.classList.add('page-admin'); newA.title = 'Админка';
					newA.innerHTML = '<span class="text-icon">✧</span><span class="name">Админка</span>';
				menul.prepend(newA);
			}
			
			if(token){
				HWS.blocks.menul.removeClass('admin');
				HWS.blocks.menur.removeClass('admin');
				let newS = menul.querySelector('#exituser');
				if(!newS){
					newS = document.createElement('span'); newS.id = 'exituser'; newS.title = 'Выйти из пользователя';
					newS.classList.add('btn_exit_user');
					newS.innerHTML = '<span class="text-icon select-none">⍅</span><span class="name select-none">Выход из пользователя</span>';
					menul.append(newS);
				}
			}
			else {
				HWS.blocks.menul.addClass('admin'); 
				HWS.blocks.menur.addClass('admin'); 
				let newS = menul.querySelector('#exituser'); 
				if(newS){newS.remove();}}
		}
		else if(!HWS.pageAccess && !token){g.exitToLogin()}
		if(HWS.hash[0] == 'login'){document.body.classList.add('full');} 
		else if(HWS.hash[0] == 'foreign_login'){document.body.classList.add('full');} 
		else {document.body.classList.remove('full'); }
		
		HWS.data.programs = '';
		g.clearFilter();
		clearTimeout(HWS.buffer.specTimeControllerId);
		HWS.buffer.specTimeControllerFlag = false;
		if(HWS.buffer.hashChange){HWS.buffer.hashChange();}
	}
	//клик по документу
	this.global = function(e){
		let menur = document.body.querySelector('.right-click-menu');
		let target = e.target;
		//let dataPi = document.getElementById('xcalend')
		//if(dataPi){dataPi.style.display='none'}
		if(target.classList.contains('time_val_data') || target.classList.contains('xcalend') || HWS.upElem(target,'.xcalend')){console.log(target);}
		else { let dataPi = document.getElementById('xcalend'); if(dataPi){dataPi.style.display='none'}}
		if(!e.target.classList.contains('helper')){document.querySelector('.helper-window').style.visibility = 'hidden';}
		if(menur){ let spm = 1;
			if(e.target.classList.contains('add_new_equp')){spm = 0}
			if(e.target.classList.contains('btn_add_fast_tile')){spm = 0}
			if(e.target.classList.contains('btn_add_fast_vijet')){spm = 0}
			
			if(spm){menur.classList.remove('active');}
		}
		if(HWS.buffer.globalClick){HWS.buffer.globalClick(e);}
	}
	// назад на 1 страницу
	this.pageBack = function(){history.back();}
	// принимает время формата Юнихтайм и вернем объект с разными идами времени
	this.unixTime = function(time,cor){ cor = cor || false; time = time || Date.now();
		let corect = new Date().getTimezoneOffset()*60*1000; corect = corect*(-1);
		let obj = {}; 
		let tim = (cor)? new Date(time) : new Date(time-corect) ;
		
		obj.unix = time;
		obj.utc = tim.getTime();
		obj.date = tim;
		obj.dd = String(tim.getDate()).padStart(2, '0');
		obj.mt = String(tim.getMonth() + 1).padStart(2, '0');
		obj.yyyy = String(tim.getFullYear()).padStart(2, '0');
		obj.hh = String(tim.getHours()).padStart(2, '0');
		obj.mm = String(tim.getMinutes()).padStart(2, '0');
		obj.ss = String(tim.getSeconds()).padStart(2, '0');
		obj.fullTime = obj.hh+':'+obj.mm+':'+obj.ss;
		obj.fullDate = obj.dd+'.'+obj.mt+'.'+obj.yyyy;
		obj.full = obj.fullDate+' '+obj.fullTime;
		obj.fullR = obj.fullTime+' '+obj.fullDate;
		
		return obj;
	}
	// вставит пробел в строку через [n] символов
	this.emptyStep4 = function(str){
		str = str.toString();
		str = str.replace(/(\d)(?=(\d{4})+(\D|$))/g, '$1 ');
		return str;
	}
	// Кнопка включения выключения класса "active"
	this.active = function(e,elem){
		let elemActive = elem;
		let targetPlus = false;
		let onof = false;
		
		if(!elem.classList.contains('blocked')){
			// Только таргет (ищит по родителю вверх)
			if (elem.dataset.target){elemActive = HWS.parentSearchClass(elem,elem.dataset.target);}
			// Только таргет (ищит от родителя вниз)
			if (elem.dataset.targetdown){elemActive = elem.querySelector(elem.dataset.targetdown);}
			// Только таргет (ищит на всей страници)
			if (elem.dataset.targetg){elemActive = document.querySelector(elem.dataset.targetg);}
			// И кнопка и таргет ( по родителю вверх )
			if (elem.dataset.targetplus){targetPlus = HWS.parentSearchClass(elem,elem.dataset.targetplus);}
			// И кнопка и таргет ( ищит на всей страници )
			if (elem.dataset.targetplusg){targetPlus = document.querySelector(elem.dataset.targetplusg);}
			// Если есть элемент onof то добавить или удалить ему класс open
			if (elem.dataset.onof){onof = document.querySelector(elem.dataset.onof);}
			
			if(elemActive.classList.contains('active')){
				elemActive.classList.remove('active');
				if(targetPlus){targetPlus.classList.remove('active')}
				if(onof){onof.classList.remove('open')}
			}
			else {
				elemActive.classList.add('active');
				if(targetPlus){targetPlus.classList.add('active')}
				if(onof){onof.classList.add('open')}
			}
		}
	}
	// Открывает 1 закрывая другой.
	this.activeOne = function(e,elem){
		let elemActive = elem;
		
		if (elem.dataset.target){
			let alemT  = document.querySelector('.'+elem.dataset.target+'.active');
			let alem  = document.querySelector('.'+elem.dataset.target+' .btn-active-one.active');
			if (alem){alem.classList.remove('active');}
			if (alemT){alemT.classList.remove('active');}
			
			let elemTarget = HWS.parentSearchClass(elem,elem.dataset.target)
			elemTarget.classList.add('active');
		}
		
		if(elemActive.classList.contains('active')){elemActive.classList.remove('active');}
		else {elemActive.classList.add('active');}
	}
	// для select, открывает или закрывает указаный элемент в параметре
	this.activeControl = function(e,elem){;
		let opt = elem.options[elem.selectedIndex], allOn = opt.dataset.on, allOf = opt.dataset.of; 
		if(allOn){allOn.split(' ').forEach(function(el){let ele = document.body.querySelector(el).classList.add('active');});};
		if(allOf){allOf.split(' ').forEach(function(el){let ele = document.body.querySelector(el).classList.remove('active');});};
	}
	// для select, открывает или закрывает указаный элемент в параметре
	this.openControl = function(e,elem){;
		let opt = elem.options[elem.selectedIndex], allOn = opt.dataset.on, allOf = opt.dataset.of; 
		if(allOn){allOn.split(' ').forEach(function(el){let ele = document.body.querySelector(el).classList.add('open');});};
		if(allOf){allOf.split(' ').forEach(function(el){let ele = document.body.querySelector(el).classList.remove('open');});};
	}	
	// по нажатию, открывает или закрывает указаный элемент в параметре
	this.btnOpenControl = function(e,elem){
		let allOn = elem.dataset.on, allOf = elem.dataset.of; 
		if(allOn){allOn.split(' ').forEach(function(el){let ele = document.body.querySelector(el).classList.add('open');});};
		if(allOf){allOf.split(' ').forEach(function(el){let ele = document.body.querySelector(el).classList.remove('open');});};
		
		// console.log(elem);
		// console.log(elem.dataset);
	}
	// выводить информационно сообщение
	this.infoMsg = function(sms,data){data = data || {};
		let t = data.top || 10;
		let l = data.left || 8;
		let Control = data.control || "timer"; // 'on' 'of'
		let mesage = document.querySelector('.info-mesage');
		if (!mesage) {
			mesage = document.createElement('div'); 
			mesage.classList.add('info-mesage');
			document.body.appendChild(mesage);
		}
		mesage.innerHTML = sms;
		mesage.style.display = 'block';
		mesage.style.top = t+'px';
		mesage.style.left = l+'px';
		if(Control == 'timer'){
			clearTimeout(HWS.data.infoMesageTimer);
			HWS.data.infoMesageTimer = setTimeout(function(){mesage.style.display = 'none';},10000);
		}
		if(Control == 'on'){}
		if(Control == 'of'){mesage.style.display = 'none'}

	}
	// запрос на сервер Отправкой массива ввиде GET
	this.get = function (url,fcn,fcnE){
		let func = fcn || function(){};
		let funcErr = fcnE || function(){};
	
		HWS.ajax({url:url,type:'GET',fc:func,fcb:funcErr});
	}
	// запрос на сервер Отправкой массива ввиде ПОСТ
	this.post = function (obj){ obj = obj || {};
		let inf = '';
		let func = obj.fcn || function(info){
			if(info.exceptions){
				let code = info.exceptions[0].code; // 5000010034
				if(code = 5000010034){HWF.push('Недостаточно прав','red')}
			}
		};
		let funcErr = obj.fcnE || HWF.getServMesErr;
		let url = HWD.api.url+obj.url;
		let data = obj.data || {}; data = {j:JSON.stringify(data)};
		let type = obj.type || 'api';
		
		if (type == 'noapi'){url = obj.url;}
		
		HWS.ajax({url:url,type:'POST',data:data,fc:func,fcb:funcErr});
	}
	// запрос на сервер Отправкой Формы
	this.form = function (obj){ obj = obj || {};
		let json = obj.data || false;
		let file = obj.file || false
		let func = obj.fcn || function(){};
		let url = HWD.api.url+obj.url;
		let type = obj.type || 'api';
		let convert = obj.cnv || 'JSON';
		
		let formD = new FormData()
		if(json){formD.append('j',JSON.stringify(json))}
		if(file){formD.append('file',file)}
		
		if (type == 'noapi'){url = obj.url;}		
			
		HWS.ajax({url:url,type:'FORM',data:formD,convert:convert,fc:func,fcb:func});
	}
	// Загрузчик языковых пакетов
	this.loadlang = function(lang,file,wh){
		let url = 'languages/'+lang+'/'+file;
		g.get(url,function(info){HWD[wh] = info;} );
	}

	// определяет , проверяет есть ли  в объекте ключь
	this.objKeyTest = function(key,obj){return key in obj;}
	//функция модального блока
	this.modal = {
		on:function(){HWS.blocks.modal.addClass('active');},
		off:function(){HWS.blocks.modal.removeClass('active');},
		of:function(){HWS.blocks.modal.removeClass('active');},
		confirm:function(title,info,dopClass,yes,no,remClas){
			dopClass = dopClass || '';
			yes = yes || 'Да';
			no = no || 'Отмена';
			let hea = HWhtml.mConfirmTitle(title);
			let html = HWhtml.mConfirm(info,dopClass,yes,no,remClas);
			HWS.blocks.modal.addClass('active');
			HWS.blocks.modal.data.title.append(hea);
			HWS.blocks.modal.data.body.append(html);
		},
		confirmCust:function(obj){
			let hea = HWhtml.mConfirmTitle(obj.title);
			let html = HWhtml.mConfirmCust(obj);
			HWS.blocks.modal.addClass('active');
			HWS.blocks.modal.data.title.append(hea);
			HWS.blocks.modal.data.body.append(html);
		},
		message:function(title,head){
			let hea = HWhtml.mConfirmTitle(title);
			let html = HWhtml.mConfirmMessage(head);
			HWS.blocks.modal.addClass('active');
			HWS.blocks.modal.data.title.append(hea);
			HWS.blocks.modal.data.body.append(html);
		},
		print:function(title,html){
			let hea = HWhtml.mConfirmTitlePrint(title);
			HWS.blocks.modal.addClass('active');
			HWS.blocks.modal.data.title.append(hea);
			HWS.blocks.modal.data.body.append(html);
		},
		printMini:function(title,print){
			title = title || '';
			HWS.blocks.modal.addClass('active');
			HWS.blocks.modal.data.title.append(title);
			HWS.blocks.modal.data.body.append(print);
		},
		loader:function(title){
			title = title || '';
			HWS.blocks.modal.addClass('active');
			HWS.blocks.modal.data.title.append(title);
			HWS.blocks.modal.data.body.append(HWhtml.loader());
		},
	}
	// Очищаем все блоки
	this.clearBlocks = function(B){
		HWS.blocks.title.clear(); HWS.blocks.menud.clear(); HWS.blocks.content.clear(); 		
		HWS.blocks.modal.data.title.clear();
		HWS.blocks.modal.data.body.clear();
	}
	// проверка имени на корректность ввода
	this.nameControl = function(name){
		let nameRegex = new RegExp("^[а-яА-Яa-zA-Z0-9-_()!№# ]+$");
		let control = false;
		
		//console.log(name);
		//console.log(nameRegex.test(name));
		
		if(nameRegex.test(name)){control = true}
		
		return control;
	}
	// функции для пагинатора
	this.pagination = new function(){
		let PAG = this;
		// ВНУТРЕННЯЯ функция, переходит на УКАЗАННУЮ страницу.
		this.goPage = function(num,elem){
			let info = get_info(elem);
			info.page = parseInt(num);
			info.start = (info.page-1)*info.amount;
			print(info);
		}
		// нажаите на страничку
		this.page = function(event,elem){
			let info = get_info(elem);
			info.page = parseInt(elem.dataset.page);
			info.start = (info.page-1)*info.amount;
			print(info);
		},
		// нажатие на стрелочку в лево
		this.left = function(event,elem){
			let info = get_info(elem);
			info.start = ((info.page-1)*info.amount)-info.amount;
			if ((info.page-1) >= 1){ info.page -= 1; print(info); }
		}
		// нажатие на стрелочку в лево END
		this.leftEnd = function(event,elem){PAG.goPage(1,elem);}
		// нажатие на стрелочку вправо
		this.right = function(event,elem){
			let info = get_info(elem);
			let page = parseInt(info.page);
			let sum = Math.ceil(info.sum/info.amount);
			info.start = ((info.page+1)*info.amount)-info.amount;
			if ((info.page+1) <= sum){ info.page += 1; print(info); }
		}
		// нажатие на стрелочку в вправо END
		this.rightEnd = function(event,elem){let info = get_info(elem); PAG.goPage(info.sumPage,elem);}
		// нажатие количество отоброжаемых элементов
		this.amount = function(event,elem){
			let info = get_info(elem);
			info.amount = parseInt(elem.value);
			localStorage.setItem('AllPageAmount',info.amount);
			print(info);
		}
		
		function get_info(elem){
			let info = {}
			let pagin = HWS.parentSearchClass(elem,'paginator');
			info.main = pagin;
			info.sum = parseInt(pagin.dataset.all);
			info.targetStr = pagin.dataset.target || 'body';
			info.target = document.querySelector(info.targetStr);
			info.list = pagin.querySelector('.pagin-numbers');
			info.page = parseInt(pagin.querySelector('.pagin-numbers .active').dataset.page);
			info.load = pagin.dataset.load;
			info.type = pagin.dataset.type;
			info.amount = parseInt(pagin.querySelector('.paginator-wiev-page').value);
			info.sumPage = Math.ceil(info.sum/info.amount);
			return info;
		}
		
		function print (info) {
			if (info.type == 'norm'){
				let textDop = info.main.querySelector('.template .tmp-dop') || false;
				if (textDop){textDop = JSON.parse(textDop.value);}
				
				info.list.innerHTML = HWhtml.pagination({type:'list',all:info.sum,number:info.page,amount:info.amount});
				load_type(info.load,info.target,info.start,info.amount,textDop)
			}
			if (info.type == 'templater'){
				let textUrl = info.main.querySelector('.template .tmp-url').value;
				let textStart = info.main.querySelector('.template .tmp-start').value;
				let textOne = info.main.querySelector('.template .tmp-one').value;
				let textEnd = info.main.querySelector('.template .tmp-end').value;
				let textDop = info.main.querySelector('.template .tmp-dop');
				let data = {number:info.page,amount:info.amount,start:info.start}
				
				if (textDop){textDop = JSON.parse(textDop.value); data.dopP = textDop}
				
				g.list(
					info.targetStr,
					textUrl,
					{start:textStart,one:textOne,end:textEnd},
					data
				);
			}
			if (info.type == 'buffer'){
				HWS.buffer[info.load](info);
				info.list.innerHTML = HWhtml.pagination({type:'list',all:info.sum,number:info.page,amount:info.amount,sumPage:info.sumPage});
			}
		}
		
		function load_type(type,target,start,amount,dop){g.load[type](target,{start:start,amount:amount},dop);}
	}
	// сброс всех ФИЛЬТРОВ запросов поумолчанию
	this.clearFilter = function(){HWS.data.apiGet = JSON.parse(JSON.stringify(HWD.api.queries));}
	
	//Создание Списка
	this.list = function(target,url,tmp,data){
		data = data || {};
		let elem = HWS.getElem(target);
		let token = HWS.pageAccess;
		let print = '';
		let numebr = data.number || 1;
		let amount = data.amount || 10;
		let listStart = data.start || 0;
		let dopP = data.dopP || 0;
		let post = {token:token,list_start:listStart,list_amount:amount}
		
		if(dopP){
			tmp.dop = JSON.stringify(dopP);
			for (let ke in dopP){post[ke] = dopP[ke]}
		}

		elem.innerHTML = HWhtml.loader();
		g.post({url:url,data:post,fcn:load});
		
		function load(info){
			tmp.url = url;
			print += tmp.start;
			print += HWhtml.list(info.list,tmp.one);
			print += tmp.end;
			print += HWhtml.pagination({
				target:target, 
				type:'templater', 
				all:info.item_count, 
				amount:amount, 
				number:numebr,
				tmp:tmp
			});
			elem.innerHTML = print;
		}
	}
		
	//Создание функций для вывода списков
	this.createrList = function(infol){
		g.print = {}; g.load = {};
		let allList = infol; //список функция для вывода списков )) в "script.js"
		let amount = localStorage.getItem('AllPageAmount') || 10;
		HWS.data.apiGet = JSON.parse(JSON.stringify(allList));
		
		for (let key in HWS.data.apiGet){
			g.load[key] = function(elem,data,dataDop,fcn){
				fcn = fcn || function(){};
				dataDop = dataDop || {};
				let token = HWS.pageAccess;
				let info = loader(elem,data);
				let post = {token:token,list_start:info.start,list_amount:info.amount}
				let url = HWS.data.apiGet[key];
				let sornName = localStorage.getItem('sort'+key);
				
				if(sornName){post.sort = sornName;}
				if (dataDop){for (let ke in dataDop){post[ke] = dataDop[ke]}}
				if(typeof url == 'object'){
					url = HWS.data.apiGet[key].url;
					if(HWS.data.apiGet[key].data){
						for (let li in HWS.data.apiGet[key].data){post[li] = HWS.data.apiGet[key].data[li];}
					}
				}
				
				g.post({url:url,data:post,fcn:load});
				
				function load(infoP){
					let list = infoP.list;
					HWS.buffer.equpsList = list;
					info.target.innerHTML = HWhtml[key](list,{
						view:info.view,
						a:info.a,
						type:info.type,
						click:info.click,
					});
					if(info.fcn){info.fcn(infoP);}
					fcn();
				}
				
				function error(i){
					console.log(g.getServMesErr(i));
					console.log(i);
				}
			}
			
			g.print[key] = function(target,data,fcn){
				fcn = fcn || function(){};
				let t = create_table(target,data);
				let pagin = {
					target:t.target,
					load:key,
					amount:localStorage.getItem('AllPageAmount') || amount,
					number:1
				}
				let dop = false;
				
				if(data){dop = data.dop || false;}
				if(dop){pagin.tmp = {dop:JSON.stringify(dop)};}

				t.mid.innerHTML = HWhtml.loader();
				g.load[key](t.mid,function(info){
					pagin.all = info.item_count;
					t.bot.innerHTML = HWhtml.pagination(pagin);
				},dop,fcn);
			}
		}
		
		function create_table(target,data){
			let da = data || {};
			let d = {};
				d.a = da.a || ''; 
				d.type = da.type || ''; 
				d.view = da.view || ''; // tile
				d.click = da.click || ''; // tile
				d.top = da.top || '';
			
			let mainElem;
			if (typeof target == 'string'){mainElem = document.querySelector(target)}
			if (target instanceof HTMLElement){mainElem = target}
			let table = {};
			mainElem.innerHTML = '';
			
			table.top = creat_elem(mainElem,'table-top');
			table.mid = creat_elem(mainElem,'table-mid');
			table.bot = creat_elem(mainElem,'table-bot');
			table.target = target+' .table-mid';
			
			table.top.classList.add('flex_between');
			table.top.innerHTML = d.top;
			table.mid.classList.add('flex_center_top');
			if(d.a){table.mid.dataset.a = d.a;}
			if(d.type){table.mid.dataset.type = d.type;}
			if(d.view){table.mid.dataset.view = d.view;}
			if(d.click){table.mid.dataset.click = d.click;}
			
			function creat_elem(target,clas){
				let newElem = target.querySelector('.'+clas);
				if(!newElem){
					newElem = document.createElement('div'); 
					newElem.classList.add(clas);
					target.appendChild(newElem);
				}
				return newElem;
			}
			table.mid.innerHTML = ''; table.bot.innerHTML = '';
			return table;
		}
		
		function loader(elem,data){
			let da = {}
			if(typeof data == 'function'){da.fcn = data;}
			if(typeof data == 'object'){da = data;}
			let info = {}
				info.start = da.start || 0;
				info.amount = localStorage.getItem('AllPageAmount') || da.amount || amount;
				info.fcn = da.fcn || '';
				info.view = elem.dataset.view || '';
				info.a = elem.dataset.a || '';
				info.type = elem.dataset.type || '';
				info.click = elem.dataset.click || '';
			if (typeof elem == 'string'){info.target = document.querySelector(elem)}
			if (elem instanceof HTMLElement){info.target = elem}
			info.target.innerHTML = HWhtml.loader();
			return info;
		}
	}
	
	// Для инпутов, ввод чисто цифр.
	this.inpOnlyNumb = function(e,el){
		let newNum = el.value.replace(/[^\d-.]/g,'') || '';
		el.value = newNum || '';
	}	
	
	// Для инпутов, ввод чисто цифр.
	this.eyeRead = function(e,el){
		let blockUp = HWS.upElem(el,'.spec-password');
		let inp = blockUp.querySelector('input');
		console.log(inp);
		if(el.classList.contains('active')){
			el.classList.remove('active');
			inp.setAttribute('type', 'password');
		} else {
			el.classList.add('active');
			inp.setAttribute('type', 'text');
		}
	}
		
	this.helper = function(e,elem){
		let wind = document.querySelector('.helper-window');
		if(!wind.classList.contains('active')){
			let sms =  elem.dataset.text;
			
			if (!wind){
				wind = document.createElement('div');
				wind.classList.add('helper-window');
				document.body.appendChild(wind);
			}
			let xr = e.clientX, ww = document.documentElement.clientWidth;
			let yr = e.clientY, wh = document.documentElement.clientHeight;
			let wr = wind.offsetWidth, hr = wind.offsetHeight;
			
			if(xr+wr > ww){xr = (ww-wr)-10}
			if(yr+hr > wh){yr = wh-hr}
			
			wind.style.visibility = 'visible';
			wind.style.top = yr+'px';
			wind.style.left = xr+'px';
			wind.classList.add('active');
			wind.innerHTML = sms;
		}
	}
	this.helperEnd = function(e,elem){
		let wind = document.querySelector('.helper-window');
		wind.style.visibility = 'hidden';
		wind.classList.remove('active');
		wind.style.top = '-300px';
		wind.style.left = '-300px';
	}
	// перебирает объект ( массив ) выполняю функц-ю на каждой итерации
	this.recount = function(obj,fcn,apply){
		fcn = fcn || function(){};
		if(apply == 'on') {obj = Array.apply(null,obj)}
		for (let key in obj){
			fcn(obj[key],key);
		}
	}
	// Массовые Active. добовляет всем указанным классам доп класс active
	// block - элемент В котором ищем. query - CSS селектрок для выборки всех элементов добавления
	this.allActive = function(block,query){
		if(typeof block == 'string'){block = document.querySelector(block)}
		let allR = block.querySelectorAll(query);
		let speci = 0;
		g.recount(allR,function(elem){
			if(elem.classList.contains('active')){speci++}
			else {elem.classList.add('active')}
		},'on');
		if(speci == allR.length){
			g.recount(allR,function(elem){elem.classList.remove('active')},'on');
		}
	}
	// получить сообщение об ошибки от сервера
	this.getServMesErr = function(info){
		let mesage = [];
		let keyCode = false;
		
		if (typeof info.exceptions == 'object') {for (let ex in info.exceptions){
			if(info.exceptions[ex]){keyCode = info.exceptions[ex].code; mesage.push(keyCode);}
		}}
		if (typeof info.fields_exceptions == 'object') {
			for (let fi in info.fields_exceptions){
				let mes = info.fields_exceptions[fi];
				if(typeof info.fields_exceptions[fi] == 'object'){
					for (let me in mes){keyCode = mes[me]; mesage.push(keyCode);}
				}
				else {keyCode = info.fields_exceptions[fi]; mesage.push(keyCode);}
			}
		}
		// Выход из админки
		if(keyCode == 5000010001 && localStorage.getItem('PITn0Hxg')){
			HWS.pageAccess = false;
			localStorage.setItem('PageAccess','');
			HWS.go('god');
		}
		
		mesage = HWD.errors[mesage[0]] || mesage[0];
		console.log(info);console.log(mesage);
		return mesage;
	}
	// Вывести сообщение об ошибки от сервера в модальном окне
	this.modalMess = function(info,key){
		key = key || '';
		let mes = g.getServMesErr(info) || 'Что-то пошло нетак, попробуйте еще раз';
		g.modal.message(mes,'Ошибка '+key);
		console.log(info);
	}
	// Если изменилось значениеа текстовом поле (кнопки + - )
	this.changeBtnInt = function(e,elem){
		let el = elem;
		let main = el.parentElement;
		let fcn = main.dataset.fcn || '';
		let min = main.dataset.min || 0;
		let max = main.dataset.max || 0;
		let val = parseFloat(elem.value) || 0;
		let tofix = parseInt(main.dataset.tofix) || 0;

		if(min) {
			if(min == 'null' && val < 0){val = 0}
			else if(val < min){val = parseFloat(min)}
			elem.value = val.toFixed(tofix);
		}
		if(max) {
			if(max == 'null' && val > 0){val = 0}
			else if(val > max){val = parseFloat(max)}
			elem.value = val.toFixed(tofix);
		}
		
		if(HWS.buffer[fcn]){HWS.buffer[fcn](elem.value,elem);}
	}
	// нажатие на кнопку цифрового значения
	this.clickBtnInt = function(e,elem){
		let el = elem;
		let main = el.parentElement;
		let step = parseFloat(main.dataset.step) || 1;
		let tofix = parseInt(main.dataset.tofix) || 0;
		let min = main.dataset.min || 0;
		let max = main.dataset.max || 0;
		let fcn = main.dataset.fcn || '';
		let cont = el.dataset.cont || '+';
		let input = main.querySelector('input');
		let val = 0;
		if(cont == '-'){
			val = parseFloat(input.value) - step; 
			if(min) {
				if(min == 'null' && val < 0){val = 0}
				else if(val < min){val = parseFloat(min)}
			}
		}
		if(cont == '+'){
			val = parseFloat(input.value) + step; 
			if(max) {
				if(max == 'null' && val > 0){val = 0}
				else if(val > max){val = parseFloat(max)}
			}
		}
		
		input.value = val.toFixed(tofix);
		if(HWS.buffer[fcn]){
			clearTimeout(HWS.buffer.clickBtnIntTimerFcn);
			HWS.buffer.clickBtnIntTimerFcn = setTimeout(function(){
				HWS.buffer[fcn](input.value,elem);
			},800);
		}
	}
	//Уникальные элементы массива ( уберёт все похожие элементы )
	this.unique = function(arr) {
		let result = [];
		for (let str of arr) {if (!result.includes(str)) {result.push(str);}}
		return result;
	}
	// функция постоянныхх запросов
	this.timerUpdate = function(fcn,time,Stime){ time = time || 10000; Stime = Stime || time;
		clearTimeout(HWS.buffer.specTimeControllerId);
		HWS.buffer.specTimeControllerFlag = true; // позволяет отключить авто абновление
		HWS.buffer.specTimeController = function(){
			if(HWS.buffer.specTimeControllerFlag){
				HWS.buffer.specTimeControllerId = setTimeout(HWS.buffer.specTimeController,time);
				fcn(HWS.buffer.specTimeControllerId);
			}
		}
		HWS.buffer.specTimeControllerId = setTimeout(HWS.buffer.specTimeController,Stime);
	}
	// Функция для работы с пушами
	this.push = function(html,color){ color = color || 'green';
		let windowP = document.body.querySelector('#push-mesage-window');
		let windowPL = document.body.querySelector('#push-mesage-window .push-list');
		let newDiv = document.createElement('div');
			newDiv.classList.add('one-push-message');
			newDiv.classList.add(color);
			html+= '<div class="clouse-one-push-message icon icon-clouse"></div>'
		
		clearTimeout(HWS.buffer.pushIDTimer);
		windowP.classList.add('open');
		newDiv.innerHTML = html;
		windowPL.prepend(newDiv);
		HWS.buffer.pushIDTimer = setTimeout(function(){let windowP = document.body.querySelector('#push-mesage-window'); windowP.classList.remove('open');},5000);
	}
	this.clousePush = function(){
		let windowP = document.body.querySelector('#push-mesage-window');
		clearTimeout(HWS.buffer.pushIDTimer);
		windowP.classList.remove('open');
	}
	this.clearPush = function(){
		let windowP = document.body.querySelector('#push-mesage-window .push-list');
		windowP.innerHTML = '';
	}
	this.clouseOnePush = function(e,el){
		let onePush = HWS.upElem(el,'.one-push-message');
		onePush.remove();
	}
	
	this.btnOpenFullMainw = function(e,el){
		let section = document.body.querySelector('main');
		if (el.classList.contains('active')){
			section.classList.remove('fullScreen');
			HWF.fullScreen();
			el.classList.remove('active');
		}else {
			section.classList.add('fullScreen');
			HWF.fullScreen(section);
			el.classList.add('active');
		}
	}
	this.btnClouseFullMainw = function(){
		let section = document.body.querySelector('main');
			section.classList.remove('fullScreen');
		HWF.fullScreen();
	}
	// Закрытие полноэкранного режима
	this.clickEscOfFullScrin = function(e){
		if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
			let main = document.body.querySelector('main');
			let bnt = document.body.querySelector('.btn_open_full_mainw ');
			if (main.classList.contains('fullScreen')) {main.classList.remove('fullScreen')}
			if (bnt.classList.contains('active')) {bnt.classList.remove('active')}
		}
	}
	// нажатие на любую кнопку
	this.btnClickRandom = function(e){
		if (e.code == 'Escape') {HWF.modal.of();}
		if (e.code == 'Enter') {
			let btn = HWS.getElem('.del_vijet_for_page');
			if (btn) {
				let token = HWS.pageAccess;
				let block = HWS.buffer.deleteFaseElem;
				let id = block.dataset.id;
				let mainBlock = document.querySelector('.fast-menu');
				HWF.post({url:'objects/delete_favorite_objects',data:{token:token,id_objects:[id]}});
				block.remove();

				let testBlock = mainBlock.querySelector('.one-tile');
				if(!testBlock){
					mainBlock.innerHTML = '<div id="no-tile-vijet" class="flex_center">\
						<div>\
							<p class="block_pb_20"><img src="img/add_vijet_new.png"></p>\
							<p class="block_pb_10 ts_24p tf_mb tc_greyС9">Добавьте виджет!</p>\
							<p class="tc_greyС9">У вас пока нет виджетов, добавьте <br> ваш первый виджет, нажав на кнопку <br> в правом верхнем углу экрана.</p>\
						</div>\
					</div>';
				}
				HWF.modal.of();
			}
		}
	}
	
	// расскрывает элемент наполный экран, если элемента нету то закрыает полный экран
	this.fullScreen = function(elem){
		elem = elem || false;
		if(elem){
		  if(elem.requestFullScreen) {elem.requestFullScreen();} 
		  else if(elem.mozRequestFullScreen) {elem.mozRequestFullScreen();} 
		  else if(elem.webkitRequestFullScreen) {elem.webkitRequestFullScreen();}
		}
		else {
		  if(document.cancelFullScreen) {document.cancelFullScreen();} 
		  else if(document.mozCancelFullScreen) {document.mozCancelFullScreen();} 
		  else if(document.webkitCancelFullScreen) {document.webkitCancelFullScreen();}
		}
	}
	
	// Выделяет активный пукт дашбоардного меню, присваевает класс "актив" если 2й тег соответствует 2му тегу ссылки
	this.activeDasboartMenu = function(){
		let allA = HWS.blocks.menud.elem.querySelectorAll('a');
		if(HWS.hash[1]){
			HWF.recount(allA,function(e){
				let hash = e.hash.split('/');
				if(hash[1] && HWS.hash[1]) {
					if(hash[1] == HWS.hash[1]){e.classList.add('active');}
					else {e.classList.remove('active');}
				}
			},'on');
		}
	}
	
	// Выделяет активный пукт дашбоардного меню, присваевает класс "актив" также выполняет функции из буфера прописанную в теге ** data-buffcn="fcn" **
	this.activeClickDasboartMenu = function(){
		let allA = HWS.blocks.menud.elem.querySelectorAll('.dasboart-menu .btn');
		HWF.recount(allA,function(el){
			el.addEventListener('click',function(){
				if(!el.classList.contains('active')){
					let fcn = el.dataset.fcn || false;
					let activ = document.body.querySelector('.dasboart-menu .active');
					if(activ){activ.classList.remove('active')};
					el.classList.add('active');
					if(fcn){HWS.buffer[fcn]();}
				}
			});
		},'on');
	}
	
	this.clickDasboartMenu = function(e,el){
		if(!el.classList.contains('active')){
			let fcn = el.dataset.fcn || false;
			let activ = document.body.querySelector('.dasboart-menu .active');
			if(activ){activ.classList.remove('active')};
			el.classList.add('active');
			if(fcn){HWS.buffer[fcn]();}
		}
	}
	
	
	// функции для распечатки разных списков
	this.printList = function(sd){ sd = sd || {}
		let url = sd.url; // урл запроса к Серверу
		let name = sd.name || 'list'; // имя буфер функции ( котороя по КД выполняется в моменты обновления и пагинации )
		let dataD = sd.data || {}; // + к передоваемому запросу на сервер
		let fcn = sd.fcn; // функция распечатки и ответа от сервера, в конце необходим - return
		let clas = (sd.clas)? ' '+sd.clas : '' ; // просто доп класс для блока
		let topHtml = sd.topHtml || ''; //ХТМ который передаётся в шапку списка
		let target = HWS.getElem(sd.target);
		let block = HWhtml.tile('print-list'+clas);
		let oneElemFcn = sd.fcnOne || false;
		
		target.innerHTML = '';
		target.appendChild(block.b);
		
		HWS.buffer[name] = function(num){ num = num || 1;
			block.mid.innerHTML = HWhtml.loader();
			block.top.innerHTML = topHtml;
			let token = HWS.pageAccess;
			let lAmount = localStorage.getItem('AllPageAmount') || 10;
			let lStart = num.start || 0;
			let data = {token:token,list_start:lStart,list_amount:lAmount}
				data = Object.assign(data,dataD)
			HWF.post({url:url,data:data,fcn:function(info){
				//console.log(info);
				let lCount = info.item_count || 0;
				if(lCount == 1 && oneElemFcn){oneElemFcn(info);}
				else {
					block.mid.innerHTML = fcn(info);
					if(num == 1){block.bot.innerHTML = HWhtml.pagination({number:num,amount:lAmount,all:lCount,type:'buffer',load:name});}
				}
			}});
		}
		HWS.buffer[name]();
	}
	
	// выбор времени и периуда для разныш штук
	this.clickTimeChange = function(e,el){
		let block = HWS.upElem(el,'.block-control-graph');
		let fcn = block.dataset.fcn || '';
		let h = el.dataset.h || 1;
		HWS.upElem(el,'div').querySelector('.btn-select-time.active').classList.remove('active');
		el.classList.add('active');
		if(HWS.buffer[fcn]){HWS.buffer[fcn](h)}
	}	
	this.clickLoadTypeInfo = function(e,el){
		console.log(111);
		let block = HWS.upElem(el,'.block-control-graph');
		let h = block.querySelector('.statistic_time_wiev.active').dataset.h;
		let type = block.querySelector('.select_type_format ').value;
		let fcnl = block.dataset.fcnl || '';
		if(HWS.buffer[fcnl]){HWS.buffer[fcnl](h,type)}
	}
	// Функция глобального обновления всего сайта
	this.globalReolad = function(){
		let iconSystemConect = HWS.getElem('#system-icon-global-conection');
		let token1 = HWS.pageAccess || localStorage.getItem('PageAccess') || false;
		
		function reload(){
			let token12 = HWS.pageAccess || localStorage.getItem('PageAccess') || false;
			HWF.post({url:'user/info_about_systems',data:{token:token12},fcn:function(info){
				if(info.count > 0){
					if(info.count_offline > 0){iconSystemConect.classList.remove('of'); iconSystemConect.classList.remove('green');}
					else {iconSystemConect.classList.remove('of'); iconSystemConect.classList.add('green');}
				} else {iconSystemConect.classList.add('of');}
			}});
		}
		
		if(token1){reload();}
		else {console.log('Точка в облачке НЕОБНОВЛЯЕТСЯ, отсутствие токена пользователя!')}
		setInterval(reload,10000);
	}
	
	this.scrolDuoChange = function(target,min,max){
		min = min || false; max = max || false;
		let scrol = HWS.getElem(target);
		let tofix = scrol.dataset.tofix || 1;
		let minPred = parseFloat(scrol.dataset.min) || 0;
		let maxPred = parseFloat(scrol.dataset.max) || 100;
		let kof = parseFloat((100/(maxPred-minPred)).toFixed(4));

		if(min){
			let lineMin = scrol.querySelector('.scale-temp .line-color-1');
			let inpMin = scrol.querySelector('.right .val_min .btn-int .scrol_inp_num_val');
			let dopLineMin = scrol.querySelector('.scale-temp .val_1');
			let numMinL = parseFloat(min);
			let sumMin = (((numMinL-minPred)*kof).toFixed(4))+'%';
			let sumPrint = numMinL.toFixed(tofix);
			
			lineMin.style.width = sumMin;
			dopLineMin.style.left = sumMin;
			dopLineMin.querySelector('.btn-line.top').innerHTML = sumPrint;
			dopLineMin.querySelector('.btn-line.bot').innerHTML = sumPrint;
			inpMin.value = sumPrint;
		}
		if(max){
			let lineMax = scrol.querySelector('.scale-temp .line-color-2');	
			let inpMax = scrol.querySelector('.right .val_max .btn-int .scrol_inp_num_val');
			let dopLineMax = scrol.querySelector('.scale-temp .val_2');
			let numMaxL = parseFloat(max);
			let sumMax = (((maxPred-numMaxL)*kof).toFixed(4))+'%';
			let sumPrintM = numMaxL.toFixed(tofix);
			
			lineMax.style.width = sumMax;
			dopLineMax.style.left = (100-parseFloat(sumMax))+'%';
			dopLineMax.querySelector('.btn-line.top').innerHTML = sumPrintM;
			dopLineMax.querySelector('.btn-line.bot').innerHTML = sumPrintM;
			inpMax.value = sumPrintM;
		}
	}
	
	this.randomInteger = function(min, max) {
		let rand = min - 0.5 + Math.random() * (max - min + 1);
		return Math.round(rand);
	}
	
	this.getStateColor = function(state) { state = parseInt(state) || 0;
		let color = HWD.color.grey;
		if(state == 0){color = HWD.color.grey}
		if(state == 1){color = HWD.color.red}
		if(state == 3){color = HWD.color.red}
		if(state == 5){color = HWD.color.green}
		if(state == 8){color = HWD.color.green}
		if(state == 9){color = HWD.color.green2}
		//if(state == 9){color = HWD.color.red}
		if(state == 10){color = HWD.color.red}
		
		if(state == 11){color = HWD.color.red}
		if(state == 12){color = HWD.color.green}
		if(state == 13){color = HWD.color.green}
		if(state == 14){color = HWD.color.grey}
		if(state == 15){color = HWD.color.blue}
		if(state == 16){color = HWD.color.grey}
		if(state == 17){color = HWD.color.grey2}
		if(state == 18){color = HWD.color.green}
		if(state == 19){color = HWD.color.orange}
		if(state == 21){color = HWD.color.red}
		if(state == 31){color = HWD.color.blue}
		if(state == 30){color = HWD.color.grey}
		
		if(state == 111){color = HWD.color.grey}
		if(state == 112){color = HWD.color.red}
		if(state == 113){color = HWD.color.grey}
		if(state == 114){color = HWD.color.green}
		if(state == 115){color = HWD.color.grey}
		if(state == 116){color = HWD.color.green}
		if(state == 117){color = HWD.color.grey}
		if(state == 118){color = HWD.color.blue}
		if(state == 119){color = HWD.color.red}
		if(state == 120){color = HWD.color.grey}
		if(state == 121){color = HWD.color.grey}
		if(state == 122){color = HWD.color.blue}
		if(state == 123){color = HWD.color.red}
		if(state == 124){color = HWD.color.grey}
		if(state == 125){color = HWD.color.grey}
		if(state == 126){color = HWD.color.blue}
		if(state == 127){color = HWD.color.red}
		if(state == 128){color = HWD.color.grey}
		
		if(state == 255){color = HWD.color.grey}
		if(state == 254){color = HWD.color.red}

		return color
	}
	
	this.printModalAddSystem = function(){
		// Модальное добавление системы
		function print_modal_add_system(){
			let print = '';
			
			print+= '<div class="flex_center_left"><span class="block_pb_15 block_pl_20 block_pr_10">Введите серийный номер</span>';
			print+= '<p class="ta_left"><input id="imei_system" class="ts_12" type="text" maxlength="15" placeholder="15 цифр или 12 символов">';
			print+= '<br><span id="modal-error-system-add" class="tc_red"></span></p>';
			print+= '</div>';
			
			print+= '<div class="flex_center_top block_mt_20 block_w_650p">';
			
				print+= '<div class="flex_center_left block_w_50 box-sizing-box border_right block_p_20">';
				print+= '<p class="ta_left">Серийный номер указан на этикетке  <br> в нижней части корпуса</p>';
				print+= '<img class="block_w_100 block_mt_10" src="img/modal-add-img-1.png">';
				print+= '</div>';
				
				print+= '<div class="flex_center_left block_w_50 box-sizing-box block_p_20">';
				print+= '<p class="ta_justify">Если система далеко, но она использует SIM-карту, отправьте ей SMS с текстом V</p>';
				print+= '<img class="block_w_100 block_mt_10" src="img/modal-add-img-2.png">';
				print+= '</div>';
				
			print+= '</div>'
			
			HWF.modal.confirm('Добавление системы',print,'add-new-system-emai','Добавить','','of');
		}
		// Модальное добавление системы Удалось
		function print_modal_add_system_success(){
			let emai = HWS.buffer.emaiSystem  || '';
			let print = 'Система <b>'+emai+'</b> и все подключенные <br> устройства добавлены в ваш аккаунт!';
			
			HWF.modal.confirmCust({
				title:'Получилось!',
				print:print,
				yes:'К оборудованию',
				no:'Добавить еще',
				classNo:'rm-add-system',
				clouseNo:'1',
			});
		}
		// Такая система уже етсь
		function print_modal_add_system_yes_sys (){
			let emai = HWS.buffer.emaiSystem  || '';
			let print = 'Система <b>'+emai+'</b> уже присутствует  <br> в вашем аккаунте, проверьте правильность серийного номера (IMEI)';
			
			HWF.modal.confirmCust({
				title:'Такая система уже есть!',
				print:print,
				yes:'Ввести снова',
				classYes:'rm-add-system',
				clouseYes:'1',
			});
		}
		// Система принадлежит не вам
		function print_modal_add_system_not_you (){
			let emai = HWS.buffer.emaiSystem  || '';
			let print = 'К сожалению, кто-то другой уже добавил систему <b>'+emai+'</b> ранее. <br>\
					Если система у вас в руках, пожалуйста, обратитесь в службу технической поддержки.';
			
			HWF.modal.confirmCust({
				title:'Эта система в другом аккаунте!',
				print:print,
				yes:'Ввести снова',
				classYes:'rm-add-system',
			});
		}
		
		// Нажатие на добавить систему
		HWS.on('click','rm-add-system',function(e,el){ print_modal_add_system();	});	
		// Принять емай
		HWS.on('click','add-new-system-emai',function(e,el){
			let emai = HWS.getElem('#imei_system').value; //emai = parseInt(emai).toString();
			emai = emai.split('-').join('');
			emai = emai.split(' ').join('');
			emai = emai.toUpperCase();
			emai = emai.split('I').join('1');
			emai = emai.split('O').join('0');

			if(emai.length == 15 || emai.length == 12){
				HWS.buffer.emaiSystem = emai;
				HWF.modal.loader('Добавление системы');
				
				let dataP = {token:HWS.pageAccess,ident:emai}
				HWF.post({url:HWD.api.gets.register3xSystem,data:dataP,fcn:function(inf){
					console.log(inf);
					if(inf.status == "success"){ print_modal_add_system_success() } // всё хорошо
					if(inf.status == "wait_ust"){ print_modal_add_system_not_you() } // система в чужом акаунте
					if(inf.status == "already_exist_in_this_account"){ print_modal_add_system_yes_sys() } // система у вас уже есть
				}});
			}
			else {HWS.getElem('#modal-error-system-add').innerHTML = 'Неправильный серийный номер (IMEI)';}
		});
		
		print_modal_add_system();
	}
}
// Для работы с главной страницей
function mainPage(){
	let timeC = 0,timeId = 0, timeI = 0;
	let mainG = this;
	// нажатия на картинку
	this.imgClick = function(e,elem){
		let active = document.body.querySelector('.container .open');
		if(active){active.classList.remove('open');}
	}
	// Правое нажатия на картинку 
	this.imgClickR = function(event,elem){
		HWF.clearFilter();
		let menu = document.querySelector('.right-click-menu');
		let target = document.querySelector('.target-click');
		let CX = event.offsetX || 0, CY = event.offsetY || 0;
		let EW = 100/elem.offsetWidth, EH = 100/elem.offsetHeight;
		let x = (CX*EW).toFixed(4), y = (CY*EH).toFixed(4);
		
		menu.classList.add('active');
		menu.style.visibility = 'hidden';
		
		let xr = event.clientX, ww = document.documentElement.clientWidth;
		let yr = event.clientY, wh = document.documentElement.clientHeight;
		let wr = menu.offsetWidth; 
		let hr = menu.offsetHeight;
		
		if(xr+wr > ww){xr = ww-wr}
		if(yr+hr > wh){yr = wh-hr}
		
		menu.style.left = xr+"px";		
		menu.style.top = yr+"px";
		menu.style.visibility = 'visible';
		
		HWS.data.xImg = x; HWS.data.yImg = y;
		if (!target){
			target = document.createElement('div'); 
			target.classList.add('target-click');
			target.classList.add('tc_orange');
			HWS.blocks.modal.data.body.append(target);
		}
		
		target.dataset.x = x; target.dataset.x = y;
		target.innerHTML = x+' - '+y;
	}
	// нажатие на кнопку добавить "+"
	this.btnAddElem = function (e,el){
		let xr = (e.clientX-200);
		let yr = e.clientY;
		HWS.data.xImg = 8,HWS.data.yImg = 8;
		let menu = document.querySelector('.right-click-menu'); console.log(menu);

		if (menu.style.top == yr+'px' && menu.style.left == xr+'px'){
			if (menu.classList.contains('active')){menu.classList.remove('active');}
			else {menu.classList.add('active');}
		}else {
			menu.classList.add('active');
			menu.style.top = yr+'px';
			menu.style.left = xr+'px';
		} 
	}
	// открытие дополнительного меню у элемента
	this.elemClick = function(e,elem){
		let controlTime = new Date().getTime();
		let block = HWS.parentSearchClass(elem,'info-block');
		
		if (controlTime < timeC+200){clearTimeout(timeId);dbClick();}
		else {timeId = setTimeout(Click,210);}
		timeC = controlTime;
		// 1 нажатие на элемент
		function Click(){
			if (!block.classList.contains('open')){
				let active = document.body.querySelector('.container .open');
				if(active){active.classList.remove('open');}
				block.classList.add('open');
			}
			else{block.classList.remove('open');}
		}
		// 2 нажатия на элемент
		function dbClick(){
			let id = block.dataset.id;
			let type = block.dataset.type;
			// если реле
			if (type == 'equp' && (block.classList.contains('state-off') || block.classList.contains('state-on'))) {on_off_rele(elem);}
			// если проект
			if (type == 'proj') { HWS.go(HWD.page.proj+'/'+id);}
			// если группа
			//if (type == HWT.gp) { HWS.go(HWS.data.page.group+'/'+id);}
		}
	}
	// закрыть элемент
	this.clouse = function(e,elem){
		let newObj = HWS.data.elem.project_data
		let block = HWS.parentSearchClass(elem,'info-block');
		let type = block.dataset.type;
		let list = 'equipments';
			if(type == 'proj'){list = 'projects'}
			if(type == 'prog'){list = 'programs'}
		
		let idEl = block.dataset.id;
		block.style.transition = 'all 0.5s';
		block.style.opacity = '0';
		setTimeout(function(){block.parentNode.removeChild(block);},500);
		
		delete newObj[list][idEl];
		HWF.post({url:'projects/change_project_data',data:{token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:newObj}});
	}	
	// движение блока
	this.move = new function(){
		let S={},K={},x=0,y=0,block;
		let Xmax = 90,Xmin = 4;
		let Ymax = 85,Ymin = 0;
		let control = false;
		let tepe = false;
		this.s = function(event){start(event); control = true; tepe = 'move';}
		this.r = function(event){start(event); control = true; tepe = 'resize';}
		this.m = function (event){ 
			if (control) {
				x = event.pageX || event.targetTouches[0].pageX || e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
				y = event.pageY || event.targetTouches[0].pageY || e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
				if(event.cancelable){event.preventDefault();event.stopPropagation();}
				
				if (tepe=='move'){
					x = ((x - S.x)*K.x)+S.l; y = ((y - S.y)*K.y)+S.t;
					block.style.left = x+'%'; block.style.top = y+'%';
				}
				if (tepe=='resize'){
					x = ((x - S.x)*K.x)+S.w; y = ((y - S.y)*K.y)+S.h;
					if((x/K.x)<=100){x=100*K.x}if((y/K.y)<=60){y=60*K.y}
					block.style.width = x+'%'; block.style.height = y+'%';
				}
			}
		}
		this.e = function(event){
			control = false;
			if(block){
				let newObj = HWS.data.elem.project_data;
				let id = block.dataset.id;
				let type = block.dataset.type;
				let list = 'equipments';
				if(type == 'proj'){list = 'projects'}
				if(type == 'prog'){list = 'programs'}
				
				//if((S.l-x) > 1 || (S.l-x) < -1 || (S.t-y) > 1 || (S.t-y) < -1){
					if (tepe=='move'){
						newObj[list][id].y = parseFloat(y.toFixed(4));
						newObj[list][id].x = parseFloat(x.toFixed(4));
					}
					if (tepe=='resize'){
						newObj[list][id].height = parseFloat(y.toFixed(4));
						newObj[list][id].width = parseFloat(x.toFixed(4));
					}
					
					HWF.post({url:'projects/change_project_data',data:{token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:newObj}});
				//}
				block = false;
			}
		}
		
		function start(event){
			let elemT = event.target || event.targetTouches[0].target;
			let container = HWS.parentSearchClass(elemT,'container');
			block = HWS.parentSearchClass(elemT,'info-block');
			S.x = event.pageX || event.targetTouches[0].pageX; 
			S.y = event.pageY || event.targetTouches[0].pageY;
			S.l = parseFloat(parseFloat(block.style.left).toFixed(4));
			S.t = parseFloat(parseFloat(block.style.top).toFixed(4));
			S.w = parseFloat(parseFloat(block.style.width).toFixed(4)) || 0;
			S.h = parseFloat(parseFloat(block.style.height).toFixed(4)) || 0;
			K.x = parseFloat((100/container.offsetWidth).toFixed(4));
			K.y = parseFloat((100/container.offsetHeight).toFixed(4));
			x = S.l;
			y = S.t;
		}
	}
	// добавление элементов
	this.add = {
		proj:function(event,elem){
			let li = HWS.upElem(elem,'.one-project');
			let check = li.querySelector('.equ_check .checkbox');
			if(check.classList.contains('active')){check.classList.remove('active')}
			else{check.classList.add('active')}
		},
		projBtn:function(event,elem){
			let allActive = document.body.querySelectorAll('.modal-section .table-content .list-projects .checkbox.active');
			let newObj = HWS.data.elem.project_data;
			if(!newObj.projects){newObj.projects = {}};
			let x = parseFloat(HWS.data.xImg),y = parseFloat(HWS.data.yImg);
			let testGet = false;
			
			if(allActive.length > 0){
				let container = HWS.getElem('#ecto');
				let specObj = {};
				let allList = HWS.buffer.equpsList;

				HWF.recount(allActive,function(elem){specObj[HWS.upElem(elem,'.one-project').dataset.id] = 1;},'on');
				
				HWF.recount(allList,function(elemList){if(specObj[elemList.id]){
					x = parseFloat(x.toFixed(4)); y = parseFloat(y.toFixed(4));
					newObj.projects[elemList.id] = {id:elemList.id,x:x,y:y}
					elemList.x = x; elemList.y = y;
					let block = HWhtml.TEMP(elemList,'proj');
					container.appendChild(block);
					x = x+1; y = y+1;
				}});
				
				HWF.post({url:'projects/change_project_data',data:{token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:newObj}});
				add_move();
				HWF.modal.message('Проекты добавлены','Добавление');
			}
		},
		equip:function(event,elem){
			let li = HWS.upElem(elem,'li');
			let check = li.querySelector('.checkbox');
			if(check.classList.contains('active')){check.classList.remove('active')}
			else{check.classList.add('active')}
		},
		equipBtn:function(event,elem){
			let allActive = document.body.querySelectorAll('.modal-section .table-content .list-equipments .checkbox.active');
			let newObj = HWS.data.elem.project_data;
			if(!newObj.equipments){newObj.equipments = {}};
			let x = parseFloat(HWS.data.xImg),y = parseFloat(HWS.data.yImg);
			
			if(allActive.length > 0){
				let container = HWS.getElem('#ecto');
				let specObj = {};
				let allList = HWS.buffer.equpsList;
				
				HWF.recount(allActive,function(elem){specObj[HWS.upElem(elem,'.one-equipment').dataset.id] = 1;},'on');
				HWF.recount(allList,function(elemList){if(specObj[elemList.id]){
					x = parseFloat(x.toFixed(4)); y = parseFloat(y.toFixed(4));
					newObj.equipments[elemList.id] = {id:elemList.id,x:x,y:y}
					elemList.x = x; elemList.y = y;
					let block = HWhtml.TEMP(elemList,'equp');
					container.appendChild(block);
					x = x+1; y = y+1;
				}});
				
				let specD = {token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:newObj};
				HWF.post({url:'projects/change_project_data',data:specD});
				add_move();
				
				
				// следующее модалное окно 
				let projectID = HWS.data.elem.id;
				
				HWF.modal.print('Добавить Оборудование','<div id="modal-fr-list" class="table-content block_w_100 block_h_85"> '+HWhtml.loader()+'</div>,<div class="button add_fr_equp_btn">Добавить</div><br><br>');
				
				HWF.printList({name:'printListMyFrBuffer',url:HWD.api.gets.projListUser,target:'#modal-fr-list',
				data:{id_project:projectID},
				clas:'block_w_100',
				fcn:function(inf){
					HWS.blocks.modal.data.title.append('Добавить доступы');
					if(inf.list.length > 0){
						let list = inf.list;
						let html = '<ul id="list-proj-friends" class="list list-equipments block_pt_20">';
						
						html+= '<li class="one-head sticky">';
						html+= '<div class="l-mail">Емайл</div>';
						html+= '<div class="l-name ta_left">Имя</div>';
						html+= '<div class="l-btn block_w_100p flex_between">\
							<div class="btn_text block_w_20p ta_center fr_select_r">r</div>\
							<div class="btn_text block_w_20p ta_center fr_select_w">w</div>\
							<div class="btn_text block_w_20p ta_center fr_select_x">x</div>\
						</div>';
						html+= '<div class="l-btn"><span class="btn_text equp_select_all">Всё</span></div>';
						html+= '</li>';
						HWF.recount(list,function(el){
							html+= '<li data-id="'+el.id_user+'" class="one_fr">';
							html+= '<div class="l-mail">'+el.email+'</div>';
							html+= '<div class="l-name ta_left">'+el.name+'</div>';
							html+= '<div class="l-btn block_w_100p flex_between">\
								<div class="btn-active active checkbox-empty click_acse click_acse_r">r</div>\
								<div class="btn-active checkbox-empty click_acse click_acse_w">w</div>\
								<div class="btn-active checkbox-empty click_acse click_acse_x">x</div>\
							</div>';
							html+= '<div class="l-btn ta_right"><div class="btn-active active checkbox"></div></div>';
							html+= '<span class="click btn-one-equip"></span>';
							html+= '</li>';
						});
						html+='</ul>';
						
						HWS.on('click','fr_select_r',function(){HWF.allActive('#list-proj-friends','.click_acse_r'); mass_rwx();});
						HWS.on('click','fr_select_w',function(){HWF.allActive('#list-proj-friends','.click_acse_w'); mass_rwx();});
						HWS.on('click','fr_select_x',function(){HWF.allActive('#list-proj-friends','.click_acse_x'); mass_rwx();});
						
						return html;
					}
					else {HWF.modal.message('Оборудование добавлено','Добавление');}
				}});
			}
			else {console.log('no element')}
		},
		equipBtnfr:function(event,elem){ // добавление Доступов к пользователю
			let allLi = document.body.querySelectorAll('#list-proj-friends .one_fr');
			let token = HWS.pageAccess;
			let projectID = HWS.hash[1];
			let dataD =[];
			let equpList= HWS.buffer.equpListFrControl;
			HWF.modal.print('Добавление',HWhtml.loader());
			if(allLi.length > 0){
				HWF.recount(allLi,function(li){
					let id = li.dataset.id;
					let r = li.querySelector('.click_acse_r');
					let w = li.querySelector('.click_acse_w');
					let x = li.querySelector('.click_acse_x');
					
					let security = '';
					(r.classList.contains('active'))? security+= 'r' : security+= '-';
					(w.classList.contains('active'))? security+= 'w' : security+= '-';
					(x.classList.contains('active'))? security+= 'x' : security+= '-';
					
					HWF.recount(equpList,function(eq){dataD.push({id_user:id,id_object:eq,security:security})});
				},'on');
				
				let data = {token:token,data:dataD}
				// ДОбавление Оборудования другу idList.push({id_user:idUser,id_object:id,security:'r--'});
				HWF.post({url:HWD.api.gets.objectsSecurity,data:data,fcn:function(){
					HWF.modal.message('Добавлено','Добавление');
				}});
			} else{HWF.modal.message('Добавлено','Добавление');}
		},
		prog:function(event,elem){
			let li = HWS.upElem(elem,'.one-program');
			let check = li.querySelector('.equ_b .settings-butt .checkbox');
			if(check.classList.contains('active')){check.classList.remove('active')}
			else{check.classList.add('active')}
		},
		progBtn:function(event,elem){}
	}
	// открытие модальных окон 
	this.modal = {
		objPin:function(){
			let print = ''+
			'<div class="filter block_w_100 ta_center">'+HWhtml.search()+'</div>'+
			'<div class="table-content block_w_100 block_h_85 all-equipments"></div>'+
			//'<div class="style_btn orange block_w_200p add_more_proj_btn">Добавить</div>'+
			'';
			HWF.modal.print('Добавить Проект',print);
			HWF.print.projects('.modal-window .modal-section .table-content');
			HWF.modal.on();HWmain.ofRMenu();
			
			// выбор сазу всех элементов
			HWS.on('click','proj_select_all',function(event,elem){
				HWF.allActive('.list-projects','.checkbox');
			});
		},
		obj:function(){
			let print = ''+
			'<div class="filter block_w_100 ta_center">'+HWhtml.search('','btn_search_text_modsl_proj')+'</div>'+
			'<div class="table-content block_w_100 block_h_85 all-equipments"></div>'+
			'<div class="style_btn orange block_w_200p add_more_proj_btn">Добавить</div>'+
			'<br><br>';
			HWF.modal.print('Добавить Проект',print);
			
			HWS.data.apiGet.listProjectsProj.data.id_project = HWS.buffer.elemLoadInfo.id;
			HWF.print.listProjectsProj('.modal-window .modal-section .table-content',{type:'checkbox'});
			HWF.modal.on();HWmain.ofRMenu();
			
			// выбор сазу всех элементов
			HWS.on('click','proj_select_all',function(event,elem){
				HWF.allActive('.list-projects','.checkbox');
			});
			// Текстовый поиск
			HWS.on('change','btn_search_text_modsl_proj',function(e,el){
				HWS.data.apiGet.listProjectsProj.data.text = el.value;
				HWS.data.apiGet.listProjectsProj.data.id_project = HWS.buffer.elemLoadInfo.id;
				HWF.print.listProjectsProj('.modal-window .modal-section .table-content',{type:'checkbox'});
			});
		},
		equip:function(){
			let print = ''+
			'<div class="filter block_w_100 ta_center">'+HWhtml.search('','btn_search_text_modsl_equp')+'</div>'+
			'<div class="table-content block_w_100 block_h_85 all-equipments"></div>'+
			'<div class="style_btn orange block_w_200p add_more_equp_btn">Добавить</div>'+
			'<br><br>';
			HWF.modal.print('Добавить Оборудование',print);
			HWS.data.apiGet.listProjectsEqup.data.id_project = HWS.buffer.elemLoadInfo.id;
			HWF.print.listProjectsEqup('.modal-window .modal-section .table-content');
			HWmain.ofRMenu();
			
			// выбор сазу всех элементов
			HWS.on('click','equp_select_all',function(event,elem){
				HWF.allActive('.list-equipments','.checkbox');
			});
			// Текстовый поиск
			HWS.on('change','btn_search_text_modsl_equp',function(e,el){
				HWS.data.apiGet.listProjectsEqup.data.search_text = el.value;
				HWS.data.apiGet.listProjectsEqup.data.id_project = HWS.buffer.elemLoadInfo.id;
				HWF.print.listProjectsEqup('.modal-window .modal-section .table-content');
			});
		},
		prog:function(){
			let print = ''+
			'<div class="filter block_w_100 ta_center">'+HWhtml.search()+'</div>'+
			'<div class="table-content block_w_100 block_h_85 all-equipments"></div>'+
			'<div class="button add_more_prog_btn">Добавить</div>'+
			'<br><br>';
			
			HWF.modal.print('Добавить Программу',print);
			HWprog.list('.modal-window .modal-section .table-content','checkbox');
			HWF.modal.on();HWmain.ofRMenu();
			
			// выбор сазу всех элементов
			HWS.on('click','prog_select_all',function(event,elem){
				HWF.allActive('.list-programs','.checkbox');
			});
		},
		clear:function(e,elem){
			let type = HWS.data.elem.project_type;
			let newObj = HWS.data.elem.project_data;

			if(type == 1) { // img
				let allE = document.querySelectorAll('.main-page .container .info-block');
				allE = Array.apply(null, allE);
				for (let key in allE){allE[key].remove();}
				newObj.equipments = {};
				HWF.post({url:'projects/change_project_data',data:{token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:newObj}});
			}
			if(type == 2) { // map
				let allE = document.querySelectorAll('.main-page .ecto-map .ecto-pin-map');
				allE = Array.apply(null, allE);
				for (let key in allE){allE[key].remove();}
				newObj.equipments = {};
				HWF.post({url:'projects/change_project_data',data:{token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:newObj}})
			}
			if(type == 3) { // grid
				let allE = document.querySelectorAll('.main-page .container .info-block');
				allE = Array.apply(null, allE);
				for (let key in allE){allE[key].remove();}
				newObj.equipments = {};
				HWF.post({url:'projects/change_project_data',data:{token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:newObj}});
			}
		}
	};
	// Правый клик по карте
	this.rClick = function(e){
		let menu = document.querySelector('.right-click-menu');
		let targetClick = document.querySelector('.target-click');
		let x = e.originalEvent.pageX;
		let y = e.originalEvent.pageY;
		let lat = e.latlng.lat; // ширина
		let lng = e.latlng.lng; //долгота
		if (!targetClick){
			targetClick = document.createElement('div'); 
			targetClick.classList.add('target-click');
			targetClick.classList.add('tc_orange');
			HWS.blocks.modal.data.body.append(targetClick);
		}
		
		menu.classList.add('active');
		menu.style.visibility = 'hidden';
		
		let xr = event.clientX, ww = document.documentElement.clientWidth;
		let yr = event.clientY, wh = document.documentElement.clientHeight;
		let wr = menu.offsetWidth; 
		let hr = menu.offsetHeight;
		
		if(xr+wr > ww){xr = ww-wr}
		if(yr+hr > wh){yr = wh-hr}
		
		menu.style.left = xr+"px";		
		menu.style.top = yr+"px";
		menu.style.visibility = 'visible';
		
		HWS.data.latMap = lat; HWS.data.lngMap = lng;
		targetClick.dataset.lat = lat;
		targetClick.dataset.lng = lng;
		targetClick.innerHTML = lat+' - '+lng;
	}
	this.ofRMenu = function(){
		let menu = document.querySelector('.right-click-menu');
		if(menu){menu.classList.remove('active');}
	}
	// вкл выкл для блока реле
	this.onoff = function(e,elem){on_off_rele(elem);}
	//открывает дополнительное окно оборудование в разделе груп
	this.btnOpenGroupEqup = function(e,elem){
		let elemLi = HWS.parentSearchClass(elem,'one-elem');
		let target = HWS.getElem(elemLi.dataset.load);
		
		target.innerHTML = '';
		let print = '<p class="tc_red">ФЭЙК!!!!!</p>';
		let obj = {
			0:{name:'eq-1',id:'id-1'},
			1:{name:'eq-2',id:'id-2'},
			2:{name:'eq-3',id:'id-3'},
			3:{name:'eq-4',id:'id-4'},
			4:{name:'eq-5',id:'id-5'},
			5:{name:'eq-6',id:'id-6'},
			6:{name:'eq-7',id:'id-7'},
			7:{name:'eq-8',id:'id-8'},
			8:{name:'eq-9',id:'id-9'}
		}
		
		let tmpOne = '';
			tmpOne += '<li class="one-equp" data-id="{{id}}">';
			tmpOne += '<div class="name"><a href="#equipment/{{id}}">{{name}}</a></div>';
			tmpOne += '<div class="id">{{id}}</div>';
			tmpOne += '</li>';
			
			print += '<div class="table-mid flex_center_top"><ul class="list"><li class="one-head">';
			print += '<div class="name">Имя</div>';
			print += '<div class="id">id</div>';
			print += '<div class="but">Кнопки</div>';
			print += '</li>';
			print += HWhtml.list(obj,tmpOne);
			print += '</ul></div>';
			
		target.innerHTML = print;
	}
	//КАРТИНКА загрузка страници
	this.loadImg = function(ob){ console.log(ob)
		HWS.data.elem = ob;
		let proJprontIter = 0;
		let token = HWS.pageAccess;
		let projectId = ob.id;
		let obj = ob.project_data || {};
		let controlAddMove = 0;
		let img=0, loadEqup=[],loadProj=[],loadProg=[], amout=100, spType = 'equp',projType = 'project',progType = 'program';
		let clouseName = ob.project_data.clouse_name || 0;
		let container = document.createElement('div');
			container.id = "ecto";
			container.classList.add('container');
		if (clouseName == 1) {container.classList.add('clouse_name');}
				
		let rMenu = document.createElement('div');
			rMenu.classList.add('right-click-menu');
			rMenu.innerHTML = HWhtml.rMenuImg();
		
		if(obj.img && obj.img.src){
			img = document.createElement('img');
			img.src = serverURL+'uploads/'+obj.img.src;
			img.classList.add('background-img');
			container.appendChild(img);
		}else {container.classList.add('container-grid');}
		
		let RTmenu = document.querySelector('#spec-dop-mini-menu');
		let equipments = obj.equipments;
		let projects = obj.projects;

		// Если есть элементы ОБОРУДОВАНИЕ для отрисовки
		if(equipments && JSON.stringify(equipments) != "{}"){
			let getAllElemEqup = [];
			for (let key in equipments){ getAllElemEqup.push(key);}
			
			HWF.post({url:HWD.api.gets.list,data:{token:HWS.pageAccess,list_start:0,list_amount:amout,id_object:getAllElemEqup},fcn:function(inf){
				let fragmentEqup = document.createDocumentFragment();
				let list = inf.list;
				HWF.recount(list,function(oneEl){
					let elemList = oneEl; elemList.x = equipments[oneEl.id].x; elemList.y = equipments[oneEl.id].y;
					let oneBlock = HWhtml.TEMP(elemList,'equp');
					fragmentEqup.appendChild(oneBlock);
				});
				container.appendChild(fragmentEqup);
				printProj();
			}});
		}// Нету элементов для отрисовки
		else { printProj(); }
		
		// Если есть элементы ПРОЕКТОВ для отрисовки
		if(projects && JSON.stringify(projects) != "{}"){
			let getAllElemProj = [];
			for (let key in projects){ getAllElemProj.push(key);}
			HWF.post({url:'projects/get_projects_from_id_project',data:{token:HWS.pageAccess,id_project:getAllElemProj},fcn:function(inf){
				let fragmentEqup = document.createDocumentFragment();
				let list = inf.list;
				HWF.recount(list,function(oneEl){
					let elemList = oneEl; 
						elemList.x = projects[oneEl.id].x || 10; 
						elemList.y = projects[oneEl.id].y;
						elemList.width = projects[oneEl.id].width || '';
						elemList.height = projects[oneEl.id].height || '';
					let oneBlock = HWhtml.TEMP(elemList,'proj');
					fragmentEqup.appendChild(oneBlock);
				});
				container.appendChild(fragmentEqup);
				printProj();
			}});
		}// Нету элементов для отрисовки
		else { printProj(); }
		
		//printProj();
		
		// Отрисовка всего проекта.
		function printProj(){
			proJprontIter++;
			if(proJprontIter == 2){
				HWS.blocks.content.append('');
				RTmenu.innerHTML = ''+
					'<span class="icon icon-clear-window hover btn_clear_data" title="Очистить всё"></span>'+
					'<a href="#'+HWD.page.proj+'/'+projectId+'/edit" class="icon icon-settings hover" title="Редактировать"></a>'+
					'<span class="icon icon-open-window hover btn_open_full_mainw" title="Расскрыть - Свернуть"></span>'+
					'<span class="btn_add_fast_tile btnIcon">\
						<span class="select-none">Добавить объект</span>\
						<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>\
					</span>'+
				'';
				HWS.blocks.title.append('<h1>'+ob.name+'</h1>');
				HWS.blocks.content.append(container);
				document.body.querySelector('main').appendChild(rMenu);
				add_move();
				HWF.timerUpdate(auto_update);
			}
		}
		// FUNCTION AUTO LOADER
		function load_project(inf){
			let equipments = inf.project_data.equipments;
			let projects = inf.project_data.projects;
			let container = HWS.getElem('#ecto');
			let windEqup =  container.querySelectorAll('.type-equp');
			let windProj =  container.querySelectorAll('.type-proj');
			let getAllElemEqup = []; for (let key in equipments){ getAllElemEqup.push(key);}
			let getAllElemProj = []; for (let key in projects){ getAllElemProj.push(key);}
			
			if(equipments && JSON.stringify(equipments) != "{}"){
				HWF.post({url:HWD.api.gets.list,data:{token:HWS.pageAccess,list_start:0,list_amount:amout,id_object:getAllElemEqup},fcn:function(newEqInf){
					let list = newEqInf.list; 
					// Добавлени и перемещение
					if (list.length > 0) {HWF.recount(list,function(elList){
						let normInfo = HWequip.normInfo(elList);
							normInfo.x = equipments[normInfo.id].x;
							normInfo.y = equipments[normInfo.id].y;
						let winEl = container.querySelector('#info-equp-'+normInfo.id);
						if(winEl){ if (!winEl.classList.contains('open')){
							let tX=parseFloat(winEl.style.left), tY=parseFloat(winEl.style.top);
							let name = winEl.querySelector('.ib-content .ib-center .ib-name').innerHTML;
							let data = winEl.querySelector('.ib-content .ib-center .ib-data');
							// winEl - элемент на поле 
							// normInfo - элемент от сервера ( уже с кординатами )
							// перемещение
							if(tX != normInfo.x || tY != normInfo.y){
								winEl.style.left = normInfo.x+'%';
								winEl.style.top = normInfo.y+'%';
							}
							// имя
							if(normInfo.name != name){winEl.querySelector('.ib-content .ib-center .ib-name').innerHTML = normInfo.name;}
							// аналоговые датчики 
							if(normInfo.tmp == "analog3x"){
								if(normInfo.state == "no_connect"){data.innerHTML = '<span class="icon icon-radio-0"></span>'}
								else{data.innerHTML = normInfo.val}
								data.style.background = normInfo.stateColor;
							} 
							// дискретные датчики 
							else if(normInfo.tmp == "discret3x"){ 
								let view = '<span class="icon icon-check wite"></span>';
								if (normInfo.state == 'alert') {view = '<span class="icon icon-alarm wite"></span>'}; 
								if(normInfo.state == "no_connect"){view = '<span class="icon icon-radio-0"></span>'}
								data.innerHTML = view;
								data.style.background = normInfo.stateColor;
							} 
							// устройства управления / реле  
							else if(normInfo.tmp == "control_device3x"){
								if(normInfo.val == 1){winEl.classList.remove('state-off'); winEl.classList.add('state-on');}
								else {winEl.classList.remove('state-on'); winEl.classList.add('state-off');}
								let dataP = '';
								if(normInfo.state == "no_connect"){dataP = '<span class="icon icon-radio-0"></span>'}
								else{dataP = '<span class="icon icon-btn"></span>'}
								if(normInfo.elem.lk && normInfo.elem.lk.control_locked) {winEl.classList.add('blocked');}
								else {winEl.classList.remove('blocked');}
								
								data.innerHTML = dataP;
							}
						}} else {
							elList.x = equipments[normInfo.id].x;
							elList.y = equipments[normInfo.id].y;
							container.appendChild(HWhtml.TEMP(elList,'equp'));
						}
					});}
					add_move();
				}});
			} 
			// удаление элемента с поля
			if (windEqup.length > 0) {HWF.recount(windEqup,function(elWin){
				let idWE = elWin.dataset.id; if(!equipments[idWE]){elWin.remove();}
			},'on')}
			
			if(projects && JSON.stringify(projects) != "{}"){
				HWF.post({url:'projects/get_projects_from_id_project',data:{token:HWS.pageAccess,id_project:getAllElemProj},fcn:function(newProj){
					let list = newProj.list; 
					// Добавлени и перемещение
					if (list.length > 0) {HWF.recount(list,function(elList){
						elList.x = projects[elList.id].x;
						elList.y = projects[elList.id].y;
						elList.width = projects[elList.id].width || 12;
						elList.height = projects[elList.id].height || 12;
						let winEl = container.querySelector('#info-proj-'+elList.id);
						if(winEl){ if (!winEl.classList.contains('open')){
							let tX=parseFloat(winEl.style.left), tY=parseFloat(winEl.style.top);
							let tW=parseFloat(winEl.style.width), tH=parseFloat(winEl.style.height);
							let name = winEl.querySelector('.ib-content .ib-center h4').innerHTML;
							// перемещение, изменение размеров
							if(tX != elList.x || tY != elList.y){winEl.style.left = elList.x+'%'; winEl.style.top = elList.y+'%';}
							if(tW != elList.width || tH != elList.height){winEl.style.width = elList.width+'%'; winEl.style.height = elList.height+'%';}
							// имя
							if(elList.name != name){winEl.querySelector('.ib-content .ib-center h4').innerHTML = elList.name;}
						}} else {
							elList.x = projects[elList.id].x;
							elList.y = projects[elList.id].y;
							elList.width = projects[elList.id].width;
							elList.height = projects[elList.id].height;
							container.appendChild(HWhtml.TEMP(elList,'proj'));
						}
					});}
					add_move();
				}});
			} 
			// удаление элемента с поля
			if (windProj.length > 0) {HWF.recount(windProj,function(elWin){
				let idWE = elWin.dataset.id; if(!projects[idWE]){elWin.remove();}
			},'on')}
			// #info-equp-82679
			// console.log(inf);
		}
		function auto_update(){
			let project = HWS.hash[1] || false;
			let token = HWS.pageAccess;
			if(!project){HWF.post({url:'projects/get_main_project',data:{token:token},fcn:load_project});}
			else {HWF.post({url:'projects/get_project',data:{token:token,id_project:project},fcn:load_project});}
		}
		
		// ДЕЙСТВИЕ
		HWS.on('contextmenu','container',HWmain.imgClickR); // нажатие правой кнопки
		HWS.on('click','btn-one-equip',HWmain.add.equip);// нажатие на 1 оборудования
		HWS.on('click','add_more_equp_btn',HWmain.add.equipBtn);// нажатие на кнопку добавить
		HWS.on('click','add_fr_equp_btn',HWmain.add.equipBtnfr);// нажатие на кнопку добавить доступы друзьям  
		HWS.on('click','btn-one-proj',HWmain.add.proj)// Нажатие на 1 элемент проекта
		HWS.on('click','add_more_proj_btn',HWmain.add.projBtn)// Добавление Проэктов
		HWS.on('click','btn-one-prog',HWmain.add.prog)// Нажатие на 1 элемент программы
		HWS.on('click','add_more_prog_btn',HWmain.add.progBtn)// Добавление Програм
		HWS.on('click','btn_add_fast_tile',HWmain.btnAddElem); // добавление на кнопку +
		// Очистка всего оборудование страници
		HWS.on('click','btn_clear_data',function(){
			HWF.modal.confirm('Очистить экран?','Это действие повлечет за собой удаление всех объектов из вашего проекта','btn_clear_data_yes');
		});	
		HWS.on('click','btn_clear_data_yes',HWmain.modal.clear);
		// Нажатие/управление на элементом
		HWS.on('click','ib-click',HWmain.elemClick);
		HWS.on('click','ib-clouse',HWmain.clouse); // закрыть 
		HWS.on('click','ib-onoff',HWmain.onoff); // вкл. выкл.
		HWS.on('pointerdown','ib-resize',HWmain.move.r);
		HWS.on('mousemove',container,HWmain.move.m);
		HWS.on('pointerup',container,HWmain.move.e);
		HWS.on('touchend',container,HWmain.move.e);
		
		HWS.on('pointerdown','container',HWmain.imgClick); // Закрыть открытые элементы
	}
	this.loadGrid = function(ob){ mainG.loadImg(ob) }
	// КАРТА загрузка страници
	this.loadMap = function(ob){
		HWS.blocks.content.append('');
		let projectId = ob.id;
		let obj = ob.project_data || {};
		
		let RTmenu = document.querySelector('#spec-dop-mini-menu');
		RTmenu.innerHTML = ''+
			'<span class="icon icon-clear-window hover btn_clear_data" title="Очистить всё"></span>'+
			'<a href="#'+HWD.page.proj+'/'+projectId+'/edit" class="icon icon-change hover" title="Редактировать"></a>'+
			'<span class="icon icon-open-window hover btn_open_full_mainw" title="Расскрыть - Свернуть"></span>'+
			'<span class="icon icon-set-view hover btn_set_zoom_map" title="Задать Вид" ></span>'+
			'<span class="btn_add_fast_tile btnIcon">\
				<span class="select-none">Добавить объект</span>\
				<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>\
			</span>'+
		'';

		let container = document.createElement('div');
			container.id = "ecto";
			container.classList.add('container');
			container.classList.add('ecto-map');
			HWS.blocks.content.append(container);
			
		let rMenu = document.createElement('div');
			rMenu.classList.add('right-click-menu');
			rMenu.innerHTML = HWhtml.rMenuMap();
			
		let wievMap = {lat:60.539811,lng:102.2617187,zoom:4.5};
		
		if (obj.zoom){
			wievMap.lat = obj.zoom.lat || 60.539811;
			wievMap.lng = obj.zoom.lng || 102.2617187;
			wievMap.zoom = obj.zoom.zoom || 4.5;
		}
		
		let map = L.map('ecto');
			map.options.crs = L.CRS.EPSG3395;
			map.setView([wievMap.lat,wievMap.lng],wievMap.zoom);
			map.on('contextmenu',HWmain.rClick);
			map.on('zoomanim',HWmain.ofRMenu);
		
		/*L.tileLayer(
		  'https://vec{s}.maps.yandex.net/tiles?l=map&v=4.55.2&z={z}&x={x}&y={y}&scale=2&lang=ru_RU', {
			subdomains:['01', '02', '03', '04'],
			attribution:'<a http="yandex.ru" target="_blank">Яндекс</a>',
			reuseTiles:true,
			updateWhenIdle:false,
		  }
		).addTo(map);	*/	
		
		L.tileLayer(
		  'http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
			subdomains:['mt0','mt1','mt2','mt3'],
			reuseTiles:true,
			updateWhenIdle:false,
		  }
		).addTo(map);
		
		HWS.data.map = map;
		HWS.data.elem = ob;
		
		if (obj.elements){for (let key in obj.elements){
			let clas = 'norm'
			let sum = '';
			if (projectId == 137) {
				sum = HWF.randomInteger(-12,12);
				if (sum < 1){sum = 0; clas = 'green';}
				else {sum = sum; clas = 'red'; console.log(sum);}
			}
			let elem = obj.elements[key];
			let lat = elem.lat; let lng = elem.lng;
			let html = HWhtml.pin(elem.id,elem.name,elem.num,sum);
			let div = L.divIcon({className:'ecto-pin-map '+clas, html:html});
			let marcer = L.marker([lat,lng],{icon:div}).addTo(map);
		}}
		
		// добовление пина Группы
		HWS.on('click','one_group',HWmain.add.pinGrup);
		// Закрытие активного пина
		HWS.on('pointerdown','container',function(event,elem){
			let pin = elem.querySelector('.ecto-pin-map.active');
			if(pin){pin.classList.remove('active');}
		});
		// добовление пина проекта
		HWS.on('click','btn-one-proj',function(event,elem){
			let elemLi = HWS.parentSearchClass(elem,'one-project');
			let id = elemLi.dataset.id;
			let name = elemLi.querySelector('.equ_n .text_name').innerHTML;
			let lat = HWS.data.latMap; // ширина
			let lng = HWS.data.lngMap; //долгота
			let newObj = HWS.data.elem.project_data
			let clas = 'norm';
			if(!newObj.elements){newObj.elements = {};}
			let newNum  = Object.getOwnPropertyNames(newObj.elements);
			if(newNum.length != 0) {newNum = newNum[newNum.length -1]; newNum = parseInt(newNum); newNum = newNum+1} else {newNum = 1}
			let html = HWhtml.pin(id,name,newNum);
			let div = L.divIcon({className:'ecto-pin-map '+clas, html:html});
			let marcer = L.marker([lat,lng],{icon:div}).addTo(HWS.data.map);
			
			newObj.elements[newNum] = {"id": id,"lat": lat,"lng": lng,"name": name,"num":newNum};

			HWF.post({url:'projects/change_project_data',data:{token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:newObj}});
			HWF.modal.off();
		});
		// Удаление пина
		HWS.on('click','pin-clouse',function(event,elem){
			let numb = elem.dataset.num || 0;
			let block = HWS.parentSearchClass(elem,'ecto-pin-map');
			let newObj = HWS.data.elem.project_data
			
			delete newObj.elements[numb];
			
			HWF.post({url:'projects/change_project_data',data:{token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:newObj}});
			block.parentNode.removeChild(block);
		}); 
		// Зафиксировать текущий ВИД
		HWS.on('click','btn_set_zoom_map',function(e,elem){
			obj.zoom = {
				lat:HWS.data.map.getCenter().lat,
				lng:HWS.data.map.getCenter().lng,
				zoom:HWS.data.map.getZoom(),
			}
			
			HWF.post({url:'projects/change_project_data',data:{token:HWS.pageAccess,id_project:HWS.data.elem.id,project_data:obj},fcn:function(){
				HWF.push('Вид задан');
			}});
		});
		// сообщение о помощи
		HWS.on('click','btn_add_fast_tile',function(){
			HWF.infoMsg('Используйте <b>правый клик</b>, или <b>зажатие пальцем</b>, на нужном участке карты',{top:85,left:60});
		});
		// Очистка всего оборудование страници
		HWS.on('click','btn_clear_data',function(){HWF.modal.confirm('Очистить карту?','Это действие повлечет за собой удаление всех объектов из вашего проекта');});	
		HWS.on('click','btn-yes-modal',HWmain.modal.clear);
		
		HWS.blocks.title.append('<h1>'+ob.name+'</h1>');
		HWS.blocks.content.append(rMenu);
		
		HWS.on('click','rm-add-objpin',HWmain.modal.objPin);
		//HWS.blocks.content.append(menu);
	}
	//добовляет движение ( драг дроп )
	function add_move(){
		let allMove = document.querySelectorAll('.new-block');
			allMove = Array.apply(null, allMove);
		for (let keym in allMove){
			let move = allMove[keym].querySelector('.ib-move');
			allMove[keym].classList.remove('new-block');
			HWS.on('touchmove',move,HWmain.move.m);
			HWS.on('pointerdown',move,HWmain.move.s);
			HWS.on('touchstart',move,HWmain.move.s);
		}
	}
	//функция ВКЛючение - ОТКЛючения рела
	function on_off_rele(elem){
		let block = HWS.parentSearchClass(elem,'info-block');
		let state = block.classList.contains('state-on');
		let id = block.dataset.id;
		if(!block.classList.contains('blocked')){
			if (state){
				block.classList.remove('state-on');
				block.classList.add('state-off');
				if (id){
					let dataP = {token:HWS.pageAccess,id_object:id,state:0}
					HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
				}
			}
			else {
				block.classList.remove('state-off');
				block.classList.add('state-on');
				if (id){
					let dataP = {token:HWS.pageAccess,id_object:id,state:1}
					HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
				}
			}
		} else (HWF.push('Устройство используется в программе и не может управляться вручную','red'))
	}
};
// Для работы со страницей оборудования
function equipment(){
	//для работы лемитов в разделе аналоговых датчиков
	this.limit = new function(){
		let G = this, GD = {};
		G.start = function(selectElem){
			GD.main = document.querySelector(selectElem);
			GD.min = parseInt(GD.main.dataset.min);
			GD.max = parseInt(GD.main.dataset.max);
			GD.specKof = parseFloat((100/(GD.max-GD.min)).toFixed(4));
			GD.fcn = GD.main.dataset.fcn || '';
			GD.line = {}; GD.tablo = {}
			GD.line.warMin = GD.main.querySelector('.line_war_min');
			GD.line.warMax = GD.main.querySelector('.line_war_max');
			GD.line.aleMin = GD.main.querySelector('.line_alt_min');
			GD.line.aleMax = GD.main.querySelector('.line_alt_max');
			GD.tablo.warMin = GD.main.querySelector('.warning_min_predel .main-value input');
			GD.tablo.warMax = GD.main.querySelector('.warning_max_predel .main-value input');
			GD.tablo.aleMin = GD.main.querySelector('.alert_min_predel .main-value input');
			GD.tablo.aleMax = GD.main.querySelector('.alert_max_predel .main-value input');
			
			// Открыть закрыть доп настройки
			HWS.on('click','open_dop_sets',G.onofSets);
			// Вкл\выкл Предела
			HWS.on('click','active_limit',G.onofLimit);
			// Нажатие на минус и плюс
			HWS.on('click','btn_int_minusplus',G.clickMinusPlus);
			// Нажатие на линию
			HWS.on('click','line',G.onofLine);
			HWS.on('click','dop-btn-open-line',G.onofLineBtn);
			// Изменение значения в инпуте
			HWS.on('change','inp_main_val',G.changeInput);
			// Нажатие на Кнопку линии
			HWS.on('pointerdown','btn_line_move',G.doBtnLine);
			HWS.on('touchmove','btn_line_move',G.moveBtnLine); // движение пальцем по элементу 
			HWS.on('touchend','btn_line_move',G.upBtnLine); // Отпускание пальца с элемента
			document.body.addEventListener('mousemove',G.moveBtnLine); // движение мышкой по документу 
			document.body.addEventListener('mouseup',G.upBtnLine); // Отпускание мышки на документе 
			
			
			// отключает Открытую (включенную) линию в порогах
			document.body.addEventListener('pointerdown',function(e){
				if(!e.target.classList.contains('line') && !e.target.classList.contains('btn-line')){
					let line = document.querySelector('.line.open');
					if(line){line.classList.remove('open');}
				}
			});
		}
		
		G.changeInput = function(e,el){
			let val = parseFloat(el.value) || 0;
			let one = HWS.parentSearchClass(el,'one-limit');
			let main = HWS.upElem(el,'.btn-int'); //el.parentElement;
			let min = main.dataset.min || 0;
			let max = main.dataset.max || 0;
			let tofix = parseInt(main.dataset.tofix) || 0;
			
			if(min) {
				if(min == 'null' && val < 0){val = 0}
				else if(val < min){val = parseFloat(min)}
			}
			if(max) {
				if(max == 'null' && val > 0){val = 0}
				else if(val > max){val = parseFloat(max)}
			}
			
			el.value = val.toFixed(tofix);
			console.log(val);

			let typeBtn = '';
			if(one.classList.contains('warning_min_predel')){typeBtn = 'warning_min_predel'}
			if(one.classList.contains('warning_max_predel')){typeBtn = 'warning_max_predel'}
			if(one.classList.contains('alert_min_predel')){typeBtn = 'alert_min_predel'}
			if(one.classList.contains('alert_max_predel')){typeBtn = 'alert_max_predel'}
			
			G.changeLine(one,val)
			HWS.buffer[GD.fcn]({val:val,type:typeBtn});
		}
		
		G.clickMinusPlus = function(e,elem){
			let el = elem;
			let one = HWS.parentSearchClass(el,'one-limit');
			let main = el.parentElement;
			let intBtn = HWS.upElem(el,'.btn-int');
			let step = parseFloat(intBtn.dataset.step) || 1;
			let tofix = parseInt(intBtn.dataset.tofix) || 0;
			let cont = elem.dataset.cont || '+';
			let input = main.querySelector('input');
			let inVal = input.value;
			let min = intBtn.dataset.min || 0;
			let max = intBtn.dataset.max || 0;
			let val = 0;
			
			if(cont == '-'){val = parseFloat(input.value) - step; }
			if(cont == '+'){val = parseFloat(input.value) + step; }
			
			if(min) {
				if(min == 'null' && val < 0){val = 0}
				else if(val < min){val = parseFloat(min)}
			}
			if(max) {
				if(max == 'null' && val > 0){val = 0}
				else if(val > max){val = parseFloat(max)}
			}
			
			input.value = val.toFixed(tofix);
			if(main.classList.contains('main-value')){G.changeLine(one,val)};
			
			let typeBtn
			if(one.classList.contains('warning_min_predel')){typeBtn = 'warning_min_predel'}
			if(one.classList.contains('warning_max_predel')){typeBtn = 'warning_max_predel'}
			if(one.classList.contains('alert_min_predel')){typeBtn = 'alert_min_predel'}
			if(one.classList.contains('alert_max_predel')){typeBtn = 'alert_max_predel'}
			HWS.buffer[GD.fcn]({val:input.value,type:typeBtn});
			
			
			
			/*
				let el = elem;
				let main = el.parentElement;
				let fcn = main.dataset.fcn || '';
				let min = main.dataset.min || 0;
				let max = main.dataset.max || 0;
				let val = parseFloat(elem.value);
				let tofix = parseInt(main.dataset.tofix) || 0;

				if(min) {
					if(min == 'null' && val < 0){val = 0}
					else if(val < min){val = parseFloat(min)}
					elem.value = val.toFixed(tofix);
				}
				if(max) {
					if(max == 'null' && val > 0){val = 0}
					else if(val > max){val = parseFloat(max)}
					elem.value = val.toFixed(tofix);
				}
				
				if(HWS.buffer[fcn]){HWS.buffer[fcn](elem.value);}
			
			*/
		}
		
		G.onofLimit = function(e,el){
			let main = HWS.parentSearchClass(el,'one-limit');
			if (main.classList.contains('active')){
				main.classList.remove('active');
				if(main.classList.contains('warning_min_predel')){GD.line.warMin.classList.remove('active')}
				if(main.classList.contains('warning_max_predel')){GD.line.warMax.classList.remove('active')}
				if(main.classList.contains('alert_min_predel')){GD.line.aleMin.classList.remove('active')}
				if(main.classList.contains('alert_max_predel')){GD.line.aleMax.classList.remove('active')}
			}
			else {
				main.classList.add('active');
				if(main.classList.contains('warning_min_predel')){GD.line.warMin.classList.add('active')}
				if(main.classList.contains('warning_max_predel')){GD.line.warMax.classList.add('active')}
				if(main.classList.contains('alert_min_predel')){GD.line.aleMin.classList.add('active')}
				if(main.classList.contains('alert_max_predel')){GD.line.aleMax.classList.add('active')}
			}
		}
		
		G.onofSets = function(e,el){
			let main = HWS.parentSearchClass(el,'one-limit');
			if (main.classList.contains('open')){main.classList.remove('open')}
			else {main.classList.add('open')}
		}
		
		G.onofLine = function(e,el){
			let main = el.parentElement;
			let active = main.querySelector('.line.open');
			let contains = el.classList.contains('open');
			
			if(active){active.classList.remove('open')}
			if (contains){el.classList.remove('open')}
			else {el.classList.add('open')}
		}		
		G.onofLineBtn = function(e,el){
			let line = HWS.upElem(el,'.line');
			let contains = line.classList.contains('open');

			if (contains){line.classList.remove('open')}
			else {line.classList.add('open')}
		}
		
		G.doBtnLine = function(e,el){
			G.moveGo = true;
			G.line = el.parentNode;
			G.tablo = G.line.querySelector('.btn-line.top');
			G.container = G.line.parentNode;
			G.sx = event.pageX || event.targetTouches[0].pageX; 
			G.slw = parseFloat(parseFloat(G.line.style.width).toFixed(4));
			G.kof = parseFloat((100/G.container.offsetWidth).toFixed(4));
			G.revers = false;
			
			if(G.line.classList.contains('line_war_min')){ G.inp = GD.tablo.warMin; }
			if(G.line.classList.contains('line_war_max')){ G.inp = GD.tablo.warMax; G.revers = true; }
			if(G.line.classList.contains('line_alt_min')){ G.inp = GD.tablo.aleMin; }
			if(G.line.classList.contains('line_alt_max')){ G.inp = GD.tablo.aleMax; G.revers = true; }
		}
		G.moveBtnLine = function(e,el){if(G.moveGo){
			let numb = 0;
			let x = event.pageX || event.targetTouches[0].pageX;
			
			if(G.revers){
				x = (G.sx-x)*G.kof+G.slw;
				if(x >= 100){x=100}; if(x <= 0){x=0};
				numb = (GD.max-(x/GD.specKof)).toFixed(1);
			}
			else{
				x = (x-G.sx)*G.kof+G.slw;
				if(x >= 100){x=100}; if(x <= 0){x=0};
				numb = ((x/GD.specKof)+GD.min).toFixed(1);
			}
			
			G.line.style.width = x+'%';
			G.tablo.innerHTML = numb;
			G.inp.value = numb;
		}}
		G.upBtnLine = function(e,el){if(G.moveGo){
			G.moveGo = false;
			
			let typeBtn;
			if(G.line.classList.contains('line_war_min')){typeBtn = 'warning_min_predel'}
			if(G.line.classList.contains('line_war_max')){typeBtn = 'warning_max_predel'}
			if(G.line.classList.contains('line_alt_min')){typeBtn = 'alert_min_predel'}
			if(G.line.classList.contains('line_alt_max')){typeBtn = 'alert_max_predel'}
			HWS.buffer[GD.fcn]({val:G.inp.value,type:typeBtn});
		}}
		
				
		G.changeLine = function(one,val){ // Изменение положения линии
			if(one.classList.contains('warning_min_predel')){
				GD.line.warMin.querySelector('.btn_line_move.btn-line.top').innerHTML = val;
				GD.line.warMin.style.width = G.linePosition(val,'min')+'%';}
			if(one.classList.contains('warning_max_predel')){
				GD.line.warMax.querySelector('.btn_line_move.btn-line.top').innerHTML = val;
				GD.line.warMax.style.width = G.linePosition(val,'max')+'%';}
			if(one.classList.contains('alert_min_predel')){
				GD.line.aleMin.querySelector('.btn_line_move.btn-line.top').innerHTML = val;
				GD.line.aleMin.style.width = G.linePosition(val,'min')+'%';}
			if(one.classList.contains('alert_max_predel')){
				GD.line.aleMax.querySelector('.btn_line_move.btn-line.top').innerHTML = val;
				GD.line.aleMax.style.width = G.linePosition(val,'max')+'%';}
		}
		
		G.linePosition = function (numb,type){
			let ret = ''
			if (type == 'max'){ret = parseFloat((GD.max-numb)*GD.specKof).toFixed(4);}
			if (type == 'min'){ret = parseFloat((numb-GD.min)*GD.specKof).toFixed(4);}
			return ret;
		}
	}
	//для работы скроллов в разделе опентерм
	this.scrol = new function(){
		let GS = this, GD = {}; 
		GD.mainClass = 'ecto-scrol';
		
		GS.start = function(){
			
			// Нажатие на минус и плюс
			HWS.on('click','btn_scrol_num_click',GS.clickMinusPlus);
			// Нажатие на линию
			HWS.on('click','btn_scrol_line_click',GS.clickLine);
			// Изменение значения в инпуте
			HWS.on('change','scrol_inp_num_val',GS.changeInput);
			
			// Нажатие на Кнопку линии
			HWS.on('pointerdown','btn-line',GS.doBtnLine);
			HWS.on('touchmove','btn-line',GS.moveBtnLine); // движение пальцем по элементу 
			HWS.on('touchend','btn-line',GS.upBtnLine); // Отпускание пальца с элемента
			document.body.addEventListener('mousemove',GS.moveBtnLine); // движение мышкой по документу 
			document.body.addEventListener('mouseup',GS.upBtnLine); // Отпускание мышки на документе 
			
			// отключает Открытую (включенную) линию в порогах
			document.body.addEventListener('pointerdown',function(e){
				if(!e.target.classList.contains('btn_scrol_line_click') && !e.target.classList.contains('btn-line')){
					let line = document.querySelector('.ecto-scrol.active');
					if(line){line.classList.remove('active');}
				}
			});
		}
		
		GS.changeInput = function(e,el){
			let val = parseFloat(el.value);
			GD.block = HWS.parentSearchClass(el,GD.mainClass);
			GD.main = el.parentElement;
			let tofix = parseInt(GD.main.dataset.tofix) || 0;
			let min = GD.main.dataset.min || 0;
			let max = GD.main.dataset.max || 0;
			
			if(GD.block.classList.contains('duo')){
				if(GD.main.classList.contains('val_1')){GD.line = GD.block.querySelector('.scale-temp .target.val_1');}
				if(GD.main.classList.contains('val_2')){GD.line = GD.block.querySelector('.scale-temp .target.val_2');}
				
			}else{
				GD.line = GD.block.querySelector('.scale-temp .target');
			}
			
			
			if(min) {
				if(min == 'null' && val < 0){val = 0}
				else if(val < min){val = parseFloat(min)}
			}
			if(max) {
				if(max == 'null' && val > 0){val = 0}
				else if(val > max){val = parseFloat(max)}
			}
			val = parseFloat(val.toFixed(tofix));
			el.value = val;
			GS.changeLine(GD.block,GD.line,val);
			get_val(el);
		}
		
		GS.clickMinusPlus = function(e,elem){
			let el = elem;
			let block = HWS.parentSearchClass(el,GD.mainClass);
			let main = el.parentElement;
			let iter = parseFloat(main.dataset.iter) || 1;
			let tofix = parseInt(main.dataset.tofix) || 0;
			let cont = elem.dataset.cont || '+';
			let input = main.querySelector('input');
			let val = 0,line='';
			let min = main.dataset.min || 0;
			let max = main.dataset.max || 0;
			
			if(block.classList.contains('duo')){
				if(main.classList.contains('val_1')){line = block.querySelector('.scale-temp .target.val_1');}
				if(main.classList.contains('val_2')){line = block.querySelector('.scale-temp .target.val_2');}
				
			}else{
				line = block.querySelector('.scale-temp .target');
			}
			
			if(cont == '-'){val = parseFloat(input.value) - iter; }
			if(cont == '+'){val = parseFloat(input.value) + iter; }
			
			if(min) {
				if(min == 'null' && val < 0){val = 0}
				else if(val < min){val = parseFloat(min)}
				//val = val.toFixed(tofix);
			}
			if(max) {
				if(max == 'null' && val > 0){val = 0}
				else if(val > max){val = parseFloat(max)}
				//val = val.toFixed(tofix);
			}
			
			val = parseFloat(val.toFixed(tofix));
				
			input.value = val;
			if(block){GS.changeLine(block,line,val)};
			get_val(el);
		}
		
		GS.changeLine = function(block,line,val){
			let elTop = line.querySelector('.btn-line.top');
			let elBot = line.querySelector('.btn-line.bot');
			let max = block.dataset.max;
			let min = block.dataset.min;
			let pos = (val-min)*(100/(max - min)); pos = pos.toFixed(4);
			line.style.left = pos+'%';
			elTop.innerHTML = val;
			elBot.innerHTML = val;
			
			if(line.classList.contains('val_1')){block.querySelector('.line-color-1').style.width = pos+'%';}
			if(line.classList.contains('val_2')){block.querySelector('.line-color-2').style.width = (100-pos)+'%';}
		}
		
		GS.clickLine = function(e,el){
			GD = {};GD.mainClass = 'ecto-scrol';
			let main = HWS.parentSearchClass(el,GD.mainClass);
			let activeLine = document.querySelector('.ecto-scrol.active');
			if (main.classList.contains('active')){main.classList.remove('active');}
			else {main.classList.add('active');}	
			if(activeLine){activeLine.classList.remove('active');}
		}
		
		GS.doBtnLine = function(e,el){
			GD.moveGo = true;
			GD.block = HWS.parentSearchClass(el,GD.mainClass);
			GD.container = HWS.parentSearchClass(el,'scale-temp');
			GD.target = HWS.parentSearchClass(el,'target');
			GD.inpt = GD.block.querySelector('.btn-int input');
			GD.elTop = GD.target.querySelector('.btn-line.top');
			GD.elBot = GD.target.querySelector('.btn-line.bot');
			GD.toFix = parseFloat(GD.block.dataset.tofix)|| 0;
			GD.max = parseFloat(GD.block.dataset.max);
			GD.min = parseFloat(GD.block.dataset.min);
			GD.stl = parseFloat(GD.target.style.left);
			GD.sx = e.pageX || e.targetTouches[0].pageX; 
			GD.kof = parseFloat((100/GD.container.offsetWidth).toFixed(4));
			GD.kofVal = parseFloat(((GD.max - GD.min)/100).toFixed(4));
			GD.dopX = false;
			
			if(GD.target.classList.contains('val_1')){
				GD.inpt = GD.block.querySelector('.btn-int.val_1 input'); 
				GD.color = GD.block.querySelector('.line-color-1');}
			if(GD.target.classList.contains('val_2')){
				GD.inpt = GD.block.querySelector('.btn-int.val_2 input'); 
				GD.color = GD.block.querySelector('.line-color-2'); GD.dopX = true;}
		}
		GS.moveBtnLine = function(e,el){if(GD.moveGo){
			let numb = 0;
			let x = e.pageX || e.targetTouches[0].pageX;

			x = ((x-GD.sx)*GD.kof)+GD.stl;
			if(x >= 100){x=100}; if(x <= 0){x=0};
			numb = ((x*GD.kofVal)+GD.min).toFixed(GD.toFix);

			GD.target.style.left = x+'%';
			GD.elTop.innerHTML = numb;
			GD.elBot.innerHTML = numb;
			GD.inpt.value = numb;
			if(GD.color && !GD.dopX){GD.color.style.width = x+'%';}
			if(GD.color && GD.dopX){GD.color.style.width = (100-x)+'%';}
		}}
		GS.upBtnLine = function(e){if(GD.moveGo){
			GD.moveGo = false;
			get_val(e.target);
		}}
		
		function get_val(el){
			let main = HWS.upElem(el,'.'+GD.mainClass);
			let val={};
				val.min = main.querySelector('.scale-name .btn-int.val_1 .scrol_inp_num_val'); val.min = (val.min)?val.min.value : false;
				val.max = main.querySelector('.scale-name .btn-int.val_2 .scrol_inp_num_val'); val.max = (val.max)?val.max.value : false;
			if(main.dataset.fcn)(HWS.buffer[main.dataset.fcn](val))
		}
	}
	// для работы SVG погодозависемой автоматики в опен-терм
	this.openThermInitMap = function(elem){
		let activeElem = elem.dataset.selel;
		let svg = elem.contentDocument;
		let ov = svg.querySelectorAll('.ovmask');
		let fcn = elem.dataset.fcn || '';

		if(activeElem){svg.querySelector('#wac-'+activeElem).classList.add('selection');}
		
		ov = Array.apply(null, ov);
		ov.forEach(function(el){
			el.addEventListener('click',function(){
				let numb = parseInt(el.id.substr(3));
				let active = svg.querySelector('.wac.selection');
				let wac = svg.querySelector('#wac-'+numb);
				
				if(wac.classList.contains('selection')){}
				else {
					wac.classList.add('selection');
					if(active){active.classList.remove('selection');}
				}
				
				
				if(fcn){HWS.buffer[fcn](numb)}
			});
			el.onmouseover = function(){
				let numb = parseInt(el.id.substr(3));
				svg.querySelector('#wac-'+numb).classList.add('hover');
			}
			el.onmouseout = function(){
				let numb = parseInt(el.id.substr(3));
				svg.querySelector('#wac-'+numb).classList.remove('hover');
			}
		});
	}
	
	// приводит серверные данные в нормальный вид Принимает элемент оборудования
	this.normInfo = function(elem){ //console.log(elem);
		let Ninfo = {};
		let et = false ;	
		if(elem.cloud_type){
			Ninfo.type = elem.cloud_type || '';
			et = HWD.equp[Ninfo.type];
		}
		// Глобальные
		Ninfo.id = elem.id;
		Ninfo.name = elem.config.name || '';
		Ninfo.elem = elem;
		if(et){
			Ninfo.et = et;
			Ninfo.title = et.title || '';
			Ninfo.tmp = et.tmp || '';
		}else  {et = {}; et.tmp = 'error';}
		Ninfo.elemType = elem.type || '';
		
		Ninfo.branch = (elem.info.type && elem.info.type.branch)?elem.info.type.branch : '';
		Ninfo.channel_index = elem.info.channel_index || 0;
		Ninfo.ident = elem.info.ident || '';
		Ninfo.addr = elem.info.addr || '';
		Ninfo.battery = '';
		Ninfo.port = '';
		Ninfo.stateColor = '';
		Ninfo.stateColorLk = false;
		Ninfo.stateName = false;
		Ninfo.stateN = '';
		if(elem.lk && elem.lk.state_name) {Ninfo.stateName = elem.lk.state_name;}
		if(elem.lk && elem.lk.state) {Ninfo.stateN = elem.lk.state;}
		Ninfo.stateColor = HWF.getStateColor(elem.lk.state);
		
		if(elem.info.type && elem.info.type.unit) {Ninfo.unitN = elem.info.type.unit || ''; Ninfo.unit = HWD.unit[Ninfo.unitN] ||''; }
		else {Ninfo.unit = '';}

		if(Ninfo.ident == '000000') {Ninfo.ident = '';}
		if(elem.state && elem.state.power){Ninfo.battery = elem.state.power.quality || '';}
		if(elem.info.iface){
			Ninfo.group = elem.info.iface.group || '';
			Ninfo.port = elem.info.iface.port || '';
			Ninfo.portName = HWD.iface.port[Ninfo.port];
			Ninfo.portSymbol = '';
			Ninfo.portSpec = '';

			// получить Порт оборудования
			let chin = parseInt(Ninfo.channel_index);
			if (Ninfo.port == 1){Ninfo.portSpec = Ninfo.portName}
			if (Ninfo.port == 2){Ninfo.portSpec = Ninfo.portName}
			if (Ninfo.port == 3){Ninfo.portSpec = Ninfo.portName+chin}
			if (Ninfo.port == 4){Ninfo.portSpec = Ninfo.portName+chin}
			if (Ninfo.port == 5){Ninfo.portSpec = Ninfo.portName}
			if (Ninfo.port == 6){chin = chin*3; Ninfo.portSpec = String.fromCharCode(62+chin,63+chin,64+chin);}
			if (Ninfo.port == 7){
				if (chin == 1){Ninfo.portSpec = HWD.globalText.port_7_1}
				else if (chin == 2){Ninfo.portSpec = HWD.globalText.port_7_2}
				else {Ninfo.portSpec = Ninfo.portName+chin}
			}
			if (Ninfo.port == 9){Ninfo.portSpec = Ninfo.portName+chin}
			if(Ninfo.port == 10){Ninfo.portSymbol = 'R'}
			if(Ninfo.port == 12){Ninfo.portSymbol = 'W'}
			if(Ninfo.port == 13){Ninfo.portSpec = Ninfo.portName+chin}
			
			if(Ninfo.port == 10 || Ninfo.port == 12){
				let chanel = '';
				if(chin){chanel = ':'+chin}
				let port = Ninfo.portName+' : '+(Ninfo.portSymbol+Ninfo.addr);
				Ninfo.portSpec = port;
			}
			
		}
		if(elem.config.flags){Ninfo.active = elem.config.flags.enabled; Ninfo.jurnal = elem.config.flags.journal}
		else{Ninfo.active = ''; Ninfo.jurnal = '';}
		
		// Аналоговые датчики системы 3х
		if(et.tmp == 'analog3x'){ 
			elem.state.value = elem.state.value || {};
			Ninfo.val = elem.state.value.val || '';
			Ninfo.state = '';
			if(elem.state.state.val || elem.state.state.val == 0){ Ninfo.stateN = Ninfo.stateN || elem.state.state.val;
				if(elem.state.state.val == 0 || elem.state.state.val > 10){Ninfo.state = 'no_connect';}
				else if (elem.state.state.val == 1) {Ninfo.state = 'alarm_down';}
				else if (elem.state.state.val == 2) {Ninfo.state = 'alarm_down';}
				else if (elem.state.state.val == 5) {Ninfo.state = 'norm';}
				else if (elem.state.state.val == 3) {Ninfo.state = 'alarm_up';}
				else if (elem.state.state.val == 4) {Ninfo.state = 'alarm_up';}
			}
		}
		// Аналоговые датчики системы 4х
		if(et.tmp == 'analog4x'){ 
			elem.state.value = elem.state.value || {};
			Ninfo.val = elem.state.value.val || '';
			Ninfo.state = ''; Ninfo.stateN = '';
			if(elem.state.state.val){ Ninfo.stateN = Ninfo.stateN || elem.state.state.val;
				if(elem.state.state.val == 0 || elem.state.state.val > 10){Ninfo.state = 'no_connect';}
				else if (elem.state.state.val == 1) {Ninfo.state = 'alarm_down';}
				else if (elem.state.state.val == 2) {Ninfo.state = 'alarm_down';}
				else if (elem.state.state.val == 5) {Ninfo.state = 'norm';}
				else if (elem.state.state.val == 3) {Ninfo.state = 'alarm_up';}
				else if (elem.state.state.val == 4) {Ninfo.state = 'alarm_up';}
			}
		}
		// Дискретные датчики системы 3х
		if(et.tmp == 'discret3x'){
			Ninfo.val = elem.state.value.val || 0;
			Ninfo.state = ''; Ninfo.stateN = Ninfo.stateN || '';
			Ninfo.onof = false;
			Ninfo.stateAllName = {}; ;
			if(elem.config.state_name){
				Ninfo.stateAllName.alert = elem.config.state_name[0];
				Ninfo.stateAllName.norm = elem.config.state_name[1];
			}
			else {
				Ninfo.stateAllName.alert = et.alerT;
				Ninfo.stateAllName.norm = et.normT; //Ninfo.stateName.
			}
			if (et.norm == Ninfo.val){Ninfo.onof = 'on'; Ninfo.stateName = Ninfo.stateName || Ninfo.stateAllName.norm;}
			else {Ninfo.onof = 'of'; Ninfo.stateName = Ninfo.stateName || Ninfo.stateAllName.alert;}
			if(elem.state.state.val){ Ninfo.stateN = Ninfo.stateN || elem.state.state.val;
				if (Ninfo.stateN == 0 || Ninfo.stateN > 10){Ninfo.state = 'no_connect';}
				if (Ninfo.stateN == 5) {Ninfo.state = 'norm';  Ninfo.stateName = Ninfo.stateName || Ninfo.stateAllName.norm;}
				if (Ninfo.stateN == 10){Ninfo.state = 'alert'; Ninfo.stateName = Ninfo.stateName || Ninfo.stateAllName.alert;}
				if (Ninfo.stateN == 8) {Ninfo.state = 'zamk';} //Замкнут
				if (Ninfo.stateN == 9) {Ninfo.state = 'razm';} //Разомкнут
			}
		}
				// Дискретные датчики системы 4х
		if(et.tmp == 'discret4x'){
			Ninfo.val = elem.state.value.val || 0;
			Ninfo.state = ''; Ninfo.stateN = Ninfo.stateN || '';
			Ninfo.onof = false;
			Ninfo.stateAllName = {}; ;
			if(elem.config.state_name){
				Ninfo.stateAllName.alert = elem.config.state_name[0];
				Ninfo.stateAllName.norm = elem.config.state_name[1];
			}
			else {
				Ninfo.stateAllName.alert = et.alerT;
				Ninfo.stateAllName.norm = et.normT; //Ninfo.stateName.
			}
			if (et.norm == Ninfo.val){Ninfo.onof = 'on'; Ninfo.stateName = Ninfo.stateName || Ninfo.stateAllName.norm;}
			else {Ninfo.onof = 'of'; Ninfo.stateName = Ninfo.stateName || Ninfo.stateAllName.alert;}
			if(elem.state.state.val){ Ninfo.stateN = Ninfo.stateN || elem.state.state.val;
				if (Ninfo.stateN == 0 || Ninfo.stateN > 10){Ninfo.state = 'no_connect'; }
				if (Ninfo.stateN == 5) {Ninfo.state = 'norm';  Ninfo.stateName = Ninfo.stateName || Ninfo.stateAllName.norm;}
				if (Ninfo.stateN == 10) {Ninfo.state = 'alert'; Ninfo.stateName = Ninfo.stateName || Ninfo.stateAllName.alert;}
				if (Ninfo.stateN == 8) {Ninfo.state = 'zamk';} //Замкнут
				if (Ninfo.stateN == 9) {Ninfo.state = 'razm';} //Разомкнут
			}
		}
		// Устройства Управления системы 3х
		if(et.tmp == 'control_device3x'){
			Ninfo.val = elem.state.state.val || 0;
			Ninfo.state = ''; Ninfo.stateN = Ninfo.stateN || Ninfo.val; Ninfo.used = false;
			Ninfo.stateName = Ninfo.stateName || et.state[Ninfo.stateN].name;
			if (Ninfo.val == 255){Ninfo.state = 'no_connect';}
			if (Ninfo.val == 30) {Ninfo.state = 'of';}
			if (Ninfo.val == 31) {Ninfo.state = 'on';}
			if (elem.lk && elem.lk.control_locked){Ninfo.used = elem.lk.control_locked;}
		}
		// Устройства Управления системы 4х
		if(et.tmp == 'control_device4x'){
			if (elem.state.state){ Ninfo.val = elem.state.value || 0;}
			else { Ninfo.val = 0 }
			Ninfo.state = ''; Ninfo.stateN = Ninfo.stateN || Ninfo.val; Ninfo.used = false; 
			Ninfo.stateName = Ninfo.stateName || (et.state[Ninfo.stateN])?et.state[Ninfo.stateN].name:'';
			if (Ninfo.val == 255){Ninfo.state = 'no_connect';}
			if (Ninfo.val == 0) {Ninfo.state = 'of';}
			if (Ninfo.val == 1) {Ninfo.state = 'on';}
			if (elem.lk && elem.lk.control_locked){Ninfo.used = elem.lk.control_locked;}
		}
		// Фото камера
		if(et.tmp == 'foto_camera3x'){
			Ninfo.val = elem.state.state.val || '0';
		}
		// кран
		if(et.tmp == 'control_crane3x'){
			Ninfo.val = elem.state.state.val || 0;
			Ninfo.state = ''; Ninfo.stateN = Ninfo.stateN || Ninfo.val; Ninfo.used = 0;
			Ninfo.stateName = Ninfo.stateName || et.state[Ninfo.stateN].name;
			if (Ninfo.val == 255){Ninfo.state = 'no_connect';}
			if (Ninfo.val == 254){Ninfo.state = 'error';}
			if (Ninfo.val == 30) {Ninfo.state = 'of'; }
			if (Ninfo.val == 31) {Ninfo.state = 'on';}
			if(elem.state.block_local_id){Ninfo.used = 1;}
		}
		
		if(et.tmp == 'adapter_boiler3x'){
			Ninfo.state = '';
			if(!elem.connection){Ninfo.state = 'no_connect';}
			else if(elem.state.pot_link_to_boiler != 0){Ninfo.state = 'on_boiler';} 
			else {Ninfo.state = 'no_boiler';}
		}
				
		if(et.tmp == 'system3x'){}
		if(et.tmp == 'system4x'){}
		if(et.tmp == 'radio_brelok3x'){}
		
		//Программы
		if(et.tmp == 'notofications'){ // Оповещение
			//Ninfo.progEt = HWD.programs[elem.info.type.type];
		}
		if(et.tmp == 'security'){ // Охрана
			//Ninfo.progEt = HWD.programs[elem.info.type.type];
		}
		if(et.tmp == 'heating'){ // Отопление
			//Ninfo.progEt = HWD.programs[elem.info.type.type];
			Ninfo.invers = elem.config.flags.inversion_of_control;
		}
		if(et.tmp == 'alarm'){ // Реакция на датчики
			//Ninfo.progEt = HWD.programs[elem.info.type.type];
		}
		if(et.tmp == 'sheduler'){ // Расписание
			//Ninfo.progEt = HWD.programs[elem.info.type.type];
		}
		
		if(et.tmp == 'error'){}

		return Ninfo;
	}
	
	// Распечатывает страницу доступов для оборудования и программ
	this.accessUsers = function(id,content){ //console.log(elem);
		let token = HWS.pageAccess;
		let elem = HWS.buffer.elem;
		let S = {}
			S.owner_status = elem.owner_status;		

		function print_user(){
			content.innerHTML = HWhtml.loader();
			HWF.modal.of();
			let data = {token:token,list_start:0,list_amount:1000,id_object:id}; 
			HWF.post({url:HWD.api.gets.objectsSecurityUserList,data:data,fcn:function(inf){
				let list = inf.list;
				let print = '';
				S.specClass = '';
				if(list.length > 0){
					print = '<ul id="list-all-users-security" class="list left">\
					<li class="one-head sticky">\
						<div class="l-name">Аккаунт</div>\
						<div class="l-name">Имя пользователя</div>\
						<div class="l-name">Права доступа</div>\
						<div class="equ_b equp_select_all btn_text">Всё</div>\
					</li>';
					HWF.recount(list,function(one){
						let ack = one.email;
						let name = one.name;
						let security = HWhtml.select({100:'просмотр',111:'полный доступ'},'btn_select_security block_w_140p',parseInt(one.security));
						
						print+='<li class="one-elem" data-id="'+one.id_user+'">';
						
						print+='<div class="l-name">'+ack+'</div>';
						print+='<div class="l-name">'+name+'</div>';
						print+='<div class="l-name">'+security+'</div>';
						
						print+= '<div class="equ_b">\
							<div class="settings-butt">\
								<div class="btn-active checkbox one_equipment_acsses_check"></div>\
							</div>\
						</div>';
						
						print+='</li>';
					});
					print+= '</ul>';
				} else {S.specClass = 'one'}
				//blockPrint.innerHTML = print;
				
				
				S.print = '<div class="one-block ob-top block-acsses-user-for-you '+S.specClass+'">';
				S.print+= '<div class="tf_fr900 block_ptb_15 block-user-upOne">Принадлежит Вам. Вы разрешили доступ следующим пользователям:</div>';
				S.print+= '<div class="tf_fr900 block_ptb_15 block-user-none">Данный элемент принадлежит только Вам.</div>';
				S.print+= '<div class="flex_between_top block_ptb_15 block-user-upOne">\
					<div class="block_w_210p flex_center_left">\
						<div class="display_inline select-none">\
							<span id="icon-filter" class="icon icon-filter hover_orange p20"></span>\
							<span class="spec-circle-icon "></span>\
						</div>\
						<span class="vertical-middle select-none ts_12p block_pl_5">Фильтр</span>\
					</div>\
					<div id="search-block" class="search block_mr_8">\
						<input class="search_block_input" type="text" value="" placeholder="Найти">\
						<span class="click_search_block search-block">⚲</span>\
						<span class="click_clear_search_block clear-search-block tc_red" title="Сбросить">✕</span>\
					</div>\
					<div id="block-menu-action-btn-acsses" class="open-clouse block_w_420p ta_right">\
						<span class="style_btn p3 grey block_ml_6 btn_111_user_security">Полный</span>\
						<!--span class="style_btn p3 grey block_ml_6 btn_110_user_security">Настройки</span-->\
						<span class="style_btn p3 grey block_ml_6 btn_100_user_security">Просмотр</span>\
						<span class="style_btn p3 grey block_ml_6 btn_delete_user_equp">Удалить</span>\
					</div>\
				</div>';
				S.print+= '<div id="list-accept-users" class="block-user-upOne">'+print+'</div>';
				S.print+= '<div><p class="btn_text grey block_ptb_15 add_new_user_control">Разрешить доступ другим</p></div>';
				S.print+= '</div>';
				content.innerHTML = S.print;
			}});
		}
		
		function mass_re_acssec(list,lvl){ lvl = lvl || 100;
			let dataReSec = []
			HWF.recount(list,function(oneElem){
				let li = HWS.upElem(oneElem,'.one-elem')
				let idUser = li.dataset.id;
				let selector = li.querySelector('.btn_select_security ');
				
				selector.value = lvl;
				dataReSec.push({
					id_user:idUser,
					id_object:id,
					security:lvl
				});
			},'on');
			
			let data = {token:token,data:dataReSec}; 
			HWF.post({url:HWD.api.gets.objectsSecurity,data:data,fcn:function(inf){
				//print_user();
			}});
		}
			
		if(S.owner_status){
			print_user();
		} else {
			S.ovnerMail = elem.owner_info.email || '';
			S.ovnerName = elem.owner_info.name || ''; if(S.ovnerName){S.ovnerName = '('+S.ovnerName+')'}
			S.youStatus = elem.lk.security_name || '';1
			S.print = '<div class="one-block ob-top conections">';
			S.print+= '<div class="block_ptb_15">Это собственность <span class="tf_fr900">'+S.ovnerMail+' '+S.ovnerName+'</div>';
			S.print+= '<div class="block_ptb_15">Ваш уровень доступа: <span class="tf_fr900">'+S.youStatus+'</div>';
			S.print+= '<div class="flex_between block_ptb_8">\
				<span></span>\
				<span class="style_btn grey btn_del_equp_no_my_use">Удалить из аккаунта</span>\
			</div>';
			S.print+= '</div>';
			content.innerHTML = S.print;
		}
		
		// выделить всех 			
		HWS.on('click','equp_select_all',function(e,el){
			HWF.allActive('#list-all-users-security','.checkbox');
		});
		// изменение прав доступа ( внутри 1го пользователя )
		HWS.on('change','btn_select_security',function(e,el){
			let li = HWS.upElem(el,'.one-elem');
			let idUser = li.dataset.id;
			let lvl = parseInt(el.value);
			let data = {token:token,data:[{
				id_user:idUser,
				id_object:id,
				security:lvl
			}]}; 
			HWF.post({url:HWD.api.gets.objectsSecurity,data:data,fcn:function(inf){
				console.log(inf);
			}});
		});
		// нажатие на кнопку "добавить пользователя"
		HWS.on('click','add_new_user_control',function(e,el){
			HWF.modal.loader(); 
			
			let data = {token:token,list_start:0,list_amount:1000}
			HWF.post({url:HWD.api.gets.userFriendGet,data:data,fcn:function(info){
				let lCount = info.item_count || 0;
				let list = info.list;
				let listI = '';
				console.log(info);
				if(list && list.length > 0){
					listI+='<div class="">'
					listI+='<span class="block_pb_20">Выберите пользователя для предоставления ему доступа</span>'
					listI+='</div>'
					
					listI+= '<ul class="list">';
					listI+= '<li class="one-head sticky">';
					listI+= '<div class="l-mail">Электронная почта</div>';
					listI+= '<div class="l-name ta_left">Имя</div>';
					listI+= '<div class="l-btn"></div>';
					listI+= '</li>';
					HWF.recount(list,function(el){
						listI+= '<li data-id="'+el.id+'">';
						listI+= '<div class="l-mail">'+el.email+'</div>';
						listI+= '<div class="l-name ta_left">'+el.name+'</div>';
						listI+= '<div class="l-btn"></div>';
						listI+= '<div class="click btn_click_one_control_user"></div>';
						listI+= '</li>';
					});
					listI+='</ul>';
					
					listI+='<div class="block_p_20">';
					listI+='<span class="style_btn grey new_user_add">Добавить доверенного пользователя</span>';
					listI+='</div>';
				} else {
					listI+= '<div>Делиться чем-либо можно только с доверенными пользователями ЛК.</div>';	
					listI+= '<div>Добавьте хотябы одного доверенного пользователя</div>';
					listI+= '<br><br><br>';
					listI+= '<div class="block_p_20">';
					listI+= '<span class="style_btn grey new_user_add">Добавить доверенного пользователя</span>';
					listI+= '</div>';
				}
				
				HWF.modal.print('Доверенные',listI);
			}});
		});
		// Нажатие на пользователя в добавлении
		HWS.on('click','btn_click_one_control_user',function(e,el){
			let li = HWS.upElem(el,'li');
			let idUser = li.dataset.id;
			let data = {token:token,data:[{
				id_user:idUser,
				id_object:id,
				security:100
			}]}; 
			HWF.modal.loader('Добавляем');
			HWF.post({url:HWD.api.gets.objectsSecurity,data:data,fcn:function(inf){
				print_user();
			}});
		});
		// Нажатие на удоление пользователя 
		HWS.on('click','btn_delete_user_equp',function(e,el){
			let list = document.querySelectorAll('#list-accept-users .list .one-elem .equ_b .checkbox.active');
			if(list && list.length > 0){
				let dataDel = []
				HWF.recount(list,function(oneElem){
					let li = HWS.upElem(oneElem,'.one-elem')
					let idUser = li.dataset.id;
					dataDel.push({id_user:idUser,id_object:id});
					li.remove();
				},'on');
				
				let data = {token:token,data:dataDel}; 
				HWF.post({url:HWD.api.gets.objectsSecurityDel,data:data,fcn:function(inf){
					console.log(inf);
				}});
			}
			//console.log(list);
		});
		// Нажатие на Выслать приглашение
		HWS.on('click','new_user_add',function(){
			let inpSum = '';
			
			inpSum+='<div class="">'+
			'<span class="block_pb_20">Укажите e-mail пользователя Личного кабинета</span>'+
			'</div>';
			
			inpSum+= '<div class="mini-modal-wiev">'+
				'<div class="str nop">'+
					'<div>Email</div>'+
					'<div><input id="user_mail_send" type="text"></div>'+
				'</div>'+
				'<span id="info-mesage-er-add-friend" class="block_w_100 ts_08 tc_red"></span>'+
				'<span id="info-mesage-ok-add-friend" class="block_w_100 ts_08 tc_green"></span>'+
				'<div class="str top">'+
					'<div class="block_pt_8">Сообщение</div>'+
					'<div><textarea id="user_mesaje_send" rows="5">Приняв этот запрос, вы сможете делиться доступами к элементам ЛК!</textarea></div>'+
				'</div>'+
			'</div>';
			
			HWF.modal.confirmCust({
				title:'Пригласить пользователя',
				print:inpSum,
				yes:'Пригласить',
				classYes:'btn_add_user_modal',
				clouseYes:'no'
			});
		});

		// Кнопка Удалить программу/оборудование
		HWS.on('click','btn_del_equp_no_my_use',function(e,el){
			let title = 'Удалить устройство?';
			let infotext = 'Данное устройство будет удалено из личного кабинета. Вы уверены?';
			HWF.modal.confirm(title,infotext,'btn_delete_equp_yes_equ','Удалить');
		});
		HWS.on('click','btn_delete_equp_yes_equ',function(e,el){
			let idprog = id || H.hash[1] || false;;
			HWF.modal.loader('Удаление');
			HWF.post({url:HWD.api.gets.objectsDelete,data:{token:token,id_objects:[idprog]},fcn:function(info){
				HWF.modal.of();
				HWS.go('equipments');
				HWF.push('Оборудование удалено');
			}});
		});
			
		//отправка запроса на добавление в друзья
		HWS.on('click','btn_add_user_modal',function(){
			let mail = document.body.querySelector('#user_mail_send');
			let mesage = document.body.querySelector('#user_mesaje_send');
			let error = document.body.querySelector('#info-mesage-er-add-friend');
			let oksend = document.body.querySelector('#info-mesage-ok-add-friend');
			let pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			let controlSend = true;
			
			error.innerHTML = '';
			oksend.innerHTML = '';
			
			if (!pattern.test(mail.value)){error.innerHTML = 'Неверно введён Емаил'; controlSend = false;}
			
			if (controlSend){
				oksend.innerHTML = HWhtml.loader();
				let data = {token:token,email:mail.value,message:mesage.value}
				console.log(data);
				HWF.post({url:'user/set_request_for_add_friendly_user',data:data,fcn:function(inf){
					HWF.push('Приглашение новому пользователю отправлено','green');
					HWF.modal.of();
					error.innerHTML = '';
					console.log(inf);
				},fcnE:function(inf){
					oksend.innerHTML = '';
					error.innerHTML = HWF.getServMesErr(inf);
				}});
			}
			
		});
		
		// Нажатие на "Просмотр"
		HWS.on('click','btn_100_user_security',function(e,el){
			let list = document.querySelectorAll('#list-accept-users .list .one-elem .equ_b .checkbox.active');
			if(list && list.length > 0){mass_re_acssec(list,100)}
		});
		// Нажатие на "Настройки"
		HWS.on('click','btn_110_user_security',function(e,el){
			let list = document.querySelectorAll('#list-accept-users .list .one-elem .equ_b .checkbox.active');
			if(list && list.length > 0){mass_re_acssec(list,110)}
		});
		// Нажатие на "Полный"
		HWS.on('click','btn_111_user_security',function(e,el){
			let list = document.querySelectorAll('#list-accept-users .list .one-elem .equ_b .checkbox.active');
			if(list && list.length > 0){mass_re_acssec(list,111)}
		});
	}
}
// функции для раздела программ
function programse(){
	let GP = this;
	
	GP.list = function(qur,at,type,url){ type = type || 'norm'; at = at || 'on'; url = url || HWD.api.gets.listMore;
		let token = HWS.pageAccess;
		let blockPrint = '';
		let sort = localStorage.getItem('sortprograms');
		
		if (typeof qur == 'string'){blockPrint = document.querySelector(qur)}
		if (qur instanceof HTMLElement){blockPrint = qur}
		
		blockPrint.innerHTML = '<div class="program_list block_w_100">'+HWhtml.loader()+'</div> <div class="program_pagin block_w_100"></div>'
		
		HWS.buffer.programPrintList = function(num,statr){ num = num || 0; statr = statr || false;
			let lAmount = localStorage.getItem('AllPageAmount') || 10;	lAmount = parseInt(lAmount);
			let lStart = num.start || 0;
			let lCount = 0;
			let dataP = {token:token,local_id_object_group:[10],list_start:lStart,list_amount:lAmount}
			if(sort) {dataP.sort = [sort];}
			let dataDop = HWS.buffer.programListDopGetServer;
			let dataSorFilt = HWS.buffer.programListDopSortFiltGetServer;
			let blockPrintList = blockPrint.querySelector('.program_list');
				blockPrintList.innerHTML = HWhtml.loader();

			if (dataDop){for (let ke in dataDop){dataP[ke] = dataDop[ke]}}
			if (dataSorFilt){for (let ke in dataSorFilt){dataP[ke] = dataSorFilt[ke]}}
			HWF.post({url:url,data:dataP,fcn:list_load_1});
			
			function list_load_1(inf){
				lCount = inf.item_count;
				let list = inf.list
				let arrayGet = [];
				let printAr = [];
				
				HWF.recount(list,function(elem){  
					let OneProg = {}
					let type = elem.cloud_type || elem.info.type.type;
					let et = HWD.equp[type]; if(!et){et = {}; et.tmp='';}
					
					OneProg.id = elem.id;
					OneProg.system = {id:elem.id_system};
					OneProg.type = type;
					OneProg.tmp = et.tmp || '';
					OneProg.name = elem.config.name;
					OneProg.namesis = elem.system_name || elem.system_ident;
					OneProg.idsis = elem.system_ident;
					OneProg.stateName = elem.lk.state_name;
					OneProg.state = elem.lk.state;
					OneProg.lk = elem.lk || '';
					
					/*
					if(OneProg.tmp == 'notofications'){OneProg.state = (elem.config.cfg_gnotif)?1:0;}
					if(OneProg.tmp == 'security'){OneProg.state = (elem.config.flags.security_active)?1:0;}
					if(OneProg.tmp == 'alarm'){OneProg.state = (elem.config.flags.active)?1:0;}
					if(OneProg.tmp == 'sheduler'){OneProg.state = (elem.config.flags.active)?1:0;}
					if(OneProg.tmp == 'heating'){OneProg.state = (elem.config.flags.active)?3:4;}
					*/
					
					OneProg.config = elem.config;
					
					printAr.push(OneProg);
				})

				let listPrint = blockPrint.querySelector('.program_list');
				let pagin = blockPrint.querySelector('.program_pagin');
				listPrint.innerHTML = HWhtml.programs(printAr,{a:at,typeW:type});
				if(statr){pagin.innerHTML = HWhtml.pagination({number:num,amount:lAmount,all:lCount,type:'buffer',load:'programPrintList'});}
			}
		}
		
		HWS.buffer.programPrintList(0,true);
	}
	
	GP.updateSes = function(id,localId,systId,fcn){
		fcn = fcn || function(){}
		if(localId){
			let uuControl = HWS.getElem(id+' .elem-equp inf');
			let dataP = {token:HWS.pageAccess,local_id:localId,id_system:systId}
			HWF.post({url:HWD.api.gets.objectLocalId,data:dataP,fcn:function(infTmp){
				let elemInfo = HWequip.normInfo(infTmp);
				let value = elemInfo.val;

				if(elemInfo.tmp == 'control_device3x'){value = elemInfo.et.state[value].name;}
				if(elemInfo.tmp == 'discret3x'){
					if (elemInfo.state == 'norm') {value = elemInfo.stateName.norm;}
					if (elemInfo.state == 'alert') {value = elemInfo.stateName.alert;}
					if (elemInfo.onof && elemInfo.onof == 'on') {value = elemInfo.stateName.norm;}
					if (elemInfo.onof && elemInfo.onof == 'of') {value = elemInfo.stateName.alert;}
				}
				if(elemInfo.state == 'no_connect'){value = 'Нет связи';}
				
				uuControl.innerHTML = value;
				fcn(infTmp);
			}});
		}
	}
	// загрузит все элементы оборудования если они есть
	GP.loadEqups = function(obj,fcn){ obj = obj || {}; fcn = fcn || function(){};
		let iter = 0,iterFcn = 0, newObj = {};
		let token = HWS.pageAccess;
		let idSys = obj.system || false; 
		if(obj){HWF.recount(obj,function(el){ iter++; });}
		
		function all_load(){ iterFcn++; if (iter == iterFcn){
			fcn(newObj);
		}};
		
		if(obj && idSys){HWF.recount(obj,function(el,key){
			if(el){
				if (key == 'system'){
					HWF.post({url:HWD.api.gets.list,data:{list_start:0,list_amount:100,token:token,id_system:el,local_id:0},fcn:function(inf){
						newObj[key] = inf.list[0]; all_load();
					}});
				} else {
					if(typeof el == "object"){
						if(el.length > 0){
						let dataP = {token:token,list_start:0,list_amount:1000,local_id:el,id_system:idSys}
						HWF.post({url:HWD.api.gets.list,data:dataP,fcn:function(inf){newObj[key] = inf.list; all_load();}});
						} else {newObj[key] = ''; all_load()}
					}
					else {
						let dataP = {token:token,local_id:el,id_system:idSys}
						HWF.post({url:HWD.api.gets.objectLocalId,data:dataP,fcn:function(infTmp){newObj[key] = infTmp; all_load();}});
					}
				}
			}
			else {newObj[key] = ''; all_load()}
		});}
	}
}


// Фильтр
function filter(){
		// filter
	this.projects = function(){
		let html = HWhtml.projectsFilter();
		let data = {};
		let getServer = {};
		
		data.type = server_get('type');
		HWF.recount(data,function(elem,key){if(elem){getServer[key] = elem;}});	
		HWS.data.apiGet.projects.data = getServer;
		
		// Кнопка применить фильтр
		HWS.on('click','btn_accept_new_filter',function(e,el){
			let f = {}, getstr='',go='';
			let iconSC = HWS.getElem('#icon-filter'); iconSC = iconSC.parentNode.querySelector('.spec-circle-icon');

			// HTML logic
			f.typeProj = create_get('#filter-typeProj-ul');
			
			getstr = '';
			HWF.recount(f,function(elem){if(elem){getstr+=elem;}});
			if(getstr){getstr = '?'+getstr; iconSC.classList.add('active');}
			else {iconSC.classList.remove('active');}
			go = 'projects'+getstr;	
			HWS.go(go,'no_reboot');
			
			// SERVER logic
			search_text();
		});
		
		HWS.on('click','click_search_block',search_text);
		HWS.on('change','search_block_input',search_text);
		
		function search_text(){
			let data = {};
			let controlQuery = true;
			let textS = HWS.getElem('#search-block .search_block_input').value;
			
			data.type = server_get('type');
			
			HWF.recount(data,function(val){if (val == "no_select"){controlQuery = false}});

			if(controlQuery){
				let getServer = {};
				HWF.recount(data,function(elem,key){if(elem){getServer[key] = elem;}});			
				if(textS){getServer.text = textS}  else {getServer.text = '';}
				HWS.data.apiGet.projects.data = getServer;
				HWF.print.projects('.all-projects');
			}else {
				let list = HWS.getElem('.all-projects');
				list.innerHTML = '<p class="tw_600 ta_center block_p_20">Оборудование не найдено. Попробуйте изменить настройки фильтра.</p>';
			}
		}
		
		clear_filter('projects',search_text);
		click_one_filter();
		select_all_filter();
		
		return html;
	}
	
	this.equipments = function(){
		let html = HWhtml.equipFilter();
			
		// server logic  !До загрузки страници
		let getData = HWS.get;
		let act = {},serverData = {};

		serverData.local_id_object_type = "2";
		serverData.iface_group = server_get('iface_group');
		serverData.branch = server_get('branch');

		if(getData.equp_system != 2){
			if(serverData.branch != 'no_select'){serverData.local_id_object_type = [2,0];}
			else {serverData.local_id_object_type = [0];}
			serverData.local_id_object_group = [0,1,2];
		}	

		if(getData.id_system){
			let token = HWS.pageAccess;
			HWF.post({url:HWD.api.gets.objectLocalId,data:{token:token,id_system:getData.id_system,local_id:0},fcn:function(sysinfo){
				HWS.getElem('#filter-name-system-select').innerHTML = sysinfo.config.name;
			}});
		}
		serverData.connection = server_get('connection');
		HWS.data.apiGet.equipments.data = serverData;
		
		// Кнопка применить фильтр
		HWS.on('click','btn_accept_new_filter',function(e,el){
			let wind = HWS.getElem('#filter-window');
			let system = HWS.getElem('#filter-name-system-select').dataset.id || 'no_select';
			let systemSelect = HWS.getElem('.btn_filter_selector_all_or_one_sys').value;
			let f = {}, getstr='',go='',systemCheck='';
			let iconSC = HWS.getElem('#icon-filter'); iconSC = iconSC.parentNode.querySelector('.spec-circle-icon');
			
			// HTML logic
			systemCheck = wind.querySelector('#system-check-filter');
			f.type = '';
			f.branch = create_get('#filter-equp-ul','equp_system');
			f.ifaceGroup = create_get('#filter-type-connect-ul');
			if(systemSelect == 'select') {f.id_object = 'id_system='+system+'&'}
			if(!systemCheck.classList.contains('active')) {f.equp_system = 'equp_system=2&'}
			f.connection = create_get('#filter-connection-ul');
			
			getstr = '';
			HWF.recount(f,function(elem){if(elem){getstr+=elem;}});
			if(getstr){
				HWS.getElem('.icon-filter').classList.add('green');
				getstr = '?'+getstr;
				iconSC.classList.add('active');
			} else {
				HWS.getElem('.icon-filter').classList.remove('green');
				iconSC.classList.remove('active');
			}
			go = 'equipments'+getstr;	
			HWS.go(go,'no_reboot');
			HWS.getElem('#block-menu-action-btn').classList.remove('open');

			// SERVER logic
			search_text();
		});

		// Изменение селоктора ( Выбор системы )
		HWS.on('change','btn_filter_selector_all_or_one_sys',function(e,el){
			let wind = HWS.upElem(el,'div').querySelector('.selector_blocl_system');
			let val = el.value;
			
			if(val == 'all'){wind.classList.remove('block');}
			if(val == 'select'){wind.classList.add('block');}
		});
		// Выбор системы
		HWS.on('click','btn_filter_select_system',function(e,el){
			let topPrint = '<div class="block_w_100 ta_center">'+HWhtml.search()+'</div>';
			HWF.modal.print('Выберите систему','');
			HWF.print.system('.modal-section',{top:topPrint,a:'no',type:'nobtn system-list',click:'btn_selected_one_system'});
		})
		HWS.on('click','btn_selected_one_system',function(e,el){
			let li = HWS.upElem(el,'li');
			let id = li.dataset.idsystem;
			let name = li.querySelector('.equ_n .text_name').innerHTML;
			let blockSysName = HWS.getElem('#filter-name-system-select');
			let get = HWS.getHash().get; 

			blockSysName.dataset.id = id;
			blockSysName.innerHTML = name;
			HWF.modal.of();
		});
		
		HWS.on('click','click_search_block',search_text);
		HWS.on('change','search_block_input',search_text);
		
		function search_text(){
			console.log(111);
			let data = {};
			let wind = HWS.getElem('#filter-window');
			let controlQuery = true;
			let textS = HWS.getElem('#search-block .search_block_input').value;
			let system = server_get('equp_system');

			data.local_id_object_type = "2";
			data.iface_group = server_get('iface_group');
			data.branch = server_get('branch');
			data.id_system = server_get('id_system');
			
			if(system != 2){
				if(data.branch != 'no_select'){data.local_id_object_type = [2,0];}
				else {data.local_id_object_type = [0];}
				data.local_id_object_group = [0,1,2];
			}else {HWF.recount(data,function(val){if (val == "no_select"){controlQuery = false}});}	

			data.connection = server_get('connection');
			
			if(controlQuery){
				let getServer = {};
				HWF.recount(data,function(elem,key){getServer[key] = elem;});
				if(textS){getServer.search_text = textS} else {getServer.search_text = '';}
				HWS.data.apiGet.equipments.data = Object.assign(HWS.data.apiGet.equipments.data, getServer);//getServer;
				HWF.print.equipments('.all-equipments',{a:'on'});
				console.log(HWS.data.apiGet.equipments.data);
			} else {
				let list = HWS.getElem('.all-equipments');
				list.innerHTML = '<p class="tw_600 ta_center block_p_20">Оборудование не найдено. Попробуйте изменить настройки фильтра.</p>';
			}
		}

		clear_filter('equipments',search_text);
		click_one_filter();
		select_all_filter();
		
		return html;
	}
	
	this.programs = function(){
		let html = HWhtml.programsFilter();
		let getData = HWS.getHash().get;
		let startList = {}
		
		startList.id_system = server_get('id_system');
		startList.object_type = server_get('object_type');
		
		let getServerS = {};
		HWF.recount(startList,function(elem,key){if(elem){getServerS[key] = elem;}});	
		HWS.buffer.programListDopGetServer = getServerS;
		
		if(getData.id_system){
			let token = HWS.pageAccess;
			HWF.post({url:HWD.api.gets.objectLocalId,data:{token:token,id_system:getData.id_system,local_id:0},fcn:function(sysinfo){
				HWS.getElem('#filter-name-system-select').innerHTML = sysinfo.config.name;
			}});
		}
		
		// Кнопка применить фильтр
		HWS.on('click','btn_accept_new_filter',function(e,el){
			// HTML logic 
			let wind = HWS.getElem('#filter-window');
			let systemSelect = HWS.getElem('.btn_filter_selector_all_or_one_sys').value;
			let system = HWS.getElem('#filter-name-system-select').dataset.id || 'no_select';
			let f = {}, getstr='',go='';
			let iconSC = HWS.getElem('#icon-filter'); iconSC = iconSC.parentNode.querySelector('.spec-circle-icon');
			
			f.type = create_get('#filter-type-prog-ul');
			if(systemSelect == 'select') {f.id_object = 'id_system='+system+'&'}
			
			getstr = '';
			HWF.recount(f,function(elem){if(elem){getstr+=elem;}});
			if(getstr){getstr = '?'+getstr; iconSC.classList.add('active');}
			else {iconSC.classList.remove('active');}
			go = 'programs'+getstr;	
			HWS.go(go,'no_reboot');
			
			// SERVER logic
			search_text();
		});
		
		// Изменение селоктора ( Выбор системы )
		HWS.on('change','btn_filter_selector_all_or_one_sys',function(e,el){
			let wind = HWS.upElem(el,'div').querySelector('.selector_blocl_system');
			let val = el.value;
			
			if(val == 'all'){wind.classList.remove('block');}
			if(val == 'select'){wind.classList.add('block');}
		});
		// Выбор системы
		HWS.on('click','btn_filter_select_system',function(e,el){
			let topPrint = '<div class="block_w_100 ta_center">'+HWhtml.search()+'</div>';
			HWF.modal.print('Выберите систему','');
			HWF.print.system('.modal-section',{top:topPrint,a:'no',type:'nobtn system-list',click:'btn_selected_one_system'});
		})
		HWS.on('click','btn_selected_one_system',function(e,el){
			let li = HWS.upElem(el,'li');
			let id = li.dataset.idsystem;
			let name = li.querySelector('.equ_n .text_name').innerHTML;
			let blockSysName = HWS.getElem('#filter-name-system-select');
			let get = HWS.getHash().get; 

			blockSysName.dataset.id = id;
			blockSysName.innerHTML = name;
			HWF.modal.of();
		});
		
		HWS.on('click','click_search_block',search_text);
		HWS.on('change','search_block_input',search_text);
		
		function search_text(){
			let textS = HWS.getElem('#search-block .search_block_input').value;
			let data = {};
			let controlQuery = true;
			
			data.id_system = server_get('id_system');
			data.object_type = server_get('object_type');
			
			HWF.recount(data,function(val){if (val == "no_select"){controlQuery = false}});

			if(controlQuery){
				let getServer = {};
				HWF.recount(data,function(elem,key){if(elem){getServer[key] = elem;}});			
				if(textS){getServer.search_text = textS} else {getServer.search_text = '';}
				HWS.buffer.programListDopGetServer = getServer;
				HWprog.list('#all-programs','','checkbox');
			}else {
				let list = HWS.getElem('#all-programs');
				list.innerHTML = '<p class="tw_600 ta_center block_p_20">Оборудование не найдено. Попробуйте изменить настройки фильтра.</p>';
			}
		}
		
		clear_filter('programs',search_text);
		click_one_filter();
		select_all_filter();
		
		return html;
	}
	
	this.templates = function(){
		let html = HWhtml.templatesFilter();
		let data = {};
		let getServer = {};
		
		data.sensors = server_get('sensors');
		data.programs = server_get('programs');
		data.random = server_get('random');
		
		// Кнопка применить фильтр
		HWS.on('click','btn_accept_new_filter',function(e,el){
			let f = {}, getstr='',go='';
			let iconSC = HWS.getElem('#icon-filter'); iconSC = iconSC.parentNode.querySelector('.spec-circle-icon');

			// HTML logic
			f.sensors = create_get('#filter-sensors-ul');
			f.programs = create_get('#filter-programs-ul');
			f.random = create_get('#filter-random-ul');
			
			getstr = '';
			HWF.recount(f,function(elem){if(elem){getstr+=elem;}});
			if(getstr){getstr = '?'+getstr; iconSC.classList.add('active');}
			else {iconSC.classList.remove('active');}
			go = 'templates'+getstr;	
			HWS.go(go,'no_reboot');
			
			
			// SERVER logic
			search_text();
		});
		
		HWS.on('click','click_search_block',search_text);
		HWS.on('change','search_block_input',search_text);
		
		function search_text(){
			/*let data = {};
			let controlQuery = true;
			let textS = HWS.getElem('#search-block .search_block_input').value;
			
			data.type = server_get('type');
			
			HWF.recount(data,function(val){if (val == "no_select"){controlQuery = false}});

			if(controlQuery){
				let getServer = {};
				HWF.recount(data,function(elem,key){if(elem){getServer[key] = elem;}});			
				if(textS){getServer.text = textS}
				HWS.data.apiGet.projects.data = getServer;
				HWF.print.projects('.all-projects');
			}else {
				let list = HWS.getElem('.all-projects');
				list.innerHTML = '<p class="tw_600 ta_center block_p_20">Оборудование не найдено. Попробуйте изменить настройки фильтра.</p>';
			}
			*/
		}
		
		clear_filter('templates',search_text);
		click_one_filter();
		select_all_filter();
		
		return html;
	}
	
	this.btnSortAZ = function(nameList,target,fcn){
		let fcnGo = HWS.buffer[fcn] || function(){}
		if(!HWS.buffer.dopFilter){HWS.buffer.dopFilter = {}}
		let s = HWhtml.btn.sortAZ(nameList,target,fcn);
		HWS.on('click','icon_btn_sort',function(e,el){		
			if(el.classList.contains('icon-sort-az')){
				el.classList.remove('icon-sort-az');
				el.classList.add('icon-sort-za');
				HWS.buffer.dopFilter.name = 0;
			} else {
				el.classList.remove('icon-sort-za');
				el.classList.add('icon-sort-az');
				HWS.buffer.dopFilter.name = 1;
			}
			fcnGo();
		});
		return s;
	}
	
	// для ФИЛЬРОВ Странная функция творящая дичь и непонятные преоброзования
	function create_get(quer,ignore){ ignore = ignore || '';
		let get = []
		let equpBlock = document.querySelector(quer);
		let equpAllLi = equpBlock.querySelectorAll('.filter-li');
		let name = equpBlock.dataset.type;
		
		HWF.recount(equpAllLi,function(el){if(el.classList.contains('active')){
			if(el.dataset.val != ignore){get.push(el.dataset.val);}
		}},'on');
		
		if(get.length == equpAllLi.length){get = null;}
		else if(get.length > 0){get = name+'='+get.join(',')+'&';}
		else {get = name+'=no_select&'}

		return get
	}
	function server_get(quer){
		let gteUrl = HWS.getHash().get;
		let get = gteUrl[quer];
		
		if(get) { get = get.split(','); if(get.length == 1) {get = get[0]} } 
		else {get = null;}
		
		// Доп логики
		if(quer == 'branch' && get != null){
			if (typeof get == 'object') {HWF.recount(get,function(val){if(val == 3){get.push("4")}});}
			else if(get == '3'){get = ["3","4"]}
		}
		
		return get;
	}
	
	function clear_filter(page,dopFcn){ // Очистить фильтр
		HWS.on('click','btn_clear_equp_filter',function(e,el){
			let inp = HWS.getElem('.search_block_input');//search_block_input
			inp.value = '';
			dopFcn();
			HWS.go(page);
		});
	}
	
	function click_one_filter(){ // Нажатие на 1 фильтр	
		HWS.on('click','btn_one_filter',function(e,el){
			let li = HWS.upElem(el,'li');
			
			if(li.classList.contains('active')){li.classList.remove('active');} 
			else {li.classList.add('active');}
		});
	}
	
	function select_all_filter(){ // Выбрать всё		
		HWS.on('click','btn_filter_select_all',function(e,el){
			let ul = HWS.upElem(el,'ul');
			HWF.allActive(ul,'.filter-li');
		});
	}
}
//Template function
function template(){
	this.control = function(name,val,view,data){ if(HWS.buffer.elem[val]){
	data = data || {};
		let elem = HWS.buffer.elem;
		let print = '';
		let fcnStr = 'btn_template_'+val;
		let serverApi = 'patterns/update';
		let valE = elem[val].value;
		let actCheck = (elem[val].applies)?'active':'';
		let ofLi = (elem[val].applies)?'':'of';
		
		print+= '<li data-param="'+val+'" class="one-param '+ofLi+'">';
		
		if(view == 'name'){
			let nameVal = valE;
			let activeAutoNumb = (elem[val].auto_numbering)?'active':'';
			print+= '\
			<span class="name">'+name+'</span>\
			<span class="param">\
				<input type="text" class="'+fcnStr+'" value="'+nameVal+'">\
				<span class="btn-active checkbox auto_number '+activeAutoNumb+'"></span> - авто. номерация\
			</span>\
			';
			
			//
			
			HWS.on('change',fcnStr,function(e,el){
				let newName = el.value;
				let newInfo = {}; newInfo[val]={}; newInfo[val].value = newName;
				HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:elem.id,updates:newInfo}});
			});
			
			HWS.on('click','auto_number',function(e,el){
				let newInfo = {}; newInfo[val]={};
				if(el.classList.contains('active')){newInfo[val].auto_numbering = 1;} else {newInfo[val].auto_numbering = 0;}
				HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:elem.id,updates:newInfo}});
			});
		}
		
		if(view == 'switch'){
			let act = (valE)?'active':'';
			print+='\
			<span class="name">'+name+'</span>\
			<span class="param"><span class="btn-active on-off '+fcnStr+' '+act+'"></span></span>\
			';
			
			HWS.on('click',fcnStr,function(e,el){
				let newInfo = {}; newInfo[val]={};
				if(el.classList.contains('active')){newInfo[val].value = 1;} else {newInfo[val].value = 0;}
				HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:elem.id,updates:newInfo}});
			});
		}
		
		if(view == 'number'){
			let numb = valE;
			let min = data.min || '';
			let max = data.max || '';
			print+='\
			<span class="name">'+name+'</span>\
			<span class="param">'+HWhtml.btn.numb(numb,{fcn:fcnStr,min:min,max:max})+'</span>\
			';
			
			HWS.buffer[fcnStr] = function(nn){
				let newInfo = {}; newInfo[val]={}; newInfo[val].value = nn;
				HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:elem.id,updates:newInfo}});
			}
		}
		
		if(view == 'text'){
			let nameVal = valE;
			print+='\
			<span class="name">'+name+'</span>\
			<span class="param"><input type="text" class="'+fcnStr+'" value="'+nameVal+'"></span>\
			';
			
			HWS.on('change',fcnStr,function(e,el){
				let newInfo = {}; newInfo[val]={}; newInfo[val].value = el.value;
				HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:elem.id,updates:newInfo}});
			});
		}
		
		if(view == 'select'){
			let nameVal = data.list;
			let elemAct = valE; if(valE === true){valE = 1}
			print+='\
			<span class="name">'+name+'</span>\
			<span class="param">'+HWhtml.select(nameVal,fcnStr,elemAct)+'</span>\
			';
			
			HWS.on('change',fcnStr,function(e,el){
				let newInfo = {}; newInfo[val]={}; newInfo[val].value = el.value;
				HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:elem.id,updates:newInfo}});
			});
		}
		
		print+= '<span class="btn-active checkbox btn_check_template '+actCheck+'"></span>'
		print+= '</li>';
		
		if(!HWS.allEvent.clickHWn.l || !HWS.allEvent.clickHWn.l.btn_check_template){
			HWS.on('click','btn_check_template',function(e,el){
				let block = HWS.upElem(el,'.one-param');
				let param = HWS.upElem(el,'.one-param').dataset.param;
				let newInfo = {}; newInfo[param]={};
				
				if(el.classList.contains('active')){block.classList.remove('of'); newInfo[param].applies = 1;}
				else {block.classList.add('of'); newInfo[param].applies = 0;}
				HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:elem.id,updates:newInfo}});
			});
		}

		return print
	} else {return '';} }
}
//Template function
function graph(){
	this.massGraph = function(info){am4core.ready(function() {
		if(!HWS.buffer.graph){HWS.buffer.graph = {}}
		let blockGraph = info.target;
		let allAnalog = info.analog || [];
		let allDisc = info.discret || [];
		let sumDisc = 0;//info.summ || 0;
		let id = info.id || 1;
		
		am4core.useTheme(am4themes_animated);
		let chart = am4core.create(blockGraph, am4charts.XYChart);
			chart.padding(0, 0, 0, 0);
			chart.colors.step = 3;
			chart.dateFormatter.utc = true;
			chart.language.locale = am4lang_ru_RU;
		let data = [];
		let seriesGraph = {}, graphAxis = {};

		HWF.recount(allAnalog,function(infoEl,key){
			let valT = 'pricel'+key; 
			let vals = infoEl.values;
			let addD = {};
			HWF.recount(vals,function(val){	
				let unit = infoEl.unit || '';
				addD = {unit:unit}; 
				addD[valT] = val.value; 
				addD['date'+key] = val.timestamp;
				data.push(addD); 
			});
		});
		
		HWF.recount(allDisc,function(infoEl,key){
			let valT = 'pricel'+key; 
			let vals = infoEl.values; 
			HWF.recount(vals,function(val) {
				//let unit = infoEl.unit || ''
				let statColor = HWD.color.grey;;
				if(val.state == 0) {statColor = HWD.color.grey;}
				if(val.state == 1) {statColor = infoEl.colorLine || HWD.color.blue;}
				if(val.state == 8) {statColor = HWD.color.grey;}
				if(val.state == 9) {statColor = infoEl.colorLine || HWD.color.blue;}
				if(val.state == 5) {statColor = HWD.color.green;}
				if(val.state == 10){statColor = HWD.color.red;}
				if(val.state == 11){statColor = HWD.color.grey;}
				if(val.state == 12){statColor = infoEl.colorLine || HWD.color.blue;}
				if(val.state == 30){statColor = HWD.color.grey;}
				if(val.state == 31){statColor = infoEl.colorLine || HWD.color.blue;}
				let addD = {color:statColor};
				addD[valT] = 1;//val.value;
				addD['date'+key] = val.timestamp;
				data.push(addD); 
			});
			sumDisc++;
		});
		
		blockGraph.style.height = ((sumDisc*100)+280)+'px';
		chart.data = data;
		chart.leftAxesContainer.layout = "vertical";
		
		let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
		dateAxis.renderer.grid.template.location = 0;
		dateAxis.renderer.ticks.template.length = 8;
		dateAxis.renderer.ticks.template.strokeOpacity = 0.1;
		dateAxis.renderer.grid.template.disabled = true;
		dateAxis.renderer.ticks.template.disabled = false;
		dateAxis.renderer.ticks.template.strokeOpacity = 0.2;
		dateAxis.renderer.minLabelPosition = 0.01;
		dateAxis.renderer.maxLabelPosition = 0.99;
		dateAxis.keepSelection = true;
		dateAxis.minZoomCount = 5;
		//dateAxis.groupData = false;
		dateAxis.groupData = true;

		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.zIndex = 1;
		valueAxis.tooltip.disabled = true;
		valueAxis.renderer.baseGrid.disabled = true;
		//valueAxis.height = am4core.percent(65);
		valueAxis.renderer.gridContainer.background.fill = am4core.color("#000000");
		valueAxis.renderer.gridContainer.background.fillOpacity = 0.05;
		valueAxis.renderer.inside = true;
		valueAxis.renderer.labels.template.verticalCenter = "bottom";
		valueAxis.renderer.labels.template.padding(2, 2, 2, 2);
		valueAxis.renderer.fontSize = "0.8em"
		
		HWF.recount(allAnalog,function(infoEl,key){
			let valT = 'pricel'+key;
			//let unit = infoEl.unit
			let colorLine = infoEl.colorLine || '';
			let nameLabel = infoEl.nameLabel || '';
			
			seriesGraph[key] = chart.series.push(new am4charts.LineSeries());
			seriesGraph[key].dataFields.dateX = 'date'+key;
			seriesGraph[key].dataFields.valueY = valT;
			//seriesGraph[key].dataFields.valueYShow = "changePercent";
			seriesGraph[key].tooltipText = "{valueY} {unit} {dateX}";
			seriesGraph[key].tooltip.getFillFromObject = false;
			seriesGraph[key].tooltip.getStrokeFromObject = true;
			seriesGraph[key].tooltip.background.fill = am4core.color("#fff");
			seriesGraph[key].strokeWidth = 2;
			seriesGraph[key].tooltip.background.strokeWidth = 2;
			seriesGraph[key].tooltip.label.fill = am4core.color("#000000");
			if(colorLine){seriesGraph[key].stroke = am4core.color(colorLine);}
			if(nameLabel){seriesGraph[key].name = nameLabel;}
		});
		
		HWF.recount(allDisc,function(infoEl,key){
			console.log(infoEl);
			let valT = 'pricel'+key;
			//let unit = infoEl.unit;
			let colorLine = infoEl.colorLine || '';
			let nameLabel = infoEl.nameLabel || '';
			let graphAxis1 = chart.yAxes.push(new am4charts.ValueAxis());
			let seriesGraph1 = chart.series.push(new am4charts.LineSeries());
			
			graphAxis1.max  = 1;
			graphAxis1.min  = 0;
			graphAxis1.zIndex = 3;
			graphAxis1.baseValue = -10;
			graphAxis1.tooltip.disabled = true;
			graphAxis1.renderer.baseGrid.disabled = true;
			graphAxis1.height = am4core.percent(35);			
			graphAxis1.renderer.gridContainer.background.fill = am4core.color("#D3D4D3");
			graphAxis1.renderer.gridContainer.background.fillOpacity = 0.05;
			graphAxis1.renderer.inside = true;
			graphAxis1.renderer.labels.template.disabled = true;
			graphAxis1.renderer.labels.template.verticalCenter = "bottom";
			graphAxis1.renderer.labels.template.padding(2, 2, 2, 2);
			graphAxis1.renderer.fontSize = "0.8em";	
			graphAxis1.marginTop = 30;

			seriesGraph1.dataFields.dateX = 'date'+key;
			seriesGraph1.dataFields.valueY = valT;		
			seriesGraph1.propertyFields.stroke = 'color';
			seriesGraph1.propertyFields.fill = 'color';
			seriesGraph1.strokeWidth = 0;
			//seriesGraph1.tooltipText = "{unit}";
			seriesGraph1.tooltip.background.strokeWidth = 0;
			seriesGraph1.fillOpacity = 0.6;
			seriesGraph1.yAxis = graphAxis1;
			seriesGraph1.tooltip.getFillFromObject = false;
			seriesGraph1.tooltip.label.fill = am4core.color("#D3D4D3");
			if(colorLine){seriesGraph1.fill = am4core.color(colorLine);}
			if(nameLabel){seriesGraph1.name = nameLabel;}
		});
		
		chart.cursor = new am4charts.XYCursor();
		chart.scrollbarX = new am4core.Scrollbar();
		chart.legend = new am4charts.Legend();
		HWS.buffer.graph[id] = chart;
	});}
}
// Service Worker
function service_worker(){
	this.reg = function(worker) {
		let wor = worker || '/sw.js'
		if ('serviceWorker' in navigator) {
		navigator.serviceWorker.register(wor)
			.then(function(registration) {})
			.catch(function(err) {console.log('ServiceWorker registration failed: ', err)});
		}
	}
	this.del = function(){
		navigator.serviceWorker.getRegistrations().then(function(reg){
			for(let key in reg){reg[key].unregister();}
		});
	}
	this.btn = function(){
		let deferredPrompt;
		let testBtn = document.createElement('div');
			testBtn.classList.add('add-pwa');
		window.addEventListener('beforeinstallprompt',function(e){
			e.preventDefault();
			deferredPrompt = e;
			testBtn.innerHTML = 'Установить как Приложение';
			
			let targetBtn = document.querySelector('#btn-pwa-load-application');
			
			targetBtn.appendChild(testBtn);
			  
			setTimeout(function(){targetBtn.classList.add('active')},2000);
		});
		
		testBtn.addEventListener('click',function(){
		  deferredPrompt.prompt();
		  deferredPrompt.userChoice
			.then(function(choiceResult){
			  if (choiceResult.outcome === 'accepted') {
				console.log('User accepted the A2HS prompt');
			  } else {
				console.log('User dismissed the A2HS prompt');
				deferredPrompt = null;
			  }
			  deferredPrompt = null;
			});
		});
	}
}

var HWequip = new equipment; // функции только для Оборудования 
var HWmain = new mainPage; // функции только для главной страници
var HWprog = new programse; // функции только для програм
var HWtemp = new template; // функции только для шаблонов
var HWF = new globalF; // Глобальные различные вспомогательные функции
var HWG = new graph; // функции для графиков
var HWSW = new service_worker; // для работы с сервис воркерами
var HWfilter = new filter; // для работы с фильтрам


// window.getComputedStyle([HTMLElem]).color - получить стиль элемента из CSS
