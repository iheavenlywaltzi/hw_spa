"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let noConnectBlock = HWS.getElem('#no-connect-block');
	let info = HWS.buffer.elem;
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = info.id;
	let normInfo = HWequip.normInfo(info);
	let C = {}; // якоря
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageGraph">График</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
	HWS.buffer.elem.id_system = info.id_system;
	HWS.buffer.pageOpenControl = 'equpPageSets';

	let pTop = {}; 
	pTop.name = info.config.name || '';
	pTop.title = normInfo.et.title || 'none';
	pTop.icon = info.info.type.type || '';
	
	if(normInfo.port == 10 || normInfo.port == 12){
		pTop.ident = normInfo.ident || false;
		pTop.addr = (normInfo.portSymbol+normInfo.addr) || false;
		pTop.chanel = normInfo.channel_index || false;
	} else {pTop.port = normInfo.portSpec || false; }
	
	// статус по котлу
	pTop.data = info.lk.state_name || '';
	pTop.blambaColor = HWF.getStateColor(info.lk.state);
	pTop.meterage = '';

	HWS.buffer.equpPageSets = function(type){type = type || false;
		HWS.buffer.pageOpenControl = 'equpPageSets';
		B.title.append('Настройки элемента оборудования');
		clear_graph();
		if(type == '1'){print_sets(info)}
		else{
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	
	HWS.buffer.equpPageGraph = function(){
		noConnectBlock.classList.remove('on');
		HWS.buffer.pageOpenControl = 'equpPageGraph';
		let timeGraph = HWS.buffer.timeValueGraph || 1;
		clear_graph();
		let print = '<div class="one-block-col">\
			<div class="title"><h4>Статистика работы отопителя  <span title="текст" class="helper">?</span></h4></div>\
			<div class="ltft"></div>\
			<div class="title">\
				<div id="equp-statistic-graf" class="block-w-100">'+
					HWhtml.timeControler(timeGraph,'timeControlLoadAllGraph','',{load:'of'})+
					'<div class="graph-window-heating block-w-100" style="height:200px;"> '+HWhtml.loader()+' </div>'+
				'</div>'+
			'</div>'+
		'</div>';
		
		content.innerHTML = print;
		create_graph(timeGraph);
		
		B.title.append('График элемента оборудования');
		//content.innerHTML = HWhtml.equp.graph(1,'timeControlFCNAnalog','timeControlLoadFCN');
	}
	
	HWS.buffer.equpPageJurnal = function(){
		noConnectBlock.classList.remove('on');
		B.title.append('Журнал элемента оборудования');
		content.innerHTML = "";
		clear_graph();
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'equpPageJurnal';
	}
	
	HWS.buffer.equpPageAccesses = function(){
		noConnectBlock.classList.remove('on');
		clear_graph();
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
		HWS.buffer.equpAccessesFCN();
	}	
	
	// Функции для работы
	
	// если устройство используется то прогрузим
	function used_elem(){
		let elem = HWS.buffer.elem;
		let localId = elem.state.block_local_id;
		let systemId = elem.id_system;
		let IBused = HWS.getElem('#top-dop-info-page-bottom');
		
		if(!localId){
			IBused.innerHTML = '';
		}
		else{
			HWF.post({url:HWD.api.gets.objectLocalId,data:{token:token,local_id:localId,id_system:systemId},fcn:function(inf){
				IBused.innerHTML = '<p class="flex_between tc_grey6 ts_11p block_pl_50">\
					<span class="block_mr_6">Используется в </span>\
					<span class="tw_600 block_mr_6"><a href="#'+HWD.page.prog+'/'+inf.id+'" class="tc_grey6">'+inf.config.name+'</a></span>\
					<a href="#'+HWD.page.prog+'/'+inf.id+'" class="icon icon-settings p13 hover btn-proj-change"></a>\
				</p>';
			}});
		}
	}
	
	function print_sets(info) {
		let normInfo = HWequip.normInfo(info);
		C.globalSets = 1;
		HWS.buffer.pageOpenControl = 'equpPageSets';
		Sprint = '';
		let onof = normInfo.active; if(onof){onof = 'active';};
		let journal = info.config.log; if(journal){journal = 'active';}
		let D = {};
		D.contur_1 = (info.config.channel_1_status)?'active':'';
		D.contur_2 = (info.config.channel_2_status)?'active':'';
		D.crash_temp = info.config.crash_temperature || 0;
		D.channel_min = info.config.channel_1_min_temperature || 0;
		D.channel_max = info.config.channel_1_max_temperature || 0;			
		D.press_min = info.config.press_min || 0;
		D.press_max = info.config.press_max || 0;
		D.flame = info.config.flame || 0;
		D.pza_number = info.config.pza_curve_number || 0;
		D.heat_water = (info.config.heat_water)?'active':'';
		D.heat_water_temp = info.config.set_heat_water_temperature || 0;
		D.heat_water_max_temperature = info.config.heat_water_max_temperature || 0;
		D.heat_water_min_temperature = info.config.heat_water_min_temperature || 0;
		D.flameColor = (info.state.flame)?HWD.color.blue:HWD.color.grey;
		D.radiatorColor = (info.state.channel_1)?HWD.color.blue:HWD.color.grey;
		D.faucetColor = (info.state.heat_water)?HWD.color.blue:HWD.color.grey;
		D.modul_flame = '';// моуляция горелки
		D.coolant_temperature = info.state.coolant_temperature || '';
			if(D.coolant_temperature){D.coolant_temperature = D.coolant_temperature.toFixed(1)}
		D.hot_water_supply_temperature = info.state.hot_water_supply_temperature || ''; 
			if(D.hot_water_supply_temperature){D.hot_water_supply_temperature = D.hot_water_supply_temperature.toFixed(1)}
		D.bar = info.state.pressure || ''; 
			if(D.bar == 255){D.bar = '';}
		D.hot_water_supply_expense = info.state.hot_water_supply_expense || ''; 
			if(D.hot_water_supply_expense){D.hot_water_supply_expense = D.hot_water_supply_expense.toFixed(1)}
		D.adapterError = 'of'; D.adapterErrorCod = ''; D.adapterErrorText = '';
			if(info.error_description && info.error_description.status){
				D.adapterError = 'on'; D.adapterErrorCod = info.error_description.code || ''; D.adapterErrorText = info.error_description.details || '';
			}
		
		D.coolant_temperature_unit = '°';
		D.hot_water_supply_temperature_unit = '°';
		D.bar_unit = 'бар';
		D.hot_water_supply_expense_unit = 'л/мин';
		if(!D.coolant_temperature || D.coolant_temperature == 32767 || D.coolant_temperature == 255) {D.coolant_temperature = ''; D.coolant_temperature_unit = '';}
		if(!D.hot_water_supply_temperature || D.hot_water_supply_temperature == 32767 || D.hot_water_supply_temperature == 255) {D.hot_water_supply_temperature = ''; D.hot_water_supply_temperature_unit = '';}
		if(!D.bar || D.bar == 32767 || D.bar == 255) {D.bar = ''; D.bar_unit = '';}
		if(!D.hot_water_supply_expense || D.hot_water_supply_expense == 32767 || D.hot_water_supply_expense == 255) {D.hot_water_supply_expense = ''; D.hot_water_supply_expense_unit = '';}
		
		let favorite = (info.lk.favorite)?'active':'';
			
		let connect = info.system_connection_status || false;
		if(!connect && info.cloud_type != 14719) {noConnectBlock.classList.add('on')}
		else {noConnectBlock.classList.remove('on');}
			//D.adapterError = 'on'; D.adapterErrorCod = '123'; D.adapterErrorText = 'Текст Ошибки';
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Модель котла '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerModel)+'</p>\
					<p>'+HWhtml.select(info.allowed_boiler_models,'block_w_170p model_boiler',info.config.boiler_model)+'</p>\
				</div>\
				<div class="colum">\
					<p>Журнал '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerActive)+'</p>\
					<span class="btn_equp_jurnal btn-active on-off '+journal+'"></span>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.equp.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+favorite+'" ></span>\
				</div>\
			</div>\
		</div>';
		
		Sprint += '<div class="one-block-col">\
			<div class="title"><h4>Состояние</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum4x">\
					<p>Состояние '+HWhtml.helper(HWD.helpText.prog.heatingTakeTempState)+'</p>\
					<br>\
					<p class="display_inline ta_center">\
						<span id="icon-state-cotel-flsme" class="icon icon-programs-flame" style="background:'+D.flameColor+'"></span>\
						<span id="icon-state-cotel-radiator" class="icon icon-radiator" style="background:'+D.radiatorColor+'"></span>\
						<span id="icon-state-cotel-faucet" class="icon icon-faucet" style="background:'+D.faucetColor+'"></span>\
						<br>\
					</p>\
				</div>\
				<!--div class="colum4x">\
					<p>Модуляция горелки</p><br>\
					<p><span id="boil_modul_flame" class="ts_36p tf_mb">'+D.modul_flame+'</span> <span class="tf_mb ts_24p">'+'%'+'<span></p>\
				</div-->\
				<div class="colum4x">\
					<p>Теплоноситель</p><br>\
					<p><span id="boil_coolant_temperature" class="ts_36p tf_mb">'+D.coolant_temperature+'</span> <span class="tf_mb ts_24p">'+D.coolant_temperature_unit+'<span></p>\
				</div>\
				<div class="colum4x">\
					<p>Горячая вода</p><br>\
					<p><span id="boil_hot_water_supply_temperature" class="ts_36p tf_mb">'+D.hot_water_supply_temperature+'</span> <span class="tf_mb ts_24p">'+D.hot_water_supply_temperature_unit+'<span></p>\
				</div>\
				<div class="colum4x">\
					<p>Давление</p><br>\
					<p><span id="boil_bar" class="ts_36p tf_mb">'+D.bar +'</span> <span class="tf_mb ts_24p">'+D.bar_unit+'<span></p>\
				</div>\
				<div class="colum4x">\
					<p>Расход</p><br>\
					<p><span id="boil_hot_water_supply_expense" class="ts_36p tf_mb">'+D.hot_water_supply_expense+'</span> <span class="tf_mb ts_24p">'+D.hot_water_supply_expense_unit+'<span></p>\
				</div>\
				\
				<div id="error-block-info" class="colum690 adapter-error '+D.adapterError+'">\
					<div class="colum4x">\
						<div class="adapter-of tc_green tf_fr900"><span class="icon icon-check green p32"></span>Ошибок нет</div>\
						<div class="adapter-on">\
							<p class="tc_red tf_fr900 ts_18p block_pb_20">\
								<span class="icon icon-alarm red p32"></span>\
								<spna class="vertical-middle block_pl_5"><span>Ошибка:</span> <span id="code-error">'+D.adapterErrorCod +'</span></span>\
							</p>\
							<p><span class="style_btn green btn_rebout_error p2">Сброс ошибки</span></p>\
						</div>\
					</div>\
					<div class="colum400 adapter-on">\
						<p id="text-info-error" class="tf_fr500">'+D.adapterErrorText+'</p>\<br>\
					</div>\
				</div>\
			</div>\
		</div>';
		D.adapterError = 'of'; D.adapterErrorCod = ''; D.adapterErrorText = '';
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Модуляции горелки '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerModulGorel)+'</h4></div>\
			<div class="ltft"></div>\
			<div class="right"></div>\
			<div class="title">'+HWhtml.btn.scrol(D.flame,{
				min:0,max:100,unit:'%',fcn:'modulGorelMax',id:'modul-gorel',
				helper:HWD.helpText.equp.btnHelpboilerModulGorelMax
			})+'</div>\
		</div>';
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Отопление</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Первый контур '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerKontur1)+'</p>\
					<span class="btn_contur_1 btn-active on-off '+D.contur_1+'"></span>\
				</div>\
				<div class="colum">\
					<p>Второй контур '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerKontur2)+'</p>\
					<span class="btn_contur_2 btn-active on-off '+D.contur_2+'"></span>\
				</div>\
				<div class="colum">\
					<p>Температура при аварии <br> датчика, °C '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerTempCrash)+'</p>\
					'+HWhtml.btn.numb(D.crash_temp,{min:0,max:100,id:'temp-equp-alarm',fcn:'btnTempCrash'})+'\
				</div>\
			</div>\
		</div>';		
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Теплоноситель</h4></div>\
			<div class="ltft"></div>\
			<div class="right"></div>\
			<div class="title">'+HWhtml.btn.scrolDuo([D.channel_min,D.channel_max],{
				min:0,max:100,unit:'°',fcn:'channelMaxTmp',id:'coolant',
				nameMinHelp:HWD.helpText.equp.btnHelpboilerHeatСarrierMin,
				nameMaxHelp:HWD.helpText.equp.btnHelpboilerHeatСarrierMax
			})+'</div>\
		</div>';
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Давление</h4></div>\
			<div class="ltft"></div>\
			<div class="right"></div>\
			<div class="title">'+HWhtml.btn.scrolDuo([D.press_min,D.press_max],{
				min:1,max:5,step:4,unit:' Бар',tofix:1,fcn:'pressMinMaxBar',id:'pressure',
				nameMinHelp:HWD.helpText.equp.btnHelpboilerPressureMin,
				nameMaxHelp:HWD.helpText.equp.btnHelpboilerPressureMax
			})+'</div>\
		</div>';
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>ПЗА</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Номер кривой ПЗА '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerPZA)+'</p>\
					<p>'+HWhtml.select([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],'btn_select_pza block_w_170p',(D.pza_number-1))+'</p>\
				</div>\
			</div>\
			<div class="title">\
				<div id="spec-svg-boiler-control" class="block_pl_25" data-selel="">\
					<object onload="HWequip.openThermInitMap(this)" type="image/svg+xml" data="img/wac.svg" data-selel="'+D.pza_number+'" data-fcn="btnChangePZAobject"></object>\
				</div>\
			</div>\
		</div>'; 		
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>ГВС</h4></div>\
			<div class="ltft"></div>\
			<div class="right block_pb_50">\
				<div class="colum">\
					<p>Включить ГВС '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerGVS)+'</p>\
					<span class="btn_onof_gvs btn-active on-off '+D.heat_water+'"></span>\
				</div>\
				<div class="colum">\
					<p>Поддерживать, °C '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerTemp)+'</p>\
					'+HWhtml.btn.numb(D.heat_water_temp,{min:0,max:100,id:'support-temp',fcn:'setHeatWaterTemperature'})+'\
				</div>\
			</div>\
			<div class="title"><h4>Пределы регулировки ГВС котлом '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerPZApridel)+'</h4><br>'+
			HWhtml.btn.scrolDuo([D.heat_water_min_temperature,D.heat_water_max_temperature],{
				min:0,max:100,unit:'°',fcn:'GVSPredelMinMaxTemp',id:'gvs-kotla-predel',
				nameMinHelp:HWD.helpText.equp.btnHelpboilerHeatСarrierMinKotel,
				nameMaxHelp:HWD.helpText.equp.btnHelpboilerHeatСarrierMaxKotel
			})+'</div>\
		</div>'; 
		
		if (normInfo.port == 12 || normInfo.port == 11 || normInfo.port == 3){
			Sprint += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin"></p>\
					<p class="btn_text grey btn_delete_equp">Удалить устройство</p>\
				</div>\
			</div>\
		</div>';
		}
		
		content.innerHTML = Sprint
		
		// запуск кнопок Скоролов
		HWequip.scrol.start();
	}
	
	 // переодическое обновление
	function reolad_info_page(){
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(inf){
			//let normInfoR = HWequip.normInfo(inf);
			let blamba = HWS.getElem('#blamba');
			let D = {};
			let stateName = HWS.getElem('.page-top-dop-info .data inf');
			if(inf.state.block_local_id) {used_elem();}

			if(HWS.buffer.pageOpenControl == 'equpPageSets' && !C.globalSets){
				let nameEqup = HWS.getElem('#equp-content-top .name-page .equp_name');
				let jurnal = HWS.getElem('.btn_equp_jurnal');
				let favorite = document.body.querySelector('#equp-viget');
				let contur_1 = HWS.getElem('.btn_contur_1');
				let contur_2 = HWS.getElem('.btn_contur_2');
				let tmpEqupAlarm = HWS.getElem('#temp-equp-alarm .inp_num_val');
				let boilCoolantTemperature = HWS.getElem('#boil_coolant_temperature');
				let boilHotWaterSupplyTemperature = HWS.getElem('#boil_hot_water_supply_temperature');
				
				if(inf.state.coolant_temperature && inf.state.coolant_temperature != 32767) {
					D.coolant_temperature = inf.state.coolant_temperature;
					boilCoolantTemperature.innerHTML = D.coolant_temperature.toFixed(1);
				} else {boilCoolantTemperature.innerHTML = '';}
				if(inf.state.hot_water_supply_temperature && inf.state.hot_water_supply_temperature != 32767) {
					boilHotWaterSupplyTemperature.innerHTML = inf.state.hot_water_supply_temperature.toFixed(1)
				} else {boilHotWaterSupplyTemperature.innerHTML = '';}
				if(nameEqup.value != inf.config.name && !HWS.buffer.nameFocus) {nameEqup.value = inf.config.name;}
				if(!C.jurnal) {(inf.config.log)?jurnal.classList.add('active'):jurnal.classList.remove('active');}
				if(!C.favorite) {(inf.lk.favorite)?favorite.classList.add('active'):favorite.classList.remove('active');}
				
				if(!C.contur_1) {(inf.config.channel_1_status)?contur_1.classList.add('active'):contur_1.classList.remove('active');}
				if(!C.contur_2) {(inf.config.channel_2_status)?contur_2.classList.add('active'):contur_2.classList.remove('active');}
				if(!C.tmpEqupAlarm && inf.config.crash_temperature != info.config.crash_temperature) {
					info.config.crash_temperature = inf.config.crash_temperature; 
					tmpEqupAlarm.value = inf.config.crash_temperature;
				}
				
				if(!C.coolant && (info.config.channel_1_min_temperature != inf.config.channel_1_min_temperature || info.config.channel_1_max_temperature != inf.config.channel_1_max_temperature)) {
					info.config.channel_1_min_temperature = inf.config.channel_1_min_temperature;
					info.config.channel_1_max_temperature = inf.config.channel_1_max_temperature;
					HWF.scrolDuoChange('#coolant',inf.config.channel_1_min_temperature,inf.config.channel_1_max_temperature);
				}
				if(!C.pressure && (info.config.press_min != inf.config.press_min || info.config.press_max != inf.config.press_max)) {
					info.config.press_min = inf.config.press_min;
					info.config.press_max = inf.config.press_max;
					HWF.scrolDuoChange('#pressure',inf.config.press_min,inf.config.press_max);
				}
				if (!C.pza && info.config.pza_curve_number != inf.config.pza_curve_number){
					info.config.pza_curve_number = inf.config.pza_curve_number;
					let numPza = HWS.getElem('.btn_select_pza');
					let graph  = HWS.getElem('#spec-svg-boiler-control object');
					let svg = graph.contentDocument;
					let val = parseInt(inf.config.pza_curve_number);
					
					numPza.value = val-1;
					svg.querySelector('.wac.selection').classList.remove('selection');
					svg.querySelector('#wac-'+val).classList.add('selection');
				}
				
				if(!C.gvs && info.config.heat_water != inf.config.heat_water) {
					info.config.heat_water = inf.config.heat_water;
					let gvs = HWS.getElem('.btn_onof_gvs');
						if(inf.config.heat_water){gvs.classList.add('active');}
						else {gvs.classList.remove('active');}
				}
				
				if(!C.support && info.config.set_heat_water_temperature != inf.config.set_heat_water_temperature) {
					info.config.set_heat_water_temperature = inf.config.set_heat_water_temperature;
					let support = HWS.getElem('#support-temp .inp_num_val');
						support.value = inf.config.set_heat_water_temperature;
				}
				
				let errorBlock = HWS.getElem('#error-block-info');
				if(inf.error_description && inf.error_description.status){
					let errorCode = HWS.getElem('#code-error');
					let textrCode = HWS.getElem('#text-info-error');
					errorBlock.classList.remove('of');
					errorBlock.classList.add('on');
					errorCode.innerHTML = inf.error_description.code || ''; 
					textrCode.innerHTML = inf.error_description.details || '';
				} else {
					errorBlock.classList.remove('on');
					errorBlock.classList.add('of');
				}
			}
			
			// статус по котлу
			//if(!inf.connection){stateName.innerHTML = 'Нет связи'; blamba.style.background = HWD.color.grey;}
			//else if(inf.state.pot_link_to_boiler != 0){stateName.innerHTML = 'Котел подключен'; blamba.style.background = HWD.color.green;} 
			//else {stateName.innerHTML = 'Нет котла'; blamba.style.background = HWD.color.grey;}
			stateName.innerHTML = info.lk.state_name || '';
			blamba.style.background = HWF.getStateColor(info.lk.state);

			C = {};
		}});
	}
	
	// Функци для графиков
	function clear_graph(){
		if(!HWS.buffer.graph){HWS.buffer.graph = {}}; 
		if(HWS.buffer.graph[id]){
			HWS.buffer.graph[id].dispose();
			HWS.buffer.graph[id] = null;
			HWS.buffer.controlAutoReoladStoper = true;
		}
	}
	
	function create_graph(time){ time = time || 1;
		clear_graph();
		let mainBlock = HWS.getElem('#equp-statistic-graf');
		let blockGraph = mainBlock.querySelector('.graph-window-heating');
			blockGraph.innerHTML = HWhtml.loader();
		let gTime = HWS.buffer.system_time || Date.now();
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		let arrGet = [], allAnalog = {}, allDisc = {};
			
		arrGet.push({object_id:id})
		let dataGet = {token:token,timestamp_start:sTime,timestamp_end:eTime,charts:arrGet}
		HWF.post({url:HWD.api.gets.historyСharts,data:dataGet,fcn:function(infGraph){ 
			let charts = infGraph[id].charts;
			HWF.recount(charts,function(oneChart,key){
				console.log(oneChart);
				let addData = {unit:oneChart.unit || '', values:oneChart.values || []}
				if(oneChart.index == 1){addData.colorLine = '#569AD4'; addData.nameLabel = 'Горелка'; allDisc[key] = addData;}
				if(oneChart.index == 2){addData.colorLine = '#FF0000'; addData.nameLabel = 'Теплоноситель'; allAnalog[key] = addData;}
				if(oneChart.index == 3){addData.colorLine = '#915924'; addData.nameLabel = 'ГВС'; allAnalog[key] = addData;}
				if(oneChart.index == 4){addData.colorLine = '#757575'; addData.nameLabel = 'Расход ГВС'; allAnalog[key] = addData;}
				if(oneChart.index == 5){addData.colorLine = '#47D247'; addData.nameLabel = 'Давление'; allAnalog[key] = addData;}
				if(oneChart.index == 9){addData.colorLine = '#FF0000'; addData.nameLabel = 'Ошибка'; allDisc[key] = addData;}
			});
			HWG.massGraph({
				id:id,
				target:blockGraph,
				analog:allAnalog,
				discret:allDisc,
			});
		}});
	}
	
	// Работа граффика
	HWS.buffer.timeControlLoadAllGraph = function(H){
		create_graph(H);
		let dataP = {token:HWS.pageAccess,id_object:id,web_config:{time_per_graph:H}}
		HWF.post({url:HWD.api.gets.setWebConfig,data:dataP});
	}
	
	//отрисовка HTML Шапки
	pTop.systemName = info.system_name;
	pTop.systemCN = info.system_ident;
	
	pTop = HWhtml.equp.top(pTop);
	contentTop.innerHTML = pTop;
	HWS.buffer.share_stick(info);
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	used_elem();
	// автообновление
	HWF.timerUpdate(reolad_info_page);
	// КНОПКИ  taskOpentherm3xsystem
	
	// Температура при аварии
	HWS.buffer.btnTempCrash = function(inf){
		C.tmpEqupAlarm = 1;
		let dataP = {token:token,id_object:id,crash_temperature:inf}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	}
	
	// Теплоноситель
	HWS.buffer.channelMaxTmp = function(inf){
		C.coolant = 1;
		let dataP = {token:token,id_object:id,channel_1_min_temperature:inf.min,channel_1_max_temperature:inf.max}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	}
	
	// Давление
	HWS.buffer.pressMinMaxBar = function(inf){
		C.pressure = 1;
		let dataP = {token:token,id_object:id,press_min:inf.min,press_max:inf.max}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	}
	
	// Макс мин темпратуры ГВС
	HWS.buffer.GVSPredelMinMaxTemp = function(inf){
		C.gvsPred = 1;
		let dataP = {token:token,id_object:id,heat_water_min_temperature:inf.min,heat_water_max_temperature:inf.max}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	}	
	
	// Максимум горели
	HWS.buffer.modulGorelMax = function(inf){
		C.gorelka = 1;
		let dataP = {token:token,id_object:id,flame:inf.max}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	}
	
	// Поддерживать set_heat_water_temperature
	HWS.buffer.setHeatWaterTemperature = function(inf){
		C.support = 1;
		let dataP = {token:token,id_object:id,set_heat_water_temperature:inf}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	}
	
	// Журнал
	HW.on('click','btn_equp_jurnal',function(e,elem){
		C.jurnal = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:token,id_object:id,log:1}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
		}else { // Of
			let dataP = {token:token,id_object:id,log:0}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
		}
	});	
	
	//  ПЗА
	HW.on('change','btn_select_pza',function(e,elem){
		C.pza = 1;
		let graph  = HWS.getElem('#spec-svg-boiler-control object');
		let svg = graph.contentDocument;
		let val = parseInt(elem.value)+1;
		
		svg.querySelector('.wac.selection').classList.remove('selection');
		svg.querySelector('#wac-'+val).classList.add('selection');
		
		let dataP = {token:token,id_object:id,pza_curve_number:val}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	});
	
	HWS.buffer.btnChangePZAobject = function(inf){
		C.pza = 1;
		let selector = HWS.getElem('.btn_select_pza');
			selector.value = inf-1;
		let dataP = {token:token,id_object:id,pza_curve_number:inf}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	}
	
	//  Контур - 1
	HW.on('click','btn_contur_1',function(e,elem){
		C.contur_1 = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:token,id_object:id,channel_1_status:1}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
		}else { // Of
			let dataP = {token:token,id_object:id,channel_1_status:0}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
		}
	});		
	//  Контур - 2
	HW.on('click','btn_contur_2',function(e,elem){
		C.contur_2 = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:token,id_object:id,channel_2_status:1}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
		}else { // Of
			let dataP = {token:token,id_object:id,channel_2_status:0}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
		}
	});	
	
	// Включить ГВС
	HW.on('click','btn_onof_gvs',function(e,elem){
		C.gvs = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:token,id_object:id,heat_water:1}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
		}else { // Of
			let dataP = {token:token,id_object:id,heat_water:0}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
		}
	});	
	
	HW.on('change','model_boiler',function(e,el){
		let dataP = {token:token,id_object:id,boiler_model:el.value}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	});
	
	// Нажатие на кнопку сброс ошбки
	HW.on('click','btn_rebout_error',function(e,el){
		let title = '<spam class="">ВНИМАНИЕ! ЭТО МОЖЕТ ПРИВЕСТИ  <br> К НЕПОПРАВИМЫМ ПОСЛЕДСТВИЯМ!</span>';
		let infotext = '<div class="block_plr_20 block_w_650p">Нажимая "Сброс", я подтверждаю, что нахожусь непосредственно рядом с котлом и уверен, что причины ошибки полностью устранены. Я уведомлен, что удаленный неконтролируемый перезапуск котла после ошибки может привести к порче имущества, потери здоровья или жизни.</div>';
		HWF.modal.confirm(title,infotext,'btn_accept_rebaut_error','Сброс ошибки');
	});
	HW.on('click','btn_accept_rebaut_error',function(e,el){
		HWF.push('Команда сброса ошибок отправлена');
		let dataP = {token:token,id_object:id,reset_error:1}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	});
	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/add_favorite_objects',data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/delete_favorite_objects',data:dataP});
		}
	});
	
	//Изменение времени показаний графика аналоговых
	HWS.buffer.timeControlFCNAnalog = function(H) {
		//clear_graph();
		//HWS.buffer.timeValueGraph = H;
		//HWS.buffer.load_analog_graph({time:H,typeS:3});
	}
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(info); // Проверит ваш ли это объект для вывода предупреждения
});