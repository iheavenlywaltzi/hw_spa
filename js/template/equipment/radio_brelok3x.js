"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let noConnectBlock = HWS.getElem('#no-connect-block');
	let info = HWS.buffer.elem;
	let et = HWD.equp[info.info.type.type] || {};
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = info.id;
	let normInfo = HWequip.normInfo(info);
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
	HWS.buffer.elem.id_system = info.id_system;

	let pTop = {}; 
	pTop.name = info.config.name || '';
	pTop.nameChange = 'of';
	pTop.title = et.title || 'none';
	pTop.icon = info.info.type.type || '';
	
	if(normInfo.port == 10 || normInfo.port == 12){
		pTop.ident = normInfo.ident || false;
		pTop.addr = (normInfo.portSymbol+normInfo.addr) || false;
		pTop.chanel = normInfo.channel_index || false;
	} else { pTop.port = normInfo.portSpec || false; }
	
	pTop.data = info.lk.state_name || 'Запрограммирован';  //  // state
	pTop.meterage = '';
	pTop.blambaColor = normInfo.stateColor;
	
	pTop.systemName = info.system_name;
	pTop.systemCN = info.system_ident;
	
	pTop = HWhtml.equp.top(pTop);
	contentTop.innerHTML = pTop;
	HWS.buffer.share_stick(info);
	B.menud.append(menu);

	HWS.buffer.equpPageSets = function(type){type = type || false;
		B.title.append('Настройки элемента оборудования');
		if(type == '1'){print_sets(info)}
		else{
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	HWS.buffer.equpPageSets(1);
	
	
	HWS.buffer.equpPageJurnal = function(){
		B.title.append('Журнал элемента оборудования');
		content.innerHTML = "";
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'jurnal';
		// автообновление
		HWS.buffer.specTimeControllerFlag = false;
		HWF.timerUpdate(reolad_info_page);
	}
	
	// Запрос системы и отрисовка HTML 
	/*HWF.post({url:HWD.api.gets.list,data:{list_start:0,list_amount:100,token:token,id_system:info.id_system,local_id:0},fcn:function(inf){
		HWS.buffer.SystemInfoDop = inf.list[0];
		pTop.systemName = HWS.buffer.SystemInfoDop.config.name;
		pTop.systemCN = HWS.buffer.SystemInfoDop.info.ident;
		
		pTop = HWhtml.equp.top(pTop);
		contentTop.innerHTML = pTop;
		HWS.buffer.share_stick(info);
		B.menud.append(menu);
		HWS.buffer.equpPageSets(1);
	}});*/
	
	HWS.buffer.equpPageAccesses = function(){
		noConnectBlock.classList.remove('on');
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
		HWS.buffer.equpAccessesFCN();
	}
	
	// Функции для работы
	function print_sets(info) {
		Sprint = '';
		let onof = normInfo.active; if(onof){onof = 'active';};
		let journal = normInfo.jurnal; if(journal){journal = 'active';}
		let device = info.config.id_device;
		
		let connect = info.system_connection_status || false;
		if(!connect && info.cloud_type != 14719) {noConnectBlock.classList.add('on')}
		else {noConnectBlock.classList.remove('on');}
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Журнал '+HWhtml.helper(HWD.helpText.equp.btnHelpRadioBrelocJurnal)+'</p>\
					<span id="equp-jurnal" class="equp_jurnal btn-active on-off '+journal+'" ></span>\
				</div>\
			</div>\
		</div>';
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Управление '+HWhtml.helper(HWD.helpText.equp.btnHelpRadioBrelocControl)+'</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<ses id="three-btn" data-fcn="brelockAdd" data-fcnd="brelockDelete" data-filter="relay" class="list" data-id="'+device+'">Управление третьей кнопкой</ses>\
				</div>\
			</div>\
		</div>';

		content.innerHTML = Sprint
		
		HWS.buffer.pageOpenControl = 'sets';
		// автообновление
		HWS.buffer.specTimeControllerFlag = false;
		HWF.timerUpdate(reolad_info_page);
		// запуск кнопок ПРограмм
		buttonsController.start();
	}
	
	 // переодическое обновление
	function reolad_info_page(idTimer){
		HWS.buffer.idTimerREoladPage = idTimer;
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(inf){
			if(HWS.buffer.specTimeControllerFlag){
				console.log('АВТООБНОВЛЕНИЕ НЕЗАПОЛНЕНО И НЕРАБОТАЕТ !');
			}
		}});
		//if(HWS.buffer.pageOpenControl == 'jurnal'){HWS.buffer.load_history('reload');}
	}
	
	// КНОПКИ
	
	//Изменение имени
	// Активен / не активен 
	HW.on('click','btn_active_inactive_equp',function(e,elem){
		C.actBtn = 1;
		if(elem.classList.contains('active')){ // On
			//let dataP = {token:HWS.pageAccess,id_object:id,enabled:1}
			//HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			//let dataP = {token:HWS.pageAccess,id_object:id,enabled:0}
			//HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	// Журнал
	HW.on('click','equp_jurnal',function(e,elem){
		C.jurnal = 1;
		if(elem.classList.contains('active')){ // On
			//let dataP = {token:HWS.pageAccess,id_object:id,journal:1}
			//HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			//let dataP = {token:HWS.pageAccess,id_object:id,journal:0}
			//HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	
	/*
	HW.on('focusin','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; nameFocus = 1;
	});
	HW.on('focusout','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; nameFocus = 1;
	});
	HW.on('input','equp_name',function(e,el){nameFocus = 1}); HW.on('click','equp_name',function(e,el){nameFocus = 1});
	
	
	HW.on('change','equp_name',function(e,el){
		let name = document.querySelector('.name-page input');
		let nameRegex = new RegExp(HWD.reg.name);
		
		if(HWS.buffer.elem.config.name != name.value){
			if(nameRegex.test(name.value)){
				let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
				HWF.post({url:HWD.api.gets.taskName3xDevice,data:dataP});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});*/
	
	// Добавели устройства
	HWS.buffer.brelockAdd = function(inf){
		let dataP = {token:token,trinket_id_object:id,device_id_object:inf.id}
		HWF.post({url:'task/set_id_device_to_trinket_3x_system',data:dataP});
	} // Удолилть устройство
	HWS.buffer.brelockDelete = function(inf){
		let dataP = {token:token,trinket_id_object:id,device_id_object:'delete'}
		HWF.post({url:'task/set_id_device_to_trinket_3x_system',data:dataP});
	}
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(info); // Проверит ваш ли это объект для вовода предупреждения
});