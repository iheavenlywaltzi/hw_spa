"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let info = HWS.buffer.elem;
	let et = HWD.equp[info.info.type.type] || {};
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = info.id;
	let normInfo = HWequip.normInfo(info);
	let C = {}; // якоря
	let nameFocus = false;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageGraph">График</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
	HWS.buffer.pageOpenControl = 'equpPageSets';
		
	HWS.buffer.equpPageSets = function(type){type = type || false;
		HWS.buffer.pageOpenControl = 'equpPageSets';
		B.title.append('Настройки элемента оборудования');
		if(type == '1'){print_sets(info)}
		else{
			C.globalSets = 1;
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	
	HWS.buffer.equpPageGraph = function(){
		HWS.buffer.pageOpenControl = 'equpPageGraph';
		B.title.append('График элемента оборудования');
		let graph = HWhtml.equp.graph(1,'timeControlFCNAnalog','timeControlLoadFCN');
		Sprint = graph;
		content.innerHTML = Sprint
		
		HWS.buffer.load_analog_graph({time:1,typeS:3});
	}
	
	HWS.buffer.equpPageJurnal = function(){
		HWS.buffer.pageOpenControl = 'equpPageJurnal';
		B.title.append('Журнал элемента оборудования');
		content.innerHTML = "";
		HWS.buffer.load_history();
	}
	
	HWS.buffer.equpPageAccesses = function(){
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
		B.title.append('Доступы элемента оборудования');
		Sprint = '<div class="one-block ob-top conections"> <h4>Доступы</h4>';
		Sprint+= '</div>';
		content.innerHTML = Sprint;
	}
	// инициализация страници // верхний блок ( имя и т.д )
	let pTop = {}; 
	pTop.name = info.config.name;
	pTop.title = et.title || 'none';
	pTop.icon = info.info.type.type || '';

	if(normInfo.port == 10 || normInfo.port == 12){
		pTop.chanel = normInfo.channel_index || false;
		pTop.ident = normInfo.ident || false;
		pTop.addr = (normInfo.portSymbol+normInfo.addr) || false;
	} else { pTop.port = normInfo.portSpec || false; }
	
	if(info.state.state.val == 0 || info.state.state.val > 10){
		pTop.data = HWD.globalText.no_connect;
		pTop.meterage = '';
		pTop.stateName = HWD.globalText.no_connect;
		pTop.stateClass = 'stateC_1';
	}else{
		pTop.data = info.state.value; pTop.data = pTop.data.val || 0; pTop.data = pTop.data.toFixed(1);
		pTop.meterage = HWD.unit[info.info.type.unit] || '';
		pTop.stateName = info.state.state.name || '';
		pTop.stateClass = 'stateC3_'+info.info.type.branch+'_'+info.state.state.val || '';
		pTop.power = info.state.power || 0; pTop.power = pTop.power.quality || 0;
		pTop.connect = '';	
	}
	pTop.systemName = info.system_name;
	pTop.systemCN = info.system_ident;

	// Функции для работы
	function reolad_info_page(idTimer){// переодическое обновление
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(inf){
			let blockData = document.body.querySelector('.page-top-dop-info .data inf');
			let blockDataS = document.body.querySelector('.page-top-dop-info .data span');
			let blockBlam = document.body.querySelector('#blamba');
			let nameEqup = document.body.querySelector('#equp-content-top .name-page .equp_name');
			let name = '', clasElem='false',unit='';

			if(inf.state.state.val == 0 || inf.state.state.val > 10){
				unit = '';
				name = HWD.globalText.no_connect;
				clasElem = 'bstateC_1';
			}else{
				unit = HWD.unit[inf.info.type.unit] || '';
				name = inf.state.value.val.toFixed(1);
				clasElem = 'bstateC3_'+inf.info.type.branch+'_'+inf.state.state.val || '';
			}

			if(!blockBlam.classList.contains(clasElem)){
				blockBlam.removeAttribute('class');
				blockBlam.classList.add(clasElem);
			}
			blockData.innerHTML = name;
			blockDataS.innerHTML = unit;
			if(nameEqup.value != inf.config.name && !nameFocus) {nameEqup.value = inf.config.name;}

			HWS.buffer.system_time = inf.system_time;
			if(HWS.buffer.pageOpenControl == 'equpPageSets' && !C.globalSets){
				let pravka = document.body.querySelector('#btn-pravka .inp_num_val');
				let act = document.body.querySelector('#equp-onof');
				let jurnal = document.body.querySelector('#equp-jurnal');
				let favorite = document.body.querySelector('#equp-viget');
				let delayNorm = document.body.querySelector('#btn-delay-norm .inp_num_val');
				let delayAler = document.body.querySelector('#btn-delay-alar .inp_num_val');
				let porog = HWS.getElem('#limit-block');
					let minP = porog.querySelector('.btn-controllers .alert_min_predel .btn-int .inp_main_val');
					let maxP = porog.querySelector('.btn-controllers .alert_max_predel .btn-int .inp_main_val');
						
				if(pravka.value != inf.config.correction && !C.pravka) {pravka.value = inf.config.correction;}
				if(!C.actBtn) {(inf.config.flags.enabled)?act.classList.add('active'):act.classList.remove('active');}
				if(!C.jurnal) {(inf.config.flags.journal)?jurnal.classList.add('active'):jurnal.classList.remove('active');}
				if(!C.favorite) {(inf.favorite)?favorite.classList.add('active'):favorite.classList.remove('active');}
				if(delayNorm.value != inf.config.pm_delay_norm && !C.delayNorm) {delayNorm.value = inf.config.pm_delay_norm;}
				if(delayAler.value != inf.config.pm_delay_alarm && !C.delayAler) {delayAler.value = inf.config.pm_delay_alarm;}
				if(!C.porog) {
					let min = parseInt(porog.dataset.min);
					let max = parseInt(porog.dataset.max);
					let kof = parseFloat((100/(max-min)).toFixed(4));
					if (minP && inf.config.pm_min != minP.value){
						let numMin = inf.config.pm_min.toFixed(1);
						let lineMin = porog.querySelector('.scale-temp .line_alt_min');
						
						lineMin.querySelector('.btn_line_move.btn-line.top.left').innerHTML = numMin;
						lineMin.style.width = (((numMin-min)*kof).toFixed(4))+'%';
						minP.value = numMin;
					}
					if (maxP && inf.config.pm_max != maxP.value){
						let numMax = inf.config.pm_max.toFixed(1);
						let lineMax = porog.querySelector('.scale-temp .line_alt_max ');
						
						lineMax.querySelector('.btn_line_move.btn-line.top.right').innerHTML = numMax;
						lineMax.style.width = (((max-numMax)*kof).toFixed(4))+'%';
						maxP.value = numMax;
					}
				}
			}
			if(HWS.buffer.pageOpenControl == 'equpPageGraph'){HWS.buffer.load_analog_graph({time:HWS.buffer.timeValueGraph,typeS:3,tLoad:'reload'});}
			if(HWS.buffer.pageOpenControl == 'equpPageJurnal'){}
			if(HWS.buffer.pageOpenControl == 'equpPageAccesses'){}
			
			inf.state.value = inf.state.value || {};
			info = inf;
			C = {}
		}});
	}
	// Распечатка настроек
	function print_sets(info){
		let pDat = {};
		let min = et.min;
		let max = et.max;
		pDat.onof = info.config.flags.enabled;
		pDat.journal = info.config.flags.journal;
		if(HWF.objKeyTest('correction',info.config)){pDat.correction = info.config.correction}
		if(HWF.objKeyTest('pm_delay_alarm',info.config)){pDat.delayA = info.config.pm_delay_alarm}
		if(HWF.objKeyTest('pm_delay_norm',info.config)){pDat.delayN = info.config.pm_delay_norm}
		let onof = pDat.onof; if(onof){onof = 'active';};
		let journal = pDat.journal; if(journal){journal = 'active';}
		let favorite = (info.favorite)?'active':'';
		let pravka = HWD.unit[info.info.type.unit] || '°С';
		
		// блок Глобальные
		if(pDat.correction){pDat.correction = pDat.correction || '0'; pDat.correction = pDat.correction.toFixed(1)}
		let pSets = '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Активен '+HWhtml.helper(HWD.helpText.equp.btnHelpActive)+'</p>\
					<span id="equp-onof" class="btn-active on-off btn_active_inactive_equp '+onof+'" ></span>\
				</div>\
				<div class="colum">\
					<p>Журнал '+HWhtml.helper(HWD.helpText.equp.btnHelpJurnal)+'</p>\
					<span id="equp-jurnal" class="equp_jurnal btn-active on-off '+journal+'" ></span>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.equp.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+favorite+'" ></span>\
				</div>\
				<div class="colum">\
					<p>Поправка, '+pravka+'</p>\
					<div>'+HWhtml.btn.numb(pDat.correction,{fcn:'clickPravkaSensor',id:'btn-pravka',step:0.1,tofix:1,min:min,max:max})+'</div>\
				</div>\
			</div>\
		</div>';
		
		// блок Оповещения
		if(pDat.delayN){pDat.delayN = pDat.delayN || '0';} 
		if(pDat.delayA){pDat.delayA = pDat.delayA || '0';}
		let pMesages = '<div class="one-block-col">\
			<div class="title"><h4>Оповещения</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Задержка для <br> нормы, сек '+HWhtml.helper(HWD.helpText.equp.btnHelpDelayNorm)+'</p>\
					<div>'+HWhtml.btn.numb(pDat.delayN,{fcn:'clickDelayNormSensor',id:'btn-delay-norm',min:0,max:9999})+'</div>\
				</div>\
				<div class="colum">\
					<p>Задержка для <br> тревоги, сек '+HWhtml.helper(HWD.helpText.equp.btnHelpDelayAlert)+'</p>\
					<div>'+HWhtml.btn.numb(pDat.delayA,{fcn:'clickDelayAlarmSensor',id:'btn-delay-alar',min:0,max:9999})+'</div>\
				</div>\
			</div>\
		</div>';

		// Пороги
		let porog = {}
		let setsLim = {};
			setsLim.warMin = {act:'of'};
			setsLim.warMax = {act:'of'};
			setsLim.aleMin = {act:'onlyVal',val:(info.config.pm_min || 0)};
			if(HWF.objKeyTest('pm_max',info.config)){setsLim.aleMax = {act:'onlyVal',val:info.config.pm_max};}
			else{setsLim.aleMax = {act:'of'};}
				
		porog.class = 'one-block-col';
		porog.fcn = 'cangePorogSensor';
		porog.min = min;
		porog.max = max;
		porog.warMin = setsLim.warMin;
		porog.warMax = setsLim.warMax;
		porog.aleMin = setsLim.aleMin;
		porog.aleMax = setsLim.aleMax;
		porog = HWhtml.limit(porog);
		
		Sprint = pSets+pMesages+porog;
		content.innerHTML = Sprint
		
		HWequip.limit.start('.predel-scale'); // запуск работы приделов
	}
	
	//отрисовка 
	pTop = HWhtml.equp.top(pTop);
	contentTop.innerHTML = pTop
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	HWF.timerUpdate(reolad_info_page);
	
	// Активен / не активен 
	HW.on('click','btn_active_inactive_equp',function(e,elem){
		C.actBtn = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,enabled:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,enabled:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	// Журнал
	HW.on('click','equp_jurnal',function(e,elem){
		C.jurnal = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,journal:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,journal:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,favorite:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,favorite:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	//Изменение порогов
	HWS.buffer.cangePorogSensor = function(inf){
		C.porog = 1;
		let numb = inf.val;
		if(inf.type == 'alert_max_predel'){
			clearTimeout(HWS.buffer.clickPravkaSensorTimer);
			HWS.buffer.clickPravkaSensorTimer = setTimeout(function(){
				let dataP = {token:HWS.pageAccess,id_object:id,pm_max:numb}
				HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
			},2000);
		}			
		if(inf.type == 'alert_min_predel'){
			clearTimeout(HWS.buffer.clickPravkaSensorTimer);
			HWS.buffer.clickPravkaSensorTimer = setTimeout(function(){
				let dataP = {token:HWS.pageAccess,id_object:id,pm_min:numb}
				HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP,fcnE:HWF.getServMesErr});
			},2000);
		}
	};
	//Задержка при Норме
	HWS.buffer.clickDelayNormSensor = function(numb){
		C.delayNorm = 1;
		clearTimeout(HWS.buffer.clickDelayNormSensorTime);
		HWS.buffer.clickDelayNormSensorTime = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pm_delay_norm:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP,fcnE:HWF.getServMesErr});
		},2000);
	}
	//Задержка при тревоге
	HWS.buffer.clickDelayAlarmSensor = function(numb){
		C.delayAler = 1;
		clearTimeout(HWS.buffer.clickDelayAlarmSensorTimer);
		HWS.buffer.clickDelayAlarmSensorTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pm_delay_alarm:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP,fcnE:HWF.getServMesErr});
		},2000);
	}
	//Изменение (правки) коррекции taskParametersTo3xSensor
	HWS.buffer.clickPravkaSensor = function(numb){
		C.pravka = 1;
		clearTimeout(HWS.buffer.clickPravkaSensorTimer);
		HWS.buffer.clickPravkaSensorTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,correction:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
		},2000);
	}
	//Изменение имени
	HW.on('focusin','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; nameFocus = 1;
	});
	HW.on('focusout','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; nameFocus = 1;
	});
	HW.on('input','equp_name',function(e,el){nameFocus = 1}); HW.on('click','equp_name',function(e,el){nameFocus = 1});
	
	HW.on('change','equp_name',function(e,el){
		nameFocus = false;
		let name = el;
		let nameRegex = new RegExp(HWD.reg.name);
		
		if(HWS.buffer.elem.config.name != name.value){
			if(nameRegex.test(name.value)){
				let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
				HWF.post({url:HWD.api.gets.taskName3xDevice,data:dataP,fcn:function(nInf){console.log(nInf)}});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
});