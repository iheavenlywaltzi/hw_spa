"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let info = HWS.buffer.elem;
	let et = HWD.equp[info.info.type.type] || {};
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = info.id;
	let print = {}, S={}; 
	let normInfo = HWequip.normInfo(info);
	let C = {};
	let nameFocus = false;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageGraph">График</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
	HWS.buffer.pageOpenControl = 'equpPageSets';
		
	HWS.buffer.equpPageSets = function(type){type = type || false;
		HWS.buffer.pageOpenControl = 'equpPageSets';
		if(type == '1'){print_sets(info)}
		else{
			C.globalSets = 1;
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	
	HWS.buffer.equpPageGraph = function(){
		print.graph = HWhtml.equp.graph(1,'timeControlFCNDiscret','timeControlLoadFCN');
		content.innerHTML = print.graph;
		
		HWS.buffer.pageOpenControl = 'equpPageGraph';
		HWS.buffer.load_discret_graph(1,3); // запуск и отрисовка графика Статистики показаний
	}
	
	HWS.buffer.equpPageJurnal = function(){
		content.innerHTML = "";
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'equpPageJurnal';
	}	
	
	HWS.buffer.equpPageAccesses = function(){
		Sprint = '<div class="one-block ob-top conections"> <h4>Доступы</h4>';
		Sprint+= '</div>';
		content.innerHTML = Sprint;
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
	}		

	// Функции для работы
	function reolad_info_page(idTimer){
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcnE:HWF.getServMesErr,fcn:function(inf){
			let blockData = document.body.querySelector('.page-top-dop-info .data inf');
			let blockBlam = document.body.querySelector('#blamba');
			let clasElem = '' ;
			let name = '';
			let normInfo = HWequip.normInfo(inf);

			if(inf.state.state.val == 0 || inf.state.state.val == 255){
				name = HWD.globalText.no_connect;
				clasElem = 'bstateC_1';
			}
			else {
				name = normInfo.stateName
				clasElem = 'bstateC3_'+inf.info.type.branch+'_'+inf.state.state.val || '';
			}
			
			if(!blockBlam.classList.contains(clasElem)){
				blockBlam.removeAttribute('class');
				blockBlam.classList.add(clasElem);
			}
			blockData.innerHTML = name;
			HWS.buffer.system_time = inf.system_time;
			
			if(HWS.buffer.pageOpenControl == 'equpPageSets' && !C.globalSets){
				let nameEqup = document.body.querySelector('#equp-content-top .name-page .equp_name');
				let act = document.body.querySelector('#equp-onof');
				let jurnal = document.body.querySelector('#equp-jurnal');
				let delayNorm = document.body.querySelector('#btn-delay-norm .inp_num_val');
				let delayAler = document.body.querySelector('#btn-delay-alar .inp_num_val');
				let StatNorma = HWS.getElem('#state-name-norm');
				let StatAlert = HWS.getElem('#state-name-alert');
				let SnameNorm = et.normT || 'Норма';
				let SnameAlert = et.alerT || 'Тревога';
				let StatNormaT = (inf.config.state_name)?inf.config.state_name[1]:SnameNorm;
				let StatAlertT = (inf.config.state_name)?inf.config.state_name[0]:SnameAlert;
				let favorite = document.body.querySelector('#equp-viget');

				if(!C.favorite) {(inf.favorite)?favorite.classList.add('active'):favorite.classList.remove('active');}
				if(nameEqup.value != inf.config.name && !nameFocus) {nameEqup.value = inf.config.name;}
				if(!C.actBtn) {(inf.config.flags.enabled)?act.classList.add('active'):act.classList.remove('active');}
				if(!C.jurnal) {(inf.config.flags.journal)?jurnal.classList.add('active'):jurnal.classList.remove('active');}
				if(delayNorm.value != inf.config.pm_delay_norm && !C.delayNorm) {delayNorm.value = inf.config.pm_delay_norm;}
				if(delayAler.value != inf.config.pm_delay_alarm && !C.delayAler) {delayAler.value = inf.config.pm_delay_alarm;}
				if(StatNorma.value != StatNormaT && !C.StatNorma) {StatNorma.value = StatNormaT;}
				if(StatAlert.value != StatAlertT && !C.StatAlert) {StatAlert.value = StatAlertT;}
			}
			if(HWS.buffer.pageOpenControl == 'equpPageGraph'){HWS.buffer.load_discret_graph(HWS.buffer.timeValueGraph,3,'reload');}
			info = inf;
			C = {};
		}});
	}
	
		// Распечатка настроек
	function print_sets(info){
		let pDat = {};
		let onof = info.config.flags.enabled; if(onof){onof = 'active';};
		let journal = info.config.flags.journal; if(journal){journal = 'active';}
		if(HWF.objKeyTest('pm_delay_alarm',info.config)){pDat.delayA = info.config.pm_delay_alarm || 0}
		if(HWF.objKeyTest('pm_delay_norm',info.config)){pDat.delayN = info.config.pm_delay_norm || 0}
		if(pDat.delayN){pDat.delayN = pDat.delayN || '0';} else {pDat.delayN = 0}
		if(pDat.delayA){pDat.delayA = pDat.delayA || '0';} else {pDat.delayA = 0}
		S.nameNorm = et.normT || 'Норма';
		S.nameAlert = et.alerT || 'Тревога';
		let StatNorma = (info.config.state_name)?info.config.state_name[1]:S.nameNorm;
		let StatAlert = (info.config.state_name)?info.config.state_name[0]:S.nameAlert;
		let favorite = (info.favorite)?'active':'';
		
		let global = '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Активен '+HWhtml.helper(HWD.helpText.equp.btnHelpActive)+'</p>\
					<span id="equp-onof" class="btn-active on-off btn_active_inactive_equp '+onof+'" ></span>\
				</div>\
				<div class="colum">\
					<p>Журнал '+HWhtml.helper(HWD.helpText.equp.btnHelpJurnal)+'</p>\
					<span id="equp-jurnal" class="equp_jurnal btn-active on-off '+journal+'" ></span>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.equp.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+favorite+'" ></span>\
				</div>\
			</div>\
		</div>';
		
		let mesage = '<div class="one-block-col">\
			<div class="title"><h4>Оповещения</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Задержка <br> для нормы, сек '+HWhtml.helper(HWD.helpText.equp.btnHelpDelayNorm)+'</p>\
					<div>'+HWhtml.btn.numb(pDat.delayN,{fcn:'clickDelayNormSensor',id:'btn-delay-norm',min:'null',max:9999})+'</div>\
				</div>\
				<div class="colum">\
					<p>Задержка <br> для тревоги, сек '+HWhtml.helper(HWD.helpText.equp.btnHelpDelayAlert)+'</p>\
					<div>'+HWhtml.btn.numb(pDat.delayA,{fcn:'clickDelayAlarmSensor',id:'btn-delay-alar',min:'null',max:9999})+'</div>\
				</div>\
			</div>\
		</div>';
		
		let state = '<div class="one-block-col">\
			<div class="title"><h4>Состояния</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="mcolum">\
					<p class="block_ptb_8">'+S.nameNorm+' '+HWhtml.helper(HWD.helpText.equp.btnHelpNameNorm)+'</p>\
					<br>\
					<p class="block_ptb_8">'+S.nameAlert+' '+HWhtml.helper(HWD.helpText.equp.btnHelpNameAlert)+'</p>\
				</div>\
				<div class="columx2">\
					<p class=""><input id="state-name-norm" class="btn_state_text_norm" type="text" value="'+StatNorma+'" placeholder="Своё"></p>\
					<br>\
					<p class=""><input id="state-name-alert" class="btn_state_text_aler" type="text" value="'+StatAlert+'" placeholder="Своё"></p>\
				</div>\
			</div>\
		</div>';
		
		content.innerHTML = global+mesage+state;
	}
	
	// верхний блок ( имя и т.д )
	print.top = {}; 
	print.top.name = info.config.name;
	print.top.title = et.title || 'none';
	print.top.icon = info.info.type.type || '';
	
	if(normInfo.port == 10 || normInfo.port == 12){
		print.top.ident = normInfo.ident || false;
		print.top.addr = (normInfo.portSymbol+normInfo.addr) || false;
		print.top.chanel = normInfo.channel_index || false;
	} else { print.top.port = normInfo.portSpec || false; }
	
	if(info.state.state.val == 0 || info.state.state.val == 255){
		print.top.data = HWD.globalText.no_connect;
		print.top.meterage = '';
		print.top.stateName = HWD.globalText.no_connect;
		print.top.stateClass = 'stateC_1';
	}else{
		print.top.data = normInfo.stateName;
		print.top.meterage = '';
		print.top.stateName = info.state.state.name || '';
		print.top.stateClass = 'stateC3_'+info.info.type.branch+'_'+info.state.state.val || '';
		print.top.power = info.state.power || 0; print.top.power = print.top.power.quality || 0;
	}
	
	// отрисовка страници
	print.top.systemName = info.system_name;
	print.top.systemCN = info.system_ident;
	print.top = HWhtml.equp.top(print.top);
	
	contentTop.innerHTML = print.top
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	// авто обновление
	HWF.timerUpdate(reolad_info_page);
	
	// отработка нажатий и кликов
	//Изменение имени
	HW.on('focusin','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; nameFocus = 1;
	});
	HW.on('focusout','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; nameFocus = 1;
	});
	HW.on('input','equp_name',function(e,el){nameFocus = 1}); HW.on('click','equp_name',function(e,el){nameFocus = 1});
	
	HW.on('change','equp_name',function(e,el){
		nameFocus = false;
		let name = document.querySelector('.name-page input');
		let nameRegex = new RegExp(HWD.reg.name);
		
		if(HWS.buffer.elem.config.name != name.value){
			if(nameRegex.test(name.value)){
				let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
				HWF.post({url:HWD.api.gets.taskName3xDevice,data:dataP});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
	// Активен / не активен 
	HW.on('click','btn_active_inactive_equp',function(e,elem){
		C.actBtn = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,enabled:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,enabled:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	// Журнал
	HW.on('click','equp_jurnal',function(e,elem){
		C.jurnal = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,journal:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,journal:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,favorite:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,favorite:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	//Задержка при Норме
	HWS.buffer.clickDelayNormSensor = function(numb){
		C.delayNorm = 1;
		clearTimeout(HWS.buffer.clickDelayNormSensorTime);
		HWS.buffer.clickDelayNormSensorTime = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pm_delay_norm:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
		},2000);
	}
	//Задержка при тревоге
	HWS.buffer.clickDelayAlarmSensor = function(numb){
		C.delayAler = 1;
		clearTimeout(HWS.buffer.clickDelayAlarmSensorTimer);
		HWS.buffer.clickDelayAlarmSensorTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pm_delay_alarm:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
		},2000);
	}
	//Изменение Имени "есть\норм"
	HW.on('change','btn_state_text_norm',function(e,el){
		C.StatNorma = 1;
		let dopElem = document.body.querySelector('.btn_state_text_aler');
		let name = []
			name.push(dopElem.value);
			name.push(el.value);
		let dataP = {token:HWS.pageAccess,id_object:id,state_name:name}
		HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
	});
	//Изменение Имени "нет\тревога"
	HW.on('change','btn_state_text_aler',function(e,el){
		C.StatAlert = 1;
		let dopElem = document.body.querySelector('.btn_state_text_norm');
		let name = []
			name.push(el.value);
			name.push(dopElem.value);
		let dataP = {token:HWS.pageAccess,id_object:id,state_name:name}
		HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
	});
});
