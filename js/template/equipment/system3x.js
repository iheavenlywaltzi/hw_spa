"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top'); contentTop.classList.add('system3x');
	let content = document.body.querySelector('#equp-content-mid');
	let noConnectBlock = HWS.getElem('#no-connect-block');
	let elem = HWS.buffer.elem;
	let et = HWD.equp[elem.info.type.type] || {};
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = elem.id;
	let versP = et.title;
	let C = {};
	B.title.append('<h1>Система '+versP+'</h1>');
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
	menu+= '<span class="btn btn_dash_click" data-fcn="equpPageGraph">График</span>';
	menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
	menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';	
	
	HWS.buffer.equpPageSets = function(type){type = type || false;
		clear_graph();
		if(type == '1'){print_sets(elem)}
		else{
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo);}});
		}
		HWS.buffer.pageOpenControl = 'sets';
	}

	HWS.buffer.equpPageGraph = function(){
		noConnectBlock.classList.remove('on');
		clear_graph();
		content.innerHTML = HWhtml.equp.system3(elem,'graph');
		let timeGraph = HWS.buffer.timeValueGraph || 1;
		HWS.buffer.systemTimeDayGsm = timeGraph;
		HWS.buffer.systemTimeDayWifi = timeGraph;
		// GSM
		if(HWS.buffer.modulSystem[4]){
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:HWS.buffer.modulSystem[4].id},fcn:function(inf){
				let day = HWS.buffer.systemTimeDayGsm || 1;
				load_graph_conect_system(day,inf.id,'#block-graf-analog-gsm .graph','gsm');
			}});
		}
		// Wi-Fi
		if(HWS.buffer.modulSystem[5]){
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:HWS.buffer.modulSystem[5].id},fcn:function(inf){
				let day = HWS.buffer.systemTimeDayWifi || 1;
				load_graph_conect_system(day,inf.id,'#block-graf-analog-wifi .graph','wifi');
			}});
		}
		
		HWS.buffer.pageOpenControl = 'graph';
		// автообновление
		HWS.buffer.specTimeControllerFlag = false;
		HWF.timerUpdate(reolad_info_page);
	}
	
	HWS.buffer.equpPageJurnal = function(){
		noConnectBlock.classList.remove('on');
		clear_graph();
		content.innerHTML = "";
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'jurnal';
		// автообновление
		HWS.buffer.specTimeControllerFlag = false;
		HWF.timerUpdate(reolad_info_page);
	}	
	
	HWS.buffer.equpPageAccesses = function(){
		noConnectBlock.classList.remove('on');
		clear_graph();
		HWS.buffer.pageOpenControl = 'acce';
		HWS.buffer.equpAccessesFCN();
	}	
	
	// Распчатка HTML
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	
	// распечатка страници настроек
	function print_sets(elem){
		let connect = elem.system_connection_status || false;
		if(!connect && elem.cloud_type != 14719) {noConnectBlock.classList.add('on')}
		else {noConnectBlock.classList.remove('on');}
		
		HWF.post({url:HWD.api.gets.list,data:{
				token:token,
				list_start:0,
				list_amount:1000,
				id_system:elem.id_system,
				local_id_object_type:0,
				local_id_object_group:[4,5],
			},
			fcn:system_mod
		});
	}
	
	function system_mod(inf){		
		let group = {};
		HWS.buffer.modulSystem = {};
		HWF.recount(inf.list,function(mod){group[mod.group] = mod;});
		elem.mod_gsm = {}; if(group[4]){elem.mod_gsm = group[4]; HWS.buffer.modulSystem[4] = group[4]} // GSM 
		elem.mod_wifi= {}; if(group[5]){elem.mod_wifi = group[5]; HWS.buffer.modulSystem[5] = group[5]} // Wi-Fi  
		content.innerHTML = HWhtml.equp.system3(elem,'sets');
		
		// автообновление
		HWS.buffer.specTimeControllerFlag = false;
		HWF.timerUpdate(reolad_info_page);
		// Распчатка HTML
		contentTop.innerHTML = HWhtml.equp.system3(elem,'top');
		HWS.buffer.share_stick(elem);
	}

	// Функции для работы
	function reolad_info_page(idTimer){
		if(HWS.buffer.specTimeControllerFlag){
			HWS.buffer.idTimerREoladPage = idTimer;
			// система
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){
				let OI = {};
				let elem = newInfo;
				let BlambaB = HWS.getElem('#blamba');
				let ConectB = HWS.getElem('#top-info-block .data inf');
				let systemConect = elem.lk.state_name;
				HWS.buffer.elem = elem;
				
				BlambaB.style.background = HWF.getStateColor(elem.lk.state);
				ConectB.innerHTML = systemConect;

				if(HWS.buffer.pageOpenControl == 'sets'){//Авто обновление для настроек
					HWF.post({url:HWD.api.gets.list,data:{token:token,list_start:0,list_amount:1000,id_system:elem.id_system,local_id_object_type:0,local_id_object_group:[4,5]},fcn:function(newInfo){ if(HWS.buffer.pageOpenControl == 'sets'){
						let di = {};
						let lastSysTimeTextBlock = HWS.getElem('#top-info-time-sis-text');
						let lastSysTimeBlock = HWS.getElem('#top-info-time-sis');
						let group = {}, wifi='', gsm='';
						
						di.lastSysDateT = HWF.unixTime(elem.system_connection_time,true);
						di.lastSysDateText = elem.system_connection_delta || '';
						di.lastSysDateTime = di.lastSysDateT.hh+':'+di.lastSysDateT.mm+':'+di.lastSysDateT.ss;
						di.lastSysDate = di.lastSysDateT.fullDate;
						di.systemConnection = elem.system_connection || false;
						
						HWF.recount(newInfo.list,function(mod){group[mod.group] = mod;});

						lastSysTimeTextBlock.innerHTML = di.lastSysDateText;
						lastSysTimeBlock.innerHTML = di.lastSysDate+' в '+di.lastSysDateTime;
						
						if(group[4]){  // GSM 
							gsm = group[4]; HWS.buffer.modulSystem[4] = group[4]
							let gsmGsmB = HWS.getElem('#info-connections-wifi-gsm');
							let gsmB = gsmGsmB.querySelector('.gsm');
							let gsmBicon = gsmB.querySelector('.icon');
							let gsmBiconN = gsm.state.signal_level || '0';
							let gsmBname = gsmB.querySelector('.name');
							let gsmBnameN = gsm.info.info_operator || 'Отсутствует';
							let gsmLastSms = gsm.info.info_bal;
							let roam = (gsm.state.roaming_status)?'-roam':'';
							
							OI.btnMicrophone = HWS.getElem('.btn_microphone_system_onof');
							if(!C.btnMicrophone) {(gsm.config.cfg_mphone)?OI.btnMicrophone.classList.add('active'):OI.btnMicrophone.classList.remove('active');}
							
							for (let i=0;i<=5;i++){gsmBicon.classList.remove('icon-gsm-'+i); gsmBicon.classList.remove('icon-gsm-roam-'+i);}
							gsmBicon.classList.add('icon-gsm'+roam+'-'+gsmBiconN);
							gsmBname.innerHTML = gsmBnameN;
							HWS.getElem('#gsm-last-sms-text').innerHTML = gsmLastSms || '';
						}
						
						if(group[5]){// Wi-Fi  
							wifi = group[5]; HWS.buffer.modulSystem[5] = group[5]
							let wifiGsmB = HWS.getElem('#info-connections-wifi-gsm');
							let wifiB = wifiGsmB.querySelector('.wifi');
							let wifiBicon = wifiB.querySelector('.icon');
							let wifiBiconN = wifi.state.signal_level || '0';
							let wifiBname = wifiB.querySelector('.name');
							let wifiBnameN = wifi.config.cfg_wifi_ssid || 'Отсутствует';
							let wifiConnectStatBlock = document.body.querySelector('#system-wifi-control .wifi-status-sistem');

							if(di.systemConnection){
								HWF.recount(di.systemConnection,function(eleC){
								if(eleC.type == 'wifi'){wifiConnectStatBlock.innerHTML = eleC.use_inet_str || '';}
								});
							}
							
							wifiBicon.classList.remove('icon-wifi-0'); wifiBicon.classList.remove('icon-wifi-1');
							wifiBicon.classList.remove('icon-wifi-2'); wifiBicon.classList.remove('icon-wifi-3');
							wifiBicon.classList.remove('icon-wifi-4'); wifiBicon.classList.add('icon-wifi-'+wifiBiconN);
							wifiBname.innerHTML = wifiBnameN;
						} 
						
						let ControlChange = HWS.getElem('.canche_data_system_btn');
						
						OI.btnRadio = HWS.getElem('.btn_radio_system_onof');
						OI.btnDop = HWS.getElem('.btn_portdop_onof');
						OI.btnAlertBtn = HWS.getElem('.btn_warninr_but_onof');
						OI.btnVijet = HWS.getElem('#equp-viget');
						OI.timeData = HWS.getElem('.time_val_data');
						OI.timeDataHhMm = HWS.getElem('.inp_time_text_hh_mm');
						OI.systemTextData = HWS.getElem('#system-text-data');
						OI.systemTextPoRadio = HWS.getElem('#system-text-po-radio');
						
						OI.systemTextData.innerHTML = elem.info.info_vermain || '';
						OI.systemTextPoRadio.innerHTML = elem.info.info_verrf || '';

						if(!C.favorite) {(elem.lk.favorite)?OI.btnVijet.classList.add('active'):OI.btnVijet.classList.remove('active');}
						if(!C.btnAlertBtn) {(elem.config.cfg_button)?OI.btnAlertBtn.classList.add('active'):OI.btnAlertBtn.classList.remove('active');}
						if(!C.btnDop) {(elem.config.cfg_ext)?OI.btnDop.classList.add('active'):OI.btnDop.classList.remove('active');}
						if(!C.btnRadio) {(elem.config.cfg_radio)?OI.btnRadio.classList.add('active'):OI.btnRadio.classList.remove('active');}
						
						if(OI.timeData.disabled){
							OI.sysDateF = HWF.unixTime(elem.info.info_time*1000);
							OI.sysDate = OI.sysDateF.dd+'.'+OI.sysDateF.mt+'.'+OI.sysDateF.yyyy;
							OI.timeData.value = OI.sysDate;
						}
						if(OI.timeDataHhMm.disabled){
							OI.sysTimeH = OI.sysDateF.hh;
							OI.sysTimeM = OI.sysDateF.mm;
							OI.timeDataHhMm.value = OI.sysTimeH+":"+OI.sysTimeM;
						}
					}}});
				}
				if(HWS.buffer.pageOpenControl == 'graph'){
					let timeGraph = HWS.buffer.timeValueGraph || 1;
					HWS.buffer.systemTimeDayGsm = timeGraph;
					HWS.buffer.systemTimeDayWifi = timeGraph;
					// GSM
					HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:HWS.buffer.modulSystem[4].id},fcn:function(inf){
					if(HWS.buffer.pageOpenControl == 'graph'){	
						let day = HWS.buffer.systemTimeDayGsm || 1;
						load_graph_conect_system(day,inf.id,'#block-graf-analog-gsm .graph','gsm');
					}
					}});
					// Wi-Fi
					HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:HWS.buffer.modulSystem[5].id},fcn:function(inf){
					if(HWS.buffer.pageOpenControl == 'graph'){					
						let day = HWS.buffer.systemTimeDayWifi || 1;
						load_graph_conect_system(day,inf.id,'#block-graf-analog-wifi .graph','wifi');
					}
					}});
				}
				if(HWS.buffer.pageOpenControl == 'jurnal'){}
				if(HWS.buffer.pageOpenControl == 'acce'){}
			}});
		}
		C = {};
	}
	
	function load_graph_conect_system(time,elId,target,tpy){
		time = time || 1; elId = elId || id;
		target = target || '#equp-statistic-graf .graph-windiw-statistic';
		let gTime = HWS.buffer.elem.system_time || Date.now(); 
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		let block = document.body.querySelector(target);
		let LG = {};
			LG.lc = 0;
		if(!HWS.buffer.graphConectSystem){HWS.buffer.graphConectSystem = {}}
		if(!HWS.buffer.graphConectSystem[tpy] && block){block.innerHTML = HWhtml.loader();}

		HWF.post({url:"history/chart_value_data",data:{
			token:token,
			object_id:elId,
			timestamp_start:sTime,
			timestamp_end:eTime,
			},
			fcn:loader_graph,
		});
		
		function loader_graph(inf){
			//console.log(inf);
			let idOb = inf.id_object;
			let list = inf.list || [];
			let data = [];
			let dataA = [];
			
			if(list.length > 0){
				HWF.recount(list,function(t){
					dataA.push({x:t.timestamp,y:t.value})
				});
						
				if(HWS.buffer.graphConectSystem[tpy]){
					let chart = HWS.buffer.graphConectSystem[tpy];
					let lastList = chart.data;
					let newList = dataA;
					let startPoitn = 0;
					if(newList &&  newList[0]) {startPoitn = newList[0].x || 0;}
					let endPoitn = 0; ;
					if(lastList &&  lastList[lastList.length-1]) {endPoitn = lastList[lastList.length-1].x || 0;}
					let iter = 0,shiftDel = 0,shiftAdd = 0;
					
					HWF.recount(lastList,function(pointInfo){ if(pointInfo.x < startPoitn){shiftDel++;}});
					HWF.recount(newList,function(pointInfo){ if(pointInfo.x > endPoitn){shiftAdd++} });
					
					if (shiftDel) { for (let i = 0; i < shiftDel; i++) {chart.addData([],1);} }
					if (shiftAdd) { let newAdd = newList.slice(-shiftAdd);chart.addData(newAdd); }
				} else {
					// Create chart instance
					am4core.useTheme(am4themes_animated);
					let chart = am4core.create(block, am4charts.XYChart);
						chart.cursor = new am4charts.XYCursor();	
						chart.data  = dataA;
						chart.dateFormatter.utc = true;
						
					let xAxes = chart.xAxes.push(new am4charts.DateAxis());

					// Create value axis
					let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
						yAxis.min  = 0;
						yAxis.max  = 100;
						yAxis.renderer.labels.template.adapter.add("text", function(text) {
							return parseFloat(text).toFixed(0)+'%';
						})

					// Create series
					let series = chart.series.push(new am4charts.LineSeries());
					series.dataFields.valueY = "y";
					series.dataFields.dateX = "x";
					series.stroke = am4core.color(HWD.color.green);
					series.fill = am4core.color(HWD.color.green);
					series.strokeWidth = 1;
					series.tooltipText = "{valueY}";
					series.fillOpacity = 0.2;
					
					//chart.scrollbarX = new am4core.Scrollbar();
					chart.scrollbarX = new am4charts.XYChartScrollbar();
					chart.scrollbarX.series.push(series);
					HWS.buffer.graphConectSystem[tpy] = chart;
				}
			}
		}
	}
	function clear_graph(){
		if(!HWS.buffer.graphConectSystem){HWS.buffer.graphConectSystem = {}}
		if(HWS.buffer.graphConectSystem['gsm']){HWS.buffer.graphConectSystem['gsm'].dispose(); HWS.buffer.graphConectSystem['gsm'] = null;}
		if(HWS.buffer.graphConectSystem['wifi']){HWS.buffer.graphConectSystem['wifi'].dispose(); HWS.buffer.graphConectSystem['wifi'] = null;}
	}
	
	// изменение времени ДЖСМ ВИ-ФИ графика
	HWS.buffer.timeControlFCNgraph = function(H){
		clear_graph();
		
		let gsmGraph = HWS.getElem('#block-graf-analog-gsm .graph');
		let wifiGraph = HWS.getElem('#block-graf-analog-wifi .graph');
			if(gsmGraph) { gsmGraph.innerHTML = HWhtml.loader();}
			if(wifiGraph){wifiGraph.innerHTML = HWhtml.loader();}
		let hour = parseInt(H);
		HWS.buffer.timeValueGraph = hour;
		
		HWS.buffer.systemTimeDayGsm = hour || 1;
		HWS.buffer.graphConectSystem.gsm = '';
		load_graph_conect_system(hour,HWS.buffer.modulSystem[4].id,'#block-graf-analog-gsm .graph','gsm');
		
		HWS.buffer.systemTimeDayWifi = hour || 1;
		HWS.buffer.graphConectSystem.wifi = '';
		load_graph_conect_system(hour,HWS.buffer.modulSystem[5].id,'#block-graf-analog-wifi .graph','wifi');
		let dataP = {token:HWS.pageAccess,id_object:id,web_config:{time_per_graph:H}}
		HWF.post({url:HWD.api.gets.setWebConfig,data:dataP});
	}
	
	//Wi-Fi изменить настройки
	HW.on('click','btn_acept_wifi_list',function(e,elem){
		let block = HWS.getElem('#system-wifi-control');
		let idMod = HWS.buffer.modulSystem[5].id;
		let nameWifi = block.querySelector('.btn_select_wifi_networck').value;
		let passWifi = block.querySelector('.pass-wifi').value;
		let dataP = {token:HWS.pageAccess,id_object:idMod,cfg_wifi_ssid:nameWifi,cfg_wifi_psw:passWifi}
		let wifiConnectStatBlock = block.querySelector('.wifi-status-sistem');

		if(passWifi){
			if(passWifi.length > 7 && passWifi.length < 25){
				let reg = /[^a-zA-Z0-9_-]+/g;
				let test = reg.test(passWifi);
				console.log(test);
				if (!test) {
					wifiConnectStatBlock.innerHTML = '';
					HWF.push('Отправлен запрос на изменение Wi-Fi');
					HWF.post({url:'task/send_wifi_parameters_for_3x_system',data:dataP});	
				} else {HWF.push('Некорректный пароль Wi-Fi','red'); }
			} else {HWF.push('Введите пароль Wi-Fi от 8 до 24 символов','red');}
		} else {HWF.push('Введите пароль Wi-Fi','red');}
	});
	//Wi-Fi Обновить список
	HW.on('click','btn_update_wifi_list',function(e,elem){
		let block = HWS.getElem('#system-wifi-control');
		let selectB = block.querySelector('.btn_select_wifi_networck');
		let miniLoad = block.querySelector('.mini_load_wifi_rebout ');
		let idMod = HWS.buffer.modulSystem[5].id;
		let dataP = {token:HWS.pageAccess,id_object:idMod,command:'update_networks'}
		
		miniLoad.classList.add('open');
		selectB.innerHTML = '';
		HWF.push('Отправлен запрос на обновление списка Wi-Fi');
		HWF.post({url:'task/send_wifi_parameters_for_3x_system',data:dataP,fcn:function(info){
			if(info.success) {
				HWF.post({url:HWD.api.gets.list,data:{
						token:token,
						list_start:0,
						list_amount:1000,
						id_system:HWS.buffer.elem.id_system,
						local_id_object_type:0,
						local_id_object_group:5,
					},
					fcn:function(infoWifi){
						infoWifi = infoWifi.list[0];
						let specList = [];
						let listWiFi = infoWifi.info.wifi_list;
						if(listWiFi){ for (let key in listWiFi) {specList.push({name:listWiFi[key].ssid,val:listWiFi[key].ssid});}}
						else if(infoWificonfig.cfg_wifi_ssid) {specList.push({name:infoWifi.cfg_wifi_ssid,val:infoWifi.cfg_wifi_ssid});}
						for (let key in specList) {if(specList[key].name == infoWifi.cfg_wifi_ssid) {specList[key].selected = 'selected';}}
						
						selectB.innerHTML = HWhtml.select(specList,'btn_select_wifi_networck');
						miniLoad.classList.remove('open');
					}
				});
			}
		}});
	});
	
	HW.on('change','btn_save_new_number',function(e,elem){  // сохранение кастомного запроса баланса
		console.log(elem.value);
		let block = HWS.getElem('#system-gsm-control');
		let number = elem.value || '';
		let idMod = HWS.buffer.modulSystem[4].id;
		let dataP = {token:HWS.pageAccess,id_object:idMod,phone_number:number}
		HWF.post({url:HWD.api.gets.taskGsmParametersTo3xSystem,data:dataP});
		
	});
	// GSM баланс
	HW.on('change','custom_get_balance_system',function(e,elem){  // сохранение кастомного запроса баланса
		let block = HWS.getElem('#system-gsm-control');
		let miniload = block.querySelector('.mini_load_gsm_custom');
		let idMod = HWS.buffer.modulSystem[4].id;
		let dataP = {token:HWS.pageAccess,id_object:idMod,cfg_balussd:elem.value}
		miniload.classList.add('open');
		HWF.post({url:HWD.api.gets.taskGsmParametersTo3xSystem,data:dataP,fcn:function(){miniload.classList.remove('open');}});
	});
	HW.on('change','btn_cange_text_get_balance',function(e,elem){ // изменение запроса баланса
		let block = HWS.getElem('#system-gsm-control');
		let textinfo = block.querySelector('.custom_text_query');
		let valinfo = block.querySelector('.custom_val_query');
		
		let idMod = HWS.buffer.modulSystem[4].id;
		let infBal = '';
		if(elem.value != 'custom'){
			infBal = elem.value;
			textinfo.classList.remove('open');
			valinfo.classList.remove('open');
		}else if (elem.value == 'custom') {
			textinfo.classList.add('open');
			valinfo.classList.add('open');
		}
		let dataP = {token:HWS.pageAccess,id_object:idMod,cfg_balussd:infBal}
		
		HWF.post({url:HWD.api.gets.taskGsmParametersTo3xSystem,data:dataP});
	});	
	HW.on('click','btn_get_new_balance',function(e,elem){ // Запрос на обновлния баланса
		let block = HWS.getElem('#system-gsm-control');
		let miniload = block.querySelector('.mini_load_gsm_rebout');
		HWF.push('Отправлен запрос на обновление баланса')
		let dataP = {token:HWS.pageAccess,id_object:id,command:'balance'}
		miniload.classList.add('open');
		HWF.post({url:HWD.api.gets.taskCommandTo3xSystem,data:dataP,fcn:function(){miniload.classList.remove('open');}});
	});
	
	// GSM Микрофон
	HW.on('click','btn_microphone_system_onof',function(e,elem){
		C.btnMicrophone = 1;
		let idMod = HWS.buffer.modulSystem[4].id;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:idMod,cfg_mphone:1}
			HWF.post({url:HWD.api.gets.taskGsmParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:idMod,cfg_mphone:0}
			HWF.post({url:HWD.api.gets.taskGsmParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		}
	});	
	// Приоритет подключения
	HW.on('change','btn_priority_connections',function(e,elem){
		let dataP = {token:HWS.pageAccess,id_object:id,cfg_lk:elem.value}
		HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
	});	
	// Порт доп
	HW.on('click','btn_portdop_onof',function(e,elem){
		C.btnDop = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,cfg_ext:1}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,cfg_ext:0}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		}
	});	
	// Радиосеть
	HW.on('click','btn_radio_system_onof',function(e,elem){
		C.btnRadio = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,cfg_radio:1}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,cfg_radio:0}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		}
	});		
	// Тревожная кнопка 
	HW.on('click','btn_warninr_but_onof',function(e,elem){
		C.btnAlertBtn = 1;
		if(elem.classList.contains('active')){
			let dataP = {token:HWS.pageAccess,id_object:id,cfg_button:1}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		}else {
			let dataP = {token:HWS.pageAccess,id_object:id,cfg_button:0}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		}
	});
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/add_favorite_objects',data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/delete_favorite_objects',data:dataP});
		}
	});
	// Изменение пароля
	HW.on('click','btn_cange_pasword_system',function(e,elem){
		let Mblock = document.body.querySelector('#system-new-pasword');
		let newP = Mblock.querySelector('.new_pasword');
		let newPR = Mblock.querySelector('.new_pasword');
		let lastP = Mblock.querySelector('.last_pasword');
		let error = Mblock.querySelector('.error_mesage');
		
		let testC = parseInt(newP.value);
			testC = String(testC);
			
		if(testC.length != 4){
			error.innerHTML = 'Только 4 цифры!'; 
			clearTimeout(HWS.buffer.cangePasvordIDTimer);
			HWS.buffer.cangePasvordIDTimer = setTimeout(function(){error.innerHTML = '';},5000);
		} else if(newP.value != newPR.value) {
			error.innerHTML = 'Парольи несовпадают!'; 
			clearTimeout(HWS.buffer.cangePasvordIDTimer);
			HWS.buffer.cangePasvordIDTimer = setTimeout(function(){error.innerHTML = '';},5000);
		} else if (lastP.value != HWS.buffer.elem.config.cfg_psw) {
			error.innerHTML = 'Пароль неверен'; 
			clearTimeout(HWS.buffer.cangePasvordIDTimer);
			HWS.buffer.cangePasvordIDTimer = setTimeout(function(){error.innerHTML = '';},5000);
		} else {
			error.innerHTML = ''; newP.value = ''; newPR.value = ''; lastP.value = '';
			HWF.push('Отправлен запрос на смену пароля');
			let dataP = {token:HWS.pageAccess,id_object:id,cfg_psw:testC}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		}
	});
	// Задать время на системе
	HW.on('click','canche_data_system_btn',function(e,elem){
		let block = HWS.getElem("#system-date-time");
		let valData = block.querySelector('.time_val_data');
		//let timeText = block.querySelector('.time_text_hh_mm');
		let timeVal = block.querySelector('.inp_time_text_hh_mm');
		let iconDate = block.querySelector('.selector-date .icon-setdate');
		
		if(elem.classList.contains('active')){
			let newVal = timeVal.value;
			let time = newVal.split(':');
			valData.setAttribute('disabled','disabled');
			timeVal.setAttribute('disabled','disabled');
			iconDate.classList.add('of');

			elem.classList.remove('active');
			elem.classList.add('grey');
			elem.innerHTML = 'Изменить';
			
			//момент сохранения времени
			let block = HWS.getElem("#system-date-time");
			let dataDay = valData.value;;
			let dataH = time[0];
			let dataM = time[1];
			let regDate = /([0-2]\d|3[01])\.(0\d|1[012])\.(\d{4})/
				regDate = regDate.test(dataDay);
			let regTimeH = (parseInt(dataH) <= 24 && parseInt(dataH) >= 0)? true : false ;
			let regTimeM = (parseInt(dataM) <= 60 && parseInt(dataM) >= 0)? true : false ;

			if(regDate && regTimeH && regTimeM){
				let corect = new Date().getTimezoneOffset()*60*1000; corect = corect*(-1);
				let GTO = {}, prTime = dataDay.split('.');
					GTO.y = parseInt(prTime[2]);
					GTO.m = parseInt(prTime[1])-1;
					GTO.d = parseInt(prTime[0]);
					GTO.h = parseInt(dataH);
					GTO.mi = parseInt(dataM);
					GTO.unx = ((new Date(GTO.y,GTO.m,GTO.d,GTO.h,GTO.mi).getTime()+corect))/1000;
				HWF.push('Отправлен запрос на изменение даты ');
				let dataP = {token:HWS.pageAccess,id_object:id,time:GTO.unx}; console.log(dataP);
				HWF.post({url:HWD.api.gets.taskSendSystemTime,data:dataP,fcnE:HWF.getServMesErr});
			}
			
		}
		else {
			valData.removeAttribute('disabled');
			timeVal.removeAttribute('disabled');
			iconDate.classList.remove('of');
			
			elem.classList.add('active');
			elem.classList.remove('grey');
			elem.innerHTML = 'Применить';
		}
	});	
	// Сброс настроек системы
	HW.on('click','btn_rebout_sets_system',function(e,elem){
		HWF.modal.confirm('Сброс настроек','Сбросить все настройки системы на заводские?');
		HW.on('click','btn-yes-modal',function(e,elem){
			HWF.push('Отправлен запрос на сброс настроек ');
			let dataP = {token:HWS.pageAccess,id_object:id,command:'drop_settings'}
			HWF.post({url:HWD.api.gets.taskCommandTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		});
	});	
	// Установка оборудования системы
	HW.on('click','btn_ust_new_equp_system',function(e,elem){
		HWF.modal.confirm('Добавить устройства','Добавить новое оборудование к системе?');
		HW.on('click','btn-yes-modal',function(e,elem){
			HWF.push('Отправлен запрос на установку оборудования ')
			let dataP = {token:HWS.pageAccess,id_object:id,command:'ust'}
			HWF.post({url:HWD.api.gets.taskCommandTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		});
	});	
	// Обновление системы
	HW.on('click','btn_update_new_version_system',function(e,elem){
		HWF.modal.confirm('Обновление системы','Обновить систему на новую прошивку?');
		HW.on('click','btn-yes-modal',function(e,elem){
			HWF.push('Отправлен запрос на обновление системы ')
			let dataP = {token:HWS.pageAccess,id_object:id,command:'update'}
			HWF.post({url:HWD.api.gets.taskCommandTo3xSystem,data:dataP,fcnE:HWF.getServMesErr});
		});
	});		
	// Перезагрузка системы
	HW.on('click','btn_reset_system',function(e,elem){
		HWF.modal.confirm('Перезагрузка системы','Перезагрузить систему?');
		HW.on('click','btn-yes-modal',function(e,elem){
			HWF.push('Отправлен запрос на перезагрузку системы ')
			let dataP = {token:HWS.pageAccess,id_object:id,command:'reboot'}
			HWF.post({url:HWD.api.gets.taskCommandTo3xSystem,data:dataP,fcn:HWF.getServMesErr});
		});
	});
	
	HW.on('click','btn_problem_conection',function(e,el){
		let printM = '',printD = '',print='', contPrint = '',it=1;
		if(el.dataset.type == 'wifi'){printM = HWS.buffer.mesageErrocConectionWIFImes || ''; printD = HWS.buffer.mesageErrocConectionWIFIdes || '';}
		if(el.dataset.type == 'gsm'){printM = HWS.buffer.mesageErrocConectionGSMmes || ''; printD = HWS.buffer.mesageErrocConectionGSMdes || '';}
		if(printM){HWF.recount(printM,function(elM){print+= '<li>'+elM+'</li>'})}
		if(printD){HWF.recount(printD,function(elD){print+= '<li>'+elD+'</li>'})}
		
		contPrint = '<ul class="number-info-print block_w_650p">'+print+'</ul>';
		
		HWF.modal.message('Нет связи? Проверьте:',contPrint);
	});
	
	HW.on('change','btn_timezone_selector',function(e,el){
		let dataP = {token:HWS.pageAccess,id_object:id,timezone:el.value}
		HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP});
	});
	
	HW.on('click','btn_timezone_ntp',function(e,el){
		C.timezoneNTP = 1;
		if(el.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,time_source_ntp:1}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,time_source_ntp:0}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP});
		}
	});
	
	HW.on('click','btn_timezone_gsm',function(e,el){
		C.timezoneGSM = 1;
		if(el.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,time_source_gsm:1}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,time_source_gsm:0}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSystem,data:dataP});
		}
	});
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(elem); // Проверит ваш ли это объект для вовода предупреждения
});