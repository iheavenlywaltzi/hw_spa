"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let info = HWS.buffer.elem;
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = info.id; 
	let print = {}, S={};
	let normInfo = HWequip.normInfo(info);
	let C = {};
	let nameFocus = false;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageGraph">График</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';

	HWS.buffer.equpPageSets = function(type){type = type || false;
		if(type == '1'){print_sets(info)}
		else{
			C.globalSets = 1;
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	
	HWS.buffer.equpPageGraph = function(){
		print.graph = HWhtml.equp.graph(1,'timeControlFCNDiscret','timeControlLoadFCN');
		content.innerHTML = print.graph
		
		HWS.buffer.timeValueGraph = 1;
		HWS.buffer.load_discret_graph(HWS.buffer.timeValueGraph,3); // запуск и отрисовка графика Статистики показаний
		HWS.buffer.pageOpenControl = 'equpPageGraph';
	}
	
	HWS.buffer.equpPageJurnal = function(){
		content.innerHTML = "";
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'equpPageJurnal';
	}	
	
	HWS.buffer.equpPageAccesses = function(){
		Sprint = '<div class="one-block ob-top conections"> <h4>Доступы</h4>';
		Sprint+= '</div>';
		content.innerHTML = Sprint;
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
	}	
	
	// Распечатка настроек delay_time_ontime
	function print_sets(info){
		HWS.buffer.elem = info;
		let journal = info.config.flags.journal; if(journal){journal = 'active';}
		let ontime = info.config.delay || 0; let ontimeSelect1 = ''; let ontimeSelect2 = '';
		if (ontime > 0){ontimeSelect2 = 'active';}
		else {ontimeSelect1 = 'active';}
		let favorite = (info.favorite)?'active':'';
		
		let global = '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Журнал '+HWhtml.helper(HWD.helpText.equp.btnHelpUUActive)+'</p>\
					<span id="equp-jurnal" class="equp_jurnal btn-active on-off '+journal+'" data-targetplus="warning-min-max"></span>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.equp.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+favorite+'" ></span>\
				</div>\
			</div>\
		</div>';
		// Хелперы для кнопочек
		let helpInf_1 = (normInfo.type == 7041)?HWD.helpText.equp.btnHelpUUHoldKrane:HWD.helpText.equp.btnHelpUUHold;
		let helpInf_2 = (normInfo.type == 7041)?HWD.helpText.equp.btnHelpUUTimeKrane:HWD.helpText.equp.btnHelpUUTime;
		
		let typeControl = '<div class="one-block-col">\
			<div class="title"><h4>Тип управления</h4></div>\
			<div class="ltft"></div>\
			<div class="right selectorTimeControl">\
				<div class="colum">\
					<p><span class="delay_no_limite_btn btn-active-one btn-checkboxEC '+ontimeSelect1+'" data-target="selectorTimeControl">'+normInfo.et.btnName+' и удерживать</span> '+HWhtml.helper(helpInf_1)+'</p>\
				</div>\
				<div class="colum">\
					<p><span class="delay_limite_btn btn-active-one btn-checkboxEC '+ontimeSelect2+'" data-target="selectorTimeControl">'+normInfo.et.btnName+' на время, сек</span> '+HWhtml.helper(helpInf_2)+'</p>\
					<br>\
					<p id="delay-limite-int-btn">'+HWhtml.btn.numb(ontime,{fcn:'delayTimeOntime',id:'tupe-control-time',min:0,max:9999})+'</p>\
				</div>\
			</div>\
		</div>';

		content.innerHTML = global+typeControl;
		HWS.buffer.pageOpenControl = 'equpPageSets';
	}

	// Динамическое обновление данных 
	function reolad_info_page(idTimer){ idTimer=idTimer || 0;
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(inf){
			let blockBlam = document.body.querySelector('#blamba');
			let mom = document.querySelector('#equp-content-top .page-top-dop-info');
			let tex = mom.querySelector('.data');
			let btn = mom.querySelector('.btn_onof_relay');
			let IBused = HWS.getElem('#top-dop-info-page-bottom');
			let nameEqup = HWS.getElem('#equp-content-top .name-page .equp_name');
			normInfo = HWequip.normInfo(inf);
			
			if(normInfo.used){
				tex.innerHTML = normInfo.et.state[normInfo.stateN].name;
				HWS.buffer.elem = inf;
				btn.classList.add('blocked');
				used_elem();
			}
			else {
				btn.classList.remove('blocked');
				IBused.innerHTML = '';
			}
			
			if(normInfo.stateN == 0){ btn.classList.remove('active'); btn.parentElement.classList.remove('block-onof');}
			if(normInfo.stateN == 1){ btn.classList.add('active'); btn.parentElement.classList.remove('block-onof');}
			
			if(normInfo.stateN == 254){ btn.classList.add('active'); btn.parentElement.classList.add('block-onof');}
			if(normInfo.stateN == 255){ btn.classList.add('active'); btn.parentElement.classList.add('block-onof');}
	
			tex.innerHTML = normInfo.et.state[normInfo.stateN].name;
			blockBlam.style.background = normInfo.et.state[normInfo.stateN].color;
			
			HWS.buffer.system_time = inf.system_time;
			if(nameEqup.value != inf.config.name && !nameFocus) {nameEqup.value = inf.config.name;}
			if(HWS.buffer.pageOpenControl == 'equpPageSets' && !C.globalSets){
				let jurnal = HWS.getElem('#equp-jurnal');
				let btnOn = HWS.getElem('.delay_no_limite_btn');
				let btnTime = HWS.getElem('.delay_limite_btn');
				let inpTome = HWS.getElem('#tupe-control-time .inp_num_val');
				let favorite = document.body.querySelector('#equp-viget');

				if(!C.favorite) {(inf.favorite)?favorite.classList.add('active'):favorite.classList.remove('active');}
				if(!C.jurnal) {(inf.config.flags.journal)?jurnal.classList.add('active'):jurnal.classList.remove('active');}
				if(!C.typeControl && inpTome.value != inf.config.delay) {
					inpTome.value = inf.config.delay || 0;
					if(inpTome.value > 0){ btnOn.classList.remove('active'); btnTime.classList.add('active');}
					else {btnOn.classList.add('active'); btnTime.classList.remove('active');}
				}
			}
			if(HWS.buffer.pageOpenControl == 'equpPageGraph'){HWS.buffer.load_discret_graph(HWS.buffer.timeValueGraph,3,'reload');}
			//if(HWS.buffer.pageOpenControl == 'jurnal'){HWS.buffer.load_history('reload');}
			
			C = {};
			info = inf;
			
		}});
	}
	// если устройство используется то прогрузим
	function used_elem(){
		let elem = HWS.buffer.elem;
		let localId = elem.state.block_local_id;
		let systemId = elem.id_system;
		let IBused = HWS.getElem('#top-dop-info-page-bottom');
		
		if(!localId){
			IBused.innerHTML = '<p class="flex_between">\
				<span class="block_mr_6">Используется в </span>\
				<span class="tw_600 block_mr_6">Брелке</span>\
			</p>';
		}
		else{
			HWF.post({url:HWD.api.gets.objectLocalId,data:{token:token,local_id:localId,id_system:systemId},fcn:function(inf){
				IBused.innerHTML = '<p class="flex_between tc_grey6 ts_11p block_pl_40">\
					<span class="block_mr_6">Используется в </span>\
					<span class="tw_600 block_mr_6"><a href="#'+HWD.page.prog+'/'+inf.id+'" class="tc_grey6">'+inf.config.name+'</a></span>\
					<a href="#'+HWD.page.prog+'/'+inf.id+'" class="icon icon-change p13 hover btn-proj-change"></a>\
				</p>';
			}});
		}
	}
	
	//Печать верхнего блока
	// верхний блок ( имя и т.д )
	print.top = {};
	print.top.name = info.config.name;
	print.top.title = normInfo.title || 'none';
	print.top.meterage = HWD.unit[info.info.type.unit] || '';
	print.top.stateName = info.state.state.name || '';
	if (info.state.power){print.top.power = info.state.power.val ||  info.state.power.percent || info.state.power.quality || 0;}
	else {print.top.power = 0}
	print.top.connect = 0;
	print.top.onof = {act:true,val:info.state.state.val};
	print.top.dataClass = 'block_w_280p';
	print.top.icon = info.info.type.type || '';
	S.used = false;
	
	if(normInfo.port == 10 || normInfo.port == 12){
		print.top.ident = normInfo.ident || false;
		print.top.addr = (normInfo.portSymbol+normInfo.addr) || false;
		print.top.chanel = normInfo.channel_index || false;
	} else { print.top.port = normInfo.portSpec || false; }

	if(normInfo.stateN == 255){print.top.onoShadow = 1}
	if(normInfo.stateN == 254){print.top.onoShadow = 1}
	
	print.top.data = info.state.state.name;
	print.top.blambaColor = normInfo.et.state[info.state.state.val].color;
	if(normInfo.used){
		print.top.onofBlocked = 1; S.used = true;
		print.top.dataBot = '<p class="block_pl_40">'+HWhtml.miniLoader()+'</p>';
	}
	
	// Запрос системы
	print.top.systemName = info.system_name;
	print.top.systemCN = info.system_ident;

	print.top = HWhtml.equp.top(print.top);
	contentTop.innerHTML = print.top;
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	if(S.used){used_elem()}
	// автообновление
	//HWF.timerUpdate(reolad_info_page);

	// Функции нажатий и обработки для работы
	HW.on('click','equp_jurnal',function(e,el){ //Изменение Журнал 
		C.jurnal = 1;
		if(el.classList.contains('active')){
			let dataP = {token:HWS.pageAccess,id_object:id,journal:1}
			HWF.post({url:HWD.api.gets.taskFlagsToAutomatic3xDevice,data:dataP,fcnE:HWF.getServMesErr});
		}else {
			let dataP = {token:HWS.pageAccess,id_object:id,journal:0}
			HWF.post({url:HWD.api.gets.taskFlagsToAutomatic3xDevice,data:dataP,fcnE:HWF.getServMesErr});
		}
	});

	HW.on('click','btn_onof_relay',function(e,el){  //нажатие на реле
		if(!el.classList.contains('blocked')){
			let blockBlam = document.body.querySelector('#blamba');
			let mom = document.querySelector('#equp-content-top .page-top-dop-info');
			let tex = mom.querySelector('.data');
			let timeD = document.querySelector('#delay-limite-int-btn .btn-int .inp_num_val');
					if(timeD){timeD = parseInt(timeD.value) || 0;}
					else {timeD = 0;}
			let clickVal = 0;
			
			if(el.classList.contains('active')){
				clickVal = 1;
				let dataP = {token:HWS.pageAccess,commands_to_system:{
					
				}}
				
				
				//let dataP = {token:HWS.pageAccess,id_object:id,state:clickVal,delay:timeD}
				//HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
			}else {
				clickVal = 0;
				
				//let dataP = {token:HWS.pageAccess,id_object:id,state:clickVal}
				//HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
			}
			
			tex.innerHTML = normInfo.et.state[clickVal].name;
			console.log(normInfo);
			blockBlam.style.background = normInfo.et.state[clickVal].color;

			clearInterval(HWS.buffer.idTimerREoladPage);
		}
	});
	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,favorite:1}
			HWF.post({url:HWD.api.gets.taskFlagsToAutomatic3xDevice,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,favorite:0}
			HWF.post({url:HWD.api.gets.taskFlagsToAutomatic3xDevice,data:dataP});
		}
	});	
	
	//Изменение имени
	HW.on('focusin','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; nameFocus = 1;
	});
	HW.on('focusout','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; nameFocus = 1;
	});
	HW.on('input','equp_name',function(e,el){nameFocus = 1}); HW.on('click','equp_name',function(e,el){nameFocus = 1});
	
	HW.on('change','equp_name',function(e,el){
		nameFocus = false;
		let name = document.querySelector('.name-page input');
		let nameRegex = new RegExp(HWD.reg.name);
		
		if(HWS.buffer.elem.config.name != name.value){
			if(nameRegex.test(name.value)){
				let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
				HWF.post({url:HWD.api.gets.taskName3xDevice,data:dataP});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
	
	HWS.buffer.delayTimeOntime = function(inf){
		C.typeControl = 1;
		let control = HWS.getElem('.delay_limite_btn');
		let nonstop = HWS.getElem('.delay_no_limite_btn');
		
		if(inf > 0){
			control.classList.add('active');
			nonstop.classList.remove('active');
			let dataP = {token:HWS.pageAccess,id_object:id,delay:inf}
			HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
		}else {
			control.classList.remove('active');
			nonstop.classList.add('active');
		}
	}
	
	HW.on('click','delay_limite_btn',function(e,el){
		C.typeControl = 1;
		let input = HWS.getElem('#delay-limite-int-btn .inp_num_val');
		if(input.value <= 0){input.value = 1;}
	});
	
	HW.on('click','delay_no_limite_btn',function(e,el){
		C.typeControl = 1;
		let input = HWS.getElem('#delay-limite-int-btn .inp_num_val');
		let dataP = {token:HWS.pageAccess,id_object:id,delay:0}
		HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
		input.value = 0;
	});
});