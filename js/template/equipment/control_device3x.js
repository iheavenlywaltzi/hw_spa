"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let noConnectBlock = HWS.getElem('#no-connect-block');
	let info = HWS.buffer.elem;
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = info.id; 
	let print = {}, S={};
	let normInfo = HWequip.normInfo(info);
	let C = {};
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageGraph">График</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';

	HWS.buffer.equpPageSets = function(type){type = type || false;
		clear_graph();
		if(type == '1'){print_sets(info)}
		else{
			C.globalSets = 1;
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	
	HWS.buffer.equpPageGraph = function(){
		noConnectBlock.classList.remove('on');
		let on = (normInfo.et.legend && normInfo.et.legend.on)?normInfo.et.legend.on:'Включено'; 
		let off = (normInfo.et.legend && normInfo.et.legend.off)?normInfo.et.legend.off:'Отключено'; 
		let legend = '<div class="graph-legend ta_center">\
			<div class="display_inline"><span class="figure square blue"></span> <span>'+on+'</span></div>\
			<div class="display_inline block_pl_50"><span class="figure square"></span> <span>'+off+'</span></div>\
		</div>';
		let timeGraph = HWS.buffer.timeValueGraph || 1;
		print.graph = HWhtml.equp.graph(timeGraph,'timeControlFCNDiscret','timeControlLoadFCN','',legend);
		content.innerHTML = print.graph;
		
		clear_graph();
		
		HWS.buffer.timeValueGraph = 1;
		HWS.buffer.load_discret_graph(timeGraph,3); // запуск и отрисовка графика Статистики показаний
		HWS.buffer.pageOpenControl = 'equpPageGraph';
	}
	
	HWS.buffer.equpPageJurnal = function(){
		noConnectBlock.classList.remove('on');
		clear_graph();
		content.innerHTML = "";
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'equpPageJurnal';
	}	
	
	HWS.buffer.equpPageAccesses = function(){
		noConnectBlock.classList.remove('on');
		clear_graph();
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
		HWS.buffer.equpAccessesFCN();
	}	
	
	// Распечатка настроек delay_time_ontime
	function print_sets(info){
		HWS.buffer.elem = info;
		normInfo = HWequip.normInfo(info);
		let journal = info.config.flags.journal || 0; if(journal){journal = 'active';}
		let ontime = info.config.delay_lk || 0; let ontimeSelect1 = ''; let ontimeSelect2 = '';
		let selected = info.config.delay_selected || false;
		if (selected){ontimeSelect2 = 'active';}
		else {ontimeSelect1 = 'active';}
		let favorite = (info.lk.favorite)?'active':'';
		
		let connect = info.system_connection_status || false;
		if(!connect && info.cloud_type != 14719) {noConnectBlock.classList.add('on')}
		else {noConnectBlock.classList.remove('on');}
		
		let batterySms = '', batteryBtn = '', batteryCheck = false;
		batteryCheck = HWF.objKeyTest('battery_discharge_alarm',info.config.flags);
		if(batteryCheck){
			batterySms = '';
			batteryBtn = (info.config.flags.battery_discharge_alarm)?'active':'';
		}
		else {batterySms = 'display_none'}
		
		let global = '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Журнал '+HWhtml.helper(HWD.helpText.equp.btnHelpUUActive)+'</p>\
					<span id="equp-jurnal" class="equp_jurnal btn-active on-off '+journal+'" data-targetplus="warning-min-max"></span>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.equp.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+favorite+'" ></span>\
				</div>\
				<div class="colum '+batterySms+'">\
					<p>Оповещать о разряде батарей '+HWhtml.helper(HWD.helpText.equp.battareyHelper)+'</p>\
					<span id="equp-battery" class="equp_battery_btn btn-active on-off '+batteryBtn+'" ></span>\
				</div>\
			</div>\
		</div>';
		// Хелперы для кнопочек
		let helpInf_1 = (normInfo.type == 7041)?HWD.helpText.equp.btnHelpUUHoldKrane:HWD.helpText.equp.btnHelpUUHold;
		let helpInf_2 = (normInfo.type == 7041)?HWD.helpText.equp.btnHelpUUTimeKrane:HWD.helpText.equp.btnHelpUUTime;
		let dopClassSek = (ontime > 0)?'':'display_none';
		
		let typeControl = '<div class="one-block-col">\
			<div class="title"><h4>Тип управления</h4></div>\
			<div class="ltft"></div>\
			<div class="right selectorTimeControl">\
				<div class="colum">\
					<p><span class="delay_no_limite_btn btn-active-one btn-checkboxEC '+ontimeSelect1+'" data-target="selectorTimeControl">'+normInfo.et.btnName+' и удерживать</span> '+HWhtml.helper(helpInf_1)+'</p>\
				</div>\
				<div class="colum">\
					<p><span class="delay_limite_btn btn-active-one btn-checkboxEC '+ontimeSelect2+'" data-target="selectorTimeControl">'+normInfo.et.btnName+' на время, сек</span> '+HWhtml.helper(helpInf_2)+'</p>\
					<br>\
					<p id="delay-limite-int-btn" class="'+dopClassSek+'">'+HWhtml.btn.numb(ontime,{fcn:'delayTimeOntime',id:'tupe-control-time',min:0,max:9999})+'</p>\
				</div>\
			</div>\
		</div>';
		
		let btnDelete = '';
		if (normInfo.port == 12 || normInfo.port == 11 || normInfo.port == 3){
			btnDelete += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin"></p>\
					<p class="btn_text grey btn_delete_equp">Удалить устройство</p>\
				</div>\
			</div>\
		</div>';
		}

		content.innerHTML = global+typeControl+btnDelete;
		HWS.buffer.pageOpenControl = 'equpPageSets';
	}

	// Динамическое обновление данных 
	function reolad_info_page(idTimer){ idTimer=idTimer || 0;
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(inf){
			let blockBlam = document.body.querySelector('#blamba');
			let mom = document.querySelector('#equp-content-top .page-top-dop-info');
			let tex = mom.querySelector('.data inf');
			let btn = mom.querySelector('.btn_onof_relay');
			let nameEqup = HWS.getElem('#equp-content-top .name-page .equp_name');
			normInfo = HWequip.normInfo(inf);
			let state = inf.lk.state;
			
			if(normInfo.used){HWS.buffer.elem = inf;}
			else {btn.classList.remove('blocked');}
			
			if(inf.state.block_local_id) {used_elem();}
			
			blockBlam.style.background = HWF.getStateColor(state);
			if(state == 255){btn.parentElement.classList.add('block-onof');}
			if(state == 254){btn.parentElement.classList.add('block-onof');}
			if(state == 31) {btn.classList.add('active');  btn.parentElement.classList.remove('block-onof');}
			if(state == 30) {btn.classList.remove('active'); btn.parentElement.classList.remove('block-onof');}
			
			tex.innerHTML = inf.lk.state_name;
			
			HWS.buffer.system_time = inf.system_time;
			if(nameEqup.value != inf.config.name && !HWS.buffer.nameFocus) {nameEqup.value = inf.config.name;}
			
			if(HWS.buffer.pageOpenControl == 'equpPageSets' && !C.globalSets){
				let jurnal = HWS.getElem('#equp-jurnal');
				let batteryBtn = document.body.querySelector('#equp-battery');
				let btnOn = HWS.getElem('.delay_no_limite_btn');
				let btnTime = HWS.getElem('.delay_limite_btn');
				let inpTome = HWS.getElem('#tupe-control-time .inp_num_val');
				let favorite = document.body.querySelector('#equp-viget');

				if(!C.favorite) {(inf.lk.favorite)?favorite.classList.add('active'):favorite.classList.remove('active');}
				if(!C.jurnal) {(inf.config.flags.journal)?jurnal.classList.add('active'):jurnal.classList.remove('active');}
				if(!C.batteryBtn) {(inf.config.flags.battery_discharge_alarm)?batteryBtn.classList.add('active'):batteryBtn.classList.remove('active');}
				if(!C.typeControl && inpTome.value != inf.config.delay_lk) {
					inpTome.value = inf.config.delay_lk || 0;
					if(inpTome.value > 0){ btnOn.classList.remove('active'); btnTime.classList.add('active');}
					else {btnOn.classList.add('active'); btnTime.classList.remove('active');}
				}
			}
			if(HWS.buffer.pageOpenControl == 'equpPageGraph'){HWS.buffer.load_discret_graph({time:HWS.buffer.timeValueGraph});}
			//if(HWS.buffer.pageOpenControl == 'jurnal'){HWS.buffer.load_history('reload');}
			
			C = {};
			info = inf;
		}});
	}
	// если устройство используется то прогрузим
	function used_elem(){
		let elem = HWS.buffer.elem;
		let localId = elem.state.block_local_id;
		let systemId = elem.id_system;
		let IBused = HWS.getElem('#top-dop-info-page-bottom');
		
		if(!localId){
			IBused.innerHTML = '';
		}
		else{
			HWF.post({url:HWD.api.gets.objectLocalId,data:{token:token,local_id:localId,id_system:systemId},fcn:function(inf){
				IBused.innerHTML = '<p class="flex_between tc_grey6 ts_11p block_pl_50">\
					<span class="block_mr_6">Используется в </span>\
					<span class="tw_600 block_mr_6"><a href="#'+HWD.page.prog+'/'+inf.id+'" class="tc_grey6">'+inf.config.name+'</a></span>\
					<a href="#'+HWD.page.prog+'/'+inf.id+'" class="icon icon-settings p13 hover btn-proj-change"></a>\
				</p>';
			}});
		}
	}
	
	function clear_graph(){
		if(!HWS.buffer.discret_graph){HWS.buffer.discret_graph = {}}; 
		if(HWS.buffer.discret_graph[id]){
			HWS.buffer.discret_graph[id].dispose();
			HWS.buffer.discret_graph[id] = null;
			HWS.buffer.controlAutoReoladStoper = true;
		}
		let blockT = HWS.getElem('#equp-statistic-graf .graph-windiw-statistic');
		if(blockT){blockT.innerHTML = HWhtml.loader();}
	}
	
	//Печать верхнего блока
	// верхний блок ( имя и т.д )
	print.top = {};
	print.top.name = info.config.name;
	print.top.title = normInfo.title || 'none';
	print.top.power = info.state.power.val ||  info.state.power.percent || info.state.power.quality || 0;
	print.top.connect = 0;
	print.top.onof = {act:true,val:info.state.state.val};
	print.top.dataClass = 'block_w_280p';
	print.top.icon = info.cloud_type || '';
	S.used = false;
	
	if(normInfo.port == 10 || normInfo.port == 12){
		print.top.ident = normInfo.ident || false;
		print.top.addr = (normInfo.portSymbol+normInfo.addr) || false;
		print.top.chanel = normInfo.channel_index || false;
	} else { print.top.port = normInfo.portSpec || false; }
	
	print.top.data = info.lk.state_name;
	print.top.blambaColor = HWF.getStateColor(info.lk.state);

	if(info.lk.state == 255){print.top.onoShadow = 1;}
	if(info.lk.state == 254){print.top.onoShadow = 1;}
	
	if(normInfo.used){print.top.onofBlocked = 1;}
	if(info.state.block_local_id) {print.top.dataBot = '<p class="block_pl_40">'+HWhtml.miniLoader()+'</p>'; S.used = true;}
	
	
	print.top.systemName = info.system_name;
	print.top.systemCN = info.system_ident;

	print.top = HWhtml.equp.top(print.top);
	contentTop.innerHTML = print.top;
	HWS.buffer.share_stick(info);
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	if(S.used){used_elem()}
	// автообновление
	HWF.timerUpdate(reolad_info_page);

	// Функции нажатий и обработки для работы
	HW.on('click','equp_jurnal',function(e,el){ //Изменение Журнал 
		C.jurnal = 1;
		if(el.classList.contains('active')){
			let dataP = {token:HWS.pageAccess,id_object:id,journal:1}
			HWF.post({url:HWD.api.gets.taskFlagsToAutomatic3xDevice,data:dataP,fcnE:HWF.getServMesErr});
		}else {
			let dataP = {token:HWS.pageAccess,id_object:id,journal:0}
			HWF.post({url:HWD.api.gets.taskFlagsToAutomatic3xDevice,data:dataP,fcnE:HWF.getServMesErr});
		}
	});
	
		// В оповещать о разряде батарей
	HW.on('click','equp_battery_btn',function(e,elem){
		C.batteryBtn = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,battery_discharge_alarm:1}
			HWF.post({url:HWD.api.gets.taskFlagsToAutomatic3xDevice,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,battery_discharge_alarm:0}
			HWF.post({url:HWD.api.gets.taskFlagsToAutomatic3xDevice,data:dataP});
		}
	});	

	HW.on('click','btn_onof_relay',function(e,el){  //нажатие на реле
		if(!el.classList.contains('blocked')){
			let blockBlam = document.body.querySelector('#blamba');
			let mom = document.querySelector('#equp-content-top .page-top-dop-info');
			let tex = mom.querySelector('.data inf');
			let timeD = document.querySelector('#delay-limite-int-btn .btn-int .inp_num_val');
					if(timeD){timeD = parseInt(timeD.value) || 0;}
					else {timeD = 0;}
			let clickVal = 0;
			
			if(el.classList.contains('active')){
				clickVal = 1;
				let dataP = {token:HWS.pageAccess,id_object:id,state:clickVal,delay:timeD}
				HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
			}else {
				clickVal = 0;
				let dataP = {token:HWS.pageAccess,id_object:id,state:clickVal}
				HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
			}
			
			tex.innerHTML = normInfo.et.state[clickVal].name;
			console.log(normInfo);
			blockBlam.style.background = normInfo.et.state[clickVal].color;

			clearInterval(HWS.buffer.idTimerREoladPage);
		}
	});
	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/add_favorite_objects',data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/delete_favorite_objects',data:dataP});
		}
	});	
	
	HWS.buffer.delayTimeOntime = function(inf){
		C.typeControl = 1;
		let blockSek = HWS.getElem('#delay-limite-int-btn');
		let control = HWS.getElem('.delay_limite_btn');
		let nonstop = HWS.getElem('.delay_no_limite_btn');
		
		if(inf > 0){
			control.classList.add('active');
			nonstop.classList.remove('active');
			let dataP = {token:HWS.pageAccess,id_object:id,delay:inf}
			HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
			blockSek.classList.remove('display_none');
		}else {
			let dataP = {token:HWS.pageAccess,id_object:id,delay:0}
			HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
			control.classList.remove('active');
			nonstop.classList.add('active');
			blockSek.classList.add('display_none');
		}
	}
	
	HW.on('click','delay_limite_btn',function(e,el){
		C.typeControl = 1;
		let blockSek = HWS.getElem('#delay-limite-int-btn');
		let input = HWS.getElem('#delay-limite-int-btn .inp_num_val');
		if(input.value <= 0){input.value = 1;}
		
		blockSek.classList.remove('display_none');
	});
	
	HW.on('click','delay_no_limite_btn',function(e,el){
		C.typeControl = 1;
		let blockSek = HWS.getElem('#delay-limite-int-btn');
		let input = HWS.getElem('#delay-limite-int-btn .inp_num_val');
		let dataP = {token:HWS.pageAccess,id_object:id,delay:0}
		
		blockSek.classList.add('display_none');
		HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
		input.value = 0;
	});
	
	//Изменение времени показаний графика аналоговых
	HWS.buffer.timeControlFCNDiscret = function(H) {
		clear_graph();
		HWS.buffer.timeValueGraph = H;
		HWS.buffer.load_discret_graph({time:H})
		let dataP = {token:HWS.pageAccess,id_object:id,web_config:{time_per_graph:H}}
		HWF.post({url:HWD.api.gets.setWebConfig,data:dataP});
	}
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(info); // Проверит ваш ли это объект для вовода предупреждения
});