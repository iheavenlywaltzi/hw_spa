"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let noConnectBlock = HWS.getElem('#no-connect-block');
	let info = HWS.buffer.elem;
	let et = HWD.equp[info.info.type.type] || {};
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = info.id;
	let normInfo = HWequip.normInfo(info);
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
	HWS.buffer.elem.id_system = info.id_system;
	let C = {};
	
	HWS.buffer.equpPageSets = function(type){type = type || false;
		B.title.append('Настройки элемента оборудования');
		if(type == '1'){print_sets(info)}
		else{
			C.globalSets = 1;
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	
	HWS.buffer.equpPageJurnal = function(){
		B.title.append('Журнал элемента оборудования');
		content.innerHTML = "";
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'equpPageJurnal';
	}
	
	HWS.buffer.equpPageAccesses = function(){
		noConnectBlock.classList.remove('on');
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
		HWS.buffer.equpAccessesFCN();
	}
	
	// Функции для работы
	function print_sets(info) {
		normInfo = HWequip.normInfo(info);
		Sprint = '';
		let onof = normInfo.active; if(onof){onof = 'active';};
		let journal = normInfo.jurnal; if(journal){journal = 'active';}
		let idRelay = ''
		let name = 'Мастер-ключ';
		if(info.info.channel_index != 0) {name = 'Пользовательский ключ';}
		
		let connect = info.system_connection_status || false;
		if(!connect && info.cloud_type != 14719) {noConnectBlock.classList.add('on')}
		else {noConnectBlock.classList.remove('on');}
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Тип ключа '+HWhtml.helper(HWD.helpText.equp.btnHelpTmKey)+'</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>'+name+'</p>\
				</div>\
			</div>\
		</div>';
		
		if (normInfo.port == 12 || normInfo.port == 11 || normInfo.port == 3){
			Sprint += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin"></p>\
					<p class="btn_text grey btn_delete_equp">Удалить устройство</p>\
				</div>\
			</div>\
		</div>';
		}
			
		content.innerHTML = Sprint
		
		HWS.buffer.pageOpenControl = 'equpPageSets';
		// запуск кнопок ПРограмм
		buttonsController.start();
	}
	
	 // переодическое обновление
	function reolad_info_page(idTimer){
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(inf){
			//console.log('Автообновление ОТСУТСТВУЕТ !');
		}});
		//if(HWS.buffer.pageOpenControl == 'jurnal'){HWS.buffer.load_history('reload');}
	}
	
	let pTop = {}; 
	pTop.name = info.config.name || '';
	pTop.nameChange = 'of';
	pTop.title = et.title || 'none';
	pTop.ident = normInfo.ident || false;
	pTop.cell = normInfo.channel_index || false;
	pTop.icon = info.info.type.type || ''; // Ячейка
	
	pTop.data = 'Приложите';
	pTop.dataClass = 'stateC_1';
	pTop.meterage = '';
	pTop.stateClass = 'stateC_1';
	
	pTop.systemName = info.system_name;
	pTop.systemCN = info.system_ident;
		
	pTop = HWhtml.equp.top(pTop);
	contentTop.innerHTML = pTop;
	HWS.buffer.share_stick(info);
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	HWF.timerUpdate(reolad_info_page);
	
	
	// КНОПКИ
	//Изменение имени
	/*HW.on('change','equp_name',function(e,el){
		let name = document.querySelector('.name-page input');
		let loader = el.parentElement.querySelector('.loading-content-2');
		
		loader.classList.add('open');
		setTimeout(function(){loader.classList.remove('open');},1500);
		
		if(HWS.buffer.elem.config.name != name.value){
			let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
			HWF.post({url:HWD.api.gets.taskName3xDevice,data:dataP});
			HWS.buffer.elem.config.name = name.value;
		}
	});*/
	
	// Добавели реле
	HWS.buffer.relayAdd = function(inf){
		//let dataP = {token:HWS.pageAccess,id_object:id,id_sensor_temperature:inf.id}
		//HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	} // Удолили реле
	HWS.buffer.relayDelete = function(inf){
		//let dataP = {token:HWS.pageAccess,id_object:id,id_sensor_temperature:'delete'}
		//HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	}
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(info); // Проверит ваш ли это объект для вовода предупреждения
});