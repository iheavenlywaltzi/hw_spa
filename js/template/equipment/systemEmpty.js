"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top'); contentTop.classList.add('system3x');
	let content = document.body.querySelector('#equp-content-mid');
	let elem = HWS.buffer.elem;
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = elem.id;
	let C = {}, di= {};
	B.title.append('<h1>Система EctoControl</h1>');
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
	
	HWS.buffer.equpPageSets = function(){}
	
	di.name = elem.config.name || elem.info.name || '';
	di.imei = elem.info.ident;
	di.systemConect = elem.lk.state_name || '<span id="top-info-con-sis">'+HWD.globalText.no_connect+'</span>';
	di.systemBlamba = (elem.system_connection_status)?'bstateC3_1_5':'bstateC3_1_0';
	
	C.printTop = '\
	<div class="one-block ob-top page-equp-top">\
		<div class="name-page">\
			<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
			<input class="equp_name input-name-page" type="text" value="'+di.name+'">\
			<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>'+
			HWhtml.miniLoader('open-clouse vertical-middle')+'\
		</div>\
		<div class="info-li block_w_100 flex_center_left">\
			<div id="blamba" class="'+di.systemBlamba+'"></div>\
			<div id="iconr"><span class="icon x2 icon_equip_'+elem.info.type.type+'"></span></div>\
			<div id="top-info-block" class="page-top-dop-info block-title-name1colum">\
				<div class="data"><inf>'+di.systemConect+'</inf></div>\
			</div>\
			<div class="page-top-dop-info-spec block-title-colum">\
				<p><span class="tc_black">С/н: </span> <span class="tc_black tf_fr500">'+di.imei+'</span></p>\
			</div>\
			<div class="page-top-dop-info-title block-title-colum tf_fr500">\
				Система EctoControl\
			</div>\
		</div>\
	</div>\
	';
	
	C.printMid = '<div class="one-block">'+
		'<div>'+
			'<img src="img/noConnectSys.svg">'+
			'<div class="display_inline block_p_15">'+
				'<span>Ваша система еще не вышла на связь.<br> Настройте её по инструкции (пункты 3.1, 3.2, 4.7).</span>'+
				'<br><br>'+
				'<a href="https://ectostroy.ru/download/Polnoe_rukovodstvo_EctoControl_3.2.pdf" target="_blanck" class="btn_text grey btn_more_info">Подробнее</a></span>'+
			'</div>'+
		'</div>'+
		'<div class="ta_right">'+
			'<span class="btn_text grey btn_delete_equp">Удалить устройство</span>'+
		'</div>'+
	'</div>';
	
	// Распчатка HTML
	B.menud.append(menu);
	contentTop.innerHTML = C.printTop;
	HWS.buffer.share_stick(elem);
	content.innerHTML = C.printMid;
});