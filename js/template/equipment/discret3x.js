"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let noConnectBlock = HWS.getElem('#no-connect-block');
	let info = HWS.buffer.elem;
	let et = HWD.equp[info.info.type.type] || {};
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = info.id;
	let print = {}, S={}; 
	let normInfo = HWequip.normInfo(info);
	let C = {};
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageGraph">График</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
	HWS.buffer.pageOpenControl = 'equpPageSets';
	HWS.buffer.controlAutoReoladStoper = false;
		
	HWS.buffer.equpPageSets = function(type){type = type || false;
		HWS.buffer.pageOpenControl = 'equpPageSets';
		clear_graph()
		if(type == '1'){print_sets(info)}
		else{
			C.globalSets = 1;
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	
	HWS.buffer.equpPageGraph = function(){
		noConnectBlock.classList.remove('on');
		let nameNorm = 'Есть';
		let nameAlert = 'Нет';
		if (info.config.state_name){
			nameNorm = info.config.state_name[1] || 'Есть';
			nameAlert = info.config.state_name[0] || 'Нет';
		}
		let color2 = 'red'; 
		if (normInfo.type == 417){color2 = 'green2';};
		let legend = '<div class="graph-legend ta_center">\
			<div class="display_inline"><span class="figure square green"></span> <span>'+nameNorm+'</span></div>\
			<div class="display_inline block_pl_50"><span class="figure square '+color2+'"></span> <span>'+nameAlert+'</span></div>\
		</div>';
		
		let timeGraph = HWS.buffer.timeValueGraph || 1;
		print.graph = HWhtml.equp.graph(timeGraph,'timeControlFCNDiscret','timeControlLoadFCN','',legend);
		content.innerHTML = print.graph;
		
		clear_graph()
		
		HWS.buffer.pageOpenControl = 'equpPageGraph';
		HWS.buffer.load_discret_graph({time:timeGraph}); // запуск и отрисовка графика Статистики показаний
	}
	
	HWS.buffer.equpPageJurnal = function(){
		noConnectBlock.classList.remove('on');
		content.innerHTML = "";
		clear_graph();
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'equpPageJurnal';
	}	
	
	HWS.buffer.equpPageAccesses = function(){
		noConnectBlock.classList.remove('on');
		clear_graph()
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
		HWS.buffer.equpAccessesFCN();
	}

	function clear_graph(){
		if(!HWS.buffer.discret_graph){HWS.buffer.discret_graph = {}}; 
		if(HWS.buffer.discret_graph[id]){
			HWS.buffer.discret_graph[id].dispose();
			HWS.buffer.discret_graph[id] = null;
			HWS.buffer.controlAutoReoladStoper = true;
		}
		let blockT = HWS.getElem('#equp-statistic-graf .graph-windiw-statistic');
		if(blockT){blockT.innerHTML = HWhtml.loader();}
	}

	// Функции для работы
	function reolad_info_page(idTimer){
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcnE:HWF.getServMesErr,fcn:function(inf){
			if(!HWS.buffer.controlAutoReoladStoper){ 
				let blockData = document.body.querySelector('.page-top-dop-info .data inf');
				let blockBlam = document.body.querySelector('#blamba');
				let name = inf.lk.state_name || '';
				let normInfo = HWequip.normInfo(inf);
				let state = inf.lk.state;
				
				blockBlam.style.background = HWF.getStateColor(state)
				
				blockData.innerHTML = name;
				HWS.buffer.system_time = inf.system_time;
				HWS.buffer.elem = inf;
				
				if(HWS.buffer.pageOpenControl == 'equpPageSets' && !C.globalSets){
					let nameEqup = document.body.querySelector('#equp-content-top .name-page .equp_name');
					let act = document.body.querySelector('#equp-onof');
					let jurnal = document.body.querySelector('#equp-jurnal');
					//let delayNorm = document.body.querySelector('#btn-delay-norm .inp_num_val');
					let delayAler = document.body.querySelector('#btn-delay-alar .inp_num_val');
					let StatNorma = HWS.getElem('#state-name-norm');
					let StatAlert = HWS.getElem('#state-name-alert');
					let SnameNorm = et.normT || 'Норма';
					let SnameAlert = et.alerT || 'Тревога';
					let StatNormaT = (inf.config.state_name)?inf.config.state_name[1]:SnameNorm;
					let StatAlertT = (inf.config.state_name)?inf.config.state_name[0]:SnameAlert;
					let favorite = document.body.querySelector('#equp-viget');
					let batteryBtn = document.body.querySelector('#equp-battery');

					if(!C.favorite) {(inf.lk.favorite)?favorite.classList.add('active'):favorite.classList.remove('active');}
					if(nameEqup.value != inf.config.name && !HWS.buffer.nameFocus) {nameEqup.value = inf.config.name;}
					if(!C.actBtn) {(inf.config.flags.enabled)?act.classList.add('active'):act.classList.remove('active');}
					if(!C.jurnal) {(inf.config.flags.journal)?jurnal.classList.add('active'):jurnal.classList.remove('active');}
					if(!C.batteryBtn) {(inf.config.flags.battery_discharge_alarm)?batteryBtn.classList.add('active'):batteryBtn.classList.remove('active');}
					//if(delayNorm.value != inf.config.pm_delay_norm && !C.delayNorm) {delayNorm.value = inf.config.pm_delay_norm;}
					if(delayAler.value != inf.config.pm_delay_alarm && !C.delayAler) {delayAler.value = inf.config.pm_delay_alarm;}
					if(inf.system_info_dev > 49){
						let StatNorma = HWS.getElem('#state-name-norm');
						let StatAlert = HWS.getElem('#state-name-alert');
						if(StatNorma.value != StatNormaT && !C.StatNorma && !StatNorma.classList.contains('focus')) {StatNorma.value = StatNormaT;}
						if(StatAlert.value != StatAlertT && !C.StatAlert && !StatNorma.classList.contains('focus')) {StatAlert.value = StatAlertT;}
					}
				}
				if(HWS.buffer.pageOpenControl == 'equpPageGraph'){HWS.buffer.load_discret_graph({time:HWS.buffer.timeValueGraph});}
				info = inf;
				C = {};
				HWS.buffer.controlAutoReoladStoper = false;
			}
		}});
	}
	
		// Распечатка настроек
	function print_sets(info){
		normInfo = HWequip.normInfo(info);
		let pDat = {};
		let onof = info.config.flags.enabled; if(onof){onof = 'active';};
		let journal = info.config.flags.journal; if(journal){journal = 'active';}
		if(HWF.objKeyTest('pm_delay_alarm',info.config)){pDat.delayA = info.config.pm_delay_alarm}
		if(HWF.objKeyTest('pm_delay_norm',info.config)){pDat.delayN = info.config.pm_delay_norm}
		if(pDat.delayN){pDat.delayN = pDat.delayN || '0';}
		if(pDat.delayA){pDat.delayA = pDat.delayA || '0';}
		S.nameNorm = et.normT || 'Норма';
		S.nameAlert = et.alerT || 'Тревога';
		let StatNorma = (info.config.state_name)?info.config.state_name[1]:S.nameNorm;
		let StatAlert = (info.config.state_name)?info.config.state_name[0]:S.nameAlert;
		let favorite = (info.lk.favorite)?'active':'';
		
		let connect = info.system_connection_status || false;
		if(!connect && info.cloud_type != 14719) {noConnectBlock.classList.add('on')}
		else {noConnectBlock.classList.remove('on');}
		
		let batterySms = '', batteryBtn = '', batteryCheck = false;
		batteryCheck = HWF.objKeyTest('battery_discharge_alarm',info.config.flags);
		if(batteryCheck){
			batterySms = '';
			batteryBtn = (info.config.flags.battery_discharge_alarm)?'active':'';
		}
		else {batterySms = 'display_none'}
		
		let global = '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Активен '+HWhtml.helper(HWD.helpText.equp.btnHelpActive)+'</p>\
					<span id="equp-onof" class="btn-active on-off btn_active_inactive_equp '+onof+'" ></span>\
				</div>\
				<div class="colum">\
					<p>Журнал '+HWhtml.helper(HWD.helpText.equp.btnHelpJurnal)+'</p>\
					<span id="equp-jurnal" class="equp_jurnal btn-active on-off '+journal+'" ></span>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.equp.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+favorite+'" ></span>\
				</div>\
				<div class="colum '+batterySms+'">\
					<p>Оповещать о разряде батарей '+HWhtml.helper(HWD.helpText.equp.battareyHelper)+'</p>\
					<span id="equp-battery" class="equp_battery_btn btn-active on-off '+batteryBtn+'" ></span>\
				</div>\
			</div>\
		</div>';
		
		let timeTitle = 'сек';
		if(info.cloud_type == 737 || info.cloud_type == 738) {timeTitle = 'мин'}
		
		let mesage = '<div class="one-block-col">\
			<div class="title"><h4>Оповещения</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<!--div class="colum">\
					<p>Задержка <br> для нормы, '+timeTitle+' '+HWhtml.helper(HWD.helpText.equp.btnHelpDelayNorm)+'</p>\
					<div>'+HWhtml.btn.numb(pDat.delayN,{fcn:'clickDelayNormSensor',id:'btn-delay-norm',min:'null',max:9999})+'</div>\
				</div-->\
				<div class="colum">\
					<p>Задержка <br> для тревоги, '+timeTitle+' '+HWhtml.helper(HWD.helpText.equp.btnHelpDelayAlert)+'</p>\
					<div>'+HWhtml.btn.numb(pDat.delayA,{fcn:'clickDelayAlarmSensor',id:'btn-delay-alar',min:'null',max:9999})+'</div>\
				</div>\
			</div>\
		</div>';
		
		let state = ''
		if (info.system_info_dev <= 49){state = ''}
		else {
			state = '<div class="one-block-col">\
			<div class="title"><h4>Состояния</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="mcolum">\
					<p class="block_ptb_8">'+S.nameNorm+' '+HWhtml.helper(HWD.helpText.equp.btnHelpNameNorm)+'</p>\
					<br>\
					<p class="block_ptb_8">'+S.nameAlert+' '+HWhtml.helper(HWD.helpText.equp.btnHelpNameAlert)+'</p>\
				</div>\
				<div class="columx2">\
					<p class=""><input id="state-name-norm" class="btn_state_text_norm" type="text" value="'+StatNorma+'" placeholder="Своё" maxlength="16"></p>\
					<br>\
					<p class=""><input id="state-name-alert" class="btn_state_text_aler" type="text" value="'+StatAlert+'" placeholder="Своё" maxlength="16"></p>\
				</div>\
			</div>\
		</div>';
		}
		
		let btnDelete = '';
		if (normInfo.port == 12 || normInfo.port == 11 || normInfo.port == 3){
			btnDelete += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin"></p>\
					<p class="btn_text grey btn_delete_equp">Удалить устройство</p>\
				</div>\
			</div>\
		</div>';
		}
		
		content.innerHTML = global+mesage+state+btnDelete;
	}
	
	// верхний блок ( имя и т.д )
	print.top = {}; 
	print.top.name = info.config.name;
	print.top.title = et.title || 'none';
	print.top.icon = info.cloud_type || '';
	
	if(normInfo.port == 10 || normInfo.port == 12){
		print.top.ident = normInfo.ident || false;
		print.top.addr = (normInfo.portSymbol+normInfo.addr) || false;
		print.top.chanel = normInfo.channel_index || false;
	} else { print.top.port = normInfo.portSpec || false; }
	
	print.top.power = info.state.power.quality || 0;
	print.top.data = info.lk.state_name;
	print.top.blambaColor = HWF.getStateColor(info.lk.state);
	
	// отрисовка страници
	print.top.systemName = info.system_name;
	print.top.systemCN = info.system_ident;
	print.top = HWhtml.equp.top(print.top);
	
	contentTop.innerHTML = print.top;
	HWS.buffer.share_stick(info);
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	// авто обновление
	HWF.timerUpdate(reolad_info_page);
	
	function not_you_elem(info){
		let code = info.exceptions[0].code; // 5000010034
		let divInf =  document.createElement('div');
		if(code = 5000010034){HWF.push('Недостаточно прав','red')}
		
		divInf.classList.add('one-block-col');
		divInf.innerHTML = 'RE RE';
		content.prepend(divInf);
	}
	
	// отработка нажатий и кликов

	// Активен / не активен 
	HW.on('click','btn_active_inactive_equp',function(e,elem){
		C.actBtn = 1; let dataP = {token:HWS.pageAccess,id_object:id,enabled:1}
		if(elem.classList.contains('active')){ dataP.enabled = 1; } // On
		else { dataP.enabled = 0; } // Off
		HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
	});	
	// Журнал
	HW.on('click','equp_jurnal',function(e,elem){
		C.jurnal = 1; let dataP = {token:HWS.pageAccess,id_object:id,enabled:1}
		if(elem.classList.contains('active')){ dataP.journal = 1; } // On
		else { dataP.journal = 0; } // Off
		HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
	});	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/add_favorite_objects',data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/delete_favorite_objects',data:dataP});
		}
	});	
	// В оповещать о разряде батарей
	HW.on('click','equp_battery_btn',function(e,elem){
		C.batteryBtn = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,battery_discharge_alarm:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,battery_discharge_alarm:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	//Задержка при Норме
	HWS.buffer.clickDelayNormSensor = function(numb){
		C.delayNorm = 1;
		clearTimeout(HWS.buffer.clickDelayNormSensorTime);
		HWS.buffer.clickDelayNormSensorTime = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pm_delay_norm:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
		},2000);
	}
	//Задержка при тревоге
	HWS.buffer.clickDelayAlarmSensor = function(numb){
		C.delayAler = 1;
		clearTimeout(HWS.buffer.clickDelayAlarmSensorTimer);
		HWS.buffer.clickDelayAlarmSensorTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pm_delay_alarm:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
		},2000);
	}
	//Изменение Имени "есть\норм"
	HW.on('focusin','btn_state_text_norm',function(e,el){el.classList.add('focus')});
	HW.on('focusout','btn_state_text_norm',function(e,el){el.classList.remove('focus')});
	HW.on('change','btn_state_text_norm',function(e,el){
		C.StatNorma = 1;
		let dopElem = document.body.querySelector('.btn_state_text_aler');
		let name = []
			name.push(dopElem.value);
			name.push(el.value);
		let dataP = {token:HWS.pageAccess,id_object:id,state_name:name}
		HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
	});
	//Изменение Имени "нет\тревога"
	HW.on('focusin','btn_state_text_aler',function(e,el){el.classList.add('focus')});
	HW.on('focusout','btn_state_text_aler',function(e,el){el.classList.remove('focus')});
	HW.on('change','btn_state_text_aler',function(e,el){
		C.StatAlert = 1;
		let dopElem = document.body.querySelector('.btn_state_text_norm');
		let name = []
			name.push(el.value);
			name.push(dopElem.value);
		let dataP = {token:HWS.pageAccess,id_object:id,state_name:name}
		HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
	});
	
	//Изменение времени показаний графика аналоговых
	HWS.buffer.timeControlFCNDiscret = function(H) {
		clear_graph();
		HWS.buffer.timeValueGraph = H;
		HWS.buffer.load_discret_graph({time:H})
		let dataP = {token:HWS.pageAccess,id_object:id,web_config:{time_per_graph:H}}
		HWF.post({url:HWD.api.gets.setWebConfig,data:dataP});
	}
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(info); // Проверит ваш ли это объект для вовода предупреждения
});
