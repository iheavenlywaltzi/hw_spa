"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let noConnectBlock = HWS.getElem('#no-connect-block');
	let info = HWS.buffer.elem;
	let et = HWD.equp[info.info.type.type] || {};
	let Sprint = '';
	let token = HWS.pageAccess;
	let id = info.id;
	let normInfo = HWequip.normInfo(info);
	let C = {}; // якоря
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageGraph">График</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
	HWS.buffer.pageOpenControl = 'equpPageSets';
	HWS.buffer.controlAutoReoladStoper = false;
		
	HWS.buffer.equpPageSets = function(type){type = type || false;
		HWS.buffer.pageOpenControl = 'equpPageSets';
		clear_graph();
		B.title.append('Настройки элемента оборудования');
		if(type == '1'){print_sets(info)}
		else{
			C.globalSets = 1;
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	
	HWS.buffer.equpPageGraph = function(){
		noConnectBlock.classList.remove('on');
		
		HWS.buffer.pageOpenControl = 'equpPageGraph';
		B.title.append('График элемента оборудования');
		clear_graph();
		let timeGraph = HWS.buffer.timeValueGraph || 1;
		let graph = HWhtml.equp.graph(timeGraph,'timeControlFCNAnalog','timeControlLoadFCN','analog');
		Sprint = graph;
		content.innerHTML = Sprint
		
		HWS.buffer.load_analog_graph({time:timeGraph});
	}
	
	HWS.buffer.equpPageJurnal = function(){
		noConnectBlock.classList.remove('on');
		HWS.buffer.pageOpenControl = 'equpPageJurnal';
		clear_graph();
		B.title.append('Журнал элемента оборудования');
		content.innerHTML = "";
		HWS.buffer.load_history();
	}
	
	HWS.buffer.equpPageAccesses = function(){
		noConnectBlock.classList.remove('on');
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
		clear_graph();
		HWS.buffer.equpAccessesFCN();
	}
	// инициализация страници // верхний блок ( имя и т.д )
	let pTop = {}; 
	pTop.name = info.config.name;
	pTop.title = et.title || 'none';
	pTop.icon = info.cloud_type || '';

	if(normInfo.port == 10 || normInfo.port == 12){
		pTop.chanel = normInfo.channel_index || false;
		pTop.ident = normInfo.ident || false;
		pTop.addr = (normInfo.portSymbol+normInfo.addr) || false;
	} else { pTop.port = normInfo.portSpec || false; }
	
	pTop.blambaColor = normInfo.stateColor;
	
	pTop.connect = '';
	pTop.power = info.state.power.quality || 0;
	pTop.meterage = '';
	pTop.data = info.lk.state_name || '';
	pTop.systemName = info.system_name;
	pTop.systemCN = info.system_ident;
	
	function clear_graph(){
		if(!HWS.buffer.analog_graph){HWS.buffer.analog_graph = {}}; 
		if(HWS.buffer.analog_graph[id]){
			HWS.buffer.analog_graph[id].dispose();
			HWS.buffer.analog_graph[id] = null;
			HWS.buffer.controlAutoReoladStoper = true;
		}
		
		let blockT = HWS.getElem('#equp-statistic-graf .graph-windiw-statistic');
		if(blockT){blockT.innerHTML = HWhtml.loader();}
	}

	// Функции для работы
	function reolad_info_page(idTimer){// переодическое обновление
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(inf){
			if(!HWS.buffer.controlAutoReoladStoper){ 
				let blockData = document.body.querySelector('.page-top-dop-info .data inf');
				//let blockDataS = document.body.querySelector('.page-top-dop-info .data span');
				let blockBlam = document.body.querySelector('#blamba');
				let nameEqup = document.body.querySelector('#equp-content-top .name-page .equp_name');
				let name = info.lk.state_name;
				let normInfoNew = HWequip.normInfo(inf);
				blockBlam.style.background = HWF.getStateColor(normInfoNew.stateN);
				
				blockData.innerHTML = name;
				//blockDataS.innerHTML = '';
				if(nameEqup.value != inf.config.name && !HWS.buffer.nameFocus) {nameEqup.value = inf.config.name;}

				HWS.buffer.system_time = inf.system_time;
				if(HWS.buffer.pageOpenControl == 'equpPageSets' && !C.globalSets){
					let pravka = document.body.querySelector('#btn-pravka .inp_num_val');
					let act = document.body.querySelector('#equp-onof');
					let jurnal = document.body.querySelector('#equp-jurnal');
					let favorite = document.body.querySelector('#equp-viget');
					let delayNorm = document.body.querySelector('#btn-delay-norm .inp_num_val');
					let delayAler = document.body.querySelector('#btn-delay-alar .inp_num_val');
					let batteryBtn = document.body.querySelector('#equp-battery');
					let porog = HWS.getElem('#limit-block');
						let minP = porog.querySelector('.btn-controllers .alert_min_predel .btn-int .inp_main_val');
						let maxP = porog.querySelector('.btn-controllers .alert_max_predel .btn-int .inp_main_val');
							
					if(pravka.value != inf.config.correction && !C.pravka) {pravka.value = inf.config.correction || 0;}
					if(!C.actBtn) {(inf.config.flags.enabled)?act.classList.add('active'):act.classList.remove('active');}
					if(!C.jurnal) {(inf.config.flags.journal)?jurnal.classList.add('active'):jurnal.classList.remove('active');}
					if(!C.favorite) {(inf.lk.favorite)?favorite.classList.add('active'):favorite.classList.remove('active');}
					if(!C.batteryBtn) {(inf.config.flags.battery_discharge_alarm)?batteryBtn.classList.add('active'):batteryBtn.classList.remove('active');}
					if(delayNorm && delayNorm.value != inf.config.pm_delay_norm && !C.delayNorm) {delayNorm.value = inf.config.pm_delay_norm;}
					if(delayAler && delayAler.value != inf.config.pm_delay_alarm && !C.delayAler) {delayAler.value = inf.config.pm_delay_alarm;}
					if(!C.porog) {
						let min = parseInt(porog.dataset.min);
						let max = parseInt(porog.dataset.max);
						let kof = parseFloat((100/(max-min)).toFixed(4));
						if (minP && inf.config.pm_min != minP.value){
							let numMin = inf.config.pm_min.toFixed(1);
							let lineMin = porog.querySelector('.scale-temp .line_alt_min');
							
							lineMin.querySelector('.btn_line_move.btn-line.top.left').innerHTML = numMin;
							lineMin.style.width = (((numMin-min)*kof).toFixed(4))+'%';
							minP.value = numMin;
						}
						if (maxP && inf.config.pm_max != maxP.value){
							let numMax = inf.config.pm_max.toFixed(1);
							let lineMax = porog.querySelector('.scale-temp .line_alt_max ');
							
							lineMax.querySelector('.btn_line_move.btn-line.top.right').innerHTML = numMax;
							lineMax.style.width = (((max-numMax)*kof).toFixed(4))+'%';
							maxP.value = numMax;
						}
					}
				}
				if(HWS.buffer.pageOpenControl == 'equpPageGraph'){HWS.buffer.load_analog_graph({time:HWS.buffer.timeValueGraph});}
				if(HWS.buffer.pageOpenControl == 'equpPageJurnal'){}
				if(HWS.buffer.pageOpenControl == 'equpPageAccesses'){}
				
				inf.state.value = inf.state.value || {};
				info = inf;
				C = {}
				HWS.buffer.controlAutoReoladStoper = false;
			}
		}});
	}
	// Распечатка настроек
	function print_sets(info){
		normInfo = HWequip.normInfo(info);
		let pDat = {};
		let min = et.min || 0;
		let max = et.max || 10;
		let pravkaMin = (et.pravka)?et.pravka.min : min;
		let pravkaMax = (et.pravka)?et.pravka.max : max;
		pDat.onof = info.config.flags.enabled;
		pDat.journal = info.config.flags.journal;
		if(HWF.objKeyTest('correction',info.config)){pDat.correction = info.config.correction}
		if(HWF.objKeyTest('pm_delay_alarm',info.config)){pDat.delayA = info.config.pm_delay_alarm}
		if(HWF.objKeyTest('pm_delay_norm',info.config)){pDat.delayN = info.config.pm_delay_norm}
		let onof = pDat.onof; if(onof){onof = 'active';};
		let journal = pDat.journal; if(journal){journal = 'active';}
		let favorite = (info.lk.favorite)?'active':'';
		let pravka = HWD.unit[info.info.type.unit] || '°С';
		if(info.cloud_type == 384 || info.cloud_type == 385){pravka = info.config.unit || HWD.unit[info.info.type.unit] || '°С';}

		let connect = info.system_connection_status || false;
		if(!connect && info.cloud_type != 14719) {noConnectBlock.classList.add('on')}
		else {noConnectBlock.classList.remove('on');}
		
		let batterySms = '', batteryBtn = '', batteryCheck = false;
		batteryCheck = HWF.objKeyTest('battery_discharge_alarm',info.config.flags);
		if(batteryCheck){
			batterySms = '';
			batteryBtn = (info.config.flags.battery_discharge_alarm)?'active':'';
		}
		else {batterySms = 'display_none'}
		
		// блок Глобальные
		if(pDat.correction){pDat.correction = pDat.correction || '0'; pDat.correction = pDat.correction.toFixed(1)}
		console.log(et);
		let pravkaHtml = '<div class="colum">\
			<p>Поправка, '+pravka+' '+HWhtml.helper(HWD.helpText.equp.btnHelpAnalogPravka)+'</p>\
			<div>'+HWhtml.btn.numb(pDat.correction,{fcn:'clickPravkaSensor',id:'btn-pravka',step:0.1,tofix:1,min:pravkaMin,max:pravkaMax})+'</div>\
		</div>';
		if(info.system_info_dev == 49 || info.system_info_dev == 48){pravkaHtml = '';}
		let pSets = '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Активен '+HWhtml.helper(HWD.helpText.equp.btnHelpActive)+'</p>\
					<span id="equp-onof" class="btn-active on-off btn_active_inactive_equp '+onof+'" ></span>\
				</div>\
				<div class="colum">\
					<p>Журнал '+HWhtml.helper(HWD.helpText.equp.btnHelpJurnal)+'</p>\
					<span id="equp-jurnal" class="equp_jurnal btn-active on-off '+journal+'" ></span>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.equp.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+favorite+'" ></span>\
				</div>\
				<div class="colum '+batterySms+'">\
					<p>Оповещать о разряде батарей '+HWhtml.helper(HWD.helpText.equp.battareyHelper)+'</p>\
					<span id="equp-battery" class="equp_battery_btn btn-active on-off '+batteryBtn+'" ></span>\
				</div>\
				'+pravkaHtml+'\
			</div>\
		</div>';
		
		// блок Оповещения
		pDat.mesages = info.system_info_dev || 50;
		let pMesages = '';
		if(pDat.mesages != 49 && pDat.mesages != 48){
			if(pDat.delayN){pDat.delayN = pDat.delayN || '0';} 
			if(pDat.delayA){pDat.delayA = pDat.delayA || '0';}
			pMesages= '<div class="one-block-col">\
				<div class="title"><h4>Оповещения</h4></div>\
				<div class="ltft"></div>\
				<div class="right">\
					<!--div class="colum">\
						<p>Задержка для <br> нормы, сек '+HWhtml.helper(HWD.helpText.equp.btnHelpDelayNorm)+'</p>\
						<div>'+HWhtml.btn.numb(pDat.delayN,{fcn:'clickDelayNormSensor',id:'btn-delay-norm',min:'null',max:9999})+'</div>\
					</div-->\
					<div class="colum">\
						<p>Задержка для <br> тревоги, сек '+HWhtml.helper(HWD.helpText.equp.btnHelpDelayAlert)+'</p>\
						<div>'+HWhtml.btn.numb(pDat.delayA,{fcn:'clickDelayAlarmSensor',id:'btn-delay-alar',min:'null',max:9999})+'</div>\
					</div>\
				</div>\
			</div>';
		}

		// Пороги
		let porog = {}
		let setsLim = {};
			setsLim.warMin = {act:'of'};
			setsLim.warMax = {act:'of'};
			//setsLim.aleMin = {act:'onlyVal',val:info.config.pm_min};
			if(HWF.objKeyTest('pm_min',info.config)){setsLim.aleMin = {act:'onlyVal',val:info.config.pm_min};}
			else{setsLim.aleMin = {act:'of'};}
			if(HWF.objKeyTest('pm_max',info.config)){setsLim.aleMax = {act:'onlyVal',val:info.config.pm_max};}
			else{setsLim.aleMax = {act:'of'};}
				
		porog.class = 'one-block-col';
		porog.fcn = 'cangePorogSensor';
		porog.min = min; 
		porog.max = max;
		if(info.cloud_type == 384 || info.cloud_type == 385) {porog.min = info.config.min_measure_value; porog.max = info.config.max_measure_value;}
		porog.warMin = setsLim.warMin || 1;
		porog.warMax = setsLim.warMax || 10;
		porog.aleMin = setsLim.aleMin || 1;
		porog.aleMax = setsLim.aleMax;
		porog = HWhtml.limit(porog);
		
		let pIndication = '';
		if(info.cloud_type == 384 || info.cloud_type == 385) {
			pDat.ind4ma = info.config.min_measure_value|| 0; pDat.ind4ma = pDat.ind4ma.toFixed(1);
			pDat.ind20ma = info.config.max_measure_value || 100; pDat.ind20ma = pDat.ind20ma.toFixed(1);
			pDat.unit = info.config.unit || "%";
			pIndication = '<div class="one-block-col">\
				<div class="title"><h4>Показания</h4></div>\
				<div class="ltft"></div>\
				<div class="right">\
					<div class="colum">\
						<p>Показания при 4мА</p>\
						<div>'+HWhtml.btn.numb(pDat.ind4ma,{fcn:'clickIndication4ma',id:'btn-indication-4ma',step:0.1,tofix:1,min:-10000,max:10000})+'</div>\
					</div>\
					<div class="colum">\
						<p>Показания при 20мА</p>\
						<div>'+HWhtml.btn.numb(pDat.ind20ma,{fcn:'clickIndication20ma',id:'btn-indication-20ma',step:0.1,tofix:1,min:-10000,max:10000})+'</div>\
					</div>\
					<div class="colum">\
						<p>Единицы измерения</p>\
						<div><input class="select_unit block_w_140p" type="text" value="'+pDat.unit+'"></div>\
					</div>\
					<div class="colum">\
						<div class="btn_text grey btn_go_start_sets">По умолчанию</div>\
					</div>\
				</div>\
			</div>';
		}
		
		let btnDelete = '';
		if (normInfo.port == 12 || normInfo.port == 11 || normInfo.port == 3){
			btnDelete += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin"></p>\
					<p class="btn_text grey btn_delete_equp">Удалить устройство</p>\
				</div>\
			</div>\
		</div>';
		}
		
		Sprint = pSets+pMesages+porog+pIndication+btnDelete;
		content.innerHTML = Sprint
		
		HWequip.limit.start('.predel-scale'); // запуск работы приделов
	}
	
	//отрисовка 
	pTop = HWhtml.equp.top(pTop);
	contentTop.innerHTML = pTop
	HWS.buffer.share_stick(info);
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	HWF.timerUpdate(reolad_info_page);
	
	// Активен / не активен 
	HW.on('click','btn_active_inactive_equp',function(e,elem){
		C.actBtn = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,enabled:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,enabled:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	// Журнал
	HW.on('click','equp_jurnal',function(e,elem){
		C.jurnal = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,journal:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,journal:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/add_favorite_objects',data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/delete_favorite_objects',data:dataP});
		}
	});		
	// В оповещать о разряде батарей
	HW.on('click','equp_battery_btn',function(e,elem){
		C.batteryBtn = 1;
		if(elem.classList.contains('active')){ // On
			let dataP = {token:HWS.pageAccess,id_object:id,battery_discharge_alarm:1}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}else { // Of
			let dataP = {token:HWS.pageAccess,id_object:id,battery_discharge_alarm:0}
			HWF.post({url:HWD.api.gets.taskFlagsTo3xSensor,data:dataP});
		}
	});	
	// Сохранить юнит
	HW.on('change','select_unit',function(e,el){
		let dataP = {token:HWS.pageAccess,id_object:id,unit:el.value}
		HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
	});	
	// По умолчанию
	HW.on('click','btn_go_start_sets',function(e,el){
		let dataP = {token:HWS.pageAccess,id_object:id}
		HWF.post({url:'task/set_default_unit_current_sensor_3x',data:dataP});
	});
	
	//Изменение порогов
	HWS.buffer.cangePorogSensor = function(inf){
		C.porog = 1;
		let numb = inf.val;
		if(inf.type == 'alert_max_predel'){
			clearTimeout(HWS.buffer.clickPravkaSensorTimerMax);
			HWS.buffer.clickPravkaSensorTimerMax = setTimeout(function(){
				let dataP = {token:HWS.pageAccess,id_object:id,pm_max:numb}
				HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
			},1000);
		}			
		if(inf.type == 'alert_min_predel'){
			clearTimeout(HWS.buffer.clickPravkaSensorTimerMin);
			HWS.buffer.clickPravkaSensorTimerMin = setTimeout(function(){
				let dataP = {token:HWS.pageAccess,id_object:id,pm_min:numb}
				HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP,fcnE:HWF.getServMesErr});
			},1000);
		}
	};
	//Задержка при Норме
	HWS.buffer.clickDelayNormSensor = function(numb){
		C.delayNorm = 1;
		clearTimeout(HWS.buffer.clickDelayNormSensorTime);
		HWS.buffer.clickDelayNormSensorTime = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pm_delay_norm:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP,fcnE:HWF.getServMesErr});
		},2000);
	}
	//Задержка при тревоге
	HWS.buffer.clickDelayAlarmSensor = function(numb){
		C.delayAler = 1;
		clearTimeout(HWS.buffer.clickDelayAlarmSensorTimer);
		HWS.buffer.clickDelayAlarmSensorTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pm_delay_alarm:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP,fcnE:HWF.getServMesErr});
		},2000);
	}
	//Изменение (правки) коррекции taskParametersTo3xSensor
	HWS.buffer.clickPravkaSensor = function(numb){
		C.pravka = 1;
		clearTimeout(HWS.buffer.clickPravkaSensorTimer);
		HWS.buffer.clickPravkaSensorTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,correction:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
		},2000);
	}
	//Изменение Показания при 4мА
	HWS.buffer.clickIndication4ma = function(numb){
		C.indication4ma = 1;
		clearTimeout(HWS.buffer.indication4ma);
		HWS.buffer.indication4ma = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,min_measure_value:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
		},2000);
	}
	//Изменение Показания при 20мА
	HWS.buffer.clickIndication20ma = function(numb){
		C.indication20ma = 1;
		clearTimeout(HWS.buffer.indication20ma);
		HWS.buffer.indication20ma = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,max_measure_value:numb}
			HWF.post({url:HWD.api.gets.taskParametersTo3xSensor,data:dataP});
		},2000);
	}
	
	//Изменение времени показаний графика аналоговых
	HWS.buffer.timeControlFCNAnalog = function(H) {
		clear_graph();
		HWS.buffer.timeValueGraph = H;
		HWS.buffer.load_analog_graph({time:H,typeS:3});
		let dataP = {token:HWS.pageAccess,id_object:id,web_config:{time_per_graph:H}}
		HWF.post({url:HWD.api.gets.setWebConfig,data:dataP});
	}
	
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(info); // Проверит ваш ли это объект для вовода предупреждения
});