"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top');
	let content = document.body.querySelector('#equp-content-mid');
	let noConnectBlock = HWS.getElem('#no-connect-block');
	let info = HWS.buffer.elem;
	let et = HWD.equp[info.info.type.type] || {};
	let Sprint = {};
	let token = HWS.pageAccess;
	let id = info.id;
	let dat = {}
	let normInfo = HWequip.normInfo(info);
	let C = {}; // якоря
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageFoto">Снимки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
	HWS.buffer.elem.id_system = info.id_system;
	HWS.buffer.pageOpenControl = 'equpPageSets';
	
	// верхний блок ( имя и т.д )
	Sprint.pTop = {}; 
	Sprint.pTop.name = info.config.name;
	Sprint.pTop.title = et.title || 'none';
	Sprint.pTop.icon = info.info.type.type || '';
	
	if(normInfo.port == 10 || normInfo.port == 12){
		Sprint.pTop.addr = (normInfo.portSymbol+normInfo.addr) || false;
		Sprint.pTop.ident = normInfo.ident || false;
		Sprint.pTop.chanel = normInfo.chanel || false;
	}{ Sprint.pTop.port = normInfo.portSpec || false; }
	
	Sprint.pTop.data = normInfo.stateName;
	Sprint.pTop.blambaColor = normInfo.stateColor;
	Sprint.pTop.meterage = '';
	
	HWS.buffer.equpPageSets = function(type){type = type || false;
		B.title.append('Настройки оборудования');
		if(type == '1'){print_sets(info)}
		else{
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo); }});
		}
	}
	
	HWS.buffer.equpPageJurnal = function(){
		B.title.append('Журнал оборудования');
		content.innerHTML = "";
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'equpPageJurnal';
	}
	
	HWS.buffer.equpPageFoto = function(){
		B.title.append('Снимки с камеры');
		content.innerHTML = '<div class="one-block-col">'+HWhtml.loader()+'</div>';
		HWS.buffer.pageOpenControl = 'equpPageFoto';
		
		
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(getInf){
			//uploads
			let photos = getInf.config.photos || false;
			let print = '';
			HWS.buffer.equpPageFotoData = {};
			
			print = '<div class="one-block flex_center_top">';
			
			print+= '<div class="block_w_100 flex_between">\
				<div></div>\
				<div class="photo_select_all user-none btn_text">Всё</div>\
			</div>';
			
			print+= '<div id="phote-list" class="photo-list flex_center_top block_w_100">';
			if(photos && photos.length  > 0){ HWF.recount(photos,function(el){
				let name = el.filename;
				let url = serverURL+'uploads/'+el.file_path+'/'+el.filename;
				let idFile = el.id_file;
				
				print+= '<div class="one-phote-cam" data-id="'+idFile+'">';
				print+= '<p class="name">'+name+'</p>';
				print+= '<img class="one_photo_img pointer" src="'+url+'">';
				print+= '<span class="btn-active checkbox"></span>';
				print+= '</div>';
				
				HWS.buffer.equpPageFotoData[idFile] = {name:name,url:url}
			})} else {
				print+= '<div>Снимки отсутствуют</div>';
			}
			print+= '</div>';
			
			print+= '<div class="block_w_100 flex_between">\
				<div></div>\
				<div>\
					<span class="btn_save_phote_cam btn_text grey user-none">Сохранить помеченные</span>\
					<span class="btn_delete_phote_cam btn_text grey user-none block_ml_20">Удалить помеченные</span>\
				</div>\
			</div>'; 
			
			print+= '</div>';
			
			content.innerHTML = print;
		}});
		
		
		//select all
		HW.on('click','photo_select_all',function(event,elem){ HWF.allActive('.photo-list','.checkbox'); });
		
		// Открыть картинку в модальном окне
		HW.on('click','one_photo_img',function(e,el){
			let print = '';
			let src = el.src;
			let name = HWS.upElem(el,'.one-phote-cam').querySelector('.name').innerHTML;
			
			print+= '<div class="one-modal-phote-cam">\
				<img src="'+src+'">\
			</div>\
			<div class="ta_center block_w_100 block_ptb_20">\
				<a href="'+src+'" target="_blank" class="btn_text grey">Открыть оригинал</a>\
			</div>';
			
			HWF.modal.print(name,print);
		});
		
		// Удалить фото
		HW.on('click','btn_delete_phote_cam',function(e,el){
			let allPhoto = document.querySelectorAll('#phote-list .checkbox.active');
			
			if (allPhoto.length > 0){
				let delList = [];
				HWF.recount(allPhoto,function(oneEl){ 
					let delBlock = HWS.upElem(oneEl,'.one-phote-cam');
					let id = delBlock.dataset.id;
				
					delList.push(id);
					delBlock.remove();
					delete HWS.buffer.equpPageFotoData[id];
				},'on');
				HWF.post({url:'task/delete_photo_from_3x_camera',data:{token:token,id_object:id,photos:delList},fcn:function(inf){
					console.log(inf);
				}});
			}
		});
		
		// Скачать Фото
		HW.on('click','btn_save_phote_cam',function(e,el){
			let allPhoto = document.querySelectorAll('#phote-list .checkbox.active');
			if (allPhoto.length > 0){
				let listId = [];
				HWF.recount(allPhoto,function(oneEl){let PBlock = HWS.upElem(oneEl,'.one-phote-cam'); listId.push(PBlock.dataset.id);},'on');
				HWF.modal.print('Подготовка архива',HWhtml.loader());
				HWF.post({url:'upload/get_zip_archive',data:{token:token,id_files:listId},fcn:function(inf){
					HWF.modal.print('Подготовка архива','Ваш архив для скачивания <br><br> <a class="style_btn" href="/uploads/'+inf.data.path_with_file+'" target="_blank">Скачать</a>');
				}});
			}
			
		});
	}
	
	HWS.buffer.equpPageAccesses = function(){
		noConnectBlock.classList.remove('on');
		HWS.buffer.pageOpenControl = 'equpPageAccesses';
		HWS.buffer.equpAccessesFCN();
	}

	// Функции для работы
	function print_sets(info) {
		normInfo = HWequip.normInfo(info);
		HWS.buffer.pageOpenControl = 'equpPageSets';
		C.globalSets = 1;
		Sprint = '';
		let journal = normInfo.jurnal; if(journal){journal = 'active';}
		let idRelay = ''
		let shotProg = info.config.pf_act; 

		let Ld = {}
			Ld.shotProg = (shotProg == 255)?'':'active';
			Ld.shotprogONOF= (shotProg == 255)?'':'open';
		
			Ld.afterSensor = (info.config.flags.photo_after_sensor)?'active':'';
			Ld.rotate = (info.config.rotate)?'active':'';
			
			Ld.size = info.config.pf_size || 0;
			Ld.brightness = info.config.pf_brightness || 0;
			Ld.light = info.config.pf_light || 0;
			Ld.contrast = info.config.pf_contrast || 0;
			Ld.effect = info.config.pf_effect || 0;
			Ld.mode = info.config.pf_mode || 0;
			Ld.saturation = info.config.pf_saturation || 0;
			Ld.favorite = (info.lk.favorite)?'active':'';
		
		let connect = info.system_connection_status || false;
		if(!connect && info.cloud_type != 14719) {noConnectBlock.classList.add('on')}
		else {noConnectBlock.classList.remove('on');}
			
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Журнал '+HWhtml.helper(HWD.helpText.equp.photoCameraJurnal)+'</p>\
					<span id="equp-jurnal" class="btn_equp_jurnal btn-active on-off '+journal+'"></span>\
				</div>\
				<div class="colum">\
					<p>Снимок при тревоге '+HWhtml.helper(HWD.helpText.equp.photoCameraShotAlarm)+'</p>\
					<span class="btn_screen_alert btn-active on-off '+Ld.afterSensor+'" ></span>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.equp.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+Ld.favorite+'" ></span>\
				</div>\
				<div class="colum">\
					<p>Перевернуть кадр '+HWhtml.helper(HWD.helpText.equp.photoCameraShotReturn)+'</p>\
					<span class="btn_screen_rotate btn-active on-off '+Ld.rotate+'" ></span>\
				</div>\
				<div class="colum">\
					<p>Снимок при включении '+HWhtml.helper(HWD.helpText.equp.photoCameraShotProgram)+'<br> устройства в программе </p>\
					<span class="btn_set_prog_camera btn-active on-off '+Ld.shotProg+'" data-onof="#selected-programm-active"></span>\
				</div>\
				<div id="selected-programm-active" class="colum block-onof '+Ld.shotprogONOF+'">\
					<p><span id="program-name-selcamera" class="tw_700"></span></p>\
					<br>\
					<p><span class="btn_text grey btn_select_programm">Выбрать программу</span> '+HWhtml.helper(HWD.helpText.equp.photoCameraSelectProgram)+'</p>\
				</div>\
				<div class="block_w_100 ta_right"><span class="style_btn grey btn_screen_fono_cam">Сделать снимок</span></div>\
			</div>\
		</div>';
		
		Sprint+= '<div class="one-block-col">\
			<div class="title"><h4>Управление</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="mcolum">\
					<p class="block_ptb_9">Размер снимка '+HWhtml.helper(HWD.helpText.equp.photoCameraShotSize)+'</p><br>\
					<p class="block_ptb_9">Освещенность '+HWhtml.helper(HWD.helpText.equp.photoCameraShotLight)+'</p><br>\
					<p class="block_ptb_9">Эффект '+HWhtml.helper(HWD.helpText.equp.photoCameraShotEfect)+'</p><br>\
					<p class="block_ptb_9">Подсветка '+HWhtml.helper(HWD.helpText.equp.photoCameraLight)+'</p><br>\
					<p class="block_ptb_9">Яркость '+HWhtml.helper(HWD.helpText.equp.photoCameraBright)+'</p><br>\
					<p class="block_ptb_9">Контрастность '+HWhtml.helper(HWD.helpText.equp.photoCameraContrast)+'</p><br>\
					<p class="block_ptb_9">Насыщенность '+HWhtml.helper(HWD.helpText.equp.photoCameraSaturation)+'</p><br>\
				</div>\
				<div class="colum">\
					<div id="pf_size" data-get="pf_size">'+HWhtml.select({
						0:'176 x 144',
						1:'320 x 240',
						2:'352 x 288',
						3:'640 x 480',
						4:'800 x 600',
						5:'1024 x 768',
						6:'1280 x 1024',
						7:'1600 x 1200',
					},'btn_camera_select',Ld.size)+'</div><br>\
					<div id="pf_light" data-get="pf_light">'+HWhtml.select({
						0:'автовыбор',
						1:'солнечно',
						2:'пасмурно',
						3:'офис (люминесцентные лампы)',
						4:'дом (лампы накаливания)',
					},'btn_camera_select',Ld.light)+'</div><br>\
					<div id="pf_effect" data-get="pf_effect">'+HWhtml.select({
						0:'нормальные цвета',
						1:'антиквариат',
						2:'преобладание синего',
						3:'преобладание зеленого',
						4:'преобладание красного',
						5:'черно-белое изображение',
						6:'цветной негатив',
						7:'черно-белый негатив',
					},'btn_camera_select',Ld.effect)+'</div><br>\
					<div id="pf_mode" data-get="pf_mode">'+HWhtml.select({
						0:'вспышка отключена',
						1:'включена постоянно',
						2:'включается на момент экспозиции',
					},'btn_camera_select',Ld.mode)+'</div><br>\
					<div id="pf_brightness" data-get="pf_brightness">'+HWhtml.select({
						0:'+2',
						1:'+1',
						2:'0 (норма)',
						3:'-1',
						4:'-2',
					},'btn_camera_select',Ld.brightness)+'</div><br>\
					<div id="pf_contrast" data-get="pf_contrast">'+HWhtml.select({
						0:'+2',
						1:'+1',
						2:'0 (норма)',
						3:'-1',
						4:'-2',
					},'btn_camera_select',Ld.contrast)+'</div><br>\
					<div id="pf_saturation" data-get="pf_saturation">'+HWhtml.select({
						0:'+2',
						1:'+1',
						2:'0 (норма)',
						3:'-1',
						4:'-2',
					},'btn_camera_select',Ld.saturation)+'</div><br>\
				</div>\
			</div>\
		</div>';
		
		if (normInfo.port == 12 || normInfo.port == 11 || normInfo.port == 3){
			Sprint += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin"></p>\
					<p class="btn_text grey btn_delete_equp">Удалить устройство</p>\
				</div>\
			</div>\
		</div>';
		}
		
		content.innerHTML = Sprint
		// запуск кнопок ПРограмм
		buttonsController.start();
		
		//Доп логики
		if (Ld.shotprogONOF != 255 && info.config.program_local_id){
			let Lid = info.config.program_local_id;
			let idLocal = info.id_system
			let blockPrint = HWS.getElem('#program-name-selcamera');
				blockPrint.innerHTML = HWhtml.miniLoader();
			HWF.post({url:HWD.api.gets.objectLocalId,data:{token:token,local_id:Lid,id_system:idLocal},fcn:function(inf){
				blockPrint.innerHTML = inf.config.name;
			}});
		}
	}
	// переодическое обновление
	function reolad_info_page(idTimer){
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(inf){
			let normInfoR = HWequip.normInfo(inf);
			let blamba = HWS.getElem('#blamba');
			let stateName = HWS.getElem('.page-top-dop-info .data inf');
			
			if(HWS.buffer.pageOpenControl == 'equpPageSets' && !C.globalSets){
				let nameEqup = HWS.getElem('#equp-content-top .name-page .equp_name');
				let jurnal = HWS.getElem('#equp-jurnal');
				let screenAlert = HWS.getElem('.btn_screen_alert');
				let screenRotate = HWS.getElem('.btn_screen_rotate');
				let progCam = HWS.getElem('.btn_set_prog_camera');
				let progCamSel = HWS.getElem('#selected-programm-active'); //open
				let camSelSize = HWS.getElem('#pf_size .btn_camera_select'); 
				let camSelLight = HWS.getElem('#pf_light .btn_camera_select');
				let camSelEff = HWS.getElem('#pf_effect .btn_camera_select');
				let camSelMod = HWS.getElem('#pf_mode .btn_camera_select');
				let camSelBrig = HWS.getElem('#pf_brightness .btn_camera_select');
				let camSelcont = HWS.getElem('#pf_contrast .btn_camera_select');
				let camSelSat = HWS.getElem('#pf_saturation .btn_camera_select');
				
				if(nameEqup.value != inf.config.name && !HWS.buffer.nameFocus) {nameEqup.value = inf.config.name;}
				if(!C.jurnal) {(inf.config.flags.journal)?jurnal.classList.add('active'):jurnal.classList.remove('active');}
				if(!C.screenAlert) {(inf.config.flags.photo_after_sensor)?screenAlert.classList.add('active'):screenAlert.classList.remove('active');}
				if(!C.screenRotate) {(inf.config.rotate)?screenRotate.classList.add('active'):screenRotate.classList.remove('active');}
				if (!C.progCam && info.config.pf_act != inf.config.pf_act) { info.config.pf_act = inf.config.pf_act;
					if(inf.config.pf_act != 255){progCam.classList.add('active'); progCamSel.classList.add('open')}
					else{progCam.classList.remove('active'); progCamSel.classList.remove('open')}
				}
				
				if(!C.camSel && info.config.pf_size != inf.config.pf_size){ info.config.pf_size = inf.config.pf_size;
					camSelSize.value = inf.config.pf_size;}
				if(!C.camSel && info.config.pf_light != inf.config.pf_light){ info.config.pf_light = inf.config.pf_light;
					camSelLight.value = inf.config.pf_light;}
				if(!C.camSel && info.config.pf_effect != inf.config.pf_effect){ info.config.pf_effect = inf.config.pf_effect;
					camSelEff.value = inf.config.pf_effect;}
				if(!C.camSel && info.config.pf_mode != inf.config.pf_mode){ info.config.pf_mode = inf.config.pf_mode;
					camSelMod.value = inf.config.pf_mode;}			
				if(!C.camSel && info.config.pf_brightness != inf.config.pf_brightness){ info.config.pf_brightness = inf.config.pf_brightness;
					camSelBrig.value = inf.config.pf_brightness;}			
				if(!C.camSel && info.config.pf_contrast != inf.config.pf_contrast){ info.config.pf_contrast = inf.config.pf_contrast;
					camSelcont.value = inf.config.pf_contrast;}			
				if(!C.camSel && info.config.pf_saturation != inf.config.pf_saturation){ info.config.pf_saturation = inf.config.pf_saturation;
					camSelSat.value = inf.config.pf_saturation;}
			}
			if (HWS.buffer.pageOpenControl == 'equpPageFoto'){
				let newPhoto = inf.config.photos;
				let blockPhoto = HWS.getElem('#phote-list');
				let allPhoto = blockPhoto.querySelectorAll('.one-phote-cam');
				let SPcont = {};
				// добавление снимков
				if(newPhoto.length > 0){
					HWF.recount(newPhoto,function(el){
						let idFile = el.id_file;
						if (!HWS.buffer.equpPageFotoData[idFile]){
							let print = '';
							let name = el.filename;
							let url = serverURL+'uploads/'+el.file_path+'/'+el.filename;
							let newDiv = document.createElement('div');
								newDiv.classList.add('one-phote-cam');
								newDiv.dataset.id = idFile;
							
							print+= '<p class="name">'+name+'</p>';
							print+= '<img class="one_photo_img pointer" src="'+url+'">';
							print+= '<span class="btn-active checkbox"></span>';
							
							newDiv.innerHTML = print;
							blockPhoto.prepend(newDiv);

							HWS.buffer.equpPageFotoData[idFile] = {name:name,url:url}
						}
						SPcont[idFile] = 1;
					});
				}
				// удоление снимков,
				if (allPhoto.length > 0){
					HWF.recount(allPhoto,function(el){
						let idPhoto = el.dataset.id;
						if (!SPcont[idPhoto]){
							el.remove();
							delete HWS.buffer.equpPageFotoData[idPhoto];
						}
					},'on');
				}
			};
				
			if(normInfoR.val == 0){
				stateName.innerHTML = 'Ошибка';
				blamba.style.background = HWD.color.red;
			}else if (normInfoR.val == 1){
				stateName.innerHTML = 'Готов';
				blamba.style.background = HWD.color.green;	
			}else if (normInfoR.val == 2){
				stateName.innerHTML = 'Съёмка';
				blamba.style.background = HWD.color.orange;	
			}
			
			C = {};
		}});
	}
	
	
	// Запрос системы и отрисовка HTML 
	Sprint.pTop.systemName = info.system_name;
	Sprint.pTop.systemCN = info.system_ident;
	
	Sprint.pTop = HWhtml.equp.top(Sprint.pTop);
	contentTop.innerHTML = Sprint.pTop;
	HWS.buffer.share_stick(info);
	B.menud.append(menu);
	HWS.buffer.equpPageSets(1);
	// автообновление
	HWF.timerUpdate(reolad_info_page);
	
	HW.on('click','btn_select_programm',function(e,elem){
		HWF.modal.print('Выбор программы','<div id="spec-list-photocam-proh" class="block_w_100">'+HWhtml.loader()+'</div>');
		
		HWF.post({url:'objects/list_3x_program_objects_for_camera',data:{token:token,list_start:0,list_amount:100,id_camera:id},fcn:function(inf){
		//HWF.post({url:HWD.api.gets.list,data:{token:token,list_start:0,list_amount:100,id_system:HWS.buffer.elem.id_system,object_type:[32771,32772]},fcn:function(inf){
			let print = '';
			let printList = {};
			let blockList = HWS.getElem('#spec-list-photocam-proh');

			HWF.recount(inf.list,function(one){
				if(one.info.type.type == 32771){// Реакция на датчики
					printList[one.config.pau_alarm_num] = one;
				}
				if(one.info.type.type == 32772){// Расписание
					printList[one.config.pau_shedl_num+10] = one;
				}
			});
			
			print = '<ul class="list-programs list">';
			print+= '<li class="one-head sticky">\
						<div class="equ_n"><span class="equ_t">Тип</span><span class="name">Имя</span></div>\
						<div class="equ_b"></div>\
			</li>';
			HWF.recount(printList,function(one,key){
				let idEL = one.id || '';
				let nameEl = one.config.name || '';
				let typeEl = one.info.type.type || '';
				print+= '<li class="one-program" data-num="'+key+'" data-id="'+idEL+'">'+
					'<div class="equ_n">'+
						'<span class="equ_t"><span class="icon icon_equip_'+typeEl+'"></span></span>'+
						'<span class="name">'+nameEl+'</span>'+
					'</div>'+
					'<div class="equ_b"></div>'+
					'<div class="click btn_select_program_photo"></div>'+
				'';
			});
			print+= '</ul>';
			
			blockList.innerHTML = print;
		}});
	});

	HW.on('click','btn_select_program_photo',function(e,el){
		let li = HWS.upElem(el,'.one-program');
		let idEL = li.dataset.id;
		let name = li.querySelector('.equ_n .name').innerHTML;
		let dataP = {token:HWS.pageAccess,id_object:id,program_id_object:idEL}
		let nameProg = HWS.getElem('#program-name-selcamera');

		HWF.post({url:HWD.api.gets.taskSendParametersToCamera,data:dataP,fcn:function(inf){
			console.log(inf);
		}});
		nameProg.innerHTML = name;
		HWF.modal.message('Выбранная программ добавлена','Добавлено');
	});
	
	// Сделать снимок
	HW.on('click','btn_screen_fono_cam',function(e,el){
		let dataP = {token:token,id_object:id}
		HWF.post({url:'task/photo_from_3x_camera',data:dataP});
		HWF.modal.message('Снимок скоро появится во вкладке "снимки"','Запрос отправлен');
	});	
	
	// Журнал
	HW.on('click','btn_equp_jurnal',function(e,el){
		if(el.classList.contains('active')){ // On
			let dataP = {token:token,id_object:id,journal:1}
			HWF.post({url:HWD.api.gets.taskSendParametersToCamera,data:dataP});
		}else { // Of
			let dataP = {token:token,id_object:id,journal:0}
			HWF.post({url:HWD.api.gets.taskSendParametersToCamera,data:dataP});
		}
	});
	
	// Снимок при тревоге
	HW.on('click','btn_screen_alert',function(e,el){
		C.screenAlert = 1;
		if(el.classList.contains('active')){ // On
			let dataP = {token:token,id_object:id,photo_after_sensor:1}
			HWF.post({url:HWD.api.gets.taskSendParametersToCamera,data:dataP});
		}else { // Of
			let dataP = {token:token,id_object:id,photo_after_sensor:0}
			HWF.post({url:HWD.api.gets.taskSendParametersToCamera,data:dataP});
		}
	});
	
	// Переворот снимка
	HW.on('click','btn_screen_rotate',function(e,el){
		C.screenRotate = 1;
		if(el.classList.contains('active')){ // On
			let dataP = {token:token,id_object:id,rotate:1}
			HWF.post({url:HWD.api.gets.taskSendParametersToCamera,data:dataP});
		}else { // Of
			let dataP = {token:token,id_object:id,rotate:0}
			HWF.post({url:HWD.api.gets.taskSendParametersToCamera,data:dataP});
		}
	});	
	
	// Переворот снимка
	HW.on('click','btn_set_prog_camera',function(e,el){
		C.progCam = 1;
		if(el.classList.contains('active')){ // On

		}else { // Of
			let dataP = {token:token,id_object:id,program_id_object:'delete'}
			let blockPrint = HWS.getElem('#program-name-selcamera');
				blockPrint.innerHTML = '';
			HWF.post({url:HWD.api.gets.taskSendParametersToCamera,data:dataP});
		}
	});
	
	// Переключение селекторов 
	HW.on('change','btn_camera_select',function(e,el){
		C.camSel = 1;
		let val = el.value;
		let get = HWS.upElem(el,'div').dataset.get;
		let dataP = {token:token,id_object:id}; dataP[get] = val;
			
		HWF.post({url:HWD.api.gets.taskSendParametersToCamera,data:dataP});
	});
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(info); // Проверит ваш ли это объект для вовода предупреждения
});