"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#equp-content-top'); contentTop.classList.add('system3x');
	let content = document.body.querySelector('#equp-content-mid');
	let elem = HWS.buffer.elem;
	let et = HWD.equp[elem.info.type.type] || {};
	let token = HWS.pageAccess;
	let id = elem.id;
	let versP = et.title;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
	menu+= '<span class="btn btn_dash_click" data-fcn="equpPageGraph">График</span>';
	menu+= '<span class="btn btn_dash_click" data-fcn="equpPageJurnal">Журнал</span>';
	menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
	
	B.title.append('<h1>Система '+versP+'</h1>');
	
	//Переключение вкладок
	HWS.buffer.equpPageSets = function(type){ type = type || false;
		if(type == '1'){print_sets(elem)}
		else{
			content.innerHTML = HWhtml.loader();
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(newInfo){ print_sets(newInfo);}});
		}
		HWS.buffer.pageOpenControl = 'sets';
	}

	HWS.buffer.equpPageGraph = function(){
		
		HWS.buffer.pageOpenControl = 'graph';
	}
	
	HWS.buffer.equpPageJurnal = function(){
		content.innerHTML = "";
		HWS.buffer.load_history();
		HWS.buffer.pageOpenControl = 'jurnal';
	}	
	
	HWS.buffer.equpPageAccesses = function(){
		Sprint = '<div class="one-block ob-top conections"> <h4>Доступы</h4>';
		Sprint+= '</div>';
		content.innerHTML = Sprint;
		HWS.buffer.pageOpenControl = 'acce';
	}	
	
	// Распчатка HTML
	B.menud.append(menu);
	contentTop.innerHTML = print_top();
	HWS.buffer.equpPageSets(1);
	
	// Function
	function print_sets() {
		HWF.post({url:HWD.api.gets.list,data:{
				token:token,
				list_start:0,
				list_amount:1000,
				id_system:elem.id_system,
				local_id_object_type:0,
				//local_id_object_group:[3, 4, 5, 6],
			},
			fcn:system_mod
		});
	}
	
	function system_mod(inf){
		//console.log(inf);
		HWF.recount(inf.list,function(mod){
			if(mod.group == 3){elem.mod_ethernet = mod;} // Ethernet
			if(mod.group == 4){elem.mod_gsm = mod;} // GSM 
			if(mod.group == 5){elem.mod_wifi = mod;} // Wi-Fi
			if(mod.group == 6){elem.mod_inet = mod;} // Inet
			if(mod.group == 7){elem.mod_cloud = mod;} // cloud
		});

		content.innerHTML =(
			print_connect()+
			print_wifi()+
			print_gsm()+
			print_ethernet()+
			print_cloud()+
			print_modbus()+
			print_radio()+
			print_local()+
			print_time()+
			print_po()+
			print_dopbtn()
		);
	}
	
	
	// Распечатка верха
	function print_top(){
		let print='' , di = {} ;
		di.name = elem.config.name || elem.info.name || '';
		di.et = HWD.equp[elem.info.type.type];
		di.ver = di.et.title;
		di.imei = elem.info.ident;
		di.systemConect = (elem.system_connection_status)?'<span id="top-info-con-sis">На связи</span>':'<span id="top-info-con-sis">Нет связи</span>';
		di.systemBlamba = (elem.system_connection_status)?'bstateC3_1_5':'bstateC3_1_0';
		
		print = '\
		<div class="one-block ob-top page-equp-top">\
			<div class="name-page">\
				<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
				<input class="equp_name" type="text" value="'+di.name+'">\
				<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
			</div>\
			<div class="info-li block_w_100 flex_center_left">\
				<div id="blamba" class="'+di.systemBlamba+'"></div>\
				<div id="iconr"><span class="icon x2 icon_equip_'+elem.info.type.type+'"></span></div>\
				<div id="top-info-block" class="page-top-dop-info block-title-name1colum">\
					<div class="data"><inf>'+di.systemConect+'</inf></div>\
				</div>\
				<div class="page-top-dop-info-spec block-title-colum">\
					<p><span class="tc_black">С/н: </span> <span class="tc_black tw_700">'+di.imei+'</span></p>\
				</div>\
				<div class="page-top-dop-info-title block-title-colum tw_700">\
					Система '+di.ver+'\
				</div>\
			</div>\
		</div>\
		';
		
		return print;
	}
	// Распечатка связи
	function print_connect(){
		let cpo = {}
		
		function print_icon(name,type,lvl,helperText){
		let printIcon = '<div class="'+type+' flex_center_left block_pb_8">\
			<span class="icon icon-'+type+'-'+lvl+'"></span>\
			<span class="name vertical-middle tf_fr500 block_ml_10">'+(name || '[ none name ]')+'</span>\
			<span class="figure circle block_ml_10 block_mr_4"></span>\
			'+(helperText || '' )+'\
		</div>'
		return printIcon;
		}
		
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>Связь '+HWhtml.helper(HWD.helpText.equp.systemConnection)+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum pec-info-block">'+
					print_icon('','wifi',0,HWhtml.helper('[ none text ]'))+
					print_icon('','gsm',0,HWhtml.helper('[ none text ]'))+
					print_icon('','ethernet',1,HWhtml.helper('[ none text ]'))+
				'</div>\
				<div class="colum"></div>\
				<div class="colum">\
					<p>Последний сеанс связи</p>\
					<p>\
						<span class="tf_fr500">[[ TEXT ]], </span>\
						<span class="tf_fr500">[[ TIME ]] в [[ TIME ]]</span>\
					</p>\
				</div>\
			</div>\
		</div>';
		return print;
	}
	
	// Распечатка wifi
	function print_wifi(){
		let mod_inet = elem.mod_inet;
		let mod = elem.mod_wifi;
		let cpo = {}
			cpo.onof = (mod.config.flags.enabled_client)?"active":"";
			cpo.ssid = mod.info.scan_list || false; cpo.ssidN = {};
			cpo.ssid2 = mod.config.client.ssid || '';
			cpo.pass = mod.config.client.password || '';
			cpo.mac = mod.config.client.bssid || '';
			
			cpo.autoIP = (mod.config.flags.enabled_client_dhcp)?"active":"";
			cpo.ipAdr = mod.config.client.ip_address || '';
			cpo.mask = mod.config.client.netmask || '';
			cpo.gateway = mod.config.client.gateway || '';
			
			cpo.autoDNS = (mod.config.flags.use_name_servers_by_dhcp)?"active":"";
			cpo.DNS1 = (1 <= mod.config.client.name_servers.length) ? mod.config.client.name_servers[0] : "";
			cpo.DNS2 = (2 <= mod.config.client.name_servers.length) ? mod.config.client.name_servers[1] : "";
			
			cpo.connect = mod.state.client.connection.name || 'код '+mod.state.client.connection.code;
			cpo.inet = mod_inet.state.channel[1]; 
			cpo.inetIn = cpo.inet.inet_connection.name || 'код '+cpo.inet.inet_connection.code;
			cpo.inetLk = cpo.inet.cloud_connection.name || 'код '+cpo.inet.cloud_connection.code;
		
			if (cpo.ssid) { HWF.recount(cpo.ssid,function(obj){ cpo.ssidN[obj.channel] = obj.ssid; }); }
		
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>Wi-Fi '+HWhtml.helper(HWD.helpText.equp.systemWiFi)+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull">\
					<p class="block_pb_8">Функции Wi-Fi '+HWhtml.helper('[ none text ]')+'</p>\
					<span class="on-off btn-active '+cpo.onof+'"></span>\
				</div>\
				<div class="colum520">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Имя сети (SSID)</span> '+
						HWhtml.select(cpo.ssidN,'')+
					'</div>\
					<div class="flex_center_left block_pb_20">\
						<div class="block_w_150p"></div>\
						<div>\
							<input type="text" value="'+cpo.ssid2+'">\
							<br> <span class="btn_text grey block_ptb_8 ts_11p" >Обновить список сетей</span>\
						</div>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Пароль</span>\
						<input type="password" value="'+cpo.pass+'" placeholder="пароль сети">\
					</div>\
					<div class="flex_center_left block_pb_10">\
						<span class="block_w_150p">MAC адрес</span>\
						<input type="text" value="'+cpo.mac+'" placeholder="AC:2E:41:18">\
					</div>\
					<div class="flex_center_left block_pb_20"><span class="block_w_150p"></span><span class="btn_text grey ts_11p" >По умолчанию</span></div>\
				</div>\
				<div class="colum400">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Получать IP адрес <br> автоматически</span>\
						'+HWhtml.btn.onof(cpo.autoIP,'')+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">IP адрес</span>\
						<input type="text" value="'+cpo.ipAdr+'">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Маска подсети</span>\
						<input type="text" value="'+cpo.mask+'">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Основной шлюз</span>\
						<input type="text" value="'+cpo.gateway+'">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Получать адрес <br> DNS сервера <br> автоматически</span>\
						'+HWhtml.btn.onof(cpo.autoDNS,'')+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Предпочитаемый <br> DNS сервер</span>\
						<input type="text" value="'+cpo.DNS1+'">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Альтернативный <br> DNS сервер</span>\
						<input type="text" value="'+cpo.DNS2+'">\
					</div>\
				</div>\
				<div class="columFull">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Соединение или сеть:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.connect+'</span>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Интернет:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.inetIn+'</span>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Связь с ЛК:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.inetLk+'</span>\
					</div>\
				</div>\
				<div class="block_w_100 ta_right block_pb_30"><span class="style_btn grey">Сохранить настройки</span></div>\
			</div>\
		</div>';
		return print;
	}
	
	// Распечатка GSM
	function print_gsm(){
		let mod_inet = elem.mod_inet;
		let mod = elem.mod_gsm;
		let cpo = {}
			cpo.onof = mod.config.flags.enabled;
			cpo.voice = mod.config.flags.allowed_roaming_call;
			cpo.sms = mod.config.flags.allowed_roaming_sms;
			cpo.data = mod.config.flags.allowed_roaming_data;
			
			cpo.tarConect2 = mod.config.gprs.access_point_name || '';
			cpo.name = mod.config.gprs.user_name || '';
			cpo.pass = mod.config.gprs.password || '';
			cpo.balance = mod.config.bill.ussd || '';
			cpo.number = mod.config.bill.ussd || '';
			cpo.mesaj = 'Баланс на '+mod.info.bill.time+': '+ mod.info.bill.message; 
			cpo.SIM  = mod.info.imsi || '';
			cpo.IMEI  = mod.info.imei || '';
			cpo.oper  = mod.info.operator || '';
			cpo.telephone  = mod.config.telephone || '';
			
			cpo.connect = mod.state.gsm_connection.name || 'код '+mod.state.gsm_connection.code;
			cpo.pac = mod.state.pdn_connection.name || 'код '+mod.state.pdn_connection.code;
			cpo.inet = mod_inet.state.channel[0]; 
			cpo.inetIn = cpo.inet.inet_connection.name || 'код '+cpo.inet.inet_connection.code;
			cpo.inetLk = cpo.inet.cloud_connection.name || 'код '+cpo.inet.cloud_connection.code;
		
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>GSM '+HWhtml.helper('[ none text ]')+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="mcolum">\
					<p class="block_pb_8">Функции GSM '+HWhtml.helper('[ none text ]')+'</p>\
					'+HWhtml.btn.onof(cpo.onof,'')+'\
				</div>\
				<div class="mcolum">\
					<p class="block_pb_8">Голос в роуминге '+HWhtml.helper('[ none text ]')+'</p>\
					'+HWhtml.btn.onof(cpo.voice,'')+'\
				</div>\
				<div class="mcolum">\
					<p class="block_pb_8">SMS в роуминге '+HWhtml.helper('[ none text ]')+'</p>\
					'+HWhtml.btn.onof(cpo.sms,'')+'\
				</div>\
				<div class="mcolum">\
					<p class="block_pb_8">Данные в роуминге '+HWhtml.helper('[ none text ]')+'</p>\
					'+HWhtml.btn.onof(cpo.data,'')+'\
				</div>\
				<div class="columFull"></div>\
				<div class="colum520">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Точка доступа <br> в интернет</span> '+
						HWhtml.select({0:'Задать вручную',1:'Автоматически'},'')+
					'</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Точка доступа (APN)</span>\
						<input type="text" value="'+cpo.tarConect2+'">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Логин</span>\
						<input type="text" value="'+cpo.name+'" placeholder="пароль сети">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Пароль</span>\
						<input type="password" value="'+cpo.pass+'" placeholder="пароль сети">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Строка запроса <br> баланса</span> '+
						HWhtml.select({0:'Задать вручную',1:'Автоматически'},'')+'\
					</div>\
					<div class="flex_center_left block_pb_10">\
						<span class="block_w_150p">Запрос баланса</span>\
						<input type="text" value="'+cpo.balance+'" placeholder="">\
					</div>\
					<div class="flex_center_left block_pb_20"><span class="block_w_150p"></span><span class="btn_text grey ts_11p" >Обновить баланс</span></div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Сообщение <br> о балансе</span>\
						<div class="sms-info">'+cpo.mesaj+'</div>\
					</div>\
				</div>\
				<div class="colum400">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Абонентский <br> номер</span>\
						<input type="text" value="'+cpo.telephone+'" placeholder="">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">SIM IMSI</span>\
						<span>'+cpo.SIM+'</span>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">IMEI</span>\
						<span>'+cpo.IMEI+'</span>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Оператор</span>\
						<span>'+cpo.oper+'</span>\
					</div>\
				</div>\
				<div class="columFull">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Сеть GSM:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.connect+'</span>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Пакетные данные:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.pac+'</span>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Интернет:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.inetIn+'</span>\
					</div>\					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Связь с ЛК:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.inetLk+'</span>\
					</div>\
				</div>\
				<div class="block_w_100 ta_right block_pb_30"><span class="style_btn grey">Сохранить настройки</span></div>\
			</div>\
		</div>';
		return print;
	}
	
	// Распечатка Ethernet
	function print_ethernet(){
		let mod_inet = elem.mod_inet;
		let mod = elem.mod_ethernet;
		let cpo = {}
			cpo.onof = (mod.config.flags.enabled)?"active":"";
			cpo.onofdhcp = (mod.config.flags.enabled_dhcp)?"active":"";
			cpo.mac = mod.config.mac_address || '';
			cpo.ip = mod.config.ip_address || '';
			cpo.mask = mod.config.netmask || '';
			cpo.gateway = mod.config.gateway || '';
			
			cpo.autoDNS = (mod.config.flags.use_name_servers_by_dhcp)?"active":"";
			cpo.DNS1 = (1 <= mod.config.name_servers.length) ? mod.config.name_servers[0] : "";
			cpo.DNS2 = (2 <= mod.config.name_servers.length) ? mod.config.name_servers[1] : "";
			
			cpo.connect = mod.state.connection.name || 'код '+mod.state.connection.code;
			cpo.inet = mod_inet.state.channel[2]; 
			cpo.inetIn = cpo.inet.inet_connection.name || 'код '+cpo.inet.inet_connection.code;
			cpo.inetLk = cpo.inet.cloud_connection.name || 'код '+cpo.inet.cloud_connection.code;
		
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>Ethernet '+HWhtml.helper('[ none text ]')+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull">\
					<p class="block_pb_8">Функции Ethernet '+HWhtml.helper('[ none text ]')+'</p>\
					'+HWhtml.btn.onof(cpo.onof,'')+'\
				</div>\
				<div class="colum520">\
					<div class="flex_center_left block_pb_10">\
						<span class="block_w_150p">MAC адрес</span>\
						<input type="text" value="'+cpo.mac+'" placeholder="AC:2E:41:18">\
					</div>\
					<div class="flex_center_left block_pb_20"><span class="block_w_150p"></span><span class="btn_text grey ts_11p" >По умолчанию</span></div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Получать IP адрес <br> автоматически</span>\
						'+HWhtml.btn.onof(cpo.onofdhcp,'')+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">IP адрес</span>\
						<input type="text" value="'+cpo.ip+'">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Маска подсети</span>\
						<input type="text" value="'+cpo.mask+'">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Основной шлюз</span>\
						<input type="text" value="'+cpo.gateway+'">\
					</div>\
				</div>\
				<div class="colum400">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Получать адрес <br> DNS сервера <br> автоматически</span>\
						'+HWhtml.btn.onof(cpo.autoDNS,'')+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Предпочитаемый <br> DNS сервер</span>\
						<input type="text" value="'+cpo.DNS1+'">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Альтернативный <br> DNS сервер</span>\
						<input type="text" value="'+cpo.DNS2+'">\
					</div>\
				</div>\
				<div class="columFull">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Связь с сервером:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.connect+'</span>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Интернет:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.inetIn+'</span>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Связь с ЛК:</span>\
						<span class="figure circle block_mr_6"></span>\
						<span>'+cpo.inetLk+'</span>\
					</div>\
				</div>\
				<div class="block_w_100 ta_right block_pb_30"><span class="style_btn grey">Сохранить настройки</span></div>\
			</div>\
		</div>';
		return print;
	}
	
	// Распечатка Cloud service
	function print_cloud(){
		let mod_cloud = elem.mod_cloud;
		let mod_inet = elem.mod_inet;
		let cpo = {}
			cpo.prior1 = mod_inet.config.priorities[0];
			cpo.prior2 = mod_inet.config.priorities[1];
			cpo.prior3 = mod_inet.config.priorities[2];
			
			cpo.onof = (mod_cloud.config.flags.enabled)?"active":"";
			cpo.serHost = mod_cloud.config.api_host;
			cpo.serPort = mod_cloud.config.api_port;
			cpo.mail = mod_cloud.config.email_messages_day_limit.limit;
			cpo.mailN = mod_cloud.state.email_messages_day_limit.counter;
			cpo.push = mod_cloud.config.push_messages_day_limit.limit;
			cpo.pushN = mod_cloud.state.push_messages_day_limit.counter;
			
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>Доступ в интернет '+HWhtml.helper('[ none text ]')+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum520">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Высший приоритет</span> '+
						HWhtml.select({0:'GPRS',1:'Wi-Fi',2:'Ethernet'},'',cpo.prior1)+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Средний приоритет</span> '+
						HWhtml.select({0:'GPRS',1:'Wi-Fi',2:'Ethernet'},'',cpo.prior2)+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Низкий приоритет</span> '+
						HWhtml.select({0:'GPRS',1:'Wi-Fi',2:'Ethernet'},'',cpo.prior3)+'\
					</div>\
				</div>\
				<div class="block_w_100 ta_right block_pb_30"><span class="style_btn grey">Сохранить настройки</span></div>\
			</div>\
		</div>';
		
		print += '\
		<div class="one-block-col">\
			<div class="title"><h4>Облачный сервис '+HWhtml.helper('[ none text ]')+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull">\
					<p class="block_pb_8">Доступность сервиса '+HWhtml.helper('[ none text ]')+'</p>\
					'+HWhtml.btn.onof(cpo.onof,'')+'\
				</div>\
				<div class="columFull"></div>\
				<div class="colum400">\
					<div class="flex_center_left block_pb_10">\
						<span class="block_w_150p">Сервер / порт</span>\
						<input class="block_w_180p" type="text" value="'+cpo.serHost+'">\
						<input class="block_w_20p block_ml_6" type="text" value="'+cpo.serPort+'">\
					</div>\
					<div class="flex_center_left block_pb_20"><span class="block_w_150p"></span><span class="btn_text grey ts_11p" >По умолчанию</span></div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Суточное <br> ограничение email</span>\
						<input class="block_w_40p block_mr_8" type="text" value="'+cpo.mail+'">\
						<span>Сегодня отправлено: '+cpo.mailN+'</span>\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Суточное <br> ограничение push</span>\
						<input class="block_w_40p block_mr_8" type="text" value="'+cpo.push+'">\
						<span>Сегодня отправлено: '+cpo.pushN+'</span>\
					</div>\
				</div>\
				<div class="block_w_100 ta_right block_pb_30"><span class="style_btn grey">Сохранить настройки</span></div>\
			</div>\
		</div>';
		return print;
	}
	
	// Распечатка Сети RS-485 (Modbus)
	function print_modbus(){
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>Сети RS-485 (Modbus) '+HWhtml.helper('[ none text ]')+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum520">\
					<p class="block_pb_8">Ведущее устройство (RS1) '+HWhtml.helper('[ none text ]')+'</p>\
					'+HWhtml.btn.onof('active','')+'\
				</div>\
				<div class="colum400">\
					<div class="flex_center_left block_pb_20">\
						<p class="block_pb_8 block_w_100">Ведомое устройство (RS2) '+HWhtml.helper('[ none text ]')+'</p>\
						'+HWhtml.btn.onof('active','')+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Скорость обмена, <br> бит/с</span>\
						'+HWhtml.select(['115200','[ none info ]'],'block_w_100p')+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Адрес</span>\
						<input class="block_w_85p block_mr_8" type="text" value="">\
					</div>\
				</div>\
				<div class="block_w_100 ta_right block_pb_30"><span class="style_btn grey">Сохранить настройки</span></div>\
			</div>\
		</div>';
		return print;
	}
	
	// Распечатка Радиосеть
	function print_radio(){
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>Радиосеть '+HWhtml.helper('[ none text ]')+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum520">\
					<p class="block_pb_8">Функции радиосети '+HWhtml.helper('[ none text ]')+'</p>\
					'+HWhtml.btn.onof('active','')+'\
				</div>\
				<div class="colum400">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Ограничение <br> мощности, дБ</span>\
						'+HWhtml.select(['+20','[ none info ]'],'block_w_100p')+'\
					</div>\
				</div>\
				<div class="block_w_100 ta_right block_pb_30"><span class="style_btn grey">Сохранить настройки</span></div>\
			</div>\
		</div>';
		return print;
	}
	
	// Распечатка Локализация
	function print_local(){
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>Локализация '+HWhtml.helper('[ none text ]')+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum520">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Язык интерфейса</span>\
						'+HWhtml.select(['Русский','[ none info ]'],'')+'\
					</div>\
				</div>\
				<div class="block_w_100 ta_right block_pb_30"><span class="style_btn grey">Сохранить настройки</span></div>\
			</div>\
		</div>';
		return print;
	}
	
	// Распечатка Время и местоположение
	function print_time(){
		let mod_inet = elem.mod_inet;
		let mod = elem;
		let mod_gsm = elem.mod_gsm;
		let cpo = {}
			cpo.sinh = (mod.config.flags.sync_time)?"active":"";
			cpo.autServer = (mod_inet.config.flags.auto_ntp_servers)?"active":"";
			cpo.prior1 = mod.config.time_source_priorities[0];
			cpo.prior2 = mod.config.time_source_priorities[1];
			cpo.prior3 = mod.config.time_source_priorities[2];
			
			cpo.gps = (mod_gsm.config.flags.enabled_gps)?"active":"";
		
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>Время и местоположение</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull">\
					<p class="block_pb_8">Синхронизировать время</p>\
					'+HWhtml.btn.onof(cpo.sinh,'')+'\
				</div>\
				<div class="columFull"></div>\
				<div class="colum520">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Высший приоритет</span> '+
						HWhtml.select({0:'GPRS',1:'Wi-Fi',2:'Ethernet'},'',cpo.prior1)+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Средний приоритет</span> '+
						HWhtml.select({0:'GPRS',1:'Wi-Fi',2:'Ethernet'},'',cpo.prior2)+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Низкий приоритет</span> '+
						HWhtml.select({0:'GPRS',1:'Wi-Fi',2:'Ethernet'},'',cpo.prior3)+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Автоматически <br> выбирать сервера <br> NTP</span>\
						'+HWhtml.btn.onof(cpo.autServer,'')+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Сервер 1</span>\
						<input class="block_mr_8" type="text" value="">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Сервер 2</span>\
						<input class="block_mr_8" type="text" value="">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Сервер 3</span>\
						<input class="block_mr_8" type="text" value="">\
					</div>\
				</div>\
				<div class="colum400">\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Дата</span>\
						<input type="text" value="" class="time_val_data" onclick="xCal(this)">\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Время</span>\
						'+HWhtml.select(['14','[ none text ]'],'block_w_55p')+'\
						<span class="block_plr_8"> : </span>\
						'+HWhtml.select(['26','[ none text ]'],'block_w_55p')+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Часовой пояс</span>\
						'+HWhtml.select(['UTC +3','[ none text ]'],'')+'\
					</div>\
					<div class="flex_center_left block_pb_20">\
						<span class="block_w_150p">Использовать GPS</span>\
						'+HWhtml.btn.onof(cpo.gps,'')+'\
					</div>\
					<div class="flex_center_left block_pb_10">\
						<span class="block_w_150p">Данные GPS</span>\
						<span class="block_w_150p">[ none geo data ]</span>\
					</div>\
					<div class="flex_center_left block_pb_20"><span class="block_w_150p"></span><span class="btn_text grey ts_11p" >Карта</span></div>\
				</div>\
				<div class="block_w_100 ta_right block_pb_30"><span class="style_btn grey">Сохранить настройки</span></div>\
			</div>\
		</div>';
		return print;
	}
	
	
	// Распечатка Встроеное ПО
	function print_po(){
		let mod = elem;
		let cpo = {}
			cpo.spu_boot_fw = mod.info.spu_boot_fw.name+' : '+mod.info.spu_boot_fw.date|| '';
			cpo.spu_main_fw = mod.info.spu_main_fw.name+' : '+mod.info.spu_main_fw.date || '';
			cpo.sys_fw = mod.info.sys_fw.name+' : '+mod.info.sys_fw.date || '';
			cpo.mpu_main_fw = mod.info.mpu_main_fw.name+' : '+mod.info.mpu_main_fw.date || '';
		
		let print = '\
		<div class="one-block-col">\
			<div class="title"><h4>Встроеное ПО '+HWhtml.helper('[ none text ]')+'</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum4x">\
					<p class="block_h_50p">Загрузчик контроллера <br> переферии</p>\
					<p class="tw_700">'+cpo.spu_boot_fw+'</p>\
				</div>\
				<div class="colum4x">\
					<p class="block_h_50p">Контроллер перефирии</p>\
					<p class="tw_700">'+cpo.spu_main_fw+'</p>\
				</div>\
				<div class="colum4x">\
					<p class="block_h_50p">Версия операционной <br> системы</p>\
					<p class="tw_700">'+cpo.sys_fw+'</p>\
				</div>\
				<div class="colum4x">\
					<p class="block_h_50p">Версия сборки <br> главного ПО</p>\
					<p class="tw_700">'+cpo.mpu_main_fw+'</p>\
				</div>\
				<div class="columFull"></div>\
				<div class="columFull">\
					<p class="block_pb_15">Доступна новая прошивка <span class="figure circle block_ml_10 block_mr_4"></span></p>\
					<p class="tw_700 block_pb_8">[ none data ]</p>\
					<p class="block_pb_8"></span><span class="btn_text grey" >Подробнее</span></p>\
					<p class=""></span><span class="btn_text grey btn_update_system" >Обновить</span></p>\
				</div>\
			</div>\
		</div>';
		return print;
	}
	
	// Распечатка кнопки снизу
	function print_dopbtn(){
		let print = '<div class="one-block-col">'+
			'<div class="title"></div>'+
			'<div class="ltft"></div>'+
			'<div id="system-new-pasword" class="right">'+
				'<div class="colum">'+
					'<span class="btn_text grey btn_rebout_sets_system">Сброс настроек</span> '+
					HWhtml.helper(HWD.helpText.equp.systemReboutPass)+
				'</div>'+
				'<div class="colum">'+
					'<span class="btn_text grey btn_reset_system">Перезагрузка</span> '+
					HWhtml.helper(HWD.helpText.equp.systemReboutStstem)+
				'</div>'+
				'<div class="colum">'+
					'<p title="Нажать для установки нового оборудования" class="btn_text grey btn_ust_new_equp_system">Добавить устройства</p>'+
				'</div>'+
			'</div>'+
		'</div>';
		return print;
	}
	
	
	// Action
	//Сохранение имени
	HW.on('focusin','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"};
	});
	HW.on('focusout','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"};
	});

	HW.on('change','equp_name',function(e,el){
		//nameFocus = false;
		let name = document.body.querySelector('.name-page input');
		let nameRegex = new RegExp(HWD.reg.name);
		
		if(HWS.buffer.elem.config.name != name.value){
			if(nameRegex.test(name.value)){
				let comand = {}
					comand[elem.id_system] = [
							{
								"id": 123,
								"command": {
									"cmd_id": 2147483648,
									"targ_id": 65536,
									"tmt_sec": 0, // бросим на волю сревера
									"json": {
										"object": {
											"id": 0,
											"name": elem.config.name // core.name
										},
										"path": "name",
										"remove": false, // удалять свойство не нужно
										"value": JSON.stringify(el.value) // name - строка, и мы здесь указываем строку
									}
								}
							}
						]
				let data ={
					"token": token,
					"commands_to_system": comand,
				}

				HWF.post({url:'ecto4/send_command',data:data});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
	
	// Сброс настроек
	HW.on('click','btn_update_system',function(e,el){
		let comand = {}
			comand[elem.id_system] = [
					{
						"id": 123,
						"command": {
							"cmd_id": 2147483778,
							"targ_id": 0,
							"tmt_sec": 0, // бросим на волю сревера
							/*"json": {
								"object": {
									"id": 0,
									"name": elem.config.name // core.name
								},
							}
							*/
						}
					}
				]
		let data ={
			"token": token,
			"commands_to_system": comand,
		}

		HWF.post({url:'ecto4/send_command',data:data});
		HWS.buffer.elem.config.name = name.value;
	});
});