"use strict";
HWS.templateAdd(function(HW,B,H){
	let token = HWS.pageAccess;
	HWS.buffer.stopReolad = false;
	
	HWF.timerUpdate(reolad_info_page);
	function reolad_info_page(){HWF.post({url:'objects/list_favorite_objects',data:{token:token},fcn:reolad_info});}
	
	function reolad_info(info){ if(!HWS.buffer.stopReolad){
		let list = info.list;
		if (list.length > 0) {
			HWS.buffer.tileGraph = {};// Очистим списки старых графиков
			HWF.recount(list,function(elem){
				let normInfo = HWequip.normInfo(elem);
				let tile = HWS.getElem('#one-tile-'+normInfo.tmp+'-'+normInfo.id);
				if(tile){changer(tile,normInfo)}
				else {console.log('NO TILE')}
			});
			HWS.buffer.loaderGraph(HWS.buffer.tileGraph);// запуск обновлени графиков
		}
		else {
			let mainBlock = document.querySelector('.fast-menu');
				mainBlock.innerHTML = '<div id="no-tile-vijet" class="flex_center">\
					<div>\
						<p class="block_pb_20"><img src="img/add_vijet_new.png"></p>\
						<p class="block_pb_10 ts_24p tf_mb tc_greyС9">Добавьте виджет!</p>\
						<p class="tc_greyС9">У вас пока нет виджетов, добавьте <br> ваш первый виджет, нажав на кнопку <br> в правом верхнем углу экрана.</p>\
					</div>\
				</div>';
		}
		//console.log('reolad');
	} else {HWS.buffer.stopReolad = false;} }
	
	function changer(tile,info){
		var inf = {};
		if(info.name.length > 30){inf.nameN = info.name.substr(0, 25); inf.nameN = inf.nameN+'...';}
		else {inf.nameN = info.name}
		let connectBlock = tile.querySelector('.shadov-block');
		
		if(info.elem.system_connection_status){connectBlock.classList.remove('on')}
		else {connectBlock.classList.add('on')}
		
		// ОБорудование
		if(info.tmp == 'analog3x'){
			let name = tile.querySelector('header .left .name');
			let blamba = tile.querySelector('section .blamba');
			let data = tile.querySelector('section .state-info');
			let unit = tile.querySelector('section .state-unit');
			let elData = info.elem.lk.state_name || '';
			inf.blambaColor = HWF.getStateColor(info.elem.lk.state)|| info.stateColor;

			name.innerHTML = inf.nameN || '';
			blamba.style.background = inf.blambaColor || '';
			data.innerHTML = elData || ''; 
			//unit.innerHTML = info.unit || '';
			// добавим в график для обновления
			HWS.buffer.tileGraph[info.id] = {type:info.tmp,system:info.elem.id_system}; 
		}
		
		if(info.tmp == 'discret3x'){
			let name = tile.querySelector('header .left .name');
			let data = tile.querySelector('section .state-info');
			let blamba = tile.querySelector('section .blamba');

			name.innerHTML = inf.nameN || '';
			data.innerHTML = info.elem.lk.state_name || ''; 
			blamba.style.background = HWF.getStateColor(info.elem.lk.state) || '';
			// добавим в график для обновления
			HWS.buffer.tileGraph[info.id] = {type:info.tmp,system:info.elem.id_system}; 
		}
		
		if(info.tmp == 'control_device3x'){
			let O = {}
			let name = tile.querySelector('header .left .name');
			let blamba = tile.querySelector('section .blamba-btn');
			let data = tile.querySelector('section .state-info');
			let dataDopText = tile.querySelector('section .dop-state-info');
			let usedText = tile.querySelector('section .used-program');
			
			O.data = info.elem.lk.state_name || '';
			inf.timeRelayOn = '';

			if (info.val == 1){blamba.classList.add('active')}
			if (info.val == 0){blamba.classList.remove('active')}
			if (info.val == 255){blamba.classList.remove('active'); O.used = 1;}
			if(info.elem.state.block_local_id){O.used_by = HWD.globalText.used_by;}
			if(info.elem.lk.control_locked){O.used=1;}
			if(info.elem.config.delay_selected){inf.timeRelayOn = info.elem.config.delay_lk || '0'; inf.timeRelayOn = inf.timeRelayOn+' сек'}

			name.innerHTML = inf.nameN || '';
			data.innerHTML = O.data || ''; 
			usedText.innerHTML = O.used_by || '';
			dataDopText.innerHTML = inf.timeRelayOn || '';
			if (O.used){blamba.classList.remove('btn-active'); blamba.classList.add('used');}
			else {blamba.classList.add('btn-active'); blamba.classList.remove('used');}
			// добавим в график для обновления
			HWS.buffer.tileGraph[info.id] = {type:info.tmp,system:info.elem.id_system};
		}
		
		if(info.tmp == 'foto_camera3x'){
			let name = tile.querySelector('header .left .name');
			let data = tile.querySelector('section .state-info');
			let blamba = tile.querySelector('section .blamba');
			
			if (info.val == 0){inf.data = 'Ошибка'; inf.BlambaColor = HWD.color.red}
			if (info.val == 1){inf.data = 'Готов'; inf.BlambaColor = HWD.color.green}
			if (info.val == 2){inf.data = 'Съёмка'; inf.BlambaColor = HWD.color.orange}
			
			name.innerHTML = inf.nameN || '';
			data.innerHTML = inf.data || ''; 
			blamba.style.background = inf.BlambaColor || '';
		}
		
		// Программы
		if(info.tmp == 'notofications'){} // Оповещение
			
		if(info.tmp == 'security'){ // Охрана
			let name = tile.querySelector('header .left .name');
			let data = tile.querySelector('section .state-info');
			let btn = tile.querySelector('section .btn_security_vijet_click');
			let icon = tile.querySelector('section .state .icon-block .icon ');
			
			inf.state = (info.elem.config.flags.security_active)?1:0;
			inf.stateNTop = (inf.state)?'Снять с охраны':'Поставить на охрану';
			if (inf.state){
				btn.classList.add('active');
				icon.classList.add('green');
			} 
			else {
				btn.classList.remove('active');
				icon.classList.remove('green');
			}
			inf.data = info.elem.lk.state_name || '';

			name.innerHTML = inf.nameN || '';
			data.innerHTML = inf.data || ''; 
			btn.innerHTML = inf.stateNTop || '';
		}
		
		if(info.tmp == 'heating'){
			let O = {}
			O.name = tile.querySelector('header .left .name');
			O.btnEcon = tile.querySelector('section .state .btn_heating_click_tmp.eco');
			O.btnEconVal = info.elem.config.pau_therm_pre_eco || 0;
			O.btnStan = tile.querySelector('section .state .btn_heating_click_tmp.standart');
			O.btnStanVal = info.elem.config.pau_therm_pre_std || 0;
			O.btnComf = tile.querySelector('section .state .btn_heating_click_tmp.komfort');
			O.btnComfVal = info.elem.config.pau_therm_pre_comf || 0;
			O.btnCast = tile.querySelector('section .state .ta_center .btn-int'); //green   grey
			O.btnCastVal = info.elem.config.pau_therm_pre_usr || 0;
			O.btnTabl = tile.querySelector('section .state .btn_heating_click_timetable');
			
			O.current = tile.querySelector('section .ta_center .current-tmp'); // current-tmp
			O.iconInfo = tile.querySelector('section .ta_center .icon_info'); //icon-info
			O.iconRadiator = tile.querySelector('section .ta_center .icon-radiator'); //icon-radiator
			O.iconFaucet = tile.querySelector('section .ta_center .icon-faucet'); //icon-faucet
			O.iconText = tile.querySelector('section .ta_center .icon-text'); //icon-info
			O.Supported = tile.querySelector('section .ta_center .tmp_correction'); // tmp_correction
			O.state = tile.querySelector('section .state');
			
			O.temp =(info.elem.config.sensor_temperature)?HWequip.normInfo(info.elem.config.sensor_temperature):'';
			O.onof =(info.elem.config.boil_device)?HWequip.normInfo(info.elem.config.boil_device):'';
			
			function active_btn(typ){
				(typ == 'eco')?O.btnEcon.classList.add('active'):O.btnEcon.classList.remove('active');
				(typ == 'standart')?O.btnStan.classList.add('active'):O.btnStan.classList.remove('active');
				(typ == 'komfort')?O.btnComf.classList.add('active'):O.btnComf.classList.remove('active');
				(typ == 'timetable')?O.btnTabl.classList.add('active'):O.btnTabl.classList.remove('active');
				if(typ == 'custom') {O.btnCast.classList.add('green'); O.btnCast.classList.remove('grey');}
				else {O.btnCast.classList.add('grey'); O.btnCast.classList.remove('green');}
			}
			
			if(info.elem.config.flags.control_from_schedule){
				active_btn('timetable');
			}
			else{
				if(info.elem.config.pau_therm_value == info.elem.config.pau_therm_pre_eco){active_btn('eco')}
				else if (info.elem.config.pau_therm_value == info.elem.config.pau_therm_pre_std){active_btn('standart')}
				else if (info.elem.config.pau_therm_value == info.elem.config.pau_therm_pre_comf){active_btn('komfort')}
				else {active_btn('custom')}
			}
			
			if(O.temp && O.temp.stateN != 255) {O.current.innerHTML = O.temp.stateName;}
			else {O.current.innerHTML = 'Нет t°'}
			
			O.onofText = info.elem.lk.state_name || '';
			O.iconCol =  HWF.getStateColor(info.elem.lk.state);
			if(O.iconCol == '#D3D4D3') {O.iconCol = HWD.color.grey2;}
			O.flameColor = (O.onof && O.onof.elem.state.flame)?HWD.color.blue:HWD.color.grey;

			if(O.onof && !O.temp || info.stateN == 112 || info.stateN == 11){			
				O.iconInfo.dataset.text = 'В программе отсутствует датчик температуры или связь с ним';
				O.iconInfo.classList.remove('p20');
				O.iconInfo.classList.remove('icon-programs-flame');
				O.iconInfo.classList.add('icon-alarm');
				O.iconInfo.classList.add('helper');
				//O.iconInfo.classList.add('p32');
			} else {
				O.iconInfo.dataset.text = '';
				O.iconInfo.classList.remove('icon-alarm');
				O.iconInfo.classList.remove('helper');
				//O.iconInfo.classList.remove('p32');
				O.iconInfo.classList.add('p20');
				O.iconInfo.classList.add('icon-programs-flame');
			}
			
			if(O.onof && O.onof.tmp == "adapter_boiler3x"){
				let S = {};
				O.flameColor = (O.onof.elem.state.flame)?HWD.color.blue:HWD.color.grey;
				S.radiatorColor = (O.onof.elem.state.channel_1)?HWD.color.blue:HWD.color.grey;
				S.faucetColor = (O.onof.elem.state.heat_water)?HWD.color.blue:HWD.color.grey;

				O.iconInfo.style.background = O.flameColor;
				O.iconRadiator.style.background = S.radiatorColor;
				O.iconFaucet.style.background = S.faucetColor;				
				O.iconRadiator.classList.remove('display_none');
				O.iconFaucet.classList.remove('display_none');
			} else {O.iconRadiator.classList.add('display_none'); O.iconFaucet.classList.add('display_none');}
			if(info.elem.config.flags.active){O.state.classList.remove('manual-mod')}
			else {O.state.classList.add('manual-mod')}
			
			O.name.innerHTML = inf.nameN || '';
			O.btnEcon.innerHTML = 'Эконом: '+O.btnEconVal+'°'; O.btnEcon.dataset.val = O.btnEconVal;
			O.btnStan.innerHTML = 'Стандарт: '+O.btnStanVal+'°'; O.btnEcon.dataset.val = O.btnStanVal;
			O.btnComf.innerHTML = 'Комфорт: '+O.btnComfVal+'°'; O.btnEcon.dataset.val = O.btnComfVal;
			O.btnCast.querySelector('.inp_num_val').value = O.btnCastVal;
			
			O.iconInfo.style.background = O.flameColor;
			O.iconText.innerHTML = O.onofText;
			O.iconText.style.color = O.iconCol;		
			O.Supported.innerHTML =	info.elem.config.pau_therm_value.toFixed(1);
		}
		
		if(info.tmp == 'alarm'){ // Реакция на датчики
			let name = tile.querySelector('header .left .name');
			let data = tile.querySelector('section .state-info');
			let blamba = tile.querySelector('section .blamba');
			let btn = tile.querySelector('section .style_btn');
			let manualView = tile.querySelector('section .manual_mod_target ');
			let S = {};
			inf.device = (info.elem.config.automatic_device)?HWequip.normInfo(info.elem.config.automatic_device):'';
			
			S.delay = info.elem.config.pau_alarm_delay;  // Время управления
			if(S.delay == 65535){btn.classList.remove('display_none')}
			else {btn.classList.add('display_none')}
			
			if(inf.device){
				btn.classList.add('btn_rebout_relay_prog_alarm');
				btn.classList.add('hover-green');
				btn.classList.add('active');
				btn.classList.remove('Bgrey');
				btn.classList.remove('nocursor');
			} else {
				btn.classList.remove('btn_rebout_relay_prog_alarm');
				btn.classList.remove('hover-green');
				btn.classList.remove('active');
				btn.classList.add('Bgrey');
				btn.classList.add('nocursor');
			}
			if(info.elem.config.flags.active){manualView.classList.remove('block')}
			else {manualView.classList.add('block')}

			inf.data = info.elem.lk.state_name || '';
			inf.BlambaColor = HWF.getStateColor(info.elem.lk.state);

			name.innerHTML = inf.nameN || '';
			data.innerHTML = inf.data || ''; 
			blamba.style.background = inf.BlambaColor || '';
		}
		
		if(info.tmp == 'sheduler'){ // Расписание
			let name = tile.querySelector('header .left .name');
			let data = tile.querySelector('section .state-info');
			let blamba = tile.querySelector('section .blamba');
			let manualView = tile.querySelector('section');
			
			inf.device = (info.elem.config.automatic_device)?HWequip.normInfo(info.elem.config.automatic_device):'';
			
			if(inf.device){	HWS.buffer.tileGraph[info.id] = {type:inf.device.tmp,system:info.elem.id_system,id:inf.device.id}; }
			if(info.elem.config.flags.active){manualView.classList.remove('manual-mod')}
			else {manualView.classList.add('manual-mod')}
			inf.data = info.elem.lk.state_name || '';
			inf.BlambaColor = HWF.getStateColor(info.elem.lk.state);
			
			name.innerHTML = inf.nameN || '';
			data.innerHTML = inf.data || ''; 
			blamba.style.background = inf.BlambaColor || '';
		}
		
		// Адаптер котла
		if(info.tmp == 'adapter_boiler3x'){
			let O = {};
			let name = tile.querySelector('header .left .name');
			let data = tile.querySelector('section .state-info');
			let blamba = tile.querySelector('section .blamba');
			let errorBlock = tile.querySelector('section .error-block');
			let temp = tile.querySelector('section .data_info_temp');
			let bar = tile.querySelector('section .data_info_bar');
			let gvs = tile.querySelector('section .data_info_gvs');
			let usedText = tile.querySelector('section .used-program');
			
			inf.data = info.elem.lk.state_name || '';
			inf.BlambaColor = HWF.getStateColor(info.elem.lk.state);
			
			inf.temp = info.elem.state.coolant_temperature || ''; inf.temp = (inf.temp == 32766 || inf.temp == 32767)?'':inf.temp;
			inf.bar = info.elem.state.pressure || ''; inf.bar = (inf.bar == 255)?'':inf.bar;
			inf.gvs = info.elem.state.hot_water_supply_temperature || ''; inf.gvs = (inf.gvs == 32766 || inf.gvs == 32767)?'':inf.gvs;
			inf.error = info.elem.error_description;
			
			if(info.elem.lk.state == 11){inf.specColorAdapter = 'tc_grey';}
			else {inf.specColorAdapter = '';}
						
			if(inf.error.status){
				inf.errorBlock = '<span>Ошибка:</span>  <span class="tw_700 tc_red tf_mb ts_18p">'+inf.error.code+'</span> <span class="btn_text grey btn_modal_adapter_boiler">подробнее</span>';
				HWS.buffer.adapterBoiler[info.id] = {details:inf.error.details, common:inf.error.common}
			}
			else {inf.errorBlock = ''}
			
			if(!info.elem.state.block_local_id){} else {O.used_by = HWD.globalText.used_by;}
			
			inf.tempTeploHtml = (inf.temp != '')?'<span class="tw_700 tc_black tf_mb ts_16p '+inf.specColorAdapter+'"><span class="temp-carier">'+inf.temp+'</span> °</span><br><span class="tc_grey6">Теплоноситель</span>':'';
			inf.tempBarHtml = (inf.bar != '')?'<span class="tw_700 tc_black tf_mb ts_16p '+inf.specColorAdapter+'"><span class="bar-carier">'+inf.bar+'</span> Бар</span><br><span class="tc_grey6">Давление</span>':'';
			inf.tempGvsHtml = (inf.gvs != '')?'<span class="tw_700 tc_black tf_mb ts_16p '+inf.specColorAdapter+'"><span class="gvs-carier">'+inf.gvs+'</span> °</span><br><span class="tc_grey6">ГВС </span>':'';
		
			name.innerHTML = inf.nameN || '';
			data.innerHTML = inf.data || ''; 
			temp.innerHTML = inf.tempTeploHtml || ''; 
			bar.innerHTML = inf.tempBarHtml || ''; 
			gvs.innerHTML = inf.tempGvsHtml || ''; 
			errorBlock.innerHTML = inf.errorBlock || ''; 
			blamba.style.background = inf.BlambaColor || '';
			usedText.innerHTML = O.used_by || '';
		}
		// Система
		if(info.tmp == 'system3x'){
			let name = tile.querySelector('header .left .name');
			let data = tile.querySelector('section .state-info');
			let blamba = tile.querySelector('section .blamba');
			let conection = tile.querySelector('section .conection');
			let smsInfo = tile.querySelector('section .mesage');
			let smsInfoText = tile.querySelector('section .mesage .smsinfo');
			
			let system_connection = info.elem.system_connection;
			let printIcon = '';
			
			inf.data = info.elem.lk.state_name || '';
			inf.BlambaColor = HWF.getStateColor(info.elem.lk.state);
			inf.cfgLk = info.elem.config.cfg_lk;

			HWF.recount(system_connection,function(eleC,key){
				let type = eleC.type, signal=eleC.level, clasStatus = eleC.use_inet, cfgLk=info.elem.config.cfg_lk;
				if(clasStatus == 'connect'){clasStatus = 'green';}
				if(clasStatus == 'disconnect'){clasStatus = 'red';}
				if(clasStatus == 'not_use'){clasStatus = 'of';}
				let circle = '<span class="spec-circle-icon-state '+clasStatus+'"></span>';
				let iconColor = '',textType = 'основной';
				let nameConect = (type == 'wifi')?'Wi-Fi':"GSM";
				let roam = (eleC.roam)?'-roam':'';
				
				if(cfgLk == 1 && type == 'wifi'){circle=''; iconColor = 'grey'}
				if(cfgLk == 2 && type == 'gsm'){circle=''; iconColor = 'grey'}
				if(key == 1 && (inf.cfgLk == 3 || inf.cfgLk == 4)){textType = 'резервный';}
				else { (key != 0)?textType='<span class="block_w_55p"></span>':''; }
				if(eleC.info_bal && (type == 'gsm_roam' || type == 'gsm')){inf.smstext = eleC.info_bal || ''; smsInfoText.classList.add('open');}
				else {smsInfoText.classList.remove('open');}
				let print = '\
					<div class="'+type+' flex_center_left">\
						<span class="select-none position_relative block_mr_10">\
							'+circle+'\
							<span class="icon icon-'+type+roam+'-'+signal+' '+iconColor+'"></span>\
						</span>\
						<span class="block_pr_8">'+nameConect+'</span>\
						<span class="ts_11p tc_greyA5">'+textType+'</span>\
					</div>';
				
				printIcon+= print;
			});			

			name.innerHTML = inf.nameN || '';
			data.innerHTML = inf.data || ''; 
			blamba.style.background = inf.BlambaColor || '';
			conection.innerHTML = printIcon || ''; 
			smsInfoText.innerHTML = inf.smstext || ''; 
		}
	};
});