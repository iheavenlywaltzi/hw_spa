"use strict";
HWS.templateAdd(function(HW,B,H){
	let infoBlock = HWS.buffer.infoBlock;
	let GodToken = localStorage.getItem('PITn0Hxg') || false;
	let print = '';
	
	let dstat = {}
	let data = {token:GodToken}
	HWF.post({url:'admin_statistic/info',data:data,fcnE:HWF.getServMesErr,fcn:load});
	
	infoBlock.mid.innerHTML = HWhtml.loader();
	
	//load(HWD.demoAdmin.stat);
	
	// ** FCN ** 
	function load(inf){
		let print = '';
		let dopInfo = '';
		let map = '';
		let graph = '';
		
		dstat.sysall = inf.count_systems;
		dstat.syson = inf.count_online_systems;
		dstat.sysof = dstat.sysall - dstat.syson;
		dstat.userall = inf.count_users;
		dstat.useron = inf.count_online_users;
		//dstat.mapSon = inf.map.online;
		//dstat.mapSof = inf.map.offline;
		dstat.tsys = inf.count_type_systems;
		//dstat.graph = inf.graph;
		
		dopInfo = '<div class="flex_between block_pb_10 border_bot">'+
			'<div class="block-p-8">'+
				'<span> Систем зарегистрировано: <b>'+dstat.sysall+'</b> </span><br>'+
				'<span> Систем онлайн: <b>'+dstat.syson+'</b> </span>'+
			'</div>'+
			'<div class="block-p-8">'+
				'<span> Пользователей всего: <b>'+dstat.userall+'</b> </span><br>'+
				'<span> Пользователей онлайн: <b>'+dstat.useron+'</b> </span>'+
			'</div>'+
		'</div>';
		
		map = '<div class="admin-user-map-info block_pb_10 border_bot">'+
			'<div class="block-p-8">'+
				'<span class="btn-active user-none block-p-8 pointer wiev_system_map_online" data-targetdown="checkbox"><checkbox class="mini select-none"></checkbox>На связи</span>'+
				'<span class="btn-active user-none block-p-8 pointer wiev_system_map_ofline" data-targetdown="checkbox"><checkbox class="mini select-none"></checkbox>Нет связи</span>'+
			'</div>'+
			'<div id="admin-system-wiev-map">'+HWhtml.loader()+'</div>'+
		'</div>';
		
		graph = '<div class="flex_between">'+
			'<div class="admin-user-diagram">'+
				'<header>Системы</header>'+
				'<section><canvas id="canvas-system-onofline"></canvas></section>'+
			'</div>'+
			'<div class="admin-user-diagram">'+
				'<header>Все системы</header>'+
				'<section><canvas id="canvas-system-type"></canvas></section>'+
			'</div>'+
			'<div class="admin-user-graph">'+
				'<header>'+
					'<p>Аккаунты. Общее колво акаунтов</p>'+
					'<p>Системы. Общее колво систем</p>'+
					'<p>Онлайн. Общее колво онлайн систем</p>'+
					'<p>Период: '+
					HWhtml.select(['Неделя','Месяц','Пол Года','Год','Всё'],'admin_static_graf')+
					HWhtml.miniLoader('s32 open-clouse')+
					'</p>'+
				'</header>'+
				'<section><canvas id="canvas-time-info-graph"></canvas></section>'+
			'</div>'+
		'</div>';
		
		print = dopInfo+map+graph;
		infoBlock.mid.innerHTML = print;
		load_map();
		load_graf();
	}
	
	function load_map(){
		HWS.buffer.mapSystemOnline = 0;
		HWS.buffer.mapSystemOffline = 0;
		HWS.buffer.mapAllPins = [];
		document.body.querySelector('#admin-system-wiev-map').innerHTML = '';
		let wievMap = {lat:60.539811,lng:102.2617187,zoom:4};
		
		let map = L.map('admin-system-wiev-map');
			map.options.crs = L.CRS.EPSG3395;
			map.setView([wievMap.lat,wievMap.lng],wievMap.zoom);
			
		HWS.buffer.map = map;
		HWS.buffer.mapSon = [];
		HWS.buffer.mapSof = [];
		
		L.tileLayer(
		  'https://vec{s}.maps.yandex.net/tiles?l=map&v=4.55.2&z={z}&x={x}&y={y}&scale=2&lang=ru_RU', {
			subdomains:['01', '02', '03', '04'],
			attribution:'<a http="yandex.ru" target="_blank">Яндекс</a>',
			reuseTiles:true,
			updateWhenIdle:false,
		  }
		).addTo(map);
		
		map_set_pin();
		map.on('moveend',map_set_pin);
		
		/*
		let mapSon = dstat.mapSon;
		let mapSof = dstat.mapSof;
		
		if(mapSon){for (let key in mapSon){
			let div = L.divIcon({className:'ecto-pin-map', html:HWhtml.pinAdmin('on')});
			let marcer = L.marker([mapSon[key].lat,mapSon[key].lng],{icon:div});
			HWS.buffer.mapSon.push(marcer);
		}}
		
		if(mapSof){for (let key in mapSof){
			let div = L.divIcon({className:'ecto-pin-map', html:HWhtml.pinAdmin('of')});
			let marcer = L.marker([mapSof[key].lat,mapSof[key].lng],{icon:div});
			HWS.buffer.mapSof.push(marcer);
		}}
		*/
	}
	
	function load_graf(){
		let graph1_1 = {}
		graph1_1.data = {
			datasets: [{data:[dstat.syson,dstat.sysof],backgroundColor: ["#1ab148","#b3b3b3"]}],
			labels:[dstat.syson+' На связи',dstat.sysof+' Нет связи']
		};
		
		let graph1_2 = {}
		graph1_2.data = {
			datasets: [{data:[dstat.tsys[30],dstat.tsys[31],dstat.tsys[32],dstat.tsys[40]],backgroundColor: ["#dddddd","#bbbbbb",'#ffae00','#ffe5ae']}],
			labels:['3.0/ '+dstat.tsys[30],'3.1/ '+dstat.tsys[31],'3.2/ '+dstat.tsys[32],'4.0/ '+dstat.tsys[40]]
		};
		/*				
		let graph2 = {}
		graph2.legend = {display:true, align:"start", position:'bottom',  labels: {usePointStyle: true} };
		graph2.data = [];
		graph2.data.push(HWG.fcn.create_date_analog({id:1,label:'Акаунты', color:"#f00000", lineTension:'of', data:dstat.graph.acaunt}));
		graph2.data.push(HWG.fcn.create_date_analog({id:2,label:'Системы', color:"#ffae00", lineTension:'of', data:dstat.graph.system}));
		graph2.data.push(HWG.fcn.create_date_analog({id:3,label:'Онлайн', color:"#1ab148", lineTension:'of', data:dstat.graph.online}));
		*/
		HWG.create.adminDoughnut('#canvas-system-onofline',graph1_1);
		HWG.create.adminDoughnut('#canvas-system-type',graph1_2);
		//HWS.buffer.graph = HWG.create.graphTime('#canvas-time-info-graph',graph2);
	}
	
	function map_set_pin(){
		let bounds = HWS.buffer.map.getBounds();
		let data = {
			token:GodToken,
			northEast_lat:bounds._northEast.lat, 
			northEast_lng:bounds._northEast.lng, 
			southWest_lat:bounds._southWest.lat, 
			southWest_lng:bounds._southWest.lng,
			systems_online:HWS.buffer.mapSystemOnline,
			systems_offline:HWS.buffer.mapSystemOffline
		}
		HWF.post({url:'admin_statistic/get_map_points',data:data,fcnE:HWF.getServMesErr,fcn:function(inf){
			let list = inf.systems;
			// удалим пины
			HWF.recount(HWS.buffer.mapAllPins,function(elem){HWS.buffer.map.removeLayer(elem);});
			// создадим новые, и добавим их накарут
			HWF.recount(list,function(pin){
				let div = '';
				let	lat = pin.coordinate[0];
				let lng = pin.coordinate[1];
				if(pin.connection){div = L.divIcon({className:'ecto-pin-map', html:HWhtml.pinAdmin('on')});}
				else{div = L.divIcon({className:'ecto-pin-map', html:HWhtml.pinAdmin('of')});}
				let newPin = L.marker([lat,lng],{icon:div});
				
				HWS.buffer.mapAllPins.push(newPin);
				newPin.addTo(HWS.buffer.map);
			});
		}});
		
		/*
		let mapSon = dstat.mapSon;
		let mapSof = dstat.mapSof;
		
		if(mapSon){for (let key in mapSon){
			let div = L.divIcon({className:'ecto-pin-map', html:HWhtml.pinAdmin('on')});
			let marcer = L.marker([mapSon[key].lat,mapSon[key].lng],{icon:div});
			HWS.buffer.mapSon.push(marcer);
		}}
		
		if(mapSof){for (let key in mapSof){
			let div = L.divIcon({className:'ecto-pin-map', html:HWhtml.pinAdmin('of')});
			let marcer = L.marker([mapSof[key].lat,mapSof[key].lng],{icon:div});
			HWS.buffer.mapSof.push(marcer);
		}}
		*/
		
		/*
		let checbox = el.querySelector('checkbox');
		if(checbox.classList.contains('active')){HWF.recount(HWS.buffer.mapSon,function(elem){elem.addTo(HWS.buffer.map);});}
		else{HWF.recount(HWS.buffer.mapSon,function(elem){HWS.buffer.map.removeLayer(elem);});}
		*/
		
		
	}
	
	// ** ACTIVE **
	//Вкл выкл системы накарте  
	HW.on('click','wiev_system_map_online',function(e,el){
		let check = el.querySelector('checkbox');
		if(check.classList.contains('active')){HWS.buffer.mapSystemOnline = 1}
		else{HWS.buffer.mapSystemOnline = 0}
		map_set_pin();
	});
	HW.on('click','wiev_system_map_ofline',function(e,el){
		let check = el.querySelector('checkbox');
		if(check.classList.contains('active')){HWS.buffer.mapSystemOffline = 1}
		else{HWS.buffer.mapSystemOffline = 0}
		map_set_pin();
	});
	//Изменение времени отображения графика 
	HW.on('change','admin_static_graf',function(e,el){
		let loader = el.parentElement.querySelector('.loading-content-mini');
		loader.classList.add('open');
		
		// DEMO
		let infData = {
			acaunt:[{"x":10,"y":4},{"x":20,"y":2},{"x":30,"y":8},{"x":40,"y":5},{"x":50,"y":1}],
			system:[{"x":10,"y":2.8},{"x":20,"y":3},{"x":30,"y":1},{"x":40,"y":2.5},{"x":50,"y":5}],
			online:[{"x":10,"y":2},{"x":20,"y":8},{"x":30,"y":3},{"x":40,"y":18},{"x":50,"y":20}]
		}
		let newData = [];
		newData.push(HWG.fcn.create_date_analog({id:1,label:'Акаунты', color:"#f00000", lineTension:'of', data:infData.acaunt}));
		newData.push(HWG.fcn.create_date_analog({id:1,label:'Системы', color:"#ffae00", lineTension:'of', data:infData.system}));
		newData.push(HWG.fcn.create_date_analog({id:1,label:'Онлайн', color:"#1ab148", lineTension:'of', data:infData.online}));
		
		HWG.fcn.cange_graph_data(HWS.buffer.graph,newData);
		
		setTimeout(function(){loader.classList.remove('open');},1500);
		// DEMO OF
	});
});
