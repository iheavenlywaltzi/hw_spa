"use strict";
HWS.templateAdd(function(HW,B,H){
	let infoBlock = HWS.buffer.infoBlock;
	let GodToken = localStorage.getItem('PITn0Hxg') || false;
	let print = '';
	
	print += '<section>\
		<h3>Добавление нового пользователя</h3><br>\
		<ul class="dop-inp-info line size2">\
			<li><span>Email * :</span><input id="user_email" type="text" value="" placeholder="Email"></li>\
			<li><span>Телефон:</span><input id="user_phone" type="text" value="" placeholder="Телефон"></li>\
			<li id="isterick-mesage-mail" class="isterick_mesage tc_red block_w_100"></li>\
		</ul>\
		<ul class="dop-inp-info line size2">\
			<li><span>Фамилия:</span><input id="user_surname" type="text" value="" placeholder="Фамилия"></li>\
			<li><span>Имя:</span><input id="user_name" type="text" value="" placeholder="Имя"></li>\
			<li><span>Отчество:</span><input id="user_patronymic" type="text" value="" placeholder="Отчество"></li>\
			<li></li>\
		</ul>\
		<ul class="dop-inp-info line size2">\
			<li><span>Пароль * :</span><input id="user_pas" type="password" value="" placeholder="Пароль"></li>\
			<li><span>Пароль еще раз * :</span><input id="user_conpas" type="password" value="" placeholder="Пароль еще раз"></li>\
			<li><span>Уровень доступа:</span><div id="adminAcept" class="btn-active checkbox"></div>: Сделать Администратором</li>\
			<li id="isterick-mesage-pas" class="isterick_mesage tc_red block_w_100"></li>\
		</ul>\
		<ul class="dop-inp-info line size2">\
		</ul>';
	print+= '<div class="block_w_100 block_pt_10 flex_between">\
		<div>* - Обязательные поля</div>	\
		<div class="style_btn god_add_new_user">Создать</div>\
		</div>';
	infoBlock.mid.innerHTML = print+'</section>';

	// ** ACTIVE **
	HWS.on('click','god_add_new_user',function(){
		let pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
		let controlSend = true;
		let data = {};
			data.email = document.body.querySelector('#user_email').value;
			data.phone = document.body.querySelector('#user_phone').value;
			data.famile = document.body.querySelector('#user_surname').value;
			data.name = document.body.querySelector('#user_name').value;
			data.otcest = document.body.querySelector('#user_patronymic').value;
			data.pas = document.body.querySelector('#user_pas').value;
			data.conpas = document.body.querySelector('#user_conpas').value;
			data.imail = document.body.querySelector('#isterick-mesage-mail');
			data.ipas = document.body.querySelector('#isterick-mesage-pas');
			data.admin = document.body.querySelector('#adminAcept');
			
			data.imail.innerHTML = '';
			data.ipas.innerHTML = '';
			
			if(data.pas.length < 6){data.ipas.innerHTML = 'Минемум 6 символов'; controlSend = false;} 
			else {if (data.pas != data.conpas){data.ipas.innerHTML = 'Пароли несовпадают'; controlSend = false;}}
			if (!pattern.test(data.email)){data.imail.innerHTML = 'Неверно введён Емаил'; controlSend = false;}
			if(data.admin.classList.contains('active')){data.admin = 1;} else {data.admin = 0;}
			
			if(controlSend){
				let dataP = {
					token:GodToken,
					email:data.email,
					password:data.pas,
					password_confirmation:data.conpas,
					name:data.name,
					surname:data.famile,
					patronymic:data.otcest,
					phone:data.phone,
					user_is_admin:data.admin
				}
				HWF.post({url:'ecto4_users/registrate_user',data:dataP,fcn:function(info){
					if(info.success){HWS.go('god/usersaddok')}
					console.log(info)
				}});
				
				infoBlock.mid.innerHTML = HWhtml.loader();
			}
	});
});
