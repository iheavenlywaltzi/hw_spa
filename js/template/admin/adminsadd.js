"use strict";
HWS.templateAdd(function(HW,B,H){
	let infoBlock = HWS.buffer.infoBlock;
	let GodToken = localStorage.getItem('PITn0Hxg') || false;
	let print = '';
	
	print += '<section>\
		<h2><span class="tc_red">! Страница отложена, неработает !</span></h2><br>\
		<h3>Добавление нового администратора </h3><br>\
		<ul class="dop-inp-info line size2">\
			<li><span>Email * :</span><input id="user_email" type="text" value="" placeholder="Email"></li>\
			<li><span>Права:</span>'+HWhtml.select(['Админ','Супер Админ'])+'</li>\
			<li id="isterick-mesage-mail" class="isterick_mesage tc_red block_w_100"></li>\
		</ul>\
		<ul class="dop-inp-info line size2">\
			<li><span>Пароль * :</span><input id="user_pas" type="password" value="" placeholder="Пароль"></li>\
			<li><span>Пароль еще раз * :</span><input id="user_conpas" type="password" value="" placeholder="Пароль еще раз"></li>\
			<li id="isterick-mesage-pas" class="isterick_mesage tc_red block_w_100"></li>\
		</ul>';
	print+= '<div class="block_w_100 block_pt_10 flex_between">\
		<div>* - Обязательные поля</div>	\
		<div class="style_btn god_add_new_user">Создать</div>\
		</div>';
	infoBlock.mid.innerHTML = print+'</section>';
	
	// ** ACTIVE **
	HWS.on('click','god_add_new_user',function(){
		let pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
		let controlSend = true;
		let data = {};
			data.email = document.body.querySelector('#user_email').value;
			data.pas = document.body.querySelector('#user_pas').value;
			data.conpas = document.body.querySelector('#user_conpas').value;
			data.imail = document.body.querySelector('#isterick-mesage-mail');
			data.ipas = document.body.querySelector('#isterick-mesage-pas');
			
			data.imail.innerHTML = '';
			data.ipas.innerHTML = '';
			
		if(data.pas.length < 6){data.ipas.innerHTML = 'Минемум 6 символов'; controlSend = false;} 
		else {if (data.pas != data.conpas){data.ipas.innerHTML = 'Пароли несовпадают'; controlSend = false;}}
		if (!pattern.test(data.email)){data.imail.innerHTML = 'Неверно введён Емаил'; controlSend = false;}
		
		if(controlSend){
			console.log('Отправка формы');
		}
	});
});
