"use strict";
HWS.templateAdd(function(HW,B,H){
	let infoBlock = HWS.buffer.infoBlock;
	let GodToken = localStorage.getItem('PITn0Hxg') || false;
	let print = '';
	let idDamin = H.hash[2] || false;
	
	// FCN
	HWS.buffer.adminPrintAdmins = function(num){ num = num || 1;
		let list = document.body.querySelector('#admin-all-admin-list');
		let pagin = document.body.querySelector('#admin-all-admin-list-pagin');
		let lAmount = localStorage.getItem('AllPageAmount') || 10;
		let lStart = num.start || 0;
		let data = {token:GodToken,list_start:lStart,list_amount:lAmount}
		list.innerHTML = HWhtml.admin.adminListTop();
		
		HWF.post({url:'ecto4_admin/list',data:data,fcnE:HWF.getServMesErr,fcn:function(info){
			console.log(info);
			let lCount = info.item_count || 0;	
			
			list.innerHTML = HWhtml.admin.adminList(info);

			if(num == 1){pagin.innerHTML = HWhtml.pagination({number:num,amount:lAmount,all:lCount,type:'buffer',load:'adminPrintAdmins'});}
		}});
	}
	
	if(!idDamin){
		let print = '';
		print += '<div class="user-list-top-btn"><a href="#god/adminsadd" title="Добавить" class="button-add block_mr_10">✛</a>'+HWhtml.search()+'</div>';
		print += '<ul id="admin-all-admin-list" class="all-admin-list list"></ul>';
		print += '<div id="admin-all-admin-list-pagin"></div>';
		
		infoBlock.mid.innerHTML = print;
		HWS.buffer.adminPrintAdmins();
	}
	else {
		let adminInf = '';
		let print = '';
		var TestAdminlist = [ 
					{id:1,email:'biggoblin@inbox.ru',root:'Admin'}, 
					{id:2,email:'test@test.test',root:'Super Admin'},
				]
		
		
		HWF.recount(TestAdminlist,function(inf){
			if (idDamin == inf.id){adminInf = inf}
		});
		
		print += '<section>\
			<h2><span class="tc_red">! Страница отложена, неработает !</span></h2><br>\
			<h3>Профиль</h3>\
			<ul class="dop-inp-info line size2">\
				<li><span>Email:</span><input id="user_email" type="text" value="'+adminInf.email+'" placeholder="Email"></li>\
				<li><span>Права:</span> '+HWhtml.select(['Админ','Супер Админ'])+'</li>\
				<li id="isterick-mesage-mail" class="isterick_mesage tc_red block_w_100"></li>\
			</ul>\
			<ul class="dop-inp-info line size2">\
				<li><span>Новый пароль:</span><input id="user_pas" type="password" value="" placeholder="Пароль"></li>\
				<li><span>Пароль еще раз:</span><input id="user_conpas" type="password" value="" placeholder="Пароль еще раз"></li>\
				<li id="isterick-mesage-pas" class="isterick_mesage tc_red block_w_100"></li>\
			</ul>';
		print+= '</section>'
		
		infoBlock.mid.innerHTML = print;
	}
});
