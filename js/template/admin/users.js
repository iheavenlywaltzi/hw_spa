"use strict";
HWS.templateAdd(function(HW,B,H){
	let infoBlock = HWS.buffer.infoBlock;
	let GodToken = localStorage.getItem('PITn0Hxg') || false;
	let newsPageID = H.hash[2] || false;
	let print = '';
	
	print += '<div class="user-list-top-btn flex_center_left"><a href="#god/usersadd" title="Добавить" class="button-add block_mr_10">✛</a>'+HWhtml.search()+'</div>';
	print += '<ul id="admin-all-user-list" class="all-user-list list"></ul>';
	print += '<div id="admin-all-user-list-pagin"></div>';
	
	infoBlock.mid.innerHTML = print
		
	// ** FCN ** 
	HWS.buffer.adminPrintUser = function(num){ num = num || 1;
		let list = document.body.querySelector('#admin-all-user-list');
		let pagin = document.body.querySelector('#admin-all-user-list-pagin');
		let lAmount = localStorage.getItem('AllPageAmount') || 10;	
		let lStart = num.start || 0;
		let search = HWS.buffer.adminSearchInfoUser || false;
		
		list.innerHTML = HWhtml.admin.userListTop();
		
		let data = {token:GodToken,list_start:lStart,list_amount:lAmount}
		if(search){data.search_text = search}
		HWF.post({url:'ecto4_users/list',data:data,fcnE:HWF.getServMesErr,fcn:function(info){
			let lCount = info.item_count || 0;
			
			list.innerHTML = HWhtml.admin.userList(info);

			if(num == 1){pagin.innerHTML = HWhtml.pagination({number:num,amount:lAmount,all:lCount,type:'buffer',load:'adminPrintUser'});}
		}});
	}
	HWS.buffer.adminPrintSystem = function(num){ num = num || 1;
		let list = HWS.buffer.blockSystem.querySelector('.list_system');
		let userId = HWS.buffer.userId;
		let pagin = HWS.buffer.blockSystem.querySelector('.pagin_system');
		let lAmount = 10; //localStorage.getItem('AllPageAmount') || 10;	lAmount = parseInt(lAmount);
		let lStart = num.start || 0;
		
		list.innerHTML = HWhtml.admin.systemListTop();
		
		let data = {token:GodToken,list_start:lStart,list_amount:lAmount,id_user:userId}
		HWF.post({url:'ecto4_users/list_user_systems',data:data,fcnE:HWF.getServMesErr,fcn:function(info){
			let lCount = info.item_count || 0;	
			list.innerHTML = HWhtml.admin.systemList(info);
			if(num == 1){pagin.innerHTML = HWhtml.pagination({number:num,amount:lAmount,all:lCount,type:'buffer',load:'adminPrintSystem'});}
		}});
	}
	
	function search_user(e,el){		
		let block = el.parentElement;
		let search = block.querySelector('input');
		if(search.value){
			let searchVal = search.value || '';
			searchVal = searchVal.split('-').join('');
			
			block.classList.add('active');
			HWS.buffer.adminSearchInfoUser = searchVal; 
			HWS.buffer.adminPrintUser();
		}
		else {search.value = ''; HWS.buffer.adminSearchInfoUser = false; HWS.buffer.adminPrintUser(); block.classList.remove('active');}
	}	
	function clear_search_user(e,el){
		let block = el.parentElement;
		let search = block.querySelector('input');
		if(search.value){search.value = ''; HWS.buffer.adminSearchInfoUser = false; HWS.buffer.adminPrintUser(); block.classList.remove('active');}
	}
	
	// ** ACTIVE **
	HWS.buffer.adminSearchInfoUser = false;
	HWS.buffer.adminPrintUser();
	
	// поиск пользователя
	HW.on('click','click_search_block',search_user);
	HW.on('change','search_block_input',search_user);	
	HW.on('click','click_clear_search_block',clear_search_user);
	
		// раскрыть закрыть пользывателя
	HW.on('click','click-user',function(e,el){
		let li = HW.parentSearchClass(el,'one-elem');
		
		if(li.classList.contains('open')){li.classList.remove('open')}
		else {
			li.classList.add('open');
			let block = HW.parentSearchClass(el,'one-elem');
			HWS.buffer.blockSystem = block.querySelector('.user-system');
			HWS.buffer.userId = parseInt(block.dataset.id);
			HWS.buffer.adminPrintSystem();
		}
	});
	// просмотр коментариев
	HW.on('click','view_coment_elem',function(e,el){
		let topBlock = el.parentElement.parentElement;
		let liBlock = topBlock.parentElement;
		let idUser = liBlock.dataset.id;
		//let name = topBlock.querySelector('.name-elem').innerHTML;
		let print = '',title = '', Sprint = {}
		
		if(topBlock.classList.contains('user-info')){Sprint.typeV = ''}
		if(topBlock.classList.contains('one-system')){Sprint.typeV = 'Система'}
		
		Sprint.id = topBlock.querySelector('.user-info .info .number-user').innerHTML;
		Sprint.mail = topBlock.querySelector('.user-info .info .mail').innerHTML;
		Sprint.name = topBlock.querySelector('.user-info .name').innerHTML;
		Sprint.tell = topBlock.querySelector('.user-info .tell').innerHTML;
		
		//title = '<span class="tw_500 tc_grey_fon">'+Sprint.typeV+'</span>'+name;
		print = '<div class="admin-modal-mesage-user-block">'+
			'<div id="info-coment-elem" class="block-comment">'+HWhtml.loader()+'</div>'+
			'<div class="block-line"><span></span></div>'+
			'<div id="block-send-coment-elem" class="block-mesage">'+
				'<div>'+
					'<p class="mail">'+Sprint.mail+'</p>'+
					'<p class="id">'+Sprint.id+'</p>'+
					'<p class="name">Имя: '+Sprint.name+'</p>'+
					'<p class="tell">Тел: '+Sprint.tell+'</p>'+
				'</div>'+
				'<textarea id="spec-text-user" placeholder="Введите комментарий"></textarea><br>'+
				'<div class="ta_right block_w_100 block_ptb_9"><div class="style_btn orange send_comment_user block_w_140p">Добавить</div></div>'
			'</div>'+
		'</div>'
		HWF.modal.print('',print);
		
		HWS.buffer.userId = parseInt(idUser);
		let data = {token:GodToken,id_user:idUser,list_start:0,list_amount:10000}
		HWF.post({url:'ecto4_users/get_comments',data:data,fcn:load});
		
		//load(HWD.demoAdmin.coment);
		
		function load(info){
			let list = info.list;
			let blockCOment = document.body.querySelector('#info-coment-elem');		
			blockCOment.innerHTML = HWhtml.admin.commentsList(list);
		}
	});
	// отправить коментарий для пользователя
	HW.on('click','send_comment_user',function(e,el){
		let idUser = HWS.buffer.userId;
		let textComent = HWS.getElem('#spec-text-user');
		if(textComent.value){
			let newComment = textComent.value;
			let blockCOment = document.body.querySelector('#info-coment-elem');
				blockCOment.innerHTML = HWhtml.loader();
			
			textComent.value = '';
			let data = {token:GodToken,id_user:idUser,comment:newComment}
			HWF.post({url:'ecto4_users/set_comment',data:data,fcnE:HWF.getServMesErr,fcn:load});
			
			function load(info){
				let data = {token:GodToken,id_user:idUser,list_start:0,list_amount:10000}
				HWF.post({url:'ecto4_users/get_comments',data:data,fcnE:HWF.getServMesErr,fcn:function(info){
					let list = info.list;
					let blockCOment = document.body.querySelector('#info-coment-elem');		
					blockCOment.innerHTML = HWhtml.admin.commentsList(list);
				}});
			}
		}
	});
	// Удалить коментарий 
	HW.on('click','btn_delete_coment',function(e,el){
		let liBlock = HW.parentSearchClass(el,'one-coment');
		let idComment = liBlock.dataset.id;
		let data = {token:GodToken,id_comment:idComment}
		HWF.post({url:'ecto4_users/delete_comment',data:data,fcnE:HWF.getServMesErr});
		liBlock.remove();
	});
	
	//удалить пользователя
	HW.on('click','delete_user',function(e,el){
		let elem = HW.parentSearchClass(el,'one-elem');
		let name =  elem.querySelector('.name-elem').innerHTML;
		HWS.buffer.delUserElem = elem
		HWF.modal.confirm('Удалить пользователя ?',name,'delete_user_acept');
	});
	HW.on('click','delete_user_acept',function(e,el){
		let userId = HWS.buffer.delUserElem.dataset.id;
		let data = {token:GodToken,id_user:userId}
		HWF.post({url:'ecto4_users/delete_user',data:data,fcnE:HWF.getServMesErr});
		HWS.buffer.delUserElem.remove();
	});
	//Заблокировать/Разблокировать систему
	HW.on('click','blocked_system',function(e,el){
		let elem = HW.parentSearchClass(el,'one-system');
		let name = elem.querySelector('.name-elem').innerHTML;
		HWS.buffer.systemId = elem.dataset.id;
		GD.blockedSystemIcon = el;
		if(el.classList.contains('red')){HWF.modal.confirm('Разблокировать систему ?',name,'unblocked_system_acept');}
		else{HWF.modal.confirm('Заблокировать систему ?',name,'blocked_system_acept');}
	});
	HW.on('click','blocked_system_acept',function(e,el){// Заблокировать
		let data = {token:GodToken,statuses_data:[{id_system:HWS.buffer.systemId,status:1}]}
		HWF.post({url:'ecto4_users/update_block_systems_status',data:data,fcnE:HWF.getServMesErr});
		GD.blockedSystemIcon.classList.add('red');
	}); 
	HW.on('click','unblocked_system_acept',function(e,el){ //Разблокировать
		let data = {token:GodToken,statuses_data:[{id_system:HWS.buffer.systemId,status:0}]}
		HWF.post({url:'ecto4_users/update_block_systems_status',data:data,fcnE:HWF.getServMesErr});
		GD.blockedSystemIcon.classList.remove('red');
	});
	// Очистить базу данных системы
	HW.on('click','clear_base_system',function(e,el){
		let elem = HW.parentSearchClass(el,'one-system');
		let name = elem.querySelector('.name-elem').innerHTML;
		HWF.modal.confirm('Очистить базу данных системы ?',name,'clear_base_system_acept');
	});
	// Зайти в пользователя click_get_user_token
	HW.on('click','click_get_user_token',function(e,el){
		let liBlock = HW.parentSearchClass(el,'one-elem');
		let idUser = liBlock.dataset.id;
		let userName = liBlock.dataset.email;
		let userNumber = liBlock.dataset.numb;
		HWF.modal.print('Переход в пользователя',HWhtml.loader());
		
		let data = {token:GodToken,id_user:idUser}
		HWF.post({url:'ecto4_users/get_token',data:data,fcnE:HWF.getServMesErr,fcn:function(inf){
			HWS.pageAccess = inf.user_token;
			localStorage.setItem('PageAccess',inf.user_token);
			localStorage.setItem('UserEmail',userName);
			localStorage.setItem('UserNumber',userNumber);
			HWS.go('main');
			HWF.setInfoTopMenu();
		}});
	});
});
