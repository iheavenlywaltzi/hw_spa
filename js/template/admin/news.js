"use strict";
HWS.templateAdd(function(HW,B,H){
	let content = HWS.buffer.infoBlock;
	let print = '<h2>Страница новостей</h2>';
	let GodToken = localStorage.getItem('PITn0Hxg') || false;
	let newsPageID = H.hash[2] || false;
	
	if(newsPageID){
		content.mid.innerHTML = HWhtml.loader();
		let data = {token:GodToken,news_id:newsPageID}
			HWF.post({url:'ecto_news/get_news',data:data,fcn:function(info){
				console.log(info);
				let user = '';
				let timer = HWF.unixTime(info.news_time);
				timer = timer.dd+'.'+timer.mt+'.'+timer.yyyy;
				if (info.type_news == 2){user = '<input id="NewsUser" type="hidden" value="'+info.id_user+'"></div>';}
				print = '<div class="news_mesage block_w_45 mobile-100 block_m_20">'+
				'<h4 class="block_pb_8"><span class="tc_grey8"></span><a href="#god/news">Новости</a></h4>'+
					
					'<div class="block_pb_8 flex_between">'+
						'<div class="block_w_60"><h4 class="block_m_0">Тема новости</h4>'+
						'<input id="NewsTitle" type="text" class="block_w_100" value="'+info.theme+'"></div>'+
						
						'<div><h4 class="block_m_0">Время</h4>'+
						'<input id="NewsTime" type="text" title="Дата" onclick="xCal(this)" onkeyup="xCal()" placeholder="01.01.0001" value="'+timer+'"></div>'+
					'</div>'+

					'<div class="block_pb_8"><h4>Краткое описание</h4>'+
					'<textarea id="NewsMiniText" class="block_w_100 block_mp_0" rows="4">'+info.description+'</textarea></div>'+

					'<div class="block_pb_8"><h4 class="block_m_0">Текст новости</h4>'+
					'<textarea id="NewsText" class="block_w_100 block_mp_0" rows="8">'+info.news_text+'</textarea></div>'+
					
					'<div class="flex_between">'+
						'<div class="newscheck"><a href="#god/news">Назад</a></div>'+
						'<span class="button btn_send_news">Сохранить</span>'+
					'</div>'+
					'<div class="errors tc_red"></div>'+
					'<div class="mesage tc_green"></div>'+
					'<input id="NewsType" type="hidden" value="'+info.type_news+'">'+user+
				'</div>';
				content.mid.innerHTML = print;
			}});
			
		HW.on('click','btn_send_news',function(e,el){
			let data = {};
			let block = HW.parentSearchClass(el,'news_mesage');
			let title = block.querySelector('#NewsTitle');
			let time = block.querySelector('#NewsTime');
			let miniText = block.querySelector('#NewsMiniText');
			let newsText = block.querySelector('#NewsText');
			let newsType = block.querySelector('#NewsType');
			let newsUser = block.querySelector('#NewsUser');
			let errors = block.querySelector('.errors');
			let mesage = block.querySelector('.mesage');
			
			errors.innerHTML = "";
			if(!title.value){errors.innerHTML="В ведите Тему новости"}
			else if(!time.value){errors.innerHTML="Укажите время"}
			else if(!miniText.value){errors.innerHTML="Отсутствует краткое описание"}
			else if(!newsText.value){errors.innerHTML="Отсутствует описание"}
			else {
				mesage.innerHTML = HWhtml.loader();
				let timer = time.value;
				timer = timer.split(".");
				timer = new Date(timer[2],timer[1],timer[0]).getTime();
				console.log(time.value);
				data = {
					token:GodToken,
					id_news:newsPageID,
					news_time:timer,
					type_news:newsType.value,
					theme:title.value,
					description:miniText.value,
					news_text:newsText.value,
				}
				if (newsType.value == 2){data.id_user = newsUser.value}
				console.log(data);
				HWF.post({url:'ecto_news/edit_news',data:data,fcn:function(info){
					console.log(info);
					mesage.innerHTML = 'Сохранено'; 
				}});
			}
		});
	}
	else{
		HWS.buffer.adminPrintNews = function(num){ num = num || 1;
			let list = document.body.querySelector('#admin-news-list');
			let pagin = document.body.querySelector('#admin-news-list-pagin');
			let lAmount = localStorage.getItem('AllPageAmount') || 10;	
			let lStart = num.start || 0;
			let search = HWS.buffer.adminSearchInfoUser || false;
			
			list.innerHTML = '<li>'+HWhtml.loader()+'</li>';
			
			let data = {token:GodToken,list_start:lStart,list_amount:lAmount}
			if(search){data.search_text = search}
			HWF.post({url:'ecto_news/list_news',data:data,fcn:function(info){
				let lCount = info.item_count || 0;
				let listM = info.list || 0;
				let echo = '';

				echo+= '<li class="one-elem top-info user-info tc_grey">\
					<span class="block_w_280p ta_left">Название</span>\
					<span class="block_w_100p">Время</span>\
					<span class="block_w_80p"></span></li>\
				';
				HWF.recount(listM,function(elem){
					let timer = HWF.unixTime(elem.news_time).fullDate;
					echo+= '<li class="one-elem top-info user-info" data-id="'+elem.id+'">\
						<span class="block_w_280p ta_left">'+elem.theme+'</span>\
						<span class="block_w_100p">'+timer+'</span>\
						<a href="#god/news/'+elem.id+'" class="click"></a>\
						<span class="btns block_w_80p ta_right"><span class="tc_red text-icon block_plr_8 delete_news_admin" title="Удалить">✕</span></span></li>\
					';
				});
				list.innerHTML = echo;

				if(num == 1){pagin.innerHTML = HWhtml.pagination({number:num,amount:lAmount,all:lCount,type:'buffer',load:'adminPrintNews'});}
			}});
		}
		
		print+= '<ul id="admin-news-list" class="list"></ul>'
		print+= '<div id="admin-news-list-pagin"></div>'
		content.mid.innerHTML = print;
		
		HWS.buffer.adminPrintNews();
	}
	
	
	HW.on('click','delete_news_admin',function(e,el){
		let li = HWS.upElem(el,'li');
		let dataD = {token:GodToken,id_news:li.dataset.id}
		HWF.post({url:'ecto_news/delete_news',data:dataD});
		li.remove();
	});
	
});
