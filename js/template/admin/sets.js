"use strict";
HWS.templateAdd(function(HW,B,H){
	let infoBlock = HWS.buffer.infoBlock;
	let GodToken = localStorage.getItem('PITn0Hxg') || false;
	let print = '';
	let time = HWF.unixTime(false,'of');
	let adminEmail = localStorage.getItem('AdminEmail') || '';
	
	print += '<section>\
		<h3>Профиль ID: [Тут будет ID]</h3>\
		<ul class="dop-inp-info line size2">\
			<li><span>Email:</span><input id="user_email" type="text" value="'+adminEmail+'" placeholder="Email"></li>\
			<li><span>Новый пароль:</span><input id="user_pas" type="password" value="" placeholder="Пароль"></li>\
			<li><span>Пароль еще раз:</span><input id="user_conpas" type="password" value="" placeholder="Пароль еще раз"></li>\
			<li id="isterick-mesage-pas" class="isterick_mesage tc_red block_w_100"></li>\
		</ul>';
	print+= '</section>'
	
	print += '<section class="block_pt_10">\
		<h3>Дата и время</h3>\
		<ul class="dop-inp-info line size2">\
			<li><span>Дата:</span><input class="data_day" value="'+time.fullDate+'" type="text" title="Дата" onclick="xCal(this)" onkeyup="xCal()" placeholder="01.01.0001"></li>\
			<li><span>Время:</span><input class="data_day" value="'+time.fullTime+'" type="text" title="Время"></li>\
		</ul>\
		<br>\
		<ul class="dop-inp-info line size2">\
			<li><span>NTP-сервер:</span><input id="ntp_server" type="text" value="" placeholder="NTP-сервер"></li>\
		</ul>\
		<h3 class="block_pt_10">Местоположение</h3>\
		<ul class="dop-inp-info line size2">\
			<li><span>Ключ к сервису карт:</span><input id="key_map" value="" placeholder="Карта"></li>\
		</ul>';
	print += '<h3>Ваш тариф</h3>';
	print += HWhtml.select(['Обычный','Улучшенный','VIP']);
	
	print+= '<div class="block_w_100 flex_between">\
		<div><span class="btn_text btn_exit_admin">Выйти</span></div>\
		<div class="style_btn god_add_new_user">Сохранить</div>\
		</div>';
	print+='</section>'; 
	
	
	infoBlock.mid.innerHTML = print;
	
	HWS.on('click','god_add_new_user',function(){
		let pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
		let controlSend = true;
		let data = {};
			data.email = document.body.querySelector('#user_email').value;
			data.pas = document.body.querySelector('#user_pas').value;
			data.conpas = document.body.querySelector('#user_conpas').value;
			data.imail = document.body.querySelector('#isterick-mesage-mail');
			data.ipas = document.body.querySelector('#isterick-mesage-pas');
			
			data.imail.innerHTML = '';
			data.ipas.innerHTML = '';
			
		if(data.pas.length < 6 && data.pas.length > 0){data.ipas.innerHTML = 'Минемум 6 символов'; controlSend = false;} 
		else {if (data.pas != data.conpas){data.ipas.innerHTML = 'Пароли несовпадают'; controlSend = false;}}
		if (!pattern.test(data.email)){data.imail.innerHTML = 'Неверно введён Емаил'; controlSend = false;}
		
		if(controlSend){
			console.log('Отправка формы');
		}
	});	
	
});
