"use strict";
HWS.templateAdd(function(HW,B,H){
	let infoBlock = HWS.buffer.infoBlock;
	let GodToken = localStorage.getItem('PITn0Hxg') || false;
	let print = '';
	let s = {};
	
	print += '<h3>Отправка оповещений</h3>';
	
	s.mesege = '<div class="block_w_45 mobile-100 block_m_20">'+
		'<h4><span class="tc_grey8">На почту</span> для пользователей</h4>'+
		'<textarea class="block_w_100 block_mp_0" rows="8"></textarea>'+
		'<div class="flex_between">'+
			'<div>'+
				'<span><span class="checkbox btn-active"></span> 3.0 / 3.1</span>'+
				'<span><span class="checkbox btn-active"></span> 3.2</span>'+
			'</div>'+
			'<span class="button">Отправить</span>'+
		'</div>'+
	'</div>';
	
	s.push = '<div class="block_w_45 mobile-100 block_m_20">'+
		'<h4><span class="tc_grey8">PUSH</span> уведомление</h4>'+
		'<textarea class="block_w_100 block_mp_0" rows="8"></textarea>'+
		'<div class="flex_between">'+
			'<div>'+
				'<span><span class="checkbox btn-active"></span> 3.0 / 3.1</span>'+
				'<span><span class="checkbox btn-active"></span> 3.2</span>'+
			'</div>'+
			'<span class="button">Отправить</span>'+
		'</div>'+
	'</div>';
	
	s.news = '<div class="news_mesage block_w_45 mobile-100 block_m_20">'+
		'<h4 class="block_pb_8"><span class="tc_grey8">В акаунт</span> <a href="#god/news">новости</a></h4>'+
		
		'<div class="block_pb_8 flex_between">'+
			'<div class="block_w_60"><h4 class="block_m_0">Тема новости</h4>'+
			'<input id="NewsTitle" type="text" class="block_w_100" value=""></div>'+
			
			'<div><h4 class="block_m_0">Дата</h4>'+
			'<input id="NewsTime" type="text" title="Дата" onclick="xCal(this)" onkeyup="xCal()" placeholder="01.01.0001"></div>'+
		'</div>'+

		'<div class="block_pb_8"><h4>Краткое описание</h4>'+
		'<textarea id="NewsMiniText" class="block_w_100 block_mp_0" rows="4"></textarea></div>'+

		'<div class="block_pb_8"><h4 class="block_m_0">Текст новости</h4>'+
		'<textarea id="NewsText" class="block_w_100 block_mp_0" rows="8"></textarea></div>'+
		
		'<div class="flex_between">'+
			'<div class="newscheck">'+
				'<span><span id="NewsAllUser" class="active checkbox btn-active-one" data-target="newscheck"></span>Новость для всех</span>'+
				'<span><span id="NewsUnqUser" class="checkbox btn-active-one" data-target="newscheck"></span>Индивидуальная новость</span>'+
			'</div>'+
			'<span class="button btn_send_news">Отправить</span>'+
		'</div>'+
		'<div class="errors tc_red"></div>'+
		'<div class="mesage tc_green"></div>'+
	'</div>';
	
	s.print = s.mesege;
	infoBlock.mid.innerHTML = print+'<div class="flex_between">'+s.print+s.push+s.news+'</div>';
	

		
	// ** FCN ** 
	function print_system_user(inf){
		console.log(inf);
		let print = HWhtml.admin.systemList(inf);
		HWS.buffer.blockSystem.innerHTML = print;
	}
	
	//поиск пользователя	
	function m_search_user(e,el){	
		let block = el.parentElement;
		let search = block.querySelector('input');
		if(search.value){
			block.classList.add('active');
			HWS.buffer.adminSearchInfoUser = search.value; 
			HWS.buffer.pushPrintUser();
		}
		else {search.value = ''; HWS.buffer.adminSearchInfoUser = false; HWS.buffer.pushPrintUser(); block.classList.remove('active');}
	}
	function m_clear_search_user(e,el){
		let block = el.parentElement;
		let search = block.querySelector('input');
		if(search.value){search.value = ''; HWS.buffer.adminSearchInfoUser = false; HWS.buffer.pushPrintUser(); block.classList.remove('active');}
	}

	
	// ** ACTIVE **
	// поиск пользователя
	HW.on('click','m_click_search_block',m_search_user);
	HW.on('change','m_search_block_input',m_search_user);
	HW.on('click','m_click_clear_search_block',m_clear_search_user);
	HW.on('click','btn_send_news',function(e,el){
		let data = {};
		let block = HW.parentSearchClass(el,'news_mesage');
		let title = block.querySelector('#NewsTitle');
		let time = block.querySelector('#NewsTime');
		let miniText = block.querySelector('#NewsMiniText');
		let newsText = block.querySelector('#NewsText');
		let allUser = block.querySelector('#NewsAllUser');
		let unqUser = block.querySelector('#NewsUnqUser');
		let errors = block.querySelector('.errors');
		let mesage = block.querySelector('.mesage');
		
		errors.innerHTML = "";
		if(!title.value){errors.innerHTML="В ведите Тему новости"}
		else if(!time.value){errors.innerHTML="Укажите время"}
		else if(!miniText.value){errors.innerHTML="Отсутствует краткое описание"}
		else if(!newsText.value){errors.innerHTML="Отсутствует описание"}
		else if(allUser.classList.contains('active')){
			mesage.innerHTML = HWhtml.loader();
			let timer = time.value;
			timer = timer.split(".");
			timer = new Date(timer[2],timer[1],timer[0]).getTime();
			console.log(timer);
			data = {
				token:GodToken,
				type_news:1,
				news_time:timer,
				theme:title.value,
				description:miniText.value,
				news_text:newsText.value,
			}
			console.log(data);
			HWF.post({url:'ecto_news/set_news',data:data,fcn:function(info){
				if(info.success){mesage.innerHTML = 'Отправлено'; }
				else {errors.innerHTML = 'Неотправилось...';mesage.innerHTML = ''; }
				
			}});
		}
		else if(unqUser.classList.contains('active')){
			let timer = time.value;
			let mPrint = '';
			
			timer = timer.split(".");
			timer = new Date(timer[2],timer[1],timer[0]).getTime();
			mPrint+= '<div class="user-list-top-btn">'+HWhtml.search('m_search_block_input','m_click_search_block','m_click_clear_search_block')+'</div>';
			mPrint+= '<ul id="admin-all-user-list" class="block_w_100 list"></ul>';
			mPrint+= '<div class="ta_left block_w_100" id="admin-all-user-list-pagin"></div>';
			mPrint+= '<div id="modal-errors-mesage" class="tc_red"></div>';
			mPrint+= '<span class="button btn_send_m_news">Отправить</span>';
			HWF.modal.print('Выбор пользователя',mPrint);
			HWS.buffer.pushPrintUser();
			
			data = {
				token:GodToken,
				type_news:2,
				news_time:timer,
				theme:title.value,
				description:miniText.value,
				news_text:newsText.value,
			}
			HWS.buffer.pushDataInfo = data;
		}
		
	});
	
	HWS.buffer.pushPrintUser = function(num){ num = num || 1;
		let list = document.body.querySelector('#admin-all-user-list');
		let pagin = document.body.querySelector('#admin-all-user-list-pagin');
		let lAmount = localStorage.getItem('AllPageAmount') || 10;	
		let lStart = num.start || 0;
		let search = HWS.buffer.adminSearchInfoUser || false;
		
		list.innerHTML = '<li>'+HWhtml.loader()+'</li>';
		
		let data = {token:GodToken,list_start:lStart,list_amount:lAmount}
		if(search){data.search_text = search}
		HWF.post({url:'ecto4_users/list',data:data,fcnE:HWF.getServMesErr,fcn:function(info){
			let lCount = info.item_count || 0;
			let listM = info.list || 0;
			let print = '';
			
			print+= '<li class="one-elem top-info user-info">\
				<span class="block_w_280p ta_left">Email</span>\
				<span class="block_w_280p">Имя</span>\
				<span class="block_w_180p">Телефон</span>\
				<span class="btns block_w_80p"></span></li>\
			';
			HWF.recount(listM,function(elem){
				print+= '<li class="one-elem top-info user-info" data-id="'+elem.id_user+'">\
					<span class="block_w_280p ta_left">'+elem.email+'</span>\
					<span class="block_w_280p">'+elem.name+'</span>\
					<span class="block_w_180p">'+elem.phone+'</span>\
					<span class="click click_btn_modal_user"></span>\
					<span class="btns block_w_80p"><span class="checkbox"></span></span></li>\
				';
			});
			list.innerHTML = print;

			if(num == 1){pagin.innerHTML = HWhtml.pagination({number:num,amount:lAmount,all:lCount,type:'buffer',load:'adminPrintUser'});}
		}});
	}
	
	HW.on('click','click_btn_modal_user',function(e,el){
		let li = HW.upElem(el,'li');
		let check = li.querySelector('.checkbox');
		if(check.classList.contains('active')){
			li.classList.remove('active');
			check.classList.remove('active');
		}
		else {
			li.classList.add('active');
			check.classList.add('active');
		}
	});
	
	HW.on('click','btn_send_m_news',function(e,el){
		document.body.querySelector('#modal-errors-mesage').innerHTML = '';
		let allLi = document.body.querySelectorAll('#admin-all-user-list li.active');
		let users = [];
		if(allLi.length > 0){
			HWF.recount(allLi,function(elli){users.push(elli.dataset.id);},'on');
			HWS.buffer.pushDataInfo.users = users;
			document.body.querySelector('.modal-section').innerHTML = HWhtml.loader();
			HWF.post({url:'ecto_news/set_news',data:HWS.buffer.pushDataInfo,fcn:function(info){
				if(info.success){HWF.modal.message('Информация отправлена','Отправлено !');}
				else {HWF.modal.message('Неотправилось...','Ошибка !');}
				
			}});
		}
		else {
			document.body.querySelector('#modal-errors-mesage').innerHTML = 'Выберите хотябы 1';
		}
	});
});
