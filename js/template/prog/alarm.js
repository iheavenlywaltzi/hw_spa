"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#prog-content-top');
	let content = document.body.querySelector('#prog-content-mid');
	let token = HWS.pageAccess;
	let page = 'PageSets';
	let id = H.hash[1] || false;
	let ET = HWS.buffer.elemEt || false;
	let D = {}; // Дада массив с ОБЪЕКТАМИ от сервера
	let M = {}; // Отрисовочный объект с нужными данными
	let C = {}; // якоря
	let nameFocus = false;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="PageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageAccesses">Доступ</span></div>';
	B.menud.append(menu);
	

	/* FUNCTION */
	function load_all_info(rInfo){
		D.elem = HWS.buffer.elem;
		
		D.sensors = D.elem.config.sensors || '';
		D.relay = D.elem.config.automatic_device || '';

		M = creator_m(D);// создаём адекватное структурирование данных
		print_header();// печатем шапку страници
		HWS.buffer[page]();// печатаем вкладку на которой находимся
		HWF.timerUpdate(reolad_info_page); //Авто обновление
	}
	
	function creator_m(CD){
		let RM = {};
		
		RM.elem = CD.elem;
		RM.name = CD.elem.config.name;
		RM.elemType = CD.elem.info.type.type;
		RM.elemTitle = ET.title;
		RM.systemName = CD.elem.system_name;
		RM.systemIdent = CD.elem.system_ident;
		RM.manualMode = CD.elem.config.flags.active; // ручной режим
		RM.viget = CD.elem.lk.favorite || ''; // в виджеты
		RM.inverse = CD.elem.config.flags.inverse; // режим работы
		RM.delay = CD.elem.config.pau_alarm_delay;  // Время управления
		RM.idRelay = CD.relay.id || ''; // Устройство управления
		
		HWS.buffer.equps = {};
		HWS.buffer.equps[RM.idRelay] = CD.relay;
		HWS.buffer.equps.sensors = CD.sensors;
		
		RM.state = CD.elem.lk;
		
		return RM;
	}
		
	function change_state(){ HWF.timerUpdate(reolad_info_page,'',100); }  //Авто обновление через 100 милисекунд
	function re_print_state(MNew){
		// Изменение статуса
		let S = {};
		let state = MNew.state.state;
		let name = MNew.state.state_name;
		let nameStat = HWS.getElem('#name-status inf')
		let blamba = HWS.getElem('#blamba')

		S.blambaColor = HWD.state.prog[state].colorBlamba;
		S.stateText = name;
		S.stateTextColor = HWD.state.prog[state].colorText;
		
		blamba.style.background = S.blambaColor;
		nameStat.style.color = S.stateTextColor;
		nameStat.innerHTML = S.stateText;
	}
	
	function reolad_info_page(){if(page && page == 'PageSets'){ HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(rEl){
		console.log(111)
		let DNew =  {}; DNew.elem = rEl;
			DNew.sensors = DNew.elem.config.sensors || '';
			DNew.relay = DNew.elem.config.automatic_device || '';

			D = DNew;
			let MNew = creator_m(DNew);// создаём адекватное структурирование данных
			let S = {};
			// Имя
			if(!nameFocus && M.name != MNew.name){ M.name = MNew.name; HWS.getElem('#prog-name').value = MNew.name;}
			//Ручной режим
			if(!C.manualMode && M.manualMode != MNew.manualMode ){M.manualMode = MNew.manualMode; 
				let btnMode = HWS.getElem('.btn_onof_mode');
				let textPole = HWS.getElem('#manual-mode-info');
				if(M.manualMode){ btnMode.classList.remove('active'); textPole.innerHTML = ''; }
				else { btnMode.classList.add('active'); textPole.innerHTML = 'Вручную';}
			}
			// В виджеты
			if(!C.favorite && M.viget != MNew.viget){ M.viget = MNew.viget;
				let favorite = HWS.getElem('#equp-viget');
				(MNew.viget)?favorite.classList.add('active'):favorite.classList.remove('active');
			}
			// Режим работы
			if(!C.typeWork && M.inverse != MNew.inverse) { M.inverse = MNew.inverse;
				S.typeWork = HWS.getElem('.inversion_reaction_sensor');
				S.typeWork.value = (MNew.inverse)?2:1;
			}
			// Время работы
			if(!C.timeNumb  && M.delay != MNew.delay){ M.delay = MNew.delay;
				S.timeNumb = HWS.getElem('.time_reaction_sensor');
				S.bTime = HWS.getElem('#alarm-control-timer');
				S.inpTime = HWS.getElem('#alarm-control-timer .inp_num_val');
				S.btnReset = HWS.getElem('#btn-rebout')
		

				if(MNew.delay == 0){
					S.timeNumb.value = 1;
					S.bTime.classList.remove('open');
					S.inpTime.value = 0;
					S.btnReset.classList.add('display_none');
				}
				else if(MNew.delay== 65535){
					S.timeNumb.value = 2;
					S.bTime.classList.remove('open');
					S.inpTime.value = 0;
					S.btnReset.classList.remove('display_none');
				}
				else {
					S.timeNumb.value = 3;
					S.bTime.classList.add('open');
					S.inpTime.value = MNew.delay;
					S.btnReset.classList.add('display_none');
				}
			}
			// обновление списка 
			if(!C.list){buttonsController.updateSme('#sme_0','sensors');}
			// обновление реле
			if(!C.relay){buttonsController.updateSes('#uu-relay',MNew.idRelay);}
			// ручной сброс 
			if(!C.typeWork && !C.btnRebout){
				let btnRebout = HWS.getElem('#btn-rebout');
				if(D.relay){
					let relayNorm = HWequip.normInfo(D.relay);
					if(MNew.inverse){if(!relayNorm.stateN){btnRebout.classList.add('active')} else {btnRebout.classList.remove('active')}}
					else {if(relayNorm.stateN){btnRebout.classList.add('active')} else {btnRebout.classList.remove('active')}}
				}
			}
			
			if(JSON.stringify(C) == "{}"){M = MNew;}
			re_print_state(MNew);
			C = {} // очищение якорей
		}}); }
	}
	
	function print_header(){
		let state = M.state.state;
		let name = M.state.state_name;
		let S = {};
		S.manualModeText = (M.manualMode)?'':'Вручную';
		S.blambaColor = HWD.state.prog[state].colorBlamba;
		S.stateText = name;
		S.stateTextColor = HWD.state.prog[state].colorText;
		
		let print = '\
		<div class="one-block ob-top page-equp-top">\
			<div class="name-page">\
				<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
				<input id="prog-name" type="text" class="input-name-page" value="'+M.name+'">\
				<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
			</div>\
			<div class="info-li block_w_100 flex_center_left">\
				<div id="blamba" style="background:'+S.blambaColor+'"></div>\
				<div class="page-top-dop-info tf_fr500 block-title-name1colum">\
					<div id="name-status" class="data"><inf style="color:'+S.stateTextColor+'">'+S.stateText+'</inf></div>\
					<div id="manual-mode-info" class="dop-info-name">'+S.manualModeText+'</div>\
				</div>\
				<div class="block-title-colum"></div>\
				<div class="page-top-dop-info-title block-title-colum310">\
					<p>Принадлежит</p>\
					<p><span id="name-system" class="tf_fr500">'+M.systemName+'</span></p>\
					<p><span class="tf_fr500">'+M.systemIdent+'</span></p>\
				</div>\
				<div id="iconr" class="flex_center_left">\
					<span class="block_w_150p block_pr_10">'+M.elemTitle+'</span>\
					<span class="icon p35 icon icon_equip_'+M.elemType+'"></span>\
				</div>\
			</div>\
		</div>\
		';
		contentTop.innerHTML = print;
		HWS.buffer.share_stick(M.elem);
	}
	
	// Загрузка Настроек
	HWS.buffer.PageSets = function(){ page = 'PageSets';
		let S = {};
		S.print = '';
		S.manualModeAct = (M.manualMode)?'':'active';
		S.onofNumb = (M.inverse)?2:1; S.onof = {}; S.onof[S.onofNumb] = 'selected';
		S.time = 0;
		if(M.delay == 0){S.type_action = 1}
		else if(M.delay== 65535){S.type_action = 2}
		else {S.type_action = 3; S.time = M.delay}
		S.typeAction = {}; S.typeAction[S.type_action] = 'selected';
		S.alarmSO = (S.type_action == 3)?'open':'';
		S.favorite = (M.viget)?'active':'';
		S.resrtAlarmClass = (S.type_action == 2)?'':'display_none';
		
		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Ручной режим '+HWhtml.helper(HWD.helpText.prog.manualMode)+'</p>\
					<p><span class="on-off btn-active '+S.manualModeAct+' btn_onof_mode"></span></p>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.prog.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+S.favorite+'" ></span>\
				</div>\
			</div>\
		</div>';
		
		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Датчики '+HWhtml.helper(HWD.helpText.prog.manualEgups)+'</h4></div>\
			<div class="ltft"></div>\
			<div class="right"></div>\
			<div class="title"><sme data-buf="sensors" data-list="list" data-fcn="listSensor" data-fcnd="listSensorD"></sme></div>\
		</div>';
		
		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Управление</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Режим работы '+HWhtml.helper(HWD.helpText.prog.typeWork)+'</p>\
					<br>\
					<p>\
						<select class="inversion_reaction_sensor block_w_200p">\
						<option '+(S.onof[1] || '')+' value="1">Включать (открывать)</option>\
						<option '+(S.onof[2] || '')+' value="2">Отключать (закрывать)</option>\
						</select>\
					</p>\
				</div>\
				<div class="colum">\
					<p>Время '+HWhtml.helper(HWD.helpText.prog.timeWork)+'</p>\
					<br>\
					<p>\
						<select class="open_control time_reaction_sensor block_w_200p">\
						<option '+(S.typeAction[1] || '')+' value="1" data-of="#alarm-control-timer">на время тревоги</option>\
						<option '+(S.typeAction[2] || '')+' value="2" data-of="#alarm-control-timer">при тревоге до сброса вручную</option>\
						<option '+(S.typeAction[3] || '')+' value="3" data-on="#alarm-control-timer">на заданное время, сек</option>\
						</select>\
					</p>\
					<br>\
					<p id="alarm-control-timer" class="open-clouse '+S.alarmSO+'">\
						'+HWhtml.btn.numb(S.time,{fcn:'setTime',clas:'btn_time',min:0,max:9999})+'\
					</p>\
				</div>\
				<div class="colum">\
					<ses id="uu-relay" class="list" data-id="'+M.idRelay+'" data-load="loadUU" data-fcn="changeUU" data-fcnd="changeUUd" data-filter="relay"></ses>\
					<br> <br>\
					<span id="btn-rebout" class="btn_rebout_relay btn-checkboxEC green '+S.resrtAlarmClass+'">Cброс тревоги</span>\
				</div>\
			</div>\
		</div>';
				
		S.print += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin"></p>\
					<p class="btn_text grey btn_delete_prog">Удалить программу</p>\
				</div>\
			</div>\
		</div>';
		
		// отрисовка HTML 
		content.innerHTML = S.print;
		// запуск кнопок ПРограмм 
		buttonsController.start();
	}
	// Загрузка Журнала
	HWS.buffer.PageJurnal = function(){ page = 'PageJurnal';
		let print = '';
		content.innerHTML = ''; 
		
		HWS.buffer.load_jurnal(1);
	};
	// Загрузка Доступов
	HWS.buffer.PageAccesses = function(){ page = 'PageAccesses';
		HWS.buffer.equpAccessesFCN();
	};
	
	load_all_info();
	
	//Упровляющие функции 
	// Изменение имени программы
	HW.on('focusin','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; nameFocus = 1;
	});
	HW.on('focusout','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; nameFocus = 1;
	});
	HW.on('input','input-name-page',function(e,el){nameFocus = 1}); HW.on('click','input-name-page',function(e,el){nameFocus = 1});
	HW.on('change','input-name-page',function(e,el){	
		nameFocus = false;
		let name = document.body.querySelector('.ob-top .name-page input');	
		//let nameRegex = new RegExp(HWD.reg.name);	

		if(M.name != name.value){
			//if(nameRegex.test(name.value)){
			if(name.value){
				let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
				HWF.post({url:HWD.api.gets.taskROSProgram3xSystem,data:dataP});
				M.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
	
	// Добавление в лист устройства
	HWS.buffer.listSensor = function(inf){
		C.list = 1;
		let dataP = {token:HWS.pageAccess,id_object:id,sensors:inf.listId}
		HWF.post({url:HWD.api.gets.taskAddROSProgram3xSystem,data:dataP});
	}
	// Удаение из листа устройств
	HWS.buffer.listSensorD = function(inf){
		C.list = 1; console.log(inf);
		let dataP = {token:HWS.pageAccess,id_object:id,sensors:[inf]}
		HWF.post({url:HWD.api.gets.taskDelROSProgram3xSystem,data:dataP});
	}
	
	// Загрузка Устройства управления
	HWS.buffer.loadUU = function(inf){};
	// Изменение Устройство управления
	HWS.buffer.changeUU = function(inf){
		C.relay = 1; // Якорь для реле
		D.relay = inf;
		M.idRelay = inf.id;
		HWS.buffer.equps[M.idRelay] = D.relay;
		change_state();
		let dataP = {token:HWS.pageAccess,id_object:id,id_automatic_device:inf.id}
		HWF.post({url:HWD.api.gets.taskROSProgram3xSystem,data:dataP});
	};
	// Удаление Устройство управления
	HWS.buffer.changeUUd = function(inf){	
		C.relay = 1; // Якорь для реле
		D.relay = '';
		M.idRelay = '';
		HWS.buffer.equps[M.idRelay] = D.relay;
		change_state();
		let dataP = {token:HWS.pageAccess,id_object:id,id_automatic_device:'delete'}
		HWF.post({url:HWD.api.gets.taskROSProgram3xSystem,data:dataP});
	};
	
	// Ручной режим:
	HW.on('click','btn_onof_mode',function(e,el){
		C.manualMode = 1; // якорь для автообновления
		let nameB = HWS.getElem('#manual-mode-info');
		if(el.classList.contains('active')){// on
			let dataP = {token:HWS.pageAccess,id_object:id,active:0}
			HWF.post({url:HWD.api.gets.taskROSProgram3xSystem,data:dataP});
			nameB.innerHTML = 'Вручную';
			M.manualMode = 0;
		}
		else{// of
			let dataP = {token:HWS.pageAccess,id_object:id,active:1}
			HWF.post({url:HWD.api.gets.taskROSProgram3xSystem,data:dataP});
			nameB.innerHTML = '';
			M.manualMode = 1;
		}
	});
	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			M.viget = 1;
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/add_favorite_objects',data:dataP});
		}else { // Of
			M.viget = 0;
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/delete_favorite_objects',data:dataP});
		}
	});
	
	// Время работы системы:
	HW.on('change','time_reaction_sensor',function(e,el){
		C.timeWork = 1; // якорь для автообновления
		let inpTime = HWS.getElem('#alarm-control-timer .inp_num_val');
		let btnReset = HWS.getElem('#btn-rebout')
		btnReset.classList.add('display_none');
		if(el.value == 1){
			M.delay = 0;
			let dataP = {token:HWS.pageAccess,id_object:id,pau_alarm_delay:'0'}
			HWF.post({url:HWD.api.gets.taskROSProgram3xSystem,data:dataP});
			inpTime.value = 0;
		}
		if(el.value == 2){
			M.delay = 65535;
			let dataP = {token:HWS.pageAccess,id_object:id,pau_alarm_delay:'65535'}
			HWF.post({url:HWD.api.gets.taskROSProgram3xSystem,data:dataP});
			inpTime.value = 0;
			btnReset.classList.remove('display_none');
		}
	})
	// Изменение Времени работы устройства
	HWS.buffer.setTime = function(inf){		
		C.timeNumb = 1; // якорь для автообновления
		M.delay = inf;
		clearTimeout(HWS.buffer.setMinsTimer);
		HWS.buffer.setMinsTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pau_alarm_delay:inf}
			HWF.post({url:HWD.api.gets.taskROSProgram3xSystem,data:dataP});
		},2000);
	}
	
	// Режим работы:
	HW.on('change','inversion_reaction_sensor',function(e,el){
		C.typeWork = 1; // якорь для автообновления
		let btnRebout = HWS.getElem('#btn-rebout');

		if(el.value == 1) {M.inverse = 0;}
		else {M.inverse = 1}
		
		if(D.relay){
			let relayNorm = HWequip.normInfo(D.relay);
			if(M.inverse){if(!relayNorm.stateN){btnRebout.classList.add('active')} else {btnRebout.classList.remove('active')}}
			else {if(relayNorm.stateN){btnRebout.classList.add('active')} else {btnRebout.classList.remove('active')}}
		}
		
		let dataP = {token:HWS.pageAccess,id_object:id,inverse:M.inverse}
		HWF.post({url:HWD.api.gets.taskROSProgram3xSystem,data:dataP});
	});
	
	//rebout relay
	HW.on('click','btn_rebout_relay',function(e,el){
		C.btnRebout = 1;
		if(D.relay){
			let relayNorm = HWequip.normInfo(D.relay);
			let clickVal = 0;
			
			if(M.inverse != 0) {clickVal = 1}
			let dataP = {token:HWS.pageAccess,id_object:relayNorm.id,state:clickVal}
			HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
			HWF.push('Отправлен запрос на cброс тревоги');
		}
	});
	//select all
	HW.on('click','equp_select_all',function(event,elem){HWF.allActive('.list-equipments','.checkbox');});
	// select all Modal
	HW.on('click','equp_select_all',function(event,elem){HWF.allActive('.modal-section .list-equipments','.checkbox');});
	
	// Кнопка Удолить программу
	HW.on('click','btn_delete_prog',function(e,el){
		let title = 'Что такое аварийный режим?';
		let infotext = 'Данная программа будет удалена из памяти системы. Вы уверены?';
		HWF.modal.confirm('Удалить программу?',infotext,'btn_delete_prog_yes','Удалить');
	});
	HW.on('click','btn_delete_prog_yes',function(e,el){
		let idprog = id || H.hash[1] || false;;
		HWF.modal.loader('Удаление');
		HWF.post({url:'task/delete_program_3x_system',data:{token:token,id_objects:[idprog]},fcn:function(info){
			HWF.modal.of();
			HWS.go('programs');
			HWF.push('Программа удалена','red');
		}});
	});
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(); // Проверит ваш ли это объект для вовода предупреждения
});