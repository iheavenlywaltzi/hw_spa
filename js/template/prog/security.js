"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#prog-content-top');
	let content = document.body.querySelector('#prog-content-mid');
	let token = HWS.pageAccess;
	let page = 'PageSets';
	let id = H.hash[1] || false;
	let ET = HWS.buffer.elemEt || false;
	let D = {}; // Дада массив с ОБЪЕКТАМИ от сервера
	let M = {}; // Отрисовочный объект с нужными данными
	let C = {}; // якоря
	let nameFocus = false;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="PageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageAccesses">Доступ</span></div>';
	B.menud.append(menu);
	
		
	/* FUNCTION */
	function load_all_info(rInfo){
		D.elem = HWS.buffer.elem;
		D.sensors = D.elem.config.sensors || ''; //запрос списка для sme
		D.sound = D.elem.config.sound_device || ''; //Устройство Звук
		D.light = D.elem.config.light_device || ''; //Устройство Свет 
		D.relay = D.elem.config.extend_device || ''; //Устройство Реле 

		M = creator_m(D);// создаём адекватное структурирование данных
		print_header();// печатем шапку страници
		HWS.buffer[page]();// печатаем вкладку на которой находимся
		HWF.timerUpdate(reolad_info_page); //Авто обновление
	}
	
	function creator_m(CD){
		let RM = {};
		RM.elem = CD.elem;
		RM.name = CD.elem.config.name;
		RM.elemType = CD.elem.info.type.type;
		RM.elemTitle = ET.title;
		RM.systemVer = RM.elem.system_info_dev || ET.system;
		RM.systemName = CD.elem.system_name;
		RM.systemIdent = CD.elem.system_ident;
		RM.viget = CD.elem.lk.favorite || ''; // в виджеты
		RM.delay = CD.elem.config.arm_del; // Задержка постановки
		//RM.disarm = CD.elem.config.notif_arm_disarm; // Всегда оповещать о постановке и снятии
		RM.active = CD.elem.config.flags.security_active;// флаг активности программы
		RM.customTime = CD.elem.config.arm_mode_sound.arm_disarm; // Время работы звука
		RM.soundMode = CD.elem.config.arm_mode_sound.mode; // подтверждать постановку и снятие
		RM.lightDisarm = CD.elem.config.arm_mode_light.arm_disarm; // Режим работы света
		RM.lightMode = CD.elem.config.arm_mode_light.mode; // Подтверждать постановку и снятие с охраны
		
		RM.idSound = CD.sound.id || ''; // Устройство Звука
		RM.idLight = CD.light.id || ''; // Устройство Света
		RM.idRelay = CD.relay.id || ''; // Устройство управления
		
		HWS.buffer.equps = {};
		HWS.buffer.equps[RM.idSound] = CD.sound;
		HWS.buffer.equps[RM.idLight] = CD.light;
		HWS.buffer.equps[RM.idRelay] = CD.relay;
		HWS.buffer.equps.sensors = CD.sensors;
		
		RM.state = CD.elem.lk; //get_state(RM);

		return RM;
	}
	
	function change_state(){ HWF.timerUpdate(reolad_info_page,'',100); }  //Авто обновление через 100 милисекунд
	function re_print_state(MNew){
		let state = MNew.state.state;
		let name = MNew.state.state_name;
		let btn = HWS.getElem('.btn_security_onof');
		let stat = HWS.getElem('#name-status inf');
		let blamba = HWS.getElem('#blamba');
		
		stat.innerHTML = name;
		blamba.style.background = HWD.state.prog[state].colorBlamba;
		
		if(state == 116){
			btn.innerHTML = 'Снять с охраны';
			btn.classList.add('active');
		} else if(state == 115){
			btn.innerHTML = 'Поставить на охрану';
			btn.classList.remove('active');
		}
	}
	
	function reolad_info_page(){ if(page && page == 'PageSets'){ HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(rEl){
		let DNew =  {}; DNew.elem = rEl;
			DNew.sensors = DNew.elem.config.sensors || ''; //запрос списка для sme
			DNew.sound = DNew.elem.config.sound_device || ''; //Устройство Звук
			DNew.light = DNew.elem.config.light_device || ''; //Устройство Свет 
			DNew.relay = DNew.elem.config.extend_device || ''; //Устройство Реле 
			
			D = DNew;
			let MNew = creator_m(DNew);// создаём адекватное структурирование данных
			let S = {};
			// Имя
			if(!nameFocus && M.name != MNew.name){ M.name = MNew.name; HWS.getElem('#prog-name').value = MNew.name;}
			// В виджеты
			if(!C.favorite && M.viget != MNew.viget){ M.viget = MNew.viget;
				let favorite = HWS.getElem('#equp-viget');
				(MNew.viget)?favorite.classList.add('active'):favorite.classList.remove('active');
			}
			// Задержка постановки
			if(!C.delay && M.delay != MNew.delay){M.delay = MNew.delay;
				S.delay = HWS.getElem('#delay-time .inp_num_val').value = MNew.delay;
			}
			// оповещать о постановке и снятии
			/*if(!C.disarm && M.disarm != MNew.disarm){M.disarm = MNew.disarm;
				S.disarm = HWS.getElem('#btn_notif_arm_disarm');
				if(MNew.disarm){S.disarm.classList.add('active')}
				else {S.disarm.classList.remove('active')}
			}*/
			// Режим работы звука
			if(!C.customTime && M.customTime != MNew.customTime){M.customTime = MNew.customTime;
				S.selSound = HWS.getElem('.bnt_canche_siren');
				S.timerWin = HWS.getElem('#security-control-timer');
				S.timerVal = HWS.getElem('#security-control-timer .btn-int .inp_num_val');
				
				if(MNew.customTime <= 0){
					S.selSound.value = 0;
					S.timerVal.value = 0;
					S.timerWin.classList.remove('open');
				} else {
					S.selSound.value = 1;
					S.timerVal.value = MNew.customTime;
					S.timerWin.classList.add('open');
				}
			}
			// ЗВУК Подтверждать постановку
			if(!C.soundMode && M.soundMode != MNew.soundMode){M.soundMode = MNew.soundMode;
				S.modSound = HWS.getElem('.bnt_canche_siren_check');
				if(MNew.soundMode){S.modSound.classList.add('active');}
				else {S.modSound.classList.remove('active');}
			}
			// Режим работы света
			if(!C.lightDisarm && M.lightDisarm != MNew.lightDisarm){M.lightDisarm = MNew.lightDisarm;
				S.lightDisarm = HWS.getElem('.btn_light_control').value = MNew.lightDisarm;
			}
			// СВЕТ Подтверждать постановку
			if(!C.lightMode && M.lightMode != MNew.lightMode){M.lightMode = MNew.lightMode;
				S.lightMode = HWS.getElem('.bnt_canche_light_check');
				if(MNew.lightMode){S.lightMode.classList.add('active');}
				else {S.lightMode.classList.remove('active');}
			}
			
			// обновление списка 
			if(!C.list){buttonsController.updateSme('#sme_0','sensors');}
			// обновление Звука
			if(!C.idSound){buttonsController.updateSes('#uu-sound',MNew.idSound);}
			// обновление Света
			if(!C.idLight){buttonsController.updateSes('#uu-light',MNew.idLight);}
			// обновление Реле
			if(!C.idRelay){buttonsController.updateSes('#uu-control',MNew.idRelay);}
			
			if(JSON.stringify(C) == "{}"){M = MNew;}
			re_print_state(MNew);
			C = {} // очищение якорей
		}}); }
	}
	
	function print_header(){
		let state = M.state.state;
		let name = M.state.state_name;
		let S = {};
		S.blambaColor = HWD.state.prog[state].colorBlamba;
		S.stateText = name;
		S.stateTextColor = HWD.state.prog[state].colorText;
		S.stateA = '';
		S.stateNTop = 'Поставить на охрану';
		
		if(state == 116){S.stateA = 'active'; S.stateNTop = 'Снять с охраны';}
		
		let print = '\
		<div class="one-block ob-top page-equp-top">\
			<div class="name-page">\
				<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
				<input id="prog-name" type="text" class="input-name-page" value="'+M.name+'">\
				<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
			</div>\
			<div class="info-li block_w_100 flex_center_left">\
				<div id="blamba" style="background:'+S.blambaColor+'"></div>\
				<div class="page-top-dop-info tf_fr500 block-title-name1colum">\
					<div id="name-status" class="data"><inf style="color:'+S.stateTextColor+'">'+S.stateText+'</inf></div>\
				</div>\
				<div class="block-title-colum">\
					<span class="style_btn grey hover-green btn_security_onof '+S.stateA+'">'+S.stateNTop+'</span>\
				</div>\
				<div class="page-top-dop-info-title block-title-colum310">\
					<p>Принадлежит</p>\
					<p><span id="name-system" class="tf_fr500">'+M.systemName+'</span></p>\
					<p><span class="tf_fr500">'+M.systemIdent+'</span></p>\
				</div>\
				<div id="iconr" class="flex_center_left">\
					<span class="block_w_150p block_pr_10">'+M.elemTitle+'</span>\
					<span class="icon p35 icon icon_equip_'+M.elemType+'"></span>\
				</div>\
			</div>\
		</div>\
		';
		contentTop.innerHTML = print;
		HWS.buffer.share_stick(M.elem);
	}
	
	// Загрузка настроек 
	HWS.buffer.PageSets = function(){ page = 'PageSets';
		let S = {}; S.print = '';
		//S.mesageOnOf = (M.disarm)?'active':'';
		S.favorite = (M.viget)?'active':'';
	
		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.prog.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+S.favorite+'" ></span>\
				</div>\
				<div class="colum">\
					<p>Задержка постановки/снятия, '+HWhtml.helper(HWD.helpText.prog.alarmDelayBtn)+' <br> сек</p>\
					<p>'+HWhtml.btn.numb(M.delay,{id:'delay-time',fcn:'setTime',min:'null',max:9999})+'</p>\
				</div>\
				<!--div class="colum">\
					<p>Всегда оповещать о <br> постановке и снятии</p>\
					<p><span class="on-off btn-active '+S.mesageOnOf+' btn_notif_arm_disarm"></span></p>\
				</div-->\
			</div>\
		</div>';
		
		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Охранные датчики '+HWhtml.helper(HWD.helpText.prog.securityEqupment)+'</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
			</div>\
			<div class="title"><sme data-buf="sensors" data-fcn="listSensor" data-fcnd="listSensorD"></sme></div>\
		</div>';
		// ver = 30 или 31 , управление сиреной и светом ( небудет )
		console.log(M.systemVer);
		if(M.systemVer != 49 && M.systemVer != 48){
			S.sirenTime = (M.customTime > 0)?1:0;
			S.optTimeSiren = {}; S.optTimeSiren[S.sirenTime] = 'selected';
			S.securitySO = (S.sirenTime)?'open':'';
			S.confirmOnof = (M.soundMode)?'active':'';
			S.lightMode = {}; S.lightMode[M.lightDisarm] = 'selected';
			S.confirmOnofL = (M.lightMode)?'active':'';
			
			S.print += '<div class="one-block-col">\
				<div class="title"><h4>Звук '+HWhtml.helper(HWD.helpText.prog.securitySound)+'</div>\
				<div class="ltft"></div>\
				<div class="right">\
					<div class="colum">\
						<p>Режим работы звука</p>\
						<br>\
						<p>\
							<select class="open_control bnt_canche_siren block_w_170p">\
								<option '+S.optTimeSiren[0]+' value="0" data-of="#security-control-timer">до снятия с охраны</option>\
								<option '+S.optTimeSiren[1]+' value="1" data-on="#security-control-timer">Задать время</option>\
							</select>\
						</p>\
						<br>\
						<p id="security-control-timer" class="open-clouse '+S.securitySO+'">\
							'+HWhtml.btn.numb(M.customTime,{fcn:'timeSiren',clas:'btn_time_siren',min:'null',max:9999})+'\
						</p>\
					</div>\
					<div class="colum">\
						<p>Подтверждать постановку <br> и снятие с охраны</p>\
						<p><span class="on-off btn-active '+S.confirmOnof+' bnt_canche_siren_check"></span></p>\
					</div>\
					<div class="colum">\
						<ses id="uu-sound" class="list" data-id="'+M.idSound+'" data-fcn="changeUU" data-fcnd="changeUUd">Устройство звука</ses>\
					</div>\
				</div>\
			</div>';
			
			S.print += '<div class="one-block-col">\
				<div class="title"><h4>Свет '+HWhtml.helper(HWD.helpText.prog.securityLight)+'</h4></div>\
				<div class="ltft"></div>\
				<div class="right">\
					<div class="colum">\
						<p>Режим работы света</p>\
						<br>\
						<p>\
							<select class="btn_light_control block_w_170p">\
								<option '+S.lightMode[1]+' value="1">Гореть при охране</option>\
								<option '+S.lightMode[2]+' value="2">Мигать при охране</option>\
							</select>\
						</p>\
					</div>\
					<div class="colum">\
						<p>Подтверждать постановку <br> и снятие с охраны</p>\
						<p><span class="on-off btn-active '+S.confirmOnofL+' bnt_canche_light_check"></span></p>\
					</div>\
					<div class="colum">\
						<ses id="uu-light" class="list" data-id="'+M.idLight+'" data-fcn="changeUUs" data-fcnd="changeUUsd">Устройство света</ses>\
					</div>\
				</div>\
			</div>';
			
			S.print += '<div class="one-block-col">\
				<div class="title"><h4>Устройство управления '+HWhtml.helper(HWD.helpText.prog.securityRelay)+'</h4></div>\
				<div class="ltft"></div>\
				<div class="right">\
					<div class="colum">\
						<ses id="uu-control" class="list" data-id="'+M.idRelay+'" data-fcn="changeUU2" data-fcnd="changeUU2d">Включать при постановке</ses>\
					</div>\
				</div>\
			</div>';
		}
	
		S.print += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p class="btn_text grey btn_key_tm">Ключи ТМ</p>\
				</div>\
				<div class="colum">\
					<p class="btn_text grey btn_radiobrelki">Радиобрелоки</p>\
				</div>\
			</div>\
		</div>';
		
		// отрисовка HTML 
		content.innerHTML = S.print;
		// запуск кнопок ПРограмм 
		buttonsController.start();
	};
	// Загрузка Журнала
	HWS.buffer.PageJurnal = function(){ page = 'PageJurnal';
		content.innerHTML = ''; 
		
		HWS.buffer.load_jurnal(1);
	};
	// Загрузка Доступов
	HWS.buffer.PageAccesses = function(){ page = 'PageAccesses';
		HWS.buffer.equpAccessesFCN();
	};
	
	load_all_info();

	//Упровляющие функции 
	// Изменение имени программы
	HW.on('focusin','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; nameFocus = 1;
	});
	HW.on('focusout','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; nameFocus = 1;
	});
	HW.on('input','input-name-page',function(e,el){nameFocus = 1}); HW.on('click','input-name-page',function(e,el){nameFocus = 1});
	HW.on('change','input-name-page',function(e,el){	
		nameFocus = false;	
		let name = document.body.querySelector('.ob-top .name-page input');		
		//let nameRegex = new RegExp(HWD.reg.name);	

		if(HWS.buffer.elem.config.name != name.value){
			//if(nameRegex.test(name.value)){
			if(name.value){
				M.name = name.value;
				let dataP = {token:HWS.pageAccess,id_object:id,name:M.name}
				HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
	
	// Снят поставлен 
	HW.on('click','btn_security_onof',function(e,el){
		C.active = 1;
		if(el.classList.contains('active')){M.active = 0;}
		else{ M.active = 1 ; }
		let dataP = {token:HWS.pageAccess,id_object:id,security_active:M.active}
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
		change_state();
	});
	
	//Задержка постановки на охрану
	HWS.buffer.setTime = function(n){
		C.delay = 1
		M.delay = n;
		clearTimeout(HWS.buffer.setMinsTimer);
		HWS.buffer.setMinsTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,arm_del:M.delay}
			HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
		},2000);
	}
	
	// оповещать о постановке снятии   
	/*HW.on('click','btn_notif_arm_disarm',function(e,el){
		C.disarm = 1;
		if(el.classList.contains('active')){ M.disarm = 1;}
		else{ M.disarm = 0;}
		let dataP = {token:HWS.pageAccess,id_object:id,notif_arm_disarm:M.disarm}
		console.log(dataP);
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
	});*/
	// Добавление сенсоров в лист программы
	HWS.buffer.listSensor = function(inf){
		C.list = 1;
		let dataP = {token:HWS.pageAccess,id_object:id,sensors:inf.listId}
		HWF.post({url:HWD.api.gets.taskAddSecurityProgram3xSystem,data:dataP});
	}
	// Удаение из листа устройств
	HWS.buffer.listSensorD = function(inf){
		C.list = 1; console.log(inf);
		let dataP = {token:HWS.pageAccess,id_object:id,sensors:[inf]}
		HWF.post({url:HWD.api.gets.taskDelSecurityProgram3xSystem,data:dataP});
	}
	
	
		
	// Режим работы звука
	HW.on('change','bnt_canche_siren',function(e,el){
		C.customTime = 1;
		if (el.value == 1){} // задать время
		else{ // до снятия
			let dataGo = {}; M.customTime = 0;
			dataGo.mode = (M.soundMode)?1:0;
			dataGo.arm_disarm = M.customTime;
			HWS.getElem('#security-control-timer .btn-int .inp_num_val').value = M.customTime;
			let dataP = {token:HWS.pageAccess,id_object:id,arm_mode_sound:dataGo}
			HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
		}
	}); 
	// Поддерживать постановку и снятие
	HW.on('click','bnt_canche_siren_check',function(e,el){
		C.soundMode = 1;
		let dataGo = {}
			dataGo.mode = 0;
			dataGo.arm_disarm = M.customTime;
		
		if(el.classList.contains('active')){// on
			M.soundMode = 1;
			dataGo.mode = 1;
			let dataP = {token:HWS.pageAccess,id_object:id,arm_mode_sound:dataGo}
			HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
		}
		else{// of
			M.soundMode = 0;
			dataGo.mode = 0;
			let dataP = {token:HWS.pageAccess,id_object:id,arm_mode_sound:dataGo}
			HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
		}
	});
	// Задать время работы звука
	HWS.buffer.timeSiren = function(n){
		C.customTime = 1;
		M.customTime = n;
		let dataGo = {}
			dataGo.mode = (M.soundMode)?1:0;
			dataGo.arm_disarm = M.customTime;
		clearTimeout(HWS.buffer.setMinsTimer);
		HWS.buffer.setMinsTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,arm_mode_sound:dataGo}
			HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
		},2000);
	}
	
	// Управление сиреной
	HWS.buffer.changeUU = function(inf){	
		C.idSound = 1;
		D.sound = inf;
		M.idSound = inf.local_id;
		HWS.buffer.equps[M.idSound] = D.sound;
		let dataP = {token:HWS.pageAccess,id_object:id,id_sound_device:inf.id}
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
	};
	// Удаление Управление сиреной
	HWS.buffer.changeUUd = function(inf){	
		C.idSound = 1;
		D.sound = '';
		M.idSound = '';
		HWS.buffer.equps[M.idSound] = D.sound;
		let dataP = {token:HWS.pageAccess,id_object:id,id_sound_device:'delete'}
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
	};
	
	// Управление светом
	HW.on('change','btn_light_control',function(e,el){
		C.lightDisarm = 1;
		M.lightDisarm = el.value;
		let dataGo = {}
			dataGo.mode = (M.lightMode)?1:0;
			dataGo.arm_disarm = M.lightDisarm;
		let dataP = {token:HWS.pageAccess,id_object:id,arm_mode_light:dataGo}
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
	});
	// СВЕТ Поддерживать постановку и снятие
	HW.on('click','bnt_canche_light_check',function(e,el){
		C.lightMode = 1;
		let dataGo = {}; dataGo.arm_disarm = M.lightDisarm;
		
		if(el.classList.contains('active')){M.lightMode = 1;}
		else{M.lightMode = 0;}
		
		dataGo.mode = M.lightMode;
		let dataP = {token:HWS.pageAccess,id_object:id,arm_mode_light:dataGo}
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
	});
	
	// Управление светом
	HWS.buffer.changeUUs = function(inf){	
		C.idLight = 1;
		D.light = inf;
		M.idLight = inf.local_id;
		HWS.buffer.equps[M.idLight] = D.light;
		let dataP = {token:HWS.pageAccess,id_object:id,id_light_device:inf.id}
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
	};
	// Удаление Управление светом
	HWS.buffer.changeUUsd = function(inf){	
		C.idLight = 1;
		D.light = '';
		M.idLight = '';
		HWS.buffer.equps[M.idLight] = D.light;
		let dataP = {token:HWS.pageAccess,id_object:id,id_light_device:'delete'}
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
	};
	
	// Управляющее устройство
	HWS.buffer.changeUU2 = function(inf){	
		C.idRelay = 1;
		D.relay = inf;
		M.idRelay = inf.local_id;
		HWS.buffer.equps[M.idRelay] = D.relay;
		let dataP = {token:HWS.pageAccess,id_object:id,id_extend_device:inf.id}
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
	};
	// Удаление Управляющее устройство
	HWS.buffer.changeUU2d = function(inf){	
		C.idRelay = 1
		D.relay = '';
		M.idRelay = '';
		HWS.buffer.equps[M.idRelay] = D.relay;
		let dataP = {token:HWS.pageAccess,id_object:id,id_extend_device:'delete'}
		HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP});
	};	
	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			M.viget = 1;
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/add_favorite_objects',data:dataP});
		}else { // Of
			M.viget = 0;
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/delete_favorite_objects',data:dataP});
		}
	});
	
	
	
	// Нажатие на кнопку "Ключи тачь мемори"
	HW.on('click','btn_key_tm',function(e,el){
		let print = '<div class="table-content block_w_100 block_h_85 list-equp-no-select"></div>';
		HWF.modal.print('Выбор Оборудования',print);
		HWS.data.apiGet.equipments.data = {"local_id_object_type":"2","local_id_object_group":null,"iface_group":null,"branch":7,"id_system":M.elem.id_system}
		HWF.print.equipments('.modal-window .modal-section .table-content',{a:'on'});
		HWF.clearFilter();
	});
	// Нажатие на кнопку "Радио Брелки"
	HW.on('click','btn_radiobrelki',function(e,el){
		let print = '<div class="table-content block_w_100 block_h_85 list-equp-no-select"></div>';
		HWF.modal.print('Выбор Оборудования',print);
		HWS.data.apiGet.equipments.data = {"local_id_object_type":"2","local_id_object_group":null,"iface_group":null,"branch":8,"id_system":M.elem.id_system}
		HWF.print.equipments('.modal-window .modal-section .table-content',{a:'on'});
		HWF.clearFilter();
	});
		
	//select all
	HW.on('click','equp_select_all',function(event,elem){
		HWF.allActive('.list-equipments','.checkbox');
	});
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(); // Проверит ваш ли это объект для вовода предупреждения
});