"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#prog-content-top');
	let content = document.body.querySelector('#prog-content-mid');
	let token = HWS.pageAccess;
	let page = 'PageSets';
	let id = H.hash[1] || false;
	let ET = HWS.buffer.elemEt || false;
	let D = {}; // Дада массив с ОБЪЕКТАМИ от сервера
	let M = {}; // Отрисовочный объект с нужными данными
	let C = {}; // якоря
	let nameFocus = false;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="PageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageAccesses">Доступ</span></div>';
	B.menud.append(menu);
	
	/* FUNCTION */
	function load_all_info(rInfo){
		D.elem = HWS.buffer.elem;
		M = creator_m(D);// создаём адекватное структурирование данных
		print_header();// печатем шапку страници
		HWS.buffer[page]();// печатаем вкладку на которой находимся	
		HWF.timerUpdate(reolad_info_page); //Авто обновление
	}
	
	function creator_m(CD){
		let RM = {};
		
		RM.elem = CD.elem;
		RM.name = CD.elem.config.name;
		RM.elemType = CD.elem.info.type.type;
		RM.elemTitle = ET.title;
		RM.systemName = CD.elem.system_name;
		RM.systemIdent = CD.elem.system_ident;
		RM.gnotif = CD.elem.config.cfg_gnotif; // Оповещения системы ( оно же статус )
		RM.userTel = CD.elem.config.usr_tel; // Телефоны пользователей
		RM.alive = CD.elem.config.cfg_alive; // Режим автооповещений
		RM.recal = CD.elem.config.cfg_recal; // Если нет ответа на звонок
		RM.retnorm = CD.elem.config.cfg_retnorm; // Оповещать об окончании тревоги
		RM.hours = CD.elem.config.cfg_atime[0];
		RM.mins = CD.elem.config.cfg_atime[1];
		
		RM.state = CD.elem.lk;
		
		return RM;
	}

	function change_state(){ HWF.timerUpdate(reolad_info_page,'',100); }  //Авто обновление через 100 милисекунд
	function re_print_state(MNew){
		let state = MNew.state.state;
		let name = MNew.state.state_name;
		let S = {};
		let nameStat = HWS.getElem('#name-status inf')
		let blamba = HWS.getElem('#blamba')
		let btn = HWS.getElem('.btn_alerts_all_user')
		
		S.blambaColor = HWD.state.prog[state].colorBlamba;
		S.stateText = name;
		S.stateTextColor = HWD.state.prog[state].colorText;
		
		blamba.style.background = S.blambaColor;
		nameStat.style.color = S.stateTextColor;
		nameStat.innerHTML = S.stateText;
		
		if(state == 114){btn.classList.add('active')}
		else {btn.classList.remove('active')}
	}
	
	function reolad_info_page(){if(page && page == 'PageSets'){ HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(rEl){
		let DNew =  {}; DNew.elem = rEl; D = DNew;
			let MNew = creator_m(DNew);// создаём адекватное структурирование данных
			let S = {};
			// Имя
			if(!nameFocus && M.name != MNew.name){ M.name = MNew.name; HWS.getElem('#prog-name').value = MNew.name;}
			// Режим автооповещений
			if(!C.alive && M.alive != MNew.alive){ M.alive = MNew.alive; 
				HWS.getElem('.btn_auto_alerts').value = MNew.alive;
				if(MNew.alive){HWS.getElem('#time-auto-alerts').classList.add('open')}
				else {HWS.getElem('#time-auto-alerts').classList.remove('open')}
			}
			// Если нет ответа на звонок
			if(!C.recal && M.recal != MNew.recal){ M.recal = MNew.recal; HWS.getElem('.btn_recal_tell').value = MNew.recal; }
			// Если нет ответа на звонок
			if(!C.retnorm && M.retnorm != MNew.retnorm){ M.retnorm = MNew.retnorm; 
				S.retnorm = HWS.getElem('.btn_alerts_return_norm');
				if(MNew.retnorm) {S.retnorm.classList.add('active')}
				else {S.retnorm.classList.remove('active')}
			}
			// Время автооповещений, ч
			if(!C.hours && M.hours != MNew.hours){ M.hours = MNew.hours; HWS.getElem('#noti-time-h .inp_num_val').value = MNew.hours; }
			// Время автооповещений, м
			if(!C.mins && M.mins != MNew.mins){ M.mins = MNew.mins; HWS.getElem('#noti-time-m .inp_num_val').value = MNew.mins	; }
			
			// Пользователели
			if(!C.users){
				for (let i = 0;i <= 9;i++){
					let li = HWS.getElem('#one-user-'+i);
					if(MNew.userTel[i]){
						if(li){
							let tel = li.querySelector('.sys3x-t input').value = MNew.userTel[i].phone_number;
							let name = li.querySelector('.sys3x-n .name input').value = MNew.userTel[i].name;
							let voice = li.querySelector('.onof_voice');
							let sms = li.querySelector('.onof_sms');
							let auto = li.querySelector('.onof_auto');
							let def = li.querySelector('.onof_security');
							
							if(MNew.userTel[i].call_alarm){voice.classList.add('active')}
							else {voice.classList.remove('active')}
							
							if(MNew.userTel[i].sms_alarm){sms.classList.add('active')}
							else {sms.classList.remove('active')}
							
							if(MNew.userTel[i].period_sms){auto.classList.add('active')}
							else {auto.classList.remove('active')}
							
							if(MNew.userTel[i].scurity_sms){def.classList.add('active')}
							else {def.classList.remove('active')}
						}
						else {
							let listLi = HWS.getElem('#user-tel-list');
							let newLi = document.createElement('li');
								newLi.classList.add('one-user')
								newLi.id = 'one-user-'+i;
								newLi.dataset.num = i;
								
							let voice = (MNew.userTel[i].call_alarm)?'active':''; 
							let sms = (MNew.userTel[i].sms_alarm)?'active':''; 
							let auto = (MNew.userTel[i].period_sms)?'active':''; 
							let def = (MNew.userTel[i].scurity_sms)?'active':'';
							let number = MNew.userTel[i].phone_number || '';
							let name = MNew.userTel[i].name;
							
							newLi.innerHTML = ''+
							'<div class="block_w_40p"><p class="ta_center">'+i+'</p></div>'+
							'<div class="sys3x-n">'+
								'<span class="name"><input type="text" value="'+name+'" disabled ></span>'+
							'</div>'+
							'<div class="sys3x-t">\
								<span class="vertical-middle">+</span>\
								<input class="block_w_130p user_number" type="text" value="'+number+'" disabled maxlength="12">\
							</div>'+
							'<div class="sys3x-o elements-around">'+
								'<span class="btn-active icon hover btn_onof_check icon-send-voice onof_voice '+voice+'"></span>'+
								'<span class="btn-active icon hover btn_onof_check icon-send-sms onof_sms '+sms+'"></span>'+
								'<span class="btn-active btn_onof_check icon hover icon-send-sms-auto onof_auto '+auto+'"></span>'+
								'<span class="btn-active btn_onof_check icon hover icon-send-sms-secure onof_security '+def+'"></span>'+
							'</div>'+
							'<div class="sys3x-b">'+
								//'<span class="icon icon-change hover btn_user_tell_cange block_mr_8"></span>'+
								'<span class="icon icon-clouse hover btn_user_tell_delete"></span>'+
							'</div>';
							
							listLi.appendChild(newLi);
						}
					}
					else {if (li){li.remove();}}
				}
			}

			if(JSON.stringify(C) == "{}"){M = MNew;}
			re_print_state(MNew);
			C = {} // очищение якорей
		}}); }
	}
	
	// спец лист пользывателей для старых систем  ☎ ✉ ◴ ☖
	function printUserSystem3x(list){ list = list || {};
		let S = {};
		S.print = '<ul id="user-tel-list" class="list no-hover block-w-100 list-user-system-3x">';
		S.startLi = '<li class="one-head">'+
			'<div class="block_w_40p">Ячейка</div>'+
			'<div class="sys3x-n"><span class="name">Имя</span></div>'+
			'<div class="sys3x-t">Номер</div>'+
			'<div class="sys3x-o">Типы оповещений</div>'+
			'<div class="sys3x-b"></div>'+
		'</li>';
		S.AllUser = '';
		for (let key in list){
			let user = list[key];
			let btn = {}
			let tel = user.tel; //.slice(1);
			
			btn.voice = ''; if(user.btn.voice){btn.voice = 'active'}
			btn.sms = ''; if(user.btn.sms){btn.sms = 'active'}
			btn.auto = ''; if(user.btn.auto){btn.auto = 'active'}
			btn.security = ''; if(user.btn.security){btn.security = 'active'}
			
			S.AllUser += ''+
			'<li id="one-user-'+user.id+'" class="one-user" data-num="'+user.id+'">'+
				'<div class="block_w_40p"><p class="ta_center">'+user.id+'</p></div>'+
				'<div class="sys3x-n">'+
					'<span class="name"><input class="enter_click_list_user pointer" type="text" value="'+user.name+'" disabled ></span>'+
				'</div>'+
				'<div class="sys3x-t">\
					<input class="enter_click_list_user phone_inp_text block_w_130p pointer" type="text" value="'+tel+'" disabled maxlength="20">\
				</div>'+
				'<div class="sys3x-o elements-around">'+
					'<span class="btn-active icon hover btn_onof_check icon-send-voice onof_voice '+btn.voice+'"></span>'+
					'<span class="btn-active icon hover btn_onof_check icon-send-sms onof_sms '+btn.sms+'"></span>'+
					'<span class="btn-active btn_onof_check icon hover icon-send-sms-auto onof_auto '+btn.auto+'"></span>'+
					'<span class="btn-active btn_onof_check icon hover icon-send-sms-secure onof_security '+btn.security+'"></span>'+
				'</div>'+
				'<div class="sys3x-b">'+
					'<span class="icon icon-clouse hover btn_user_tell_delete"></span>'+
				'</div>'+
			'</li>';
		}
		S.dopBtnPrint = '<div class="block_w_100 block_pt_20">'+
			'<span class="btn_text grey btn_add_user_system_3x block_pl_10">Добавить</span>'+
		'</div>';
		
		S.print = S.print + S.startLi + S.AllUser + '</ul>'+S.dopBtnPrint;
		return S.print;
	}
	
	
	function print_header(){
		let S = {};
		let state = M.state.state;
		let name = M.state.state_name;
		
		S.blambaColor = HWD.state.prog[state].colorBlamba;
		S.stateText = name;
		S.stateTextColor = HWD.state.prog[state].colorText;

		let print = '\
		<div class="one-block ob-top page-equp-top">\
			<div class="name-page">\
				<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
				<input id="prog-name" type="text" class="input-name-page" value="'+M.name+'">\
				<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
			</div>\
			<div class="info-li block_w_100 flex_center_left">\
				<div id="blamba" style="background:'+S.blambaColor+'"></div>\
				<div class="page-top-dop-info tf_fr500 block-title-name1colum">\
					<div id="name-status" class="data"><inf style="color:'+S.stateTextColor+'">'+S.stateText+'</inf></div>\
					<div id="manual-mode-info" class="dop-info-name"></div>\
				</div>\
				<div class="block-title-colum"></div>\
				<div class="page-top-dop-info-title block-title-colum310">\
					<p>Принадлежит</p>\
					<p><span id="name-system" class="tf_fr500">'+M.systemName+'</span></p>\
					<p><span class="tf_fr500">'+M.systemIdent+'</span></p>\
				</div>\
				<div id="iconr" class="flex_center_left">\
					<span class="block_w_150p block_pr_10">'+M.elemTitle+'</span>\
					<span class="icon p35 icon icon_equip_'+M.elemType+'"></span>\
				</div>\
			</div>\
		</div>\
		';
		contentTop.innerHTML = print;
		HWS.buffer.share_stick(M.elem);
	}
	
	HWS.buffer.PageSets = function(){ page = 'PageSets';
		let print = '';
		let S = {};
		S.users = [];
		HWF.recount(M.userTel,function(el,key){
			S.users.push({
				id:key,
				name:el.name || '',
				tel: el.phone_number || '',
				btn: {
					sms: el.sms_alarm ,
					auto: el.period_sms ,
					voice: el.call_alarm ,
					security: el.scurity_sms ,
				}
			});
		});
		
		S.state = (M.gnotif)?'active':'';
		S.auto = {}; S.auto[M.alive] = 'selected';
		S.callB = {}; S.callB[M.recal] = 'selected';
		S.alertsOpen = (M.alive)?'open':'';
		S.stateNorm = (M.retnorm)?'active':'';
		
		print += '<div class="one-block-col">\
			<div class="title"><h4>Глобальные <span title="" class="helper">?</span></h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Оповещения системы</p>\
					<br>\
					<p><span class="on-off btn-active '+S.state+' btn_alerts_all_user"></span></p>\
				</div>\
				<div class="colum">\
					<p>Режим автооповещений</p>\
					<br>\
					<p>\
					<select class="open_control btn_auto_alerts block_w_170p">\
					<option '+S.auto[0]+' data-of="#time-auto-alerts" value="0">Нет оповещений</option>\
					<option '+S.auto[1]+' data-on="#time-auto-alerts" value="1">Раз в 1 день</option>\
					<option '+S.auto[2]+' data-on="#time-auto-alerts" value="2">Раз в 2 дня</option>\
					<option '+S.auto[3]+' data-on="#time-auto-alerts" value="3">Раз в 3 дня</option>\
					<option '+S.auto[4]+' data-on="#time-auto-alerts" value="4">Раз в 4 дня</option>\
					<option '+S.auto[5]+' data-on="#time-auto-alerts" value="5">Раз в 5 дней</option>\
					<option '+S.auto[6]+' data-on="#time-auto-alerts" value="6">Раз в 6 дней</option>\
					<option '+S.auto[7]+' data-on="#time-auto-alerts" value="7">Раз в неделю</option>\
					<option '+S.auto[9]+' data-on="#time-auto-alerts" value="9">2 раза в день</option>\
					</select>\
					</p>\
				</div>\
				<div class="colum">\
					<p>Если нет ответа на звонок</p>\
					<br>\
					<select class="btn_recal_tell block_w_170p">\
					<option '+S.callB[0]+' value="0">Не перезванивать</option>\
					<option '+S.callB[1]+' value="1">Перезвонить 1 раз</option>\
					<option '+S.callB[2]+' value="2">Перезвонить 2 раза</option>\
					<option '+S.callB[3]+' value="3">Перезвонить 3 раза</option>\
					<option '+S.callB[4]+' value="4">Перезвонить 4 раза</option>\
					<option '+S.callB[5]+' value="5">Перезвонить 5 раз</option>\
					</select>\
				</div>\
				<div class="colum">\
					<p>Оповещать об окончании тревоги</p>\
					<br>\
					<p><span class="on-off btn-active '+S.stateNorm+' btn_alerts_return_norm"></span></p>\
				</div>\
				<div id="time-auto-alerts" class="block-onof '+S.alertsOpen+'">\
					<div class="colum">\
						<p>Время автооповещений, ч</p>\
						<br>\
						<p>'+HWhtml.btn.numb(M.hours,{fcn:'setTime',id:'noti-time-h',min:'null',max:23})+'</p>\
					</div>\
					<div class="colum">\
						<p>Время автооповещений, мин</p>\
						<br>\
						<p>'+HWhtml.btn.numb(M.mins,{fcn:'setTime',id:'noti-time-m',min:'null',max:59})+'</p>\
					</div>\
				</div>\
			</div>\
		</div>';
		
		print += '<div class="one-block-col">\
			<div class="title"><h4>Пользователи <span title="" class="helper">?</span></h4></div>\
			<div class="ltft"></div>\
			<div class="right"></div>\
			<div class="title">\
				'+printUserSystem3x(S.users)+'\
			</div>\
			<div class="title">\
				<div class="block-pr-10 block-w-100 all-span-midle ta_right">\
					<span class="block-p-10">\
						<span class="icon icon-send-voice"></span>\
						<span class="text-grey">голосом</span>\
					</span>\
					<span class="block-p-10">\
						<span class="icon icon-send-sms"></span>\
						<span class="text-grey">по SMS</span>\
					</span>\
					<span class="block-p-10">\
						<span class="icon icon-send-sms-auto"></span>\
						<span class="text-grey">автоотчеты в SMS</span>\
					</span>\
					<span class="block-p-10">\
						<span class="icon icon-send-sms-secure"></span>\
						<span class="text-grey">SMS о состоянии охраны</span>\
					</span>\
				</div>\
			</div>\
		</div>';
		
		// отрисовка HTML 
		content.innerHTML = print;
	}
	HWS.buffer.PageJurnal = function(){ page = 'PageJurnal';
		let print = '';
		content.innerHTML = ''; 
		
		HWS.buffer.load_jurnal(1);
	}
	HWS.buffer.PageAccesses = function(){ page = 'PageAccesses';
		HWS.buffer.equpAccessesFCN();
	}
	
	load_all_info();
	
	//Упровляющие функции 
	// Редактирование пользователя
	function clouse_active(liActive){let ret = 'of'; if(liActive){
		C.users = 1;
		let tel = liActive.querySelector('.sys3x-t input');
		let name = liActive.querySelector('.sys3x-n .name input');
		let number = tel.value || '';

		liActive.classList.remove('redact');
		tel.classList.remove('red');
		name.disabled = true;
		tel.disabled = true;
		ret = 'on';
		
		let n = liActive.dataset.num;
		let voice = (liActive.querySelector('.onof_voice').classList.contains('active'))?1:0;
		let sms = (liActive.querySelector('.onof_sms').classList.contains('active'))?1:0;
		let auto = (liActive.querySelector('.onof_auto').classList.contains('active'))?1:0;
		let def = (liActive.querySelector('.onof_security').classList.contains('active'))?1:0;
		let dataP = {token:HWS.pageAccess,id_object:id,
			number:n,
			name:name.value,
			phone:number,
			sms_alarm:sms,
			call_alarm:voice,
			period_sms:auto,
			scurity_sms:def
		}
		HWF.post({url:HWD.api.gets.taskPhoneNotificationProgram3xSystem,data:dataP});
		
	} return ret; }
	
	HWS.buffer.globalClick = function(e){
		let liActive = HWS.getElem('#user-tel-list .one-user.redact');
		
		if(e.target.classList.contains('enter_click_list_user')){
			C.users = 1;
			let li = HWS.upElem(e.target,'.one-user');
			
			if(!li.classList.contains('redact')){
				let tel = li.querySelector('.sys3x-t input');
				let name = li.querySelector('.sys3x-n .name input');
				let controller = '';
				li.dataset.nb = tel.value;
				controller = clouse_active(liActive);

				if(controller != 'error'){
					li.classList.add('redact');
					name.disabled = false;
					tel.disabled = false;
				}
			}
		} else {
			clouse_active(liActive);
		}
		//console.log(e);
		//console.log(1111);
	}
	// ВКЛ\ОТКЛ оповещение
	HW.on('click','btn_onof_check',function(e,el){
		C.users = 1;
		let li = HWS.parentSearchClass(el,'one-user');
		let n = li.dataset.num;
		let tel = li.querySelector('.sys3x-t input').value;
		let voice = (li.querySelector('.onof_voice').classList.contains('active'))?1:0;
		let sms = (li.querySelector('.onof_sms').classList.contains('active'))?1:0;
		let auto = (li.querySelector('.onof_auto').classList.contains('active'))?1:0;
		let def = (li.querySelector('.onof_security').classList.contains('active'))?1:0;

		let dataP = {token:HWS.pageAccess,id_object:id,
			number:n,
			phone:tel,
			sms_alarm:sms,
			call_alarm:voice,
			period_sms:auto,
			scurity_sms:def
		}
		HWF.post({url:HWD.api.gets.taskPhoneNotificationProgram3xSystem,data:dataP,fcnE:HWF.getServMesErr});
	});
	
	// Кнопка удалить пользователя
	HW.on('click','btn_user_tell_delete',function(e,el){
		let li = HWS.parentSearchClass(el,'one-user');
		let liN = li.dataset.num;
		let tel = li.querySelector('.sys3x-t input').value;
		let name = li.querySelector('.sys3x-n .name input').value;
		HWS.buffer.idDEletePhone = liN;
		HWF.modal.confirm('Удалить ?<br>'+name+'  '+tel,'Удаление номера','btn_delete_user');
	});
	HW.on('click','btn_delete_user',function(e,el){ //
		document.querySelector('#one-user-'+HWS.buffer.idDEletePhone).remove();
		let dataP = {token:HWS.pageAccess,id_object:id,number:HWS.buffer.idDEletePhone,}
		HWF.post({url:HWD.api.gets.taskDelPhoneNotificationProgram3xSystem,data:dataP,fcnE:HWF.getServMesErr});
	});
	
	// Кнопка добавить нового пользователя
	HW.on('click','btn_add_user_system_3x',function(e,el){
		let html = '';
		
		html += '<ul class="dop-inp-info ta_center">'
		html += '<li><span>Имя:</span><input id="noti-new-name" type="text" value=""></li>'
		html += '<li><p class="block_pt_10 ta_center"></p></li>'
		html += '<li>\
			<span>Телефон: 7 123 45 67 890</span> \
			<div>\
				<input class="phone_inp_text" id="noti-new-tel" type="text" maxlength="20" value="">\
			</div>\
		</li>'
		html += '<li><p class="block_pt_10 ta_center"></p></li>'
		html += '<li><span>Оповещения:</span><div class="sys3x-o elements-around">'+
			'<span id="noti-new-voice" class="btn-active icon hover block_m_4 icon-send-voice"></span>'+
			'<span id="noti-new-sms"   class="btn-active icon hover block_m_4 icon-send-sms"></span>'+
			'<span id="noti-new-auto"  class="btn-active icon hover block_m_4 icon-send-sms-auto"></span>'+
			'<span id="noti-new-def"   class="btn-active icon hover block_m_4 icon-send-sms-secure"></span>'+
		'</div></li>';
		html += '</ul>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<p id="error-mesage-modal" class="tc_red block_h_18p"></p>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<div class="style_btn orange block_w_200p btn_add_new_tel">Добавить</div>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<div class="block-pr-10 block-w-100 all-span-midle ta_center">\
			<span class="block-p-10">\
				<span class="icon icon-send-voice"></span>\
				<span class="text-grey">голосом</span>\
			</span>\
			<span class="block-p-10">\
				<span class="icon icon-send-sms"></span>\
				<span class="text-grey">по SMS</span>\
			</span>\
			<span class="block-p-10">\
				<span class="icon icon-send-sms-auto"></span>\
				<span class="text-grey">автоотчеты в SMS</span>\
			</span>\
			<span class="block-p-10">\
				<span class="icon icon-send-sms-secure"></span>\
				<span class="text-grey">SMS о состоянии охраны</span>\
			</span>\
		</div>';
		
		HWF.modal.print('Добавить пользователя',html);
	})//Добавление нового номера
	HW.on('click','btn_add_new_tel',function(e,el){
		let errorB = document.body.querySelector('#error-mesage-modal');
		let name = document.body.querySelector('#noti-new-name').value;
		let voice = (document.body.querySelector('#noti-new-voice').classList.contains('active'))?1:0;
		let sms = (document.body.querySelector('#noti-new-sms').classList.contains('active'))?1:0;
		let auto = (document.body.querySelector('#noti-new-auto').classList.contains('active'))?1:0;
		let def = (document.body.querySelector('#noti-new-def').classList.contains('active'))?1:0;
			errorB.innerHTML = '';
		let number = document.body.querySelector('#noti-new-tel').value;
			//number = String(number);
		let n=0;
		while(M.userTel[n]){n++;}
		
		if (n <= 9) {
			let list = document.body.querySelector('.list-user-system-3x');
			let newLi = document.createElement('li');
				newLi.classList.add('one-user')
				newLi.id = 'one-user-'+n;
				newLi.dataset.num = n;
			let dataP = {token:HWS.pageAccess,id_object:id,
				number:n,
				name:name,
				phone:number,
				sms_alarm:sms,
				call_alarm:voice,
				period_sms:auto,
				scurity_sms:def
			}
			HWF.post({url:HWD.api.gets.taskPhoneNotificationProgram3xSystem,data:dataP,fcnE:HWF.getServMesErr});
			
			voice = (voice)?'active':''; sms = (sms)?'active':''; auto = (auto)?'active':''; def = (def)?'active':'';
			newLi.innerHTML = ''+
			'<div class="block_w_40p"><p class="ta_center">'+n+'</p></div>'+
			'<div class="sys3x-n">'+
				'<span class="name"><input class="enter_click_list_user" type="text" value="'+name+'" disabled ></span>'+
			'</div>'+
			'<div class="sys3x-t">\
				<input class="block_w_130p phone_inp_text enter_click_list_user" type="text" value="'+number+'" disabled maxlength="20">\
			</div>'+
			'<div class="sys3x-o elements-around">'+
				'<span class="btn-active icon hover btn_onof_check icon-send-voice onof_voice '+voice+'"></span>'+
				'<span class="btn-active icon hover btn_onof_check icon-send-sms onof_sms '+sms+'"></span>'+
				'<span class="btn-active btn_onof_check icon hover icon-send-sms-auto onof_auto '+auto+'"></span>'+
				'<span class="btn-active btn_onof_check icon hover icon-send-sms-secure onof_security '+def+'"></span>'+
			'</div>'+
			'<div class="sys3x-b">'+
				//'<span class="icon icon-change hover btn_user_tell_cange block_mr_8"></span>'+
				'<span class="icon icon-clouse hover btn_user_tell_delete"></span>'+
			'</div>';
			
			list.appendChild(newLi);
			HWF.modal.off();
		}else {
			errorB.innerHTML = 'Все ячейки для номеров заняты';
			setTimeout(function(){errorB.innerHTML = '';},3000);
		}
	});
	
	// Изменение имени программы
	HW.on('focusin','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; nameFocus = 1;
	});
	HW.on('focusout','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; nameFocus = 1;
	});
	HW.on('input','input-name-page',function(e,el){nameFocus = 1}); HW.on('click','input-name-page',function(e,el){nameFocus = 1});
	HW.on('change','input-name-page',function(e,el){	
		nameFocus = false;
		let name = document.body.querySelector('.ob-top .name-page input');	
		//let nameRegex = new RegExp(HWD.reg.name);	

		if(HWS.buffer.elem.config.name != name.value){
			//if(nameRegex.test(name.value)){
			if(name.value){
				let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
				HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
	//Изменение Минут и часов оповещения
	HWS.buffer.setTime = function(n){
		let H = document.body.querySelector('#noti-time-h input').value;
		let M = document.body.querySelector('#noti-time-m input').value;
		C.hours = 1; C.mins = 1;
		clearTimeout(HWS.buffer.setMinsTimer);
		HWS.buffer.setMinsTimer = setTimeout(function(){
			let time = [H,M];
			let dataP = {token:HWS.pageAccess,id_object:id,cfg_atime:time}
			HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
		},2000);
	}
	// изменение оповещений для всех
	HW.on('change','btn_auto_alerts',function(e,el){
		C.alive = 1;
		M.alive = el.value;
		let dataP = {token:HWS.pageAccess,id_object:id,cfg_alive:M.alive}
		HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
	});	
	// изменение оповещений для всех
	HW.on('change','btn_recal_tell',function(e,el){
		C.recal = 1;
		M.recal = el.value;
		let dataP = {token:HWS.pageAccess,id_object:id,cfg_recal:M.recal}
		HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
	});
	// Оповещения о возврате в норму
	HW.on('click','btn_alerts_return_norm',function(e,el){
		C.retnorm = 1;
		if(el.classList.contains('active')){M.retnorm = 1;}
		else{ M.retnorm = 0; }
		let dataP = {token:HWS.pageAccess,id_object:id,cfg_retnorm:M.retnorm}
		HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
	});
	// изменение оповещений для всех
	HW.on('click','btn_alerts_all_user',function(e,el){
		if(el.classList.contains('active')){M.gnotif = 1}
		else{M.gnotif = 0}
		let dataP = {token:HWS.pageAccess,id_object:id,cfg_gnotif:M.gnotif}
		HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
		change_state();
	});
	
	// Ввод тел номеров 
	HWS.on('click','phone_inp_text',function(e,el){if(!el.value){el.value = '+'}});
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(); // Проверит ваш ли это объект для вовода предупреждения
});