"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#prog-content-top');
	let content = document.body.querySelector('#prog-content-mid');
	let token = HWS.pageAccess;
	let page = 'PageSets';
	let id = H.hash[1] || false; if(id){id = parseInt(id)}
	let ET = HWS.buffer.elemEt || false;
	let D = {}; // Дада массив с ОБЪЕКТАМИ от сервера
	let M = {}; // Отрисовочный объект с нужными данными
	let C = {}; // якоря
	let nameFocus = false;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="PageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageAccesses">Доступ</span></div>';
	B.menud.append(menu);
	
	/* FUNCTION */
	function load_all_info(rInfo){
		D.elem = HWS.buffer.elem;
		D.relay = D.elem.config.automatic_device || ''; //Устройство управления 
		M = creator_m(D);// создаём адекватное структурирование данных
		print_header();// печатем шапку страници
		HWS.buffer[page]();// печатаем вкладку на которой находимся
		HWF.timerUpdate(reolad_info_page); //Авто обновление
	}
	
	function creator_m(CD){
		let RM = {};
		RM.elem = CD.elem;
		RM.name = CD.elem.config.name;
		RM.elemType = CD.elem.info.type.type;
		RM.elemTitle = ET.title;
		RM.systemName = CD.elem.system_name;
		RM.systemIdent = CD.elem.system_ident;
		RM.systemTime = CD.elem.system_connection_time;
		RM.manualMode = CD.elem.config.flags.active; // ручной режим
		RM.viget = CD.elem.lk.favorite || ''; // в виджеты
		RM.idRelay = ''; // Устройство управления
		RM.tempTimetableDat = buttonsController.createrTimetableInArray(CD.elem.config.pau_shedl_time); // правильная дата для таблице  расписание
		
		HWS.buffer.equps = {};
		if(CD.relay){
			RM.idRelay = CD.relay.id || ''; // Устройство управления
			HWS.buffer.equps[RM.idRelay] = CD.relay;
		}
		
		RM.state = CD.elem.lk;
		
		return RM;
	}
	
	function change_state(){ HWF.timerUpdate(reolad_info_page,'',100); }  //Авто обновление через 100 милисекунд
	function re_print_state(MNew){
		let state = MNew.state.state;
		let name = MNew.state.state_name;
		let S = {};
		let nameStat = HWS.getElem('#name-status inf')
		let blamba = HWS.getElem('#blamba')
		
		S.blambaColor = HWD.state.prog[state].colorBlamba;
		S.stateText = name;
		S.stateTextColor = HWD.state.prog[state].colorText;
		
		blamba.style.background = S.blambaColor;
		nameStat.style.color = S.stateTextColor;
		nameStat.innerHTML = S.stateText;
	}
	
	function reolad_info_page(){if(page && page == 'PageSets'){ HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(rEl){
		let DNew =  {}; DNew.elem = rEl;
			DNew.relay = D.elem.config.automatic_device, //Устройство управления 
			D = DNew;
			let MNew = creator_m(DNew);// создаём адекватное структурирование данных
			let S = {};
			// Имя
			if(!nameFocus && M.name != MNew.name){ M.name = MNew.name; HWS.getElem('#prog-name').value = MNew.name;}
			//Ручной режим
			if(!C.manualMode && M.manualMode != MNew.manualMode ){M.manualMode = MNew.manualMode; 
				let btnMode = HWS.getElem('.btn_onof_mode');
				let textPole = HWS.getElem('#manual-mode-info');
				if(M.manualMode){ btnMode.classList.remove('active'); textPole.innerHTML = ''; }
				else { btnMode.classList.add('active'); textPole.innerHTML = 'Вручную';}
			}
			// Таблица расписания
			if(!C.tempTimetable && MNew.tempTimetableDat != M.tempTimetableDat){
				buttonsController.updateTimetable('#timetable-temp',MNew.tempTimetableDat);
			}
			// обновление реле
			if(!C.relay){buttonsController.updateSes('#uu-relay',MNew.idRelay);}
			//(!C.idRelay){buttonsController.updateSes('#change-uuo',MNew.idRelay);}
			// В виджеты
			if(!C.favorite && M.viget != MNew.viget){ M.viget = MNew.viget;
				let favorite = HWS.getElem('#equp-viget');
				(MNew.viget)?favorite.classList.add('active'):favorite.classList.remove('active');
			}
			//Время в системе
			let systemTime = HWS.getElem('#equp-system-time');
			systemTime.innerHTML = HWF.unixTime(M.systemTime,true).full

			if(JSON.stringify(C) == "{}"){M = MNew;}
			re_print_state(MNew);
			C = {} // очищение якорей
		}}); }
	}
		
	function print_header(){
		let state = M.state.state;
		let name = M.state.state_name;
		let S = {};
		S.manualModeText = (M.manualMode)?'':'Вручную';
		S.blambaColor = HWD.state.prog[state].colorBlamba;
		S.stateText = name;
		S.stateTextColor = HWD.state.prog[state].colorText;
		
		let print = '\
		<div class="one-block ob-top page-equp-top">\
			<div class="name-page">\
				<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
				<input id="prog-name" type="text" class="input-name-page" value="'+M.name+'">\
				<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
			</div>\
			<div class="info-li block_w_100 flex_center_left">\
				<div id="blamba" style="background:'+S.blambaColor+'"></div>\
				<div class="page-top-dop-info tf_fr500 block-title-name1colum">\
					<div id="name-status" class="data"><inf style="color:'+S.stateTextColor+'">'+S.stateText+'</inf></div>\
					<div id="manual-mode-info" class="dop-info-name">'+S.manualModeText+'</div>\
				</div>\
				<div class="block-title-colum"></div>\
				<div class="page-top-dop-info-title block-title-colum310">\
					<p>Принадлежит</p>\
					<p><span id="name-system" class="tf_fr500">'+M.systemName+'</span></p>\
					<p><span class="tf_fr500">'+M.systemIdent+'</span></p>\
				</div>\
				<div id="iconr" class="flex_center_left">\
					<span class="block_w_150p block_pr_10">'+M.elemTitle+'</span>\
					<span class="icon p35 icon icon_equip_'+M.elemType+'"></span>\
				</div>\
			</div>\
		</div>\
		';
		contentTop.innerHTML = print;
		HWS.buffer.share_stick(M.elem);
	}
	
	/* PAGES */
	HWS.buffer.PageSets = function(){ page = 'PageSets';
		let S = {};
		
		S.print = '';
		S.manualModeAct = (M.manualMode)?'':'active';
		S.favorite = (M.viget)?'active':'';
		S.TimeSys = HWF.unixTime(M.systemTime,true); console.log(S.TimeSys);

		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Ручной режим '+HWhtml.helper(HWD.helpText.prog.shedulerManualMode)+'</p>\
					<p><span class="on-off btn-active '+S.manualModeAct+' btn_onof_mode"></span></p>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.prog.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+S.favorite+'" ></span>\
				</div>\
				<div class="colum">\
					<p>Текущее время в системе:</p>\
					<span id="equp-system-time" class="tw_600" >'+S.TimeSys.full+'</span>\
				</div>\
				<div class="columFull">\
					<p>\
						<timetable id="timetable-temp" data-fcn="timetableUpFcn" type="custom" step=" ,00:30, ,01:30, ,02:30, ,03:30, ,04:30, ,05:30, ,06:30, ,07:30, ,08:30, ,09:30, ,10:30, ,11:30, ,12:30, ,13:30, ,14:30, ,15:30, ,16:30, ,17:30, ,18:30, ,19:30, ,20:30, ,21:30, ,22:30, ,23.30" size="19" data-data="'+M.tempTimetableDat+'">\
							<val value="0" color="#e0e0e0">Откл</val>\
							<val value="1" color="#569AD4">Вкл</val>\
						</timetable>\
					</p>\
				</div>\
			</div>\
		</div>';
		
		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Управление '+HWhtml.helper(HWD.helpText.prog.shedulerControl)+'</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p><ses id="uu-relay" class="list" data-id="'+M.idRelay+'" data-load="loadUU" data-fcn="changeUU" data-fcnd="changeUUd" data-filter="relay">Устройство управления</ses></p>\
				</div>\
			</div>\
		</div>';
		
		S.print += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin"></p>\
					<p class="btn_text grey btn_delete_prog">Удалить программу</p>\
				</div>\
			</div>\
		</div>';
		
		// отрисовка HTML 
		content.innerHTML = S.print;
		// запуск кнопок ПРограмм
		buttonsController.start();
	}
	
	HWS.buffer.PageJurnal = function(){ page = 'PageJurnal';
		let print = '';
		content.innerHTML = ''; 
		
		HWS.buffer.load_jurnal(1);
	}
	HWS.buffer.PageAccesses = function(){ page = 'PageAccesses';
		HWS.buffer.equpAccessesFCN();
	}
	
	load_all_info();
	
	//Упровляющие функции 
	// Изменение имени программы
	HW.on('focusin','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; nameFocus = 1;
	});
	HW.on('focusout','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; nameFocus = 1;
	});
	HW.on('input','input-name-page',function(e,el){nameFocus = 1}); HW.on('click','input-name-page',function(e,el){nameFocus = 1});
	HW.on('change','input-name-page',function(e,el){	
		nameFocus = false;
		let name = document.body.querySelector('.ob-top .name-page input');		
		//let nameRegex = new RegExp(HWD.reg.name);	

		if(HWS.buffer.elem.config.name != name.value){
			//if(nameRegex.test(name.value)){
			if(name.value){
				M.name = name.value;
				let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
				HWF.post({url:HWD.api.gets.taskShceduleProgram3xSystem,data:dataP});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
	
	// Ручной режим:
	HW.on('click','btn_onof_mode',function(e,el){
		C.manualMode = 1
		let nameB = HWS.getElem('#manual-mode-info');
		if(el.classList.contains('active')){// on
			M.manualMode = 0;
			let dataP = {token:HWS.pageAccess,id_object:id,active:0}
			HWF.post({url:HWD.api.gets.taskShceduleProgram3xSystem,data:dataP});
			nameB.innerHTML = 'Вручную';
		}
		else{// of
			M.manualMode = 1;
			let dataP = {token:HWS.pageAccess,id_object:id,active:1}
			HWF.post({url:HWD.api.gets.taskShceduleProgram3xSystem,data:dataP});
			nameB.innerHTML = '';
		}
	});
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			M.viget = 1;
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/add_favorite_objects',data:dataP});
		}else { // Of
			M.viget = 0;
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/delete_favorite_objects',data:dataP});
		}
	});
	
	// Изменение календаря
	HWS.buffer.timetableUpFcn = function(inf){
		C.tempTimetable = 1;
		clearTimeout(HWS.buffer.idTimerTimetableUpFcn);
		HWS.buffer.idTimerTimetableUpFcn = setTimeout(function(){
			let allElem = inf.allElem;
			let specArrey = []
			for (let key in allElem){
				let val = (allElem[key].dataset.val == 1)?1:0;
				specArrey.push(val);
			}
			let dataP = {token:HWS.pageAccess,id_object:id,pau_shedl_time:specArrey}
			HWF.post({url:HWD.api.gets.taskShceduleProgram3xSystem,data:dataP});
		},2000);
	}
	
	// Загрузка Устройство управления
	HWS.buffer.loadUU = function(inf){}
	// Добавление Устройство управления
	HWS.buffer.changeUU = function(inf){
		C.relay = 1; // Якорь для реле
		D.relay = inf;
		M.idRelay = inf.id;
		HWS.buffer.equps[M.idRelay] = D.relay;
		change_state();
		let dataP = {token:HWS.pageAccess,id_object:id,id_automatic_device:inf.id}
		HWF.post({url:HWD.api.gets.taskShceduleProgram3xSystem,data:dataP,fcnE:HWF.getServMesErr});
	};
	// Удаление Устройство управления
	HWS.buffer.changeUUd = function(inf){
		C.relay = 1; // Якорь для реле
		D.relay = '';
		M.idRelay = '';
		HWS.buffer.equps[M.idRelay] = D.relay;
		change_state();
		let dataP = {token:HWS.pageAccess,id_object:id,id_automatic_device:'delete'}
		HWF.post({url:HWD.api.gets.taskShceduleProgram3xSystem,data:dataP});
	};
	
	// Кнопка Удолить программу
	HW.on('click','btn_delete_prog',function(e,el){
		let title = 'Что такое аварийный режим?';
		let infotext = 'Данная программа будет удалена из памяти системы. Вы уверены?';
		HWF.modal.confirm('Удалить программу?',infotext,'btn_delete_prog_yes','Удалить');
	});
	HW.on('click','btn_delete_prog_yes',function(e,el){
		let idprog = id || H.hash[1] || false;;
		HWF.modal.loader('Удаление');
		HWF.post({url:'task/delete_program_3x_system',data:{token:token,id_objects:[idprog]},fcn:function(info){
			HWF.modal.of();
			HWS.go('programs');
			HWF.push('Программа удалена','red');
		}});
	});
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(); // Проверит ваш ли это объект для вовода предупреждения
});