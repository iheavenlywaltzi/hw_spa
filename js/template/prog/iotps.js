"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#prog-content-top');
	let content = document.body.querySelector('#prog-content-mid');
	let token = HWS.pageAccess;
	let page = 'PageSets';
	let id = H.hash[1] || false;
	let ET = HWS.buffer.elemEt || false;
	let D = {}; // Дада массив с ОБЪЕКТАМИ от сервера
	let M = {}; // Отрисовочный объект с нужными данными
	let C = {}; // якоря
	let nameFocus = false;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="PageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageAccesses">Доступ</span></div>';
	B.menud.append(menu);
	

	/* FUNCTION */
	function creator_m(CD){
		let RM = {};
		
		RM.elem = CD.elem;
		RM.id = CD.elem.id;
		RM.lid = CD.elem.local_id;
		RM.lid2 = CD.elem.local_id2;
		RM.idsys = CD.elem.id_system;
		RM.name = CD.elem.config.name;
		RM.elemType = CD.elem.cloud_type || CD.elem.info.type.type;
		RM.elemTitle = ET.title;
		RM.systemName = CD.elem.system_name;
		RM.systemIdent = CD.elem.system_ident;
		RM.onof = CD.elem.config.flags.enabled;
		RM.scene = CD.elem.config.scripts || {};
		RM.active = CD.elem.state.active || '';
		RM.bufRedact = '';
		
		return RM;
	}
	
	
	function print_header(){
		let S = {};
		S.manualModeText = '';
		S.blambaColor = '#9613ee'; //ET.state[M.state].colorBlamba;
		S.stateText = (M.active && M.scene[M.active])?M.scene[M.active].title:'';
		S.stateTextColor = ''; //ET.state[M.state].colorText;
		
		let print = '\
		<div class="one-block ob-top page-equp-top">\
			<div class="name-page">\
				<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
				<input id="prog-name" type="text" class="input-name-page prog-name" value="'+M.name+'">\
				<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
			</div>\
			<div class="info-li block_w_100 flex_center_left">\
				<div id="blamba" style="background:'+S.blambaColor+'"></div>\
				<div class="page-top-dop-info tf_fr500 block-title-name1colum">\
					<div id="name-status" class="data"><inf style="color:'+S.stateTextColor+'">'+S.stateText+'</inf></div>\
					<div id="manual-mode-info" class="dop-info-name">'+S.manualModeText+'</div>\
				</div>\
				<div class="block-title-colum"></div>\
				<div class="page-top-dop-info-title block-title-colum310">\
					<p>Принадлежит</p>\
					<p><span id="name-system" class="tf_fr500">'+M.systemName+'</span></p>\
					<p><span class="tf_fr500">'+M.systemIdent+'</span></p>\
				</div>\
				<div id="iconr" class="flex_center_left">\
					<span class="block_w_150p block_pr_10">'+M.elemTitle+'</span>\
					<span class="icon p35 icon icon_equip_'+M.elemType+'"></span>\
				</div>\
			</div>\
		</div>\
		';
		contentTop.innerHTML = print;
		HWS.buffer.share_stick(M.elem);
	}
	
	
	function load_all_info(rInfo){
		D.elem = HWS.buffer.elem;
		//console.log(D.elem);
		//D.sensors = D.elem.config.sensors || '';
		//D.relay = D.elem.config.automatic_device || '';
		
		let dopId = (5 << 24) | (D.elem.local_id & 0x00ffffff);
		let dataP = {token:token,id_system:D.elem.id_system,local_id:dopId}
		HWF.post({url:HWD.api.gets.objectLocalId,data:dataP,fcn:function(dInfo){ console.log(dInfo);
			if(!dInfo.success){
				D.elem.state = dInfo.state;
				D.elem.config.scripts = dInfo.config.scripts;
				D.elem.local_id2 = dInfo.local_id;
			}else {console.log('ERROR LOAD PAGE');  console.log(dInfo);}
			M = creator_m(D);// создаём адекватное структурирование данных
			print_header();// печатем шапку страници
			HWS.buffer[page]();// печатаем вкладку на которой находимся
		}});


		//HWF.timerUpdate(reolad_info_page); //Авто обновление
	}
	
	function get_state(CDD){

	}
		
	function change_state(){

	}
	
	function reolad_info_page(){if(page && page == 'PageSets'){ HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(rEl){
		let DNew =  {}; DNew.elem = rEl;
			DNew.sensors = DNew.elem.config.sensors || '';
			DNew.relay = DNew.elem.config.automatic_device || '';

			D = DNew;
			let MNew = creator_m(DNew);// создаём адекватное структурирование данных
			let S = {};

			change_state(); // изменение статуса
			C = {} // очищение якорей
		}}); }
	}
	
	function create_btn (){
		let S = {};
		S.scene = '';
		if(M.scene) {
			HWF.recount(M.scene,function(el,key){
				let act = '';
				if(M.active == key){act = 'active';}
				let btn = '<span class="btn">\
					<span class="icon icon-settings hover redact_scene_elem" title="Редактировать"></span>\
					<span class="icon icon-send-delete hover delete_scene_elem" title="Удалить"></span>\
				</span>';
				S.scene += '<div class="one-elem '+act+'" data-id="'+key+'"><span class="name change_scene_elem">'+el.title+'</span>'+btn+'</div>';
			});
		}
		S.scene+= '<div class="one-elem add_new_elem">\
			<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>\
			<span class="add-name vertical-middle select-none">Добавить сценарий</span>\
		</div>';
		
		return S.scene;
	}
	
	function create_list_scene(){
		let html = '';
		let listBlick = HWS.getElem('#scene-list-redact');
			listBlick.innerHTML = HWhtml.loader();
			
		function print_list(ld){ 
		let S = {};
			S.print = '';
			S.ul = '<ul class="list-equipments">';
			S.liTop = '<li class="one-head sticky">\
				<div class="spec_prog_iotps">\
					<span class="block_mr_30">Тип</span>\
					<span class="name">Имя</span>\
					<span class="data">Данные</span>\
					<span class="send"></span>\
				</div>\
				<div class="equ_b equp_select_all user-none">Всё</div>\
			</li>';
			S.li = '';
			S.ulE = '</ul> <div class="block_pt_10"><p class="btn_text grey btn_add_new_equp_scene">Добавить</p></div>';
			
			if (ld.length > 0){
				HWF.recount(ld,function(el,key){
					let name = el.name;
					let type = el.type;
					let dataP = '';
					if(type != 32710){dataP = HWhtml.select({"enable":'Включено',"disable":'Отключено'},'change_rule_elem_relay',el.action);}
					else { dataP = '<input type="text" class="change_rule_elem" value="'+el.action+'">'; }
					S.li+= '<li class="one-equipment" data-rulekey="'+el.rule_key+'">'+
						'<div class="spec_prog_iotps">'+
							'<span class="icon icon_equip_'+type+'"></span>'+
							'<span class="name">'+name+'</span>'+
							'<span class="data">'+dataP+'</span>'+
							'<span class="send btn_text grey send_command_elem">Отправить</span>'+
						'</div>'+
						'<div class="equ_b"><span class="icon icon-send-delete hover delete_one_elem" title="Удалить"></span></div>'+
					'</li>';
				}); 
				S.print = S.ul+S.liTop+S.li+S.ulE;
			} else {
				S.print = S.ul+S.liTop+S.ulE;
			}
		
			listBlick.innerHTML = S.print;
		};
		
		if(M.bufRedact){
			let idScen = M.bufRedact.dataset.id;
			let getList = [],dataList = [], spList = []
			let scenList = M.scene[idScen].rules;
			HWF.recount(scenList,function(el,key){ getList.push(el.device); });
			if(getList.length > 0){
				let data = {
					"token": token,
					"list_start":0,
					"list_amount":1000,
					"id_system":M.idsys,
					"local_id":getList
				}
				HWF.post({url:HWD.api.gets.listMore,data:data,fcn:function(info){
					let list = info.list;
					if(list.length > 0) {
						HWF.recount(scenList,function(elsc,key){
							let spObj = {};
							HWF.recount(list,function(elli,keyr){ if(elli.local_id == elsc.device) {
								spObj.rule_key = key;
								spObj.action = elsc.action;
								spObj.name = elli.config.name || elli.info.name;
								spObj.type = elli.cloud_type;
								spList.push(spObj);
							}});
						});
					}
					print_list(spList);
				}});
			} else {print_list(spList);}
		}
	}
	
	// Загрузка Настроек
	HWS.buffer.PageSets = function(){ page = 'PageSets';
		let S = {};
		S.print = '';
		S.scene = create_btn();
		
		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Работает '+HWhtml.helper(HWD.helpText.prog.manualMode)+'</p>\
					<p>'+HWhtml.btn.onof(M.onof,'btn_onof')+'</p>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.prog.btnHelpVviget)+'</p>\
					<p>'+HWhtml.btn.onof('active','')+'</p>\
				</div>\
			</div>\
		</div>';
		
		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Сценарии '+HWhtml.helper('Описания НЕТ')+'</h4></div>\
			<div class="ltft"></div>\
			<div class="right"><div id="prog-iotps">\
				'+S.scene+'\
			</div></div>\
			<div class="title"><sme data-buf="sensors" data-list="list" data-fcn="listSensor" data-fcnd="listSensorD"></sme></div>\
		</div>';
		
		S.print += '<div class="one-block-col">\
			<div class="title"><h4>Управление</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Имя сценария</p>\
					<p><input id="name_scene" type="text" class="name_redact_scene" value=""><br><span id="error-redact-name" class="tc_red"></span></p>\
				</div>\
			</div>\
			<div id="scene-list-redact" class="title"></div>\
		</div>';
		
		// отрисовка HTML 
		content.innerHTML = S.print;
		// запуск кнопок ПРограмм 
		//buttonsController.start();
	}
	// Загрузка Журнала
	HWS.buffer.PageJurnal = function(){ page = 'PageJurnal';
		let print = '';
		content.innerHTML = ''; 
	};
	// Загрузка Доступов
	HWS.buffer.PageAccesses = function(){ page = 'PageAccesses';
		HWS.buffer.equpAccessesFCN();
	};
	
	load_all_info();
	
	//Упровляющие функции 
	//Сохранение имени
	HW.on('focusin','prog-name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"};
	});
	HW.on('focusout','prog-name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"};
	});

	HW.on('change','prog-name',function(e,el){
		//nameFocus = false;
		let name = el.value;

		if(M.name != name){
			let comand = {}
				comand[M.idsys] = [
						{
							"id": 123,
							"command": {
								"cmd_id": 2147483648,
								"targ_id": 65536,
								"tmt_sec": 0, // бросим на волю сревера
								"json": {
									"object": {
										"id": M.lid,
										"name": M.name // core.name
									},
									"path": "name",
									"remove": false, // удалять свойство не нужно
									"value": JSON.stringify(name) // name - строка, и мы здесь указываем строку
								}
							}
						}
					]
			let data ={
				"token": token,
				"commands_to_system": comand,
			}
			HWF.post({url:'ecto4/send_command',data:data});
			M.name = name;
		}

		el.blur();
	});	
	
	// Кнопка вкыл выкл
	HW.on('click','btn_onof',function(e,el){
		let onof = 0;
		if(el.classList.contains('active')){M.onof = 1}

		let comand = {}
			comand[M.idsys] = [
				{
					"id": 123,
					"command": {
						"cmd_id": 2147483648,
						"targ_id": 65536,
						"tmt_sec": 0, // бросим на волю сревера
						"json": {
							"object": {
								"id": M.lid,
								"name": M.name // core.name
							},
							"path": "flags.enabled",
							"remove": false, // удалять свойство не нужно
							"value":  JSON.stringify(M.onof) // name - строка, и мы здесь указываем строку
						}
					}
				}
			]
		let data ={
			"token": token,
			"commands_to_system": comand,
		}
		HWF.post({url:'ecto4/send_command',data:data});
	});
	
	
	// выбор активной кнопки
	HW.on('click','change_scene_elem',function(e,el){
		let mom = HWS.upElem(el,'.one-elem');
		let active = HWS.getElem('#prog-iotps .one-elem.active');
		let id = mom.dataset.id;
			//SD.
			
		if(active && mom != active){active.classList.remove('active');}
		if(!mom.classList.contains('active')) {
			HWS.getElem('#name-status inf').innerHTML = el.innerHTML;
			mom.classList.add('active');
			let comand = {}
			comand[M.idsys] = [
				{
					"id": 123,
					"command": {
						"cmd_id": 2147484672,
						"targ_id": M.lid2,
						"tmt_sec": 0, // бросим на волю сревера
						"json": {
							"object": {
								"id": M.lid2,
								"name": M.name // core.name
							},
							"command": "activate",
							"argument":  JSON.stringify({"script":id}) // name - строка, и мы здесь указываем строку
						}
					}
				}
			]
			let data ={
				"token": token,
				"commands_to_system": comand,
			}
			HWF.post({url:'ecto4/send_command',data:data});
		}
	});
	
	// Редактирование кнопки
	HW.on('click','redact_scene_elem',function(e,el){
		let mom = HWS.upElem(el,'.one-elem');
		let id = mom.dataset.id;
		let name = M.scene[id].title;
		let active = HWS.getElem('#prog-iotps .redact_scene_elem.active');
		let changer = HWS.getElem('#name_scene');
		
		
		if(active && el != active){active.classList.remove('active');}
		if(!el.classList.contains('active')) {
			el.classList.add('active');
			changer.value = name;
			M.bufRedact = mom;
			create_list_scene();
		} else {
			el.classList.remove('active');
			changer.value = '';
			M.bufRedact = '';
			HWS.getElem('#scene-list-redact').innerHTML = '';
		}
	});
	HW.on('change','name_redact_scene',function(e,el){
		if (M.bufRedact) {
			let name = M.scene[M.bufRedact.dataset.id].title;
			let error = HWS.getElem('#error-redact-name');
			//let allElem = HWS.getElem('#prog-iotps').querySelectorAll('.one-elem');
			if (!el.value){error.innerHTML = 'Введите имя';} else { if (el.value != name){ 
				error.innerHTML = '';
				M.bufRedact.querySelector('.name').innerHTML = el.value;
				M.scene[M.bufRedact.dataset.id].title = el.value;
				if(M.bufRedact.classList.contains('active')){HWS.getElem('#name-status inf').innerHTML = el.value;}
				
					let path = 'scripts.\"'+M.bufRedact.dataset.id+'\".title';
					let comand = {}
					comand[M.idsys] = [
						{
							"id": 123,
							"command": {
								"cmd_id": 2147483648,
								"targ_id": 65536,
								"tmt_sec": 0, // бросим на волю сревера
								"json": {
									"object": {
										"id": M.lid2,
										"name": M.name // core.name
									},
									"remove": false,
									"path": path,
									"value":  JSON.stringify(el.value) // name - строка, и мы здесь указываем строку
								}
							}
						}
					]
					let data ={
						"token": token,
						"commands_to_system": comand,
					}
					HWF.post({url:'ecto4/send_command',data:data});
				
			}}
		}
		el.blur();
	});
	
	
	// удоление кнопки
	HW.on('click','delete_scene_elem',function(e,el){
		let mom = HWS.upElem(el,'.one-elem');
		let id = mom.dataset.id;
		
		let path = 'scripts.\"'+id+'\"';
		let comand = {}
		comand[M.idsys] = [
			{
				"id": 123,
				"command": {
					"cmd_id": 2147483648,
					"targ_id": 65536,
					"tmt_sec": 0, // бросим на волю сревера
					"json": {
						"object": {
							"id": M.lid2,
							"name": M.name // core.name
						},
						"remove": true,
						"path": path,
						"value": null // name - строка, и мы здесь указываем строку
					}
				}
			}
		]
		let data ={
			"token": token,
			"commands_to_system": comand,
		}
		HWF.post({url:'ecto4/send_command',data:data});
		
		mom.remove();
		delete M.scene[id];
	});

	// Добавление новой кнопки
	HW.on('click','add_new_elem',function(e,el){
		let mom = HWS.upElem(el,'.one-elem');
		let html = '<p>\
			Новое имя: \
			<input id="new_name_scene" type="text" class="" value="">\
			<br>\
			<span id="error-new-name" class="tc_red"></span>\
		</p>';
		
		HWF.modal.confirmCust({
			title:'Новый сценарии',
			yes:'Создать',
			classYes:'create_new_scene',
			clouseYes:'no',
			print:html,
		})
	});
	HW.on('click','create_new_scene',function(e,el){
		let error = HWS.getElem('#error-new-name');
		let name = HWS.getElem('#new_name_scene');
		let scene = HWS.getElem('#prog-iotps');
		
		if(name.value){
			let newID = '',newItr = 1,timer = Date.now();
			timer = String(timer).substr(-8,6); timer = parseInt(timer);
			if(M.scene){
				HWF.recount(M.scene,function(el,key){
					let str = String(key).substr(0,4); str = parseInt(str);
					if(newItr < str){newItr = str+1;}
				});
				newID = parseInt(String(newItr)+String(timer));
			}
			M.scene[newID] = {title:name.value,rules:{}};
			scene.innerHTML = create_btn();
			HWF.modal.of();
			
			let path = 'scripts.\"'+newID+'\"';
			let comand = {}
			comand[M.idsys] = [
				{
					"id": 123,
					"command": {
						"cmd_id": 2147483648,
						"targ_id": 65536,
						"tmt_sec": 0, // бросим на волю сревера
						"json": {
							"object": {
								"id": M.lid2,
								"name": M.name // core.name
							},
							"remove": false,
							"path": path,
							"value":  JSON.stringify({"title":name.value,"rules":{}}) // name - строка, и мы здесь указываем строку
						}
					}
				}
			]
			let data ={
				"token": token,
				"commands_to_system": comand,
			}
			HWF.post({url:'ecto4/send_command',data:data});
			
		} else { error.innerHTML = 'Введите имя';}
	});
	
	// изменение Данных в списке оборудования (тектовое)
	HW.on('change','change_rule_elem',function(e,el){
		let li = HWS.upElem(el,'.one-equipment');
		let rulekey = li.dataset.rulekey;
		let idScript = M.bufRedact.dataset.id;
		let rulData = M.scene[idScript].rules[rulekey].action
		
		if(el.value && el.value != rulData){
			M.scene[idScript].rules[rulekey].action = el.value;
			
			let path = 'scripts.\"'+idScript+'\".rules.\"'+rulekey+'\".action';
			let comand = {}
			comand[M.idsys] = [
				{
					"id": 123,
					"command": {
						"cmd_id": 2147483648,
						"targ_id": 65536,
						"tmt_sec": 0, // бросим на волю сревера
						"json": {
							"object": {
								"id": M.lid2,
								"name": M.name // core.name
							},
							"remove": false,
							"path": path,
							"value": JSON.stringify(el.value) // name - строка, и мы здесь указываем строку
						}
					}
				}
			]
			let data ={ "token": token, "commands_to_system": comand}
			HWF.post({url:'ecto4/send_command',data:data});
		}

		el.blur();
	});	
	// изменение Данных в списке оборудования (реле)
	HW.on('change','change_rule_elem_relay',function(e,el){
		let li = HWS.upElem(el,'.one-equipment');
		let rulekey = li.dataset.rulekey;
		let idScript = M.bufRedact.dataset.id;
		let rulData = M.scene[idScript].rules[rulekey].action
		
		let path = 'scripts.\"'+idScript+'\".rules.\"'+rulekey+'\".action';
		let comand = {}
		comand[M.idsys] = [
			{
				"id": 123,
				"command": {
					"cmd_id": 2147483648,
					"targ_id": 65536,
					"tmt_sec": 0, // бросим на волю сревера
					"json": {
						"object": {
							"id": M.lid2,
							"name": M.name // core.name
						},
						"remove": false,
						"path": path,
						"value": JSON.stringify(el.value) // name - строка, и мы здесь указываем строку
					}
				}
			}
		]
		let data ={ "token": token, "commands_to_system": comand}
		HWF.post({url:'ecto4/send_command',data:data});
		M.scene[idScript].rules[rulekey].action = el.value;
		console.log(M.scene[idScript].rules[rulekey]);
	});
	
	// Кнопка отправить
	HW.on('click','send_command_elem',function(e,el){
		let li = HWS.upElem(el,'.one-equipment');
		let rulekey = li.dataset.rulekey;
		let name = li.querySelector('.spec_prog_iotps .name').innerHTML;
		let dataIp = li.querySelector('.spec_prog_iotps .data input');
		let idScript = M.bufRedact.dataset.id;
		
		let comand = {}
		comand[M.idsys] = [
			{
				"id": 123,
				"command": {
					"cmd_id": 2147484672,
					"targ_id": M.lid2,
					"tmt_sec": 0, // бросим на волю сревера
					"json": {
						"object": {
							"id": M.lid2,
							"name": M.name // core.name
						},
						"command": "activate",
						"argument":  JSON.stringify({"script":idScript,"rule":rulekey})
					}
				}
			}
		]
		let data ={
			"token": token,
			"commands_to_system": comand,
		}
		console.log(data);
		HWF.post({url:'ecto4/send_command',data:data});
		HWF.push('Отправлены данные устройству <b>'+name+'</b>');
	});
	
	// Удоление элемента управления
	HW.on('click','delete_one_elem',function(e,el){
		let li = HWS.upElem(el,'.one-equipment');
		let rulekey = li.dataset.rulekey;
		let idScript = M.bufRedact.dataset.id;
		
		li.remove();
		
		let path = 'scripts.\"'+idScript+'\".rules.\"'+rulekey+'\"';
		let comand = {}
		comand[M.idsys] = [
			{
				"id": 123,
				"command": {
					"cmd_id": 2147483648,
					"targ_id": 65536,
					"tmt_sec": 0, // бросим на волю сревера
					"json": {
						"object": {
							"id": M.lid2,
							"name": M.name // core.name
						},
						"remove": true,
						"path": path,
						"value": null // name - строка, и мы здесь указываем строку
					}
				}
			}
		]
		let data ={ "token": token, "commands_to_system": comand}
		HWF.post({url:'ecto4/send_command',data:data});
	});
	
	
	// Добавление элемента управления
	HW.on('click','btn_add_new_equp_scene',function(e,el){
		let iterKey = 0;
		let list = '',modbus = '';
		let print = ''+
		'<div id="print-modal-window-list" class="table-content block_w_100 block_h_85 all-equipments">'+HWhtml.loader()+'</div>'+
		'<div class="block_p_8 block_h_100"></div>'+
		'';
		HWF.modal.print('Добавить',print);
		
		function load_info(){
			iterKey++;
			if(iterKey == 2){
				let normList = list.unshift(modbus);
				let S = {}
					S.ulOpen = '<ul class="list-equipments">';
					S.ulClouse = '</ul>';
					S.head = '\
						<li class="one-head sticky">\
							<div class="equ_n"><span class="block_mr_30">Тип</span><span>Имя</span></div>\
							<div class="equ_d">Данные</div>\
							<div class="equ_p">Подключение</div>\
							<div class="equ_p">Порт : адрес</div>\
							<div class="equ_i">id : канал </div>\
						</li>';
					S.list = ''
					S.print = ''
				
				HWF.recount(list,function(el,key){if(el){
					let normInfo = HWequip.normInfo(el);
					let type = el.cloud_type;
					let name = el.config.name || el.info.name;
					let value = '',stateColor='',port = '',ident = '';
					let connect = '';
					
					if(el.info && el.info.iface && el.info.iface.group) {connect = HWD.iface.group[el.info.iface.group] || ''}
					if(normInfo.tmp == 'control_device3x'){
						value = normInfo.et.state[normInfo.stateN].name
						if (normInfo.state == 'no_connect') {stateColor='tc_grey8';}
						if (normInfo.state == 'on') {stateColor='tc_blue';}
						if (normInfo.state == 'of') {stateColor='';}
					}
					if(normInfo.tmp == 'control_device4x'){
						value = el.state.state.name;
						connect = '';
					}
					
					if(normInfo.port == 10 || normInfo.port == 12){
						let chanel = '';
						if(normInfo.channel_index){chanel = ':'+normInfo.channel_index}
						ident = normInfo.ident+chanel;
						port = normInfo.portName+' : '+(normInfo.portSymbol+normInfo.addr);
					}
					
					S.list += '<li class="one-equipment" data-localid="'+el.local_id+'" data-type="'+type+'">\
						<div class="equ_n">\
							<span class="icon icon_equip_'+type+'"></span>\
							<span class="text_name vertical-middle">'+name+'</span>\
						</div>\
						<div class="equ_d '+stateColor+'">\
							<span class="tw_700 data">'+value+' </span>\
						</div>\
						<div class="equ_p">'+connect+'</div>\
						<div class="equ_p">'+port+'</div>\
						<div class="equ_i">'+ident+'</div>\
						<div class="click btn_one_equipment"></div>\
					</li>';
				}});
				
				S.print = '<div class="overflow-auto block_h_100">'+S.ulOpen+S.head+S.list+S.ulClouse+'</div>';
				HWS.getElem('#print-modal-window-list').innerHTML = S.print;
			}
		}
		
		let data = { "token": token,"list_start":0,"list_amount":1000,"id_system":M.idsys,"branch":3}
		HWF.post({url:HWD.api.gets.list,data:data,fcn:function(info){
			console.log(info)
			list = info.list; load_info();
		}});
		
		HWF.post({url:HWD.api.gets.listMore,data:{"token": token,"list_start":0,"list_amount":1000,"id_system":M.idsys,local_id:"589824"},fcn:function(info){
			console.log(info)
			modbus = info.list[0]; load_info();
		}});
		
		
	}); // Нажатие на сам объект
	HW.on('click','btn_one_equipment',function(e,el){
		let idScript = M.bufRedact.dataset.id;
		let listScene = HWS.getElem('#scene-list-redact .list-equipments');
		let li = HWS.upElem(el,'.one-equipment');
		let type = li.dataset.type;
		let localid = parseInt(li.dataset.localid);
		let name = li.querySelector('.equ_n .text_name').innerHTML;
		let dataP = '',action='';
		let newID = '',newItr = 1,timer = Date.now();
		timer = String(timer).substr(-8,6); timer = parseInt(timer);
		
		if(M.scene[idScript].rules){
			HWF.recount(M.scene[idScript].rules,function(el,key){
				let str = String(key).substr(0,4); str = parseInt(str);
				if(newItr < str){newItr = str+1;}
			});
			newID = parseInt(String(newItr)+String(timer));
		}

		let newLi = document.createElement('li');
			newLi.classList.add('one-equipment');
			newLi.dataset.rulekey = newID;
		
		if(type != 32710){ dataP = HWhtml.select({enable:'Включено',disable:'Отключено'},'change_rule_elem_relay'); action="enable";}
		else { dataP = '<input type="text" class="change_rule_elem" value="">'; action='';}
			
		newLi.innerHTML = ''+
			'<div class="spec_prog_iotps">'+
				'<span class="icon icon_equip_'+type+'"></span>'+
				'<span class="name">'+name+'</span>'+
				'<span class="data">'+dataP+'</span>'+
				'<span class="send btn_text grey send_command_elem">Отправить</span>'+
			'</div>'+
			'<div class="equ_b"><span class="icon icon-send-delete hover delete_one_elem" title="Удалить"></span></div>'+
		'';
		
		let path = 'scripts.\"'+idScript+'\".rules.\"'+newID+'\"';
		let comand = {}
		comand[M.idsys] = [
			{
				"id": 123,
				"command": {
					"cmd_id": 2147483648,
					"targ_id": 65536,
					"tmt_sec": 0, // бросим на волю сревера
					"json": {
						"object": {
							"id": M.lid2,
							"name": M.name // core.name
						},
						"remove": false,
						"path": path,
						"value": JSON.stringify({device:localid, action:action})
					}
				}
			}
		]
		let data ={ "token": token, "commands_to_system": comand}
		HWF.post({url:'ecto4/send_command',data:data});
		
		listScene.appendChild(newLi);
		M.scene[idScript].rules[newID] = {action:action,device:localid};
		HWF.modal.of();
	});
	
	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(); // Проверит ваш ли это объект для вовода предупреждения
});