"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = HWS.getElem('#prog-content-top');
	let content = HWS.getElem('#prog-content-mid');
	let token = HWS.pageAccess;
	let page = 'PageSets';
	let id = H.hash[1] || false;
	let ET = HWS.buffer.elemEt || false;
	let D = {}; // Дада массив с ОБЪЕКТАМИ от сервера
	let M = {}; // Отрисовочный объект с нужными данными
	let C = {}; // якоря
	let nameFocus = false;
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="PageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageGraph">График</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageJurnal">Журнал</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="PageAccesses">Доступ</span></div>';
	B.menud.append(menu);
	
	/* FUNCTION */
	function load_all_info(rInfo){
		D.elem = HWS.buffer.elem;
		D.temp = D.elem.config.sensor_temperature || '';
		D.relay = D.elem.config.boil_device || '';
		D.pump = D.elem.config.pump_device || '';
		D.addit = D.elem.config.sensor_environment || '';
		
		M = creator_m(D);// создаём адекватное структурирование данных
		print_header();// печатем шапку страници
		HWS.buffer[page]();// печатаем вкладку на которой находимся
		HWF.timerUpdate(reolad_info_page); //Авто обновление
	}
	
	function creator_m(CD){
		let RM = {}
		let state = '';
		
		RM.elem = CD.elem;
		RM.name = CD.elem.config.name;
		RM.elemType = CD.elem.info.type.type;
		RM.elemTitle = ET.title;
		RM.systemName = CD.elem.system_name;
		RM.systemIdent = CD.elem.system_ident;
		RM.systemTime = CD.elem.system_connection_time;
		RM.invers = CD.elem.config.flags.inversion_of_control;
		RM.manualMode = CD.elem.config.flags.active; // ручной режим
		RM.viget = CD.elem.lk.favorite || ''; // в виджеты
		RM.tempSuport = CD.elem.config.pau_therm_value; // температура поддержания 
		RM.tempTimetable = CD.elem.config.flags.control_from_schedule; // вкл откл таблици расписание
		RM.tempTimetableDat = buttonsController.createrTimetableInArray(CD.elem.config.pau_therm_time,24); // правильная дата для таблице  расписание
		RM.tempEco = CD.elem.config.pau_therm_pre_eco; // температура эконом
		RM.tempStd = CD.elem.config.pau_therm_pre_std; // температура стандарт
		RM.tempKom = CD.elem.config.pau_therm_pre_comf; // температура комфорт
		RM.tempCus = CD.elem.config.pau_therm_pre_usr; // температура своя
		RM.glist = CD.elem.config.pau_therm_hyst; //Гистерезис 1.0 ... 5.0
		RM.pumpVal = CD.elem.config.pau_therm_del_pump || 0; // 0 ( неотключать вообще ), всё остальное минуты до отключение
		
		if(RM.tempSuport){RM.tempSuport = RM.tempSuport.toFixed(1)}
		
		RM.idTemp = CD.temp.id || '';
		RM.idRelay = CD.relay.id || '';
		RM.idPump = CD.pump.id || '';
		RM.idTempAddit = (CD.addit)?CD.addit.id:'';
		
		HWS.buffer.equps = {};
		HWS.buffer.equps[RM.idTemp] = CD.temp;
		HWS.buffer.equps[RM.idRelay] = CD.relay;
		HWS.buffer.equps[RM.idPump] = CD.pump;
		HWS.buffer.equps[RM.idTempAddit] = CD.addit;

		RM.state = CD.elem.lk;
		RM.stateVal = RM.state.state;
		RM.blambaColor = HWF.getStateColor(RM.stateVal);//HWD.state.prog[RM.stateVal].colorBlamba;;
		RM.stateText = RM.state.state_name;
		RM.stateTextColor = HWD.color.black; // HWD.state.prog[RM.stateVal].colorText;
		
		let equpTmp = get_temp(CD);
		RM.tempActive = equpTmp.val; // температура, 
		
		//Режимы ( с адаптером и без )
		RM.adapter = false;
		if(CD.relay && (CD.relay.cloud_type == 1 || CD.relay.cloud_type == 2)){RM.adapter = true;}
		
		return RM;
	}
	
	function get_temp(CDD){
		let temp = {val:'нет',unit:''};
		if(CDD.temp){temp.val = CDD.temp.lk.state_name;}
		return temp;
	}
	
	function change_state(){ HWF.timerUpdate(reolad_info_page,'',100); }  //Авто обновление через 100 милисекунд
	function re_print_state(MNew){
		let S ={}
		let tenp = get_temp(D);
		
		let blamba = HWS.getElem('#blamba');
		let iconState = HWS.getElem('#icon-state-cotel-flsme');
		let infoInfBlock = HWS.getElem('#heating-state-info');
		let dopTextHelper = HWS.getElem('#dop-text-info-elper');
		let tempVal = HWS.getElem('#heating-state-val');
		
		if(MNew.idRelay){
			S.flameColor = MNew.blambaColor;
			S.flameText = MNew.stateText;
			if(MNew.adapter){ S.flameColor = (HWS.buffer.equps[MNew.idRelay].state.flame)?HWD.color.blue:HWD.color.grey; }
		}
		else {S.flameColor = ET.state[0].colorBlamba; S.flameText = ET.state[0].name;}
		if(MNew.adapter){content.classList.add('adapter')}
		else {content.classList.remove('adapter')}
		
		blamba.style.background = MNew.blambaColor;
		iconState.style.background = S.flameColor;
		infoInfBlock.innerHTML = MNew.stateText;
		infoInfBlock.style.color = MNew.stateTextColor;
		tempVal.innerHTML = tenp.val;
	}
	
	function print_header(){
		let S = {};
		S.manualModeText = (M.manualMode)?'':'Вручную';
		S.blambaColor = M.blambaColor;
		S.stateText = M.stateText;
		S.stateTextColor = M.stateTextColor;
		S.dopText = '';
		//if(M.state == 3) {S.dopText = 'Проблема?';}
		if(M.stateVal == 112){S.dopText = '<span class="btn_text grey btn_whey_error">Почему?</span>'}
		
		let print = '\
			<div class="one-block ob-top page-equp-top">\
			<div class="name-page">\
				<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
				<input id="prog-name" type="text" class="input-name-page" value="'+M.name+'">\
				<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
			</div>\
			<div class="info-li block_w_100 flex_center_left">\
				<div id="blamba" style="background:'+S.blambaColor+'"></div>\
				<div class="page-top-dop-info block-title-name1colum">\
					<div class="data tt_capitalize"><inf id="heating-state-info" style="color:'+S.stateTextColor+'">'+S.stateText+'</inf></div>\
					<div id="manual-mode-info" class="dop-info-name">'+S.manualModeText+'</div>\
				</div>\
				<div class="page-top-dop-info-spec block-title-colum"></div>\
				<div class="page-top-dop-info-title block-title-colum310">\
					<p>Принадлежит</p>\
					<p><span id="name-system" class="tf_fr500">'+M.systemName+'</span></p>\
					<p><span class="tf_fr500">'+M.systemIdent+'</span></p>\
				</div>\
				<div id="iconr" class="flex_center_left">\
					<span class="block_w_150p block_pr_10">'+M.elemTitle+'</span>\
					<span class="icon p35 icon icon_equip_'+M.elemType+'"></span>\
				</div>\
			</div>\
			<div id="top-dop-info-page-bottom" class="block_w_100 flex_center_left">\
				<p class="flex_between tc_grey6 ts_14p block_pl_50">\
					<span id="dop-text-info-elper" class="block_mr_6 btn_dop_text_helper btn_text grey">'+S.dopText+'</span>\
				</p>\
			</div>\
		</div>\
		';
		contentTop.innerHTML = print;
		HWS.buffer.share_stick(M.elem);
	}
	
	// Функци для графиков
	function clear_graph(){
		if(!HWS.buffer.graph){HWS.buffer.graph = {}}; 
		if(HWS.buffer.graph[id]){
			HWS.buffer.graph[id].dispose();
			HWS.buffer.graph[id] = null;
			HWS.buffer.controlAutoReoladStoper = true;
		}
	}
	
	function create_graph(time){ time = time || 1;
		clear_graph();
		let mainBlock = HWS.getElem('#equp-statistic-graf');
		let blockGraph = mainBlock.querySelector('.graph-window-heating');
			blockGraph.innerHTML = HWhtml.loader();
		let gTime = HWS.buffer.system_time || Date.now();
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		let arrGet = [], allAnalog = {}, allDisc = {};
			
		arrGet.push({object_id:id})
		if(D.relay){ arrGet.push({object_id:D.relay.id})}
		if(D.pump) { arrGet.push({object_id:D.pump.id}) }
		if(D.temp) { arrGet.push({object_id:D.temp.id}) }
		if(D.addit){ arrGet.push({object_id:D.addit.id}) }
		
		let dataGet = {token:token,timestamp_start:sTime,timestamp_end:eTime,charts:arrGet}
		HWF.post({url:HWD.api.gets.historyСharts,data:dataGet,fcn:function(infGraph){
			HWF.recount(infGraph,function(oneEl,key){
				if(!oneEl.exceptions){
					let type = HWD.equp[oneEl.cloud_type] || ''; if(type){type = type.tmp;}
					let addData = {unit:oneEl.charts[0].unit || '', values:oneEl.charts[0].values || []}
					
					if(key == id){addData.colorLine = '#F1AD32'; addData.nameLabel = 'Уставка'; allAnalog[key] = addData;}
					if(type == 'adapter_boiler3x'){
						HWF.recount(oneEl.charts,function(oneChart,key){
							addData = {unit:oneEl.charts[key].unit || '', values:oneEl.charts[key].values || []}
							if(oneChart.index == 1){addData.colorLine = '#569AD4'; addData.nameLabel = 'Горелка'; allDisc[key] = addData;}
							if(oneChart.index == 2){addData.colorLine = '#FF0000'; addData.nameLabel = 'Теплоноситель'; allAnalog[key] = addData;}
							if(oneChart.index == 3){addData.colorLine = '#915924'; addData.nameLabel = 'ГВС'; allAnalog[key] = addData;}
							if(oneChart.index == 4){addData.colorLine = '#757575'; addData.nameLabel = 'Расход ГВС'; allAnalog[key] = addData;}
							if(oneChart.index == 5){addData.colorLine = '#000000'; addData.nameLabel = 'Давление'; allAnalog[key] = addData;}
							if(oneChart.index == 9){addData.colorLine = '#FF0000'; addData.nameLabel = 'Ошибка'; allDisc[key] = addData;}
						});
					}
					if(type == 'analog3x'){
						if(D.addit && key == D.addit.id) {addData.colorLine = '#5B86E0'; addData.nameLabel = 'На улице'; allAnalog[key] = addData;}
						else {addData.colorLine = '#0BC40B'; addData.nameLabel = 'Температура помещения'; allAnalog[key] = addData;}
					}
					if(type == 'control_device3x'){
						addData.colorLine = '#569AD4'; 
						addData.nameLabel = 'Реле отопителя'; 
						if(key == D.relay.id){addData.colorLine = '#569AD4'; addData.nameLabel = 'Отопитель'; }
						if(key == D.pump.id){addData.colorLine = '#B5A5C5'; addData.nameLabel = 'Насос'; }
						allDisc[key] = addData;
					}
				}
			});
			HWG.massGraph({
				id:id,
				target:blockGraph,
				analog:allAnalog,
				discret:allDisc,
			});
		}});
	}
	//авто обновление
	function reolad_info_page(){
		if(page && page == 'PageSets'){ HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(rEl){
			let DNew =  {}; DNew.elem = rEl;
				DNew.temp = DNew.elem.config.sensor_temperature || '';
				DNew.relay = DNew.elem.config.boil_device || '';
				DNew.pump = DNew.elem.config.pump_device || '';
				DNew.addit = DNew.elem.config.sensor_environment || '';
			
				D = DNew;
				let MNew = creator_m(DNew);// создаём адекватное структурирование данных
				let S = {};
				
				if(MNew.stateVal == 112){HWS.getElem('#dop-text-info-elper').innerHTML = '<span class="btn_text grey btn_whey_error">Почему?</span>'}
				else {HWS.getElem('#dop-text-info-elper').innerHTML = ''}
				
				// Имя
				if(!nameFocus && M.name != MNew.name){HWS.getElem('#prog-name').value = MNew.name;}
				
				//Ручной режим
				if(!C.manualMode && M.manualMode != MNew.manualMode ){M.manualMode = MNew.manualMode; 
					let btnMode = HWS.getElem('.btn_onof_mode');
					let textPole = HWS.getElem('#manual-mode-info');
					if(M.manualMode){
						btnMode.classList.remove('active');
						textPole.innerHTML = '';
					} else  {
						btnMode.classList.add('active');
						textPole.innerHTML = 'Вручную';
					}
				}
				
				// В виджеты
				if(!C.favorite && M.viget != MNew.viget){ M.viget = MNew.viget;
					let favorite = HWS.getElem('#equp-viget');
					(MNew.viget)?favorite.classList.add('active'):favorite.classList.remove('active');
				}
								
				//расписание кнопка
				if(MNew.tempTimetable != M.tempTimetable){
					let check = HWS.getElem('#temp-supported-by-timetable checkbox');
					let table = HWS.getElem('#heating-schedule-timetable');
					if(MNew.tempTimetable){
						check.classList.add('active'); table.classList.add('open');
					} else { 
						check.classList.remove('active'); table.classList.remove('open');
					}
				}
				
				// Таблица расписания
				if(!C.tempTimetable && MNew.tempTimetable && MNew.tempTimetableDat != M.tempTimetableDat){
					buttonsController.updateTimetable('#timetable-temp',MNew.tempTimetableDat);
				}
				
				//Поддерживаемая Температура
				if(M.tempSuport != MNew.tempSuport || MNew.tempTimetable != M.tempTimetable){
					let Block = {};
						Block.tempSB = HWS.getElem('#temp-supported-by');
						Block.tempSB.innerHTML = MNew.tempSuport;
						
					if(!C.tempSuport){// проверка на якорь
						function change_btn_temp(id,act){
							let check = HWS.getElem(id+' checkbox');
							if(act){check.classList.add('active')}
							else {check.classList.remove('active')}
						}

						S.tempEcoAct = (MNew.tempSuport == MNew.tempEco && !MNew.tempTimetable)?'active':'';
						S.tempStdAct = (MNew.tempSuport == MNew.tempStd && !MNew.tempTimetable)?'active':'';
						S.tempKomAct = (MNew.tempSuport == MNew.tempKom && !MNew.tempTimetable)?'active':'';
						S.tempCusAct = (MNew.tempSuport == MNew.tempCus && !MNew.tempTimetable)?'active':'';
						
						change_btn_temp('#temp-supported-by-eco',S.tempEcoAct);
						change_btn_temp('#temp-supported-by-standart',S.tempStdAct);
						change_btn_temp('#temp-supported-by-komfort',S.tempKomAct);
						change_btn_temp('#temp-supported-by-custom',S.tempCusAct);
					}
				}
				//Эконом Температура
				if(!C.tempSuport && M.tempEco != MNew.tempEco){HWS.getElem('#temp-supported-by-eco .inp_num_val').value = MNew.tempEco;}
				//Стандарт Температура
				if(!C.tempSuport && M.tempStd != MNew.tempStd){HWS.getElem('#temp-supported-by-standart .inp_num_val').value = MNew.tempStd;}
				//Комфорт Температура
				if(!C.tempSuport && M.tempKom != MNew.tempKom){HWS.getElem('#temp-supported-by-komfort .inp_num_val').value = MNew.tempKom;}
				//Кастом Температура
				if(!C.tempSuport && M.tempCus != MNew.tempCus){HWS.getElem('#temp-supported-by-custom .inp_num_val').value = MNew.tempCus;}
				
				//Глистерезис
				if(!C.glist && M.glist != MNew.glist){ HWS.getElem('#glista-num .inp_num_val').value = MNew.glist; }
				
				// Насос
				if(!C.pumpVal && M.pumpVal != MNew.pumpVal){
					let checkNosto = HWS.getElem('.btn_pump_nonstop');
					let checkTimer = HWS.getElem('.btn_pump_timer');
					let time = HWS.getElem('#heating-pump-timer');
					let timeTimer = HWS.getElem('#heating-pump-timer .inp_num_val');
					
					if(MNew.pumpVal != 65535){
						checkNosto.classList.remove('active');
						checkTimer.classList.add('active');
						time.classList.remove('display_none');
					} else  {
						checkNosto.classList.add('active');
						checkTimer.classList.remove('active');
						time.classList.add('display_none');
					}
					timeTimer.value = MNew.pumpVal;
				}
				
				// Инверсия
				if(!C.invers && M.invers != MNew.invers){
					if(MNew.invers){HWS.getElem('.btn_inversion_mode').classList.add('active');}
					else {HWS.getElem('.btn_inversion_mode').classList.remove('active');}
				}
				
				if(!C.idRelay){buttonsController.updateSes('#change-uuo',MNew.idRelay);}
				if(!C.pump){buttonsController.updateSes('#change-uu-nasos',MNew.idPump);}
				if(!C.idTemp){buttonsController.updateSes('#change-equp-temp',MNew.idTemp);}
				
				// если есть АДАПТЕР 
 				if(MNew.adapter){
					let equp = HWS.buffer.equps[MNew.idRelay];
					if (!C.pza && equp.config.pza_curve_number){
						let numPza = HWS.getElem('.btn_select_pza');
						let graph  = HWS.getElem('#spec-svg-boiler-control object');
						let svg = graph.contentDocument;
						let val = parseInt(equp.config.pza_curve_number);
						
						numPza.value = val-1;
						if(svg){
							if(svg.querySelector('.wac.selection')){svg.querySelector('.wac.selection').classList.remove('selection')}
							svg.querySelector('#wac-'+val).classList.add('selection');
						}
					}
					S.radiatorColor = (equp.state.channel_1)?HWD.color.blue:HWD.color.grey;
					S.faucetColor = (equp.state.heat_water)?HWD.color.blue:HWD.color.grey;
					
					S.coolant_temperature = equp.state.coolant_temperature || '';
					S.sensor_temperature_environment = (MNew.elem.config.sensor_environment)?MNew.elem.config.sensor_environment.lk.state_name:'';
					S.pressure = '';//equp.state.pressure || ''; 
					S.hot_water_supply_temperature = equp.state.hot_water_supply_temperature || ''; 
					S.bar = equp.state.pressure || ''; 
					S.hot_water_supply_expense = equp.state.hot_water_supply_expense || ''; 
					
					if(S.coolant_temperature){S.coolant_temperature = S.coolant_temperature.toFixed(1)}
					if(S.hot_water_supply_temperature){S.hot_water_supply_temperature = S.hot_water_supply_temperature.toFixed(1)}
					if(S.hot_water_supply_expense){S.hot_water_supply_expense = S.hot_water_supply_expense.toFixed(1)}
					
					S.pzaNum = equp.config.pza_curve_number || 1;
					if(S.pressure == 255){S.pressure = '';}
					if(S.bar == 255){S.bar = '';}
					
					S.relay_mode = equp.config.relay_mode || 0;
					S.pza = equp.config.pza || 0;
					S.aux_sensor = equp.config.aux_sensor || 0;
					S.adapterListNumb = 0;
					
					let pzaBlock = HWS.getElem('#pza-heating-graph-blockc');
					if(S.adapterListNumb == 0) {pzaBlock.classList.add('display_none');}
					if(S.relay_mode == 1 && S.pza == 1 && S.aux_sensor == 1	){S.adapterListNumb = 4; pzaBlock.classList.remove('display_none');}
					if(S.relay_mode == 0 && S.pza == 1 && S.aux_sensor == 1	){S.adapterListNumb = 3; pzaBlock.classList.remove('display_none');}
					if(S.relay_mode == 0 && S.pza == 1 && S.aux_sensor == 0	){S.adapterListNumb = 2; pzaBlock.classList.remove('display_none');}
					if(S.relay_mode == 1 && S.pza == 0){S.adapterListNumb = 1;pzaBlock.classList.add('display_none');}
					
					
					if(S.sensor_temperature_environment == 32767 || S.sensor_temperature_environment == 255) {S.sensor_temperature_environment = '';}
					if(S.pressure == 32767 || S.pressure == 255) {S.pressure = '';}
					if(S.coolant_temperature == 32767 || S.coolant_temperature == 255) {S.coolant_temperature = '';}
					if(S.hot_water_supply_temperature == 32767 || S.hot_water_supply_temperature == 255) {S.hot_water_supply_temperature = '';}
					if(S.bar == 32767 || S.bar == 255) {S.bar = '';}
					if(S.hot_water_supply_expense == 32767 || S.hot_water_supply_expense == 255) {S.hot_water_supply_expense = '';}
					
					S.sensor_temperature_environment_unit = '';//(S.sensor_temperature_environment)?'°':'';
					S.pressure_unit = (S.pressure)?'%':'';
					S.coolant_temperature_unit = (S.coolant_temperature)?'°':'';
					S.hot_water_supply_temperature_unit = (S.hot_water_supply_temperature)?'°':'';
					S.bar_unit = (S.bar)?'бар':'';
					S.hot_water_supply_expense_unit = (S.hot_water_supply_expense)?'л/мин':'';
					
					
					HWS.getElem('.btn_temp_correct').value = S.adapterListNumb;
					HWS.getElem('#boil_coolant_temperature').innerHTML = S.coolant_temperature;
					HWS.getElem('#boil_sensor_temperature_environment').innerHTML = S.sensor_temperature_environment;
					//S.pressure = equp.state.pressure || ''; 
					HWS.getElem('#boil_hot_water_supply_temperature').innerHTML = S.hot_water_supply_temperature; 
					HWS.getElem('#boil_bar').innerHTML = S.bar || ''; 
					HWS.getElem('#boil_hot_water_supply_expense').innerHTML = S.hot_water_supply_expense; 
					HWS.getElem('#icon-state-cotel-radiator').style.background = S.radiatorColor ;
					HWS.getElem('#icon-state-cotel-faucet').style.background = S.faucetColor;
					
					let errorBlock = HWS.getElem('#error-block-info');
					if(equp.error_description && equp.error_description.status){
						let errorCode = HWS.getElem('#code-error');
						let textrCode = HWS.getElem('#text-info-error');
						errorBlock.classList.remove('of');
						errorBlock.classList.add('on');
						errorCode.innerHTML = equp.error_description.code || ''; 
						textrCode.innerHTML = equp.error_description.details || '';
					} else {
						errorBlock.classList.remove('on');
						errorBlock.classList.add('of');
					}

				}
				//Время в системе
				let systemTime = HWS.getElem('#equp-system-time');
				systemTime.innerHTML = HWF.unixTime(M.systemTime,true).full
				
				if(JSON.stringify(C) == "{}"){M = MNew;}
				re_print_state(MNew)// проверка и изменение статусов и температуры
				C = {} // очищение якорей
		}}); }
		
	}

	/* pages */
	HWS.buffer.PageSets = function(){ page = 'PageSets';
		clear_graph();
		let S = {};
		let print = '';
		S.inversAct = (M.invers)?'active':'';
		S.tempTimetableAct = (M.tempTimetable)?'active':'';
		S.tempTimetableOpn = (M.tempTimetable)?'open':'';
		S.manualModeAct = (M.manualMode)?'':'active';
		S.controlerBtn = true;
		if (M.tempSuport == M.tempEco && !M.tempTimetable && S.controlerBtn){S.tempEcoAct = 'active'; S.controlerBtn = false};
		if (M.tempSuport == M.tempStd && !M.tempTimetable && S.controlerBtn){S.tempStdAct = 'active'; S.controlerBtn = false};
		if (M.tempSuport == M.tempKom && !M.tempTimetable && S.controlerBtn){S.tempKomAct = 'active'; S.controlerBtn = false};
		if (M.tempSuport == M.tempCus && !M.tempTimetable && S.controlerBtn){S.tempCusAct = 'active'; S.controlerBtn = false};
		S.tempSuport_unit = (M.tempSuport)?'°':'';
		S.favorite = (M.viget)?'active':'';
		S.flameColor = '';
		S.flameText = '';

		if(M.idRelay){
			S.flameColor = (M.blambaColor == HWD.color.grey)? HWD.color.grey2 : M.blambaColor;
			S.flameText = M.stateText;
			if(M.adapter){content.classList.add('adapter')}
			else {content.classList.remove('adapter')}
		}
		else {S.flameColor = ET.state[0].colorBlamba; S.flameText = ET.state[0].name;}
		console.log(M.elem);
		if(M.elem.system_info_dev == 49 || M.elem.system_info_dev == 48){content.classList.add('system30')}
		
		if(M.pumpVal != 65535){S.pumpS1 = ''; S.pumpS2 = 'active';  S.pumpSinp = ''}
		else {S.pumpS1 = 'active'; S.pumpS2 = '';  S.pumpSinp = 'display_none';}
		
		S.adapterError = ''; S.adapterErrorCod = ''; S.adapterErrorText = '';
		S.TimeSys = HWF.unixTime(M.systemTime,true);
		// Если есть Адаптер
		if(M.adapter){
			let equp = HWS.buffer.equps[M.idRelay];
			S.flameColor = (equp.state.flame)?HWD.color.blue:HWD.color.grey2;
			S.radiatorColor = (equp.state.channel_1)?HWD.color.blue:HWD.color.grey2;
			S.faucetColor = (equp.state.heat_water)?HWD.color.blue:HWD.color.grey2;
			
			S.coolant_temperature = equp.state.coolant_temperature || '';
			S.sensor_temperature_environment = (M.elem.config.sensor_environment)?M.elem.config.sensor_environment.lk.state_name:'';
			S.pressure = '';//equp.state.pressure || ''; 
			S.hot_water_supply_temperature = equp.state.hot_water_supply_temperature || ''; 
			S.bar = equp.state.pressure || ''; 
			S.hot_water_supply_expense = equp.state.hot_water_supply_expense || ''; 
			
			if(S.coolant_temperature){S.coolant_temperature = S.coolant_temperature.toFixed(1)}
			if(S.hot_water_supply_temperature){S.hot_water_supply_temperature = S.hot_water_supply_temperature.toFixed(1)}
			if(S.hot_water_supply_expense){S.hot_water_supply_expense = S.hot_water_supply_expense.toFixed(1)}
			
			S.pzaNum = equp.config.pza_curve_number || 1;
			if(S.pressure == 255){S.pressure = '';}
			if(S.bar == 255){S.bar = '';}
			
			if(S.sensor_temperature_environment == 32767 || S.sensor_temperature_environment == 255) {S.sensor_temperature_environment = '';}
			if(S.pressure == 32767 || S.pressure == 255) {S.pressure = '';}
			if(S.coolant_temperature == 32767 || S.coolant_temperature == 255) {S.coolant_temperature = '';}
			if(S.hot_water_supply_temperature == 32767 || S.hot_water_supply_temperature == 255) {S.hot_water_supply_temperature = '';}
			if(S.bar == 32767 || S.bar == 255) {S.bar = '';}
			if(S.hot_water_supply_expense == 32767 || S.hot_water_supply_expense == 255) {S.hot_water_supply_expense = '';}
			
			S.relay_mode = equp.config.relay_mode || 0;
			S.pza = equp.config.pza || 0;
			S.aux_sensor = equp.config.aux_sensor || 0;
			S.adapterListNumb = 0;
			S.pza_display = 'display_none';
			
			if(S.relay_mode == 1 && S.pza == 1 && S.aux_sensor == 1	){S.adapterListNumb = 4; S.pza_display = ''; }
			if(S.relay_mode == 0 && S.pza == 1 && S.aux_sensor == 1	){S.adapterListNumb = 3; S.pza_display = '';}
			if(S.relay_mode == 0 && S.pza == 1 && S.aux_sensor == 0	){S.adapterListNumb = 2; S.pza_display = '';}
			if(S.relay_mode == 1 && S.pza == 0){S.adapterListNumb = 1; S.pza_display = 'display_none';}
			
			S.adapterError = 'of'; S.adapterErrorCod = ''; S.adapterErrorText = '';
			if(equp.error_description && equp.error_description.status){
				S.adapterError = 'on'; S.adapterErrorCod = equp.error_description.code || ''; S.adapterErrorText = equp.error_description.details || '';
			}
		}
		
		S.sensor_temperature_environment_unit = ''//(S.sensor_temperature_environment)?'°':'';
		S.pressure_unit = (S.pressure)?'%':'';
		S.coolant_temperature_unit = (S.coolant_temperature)?'°':'';
		S.hot_water_supply_temperature_unit = (S.hot_water_supply_temperature)?'°':'';
		S.bar_unit = (S.bar)?'бар':'';
		S.hot_water_supply_expense_unit = (S.hot_water_supply_expense)?'л/мин':'';
		
		print += '<div class="one-block-col">\
			<div class="title"><h4>Глобальные</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Ручной режим '+HWhtml.helper(HWD.helpText.prog.heatingManualMode)+'</p>\
					<p><span class="on-off btn-active '+S.manualModeAct+' btn_onof_mode"></span></p>\
				</div>\
				<div class="colum">\
					<p>В виджеты '+HWhtml.helper(HWD.helpText.prog.btnHelpVviget)+'</p>\
					<span id="equp-viget" class="equp_viget btn-active on-off '+S.favorite+'" ></span>\
				</div>\
			</div>\
		</div>';
		
		print += '<div class="one-block-col">\
			<div class="title"><h4>Состояние</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum4x">\
					<p>Состояние '+HWhtml.helper(HWD.helpText.prog.heatingTakeTempState)+'</p>\
					<br>\
					<p class="display_inline ta_center">\
						<span id="icon-state-cotel-flsme" class="icon icon-programs-flame" style="background:'+S.flameColor+'"></span>\
						<span id="icon-state-cotel-radiator" class="ofadapter icon icon-radiator" style="background:'+S.radiatorColor+'"></span>\
						<span id="icon-state-cotel-faucet" class="ofadapter icon icon-faucet" style="background:'+S.faucetColor+'"></span>\
						<br>\
						<!--span id="icon-state-cotel-name" class="">'+S.flameText+'</span-->\
					</p>\
				</div>\
				\
				<div class="colum4x">\
					<p>Поддерживается '+HWhtml.helper(HWD.helpText.prog.heatingSupurtedTemp)+'</p>\
					<br>\
					<p><span id="temp-supported-by" class="ts_36p tf_mb">'+M.tempSuport+'</span> <span class="tf_mb ts_24p">'+S.tempSuport_unit+'<span></p>\
				</div>\
				<div class="colum4x">\
					<p>В помещении '+HWhtml.helper(HWD.helpText.prog.heatingTakeTemp)+'</p><br>\
					<p><span id="heating-state-val" class="ts_36p tf_mb">'+M.tempActive+'</span></p>\
				</div>\
				<div class="colum4x ofadapter">\
					<p>На улице</p><br>\
					<p><span id="boil_sensor_temperature_environment" class="ts_36p tf_mb">'+(S.sensor_temperature_environment || '')+'</span> <span class="tf_mb ts_24p">'+S.sensor_temperature_environment_unit+'<span></p>\
				</div>\
				\
				<!--div class="colum4x ofadapter">\
					<p>Модуляция горелки</p><br>\
					<p><span id="boil_" class="ts_36p tf_mb">'+S.pressure+'</span> <span class="tf_mb ts_24p">'+S.pressure_unit+'<span></p>\
				</div-->\
				<div class="colum4x ofadapter">\
					<p>Теплоноситель</p><br>\
					<p><span id="boil_coolant_temperature" class="ts_36p tf_mb">'+(S.coolant_temperature || '')+'</span> <span class="tf_mb ts_24p">'+S.coolant_temperature_unit+'<span></p>\
				</div>\
				<div class="colum4x ofadapter">\
					<p>Горячая вода</p><br>\
					<p><span id="boil_hot_water_supply_temperature" class="ts_36p tf_mb">'+(S.hot_water_supply_temperature || '')+'</span> <span class="tf_mb ts_24p">'+S.hot_water_supply_temperature_unit+'<span></p>\
				</div>\
				<div class="colum4x ofadapter">\
					<p>Давление</p><br>\
					<p><span id="boil_bar" class="ts_36p tf_mb">'+S.bar +'</span> <span class="tf_mb ts_24p">'+(S.bar_unit || '')+'<span></p>\
				</div>\
				<div class="colum4x ofadapter">\
					<p>Расход</p><br>\
					<p><span id="boil_hot_water_supply_expense" class="ts_36p tf_mb">'+(S.hot_water_supply_expense || '')+'</span> <span class="tf_mb ts_24p">'+S.hot_water_supply_expense_unit+'<span></p>\
				</div>\
				\
				<div id="error-block-info" class="colum690 adapter-error '+S.adapterError+' ofadapter">\
					<div class="colum4x">\
						<div class="adapter-of tc_green tf_fr900"><span class="icon icon-check green p32"></span>Ошибок нет</div>\
						<div class="adapter-on">\
							<p class="tc_red tf_fr900 ts_18p block_pb_20">\
								<span class="icon icon-alarm red p32"></span>\
								<spna class="vertical-middle block_pl_5"><span>Ошибка:</span> <span id="code-error">'+S.adapterErrorCod +'</span></span>\
							</p>\
							<p><span class="style_btn green btn_rebout_error p2">Сброс ошибки</span></p>\
						</div>\
					</div>\
					<div class="colum400 adapter-on">\
						<p id="text-info-error" class="tf_fr500">'+S.adapterErrorText+'</p>\<br>\
					</div>\
				</div>\
			</div>\
		</div>'
		
		print += '<div class="one-block-col">\
			<div class="title"><h4>Температура поддержания '+HWhtml.helper(HWD.helpText.prog.heatingTakeTempTupe)+'</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div id="temp-supported-by-eco" class="colum">\
					<p><checkbox data-fcn="btnCheckEco" name="temp" class="ecto no_of '+S.tempEcoAct+' btn_open_control" data-of="#heating-schedule-timetable">Эконом</checkbox></p>\
					<br>\
					<p>'+HWhtml.btn.numb(M.tempEco,{fcn:'bntValEco',min:-40,max:99,step:0.5,tofix:1})+'</p>\
				</div>\
				<div id="temp-supported-by-standart" class="colum">\
					<p><checkbox data-fcn="btnCheckStd" name="temp" class="ecto no_of '+S.tempStdAct+' btn_open_control" data-of="#heating-schedule-timetable">Стандарт</checkbox></p>\
					<br>\
					<p>'+HWhtml.btn.numb(M.tempStd,{fcn:'bntValStd',min:-40,max:99,step:0.5,tofix:1})+'</p>\
				</div>\
				<div id="temp-supported-by-komfort" class="colum">\
					<p><checkbox data-fcn="btnCheckKom" name="temp" class="ecto no_of '+S.tempKomAct+' btn_open_control" data-of="#heating-schedule-timetable">Комфорт</checkbox></p>\
					<br>\
					<p>'+HWhtml.btn.numb(M.tempKom,{fcn:'bntValKom',min:-40,max:99,step:0.5,tofix:1})+'</p>\
				</div>\
				<div id="temp-supported-by-custom" class="colum">\
					<p><checkbox data-fcn="btnCheckCus" name="temp" class="ecto no_of '+S.tempCusAct+' btn_open_control" data-of="#heating-schedule-timetable">Свое</checkbox></p>\
					<br>\
					<p>'+HWhtml.btn.numb(M.tempCus,{fcn:'bntValCus',min:-40,max:99,step:0.5,tofix:1})+'</p>\
				</div>\
				<div id="temp-supported-by-timetable" class="colum">\
					<p><checkbox data-fcn="btnCheckTim" name="temp" class="ecto no_of '+S.tempTimetableAct+' btn_open_control" data-on="#heating-schedule-timetable">Расписание</checkbox></p>\
					<br>\
					<p></p>\
				</div>\
				<div id="heating-schedule-timetable" class="columFull block-onof '+S.tempTimetableOpn+'">\
					<div>\
						<timetable id="timetable-temp" data-fcn="timetableUpFcn" size="20" data-data="'+M.tempTimetableDat+'">\
						<val value="0" color="#569AD4">Эконом</val>\
						<val value="1" color="#ffae00">Стандарт</val>\
						<val value="2" color="#f22222">Комфорт</val>\
						</timetable>\
						<div class="block_pt_20">Текущее время в системе:<br> <span id="equp-system-time" class="tw_600 block_pt_4" >'+S.TimeSys.full+'</span></div>\
					</div>\
				</div>\
			</div>\
		</div>'
		
		S.adapterList = ['Термостат','Термостат, релейный режим','ПЗА с уличным датчиком','ПЗА с 2мя датчиками','ПЗА с 2мя датчиками, релейный режим'];
		print += '<div class="one-block-col ofadapterF">\
			<div class="title"><h4>Температура поддержания</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Режим работы адаптера</p>\
					<p>'+HWhtml.select(S.adapterList,'btn_temp_correct',S.adapterListNumb)+'</p>\
				</div>\
			</div>\
		</div>';
		
		S.pza_number = S.pzaNum|| 1;
		
		print += '<div id="pza-heating-graph-blockc" class="one-block-col ofadapterF '+S.pza_display+'">\
			<div class="title"><h4>ПЗА</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="colum">\
					<p>Номер кривой ПЗА '+HWhtml.helper(HWD.helpText.equp.btnHelpboilerPZA)+'</p>\
					<p>'+HWhtml.select([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],'btn_select_pza block_w_170p',(S.pza_number-1))+'</p>\
				</div>\
			</div>\
			<div class="title">\
				<div id="spec-svg-boiler-control" class="block_pl_25" data-selel="">\
					<object onload="HWequip.openThermInitMap(this)" type="image/svg+xml" data="img/wac.svg" data-selel="'+S.pza_number+'" data-fcn="btnChangePZAobject"></object>\
				</div>\
			</div>\
		</div>';
		
		print += '<div class="one-block-col">\
			<div class="title"><h4>Настройки</h4></div>\
			<div class="ltft"></div>\
			<div class="right selectorTimeControl">\
				<div class="columFull">\
					<p>Дельта (гистерезис) '+HWhtml.helper(HWD.helpText.prog.heatingGlisterezis)+'</p>\
					<p class="ts_08">Внимание! Не меняйте эту настройку без необходимости!</p>\
					<br>'+
					'<p>'+HWhtml.btn.numb(M.glist.toFixed(1),{id:'glista-num',fcn:'btnGlistScrol',step:0.1,tofix:1,min:0.1,max:5})+'</p>\
				</div>\
				<div class="colum">\
					<ses id="change-uuo" data-load="loadUUo" data-fcn="changeUUo" data-fcnd="changeUUod" class="list" data-id="'+M.idRelay+'">Управление отоплением</ses>\
				</div>\
				<div class="colum ofadapterR">\
					<p>Инверсный режим реле '+HWhtml.helper(HWD.helpText.prog.heatingInverRelay)+'</p>\
					<br>\
					<p><span class="btn_inversion_mode btn-active on-off '+S.inversAct+'"></span></p>\
				</div>\
				<div class="colum">\
					<ses id="change-equp-temp" data-load="loadUUt" data-fcn="changeUUt" data-fcnd="changeUUtd" data-filter="sensor" class="list" data-id="'+M.idTemp+'">Температура помещения</ses>\
				</div>\
				<div class="colum ofadapter">\
					<ses id="change-equp-temp-out" data-load="loadUUtO" data-fcn="changeUUtO" data-fcnd="changeUUtdO" data-filter="sensor" class="list" data-id="'+M.idTempAddit+'">Температура улицы</ses>\
				</div>\
				<div class="colum">\
					<ses id="change-uu-nasos" data-fcn="changeUUn" data-fcnd="changeUUnd" class="list" data-filter="pump" data-id="'+M.idPump+'">Управление насосом</ses>\
				</div>\
				<div class="colum">\
					<p><span class="btn_pump_timer btn-active-one btn-checkboxEC '+S.pumpS2+'" data-target="selectorTimeControl">Выбег насоса, сек</span> '+HWhtml.helper(HWD.helpText.prog.heatingPumpRun)+'</p>\
					<br>\
					<p>'+HWhtml.btn.numb(M.pumpVal,{fcn:'btnTimeFuncPump',min:'null',id:'heating-pump-timer',clas:S.pumpSinp})+'</p>\
				</div>\
				<div class="colum">\
					<p><span class="btn_pump_nonstop btn-active-one btn-checkboxEC '+S.pumpS1+'" data-target="selectorTimeControl">Насос вкл.всегда</span> '+HWhtml.helper(HWD.helpText.prog.heatingPumpOnlyOn)+'</p>\
				</div>\
			</div>\
		</div>';
		
		print += '<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin"></p>\
					<p class="btn_text grey btn_delete_prog">Удалить программу</p>\
				</div>\
			</div>\
		</div>';
		// отрисовка HTML 
		content.innerHTML = print;
		// запуск кнопок ПРограмм
		buttonsController.start();
	}
	HWS.buffer.PageGraph = function(){ page = 'PageGraph';
		//content.innerHTML = HWhtml.loader();
		clear_graph();
		let timeGraph = HWS.buffer.timeValueGraph || 1;
		let print = '<div class="one-block-col">\
			<div class="title"><h4>Статистика работы отопителя  <span data-text="Графики работы всех устройств этой программы" class="helper">?</span></h4></div>\
			<div class="ltft"></div>\
			<div class="title">\
				<div id="equp-statistic-graf" class="block-w-100">'+
					HWhtml.timeControler(timeGraph,'timeControlLoadAllGraph','',{load:'of'})+
					'<div class="graph-window-heating block-w-100" style="height:200px;"> '+HWhtml.loader()+' </div>'+
				'</div>'+
			'</div>'+
		'</div>';
		
		content.innerHTML = print;
		create_graph(timeGraph);
	}
	HWS.buffer.PageJurnal = function(){ page = 'PageJurnal';
		clear_graph();
		let print = '';
		content.innerHTML = print;
		
		HWS.buffer.load_jurnal(1);
	}
	HWS.buffer.PageAccesses = function(){ page = 'PageAccesses';
		clear_graph();
		HWS.buffer.equpAccessesFCN();
	}
	
	load_all_info();

	
	/* ACTIV */
	// Изменение имени программы
	HW.on('focusin','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; nameFocus = 1;
	});
	HW.on('focusout','input-name-page',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; nameFocus = 1;
	});
	HW.on('input','input-name-page',function(e,el){nameFocus = 1}); HW.on('click','input-name-page',function(e,el){nameFocus = 1});
	HW.on('change','input-name-page',function(e,el){	
		nameFocus = false;
		let name = document.body.querySelector('.ob-top .name-page input');	
		//let nameRegex = new RegExp(HWD.reg.name);	

		if(HWS.buffer.elem.config.name != name.value){
			//if(nameRegex.test(name.value)){
			if(name.value){
				M.name = name.value;
				let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
				HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
			
	// Ручной режим:
	HW.on('click','btn_onof_mode',function(e,el){
		let nameB = HWS.getElem('#manual-mode-info');
		C.manualMode = 1; // якорь для автообновления
		if(el.classList.contains('active')){// on
			M.manualMode = 0;
			let dataP = {token:HWS.pageAccess,id_object:id,active:0}
			HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
			nameB.innerHTML = 'Вручную';
		}
		else{// of
			M.manualMode = 1;
			let dataP = {token:HWS.pageAccess,id_object:id,active:1}
			HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
			nameB.innerHTML = '';
		}
	});
	
	// В виджеты
	HW.on('click','equp_viget',function(e,elem){
		C.favorite = 1;
		if(elem.classList.contains('active')){ // On
			M.viget = 1;
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/add_favorite_objects',data:dataP});
		}else { // Of
			M.viget = 0;
			let dataP = {token:HWS.pageAccess,id_objects:[id]}
			HWF.post({url:'objects/delete_favorite_objects',data:dataP});
		}
	});
	
	HW.on('click','btn_dop_text_helper',function(e,elem){
		let contPrint = '\
		<ul class="number-info-print">\
			<li>Датчик температуры не задан в программе;</li>\
			<li>Датчик температуры отключен;</li>\
			<li>Датчик температуры не на связи</li>\
		</ul>\
		<div class="ta_left block_pl_30">Алгоритм работы аварийного режима: отопитель включается и отключается <br> каждые 15 минут до появления датчика температуры в программе.</div>\
		';
		
		HWF.modal.message('Авариный режим? Причина:',contPrint);
	});
	
	//Температура поддерживания
	// Econom
	HWS.buffer.btnCheckEco = function(inf,el){
		C.tempSuport = 1;
		let val = el.parentElement.parentElement.querySelector('.btn-int  input').value; val = val;
		if(inf == 'on'){
			let dataP = {token:HWS.pageAccess,id_object:id,pau_therm_value:val,control_from_schedule:0}
			HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
		}
		HWS.buffer.tempBtnActive = el;
		let bInfo = HWS.getElem('#temp-supported-by'); bInfo.innerHTML = val;
	}
	HWS.buffer.bntValEco = function(inf){
		M.tempEco = inf;
		C.tempSuport = 1; // Якоря автообновы
		let dataP = '';
		let check = HWS.getElem('#temp-supported-by-eco checkbox');
		let bInfo = HWS.getElem('#temp-supported-by');
		
		if(check.classList.contains('active')){
			 bInfo.innerHTML = inf;
			dataP = {token:HWS.pageAccess,id_object:id,pau_therm_pre_eco:M.tempEco,pau_therm_value:M.tempEco}
		} else {
			dataP = {token:HWS.pageAccess,id_object:id,pau_therm_pre_eco:M.tempEco}
		}
		
		clearTimeout(HWS.buffer.bntValTempTimer);
		HWS.buffer.bntValTempTimer = setTimeout(function(){ HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP}); },2000);
	}
	// Standart
	HWS.buffer.btnCheckStd = function(inf,el){
		C.tempSuport = 1;
		let val = el.parentElement.parentElement.querySelector('.btn-int input').value; val = val;
		if(inf == 'on'){let dataP = {token:HWS.pageAccess,id_object:id,pau_therm_value:val,control_from_schedule:0}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});}
		let bInfo = HWS.getElem('#temp-supported-by'); bInfo.innerHTML = val;
	}
	HWS.buffer.bntValStd = function(inf){
		M.tempStd = inf;
		C.tempSuport = 1; // Якоря автообновы
		let dataP = '';
		let check = HWS.getElem('#temp-supported-by-standart checkbox');
		let bInfo = HWS.getElem('#temp-supported-by');
		
		if(check.classList.contains('active')){
			bInfo.innerHTML = inf;
			dataP = {token:HWS.pageAccess,id_object:id,pau_therm_pre_std:M.tempStd,pau_therm_value:M.tempStd}
		} else {
			dataP = {token:HWS.pageAccess,id_object:id,pau_therm_pre_std:M.tempStd}
		}
		
		clearTimeout(HWS.buffer.bntValTempTimer);
		HWS.buffer.bntValTempTimer = setTimeout(function(){ HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP}); },2000);
	}
	// Komfort
	HWS.buffer.btnCheckKom = function(inf,el){
		C.tempSuport = 1;
		let val = el.parentElement.parentElement.querySelector('.btn-int input').value; val = val;
		if(inf == 'on'){let dataP = {token:HWS.pageAccess,id_object:id,pau_therm_value:val,control_from_schedule:0}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});}
		let bInfo = HWS.getElem('#temp-supported-by'); bInfo.innerHTML = val;
	}
	HWS.buffer.bntValKom = function(inf){
		M.tempKom = inf;
		C.tempSuport = 1; // Якоря автообновы
		let dataP = '';
		let check = HWS.getElem('#temp-supported-by-komfort checkbox');
		let bInfo = HWS.getElem('#temp-supported-by');

		if(check.classList.contains('active')){
			bInfo.innerHTML = inf;
			dataP = {token:HWS.pageAccess,id_object:id,pau_therm_pre_comf:M.tempKom,pau_therm_value:M.tempKom}
		} else {
			dataP = {token:HWS.pageAccess,id_object:id,pau_therm_pre_comf:M.tempKom}
		}
		
		clearTimeout(HWS.buffer.bntValTempTimer);
		HWS.buffer.bntValTempTimer = setTimeout(function(){ HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP}); },2000);
	}
	// Custom
	HWS.buffer.btnCheckCus = function(inf,el){
		C.tempSuport = 1;
		let val = el.parentElement.parentElement.querySelector('.btn-int input').value; val = val;
		if(inf == 'on'){let dataP = {token:HWS.pageAccess,id_object:id,pau_therm_value:val,control_from_schedule:0}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});}
		let bInfo = HWS.getElem('#temp-supported-by'); bInfo.innerHTML = val;
	}
	HWS.buffer.bntValCus = function(inf){
		M.tempCus = inf;
		C.tempSuport = 1; // Якоря автообновы
		let dataP = '';
		let check = HWS.getElem('#temp-supported-by-custom checkbox');
		let bInfo = HWS.getElem('#temp-supported-by');

		if(check.classList.contains('active')){
			bInfo.innerHTML = inf;
			dataP = {token:HWS.pageAccess,id_object:id,pau_therm_pre_usr:M.tempCus,pau_therm_value:M.tempCus}
		} else {
			dataP = {token:HWS.pageAccess,id_object:id,pau_therm_pre_usr:M.tempCus}
		}
		
		clearTimeout(HWS.buffer.bntValTempTimer);
		HWS.buffer.bntValTempTimer = setTimeout(function(){ HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP}); },2000);
	}
	
	// Timetable flags
	HWS.buffer.btnCheckTim = function(inf,el){
		C.tempSuport = 1; // Якоря автообновы
		if(inf == 'on'){
			M.tempTimetable = 1;
			let bInfo = HWS.getElem('#temp-supported-by'); bInfo.innerHTML = '--';
			let dataP = {token:HWS.pageAccess,id_object:id,control_from_schedule:M.tempTimetable}
			HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
		}
		else {
			M.tempTimetable = 0;
			let dataP = {token:HWS.pageAccess,id_object:id,control_from_schedule:M.tempTimetable}
			HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
		}
	}
	// Изменение календаря
	HWS.buffer.timetableUpFcn = function(inf){
		C.tempTimetable = 1;
		clearTimeout(HWS.buffer.idTimerTimetableUpFcn);
		HWS.buffer.idTimerTimetableUpFcn = setTimeout(function(){
			let allElem = inf.allElem;
			let specArrey = []
			for (let key in allElem){
				let val = allElem[key].dataset.val || 0;
				specArrey.push(val);
			}
			let dataP = {token:HWS.pageAccess,id_object:id,pau_therm_time:specArrey}
			HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
		},2000);
	}
	// Изменение гистерезиса 
	HWS.buffer.btnGlistScrol = function(inf){
		M.glist = inf;
		C.glist = 1;
		console.log(inf);
		let dataP = {token:HWS.pageAccess,id_object:id,pau_therm_hyst:inf}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	}
	
	// Инвернсный режим btnInversionMode
	HW.on('click','btn_inversion_mode',function(e,el){
		C.invers = 1; // Якорь для инверсии
		let state = HWS.getElem('#change-uuo').dataset.val;
		if(el.classList.contains('active')){
			M.invers = 1;
			let dataP = {token:HWS.pageAccess,id_object:id,inversion_of_control:M.invers}
			HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
		}
		else {
			M.invers = 0;
			let dataP = {token:HWS.pageAccess,id_object:id,inversion_of_control:M.invers}
			HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
		}	
		change_state();
	});
	
	// Работа насоса
	HW.on('click','btn_pump_nonstop',function(e,el){
		let time = HWS.getElem('#heating-pump-timer');
			time.classList.add('display_none');
		let	timeInp = HWS.getElem('#heating-pump-timer input');
			time.value = 65535;
		M.pumpVal = 65535;
		C.pumpVal = 1; // Якоря для насоса
		let dataP = {token:HWS.pageAccess,id_object:id,pau_therm_del_pump:M.pumpVal}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	});	
	HW.on('click','btn_pump_timer',function(e,el){
		C.pumpVal = 1; // Якоря для насоса
		
		let timeInp = HWS.getElem('#heating-pump-timer input');
			M.pumpVal = timeInp.value || 0;
			if(M.pumpVal == 65535){M.pumpVal = 0; timeInp.value =0;}
		let time = HWS.getElem('#heating-pump-timer');
			time.classList.remove('display_none');
		
		let dataP = {token:HWS.pageAccess,id_object:id,pau_therm_del_pump:M.pumpVal}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	});
	HWS.buffer.btnTimeFuncPump =  function(inf){
		C.pumpVal = 1; // Якоря для насоса
		let checkNosto = HWS.getElem('.btn_pump_nonstop');
		let checkTimer = HWS.getElem('.btn_pump_timer');
		if (inf != 65535){
			checkNosto.classList.remove('active');
			checkTimer.classList.add('active');
		}else {
			checkNosto.classList.add('active');
			checkTimer.classList.remove('active');
			
			let time = HWS.getElem('#heating-pump-timer');
			time.classList.add('display_none');
		}
		M.pumpVal = inf;
		clearTimeout(HWS.buffer.bntValCusPumpTimer);
		HWS.buffer.bntValCusPumpTimer = setTimeout(function(){
			let dataP = {token:HWS.pageAccess,id_object:id,pau_therm_del_pump:inf,wait_code:'bntValCusPumpTimer',time_for_wait:5000}
			HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
		},2000);
	}
	
	// Устройства
	// Загрузка Реле
	HWS.buffer.loadUUo = function(inf){
		console.log(inf);
		change_state();
	}
	// Изменение реле
	HWS.buffer.changeUUo = function(inf){
		C.idRelay = 1; // Якорь для реле
		D.relay = inf;
		M.idRelay = inf.id;
		HWS.buffer.equps[M.idRelay] = D.relay;
		
		let dataP = {token:HWS.pageAccess,id_object:id,id_boil_device:inf.id}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP,fcn:change_state});
	}
	// удаление реле
	HWS.buffer.changeUUod = function(inf){
		C.idRelay = 1; // Якорь для реле
		D.relay = '';
		M.idRelay = '';
		HWS.buffer.equps[M.idRelay] = D.relay;

		let dataP = {token:HWS.pageAccess,id_object:id,id_boil_device:'delete'}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP,fcn:change_state});
	}
	
	// Управление насосом
	HWS.buffer.changeUUn = function(inf){
		D.pump = inf;
		M.idPump = inf.id;
		HWS.buffer.equps[M.idPump] = D.pump;
		C.pump = 1; // Якорь для насоса
		let dataP = {token:HWS.pageAccess,id_object:id,id_pump_device:M.idPump}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	}
	HWS.buffer.changeUUnd = function(inf){
		D.pump = '';
		M.idPump = '';
		HWS.buffer.equps[M.idPump] = D.pump;		
		C.pump = 1; // Якорь для насоса
		let dataP = {token:HWS.pageAccess,id_object:id,id_pump_device:'delete'}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	}
	// Датчик поддержания Загрузка
	HWS.buffer.loadUUt = function(inf){}
	// Датчик поддержания Установка
	HWS.buffer.changeUUt = function(inf){
		C.idTemp = 1; // якоря для Датчика температуры
		D.temp = inf;
		M.idTemp = inf.id;
		HWS.buffer.equps[M.idTemp] = D.temp;	
			
		let dataP = {token:HWS.pageAccess,id_object:id,id_sensor_temperature:M.idTemp}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP,fcn:change_state});
	}// Датчик поддержания Удаление
	HWS.buffer.changeUUtd = function(inf){
		C.idTemp = 1;
		D.temp = '';
		M.idTemp = '';
		HWS.buffer.equps[M.idTemp] = D.temp;
		
		let dataP = {token:HWS.pageAccess,id_object:id,id_sensor_temperature:'delete'}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP,fcn:change_state});
	}	
	
	// Датчик температуры улица
	HWS.buffer.changeUUtO = function(inf){
		if(M.adapter){
			let dataP = {token:HWS.pageAccess,id_object:M.idRelay,id_sensor_environment:inf.id}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP,fcn:change_state});
		}
	}// Датчик температуры улица
	HWS.buffer.changeUUtdO = function(inf){
		if(M.adapter){
			let dataP = {token:HWS.pageAccess,id_object:M.idRelay,id_sensor_environment:'delete'}
			HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP,fcn:change_state});
		}
	}
	
	// Работа граффика
	HWS.buffer.timeControlLoadAllGraph = function(H){
		create_graph(H);
		HWS.buffer.timeValueGraph = H;
		let dataP = {token:HWS.pageAccess,id_object:id,web_config:{time_per_graph:H}}
		HWF.post({url:HWD.api.gets.setWebConfig,data:dataP});
	}
	
	//Температура поддержания
	HW.on('change','btn_temp_correct',function(e,elem){
		let val = elem.value;
		let idRel = M.idRelay
		let dataP = {token:token,id_object:idRel}
		let pzaBlock = HWS.getElem('#pza-heating-graph-blockc');
		
		if(val == 0) {dataP.relay_mode = 0; dataP.pza = 0; dataP.aux_sensor = 0; pzaBlock.classList.add('display_none');}
		if(val == 1) {dataP.relay_mode = 1; dataP.pza = 0; dataP.aux_sensor = 0; pzaBlock.classList.add('display_none');}
		if(val == 2) {dataP.relay_mode = 0; dataP.pza = 1; dataP.aux_sensor = 0; pzaBlock.classList.remove('display_none');}
		if(val == 3) {dataP.relay_mode = 0; dataP.pza = 1; dataP.aux_sensor = 1; pzaBlock.classList.remove('display_none');}
		if(val == 4) {dataP.relay_mode = 1; dataP.pza = 1; dataP.aux_sensor = 1; pzaBlock.classList.remove('display_none');}
		
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	});
	
	//  ПЗА
	HW.on('change','btn_select_pza',function(e,elem){
		C.pza = 1;
		let idRel = M.idRelay
		let graph  = HWS.getElem('#spec-svg-boiler-control object');
		let svg = graph.contentDocument;
		let val = parseInt(elem.value)+1;
		
		if(svg.querySelector('.wac.selection')){svg.querySelector('.wac.selection').classList.remove('selection')}; 
		svg.querySelector('#wac-'+val).classList.add('selection');
		
		let dataP = {token:token,id_object:idRel,pza_curve_number:val}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	});
	
	HWS.buffer.btnChangePZAobject = function(inf){
		C.pza = 1;
		let idRel = M.idRelay
		let selector = HWS.getElem('.btn_select_pza');
			selector.value = inf-1;
		let dataP = {token:token,id_object:idRel,pza_curve_number:inf}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	}
	
	// Нажатие на кнопку сброс ошбки
	HW.on('click','btn_rebout_error',function(e,el){
		let title = '<spam class="">ВНИМАНИЕ! ЭТО МОЖЕТ ПРИВЕСТИ  <br> К НЕПОПРАВИМЫМ ПОСЛЕДСТВИЯМ!</span>';
		let infotext = '<div class="block_p_20 block_w_650p block_m_auto">Нажимая "Сброс", я подтверждаю, что нахожусь непосредственно рядом с котлом и уверен, что причины ошибки полностью устранены. Я уведомлен, что удаленный неконтролируемый перезапуск котла после ошибки может привести к порче имущества, потери здоровья или жизни.</div>';
		HWF.modal.confirm(title,infotext,'btn_accept_rebaut_error','Сброс ошибки');
	});
	HW.on('click','btn_accept_rebaut_error',function(e,el){
		let idRel = M.idRelay;
		HWF.push('Команда сброса ошибок отправлена');
		let dataP = {token:token,id_object:idRel,reset_error:1}
		HWF.post({url:HWD.api.gets.taskOpentherm3xsystem,data:dataP});
	});	
	// Кнопка почему аварийный режим
	HW.on('click','btn_whey_error',function(e,el){
		let title = '<spam class="block_w_650p">Что такое аварийный режим?</span>';
		let infotext = '<div class="block_plr_20 block_w_650p">В настройках программы не указан датчик температуры поддержания или этот датчик не на связи, поэтому программа перешла в аварийный режим. Теперь если котлом управляет реле, оно будет включаться и отключаться с периодом 15 мин, а если котлом управляет адаптер - будет поддерживаться заданная в настройках адаптера аварийная температура в контуре отопления. Как только датчик температуры поддержания восстановит свою работу, аварийный режим отключится автоматически</div>';
		HWF.modal.message(title,infotext);
	});	
	// Кнопка Удолить программу
	HW.on('click','btn_delete_prog',function(e,el){
		let title = 'Что такое аварийный режим?';
		let infotext = 'Данная программа будет удалена из памяти системы. Вы уверены?';
		HWF.modal.confirm('Удалить программу?',infotext,'btn_delete_prog_yes','Удалить');
	});
	HW.on('click','btn_delete_prog_yes',function(e,el){
		let idprog = id || H.hash[1] || false;;
		HWF.modal.loader('Удаление');
		HWF.post({url:'task/delete_program_3x_system',data:{token:token,id_objects:[idprog]},fcn:function(info){
			HWF.modal.of();
			HWS.go('programs');
			HWF.push('Программа удалена','red');
		}});
	});

	// Dop render FCN
	HWS.buffer.notYouElemInfoFCN(); // Проверит ваш ли это объект для вовода предупреждения
});