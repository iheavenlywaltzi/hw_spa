"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1>Пользователь</h1>';
	let menu = '<div>\
			<a class="btn active" href="#user/prof">Профиль</a>\
			<a class="btn" href="#user/friends">Доверенные</a>\
			<a class="btn" href="#user/rate">Тариф</a>\
			<a class="btn" href="#user/pay">Платежи</a>\
		</div><div id="dop-btn-top-menu"></div>';
		
	let btnAddNewUser = '<span class="new_user_add btnIcon">\
			<span class="select-none">Добавить пользователя</span>\
			<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>\
		</span>';
	
	let content = document.createElement('div');
		content.classList.add('main-block'); content.classList.add('max-1200');
		content.classList.add('user-page');
		content.innerHTML = HWhtml.loader();
	let token = HWS.pageAccess;
	let pageA = H.hash[1] || 'prof';

	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);
	
	//** lOGICS **
	if(pageA == 'prof') {HWS.getElem('#dop-btn-top-menu').innerHTML = ''; load_page();}
	if(pageA == 'rate') {HWS.getElem('#dop-btn-top-menu').innerHTML = ''; load_rate()}
	if(pageA == 'pay')  {HWS.getElem('#dop-btn-top-menu').innerHTML = ''; load_pay()}
	if(pageA == 'friends') {HWS.getElem('#dop-btn-top-menu').innerHTML = btnAddNewUser; HW.load('friends');}
	if(pageA == 'notification') {/*HW.load('message');*/}
	//** -lOGICS- **
	
	

	function load_page(){
	HWF.post({url:"user/get_settings",data:{token:token},fcn:function(info){   console.log(info);
		let btnClouse = '<div class="icon icon-clouse btn_exit_admin" title="Выйти ?"></div>';
		let btnSAve = '<div class="icon icon-save btn_save_user" title="Сохранить"></div>';
		let user = {};
		let block = HWhtml.tile('one-block');
		let mid = '';  

		user.id = info.id || '';
		user.phone = info.phone || '';
		user.email = info.email || '';
		user.name = info.name || '';
		//user.last_name = info.surname || '';
		//user.middle_name = info.patronymic || '';
		user.nubm = HWF.emptyStep4(info.user_number);
		
		mid = '\
		<div class="one-block-col">\
			<div class="title flex_between">\
				<h4>Настройки профиля '+HWhtml.helper(HWD.helpText.pages.userSets)+'</h4>\
				<div><span class="tc_grey6">'+HWhtml.helper(HWD.helpText.pages.userId)+' id: <span id="copy_target" >'+user.nubm+'</span></span> \
				<span class="copy_text btn icon icon-send-copy p13 pointer hover blue hover_orange"></span></div>\
			</div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="mcolum">\
					<p class="block_ptb_8">Имя</p> <br>\
					<p class="block_ptb_8">Номер телефона</p> <br>\
					<p class="block_ptb_8">Электронный адрес</p>\
				</div>\
				<div class="colum">\
					<p class="block_p_0"><input id="user_name" type="text" value="'+user.name+'" placeholder="Имя"></p> <br>\
					<div class="block_p_0"><input class="phone_inp_text" id="user_phone" type="tel" maxlength="20" value="'+user.phone+'" placeholder="Телефон"></div> <br>\
					<p class="block_p_0"><input id="user_email" type="text" value="'+user.email+'" placeholder="Email"></p> <br>\
				</div>\
			</div>\
		</div>';
		
		let mainP = localStorage.getItem('PageMain') || 'proj';
		mainP = (mainP == 'proj')?0:1;
		
		/*mid+= '<div class="one-block-col">\
			<div class="title"><h4>Глобальные '+HWhtml.helper(HWD.helpText.pages.userMain)+'</h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="mcolum">\
					<p class="block_ptb_8">Главная</p>\
				</div>\
				<div class="colum">\
					<p>'+HWhtml.select(['Главный проект','Быстрый доступ'],'change_select_main',mainP)+'</p>\
				</div>\
			</div>\
		</div>';*/
		
		mid+= '\
		<div class="one-block-col">\
			<div class="title"></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="columFull flex_between">\
					<p class="btn_text grey btn_exit_admin">Выйти</p>\
					<p class="btn_text grey btn_change_pass">Изменить пароль</p>\
					<div class="style_btn grey btn_save_user">Сохранить настройки</div>\
				</div>\
			</div>\
		</div>';
		content.innerHTML = mid;
	}});
	}
	//Тариф
	function load_rate(){
	HWF.post({url:"billing/get_invoice_data",data:{token:token},fcn:function(info){   console.log(info);
		let top='', mid='', bot='';
		let block = HWhtml.tile('one-block block_w_100');
		let blockT1 = HWhtml.tile('tarif-block block_r9');
		let blockT2 = HWhtml.tile('tarif-block block_r9');
		let blockT3 = HWhtml.tile('tarif-block block_r9');
		let Dinf = {
			id_rate:info.id_rate, // тип тарифа 1)Базовый 2)Комфорт 3)Бизнес 4)Бизнес+
			amount:info.amount, // баланс счета
			count_systems:info.count_systems, // количество систем
			count_projects:info.count_projects, // количество проектов
			rate_state:info.rate_state, // статус тарифа (активен/нет)
			discount:info.discount, // баланс счета Ограничения: 0..100 
			rate_state_text:'Неактивен',
			id_rate_tth_text:'',
			blamba_class:'bstateC3_1_0',
			select_plan_text1:'ДОСТУПЕН',
			select_plan_text2:'ДОСТУПЕН',
			select_plan_text3:'ДОСТУПЕН',
		}
		
		if(Dinf.rate_state){Dinf.rate_state_text = 'Активен'; Dinf.blamba_class = 'bstateC3_1_5'}
		if(Dinf.id_rate == 1){blockT1.b.classList.add('active'); Dinf.select_plan_text1 ='Текущий'; Dinf.id_rate_tth_text = '\
			<li class=""><span class="icon icon-check green"></span>Один проект</li>\
			<li class=""><span class="icon icon-check green"></span>Одна система</li>\
			<li class=""><span class="icon icon-check green"></span>Хранение 3 месяца</li>';}
		if(Dinf.id_rate == 2){blockT2.b.classList.add('active'); Dinf.select_plan_text2 ='Текущий'; Dinf.id_rate_tth_text = '\
			<li class=""><span class="icon icon-check green"></span>Три проекта</li>\
			<li class=""><span class="icon icon-check green"></span>Неограниченное кол-во систем</li>\
			<li class=""><span class="icon icon-check green"></span>Хранение 1 год</li>';}
		if(Dinf.id_rate == 3){blockT3.b.classList.add('active'); Dinf.select_plan_text3 ='Текущий'; Dinf.id_rate_tth_text = '\
			<li class=""><span class="icon icon-check green"></span>Неограниченное кол-во проектов</li>\
			<li class=""><span class="icon icon-check green"></span>Неограниченное кол-во систем</li>\
			<li class=""><span class="icon icon-check green"></span>Хранение 5 лет</li>\
			<li class=""><span class="icon icon-check green"></span>Vip обслуживание, интеграции.</li>';}
		
		top+= '<div class="block_w_100 flex_between">\
			<h5 class="tw_700 tc_black">Текущий тариф</h5>\
		</div>';
		
		mid+= '<div class="spec-blamba '+Dinf.blamba_class+'"></div>';
		mid+= '<div class="block_ptb_15 block_w_100"></div>';
		mid+= '<div class="block_pl_30">\
			<p class="tf_mb tc_black ts_20">Бесплатный</p>\
			<br>\
			<p>Для частного<br>Использования</p>\
		</div>';
		mid+= '<ul class="tarif-info">'+Dinf.id_rate_tth_text+'</ul>';
		mid+= '<div class="">\
			<p class="tc_grey8">Баланс</p>\
			<p class="tw_700 tc_black">'+Dinf.amount+' Р</p>\
			<br>\
			<p class="tc_grey8">Состояние</p>\
			<p class="tw_700 tc_black">'+Dinf.rate_state_text+'</p>\
		</div>';
		mid+= '<div class="">\
			<p class="tc_grey8">Проекты</p>\
			<p class="tw_700 tc_black">'+Dinf.count_projects+' из 1</p>\
			<br>\
			<p class="tc_grey8">Системы</p>\
			<p class="tw_700 tc_black">'+Dinf.count_systems+' из 1</p>\
		</div>';
		mid+= '<div class="ta_right">\
			<div class="btn_send_pay style_btn grey">Оплатить</div>\
			<br><br>\
			<a href="#user/pay" class="btn_text grey">История платежей</a>\
			<br><br>\
		</div>';
		
		block.b.classList.add('block_r9');
		block.mid.classList.add('flex_between_top');
		
		block.top.innerHTML = top;
		block.mid.innerHTML = mid;
		block.bot.innerHTML = bot;
		
		
		blockT1.mid.innerHTML = '\
			<span class="fon"></span>\
			<span class="tc_white">Тариф</span>\
			<h4 class="tc_white">Бесплатный</h4>\
			<hr />\
			<div class="top-info tc_white">\
				<span>Для частного <br> использования</span>\
				<span class="btn">'+Dinf.select_plan_text1+'</span>\
			</div>\
			<p class="sum">0 Р/мес.</p>\
			<p class="tc_grey8 block_pb_4">В этот план входят:</p>\
			<ul>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Один проект</li>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Одна система</li>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Хранение 3 месяца</li>\
			</ul>\
			<span class="btn-set">Выбрать</span>\
		';
		
		blockT2.mid.innerHTML = '\
			<span class="fon"></span>\
			<span class="tc_white">Тариф</span>\
			<h4 class="tc_white">Базовый</h4>\
			<hr />\
			<div class="top-info tc_white">\
				<span>Для малого <br> Бизнеса</span>\
				<span class="btn">'+Dinf.select_plan_text2+'</span>\
			</div>\
			<p class="sum">0 Р/мес.</p>\
			<p class="tc_grey8 block_pb_4">В этот план входят:</p>\
			<ul>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Три проекта</li>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Неограниченное кол-во систем</li>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Хранение 1 год</li>\
			</ul>\
			<span class="btn-set">Выбрать</span>\
		';
		
		blockT3.mid.innerHTML = '\
			<span class="fon"></span>\
			<span class="tc_white">Тариф</span>\
			<h4 class="tc_white">Про</h4>\
			<hr />\
			<div class="top-info tc_white">\
				<span>Для среднего <br>и крупного Бизнеса</span>\
				<span class="btn">'+Dinf.select_plan_text3+'</span>\
			</div>\
			<p class="sum">0 Р/мес.</p>\
			<p class="tc_grey8 block_pb_4">В этот план входят:</p>\
			<ul>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Неограниченное кол-во проектов</li>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Неограниченное кол-во систем</li>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Хранение 5 лет</li>\
				<li class=""><span class="tc_green block_pr_8">✓</span>Vip обслуживание, интеграции.</li>\
			</ul>\
			<span class="btn-set">Выбрать</span>\
		';
		
		content.innerHTML = '';
		content.appendChild(block.b)
		content.appendChild(blockT1.b)
		content.appendChild(blockT2.b)
		content.appendChild(blockT3.b)
	}});
	}
	// Платежи
	function load_pay(){
		let top='', mid='', bot='';
		let block = HWhtml.tile('one-block');
		
		mid+= 'Платежи';
		
		block.top.innerHTML = top;
		block.mid.innerHTML = mid;
		block.bot.innerHTML = bot;
		
		content.innerHTML = '';
		content.appendChild(block.b)
	}

	// Сохранить данные пользователя 
	HW.on('click','btn_save_user',function(e,elem){
		let inf = {}
		inf.name = HW.getElem('#user_name').value || '';
		inf.email = HW.getElem('#user_email').value || '';
		inf.phone = HW.getElem('#user_phone').value || '';
		//inf.surname = HW.getElem('#user_surname').value || '';
		//inf.patronymic = HW.getElem('#user_patronymic').value || '';
		
		send_server();
		
		function send_server(){
			content.innerHTML = HWhtml.loader();
			let data = {
				token:token,
				email:inf.email,
				name:inf.name,
				//surname:inf.surname,
				//patronymic:inf.patronymic,
				phone:inf.phone || ''
			}
			HWF.post({url:"user/change_settings",data:data,
				fcn:load_page // Точка оптимизации. Можно убрать 2й запрос информации, и просто заменить её на текущей вёрстке
			});
		}
	});
	
	// сменить пароль
	HW.on('click','btn_change_pass',function(e,elem){
		let html = '';
		
		html +='\
		<div class="one-block-modal">\
			<div class="mcolum">\
				<p class="block_ptb_8">Новый пароль</p> <br>\
				<p class="block_ptb_8">Повторить пароль</p> <br>\
				<p class="block_ptb_8">Старый пароль</p> \
			</div>\
			<div class="colum">\
				<p class=""><span class="spec-password"><input id="user_password" type="password"><span class="btn_view_pass icon hover_orange icon-read"></span></span></p> <br>\
				<p class=""><span class="spec-password"><input id="user_password_confirmation" type="password"><span class="btn_view_pass icon hover_orange icon-read"></span></span></p> <br>\
				<p class=""><span class="spec-password"><input id="user_old_password" type="password"><span class="btn_view_pass icon hover_orange icon-read"></span></span></p> \
			</div>\
		</div>\
		';
		
		HWF.modal.confirm('Смена пароля',html,'btn_yes_change_modal','Изменить','Отмена');
	});
	//Скопировать текст
	HW.on('click','copy_text',function(e,elem){
		let mesag = HWS.getElem('#copy_target').innerHTML;
		
        var textarea = document.createElement("textarea");
        textarea.id = 'spex_text_cop';
        // Place in top-left corner of screen regardless of scroll position.
        textarea.style.position = 'fixed';
        textarea.style.top = 0;
        textarea.style.left = 0;

        // Ensure it has a small width and height. Setting to 1px / 1em
        // doesn't work as this gives a negative w/h on some browsers.
        textarea.style.width = '1px';
        textarea.style.height = '1px';

        // We don't need padding, reducing the size if it does flash render.
        textarea.style.padding = 0;

        // Clean up any borders.
        textarea.style.border = 'none';
        textarea.style.outline = 'none';
        textarea.style.boxShadow = 'none';

        // Avoid flash of white box if rendered for any reason.
        textarea.style.background = 'transparent';
        document.querySelector("body").appendChild(textarea);
        let existsTextarea = document.getElementById('spex_text_cop');

		existsTextarea.value = mesag;
		existsTextarea.select();
        let status = document.execCommand('copy');
		
		HWF.push('ID пользователя скопирован в буфер обмена');
	});
	
	//Кнопка изменить пароль
	HW.on('click','btn_yes_change_modal',function(e,elem){
		let inf = {}
		inf.newPass = HW.getElem('#user_password').value || '';
		inf.newPassD = HW.getElem('#user_password_confirmation').value || '';
		inf.oldPass = HW.getElem('#user_old_password').value || '1';
		
		HWF.modal.print('Смена пароля',HWhtml.loader());
		
		HWF.post({url:"user/change_password",data:{
			token:token,
			old_password:inf.oldPass,
			password:inf.newPass,
			password_confirmation:inf.newPassD
		},fcn:function(info){
			HWF.modal.message('Пароль успешно изменён','Смена пароля')
		},fcnE:HWF.modalMess});
	});
	
	// send pay
	HWS.on('click','btn_send_pay',function(e,el){
		let inpSum = '<div class="mini-modal-wiev">'+
			'<div class="str">'+
				'<div>Сумма</div>'+
				'<div class="modal-sum-send-maney"><input id="summ-pay-send" class="inp_only_numb" type="text"><span class="spec-unit-p">₽</span></div>'+
			'</div>'+
			'<div class="str opacity02">'+
				'<div>Куда прислать чек?</div>'+
				'<div>'+HWhtml.select(['Телефон','Email'],'block_w_170p')+'</div>'+
			'</div>'+
			'<div class="str opacity02">'+
				'<div>Телефон</div>'+
				'<div><span class="plus-from-input-tel">+</span><input class="tel block_w_153p" id="user_phone" type="tel" maxlength="12" value="" placeholder="Телефон"></div>'+
			'</div>'+
			'<div class="str opacity02">'+
				'<div><span class="btn-active checkbox"></span> Подключить автоматическое пополнение</div>'+
			'</div>'+
		'</div>';
		
		HWF.modal.confirmCust({
			title:'Cумма к оплате',
			print:inpSum,
			yes:'Оплатить',
			classYes:'click_send_my_mone',
			clouseYes:'no'
		});
	});
	HWS.on('click','click_send_my_mone',function(e,el){
		let summ = HWS.getElem('#summ-pay-send');
		let sumval = parseInt(summ.value);
		let mail = HWS.getElem('.right-menu .user-dop-info .user-email-info').innerHTML;
		
		if(sumval > 0 && sumval && sumval != 0) {
			summ.classList.remove('red');
			
			HWF.post({url:"billing/get_pay_link",data:{token:token,amount:sumval},fcn:function(inf2){
				HWF.modal.printMini('Банк готов принять платеж!','<br><br> <a href="'+inf2.link+'" target="_blanck" class="style_btn orange">Оплатить</a>  <br><br>');
			}});
			HWF.modal.loader('Готовим платежную систему...');
		}
		else {summ.classList.add('red')}
	});
	// ACTIVITI
	/*HWS.on('change','change_select_main',function(e,el){
		let val = el.value;
		if(val == 0) {localStorage.setItem('PageMain','proj')}
		if(val == 1) {localStorage.setItem('PageMain','fast')}
	});*/
	// Ввод тел номеров 
	HWS.on('click','phone_inp_text',function(e,el){if(!el.value){el.value = '+'}});
	
	// Выделяет активный пукт дашбоардного меню, присваевает класс "актив" если 2й тег соответствует 2му тегу ссылки
	HWF.activeDasboartMenu();
});