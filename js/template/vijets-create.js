"use strict";
HWS.templateAdd(function(HW,B,H){
	let token = HWS.pageAccess;
	HWS.buffer.tileGraph={};
	HWS.buffer.createTileCheck = true;
	HWS.buffer.adapterBoiler = {}
	// Создание плиточек
	HWS.buffer.createTile = function(elem){
		let info = HWequip.normInfo(elem);
		let tile = HWhtml.tile();
		let inf = {}; 
		let ident = '';
		let port = '';
		let battery = '';
		let connect = elem.system_connection_status;
		let connectBlock = document.createElement('div');
			connectBlock.classList.add('shadov-block');
			tile.b.append(connectBlock);
			if (!connect) {connectBlock.classList.add('on');}

		if(info.name.length > 30){inf.nameN = info.name.substr(0, 25); inf.nameN = inf.nameN+'...';}
		else {inf.nameN = info.name}
		
		let blockShare =  document.createElement('div');
			blockShare.classList.add('block_share_sticer_vijet');
		let share_status = elem.lk.share_status || '';
		
		if (share_status &&  share_status  == 1){share_status = '';}
		if (share_status &&  share_status  == 3){share_status = '<span class="sticer-share-up_vijet btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconUp+'"></span>';}
		if (share_status &&  share_status  == 2){share_status = '<span class="sticer-share-down_vijet btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconDown+'"></span>';}
		blockShare.innerHTML = share_status;
		
		tile.b.append(blockShare);
		
		// общее для всех блоков
		function create_top(block,infoEl){
			block.id = 'one-tile-'+infoEl.tmp+'-'+infoEl.id;
			block.dataset.id = infoEl.id;
			block.dataset.tmp = infoEl.tmp;
			block.classList.add(infoEl.tmp);
			return block;
		}
		
		if(info.tmp == 'analog3x'){
			tile.b = create_top(tile.b,info);
			
			let label = elem.lk.state_name || '';
			inf.blambaColor = HWF.getStateColor(elem.lk.state)|| info.stateColor;
			let pred_max = elem.config.pm_max || 0; 
			let pred_min = elem.config.pm_min || 0; 
			let pred_razdel = ' ... '; 
			let unut = HWD.unit[elem.info.type.unit];
			
			if(info.type == 1889){
				if(pred_max > 0){pred_max = '+'+pred_max;}
				if(pred_min > 0){pred_min = '+'+pred_min;}
			}
			
			pred_max = pred_max+unut;
			pred_min = pred_min+unut;
			
			if(info.type == 321 || info.type == 320) {
				pred_razdel=''; pred_max = '';
			}
			
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#equipment/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<span class="blamba" style="background:'+inf.blambaColor+'"></span>\
					<span class="state-info">'+label+'</span>\
					<span class="predel-info">'+pred_min+pred_razdel+pred_max+'</span>\
				</div>\
				<div id="graph-vijet-'+info.id+'" class="graph">'+HWhtml.loader()+'</div>\
			';
			
			if (info.port == 10 || info.port == 12){
				ident = info.ident;
				port = info.portName+' : '+(info.portSymbol+info.addr) || ''; 
				if(info.channel_index){ident+= ' : '+info.channel_index;}
			} else {port = info.portSpec}
			if (info.battery){battery = '<span class="icon icon-battery-'+info.battery+'"></span>'}
			
			tile.bot.classList.add('bot-info');
			let printBot = '';
				printBot+= (ident)?'<p>'+ident+'</p>':'';
				printBot+= (port)?'<p>'+port+'</p>':'';
				printBot+= (info.title)?'<p>'+info.title+'</p>':'';
				printBot+= (battery)?'<p>'+battery+'</p>':'';
			tile.bot.innerHTML = printBot;
			HWS.buffer.tileGraph[info.id] = {type:info.tmp,system:elem.id_system,cloud_type:info.type};
		} 
		
		else if(info.tmp == 'discret3x'){
			tile.b = create_top(tile.b,info);
			
			inf.data = elem.lk.state_name || '';
			inf.stateB = HWF.getStateColor(elem.lk.state);
			
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#equipment/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<span class="blamba" style="background:'+inf.stateB+';"></span>\
					<span class="state-info">'+inf.data+'</span>\
				</div>\
				<div id="graph-vijet-'+info.id+'" class="graph">'+HWhtml.loader()+'</div>\
			';
			
			if (info.port == 10 || info.port == 12){
				port = info.portName+' : '+(info.portSymbol+info.addr) || ''; 
				ident = info.ident;
				if(info.channel_index){ident+= ' : '+info.channel_index;}
			} else {port = info.portSpec}
			if (info.battery){battery = '<span class="icon icon-battery-'+info.battery+'"></span>'}

			tile.bot.classList.add('bot-info');
			let printBot = '';
				printBot+= (ident)?'<p>'+ident+'</p>':'';
				printBot+= (port)?'<p>'+port+'</p>':'';
				printBot+= (info.title)?'<p>'+info.title+'</p>':'';
				printBot+= (battery)?'<p>'+battery+'</p>':'';
			tile.bot.innerHTML = printBot;
			HWS.buffer.tileGraph[info.id] = {type:info.tmp,system:elem.id_system,cloud_type:info.type};
		}
		
		else if(info.tmp == 'control_device3x'){
			tile.b = create_top(tile.b,info);
			
			inf.dopBtnClas = 'btn-active';
			inf.data = elem.lk.state_name || '';
			inf.timeRelayOn = '';
			
			if (info.val == 1){inf.stateB = 'active';}
			if (info.val == 0){inf.stateB = '';}
			if (info.val == 255){inf.stateB = ''; inf.dopBtnClas = 'used';}
			inf.used_by = '';
			if(elem.state.block_local_id){inf.used_by = HWD.globalText.used_by;}
			if(elem.lk.control_locked){inf.dopBtnClas = 'used';}
			if(elem.config.delay_selected){inf.timeRelayOn = elem.config.delay_lk || '0'; inf.timeRelayOn = inf.timeRelayOn+' сек'}
		
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#equipment/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state flex_center_left">\
					<span class="blamba-btn '+inf.dopBtnClas +' '+inf.stateB+'"></span>\
					<span class="state-info">'+inf.data+'</span>\
					<span class="dop-state-info">'+inf.timeRelayOn+'</span>\
				</div>\
				<div id="graph-vijet-'+info.id+'" class="graph">'+HWhtml.loader()+'</div>\
				<div class="used-program">'+inf.used_by+'</div>\
			';
			
			if (info.port == 10 || info.port == 12){
				port = info.portName+' : '+(info.portSymbol+info.addr) || ''; 
				ident = info.ident;
				if(info.channel_index){ident+= ' : '+info.channel_index;}
			}  else {port = info.portSpec}
			if (info.battery){battery = '<span class="icon icon-battery-'+info.battery+'"></span>'}
			
			tile.bot.classList.add('bot-info');
			let printBot = '';
				printBot+= (ident)?'<p>'+ident+'</p>':'';
				printBot+= (port)?'<p>'+port+'</p>':'';
				printBot+= (info.title)?'<p>'+info.title+'</p>':'';
				printBot+= (battery)?'<p>'+battery+'</p>':'';
			tile.bot.innerHTML = printBot;
			
			HWS.buffer.tileGraph[info.id] = {type:info.tmp,system:elem.id_system,cloud_type:info.type};
		}
		
		else if(info.tmp == 'foto_camera3x'){
			tile.b = create_top(tile.b,info);
			
			if (info.val == 0){inf.data = 'Ошибка'; inf.stateB = HWD.color.red}
			if (info.val == 1){inf.data = 'Готов'; inf.stateB = HWD.color.green}
			if (info.val == 2){inf.data = 'Съёмка'; inf.stateB = HWD.color.orange}
			
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#equipment/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<span class="blamba" style="background:'+inf.stateB+'"></span>\
					<span class="state-info">'+inf.data+'</span>\
				</div>\
				<div class="block_mt_8"><span class="btn_shot_camera btn-checkboxEC green">Сделать снимок</span></div>\
			';
			
			if (info.port == 10 || info.port == 12){
				port = info.portName+' : '+(info.portSymbol+info.addr) || ''; 
				ident = info.ident;
				if(info.channel_index){ident+= ' : '+info.channel_index;}
			}  else {port = info.portSpec}
			if (info.battery){battery = '<span class="icon icon-battery-'+info.battery+'"></span>'}
			
			tile.bot.classList.add('bot-info');
			let printBot = '';
				printBot+= (ident)?'<p>'+ident+'</p>':'';
				printBot+= (port)?'<p>'+port+'</p>':'';
				printBot+= (info.title)?'<p>'+info.title+'</p>':'';
				printBot+= (battery)?'<p>'+battery+'</p>':'';
			tile.bot.innerHTML = printBot;
		}
		
		// PROGRAMS
		else if(info.tmp == 'notofications'){ // Оповещение
			tile.b = create_top(tile.b,info);
			
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#program/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<div>В разработке</div>\
				</div>\
			';
			
			tile.bot.classList.add('bot-info');
			tile.bot.innerHTML = '\
				<p>'+elem.system_ident+'</p>\
				<p>'+elem.system_name+'</p>\
			';
		}
		
		else if(info.tmp == 'security'){ // Охрана
			tile.b = create_top(tile.b,info);
			
			let S = {}
			S.state = (elem.config.flags.security_active)?1:0;
			S.stateNTop = (S.state)?'Снять с охраны':'Поставить на охрану';
			S.iconColor = (S.state)?'green':'';
			S.stateA = (S.state)?'active':'';

			inf.data = elem.lk.state_name || '';
			
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#program/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<div class="state-info">'+inf.data+'</div>\
					<div class="block_ptb_15"> <span class="btn_security_vijet_click style_btn grey hover-green '+S.stateA+'">'+S.stateNTop+'</span> </div>\
					<div class="icon-block"> <span class="icon p52 black '+S.iconColor+' icon_equip_'+info.type+'"></span> </div>\
				</div>\
			';
			
			tile.bot.classList.add('bot-info');
			tile.bot.innerHTML = '\
				<p>'+elem.system_ident+'</p>\
				<p>'+elem.system_name+'</p>\
			';
		}
		
		else if(info.tmp == 'heating'){ // Отопление
			tile.b = create_top(tile.b,info);
			
			let S = {};
			let onof = (elem.config.boil_device)?HWequip.normInfo(elem.config.boil_device):'';
			let temp =(elem.config.sensor_temperature)?HWequip.normInfo(elem.config.sensor_temperature):'';
			S.iconClass = 'icon-programs-flame'; S.iconhelp = ''; S.iconP = 'p20';
			S.onof = elem.lk.state_name || '';
			S.iconCol = HWF.getStateColor(elem.lk.state);
			if(S.iconCol == '#D3D4D3') {S.iconCol = HWD.color.grey2;}

			if(temp && temp.stateN != 255) {S.tempprint = temp.stateName}
			else {S.tempprint = 'Нет t°'}
			
			if(onof && !temp || info.stateN == 112 || info.stateN == 11){
				S.iconClass = 'icon-alarm helper'; S.iconhelp = 'В программе отсутствует датчик температуры или связь с ним';
			}

			S.flameColor = (onof && onof.elem.state.flame)?HWD.color.blue:HWD.color.grey2;
			S.iconPrintFlame = '<span data-text="'+S.iconhelp+'" class="icon '+S.iconP+' '+S.iconClass+' icon_info" style="background:'+S.flameColor+';"></span>';
			S.iconDisp = 'display_none';
			
			if(onof && onof.tmp == "adapter_boiler3x"){
				S.flameColor = (onof.elem.state.flame)?HWD.color.blue:HWD.color.grey2;
				S.radiatorColor = (onof.elem.state.channel_1)?HWD.color.blue:HWD.color.grey2;
				S.faucetColor = (onof.elem.state.heat_water)?HWD.color.blue:HWD.color.grey2;
				
				S.iconPrintFlame = '<span data-text="'+S.iconhelp+'" class="icon icon-programs-flame icon_info" style="background:'+S.flameColor+'"></span>';
				S.iconDisp = '';
			}
			
			S.temp = {
				'eco':{tmp:elem.config.pau_therm_pre_eco,active:''},
				'standart':{tmp:elem.config.pau_therm_pre_std,active:''},
				'komfort':{tmp:elem.config.pau_therm_pre_comf,active:''},
				'custom':{tmp:elem.config.pau_therm_pre_usr,active:'grey'},
				'timetable':{tmp:elem.config.flags.control_from_schedule,active:''}
			}
			if(elem.config.flags.control_from_schedule){S.temp['timetable'].active = 'active';}
			else{
				if(elem.config.pau_therm_value == elem.config.pau_therm_pre_eco){S.temp['eco'].active = 'active';}
				else if (elem.config.pau_therm_value == elem.config.pau_therm_pre_std){S.temp['standart'].active = 'active';}
				else if (elem.config.pau_therm_value == elem.config.pau_therm_pre_comf){S.temp['komfort'].active = 'active';}
				else {S.temp['custom'].active = 'green';}
			}
			S.manualMod = (elem.config.flags.active)?'':'manual-mod';	

			
			S.valS = elem.config.pau_therm_value.toFixed(1); // поддерживается
			
			S.iconPrintRadiator = '<span class="'+S.iconDisp+' icon icon-radiator block_mrl_10" style="background:'+S.radiatorColor+'"></span>';
			S.iconPrintFaucet = '<span class="'+S.iconDisp+' icon icon-faucet" style="background:'+S.faucetColor+'"></span>';
			
			S.iconPrint = '<span class="block-icons">'+
				S.iconPrintFlame+
				S.iconPrintRadiator+
				S.iconPrintFaucet+
			'</span>';
			
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#program/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.classList.add('flex_between_top');
			tile.mid.innerHTML = '\
				<div class="state '+S.manualMod+' block_w_150p">\
					<div class="btn-heating">\
						<div class="block_pb_4"> <span class="btn_heating_click_tmp eco style_btn p5 Bgrey active-green w150p ts_11p '+S.temp['eco'].active+'" data-val="'+S.temp['eco'].tmp+'">Эконом: '+S.temp['eco'].tmp+'°</span> </div>\
						<div class="block_pb_4"> <span class="btn_heating_click_tmp standart style_btn p5 Bgrey active-green w150p ts_11p '+S.temp['standart'].active+'" data-val="'+S.temp['standart'].tmp+'">Стандарт: '+S.temp['standart'].tmp+'°</span> </div>\
						<div class="block_pb_4"> <span class="btn_heating_click_tmp komfort style_btn p5 Bgrey active-green w150p ts_11p '+S.temp['komfort'].active+'" data-val="'+S.temp['komfort'].tmp+'">Комфорт: '+S.temp['komfort'].tmp+'°</span> </div>\
						<div class="block_pb_4"> <span class="btn_heating_click_timetable style_btn p5 Bgrey active-green w150p ts_11p '+S.temp['timetable'].active+'">Расписание</span> </div>\
						<div class="ta_center ts_11p"> Свое значение </div>\
						<div class="ta_center">'+HWhtml.btn.numb(S.temp['custom'].tmp,{clas:'w55p '+S.temp['custom'].active,clasinp:'inp_num_val tf_mb btn_click_custom',fcn:'btnClickCustom',step:0.5,tofix:1})+'</div>\
					</div>\
					<div class="manual_mod_target ta_center block_mt_35"><span class="icon icon-alarm red p40 btn_helper" data-text="'+HWD.helpText.vijets.heatingManualMod+'"></span> <br> <span class="tf_fr500">Ручной режим</span></div>\
				</div>\
				<div class="ta_center block_w_40">\
					<div class="block_pb_15">\
						<p class="tf_mb ts_24p current-tmp">'+S.tempprint+'</p>\
						<p class="ts_08">Текущая t°</p>\
					</div>\
					<div class="display_inline ta_center block_pb_10">\
						'+S.iconPrint+'\
						<br>\
						<span class="ts_08 icon-text" style="color:'+S.iconCol+';">'+S.onof+'</span>\
					</div>\
					<div>\
						<p class="tf_mb ts_24p"><span class="tmp_correction">'+S.valS+'</span>°</p>\
						<p class="ts_08">Поддерживается</p>\
					</div>\
				</div>\
			';
			
			tile.bot.classList.add('bot-info');
			tile.bot.innerHTML = '\
				<p>'+elem.system_ident+'</p>\
				<p>'+elem.system_name+'</p>\
			';
		}
		
		else if(info.tmp == 'alarm'){ // Реакция на датчики
			tile.b = create_top(tile.b,info);
			
			let S = {}; 
			let device = (elem.config.automatic_device)?HWequip.normInfo(elem.config.automatic_device):'';
			let invers = (elem.config.flags.inverse)?1:0;
			S.idDivice = ''; S.btnClass =''; S.btnClassW ='';
			
			S.delay = elem.config.pau_alarm_delay;  // Время управления
			S.resrtAlarmClass = 'display_none';
			if(S.delay == 65535){S.resrtAlarmClass = ''}

			tile.b.dataset.invers = invers;
			
			if(device){
				S.idDivice = device.id || '';
				S.btnClass = 'btn_rebout_relay_prog_alarm';
				S.btnClassW = 'hover-green active';
				tile.b.dataset.iddivice = S.idDivice;
			} else {
				S.btnClassW = 'Bgrey nocursor';
			}
			S.manualMod = (elem.config.flags.active)?'':tile.mid.classList.add('manual-mod'); // block'
			S.data = elem.lk.state_name || '';
			S.state = HWF.getStateColor(elem.lk.state);

			S.rebout = '';
			
			
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#program/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<span class="blamba" style="background:'+S.state+'";></span>\
					<span class="state-info">'+S.data+'</span>\
				</div>\
				<div class="manual-icon">\
					<span class="'+S.btnClass+' style_btn '+S.btnClassW+' '+S.resrtAlarmClass+'">Cброс тревоги</span>\
					<div class="manual_mod_target ta_center"><span class="icon icon-alarm red p40 btn_helper" data-text="'+HWD.helpText.vijets.alarmManualMod+'"></span> <br> <span class="tf_fr500">Ручной режим</span></div>\
				</div>\
				<div></div>\
			';
			
			tile.bot.classList.add('bot-info');
			tile.bot.innerHTML = '\
				<p>'+elem.system_ident+'</p>\
				<p>'+elem.system_name+'</p>\
			';
		}
		
		else if(info.tmp == 'sheduler'){ // Расписание
			tile.b = create_top(tile.b,info);
			
			let S = {};
			let device = (elem.config.automatic_device)?HWequip.normInfo(elem.config.automatic_device):'';
			
			S.idGraph = info.id;
			if(device){
				S.contGraph = HWhtml.loader();
				HWS.buffer.tileGraph[S.idGraph] = {type:device.tmp,system:elem.id_system,id:device.id,cloud_type:info.type};
			} else {
				S.contGraph = '';
			}
			S.manualMod = (elem.config.flags.active)?'':tile.mid.classList.add('manual-mod'); // block'
			S.data = elem.lk.state_name || '';
			S.state = HWF.getStateColor(elem.lk.state);
			
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#program/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<span class="blamba" style="background:'+S.state+'";></span>\
					<span class="state-info">'+S.data+'</span>\
				</div>\
				<div id="graph-vijet-'+S.idGraph+'" class="graph">'+S.contGraph+'</div>\
				<div class="manual_mod_target ta_center"><span class="icon icon-alarm red p40 btn_helper" data-text="'+HWD.helpText.vijets.shedulerManualMod+'"></span> <br> <span class="tf_fr500">Ручной режим</span></div>\
				<div></div>\
			';
			
			tile.bot.classList.add('bot-info');
			tile.bot.innerHTML = '\
				<p>'+elem.system_ident+'</p>\
				<p>'+elem.system_name+'</p>\
			';
		}
		// Адаптер котла
		else if(info.tmp == 'adapter_boiler3x'){
			tile.b = create_top(tile.b,info);
			// статус по котлу
			inf.name = elem.lk.state_name || '';
			inf.bcolor = HWF.getStateColor(elem.lk.state);
			inf.used_by = '';
			if(elem.state.block_local_id){inf.used_by = HWD.globalText.used_by; inf.dopBtnClas = 'used';}
			
			inf.temp = info.elem.state.coolant_temperature || ''; inf.temp = (inf.temp == 32766 || inf.temp == 32767)?'':inf.temp;
			inf.bar = info.elem.state.pressure || ''; inf.bar = (inf.bar == 255)?'':inf.bar;
			inf.gvs = info.elem.state.hot_water_supply_temperature || ''; inf.gvs = (inf.gvs == 32766 || inf.gvs == 32767)?'':inf.gvs;
			inf.error = info.elem.error_description;

			if(elem.lk.state == 11){inf.specColorAdapter = 'tc_grey';}
			else {inf.specColorAdapter = '';} 
			
			inf.tempTeploHtml = (inf.temp != '')?'<span class="tw_700 tc_black tf_mb ts_16p '+inf.specColorAdapter+'"><span class="temp-carier">'+inf.temp+'</span>  °</span><br><span class="tc_grey6">Теплоноситель</span>':''; 
			inf.tempBarHtml = (inf.bar != '')?'<span class="tw_700 tc_black tf_mb ts_16p '+inf.specColorAdapter+'"><span class="bar-carier">'+inf.bar+'</span>   Бар</span><br><span class="tc_grey6">Давление</span>':'';
			inf.tempGvsHtml = (inf.gvs != '')?'<span class="tw_700 tc_black tf_mb ts_16p '+inf.specColorAdapter+'"><span class="gvs-carier">'+inf.gvs+'</span> °</span><br><span class="tc_grey6">ГВС </span>':'';
			
			if(inf.error.status){
				inf.errorBlock = '<span>Ошибка:</span>  <span class="tw_700 tc_red tf_mb ts_18p">'+inf.error.code+'</span> <span class="btn_text grey btn_modal_adapter_boiler">подробнее</span>';
				HWS.buffer.adapterBoiler[info.id] = {details:inf.error.details, common:inf.error.common}
			}
			else {inf.errorBlock = ''}
			
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#equipment/'+info.id+'" class="icon p16 icon-settings hover"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<span class="blamba" style="background:'+inf.bcolor+'"></span>\
					<span class="state-info">'+inf.name+'</span>\
				</div>\
				<div class="flex_around block_pt_15">\
					<div class="data_info_temp">'+inf.tempTeploHtml+'</div>\
					<div class="data_info_gvs">'+inf.tempGvsHtml+'</div>\
					<div class="data_info_bar">'+inf.tempBarHtml+'</div>\
				</div>\
				<div class="flex_center block_pt_30">\
					<div class="error-block">'+inf.errorBlock+'</div>\
				</div>\
				<div class="used-program">'+inf.used_by+'</div>\
			';
			
			if (info.port == 10 || info.port == 12){
				port = info.portName+' : '+(info.portSymbol+info.addr) || ''; 
				ident = info.ident;
				if(info.channel_index){ident+= ' : '+info.channel_index;}
			}  else {port = info.portSpec}
			if (info.battery){battery = '<span class="icon icon-battery-'+info.battery+'"></span>'}
			
						
			if (info.port == 10 || info.port == 12){
				port = info.portName+' : '+(info.portSymbol+info.addr) || ''; 
				ident = info.ident;
				if(info.channel_index){ident+= ' : '+info.channel_index;}
			}  else {port = info.portSpec}
			if (info.battery){battery = '<span class="icon icon-battery-'+info.battery+'"></span>'}

			tile.bot.classList.add('bot-info');
			let printBot = '';
				printBot+= (ident)?'<p>'+ident+'</p>':'';
				printBot+= (port)?'<p>'+port+'</p>':'';
				printBot+= (info.title)?'<p>'+info.title+'</p>':'';
				printBot+= (battery)?'<p>'+battery+'</p>':'';
			tile.bot.innerHTML = printBot;
		}
		// Система
		else if(info.tmp == 'system3x'){
			tile.b = create_top(tile.b,info);
			
			inf.textVal = 'Нет связи'; //На связи
			inf.blamColor = HWD.color.grey; // bc-green
			inf.ident = elem.info.ident;
			inf.versis = elem.info.info_vermain;
			inf.wifiTail = '';
			inf.gsmTail = '';
			inf.smstext = ''
			inf.cfgLk = elem.config.cfg_lk;
			let system_connection = elem.system_connection;
			let printIcon = '';

			HWF.recount(system_connection,function(eleC,key){
				let type = eleC.type, signal=eleC.level, clasStatus = eleC.use_inet, cfgLk=elem.config.cfg_lk;
				if(clasStatus == 'connect'){clasStatus = 'green';}
				if(clasStatus == 'disconnect'){clasStatus = 'red';}
				if(clasStatus == 'not_use'){clasStatus = 'of';}
				let circle = '<span class="spec-circle-icon-state '+clasStatus+'"></span>';
				let iconColor = '',textType = 'основной';
				let nameConect = (type == 'wifi')?'Wi-Fi':"GSM";
				let roam = (eleC.roam)?'-roam':'';
				//console.log(inf.cfgLk);
				if(key == 1 && (inf.cfgLk == 3 || inf.cfgLk == 4)){textType = 'резервный';}
				else { (key != 0)?textType='<span class="block_w_55p"></span>':''; }
				if(cfgLk == 1 && type == 'wifi'){circle=''; iconColor = 'grey'}
				if(cfgLk == 2 && type == 'gsm'){circle=''; iconColor = 'grey';}
				
				if(eleC.info_bal && type == 'gsm'){inf.smstext = eleC.info_bal || ''; console.log(eleC);}
				let print = '\
					<div class="'+type+' flex_center_left">\
						<span class="select-none position_relative block_mr_10">\
							'+circle+'\
							<span class="icon icon-'+type+roam+'-'+signal+' '+iconColor+'"></span>\
						</span>\
						<span class="block_pr_8">'+nameConect+'</span>\
						<span class="ts_11p tc_greyA5">'+textType+'</span>\
					</div>';
				
				printIcon+= print;
			});
			inf.textVal = info.elem.lk.state_name || '';
			inf.blamColor = HWF.getStateColor(info.elem.lk.state);

			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<span data-text="'+HWD.helpText.vijets.system3xReboutSms+'" class="icon p16 icon-update hover click_system_retyrn_balance btn_helper"></span>\
				<a href="#equipment/'+info.id+'" class="icon p16 icon-settings hover block_ml_10"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			if(inf.smstext){inf.smstextview = 'open'}
			else {inf.smstextview = '';}
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<span class="blamba" style="background:'+inf.blamColor+'";></span>\
					<span class="state-info">'+inf.textVal+'</span>\
				</div>\
				<div class="conection block_pt_10 block_pl_10 block_pr_10 flex_between">'+printIcon+'</div>\
				<div class="mesage"><div class="smsinfo open-clouse'+inf.smstextview+'">'+inf.smstext+'</div></div>\
			';

			tile.bot.classList.add('bot-info');
			tile.bot.innerHTML = '\
				<p>'+inf.ident+'</p>\
				<p>'+inf.versis+'</p>\
			';
		}
		// Пустая Система
		else if(info.tmp == 'systemEmpty'){
			tile.b = create_top(tile.b,info);
			
			inf.textVal = 'Нет связи'; //На связи
			inf.blamColor = HWD.color.grey; // bc-green
			inf.ident = elem.info.ident;
			inf.versis = elem.info.info_vermain;
			inf.wifiTail = '';
			inf.gsmTail = '';
			inf.smstext = ''
			let system_connection = elem.system_connection;
			let printIcon = '';

			inf.textVal = info.elem.lk.state_name || '';
			inf.blamColor = HWF.getStateColor(info.elem.lk.state);

			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<a href="#equipment/'+info.id+'" class="icon p16 icon-settings hover block_ml_10"></a>\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<div class="state">\
					<span class="blamba" style="background:'+inf.blamColor+'";></span>\
					<span class="state-info">'+inf.textVal+'</span>\
				</div>\
				<div class="conection block_pt_10 flex_between">'+printIcon+'</div>\
				<div class="mesage">'+inf.smstext+'</div>\
			';

			tile.bot.classList.add('bot-info');
			tile.bot.innerHTML = '\
				<p>'+inf.ident+'</p>\
				<p>'+inf.versis+'</p>\
			';
		} else {
			tile.b.id = 'one-tile-'+info.id;
			tile.b.dataset.id = info.id;
			tile.b.dataset.tmp = info.tmp;
			tile.top.innerHTML = '\
			<div class="left">\
				<span class="icon p32 black icon_equip_'+info.type+'"></span>\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<span class="icon p16 icon-delete hover_red btn_ib_clouse block_ml_10"></span>\
			</div>';
		}
		
		if(info.tmp == 'img'){
			inf.img = elem.img;
			tile.b.id = 'one-tile-'+inf.id;
			tile.b.classList.add(type);
			tile.top.innerHTML = '\
			<div class="left">\
				<img src="/img/test/icon-test-fm-11.png">\
				<span class="name">'+inf.nameN+'</span>\
			</div>\
			<div class="right">\
				<span class="text-icon x20">♆</span>\
				<span class="text-icon x20">☷</span>\
				<span class="text-icon x20">✎</span>\
			</div>';
			
			tile.mid.classList.add('mid-info');
			tile.mid.innerHTML = '\
				<img src="'+inf.img+'">\
			';
			
			tile.bot.classList.add('bot-info');
			tile.bot.innerHTML = '\
				<p><span>id: </span> <span>123 342</span></p>\
				<p><span>Порт: </span> <span>Е1</span></p>\
				<p><span>Тип: </span> <span>Температура</span></p>\
			';
		}
		
		return tile.b;
	}
	
	function create_analog_graph(id,data,min,max){
		min = min || false; max = max || false;
		if(!HWS.buffer.analog_vijet_graph){HWS.buffer.analog_vijet_graph = {}}; 
		
		if(HWS.buffer.analog_vijet_graph[id]){
			let chart = HWS.buffer.analog_vijet_graph[id];
				chart.zoomOutButton.disabled = true;
				chart.zoomOutButton.icon.disabled = true;
				chart.data = data;
				chart.xAxes.max = (data.length-1);
				if(min){chart.yAxes.min = min;}
				if(max){chart.yAxes.max = max;}
		}
		else {
			let chart = am4core.create(id, am4charts.XYChart);
				chart.data = data;
				chart.padding(0, 0, 0, 0);
				chart.chartContainer.wheelable = false;
				chart.seriesContainer.draggable = false;
				chart.seriesContainer.resizable = false;
				chart.maxZoomLevel = 1;
				chart.minZoomLevel = 1;
				chart.zoomOutButton.disabled = true;
				chart.zoomOutButton.icon.disabled = true;
				//console.log(data);
			let xAxes = chart.xAxes.push(new am4charts.ValueAxis());
				xAxes.tooltip.disabled = true;
				xAxes.renderer.labels.template.disabled = true;
				xAxes.renderer.grid.template.disabled = true;
				xAxes.renderer.ticks.template.disabled = true
				xAxes.renderer.line.disabled = true;
				xAxes.renderer.baseGrid.disabled = true;
				xAxes.max = (data.length-1);
				xAxes.min = 0;
				xAxes.strictMinMax = true;
				
			let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
				if(min){yAxis.min = min;}
				if(max){yAxis.max = max;}
				yAxis.tooltip.disabled = true;
				yAxis.renderer.labels.template.disabled = true;
				yAxis.renderer.grid.template.disabled = true;
				yAxis.renderer.ticks.template.disabled = true
				yAxis.renderer.line.disabled = true;
				yAxis.renderer.baseGrid.disabled = true;
				
			let series = chart.series.push(new am4charts.LineSeries());
				series.dataFields.valueX = "x";
				series.dataFields.valueY = "y";
				series.stroke = am4core.color("#FAB400");
				series.strokeWidth = 1;
			
			HWS.buffer.analog_vijet_graph[id] = chart;
		}
	};
	
	// Создание графиков на плиточках
	HWS.buffer.loaderGraph = function(tileGraph){
		if(!HWS.buffer.analog_vijet_graph){HWS.buffer.analog_vijet_graph = {}}; 
		let graphData = [];
		let gTime = HWS.buffer.system_time || Date.now();
		let sTime = gTime - (1*3600000);
		let eTime = gTime;
		
		HWF.recount(tileGraph,function(dataG,key){
			let typeV = (dataG.type == 'analog3x')?'value':'state';
			let id = dataG.id || key; id = parseInt(id);
			graphData.push({
				id_object:id,
				id_system:dataG.system,
				type:typeV,
			});
		});

		let setData = {token:token,data:graphData};
		HWF.post({url:"history/favorite_chart_data",data:setData,fcn:function(info){
			let listr = info.charts_data || [];
			HWF.recount(tileGraph,function(dataG,key){
				let type = dataG.type;
				let cloud_type = dataG.cloud_type;
				let id = dataG.id || key; id = parseInt(id);
				let graph = HWS.getElem('#graph-vijet-'+key);
				let servData = listr[id] || false;
				
				if(graph){
					if(type == 'analog3x'){
						let idCanva = 'analog3x-'+key;
						let max = servData.max_value || '';
						let min = servData.min_value || '';
						let dataVal = servData.values || [],dataGo = [];
						if (max && min && max == min) {max++; min--;}
						
						if(dataVal.length < 0){max = '', min = '';}
						else {HWF.recount(dataVal,function(val,key){ dataGo.push({x:key,y:val})});}
						if(cloud_type == 321 || cloud_type == 320){max = 100;}
						
						if(!HWS.buffer.analog_vijet_graph[idCanva]){
							graph.innerHTML = '\
							<div class="line-block block_pt_15"><span class="line"></span><span class="numb">'+max+'</span></div>\
							<div class="canva" id="'+idCanva+'"></div>\
							<div class="line-block"><span class="line"></span><span class="numb">'+min+'</span></div>';
						}
						create_analog_graph(idCanva,dataGo,min,max);
					}
					if(type == 'discret3x'){graph.innerHTML = HWhtml.vijetImgGraph(servData);}
					if(type == 'control_device3x'){graph.innerHTML = HWhtml.vijetImgGraph(servData);}
				}
			});
		}});
	}
});