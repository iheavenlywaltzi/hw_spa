"use strict";
HWspa.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1>Настройки набора доступа</h1>';
	let menu = '<div class="btn_back_page" title="Назад">&#8617;</div>';
	let id = H.hash[1];
	let content = document.createElement('div');
		content.classList.add('main-block'); content.classList.add('max-1200');
		content.classList.add('acces-page');
		content.innerHTML = HWhtml.loader();
		
	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);
	let data = {}
	
	if (id){HWF.post("access",{id:id},load_page);}
	else {load_page('new')}
	
	
	function load_page(info){
		let elem = info;
		let btnClouse = '<div class="icon icon-clouse btn_delete_group" title="Удалить Набор ?"></div>';
		let topB = HWhtml.tile('one-block');
		if (info == 'new'){
			B.title.append('<h1>Добавить набор доступа</h1>');
			HWF.loading('inactive');
			elem = {name:''};
			btnClouse = '';
		}
		
		let btn = document.createElement('div'); btn.classList.add('btn-page'); btn.classList.add('flex_between');
			btn.innerHTML = HWhtml.btn.back()+'<div class="icon icon-save"></div>'+btnClouse;
		
		topB.b.classList.add('ob-top');
		topB.b.appendChild(btn);
		topB.top.classList.add('ts_10');
		topB.top.innerHTML = '\
			<h5 class="block_w_100">Имя</h5>\
			<div class="name-page">\
				<input type="text" placeholder="Название набора" value="'+elem.name+'">\
			</div>\
		';
		topB.mid.innerHTML = '\
			<div class="block_w_100 flex_between">\
				<div><span class="style_btn btn_view_2 block_m_8 acces_add_proj">+ Проект</span>\
				<span class="style_btn btn_view_2 block_m_8 acces_add_equp">+ Оборудование</span>\
				<span class="style_btn btn_view_2 block_m_8 acces_add_prog">+ Программа</span></div>\
			</div>\
			<div class="flex_between block_w_100"><div></div><div class="style_btn btn_save_acces">Сохранить</div></div>\
		';
		
		let topTable = ''+
			'<div class="elem-type">Тип</div>'+
			'<div class="elem-name">Имя</div>'+
			'<div class="elem-chec">Доступ '+
				'<span class="helper btn_helper" title="Смотреть-Менять-Выполнять">?</span>'+
				'<p class="flex_around fast-btn-select">\
					<span class="btn_all_r user-none">r</span>\
					<span class="btn_all_w user-none">w</span>\
					<span class="btn_all_x user-none">x</span>\
				</p>'+
			'</div>'+
			'<div class="elem-butt"></div>';
		let standartBut = '\
		<div class="elem-butt">\
			<div class="text-icon btn_change_elem">✎</div>\
			<div class="text-icon btn_clouse_elem">✕</div>\
		</div>';
		
		let proj = HWhtml.tile('one-block');
			proj.top.innerHTML = '<h5>Проекты</h5><span class="text-icon dop-but-add acces_add_proj">+</span>';
			proj.data = [{html:topTable,class:'one-head'}];
			
		let equp = HWhtml.tile('one-block');
			equp.top.innerHTML = '<h5>Оборудование</h5><span class="text-icon dop-but-add acces_add_equp">+</span>';
			equp.data = [{html:topTable,class:'one-head'}];
			
		let prog = HWhtml.tile('one-block');
			prog.top.innerHTML = '<h5>Программы</h5><span class="text-icon dop-but-add acces_add_prog">+</span>';
			prog.data = [{html:topTable,class:'one-head'}];
			
		data = {proj:proj,equp:equp,prog:prog}
		
		for (let key in elem.list){
			let one = elem.list[key];
			let icon = '',html='';
			let r = one.r || 0, w = one.w || 0, x = one.x || 0;
			let active = {};
			active.r = (r == 1)?'active':''; active.w = (w == 1)?'active':'';active.x = (x == 1)?'active':'';
			let name = one.name;
			if(one.uid) {
				icon =  '<span class="icon icon_equip_'+HWF.getTypeUid(one.uid)+'"></span>';
				html = create_one_elem({icon:icon,name:name,active:active})
				equp.data.push({class:"one-elem",html:html,data:{id:one.id}});
			}
			else if(one.group && one.group == 'projects') {
				icon =  '<span class="icon icon_projects_'+one.type+'"></span>';
				html = create_one_elem({icon:icon,name:name,active:active})
				proj.data.push({class:"one-elem",html:html,data:{id:one.id}});
			}
			else if(one.group && one.group == 'programs') {
				icon =  '<span class="icon icon-programs"></span>';
				html = create_one_elem({icon:icon,name:name,active:active})
				prog.data.push({class:"one-elem",html:html,data:{id:one.id}});
			}
		}
		
		proj.mid.innerHTML = HWhtml.ul(proj.data);
		equp.mid.innerHTML = HWhtml.ul(equp.data);
		prog.mid.innerHTML = HWhtml.ul(prog.data);
		
		content.innerHTML = '';
		content.appendChild(topB.b);
		content.appendChild(proj.b);
		content.appendChild(equp.b);
		content.appendChild(prog.b);
	}
	
	function create_one_elem(data){
		data = data || {};
		data.active = data.active || {r:'active',w:'',x:''};
		let checked = '\
			<div title="Смотреть" class="acces_lvl_r btn-active checkbox-empty '+data.active.r+'">r</div>\
			<div title="Менять" class="acces_lvl_w btn-active checkbox-empty '+data.active.w+'">w</div>\
			<div title="Выполнять" class="acces_lvl_x btn-active checkbox-empty '+data.active.x+'">x</div>';
		
		let standartBut = '\
		<div class="elem-butt">\
			<div class="text-icon btn_change_elem">✎</div>\
			<div class="text-icon btn_clouse_elem">✕</div>\
		</div>';
		
		let html = ''+
		'<div class="elem-type">'+data.icon+'</div>'+
		'<div class="elem-name">'+data.name+'</div>'+
		'<div class="elem-chec flex_around">'+checked+'</div>'+
		'<div class="elem-butt">'+standartBut+'</div>';
		return html;
	}
	
	function add_new_elem_modal(elem,clas){
		let block = HW.parentSearchClass(elem,clas);//
		let id = block.dataset.id;
		let name = block.querySelector('.equ_n').innerHTML;
		let icon = block.querySelector('.equ_t').innerHTML;
		let newLi = document.createElement('li');
			newLi.dataset.id = id;
			newLi.classList.add('one-elem');
			newLi.innerHTML = create_one_elem({name:name,icon:icon});
		return newLi;
	}
	
	HW.on('click','acces_add_proj',HWmain.modal.obj);
	HW.on('click','acces_add_equp',HWmain.modal.equip);
	HW.on('click','acces_add_prog',HWmain.modal.prog);
	// change elem
	HW.on('click','btn_change_elem',function(e,elem){
		let id = HW.parentSearchClass(elem,'one-elem').dataset.id;
		HW.go('equipment/'+id);
	});
	HW.on('click','btn_clouse_elem',function(e,elem){
		let li = HW.parentSearchClass(elem,'one-elem');
		li.parentNode.removeChild(li);
	});
	// удаление Группы 
	HW.on('click','btn_delete_group',function(e,elem){
		let user = HW.parentSearchClass(elem,'one-block');
		let name = user.querySelector('.name-page input').value;
		HW.data.elemDelete = user;
		HWF.modal.confirm('Удалить <b>'+name+'</b> ? ');
	});
	HW.on('click','btn-yes-modal',function(e,elem){
		let main = document.querySelector('.acces-page');
			main.innerHTML = HWhtml.delete.load();
			
		setTimeout(function(){
			main.innerHTML = HWhtml.delete.ok();	
		},1000);
	});
	// Добавить Проект
	HW.on('click','btn-one-proj',function(e,elem){
		let newLi = add_new_elem_modal(elem,'one-project');
		data.proj.mid.querySelector('.list').appendChild(newLi);
		HWF.modal.off();
	});
	// Добавить Оборудование
	HW.on('click','btn-one-equip',function(e,elem){
		let newLi = add_new_elem_modal(elem,'one-equipment');
		data.equp.mid.querySelector('.list').appendChild(newLi);
		HWF.modal.off();
	});
	// Добавить Программу
	HW.on('click','btn-one-prog',function(e,elem){
		let newLi = add_new_elem_modal(elem,'one-program');
		data.prog.mid.querySelector('.list').appendChild(newLi);
		HWF.modal.off();
	});
	
	// Выбрать все r 
	HW.on('click','btn_all_r',function(e,elem){
		let list = HW.parentSearchClass(elem,'list');
		HWF.allActive(list,'.acces_lvl_r');
	});
	// Выбрать все w
	HW.on('click','btn_all_w',function(e,elem){
		let list = HW.parentSearchClass(elem,'list');
		HWF.allActive(list,'.acces_lvl_w');
	});
	// Выбрать все x 
	HW.on('click','btn_all_x',function(e,elem){
		let list = HW.parentSearchClass(elem,'list');
		HWF.allActive(list,'.acces_lvl_x');
	});
});