"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1>Настройки шаблона</h1>';
	let id = H.hash[1];
	let token = HWS.pageAccess;	
	let contentTop = document.createElement('div');
		contentTop.classList.add('main-block'); contentTop.classList.add('max-1200');
		contentTop.classList.add('no-P-bot');
		contentTop.id = 'temp-content-top';
	let content = document.createElement('div');
		content.classList.add('main-block'); content.classList.add('max-1200');
		content.classList.add('no-p-top');
		content.id = 'temp-content-mid';
		content.innerHTML = HWhtml.loader();
	
	B.title.append(title);
	B.content.append(contentTop);
	B.content.append(content);

	//запросы информации
	HWF.post({url:'patterns/get',data:{token:token,id:id},fcn:load_page});
	
	function load_page(info){ 		
		console.log(info);
		let type = info.pattern.type;
		HWS.buffer.elem = info.pattern;
		HW.load(HWD.page.temp+'/'+HWD.temp[type].tmp); // вызов соответствующего шаблона
		
		// функция изменения имени
		HWS.on('change','input-name-page',function(e,el){
			let newInfo = {}; newInfo.name = el.value;
			let dataSend = {token:HWS.pageAccess,id:info.pattern.id,updates:newInfo};
			HWF.post({url:'patterns/update',data:dataSend});
		});
	}
});