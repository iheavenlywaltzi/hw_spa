"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1 class="test_page">Настройки</h1>';
	let content = document.createElement('div');
		content.classList.add('main-block'); content.classList.add('max-1200');
		content.classList.add('settings-page');
	let token = HWS.pageAccess;

	B.title.append(title);
	B.content.append(content);
	
	load_page();

	function load_page(){
		let print = '';
		let mainP = localStorage.getItem('PageMain') || 'proj';
			mainP = (mainP == 'proj')?0:1;
		
		print = '<div class="one-block-col">\
			<div class="title"><h4>Глобальные <span title="" class="helper">?</span></h4></div>\
			<div class="ltft"></div>\
			<div class="right">\
				<div class="mcolum">\
					<p class="block_ptb_8">Главная</p>\
				</div>\
				<div class="colum">\
					<p>'+HWhtml.select(['Главный объект','Быстрый доступ'],'change_select_main',mainP)+'</p>\
				</div>\
			</div>\
		</div>';
		
		content.innerHTML = print;
	}
	
	// ACTIVITI
	HWS.on('change','change_select_main',function(e,el){
		let val = el.value;
		if(val == 0) {localStorage.setItem('PageMain','proj')}
		if(val == 1) {localStorage.setItem('PageMain','fast')}
	});
});