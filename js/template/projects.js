"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	HWS.buffer.dopFilter = {}//  Дополнительный фильтр.
	let GT={}
	let token = HWS.pageAccess;
	let viewTable = 'list'; // localStorage.getItem('viewProjects') || 
	let vieeControl = {}; vieeControl[viewTable] = 'active';
	let title = '<h1>Проекты</h1>';
	let hashGet = HWS.getHash().get;
	//let menu = '<a href="#project-add" title="Добавить новый проект">'+HWhtml.btn.add()+'</a>';
	
	let menu = '<div></div>';
		menu += '<div>';
		menu += '<a href="#project-add" class="btnIcon">\
				<span class="select-none">Добавить проект</span>\
				<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>\
			</a>';
		menu += '</div>';
	
	if(JSON.stringify(hashGet) != "{}"){hashGet = 'active';} else {hashGet = '';}
	let content = '<div class="main-block ob-center max-1200">\
		<div class="filter-equipments one-block-col">\
			<div class="flex_between block_w_100">\
				<div class="btn-open-filter btn-active pointer" data-onof="#filter-window" data-targetdown="#icon-filter">\
					<div class="display_inline select-none">\
						<span class="spec-circle-icon '+hashGet+'"></span>\
						<span id="icon-filter" class="icon icon-filter hover_orange p20"></span>\
					</div>\
					<span class="vertical-middle select-none ts_12p block_pl_5">Фильтр</span>\
				</div>\
				'+HWhtml.search('block_mr_8')+'\
				<span></span>\
			</div>\
			'+HWfilter.projects()+'\
		</div>\
		<div class="one-block nop all-projects"></div>\
	</div>';

	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);
	
	HWF.print.projects('.all-projects');
	
	// FUNCTION
	function dop_filter(){
		let arrGet = [];
		let objControl = HWS.buffer.dopFilter;

		HWF.recount(objControl,function(elf,keyf){
			if(elf){
				if(keyf == 'name'){keyf = '-name';}
				arrGet.push(keyf);
			}
		});
		
		HWS.data.apiGet.projects.data.sort = arrGet;
		HWF.print.projects('.all-projects');
		console.log(arrGet);
	}
	
	// ACTION
	// Удаление Проекта
	HW.on('click','btn-yes-modal',function(event,elem){
		let idProj = GT.id;
		HWF.post({url:'projects/delete_project',data:{token:token,id_project:idProj}});
		HW.data.elemDelete.parentNode.removeChild(HW.data.elemDelete);
	});
	HW.on('click','btn-proj-delete',function(event,elem){
		let block = HWS.parentSearchClass(elem,'one-project');
		let name = block.querySelector('.equ_n .text_name');
			name = name.innerHTML;
		GT.id = block.dataset.id;
		
		HW.data.elemDelete = block;
		HWF.modal.confirm('Удалить проект?','Вы действительно хотите удалить <b>'+name+'</b>? Это действие нельзя отменить.');
	});
	
	HW.on('click','btn-proj-change',function(event,elem){
		let id = HWS.parentSearchClass(elem,'one-project').dataset.id;
		HW.go(HWS.data.page.proj+'/'+id+'/edit');
	});
	
	HW.on('click','btn-one-proj',function(event,elem){
		let id = HWS.parentSearchClass(elem,'one-project').dataset.id;
		HWS.go(HWS.data.page.proj+'/'+id);
	});
	
	//ФИЛЬТР сортировать ПО имени. btn_dop_sort_system
	HWS.buffer.fcnSornName = function(){dop_filter();}
});
