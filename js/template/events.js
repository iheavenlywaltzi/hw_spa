"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1>События</h1>';
	let menu = '<a class="btn active" href="#events/list">Список</a> <a class="btn" href="#events/set">Настройки</a>';
	let content = '<div class="main-block ob-center max-1200 info-page">';
		content+= '<div id="all-info-content" class="one-block"></div>';
		content+= '</div>';
	let pageA = H.hash[1] || 'list';

	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);

	let contentB = document.body.querySelector('#all-info-content');
	
	//** lOGICS **
	if(pageA == 'list') {load_list()}
	if(pageA == 'set') {load_set()}
	//** -lOGICS- **
	
	//** FCN **
	//Загрузка новостей
	function load_list(){
		contentB.innerHTML = '';
		
		contentB.innerHTML = 'Список'
	}
	//Загрузка Помощьи
	function load_set(){
		contentB.innerHTML = '';
		
		contentB.innerHTML = 'Настройки'
	}
	//** -FCN- **
	
	//** ACTION **
	// Выделяет активный пукт дашбоардного меню, присваевает класс "актив" если 2й тег соответствует 2му тегу ссылки
	HWF.activeDasboartMenu();
	//** -ACTION- **
});