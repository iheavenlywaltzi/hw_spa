"use strict";
HWS.templateAdd('foreign_login',function(HW,B,H){ 
	HWF.clearBlocks();//очистим блоки контена

	let verification = false;
	let recovery = false
		// /#login?verification=c4c29f8d2a7146599e6abe28ded17cae
		if(H){verification = H.get.verification || false;} // регистрация прошла успешно
		// /#login?recovery=051001c9406940319b0c63edce3bbe8d
		if(H){recovery = H.get.recovery || false;}
	let title = '<h1>Авторизация</h1>';
	let container = document.createElement('div');
		container.classList.add('login-page-form');
	let btnPlay = document.createElement('div');
		btnPlay.classList.add('btn-play');
	let forma = document.createElement('form');
	let imgLogo = '<img src="img/logo_black.svg">';
	let containerForm = document.createElement('div');
		containerForm.classList.add('login-cont-form');
	let containerSpecTex = document.createElement('div');
		containerSpecTex.classList.add('login-dop-cpec-text');
	
	
	let HtmlLogin = function(mail){mail = mail || ''; let s = '\
		<p class="logo">'+imgLogo+'</p>\
		<p class="title">Вход в личный кабинет</p>\
		<p id="error-login"></p>\
		<input type="hidden" id="specHidden" value="login">\
		<p><input name="email" type="email" id="user_mail" value="'+mail+'" placeholder="Электронная почта" required></p>\
		<p><span class="spec-password">\
			<input name="password" type="password" id="user_pass" placeholder="Пароль" required>\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>\
		<p class="dop-btn">\
			<span class="btn_text grey btn_register_user">Регистрация</span>\
			<span class="btn_text grey btn_bad_memory">Забыли пароль?</span>\
		</p>\
		<p class="block_pt_10"><button class="btn-form-click">Войти</button></p>\
		<p class="demo"></p>\
		<p class="store">\
			<a href="https://apps.apple.com/mg/app/ectocontrol/id1520421408" target="_blanck"><img src="img/a_store.svg"></a>\
			<a href="https://play.google.com/store/apps/details?id=ru.ectocontrol.app&hl=ru&gl=US" target="_blanck"><img src="img/g_play.svg"></a>\
		</p>\
		<!--p class="info-text">\
			При входе вы подтверждаете свое согласие с <span class="btn_text grey">условиями использования</span> <br>\
			ectoControl и <span class="btn_text grey">политикой о данных пользователей.</span> \
		</p-->\
		<p class="block_p_10"></p>\
	';
		return s;
	}
	let HtmlRegistr = function(mail){mail = mail || ''; let s ='\
		<p class="logo">'+imgLogo+'</p>\
		<p class="title">Регистрация в личном кабинете</p>\
		<p id="error-login"></p>\
		<input type="hidden" id="specHidden" value="registr">\
		<p><input name="email" type="email" id="user_mail" value="'+mail+'" placeholder="Электронная почта" required></p>\
		<p><span class="spec-password">\
			<input name="password" type="password" id="user_pass" placeholder="Пароль" required>\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>\
		<p><span class="spec-password">\
			<input name="password2" type="password" id="user_pass2" placeholder="Пароль еще раз" required>\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>\
		<p></p>\
		<p class="dop-btn">\
			<span class="btn_text grey btn_login_user">Вход</span>\
			<span><input name="rules" type="checkbox" id="rules" required>согласен с <span class="btn_text grey btn_info_user">правилами</span>.</span>\
		</p>\
		<p class="block_pt_10"><button class="btn-form-click">Регистрация</button></p>\
		<p class="demo"></p>\
		<p class="store">\
			<a href="https://apps.apple.com/mg/app/ectocontrol/id1520421408" target="_blanck"><img src="img/a_store.svg"></a>\
			<a href="https://play.google.com/store/apps/details?id=ru.ectocontrol.app&hl=ru&gl=US" target="_blanck"><img src="img/g_play.svg"></a>\
		</p>\
		<!--p class="info-text">\
			При входе вы подтверждаете свое согласие с <span class="btn_text grey">условиями использования</span> <br>\
			ectoControl и <span class="btn_text grey">политикой о данных пользователей.</span> \
		</p>-->\
		<p class="block_p_10"></p>\
		';
		return s;
	}
	let HtmlRecovery = function(mail){mail = mail || ''; let s ='\
		<p class="logo">'+imgLogo+'</p>\
		<p class="title">Восстанавление пароля</p>\
		<p id="error-login"></p>\
		<input type="hidden" id="specHidden" value="recovery">\
		<p><input name="email" type="email" id="user_mail"  value="'+mail+'" placeholder="Электронная почта" required></p>\
		<p class="tc_grey block_pl_10">*Мы вышлем вам инструкции на Email</p>\
		<p class="dop-btn">\
			<span class="btn_text grey btn_login_user">Вход</span>\
			<span class="btn_text grey btn_register_user">Регистрация</span>\
		</p>\
		<p class="block_pt_10"><button class="btn-form-click">Выслать</button></p>\
		<p class="demo"></p>\
		<p class="store">\
			<a href="https://apps.apple.com/mg/app/ectocontrol/id1520421408" target="_blanck"><img src="img/a_store.svg"></a>\
			<a href="https://play.google.com/store/apps/details?id=ru.ectocontrol.app&hl=ru&gl=US" target="_blanck"><img src="img/g_play.svg"></a>\
		</p>\
		<p class="block_p_10"></p>\
		';
		return s;
	}
	let HtmlRecoveryNewPass = function(){let s =''+
		'<p class="logo">'+imgLogo+'</p>'+
		'<p class="title no-p-bot">Введите новый пароль</p>'+
		'<p id="error-login"></p>'+
		'<input type="hidden" id="specHidden" value="newpass">'+
		'<p><span class="spec-password">\
			<input name="password" type="password" id="user_pass" placeholder="Пароль" required>\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>'+
		'<p class="title no-p-bot">Повторите пароль</p>'+
		'<p><span class="spec-password">\
			<input name="passwordto" type="password" id="user_pass2" placeholder="Пароль" required>\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>'+
		'<p class="block_pt_10"><button class="btn-form-click">Готово!</button></p>'+
		'<p class="block_pt_15">\
			<a href="/" class="btn_text grey">Отмена</a>\
		</p>'+
		'<br> <br> <br>';
		return s;
	}
	
	btnPlay.innerHTML = '\
		<p><span><img src="img/img-play-login.png"><span>Как пользоваться<br>личным кабинетом?</span></span></p>\
	';

	forma.innerHTML = HtmlLogin();
	
	if (verification){
		forma.innerHTML = ''+
			'<p class="logo"><img src="img/logo_black.png"></p>'+
			'<p class="title">Авторизация</p>'+
			'<p>Авторизация...</p>'+HWhtml.loader()+
			'<br> <br> <br> <br>'+
		'';
		
		let data = {code:verification};
		
		HWF.post({url:'registration/check',data:data,fcn:verification_ok,fcnE:modal_mes});
		
		function verification_ok(info){
			if(info.success){ console.log(info);
				let connect = info.success;
				let token = info.token;

				localStorage.setItem('PageAccess',token)
				localStorage.setItem('UserEmail','');
				forma.innerHTML = ''+
					'<p class="logo"><img src="img/logo_black.png"></p>'+
					'<p class="title">Регистрация прошла успешно!</p>'+
					'<p><a href="/">На главную</a></p>'+
					'<br> <br> <br> <br>'+
				'';
			}
		}
	}	
	
	if (recovery){
		forma.innerHTML = ''+
			'<p class="logo"><img src="img/logo_black.png"></p>'+
			'<p class="title">Восстанавление</p>'+
			'<p>Восстанавливаем...</p>'+HWhtml.loader()+
			'<br> <br> <br> <br>'+
		'';
		
		let data = {restore_code:recovery};
		HWF.post({url:'user/restore-password',data:data,fcn:recovery_ok,fcnE:modal_mes});
		
		function recovery_ok(info){
			if(info.success){
				forma.innerHTML = ''+
					'<p class="logo"><img src="img/logo_black.png"></p>'+
					'<p class="title">Восстанавление прошло успешно!</p>'+
					'<p><a href="/">Войти</a></p>'+
					'<br> <br> <br> <br>'+
				'';
			}
		}
	}
		
	// функции для этой страници
	forma.onsubmit = function(){
		let spec = document.getElementById('specHidden').value;
		let email = document.getElementById('user_mail').value; HWS.buffer.mail = email;
		let name = document.getElementById('user_name'); name = (name)?name.value:false;
		let pass = document.getElementById('user_pass'); pass = (pass)?pass.value:false;
		let pass2 = document.getElementById('user_pass2'); pass2 = (pass2)?pass2.value:false;
		let rules = document.getElementById('rules'); rules = (rules)?rules.value:false;
		
		if (spec == 'login'){
			console.log(1111);
			forma.innerHTML = ''+
				'<p class="logo"><img src="img/logo_black.png"></p>'+
				'<p class="title">Вход в личный кабинет</p>'+
				'<p>Вход...</p>'+HWhtml.loader()+
				'<br> <br> <br> <br>'+
			'';
			localStorage.setItem('UserEmail',email);
			
			let specStr = HWS.getHashStr().substr(15);
			console.log(specStr);
			
			let data = {
				email:email,
				password:pass,
				query:specStr
			}

			HWF.post({url:HWD.api.foreign_login,type:'noapi',data:data,fcn:function(mes){
				if (mes.success) {
					if (mes.redirect_uri){
						console.log(mes);
						document.location.href = mes.redirect_uri;
					}
				} else {error_login(mes); console.log(mes);}
			},fcnE:error_login});
		}
		
		if (spec == 'registr'){
			forma.innerHTML = ''+
				'<p class="logo"><img src="img/logo_black.png"></p>'+
				'<p class="title">Регистрация в личный кабинет</p>'+
				'<p>Регистрация...</p>'+HWhtml.loader()+
				'<br> <br> <br> <br>'+
			'';
			if (rules == 'on') {rules = true;}
			let data = {
				email:email,
				password:pass,
				password_confirmation:pass2,
				rules:rules
			}
			HWF.post({url:'registration/registrate',data:data,fcn:ok_registr,fcnE:error_registr});
		}
		
		if (spec == 'recovery'){
			forma.innerHTML = ''+
				'<p class="logo"><img src="img/logo_black.png"></p>'+
				'<p class="title">Восстанавление</p>'+
				'<p>Восстанавливаем...</p>'+HWhtml.loader()+
				'<br> <br> <br> <br>'+
			'';
			
			let data = {email:email}
			HWF.post({url:'user/send-code-for-restore-password',data:data,fcn:recovery_ok,fcnE:error_recovery});
		}
		return false;
	};
	// успешный вход
	function load_project(info){
		let connect = info.success;
		let token = info.token;
		let email = localStorage.getItem('UserEmail') || 'none';
		let uNumber = localStorage.getItem('UserNumber') || false;
		if (connect){
			HWS.data.token = token;
			HWS.pageAccess = token;
			
			if(info.admin==true){
				//HWS.blocks.menul.append(HWhtml.lMenu('admin')); 
				//HWS.blocks.menu.append(HWhtml.tRMenu('admin'));
				localStorage.setItem('AdminEmail',email);
				localStorage.setItem('PITn0Hxg',token);
				document.body.classList.add('admin');
				HWS.go('god');
			}
			else {
				localStorage.setItem('PageAccess',token);
				localStorage.setItem('PITn0Hxg','');
				localStorage.setItem('UserEmail',email);
				document.body.classList.remove('admin');
				HWS.go('main');
				HWF.setInfoTopMenu();
			}
		}
	}
	// регистрация успешна
	function ok_registr(info){
		forma.innerHTML = ''+
			'<p class="logo"><img src="img/logo_black.png"></p>'+
			'<p class="title">Регистрация</p>'+
			'<p>Мы <b>выслали вам письмо</b>,</p>'+
			'<p>Следуйте инструкциям в письме</p>'+
			'<br> <br> <br> <br>'+
		'';
	}
	
	// Востановления удачно
	function recovery_ok(info){
		HWS.data.token = false;
		HWS.pageAccess = false;
		localStorage.setItem('PageAccess','');
		
		forma.innerHTML = ''+
			'<p class="logo"><img src="img/logo_black.png"></p>'+
			'<p class="title">Регистрация</p>'+
			'<p>Мы <b>выслали вам письмо</b>,</p>'+
			'<p>Следуйте инструкциям в письме</p>'+
			'<p><a href="/#login">ок</a></p>'+
			'<br> <br> <br> <br>'+
		'';
	}

	// печатает сообщение от сервера
	function modal_mes(info,key){
		let mes = HWF.getServMesErr(info) || 'Что-то пошло нетак, попробуйте еще раз';
		HWF.modal.message(mes,'Ошибка '+key);
		forma.innerHTML = HtmlLogin();
	}
	
	//Ошибки во время логоизации
	function error_login(info){
		forma.innerHTML = HtmlLogin(HWS.buffer.mail);
		let mes = document.body.querySelector('#error-login');
		let eror = HWF.getServMesErr(info) || 'Что-то пошло нетак, попробуйте еще раз';
		mes.innerHTML = eror;
	}	
	//Ошибки во время регистрации
	function error_registr(info){
		forma.innerHTML = HtmlRegistr(HWS.buffer.mail);
		let mes = document.body.querySelector('#error-login');
		let eror = HWF.getServMesErr(info) || 'Что-то пошло нетак, попробуйте еще раз';
		mes.innerHTML = eror;
	}	
	//Ошибки во время регистрации
	function error_recovery(info){
		forma.innerHTML = HtmlRecovery(HWS.buffer.mail);
		let mes = document.body.querySelector('#error-login');
		let eror = HWF.getServMesErr(info) || 'Что-то пошло нетак, попробуйте еще раз';
		mes.innerHTML = eror;
	}
	
		
	containerForm.appendChild(forma);
	//containerForm.appendChild(btnPlay);
	container.appendChild(containerForm);
	container.appendChild(containerSpecTex);
	
	B.title.append(title);
	B.content.append(container);
	 
	HW.on('click','btn_register_user',function(){B.title.append('<h1>Регистрация</h1>'); forma.innerHTML = HtmlRegistr()});
	HW.on('click','btn_login_user',function(){B.title.append('<h1>Авторизация</h1>'); forma.innerHTML = HtmlLogin()});
	HW.on('click','btn_bad_memory',function(){B.title.append('<h1>Восстановление</h1>'); forma.innerHTML = HtmlRecovery()});
	HW.on('click','btn_info_user',function(){console.log(111); HWF.modal.print('Правила',HWD.regRegulations);});
	HW.on('click','btn_demo_user',function(){
		forma.innerHTML = '<p>Вход...</p>'+HWhtml.loader();
		localStorage.setItem('UserEmail','demolk@ectostroy.ru');
		HWF.post({url:'user/demo_login',fcn:function(info){
			HWS.pageAccess = info.token;
			localStorage.setItem('PageAccess',info.token);
			localStorage.setItem('PITn0Hxg','');
			localStorage.setItem('UserEmail','demolk@ectostroy.ru');
			document.body.classList.remove('admin');
			HWS.go('main');
			HWF.setInfoTopMenu();
		}});
	});
});