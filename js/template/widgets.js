"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1>Информация</h1>';
	let menu = '<div class="widget_add" title="Добавить виджет">+</div>';
	let content = '<div class="main-block ob-center max-1200 info-page"></div>';

	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);
	
	//запросы информации
	//HWF.post("info",load_page);
	load_page(HWD.demoInfo);
	//let tile  = HWhtml.tile();
		//tile.mid.innerHTML = 'Страници в разработке :)';
	//document.querySelector('.main-block').appendChild(tile.b) ;
	
	function load_page(info){
		let allWidget = info;
		let fragment = document.createDocumentFragment();
		let mainBlock = document.querySelector('.main-block');
		let numW = 1;
		for (let key in allWidget){
			let widget = allWidget[key];
			let block = false;
			
			if (widget.type == 'signal'){block = HWwidget.signal(widget);}
			if (widget.type == 'graph'){block = HWwidget.graph(widget);}
			if (widget.type == 'history'){block = HWwidget.history(widget);}
			
			if(block){fragment.appendChild(block);}
			numW++;
		}
		mainBlock.appendChild(fragment);
		
		HW.on('click','btn_full_screen',function(event,elem){
			let widget = HW.parentSearchClass(elem,'one-block');
			if(widget.classList.contains('widget-full-screen'))
			  {widget.classList.remove('widget-full-screen');HWF.fullScreen();}
			else {widget.classList.add('widget-full-screen');HWF.fullScreen(widget);}
		});
		
		// delete widget
		HW.on('click','btn_clouse_widget',function(event,elem){
			let widget = HW.parentSearchClass(elem,'one-block');
			if(widget.classList.contains('widget-full-screen'))
			  {widget.classList.remove('widget-full-screen');HWF.fullScreen();}
			let name = widget.querySelector('h3').innerHTML;
			HW.data.elemDelete = widget;
			HWF.modal.confirm('Удалить: <b>'+name+'</b> ?');
		});
		HW.on('click','btn-yes-modal',function(event,elem){
			HW.data.elemDelete.parentNode.removeChild(HW.data.elemDelete);
		});
		
		// add widget widget_add
		HW.on('click','widget_add',function(event,elem){
			let content = document.createElement('div'); content.classList.add('add-widget-modal');
			let signal = HWhtml.tile('one-tile');
			let graph = HWhtml.tile('one-tile');
			let history = HWhtml.tile('one-tile');
			
			signal.top.innerHTML = 'Сигналы';
			signal.mid.innerHTML = '<img src="img/signal-preview.jpg" />';
			
			graph.top.innerHTML = 'График';
			graph.mid.innerHTML = '<img src="img/graph-preview.jpg" />';
			
			history.top.innerHTML = 'История';
			history.mid.innerHTML = '<img src="img/history-preview.jpg" />';
			
			content.appendChild(signal.b);
			content.appendChild(graph.b);
			content.appendChild(history.b);
			
			HWF.modal.on();
			HWS.blocks.modal.data.title.append('Добавить Виджет');
			HWS.blocks.modal.data.body.clear();
			HWS.blocks.modal.data.body.append(content);
		});
	}
});

var HWwidget = new function (){
	function standartBtn(num){
		num = num || 1;
		let print = '\
		<div class="widget-top-right flex_between">\
			<div>\
				<span>↑</span>\
				<input type="text" value="'+num+'" placeholder="">\
				<span>↓</span>\
			</div>\
			<div class="icon icon-full-screen btn_full_screen" title="Раскрыть на весь экран"></div>\
			<div class="text-icon btn_clouse_widget" title="Удалить данный виджет ?">✕</div>\
		</div>';
		
		return print;
	}
	
	function create_widget(elem){
		let bb = HWhtml.tile('one-block');
			bb.b.classList.add('no-flex');
			bb.b.classList.add('block_w_100');
			bb.b.classList.add('widget-'+elem.type);
			bb.b.dataset.type = elem.type;
			bb.b.dataset.id = elem.id;		
		return bb;
	}
	// signal
	this.signal = function(elem){
		let W = create_widget(elem);
		let view = localStorage.getItem('signal_'+elem.id) || 3;
		let selected = {}; selected[view] = 'selected';
		W.b.classList.add('signal-view-'+view);
		W.top.classList.add('flex_between');
		W.top.innerHTML = '\
		<div class="widget-top-left">\
			<h3>'+elem.name+'</h3>\
			<div class="widget-dop-btn flex_center_left">\
				<span class="text-icon dop-but-add">+</span>\
				<span class="ts_08 tc_grey"> По: </span>\
				<select class="widget_signal_sum" title="Отображение кол-ва элементов" size="1" name="sumelem[]">\
					<option value="1" '+selected[1]+'>1</option>\
					<option value="2" '+selected[2]+'>2</option>\
					<option value="3" '+selected[3]+'>3</option>\
					<option value="4" '+selected[4]+'>4</option>\
					<option value="5" '+selected[5]+'>5</option>\
					<option value="6" '+selected[6]+'>6</option>\
				</select>\
				<span class="ts_08 tc_grey"> За: </span>\
				<select title="За какой периуд времени" size="1" name="sumelem[]">\
					<option value="15">15м</option>\
					<option value="30">30м</option>\
					<option value="1" selected="">1ч</option>\
					<option value="2">2ч</option>\
					<option value="4">4ч</option>\
				</select>\
			</div>\
		</div>'
		W.top.innerHTML += standartBtn(2);
		
		if (elem.data){
			W.mid.classList.add('flex_between');
			let fragmentW = document.createDocumentFragment();
			let cl = '000000', cb = '888888' , act = 'noen';
			let leftMenu = '<div class="SWT-btn">\
				<div class="icon icon-full-width btn_signal_full_width" title="Раскрыть на всю шерену"></div>\
				<div class="text-icon btn_signal_clouse_tile" title="Удалить данный объект ?">✕</div>\
			</div>';
			for (let key in elem.data){
				let graph = elem.data[key];
				let tile = document.createElement('div'); tile.classList.add('signal-widget-tile');
				let topT = document.createElement('header'); tile.appendChild(topT);
				let midT = document.createElement('section'); tile.appendChild(midT);
				let canva = document.createElement('canvas'); midT.appendChild(canva);
				let type = graph.uid;
				if(graph.state){
					tile.classList.add('signal-tile-state-'+graph.state);
					if (graph.state == 'n')  {cl = '#1ab148'; cb = '#bfe1c9'; act='норма'; }
					if (graph.state == 'w')  {cl = '#f89148'; cb = '#ffc6a0'; act='внимание';}
					if (graph.state == 'a')  {cl = '#ffffff'; cb = '#f892a7'; act='ТРЕВОГА!';}
					if (graph.state == 'off'){cl = '#a0a0a0'; cb = '#dcdcdc'; act='';}
					if (graph.state == 'on') {cl = '#6798c1'; cb = '#c7d8e8'; act='active';}
				}
				
				if (type == 'RP'){HWG.create.discrete(canva,{data:graph.graph,cl:cl,cb:cb});} //реле
				if (type == 'DS'){HWG.create.discrete(canva,{data:graph.graph,cl:cl,cb:cb});} //Датчик дыма
				if (type == 'DW'){HWG.create.discrete(canva,{data:graph.graph,cl:cl,cb:cb});} //Датчик протечки
				if (type == 'DT'){HWG.create.analog(canva,{data:graph.graph,cl:cl,cb:cb});} //Датчик температуры
				if (type == 'DV'){HWG.create.analog(canva,{data:graph.graph,cl:cl,cb:cb});} //Датчик влажности
				
				topT.classList.add('flex_between');
				topT.innerHTML = '<div class="SWT-name">'+graph.name+'</div>';
				topT.innerHTML += leftMenu;
				
				fragmentW.appendChild(tile);
			}
			W.mid.appendChild(fragmentW);
		}
		
		// signal sum widget_signal_sum
		HWS.on('change','widget_signal_sum',function(event,elem){
			let block = HWS.parentSearchClass(elem,'one-block');
			let view = localStorage.getItem('signal_'+block.dataset.id) || 3;
			block.classList.remove('signal-view-'+view);
			block.classList.add('signal-view-'+elem.value);
			localStorage.setItem('signal_'+block.dataset.id,elem.value);
		});	
		
		// full width
		HWS.on('click','btn_signal_full_width',function(event,elem){
			let block = HWS.parentSearchClass(elem,'signal-widget-tile');
			if(block.classList.contains('signal-widget-tile-full'))
				{block.classList.remove('signal-widget-tile-full');}
			else{block.classList.add('signal-widget-tile-full');}
		});
		
		// clouse tile
		HWS.on('click','btn_signal_clouse_tile',function(event,elem){
			let block = HWS.parentSearchClass(elem,'signal-widget-tile');
			block.parentNode.removeChild(block);
		});
			
		return W.b;
	}
	// graph
	this.graph = function(elem){
		let W = create_widget(elem);
		let midL = document.createElement('ul'); W.mid.appendChild(midL);
		let midR = document.createElement('div'); W.mid.appendChild(midR);
			
		W.mid.classList.add('flex_between');
		midL.classList.add('widget-graph-left');
		midL.classList.add('list');
		midR.classList.add('widget-graph-right');
		
		W.top.classList.add('flex_between');
		W.top.innerHTML = '\
		<div class="widget-top-left">\
			<h3>'+elem.name+'</h3>\
			<div class="widget-dop-btn flex_center_left">\
				<span class="text-icon dop-but-add">+</span>\
				<span class="ts_08 tc_grey"> За: </span>\
				<select title="За какой периуд времени" size="1" name="sumelem[]">\
					<option value="15">15м</option>\
					<option value="30">30м</option>\
					<option value="1" selected="">1ч</option>\
					<option value="2">2ч</option>\
					<option value="4">4ч</option>\
				</select>\
			</div>\
		</div>'
		W.top.innerHTML += standartBtn(3);
		
		if (elem.data){
			function getRandomColor() {
			  var letters = '0123456789ABCDEF';
			  var color = '#';
			  for (var i = 0; i < 6; i++) {
				color += letters[Math.floor(Math.random() * 16)];
			  }
			  return color;
			}
			let prLeft = '';
			let canva = document.createElement('canvas'); midR.appendChild(canva);
			let Gdata = [];
			for (let key in elem.data){
				let one = elem.data[key];
				let oneD = one.graph;
				let color = getRandomColor();
				let viewG = false;
				let type = one.uid;
				
				if (type == 'RP'){viewG = true;} //реле
				if (type == 'DS'){viewG = true;} //Датчик дыма
				if (type == 'DW'){viewG = true;} //Датчик протечки
				
				prLeft += '<li>\
					<p>'+one.name+'<p>\
					<div>\
						<span class="btn_color-graph" style="background:'+color+'"></span>\
						<span class="text-icon">✕</span>\
					</div>\
				</li>';
				Gdata.push({
					label:one.name,
					borderColor:color,
					backgroundColor:color,
					steppedLine:viewG,
					fill: false,
					data:oneD,
				});
			}
			HWG.widget.graph(canva,Gdata);
			
			midL.innerHTML = prLeft;
			midR.appendChild(canva);
		}

		return W.b;
	}
	// history
	this.history = function(elem){
		let W = create_widget(elem);
		let print = ''
		
		W.top.classList.add('flex_between');
		W.mid.classList.add('flex_between');
		W.top.innerHTML = '<div class="widget-top-left"><h3>'+elem.name+'</h3></div>';
		W.top.innerHTML += standartBtn(1);

		print += '<div class="widget-history-time">';
		print += '<p>Выбрать период</p>';
		print += '<input type="text" title="Начало период" onClick="xCal(this)" onKeyUp="xCal()" placeholder="от" />';
		print += '<input type="text" title="Конец периода"onClick="xCal(this)" onKeyUp="xCal()" placeholder="до"/>';
		print += '<p><input type="text" title="Поиск данных" placeholder="Поиск по журналу"/></p>';
		print += '</div>';
		
		print += '<div class="widget-history-checbox">';
		print += '<label><div class="checkbox"></div> <span>Входящие SMS</span></label>';
		print += '<label><div class="checkbox"></div> <span>Исходящие SMS</span></label>';
		print += '<label><div class="checkbox"></div> <span>Входящие звонки</span></label>';
		print += '<label><div class="checkbox"></div> <span>Исходящие звонки</span></label>';
		print += '<label><div class="checkbox"></div> <span>Системные события</span></label>';
		print += '<label><div class="checkbox"></div> <span>Охрана</span></label>';
		print += '</div>';
		
		print += '<div class="widget-history-selects">';
		print += '<p><select title="Выбрать оборудование" size="1" name="sumelem[]">\
					<option value="15">Всё</option>\
					<option value="15">Все Датчики</option>\
					<option value="30">Все Устройства</option>\
					<option value="10">Все Системы</option>\
					<option value="20">Все Реле</option>\
					<option value="40">Может быть что то ещё</option>\
				</select></p>';
		print += '<p><span class="style_btn">Показать</span></p>';
		print += '</div>';
		
		W.mid.innerHTML = print;
		
		W.bot.innerHTML = '<ul class="widget-history-list">\
		<li><p>10.04.2019</p><p>Тут что то произашло</p></li>\
		<li><p>10.04.2019</p><p>Тут что то произашло</p></li>\
		<li><p>10.04.2019</p><p>Тут что то произашло</p></li>\
		<li><p>10.04.2019</p><p>Тут что то произашло</p></li>\
		</ul>'+HWhtml.pagination({target:'.widget-history-list'});	
		return W.b;
	}
}