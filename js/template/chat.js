"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1 class="test_page">Связь с нами</h1>';
	let menu = HWhtml.btn.back();
	let content = document.createElement('div');
		content.classList.add('main-block'); content.classList.add('max-1200');
		content.classList.add('settings-page');
	let token = HWS.pageAccess;
	let chatId = H.hash[1];
	
	if(chatId){load_chat()}
	else {load_page()}

	function load_page(){
		let dopB = HWhtml.tile('one-block');
		let topB = HWhtml.tile('one-block');
		
		
		content.appendChild(dopB.b);
		content.appendChild(topB.b);

		B.title.append(title);
		B.menud.append(menu);
		B.content.append(content);
		
		topB.top.innerHTML = '<h4>Задать вопрос</h4>';
		topB.mid.innerHTML = ''+
			'<ul class="dop-inp-info size2">'+
			'<li><span>Тема:</span>'+HWhtml.select(['Поломка','Ошибка','Другое'])+'</li>'+
			'</ul>'+
			'<ul class="dop-inp-info size2 block_w_100">'+
			'<li><span>Заголовок:</span><input class="block_w_50" type="text"></li>'+
			'</ul>'+
			'<ul class="dop-inp-info line size2 block_w_100">'+
			'<li class="block_w_100"><span>Вопрос:</span><textarea class="block_w_95" rows="10" name="text"></textarea></li>'+
			'</ul>'+
			'';
		topB.mid.innerHTML += '<div class="flex_between">\
			<div></div>\
			<div class="style_btn">Отправить</div>\
		</div>';
		
		
		let print = '<p class="tc_red">ФЭЙК!!!!!</p>';
		let obj = {
			0:{name:'Ошибка',id:'id-1',date:'28.09.2019',state:'Расматривается',title:'Привет заголовок'},
			1:{name:'Ошибка',id:'id-2',date:'28.08.2019',state:'Закрыт',title:'Привет заголовок'},
			2:{name:'Ошибка',id:'id-3',date:'17.09.2019',state:'Ожидает ответа'},
			3:{name:'Другое',id:'id-4'},
			4:{name:'Ошибка',id:'id-5'},
			5:{name:'Другое',id:'id-6'},
			6:{name:'Поломка',id:'id-7'},
			7:{name:'Другое',id:'id-8'},
			8:{name:'Поломка',id:'id-9'}
		}
		
		let tmpOne = '';
			tmpOne += '<li class="one-tiket pointer" data-id="{{id}}">';
			tmpOne += '<div class="title">{{title}}</div>';
			tmpOne += '<div class="name">{{name}}</div>';
			tmpOne += '<div class="date">{{date}}</div>';
			tmpOne += '<div class="click btn_one_tiket"></div>';
			tmpOne += '<div class="state">{{state}}</div>';
			tmpOne += '</li>';
			
			print += '<div class="table-mid flex_center_top"><ul class="list chat-all-tiket"><li class="one-head">';
			print += '<div class="title">Заголовок</div>';
			print += '<div class="name">Тема</div>';
			print += '<div class="date">Дата</div>';
			print += '<div class="state">Статус</div>';
			print += '</li>';
			print += HWhtml.list(obj,tmpOne);
			print += '</ul></div>';
		
		dopB.top.innerHTML = '<h4>Мои тикеты</h4>';
		dopB.mid.innerHTML = print;

		HWF.loading('of');
	}
	
	function load_chat(){
		let dopB = HWhtml.tile('one-block');
		let btn = document.createElement('div');
			btn.classList.add('btn-page'); btn.classList.add('flex_between');
			
		B.title.append('<h1>Чат с оператором</h1>');
		B.menud.append(HWhtml.btn.back());
		dopB.b.appendChild(btn);
		content.appendChild(dopB.b);
		B.content.append(content);
		
		dopB.top.innerHTML = '<h4>Оператор 28</h4>';
		dopB.top.innerHTML += '<h5><i>Тема:</i> Поломка</h5>';
		btn.innerHTML += HWhtml.btn.back()+'<div class="text-icon btn_delete_equp block_mrl_4" title="Закрыть тикет?">✕</div>';
		
		dopB.mid.innerHTML = '<div class="chat">'+
			'<p class="user"><span>Вы:</span>Здравствуйте у меня сломалась система 3.1</p>'+
			'<p class="operator"><span>Оператор:</span>Добрый день приобретите 4.0</p>'+
			'<p class="user"><span>Вы:</span>У меня нету на неё денег</p>'+
			'<p class="operator"><span>Оператор:</span>Вы можете заработать на неё деньги с помощью нашего уникального предложения о сотрудничестве</p>'+
		'</div>';

		
		dopB.bot.innerHTML = '</ul>'+
			'<ul class="dop-inp-info line size2 block_w_100">'+
			'<li class="block_w_100"><span>Вопрос:</span><br><textarea class="block_w_95" rows="4" name="text"></textarea></li>'+
		'</ul>'+
		'<div class="flex_between">'+
			'<div></div>'+
			'<div class="style_btn">Отправить</div>'+
		'</div>';
	}
	

	// Нажатие на элемент списка в тикетах
	HW.on('click','btn_one_tiket',function(e,elem){		
		let main = HW.parentSearchClass(elem,'one-tiket');
		let uid = main.dataset.id;
		HWS.go('chat/'+uid);
	});
});