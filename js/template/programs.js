"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	HWS.buffer.dopFilter = {}//  Дополнительный фильтр. для оборудования
	let viewTable = localStorage.getItem('viewPrograms') || 'tile';
	let vieeControl = {}; vieeControl[viewTable] = 'active';
	let title = '<h1>Программы</h1>';
	let menu = '<div></div>';
		menu += '<div>';
		menu += '<span class="add_program btnIcon">\
				<span class="select-none">Добавить программу</span>\
				<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>\
			</span>';
		menu += '</div>';
	let token = HWS.pageAccess;
	let hashGet = HWS.getHash().get;
	if(JSON.stringify(hashGet) != "{}"){hashGet = 'active';} else {hashGet = '';}
	let content = '<div class="main-block ob-center max-1200 programs-page">\
		<div class="filter-equipments one-block-col">\
			<div class="flex_between block_w_100">\
				<div class="btn-open-filter btn-active pointer block_w_210p" data-onof="#filter-window" data-targetdown="#icon-filter">\
					<div class="display_inline select-none">\
						<span class="spec-circle-icon '+hashGet+'"></span>\
						<span id="icon-filter" class="icon icon-filter hover_orange p20"></span>\
					</div>\
					<span class="vertical-middle select-none ts_12p block_pl_5">Фильтр</span>\
				</div>\
				'+HWhtml.search('block_mr_8')+'\
				<div id="block-menu-action-btn" class="open-clouse block_w_305p">\
					<span class="style_btn p3 grey block_ml_6 btn_set_tmp">Шаблон</span>\
					<span class="style_btn p3 grey block_ml_6 btn_go_vijet">В виджеты</span>\
					<span class="style_btn p3 grey block_ml_6 btn_delete_elems">Удалить</span>\
				</div>\
			</div>\
			'+HWfilter.programs()+'\
			<div class="dop-sort-filter">\
				<div class="checkbox-text btn-active btn_dop_sort_filter_manual_mode">\
					<span class="checkbox select-none"></span>\
					<span class="name select-none">Сначала в ручном режиме</span>\
				</div>\
			</div>\
		</div>\
		<div id="all-programs" class="one-block nop all-programs">'+HWhtml.loader()+'</div>\
	</div>';
	
	//HWS.blocks.content.append(rMenu);
	
	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);

	let get = HWS.getHash().get;
	let controlQuery = true;
	HWF.recount(get,function(val){if (val == "no_select"){controlQuery = false}});
	if(controlQuery){HWprog.list('#all-programs','','checkbox');}
	else {
		let list = HWS.getElem('#all-programs');
		list.innerHTML = '<p class="tw_600 ta_center block_p_20">Оборудование не найдено. Попробуйте изменить настройки фильтра.</p>';
	}
	
	
	//  /Function
	
	function dop_filter(){
		if(!HWS.buffer.programListDopSortFiltGetServer){HWS.buffer.programListDopSortFiltGetServer = {}}
		let arrGet = [];
		let objControl = HWS.buffer.dopFilter;

		HWF.recount(objControl,function(elf,keyf){
			if(elf){
				if(keyf == 'name'){keyf = '-name';}
				arrGet.push(keyf);
			}
		});
		
		HWS.buffer.programListDopSortFiltGetServer.sort = arrGet;
		HWprog.list('#all-programs','','checkbox');
		console.log(arrGet);
	}
	
	//Нажатие на чекбокс
	HW.on('click','one_prog_check',function(e,el){
		let btnBox = HWS.getElem('#block-menu-action-btn');
		
		if(el.classList.contains('active')){
			btnBox.classList.add('open');
		}else {
			let allAct = document.querySelectorAll('.list-programs .one-program .equ_b .one_prog_check.active');
			if (allAct.length == 0){btnBox.classList.remove('open');}
		}
	});
	//Выбрать ВСЕ программы
	HW.on('click','prog_select_all',function(event,elem){
		HWF.allActive('.list-programs','.checkbox');
		
		let testSheck = HWS.getElem('.list-programs .one-program .one_prog_check.active');
		let btnBox = HWS.getElem('#block-menu-action-btn');
		
		if(testSheck){btnBox.classList.add('open');}
		else {btnBox.classList.remove('open');}
	});
	
	// Нажатие на + ( добавить программу )
	HW.on('click','add_program',function(){
		HWF.modal.print('Выберите систему','');
		HWF.print.system('.modal-section',{a:'no',type:'nobtn system-list'});
	});
	// Выбор системы для добавления
	HW.on('click','btn-one-equip',function(e,elem){
		HWF.modal.print('Выберите программу','<div>'+HWhtml.loader()+'</div>');
		let elemLi = HW.parentSearchClass(elem,'one-equipment');
		let version = elemLi.dataset.version;
		HWS.buffer.idSystem = elemLi.dataset.id;
		// если системы старые то грузим шаблоны для них
		if(version == 30 || version == 31 || version == 32 || version == 33){
			setTimeout(function(){
				let list = HWD.typeTempProg;
				let print = HWhtml.templatePrograms(list,version)
				B.modal.data.body.append(print);
			},200);
		}
	});
	// Выбор программы из списка програм
	HW.on('click','clic_tmp_program',function(e,el){
		let li = HW.parentSearchClass(el,'one-tmp-program');
		let tmp = li.dataset.type;
		HWF.modal.message('Запрос отправлен!','Запрос на создание программы отправлен системе.<br> Ожидайте появление программы в списке.<br><br><br><br>');
		
		let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.idSystem,program_type:tmp}
		HWF.post({url:HWD.api.gets.taskAddProg3xSystem,data:dataP,fcnE:HWF.getServMesErr});
	});
	
	
	//Кнопка Шаблон
	HW.on('click','btn_set_tmp',function(e,el){
		let allAct = document.querySelectorAll('.list-programs li .equ_b .one_prog_check.active');
		let bufAr = [], html='';
		if (allAct.length != 0){
			HWF.recount(allAct,function(elem){
				let li = HWS.upElem(elem,'li');
				bufAr.push(li.dataset.id);
			},'on');
			HWS.buffer.goTemplateArray  = bufAr;
			
			html+= '';
			
			HWF.modal.print('Выбор шаблона',html);
			HWF.print.templates('.modal-window .modal-section',{a:'off'});
		}
	});	
	// Выбор одного шаблона
	HW.on('click','click_one_temp',function(e,el){
		let id = HWS.upElem(el,'li').dataset.id;
		HWF.modal.print('Установка шаблона',HWhtml.loader());
		HWF.post({url:'patterns/apply',data:{token:token,id:id,id_objects:HWS.buffer.goTemplateArray},fcn:function(info){
			let Print = 'Шаблон применен к '+info.success_apply+' элементу(ам) из '+(info.fail_apply + info.success_apply)+'.';
			HWF.modal.message(Print,'Установка шаблона',);
		}});
	});
	
	
	//Кнопка в виджеты оборудования
	HW.on('click','btn_go_vijet',function(e,el){
		let allAct = document.querySelectorAll('.list-programs .one-program .one_prog_check.active');
		let bufAr = [];
		if (allAct.length != 0){
			HWF.recount(allAct,function(elem){
				let li = HWS.upElem(elem,'li');
				bufAr.push(li.dataset.id);
			},'on');
			HWS.buffer.goVijetArray  = bufAr;
			HWF.modal.confirm('Добавление виджетов','Добавить выбранные программы на страницу "Виджеты"?','','Добавить');
			HW.on('click','btn-yes-modal',function(e,el){
				HWF.push('Выбранные программы добавлены на страницу "Виджеты"','green');
				HWF.post({url:HWD.api.gets.vijetAdd,data:{token:token,id_objects:HWS.buffer.goVijetArray},fcn:function(info){
					
				}});
			});
		}
	});	
		
	//Кнопка удоления оборудования
	HW.on('click','btn_delete_elems',function(e,el){
		let allAct = document.querySelectorAll('.list-programs .one-program .one_prog_check.active');
		let bufAr = [];
		if (allAct.length != 0){
			let infoText = 'Выбранные программы будут удалены из систем, которые их содержат<br>Программа охраны и оповещений не могут быть удалены.';
			HWF.recount(allAct,function(elem){
				let li = HWS.upElem(elem,'li')
				bufAr.push(li);
			},'on');
			HWS.buffer.deleteEqupArray  = bufAr;
			HWF.modal.confirm('Удалить Программы?',infoText,'','Удалить');
			HW.on('click','btn-yes-modal',function(e,el){
				let allId = [];
				HWF.recount(HWS.buffer.deleteEqupArray,function(equpLi){
					allId.push(equpLi.dataset.id);
					equpLi.remove();
				});
				HWF.modal.loader();
				console.log(allId);
				HWF.post({url:'task/delete_program_3x_system',data:{token:token,id_objects:allId},fcn:function(info){
					console.log(info);
					HWF.modal.of();
				}});
			});
		}
	});
	
	//ФИЛЬТР сначала в ручном.
	HW.on('click','btn_dop_sort_filter_manual_mode',function(e,el){
		if(!HWS.buffer.programListDopSortFiltGetServer){HWS.buffer.programListDopSortFiltGetServer = {}}
		if(el.classList.contains('active')){HWS.buffer.programListDopSortFiltGetServer.no_active = 1;}
		else {HWS.buffer.programListDopSortFiltGetServer.no_active = 0}

		HWprog.list('#all-programs','','checkbox');
	});
	
	//ФИЛЬТР сортировать ПО имени. btn_dop_sort_system
	HWS.buffer.fcnSornName = function(){dop_filter();}
});