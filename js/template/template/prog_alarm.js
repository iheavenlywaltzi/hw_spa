"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#temp-content-top');
	let content = document.body.querySelector('#temp-content-mid');
	let info = HWS.buffer.elem;
	let token = HWS.pageAccess;
	let id = info.id;
	let S = {}, print = '', printT = '';

	printT+= '<div class="one-block">';
	printT+= '<div class="name-page">\
		<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
		<input id="prog-name" type="text" class="input-name-page" value="'+info.name+'">\
	</div>';
	printT+= '</div>';
	
	print+= '<div class="one-block"><ul class="list-param-template">';
	print+= HWtemp.control('Имя','object_name','name');
	print+= HWtemp.control('Ручной режим','active','switch');
	print+= HWtemp.control('Режим при тревоге','inverse','select',{list:[
			'Включать (открывать)',
			'Отключать (закрывать)',
		]});
	
	let actCheck = (info.alarm_delay.applies)?'active':'';
	let ofLi = (info.alarm_delay.applies)?'':'of';
	if(info.alarm_delay.value == 0){S.type_action = 1}
	else if(info.alarm_delay.value == 65535){S.type_action = 2}
	else {S.type_action = 3; S.time = info.alarm_delay.value}
	S.typeAction = {}; S.typeAction[S.type_action] = 'selected';
	S.alarmSO = (S.type_action == 3)?'open':'';
	print+= '<li data-param="alarm_delay" class="one-param '+ofLi+'">';
	print+= '	<span class="name">Время автооповещений</span>';
	print+= '	<span class="param">\
					<select class="open_control time_reaction_sensor vertical-middle">\
					<option '+(S.typeAction[1] || '')+' value="1" data-of="#alarm-control-timer">на время тревоги</option>\
					<option '+(S.typeAction[2] || '')+' value="2" data-of="#alarm-control-timer">при тревоге до сброса вручную</option>\
					<option '+(S.typeAction[3] || '')+' value="3" data-on="#alarm-control-timer">на заданное время, сек</option>\
					</select>\
					<span class="block_w_20p"></span>\
					<span id="alarm-control-timer" class="open-clouse vertical-middle '+S.alarmSO+'">\
						'+HWhtml.btn.numb(S.time,{fcn:'setTime',clas:'btn_time',min:0,max:9999})+'\
					</span>\
				</span>';
	print+= '	<span class="btn-active checkbox btn_check_template '+actCheck+'"></span>';
	print+= '</li>';		
		
	print+= '</ul></div>';
	
	contentTop.innerHTML = printT;
	content.innerHTML = print;
	
	// Время работы системы:
	HW.on('change','time_reaction_sensor',function(e,el){
		let inpTime = HWS.getElem('#alarm-control-timer .inp_num_val');
		let newInfo = {}; newInfo.alarm_delay={};
		if(el.value == 1){
			newInfo.alarm_delay.value = 0; inpTime.value = 0;
			HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:info.id,updates:newInfo}});
		}
		if(el.value == 2){
			newInfo.alarm_delay.value = 65535; inpTime.value = 65535;
			HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:info.id,updates:newInfo}});
		}
		if(el.value == 3){
			newInfo.alarm_delay.value = 1; inpTime.value = 1;
			HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:info.id,updates:newInfo}});
		}
	});
	HWS.buffer.setTime = function(nn){
		let newInfo = {}; newInfo.alarm_delay={}; newInfo.alarm_delay.value = nn;
		HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:info.id,updates:newInfo},fcn:function(ninfo){console.log(ninfo)}});
	}
});