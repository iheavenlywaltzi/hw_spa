"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#temp-content-top');
	let content = document.body.querySelector('#temp-content-mid');
	let info = HWS.buffer.elem;
	let token = HWS.pageAccess;
	let id = info.id;
	let S = {}, print = '', printT = '';

	printT+= '<div class="one-block">';
	printT+= '<div class="name-page">\
		<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
		<input id="prog-name" type="text" class="input-name-page" value="'+info.name+'">\
	</div>';
	printT+= '</div>';
	
	print+= '<div class="one-block"><ul class="list-param-template">';
	print+= HWtemp.control('Имя','object_name','name');
	print+= HWtemp.control('Ручной режим','active','switch');
	
	let printInfo = buttonsController.createrTimetableInArray(info.half_hourly_schedule.value);
	let actCheck = (info.half_hourly_schedule.applies)?'active':'';
	let ofLi = (info.half_hourly_schedule.applies)?'':'of';
	print+= '<li data-param="half_hourly_schedule" class="one-param '+ofLi+'">';
	print+= '	<span class="block_w_100 flex_between">\
					<span class="name-page">Расписание</span>\
					<span class="btn-active checkbox btn_check_template '+actCheck+'"></span>\
				</span>\
				<timetable id="timetable-temp" data-fcn="timetableUpFcn" type="custom" step=" ,00:30, ,01:30, ,02:30, ,03:30, ,04:30, ,05:30, ,06:30, ,07:30, ,08:30, ,09:30, ,10:30, ,11:30, ,12:30, ,13:30, ,14:30, ,15:30, ,16:30, ,17:30, ,18:30, ,19:30, ,20:30, ,21:30, ,22:30, ,23.30" size="19" data-data="'+printInfo+'">\
					<val value="0" color="#e0e0e0">Откл</val>\
					<val value="1" color="#569AD4">Вкл</val>\
				</timetable>';
	print+= '</li>';
	
	print+= '</ul></div>';
	
	contentTop.innerHTML = printT;
	content.innerHTML = print;
	// запуск кнопок ПРограмм
	buttonsController.createTimetable();
	
	// Изменение календаря
	HWS.buffer.timetableUpFcn = function(inf){
		clearTimeout(HWS.buffer.idTimerTimetableUpFcn);
		HWS.buffer.idTimerTimetableUpFcn = setTimeout(function(){
			let allElem = inf.allElem;
			let specArrey = []
			for (let key in allElem){
				let val = (allElem[key].dataset.val == 1)?1:0;
				specArrey.push(val);
			}
			let newInfo = {}; newInfo.half_hourly_schedule={}; newInfo.half_hourly_schedule.value = specArrey;
			let data = {token:HWS.pageAccess,id:info.id,updates:newInfo}
			HWF.post({url:'patterns/update',data:data,fcn:function(ninfo){console.log(ninfo)}});
		},2000);
	}
});