"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#temp-content-top');
	let content = document.body.querySelector('#temp-content-mid');
	let info = HWS.buffer.elem;
	let token = HWS.pageAccess;
	let id = info.id;
	let S = {}, print = '', printT = '';

	printT+= '<div class="one-block">';
	printT+= '<div class="name-page">\
		<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
		<input id="prog-name" type="text" class="input-name-page" value="'+info.name+'">\
	</div>';
	printT+= '</div>';
	
	print+= '<div class="one-block"><ul class="list-param-template">';
	print+= HWtemp.control('Имя','object_name','name');
	print+= HWtemp.control('Радиосеть','radio','switch');
	print+= HWtemp.control('порт ДОП','ext','switch');
	print+= HWtemp.control('Микрофон','microphone','switch');
	print+= HWtemp.control('Тревожная кнопка','alarm_button','switch');
	print+= HWtemp.control('USSD команда <br> проверки баланса','balance_ussd','text');
	print+= HWtemp.control('Приоритет доступа в ЛК','lk','select',{list:[
			'ЛК отключен',
			'Только GSM',
			'Только Wi-Fi',
			'GSM потом WiFi',
			'WiFi потом GSM',
		]});
	print+= '</ul></div>';

	contentTop.innerHTML = printT;
	content.innerHTML = print;
});