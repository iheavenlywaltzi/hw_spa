"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#temp-content-top');
	let content = document.body.querySelector('#temp-content-mid');
	let info = HWS.buffer.elem;
	let token = HWS.pageAccess;
	let id = info.id;
	let S = {}, print = '', printT = '';

	printT+= '<div class="one-block">';
	printT+= '<div class="name-page">\
		<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
		<input id="prog-name" type="text" class="input-name-page" value="'+info.name+'">\
	</div>';
	printT+= '</div>';
	
	print+= '<div class="one-block"><ul class="list-param-template">';
	print+= HWtemp.control('Имя','object_name','name');
	print+= HWtemp.control('Ручной режим','active','switch');
	print+= HWtemp.control('Эконом','temp_econom','number',{min:-40,max:99});
	print+= HWtemp.control('Стандарт','temp_standard','number',{min:-40,max:99});
	print+= HWtemp.control('Комфорт','temp_comfort','number',{min:-40,max:99});
	print+= HWtemp.control('Своё','temp_user','number',{min:-40,max:99});
	print+= HWtemp.control('Температурный режим','mode','select',{list:{
		1:'Эконом',
		2:'Стандарт',
		3:'Комфорт',
		4:'Своё',
		5:'Расписание',
	}});
	
	let printInfo = buttonsController.createrTimetableInArray(info.hourly_schedule.value,24);
	let actCheck = (info.hourly_schedule.applies)?'active':'';
	let ofLi = (info.hourly_schedule.applies)?'':'of';
	print+= '<li data-param="hourly_schedule" class="one-param '+ofLi+'">';
	print+= '	<span class="block_w_100 flex_between">\
					<span class="name-page">Расписание</span>\
					<span class="btn-active checkbox btn_check_template '+actCheck+'"></span>\
				</span>\
				<div class="block_w_100">\
					<timetable id="timetable-temp" data-fcn="timetableUpFcn" size="20" data-data="'+printInfo+'">\
					<val value="0" color="#569AD4">Эконом</val>\
					<val value="1" color="#ffae00">Стандарт</val>\
					<val value="2" color="#f22222">Комфорт</val>\
					</timetable>\
				</div>';
	print+= '</li>';
	
	print+= HWtemp.control('Гистерезис','hysteresis','number',{min:0,max:10});
	print+= HWtemp.control('Выбег насоса, сек.','delay_pump','number',{min:0,max:65535});
	print+= '</ul></div>';
	
	contentTop.innerHTML = printT;
	content.innerHTML = print;
	// запуск кнопок ПРограмм
	buttonsController.createTimetable();
	
		// Изменение календаря
	HWS.buffer.timetableUpFcn = function(inf){
		clearTimeout(HWS.buffer.idTimerTimetableUpFcn);
		HWS.buffer.idTimerTimetableUpFcn = setTimeout(function(){
			let allElem = inf.allElem;
			let specArrey = []
			for (let key in allElem){
				let val = allElem[key].dataset.val || 0;
				specArrey.push(val);
			}
			let newInfo = {}; newInfo.hourly_schedule={}; newInfo.hourly_schedule.value = specArrey;
			let data = {token:HWS.pageAccess,id:info.id,updates:newInfo}
			console.log(data);
			HWF.post({url:'patterns/update',data:data,fcn:function(ninfo){console.log(ninfo)}});
		},2000);
	}
});