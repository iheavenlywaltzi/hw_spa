"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#temp-content-top');
	let content = document.body.querySelector('#temp-content-mid');
	let info = HWS.buffer.elem;
	let token = HWS.pageAccess;
	let id = info.id;
	let S = {}, print = '', printT = '';

	printT+= '<div class="one-block">';
	printT+= '<div class="name-page">\
		<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
		<input id="prog-name" type="text" class="input-name-page" value="'+info.name+'">\
	</div>';
	printT+= '</div>';
	
	print+= '<div class="one-block"><ul class="list-param-template">';
	print+= HWtemp.control('Имя','object_name','name');
	print+= HWtemp.control('Активен','active','switch');
	print+= HWtemp.control('Журнал','journal','switch');
	print+= HWtemp.control('Поправка','correction','number',{min:0,max:100});
	print+= HWtemp.control('Задержка для <br> нормы, сек','delay_norm','number',{min:0,max:9999});
	print+= HWtemp.control('Задержка для <br> тревоги, сек','delay_alarm','number',{min:0,max:9999});
	print+= HWtemp.control('Нижний порог','min_bound','number',{min:0,max:100});
	print+= '</ul></div>';
	
	contentTop.innerHTML = printT;
	content.innerHTML = print;
});