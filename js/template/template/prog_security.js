"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#temp-content-top');
	let content = document.body.querySelector('#temp-content-mid');
	let info = HWS.buffer.elem;
	let token = HWS.pageAccess;
	let id = info.id;
	let S = {}, print = '', printT = '';

	printT+= '<div class="one-block">';
	printT+= '<div class="name-page">\
		<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
		<input id="prog-name" type="text" class="input-name-page" value="'+info.name+'">\
	</div>';
	printT+= '</div>';
	
	print+= '<div class="one-block"><ul class="list-param-template">';
	print+= HWtemp.control('Имя','object_name','name');
	print+= HWtemp.control('Задержка постановки <br> и снятия, сек','delay','number',{min:0,max:9999});
	print+= HWtemp.control('Всегда оповещать о <br> постановке и снятии','notif_arm_disarm','switch');
	print+= HWtemp.control('Звук при управлении охраной','enable_sound','switch');
	print+= HWtemp.control('длительность звука, сек','duration_sound','number',{min:0,max:9999});
	print+= HWtemp.control('Свет при управлении охраной','enable_light','switch');
	print+= HWtemp.control('Режим работы света','mode_light','select',{list:{
		1:'гореть при охране',
		2:'мигать при охране',
	}});
	print+= '</ul></div>';
	
	contentTop.innerHTML = printT;
	content.innerHTML = print;
});