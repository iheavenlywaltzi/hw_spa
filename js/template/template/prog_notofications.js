"use strict";
HWS.templateAdd(function(HW,B,H){
	let contentTop = document.body.querySelector('#temp-content-top');
	let content = document.body.querySelector('#temp-content-mid');
	let info = HWS.buffer.elem;
	let token = HWS.pageAccess;
	let id = info.id;
	let S = {}, print = '', printT = '';

	printT+= '<div class="one-block">';
	printT+= '<div class="name-page">\
		<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
		<input id="prog-name" type="text" class="input-name-page" value="'+info.name+'">\
	</div>';
	printT+= '</div>';
	
	print+= '<div class="one-block"><ul class="list-param-template">';
	print+= HWtemp.control('Имя','object_name','name');
	print+= HWtemp.control('Оповещать','global_notif','switch');
	print+= HWtemp.control('Режим автооповещений','alive','select',{list:{
		0:'Нет оповещений',
		1:'Раз в 1 день',
		2:'Раз в 2 дня',
		3:'Раз в 3 дня',
		4:'Раз в 4 дня',
		5:'Раз в 5 дней',
		6:'Раз в 6 дней',
		7:'Раз в неделю',
		9:'2 Раз в 1 день',
	}});
	print+= HWtemp.control('Режим повторного вызова','recall','select',{list:[
		'Не перезванивать',
		'Перезвонить 1 раз',
		'Перезвонить 2 раза',
		'Перезвонить 3 раза',
		'Перезвонить 4 раза',
		'Перезвонить 5 раз',
	]});
	print+= HWtemp.control('Оповещать об окончании тревоги','return_norm','switch');
	
	
	let actCheck = (info.alive_time.applies)?'active':'';
	let ofLi = (info.alive_time.applies)?'':'of';
	print+= '<li data-param="alive_time" class="one-param '+ofLi+'">';
	print+= '	<span class="name">Время автооповещений</span>';
	print+= '	<span class="param">';
	print+= '		<span>'+HWhtml.btn.numb(info.alive_time.hour,{fcn:'alive_time_h',min:0,max:23})+' ч. </span>';
	print+= '		<span class="block_w_20p"></span>';
	print+= '		<span>'+HWhtml.btn.numb(info.alive_time.minute,{fcn:'alive_time_m',min:0,max:59})+' мин. </span>';
	print+= '	</span>';
	print+= '	<span class="btn-active checkbox btn_check_template '+actCheck+'"></span>';
	print+= '</li>';
	print+= number_create('Абонент 0','phone0');
	print+= number_create('Абонент 1','phone1');
	print+= number_create('Абонент 2','phone2');
	print+= number_create('Абонент 3','phone3');
	print+= number_create('Абонент 4','phone4');
	print+= number_create('Абонент 5','phone5');
	print+= number_create('Абонент 6','phone6');
	print+= number_create('Абонент 7','phone7');
	print+= number_create('Абонент 8','phone8');
	print+= number_create('Абонент 9','phone9');
	print+= '</ul>';
	print+= '<div class="block-pr-10 block-w-100 all-span-midle ta_right">					<span class="block-p-10">						<span class="icon icon-send-voice"></span>						<span class="text-grey">голосом</span>					</span>					<span class="block-p-10">						<span class="icon icon-send-sms"></span>						<span class="text-grey">по SMS</span>					</span>					<span class="block-p-10">						<span class="icon icon-send-sms-auto"></span>						<span class="text-grey">автоотчеты в SMS</span>					</span>					<span class="block-p-10">						<span class="icon icon-send-sms-secure"></span>						<span class="text-grey">SMS о состоянии охраны</span>					</span>				</div>';
	print+= '</div>';
	
	contentTop.innerHTML = printT;
	content.innerHTML = print;
	
	// FUNCTION
	function number_create(name,param){
		let ofOnLi = (info[param].applies)?'':'of';
		let activCheck = (info[param].applies)?'active':'';
		let nameVal = info[param].value || '';
		let btn = {}
			btn.voice = ''; if(info[param].call_alarm){btn.voice = 'active'}
			btn.sms = ''; if(info[param].sms_alarm){btn.sms = 'active'}
			btn.auto = ''; if(info[param].period_sms){btn.auto = 'active'}
			btn.security = ''; if(info[param].security_sms){btn.security = 'active'}
		
		
		let li = '<li data-param="'+param+'" class="one-param '+ofOnLi+'">';	
			li+= '<span class="name">'+name+'</span>';
			li+= '<span class="param">\
				<div class="display_inline vertical-middle"><span class="plus-from-input-tel">+</span><input class="tel name_phone_user" type="tel" maxlength="12" value="'+nameVal+'"></div>\
				<span class="block_pl_8 vertical-middle">\
					<span class="btn-active icon hover icon_phon_btn icon-send-voice '+btn.voice+'"></span>\
					<span class="btn-active icon hover icon_phon_btn icon-send-sms '+btn.sms+'"></span>\
					<span class="btn-active icon hover icon_phon_btn icon-send-sms-auto '+btn.auto+'"></span>\
					<span class="btn-active icon hover icon_phon_btn icon-send-sms-secure '+btn.security+'"></span>\
				</span>\
			</span>';
			li+= '<span class="btn-active checkbox btn_check_template '+activCheck+'"></span>';
		li+= '</li>';
		return li;
	}
	
	
	
	// ACTION
	HWS.on('change','name_phone_user',function(e,el){
		let li = HWS.upElem(el,'.one-param');
		let param = li.dataset.param;
		let number = parseInt(el.value);
			number = String(number);	
		if(el.value) {
			if(number.length >= 11){
				let newInfo = {}; newInfo[param]={}; newInfo[param].value = number;
				HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:info.id,updates:newInfo}});
				el.classList.remove('red');
			} else {
				el.classList.add('red');
			}
		}
		else {
			el.value = '';
		}
	});
	
	HWS.on('click','icon_phon_btn',function(e,el){
		let li = HWS.upElem(el,'.one-param');
		let bigSpan = HWS.upElem(el,'span');
		let param = li.dataset.param;
		let btn = {}
			btn.voice = (bigSpan.querySelector('.icon-send-voice').classList.contains('active'))?'1':'0';
			btn.sms = (bigSpan.querySelector('.icon-send-sms').classList.contains('active'))?'1':'0';
			btn.auto = (bigSpan.querySelector('.icon-send-sms-auto').classList.contains('active'))?'1':'0';
			btn.security = (bigSpan.querySelector('.icon-send-sms-secure').classList.contains('active'))?'1':'0';
		
		let newInfo = {}; newInfo[param]={}; 
			newInfo[param].call_alarm = btn.voice
			newInfo[param].period_sms = btn.auto;
			newInfo[param].security_sms = btn.security;
			newInfo[param].sms_alarm = btn.sms;
		HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:info.id,updates:newInfo}});
	});
	
	HWS.buffer.alive_time_h = function(nn){
		let newInfo = {}; newInfo.alive_time={}; newInfo.alive_time.hour = nn;
		HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:info.id,updates:newInfo},fcn:function(ninfo){console.log(ninfo)}});
	}
	HWS.buffer.alive_time_m = function(nn){
		let newInfo = {}; newInfo.alive_time={}; newInfo.alive_time.minute = nn;
		HWF.post({url:'patterns/update',data:{token:HWS.pageAccess,id:info.id,updates:newInfo}});
	}
});