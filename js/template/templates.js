"use strict";
HWS.template.templates = function(HW,B,H){
	HWF.clearBlocks();//очистим блоки контена
	let hashGet = HWS.getHash().get;
	let token = HWS.pageAccess;	
	let viewTable = localStorage.getItem('viewEquipments') || 'list';
	let vieeControl = {}; vieeControl[viewTable] = 'active';
	let title = '<h1>Шаблоны  настроек</h1>';
	let menu = '<div>';
		menu += '<a class="btn" href="#equipments">Оборудование</a>';
		menu += '<a class="btn active" href="#templates">Шаблоны настроек</a>';
		menu += '</div>';
		menu += '<div>';
		menu += '<span class="add_new_template btnIcon"><span class="select-none">Добавить шаблон</span> <span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span></span>';
		menu += '</div>';

	if(JSON.stringify(hashGet) != "{}"){hashGet = 'active';} else {hashGet = '';}
	let content = '<div class="main-block ob-center max-1200 equipments-page">\
		<div class="filter-equipments one-block-col">\
			<div class="flex_between block_w_100">\
				<div class="btn-open-filter btn-active pointer block_w_210p" data-onof="#filter-window" data-targetdown="#icon-filter">\
					<div class="display_inline select-none">\
						<span class="spec-circle-icon '+hashGet+'"></span>\
						<span id="icon-filter" class="icon icon-filter hover_orange p20"></span>\
					</div>\
					<span class="vertical-middle select-none ts_12p block_pl_5">Фильтр</span>\
				</div>\
				'+HWhtml.search('block_mr_8')+'\
				<div id="block-menu-action-btn" class="open-clouse block_w_210p">\
					<span class="style_btn grey block_ml_6 btn_delete_equp">Удалить</span>\
				</div>\
			</div>\
			'+HWfilter.templates()+'\
		</div>\
		<div class="one-block nop all-templates"></div>\
	</div>';

	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);
	
	load_page();
	
	function load_page(){
		let get = HWS.getHash().get;
		let controlQuery = true;
		let system = get.equp_system;

		HWF.recount(get,function(val){if (val == "no_select"){controlQuery = false}});
		 
		if(controlQuery){ HWF.print.templates('.all-templates',{a:'on'}); }
		else {
			let list = HWS.getElem('.all-equipments');
			list.innerHTML = '<p class="tw_600 ta_center block_p_20">Оборудование не найдено. Попробуйте изменить настройки фильтра.</p>';
		}
	}
	
	HWS.on('click','add_new_template',function(e,elem){
		let html = '';
		let spec_sel = {};
		HWF.recount(HWD.temp,function(el,key){spec_sel[key] = el.title}) // сформирует из общей даты (шаблонов) список
		
		html += '<div class="confirm-modal">';
		html += '<div class="confirm-modal-block">';
		html += '<div class="confirm-top">';
		
		html += '<ul class="dop-inp-info">';
		html += '<li><span>Тип:</span>'+HWhtml.select(spec_sel,'new_type_temp')+'</li>';
		html += '</ul>';

		html += '</div>';
		html += '<div data-target="modal-window" class="btn-no-modal btn-active style_btn">Отмена</div>';
		html += '<div data-target="modal-window" class="btn_create_temp btn-active style_btn orange">Создать</div>';
		html += '</div>';
		html += '</div>';

		HWF.modal.print('Выбор шаблона',html);
	});
	
	// создание шаблона
	HWS.on('click','btn_create_temp',function(e,elem){
		let type = HWS.getElem('.new_type_temp').value;
		HWF.modal.print('Создание шаблона',HWhtml.loader());

		HWF.post({url:'patterns/create',data:{token:token,type:type},fcn:function(inf){
			if(inf.success){HWS.go(HWD.page.temp+'/'+inf.id)}
		}});
	});
	
		//select all
	HW.on('click','template_select_all',function(event,elem){
		HWF.allActive('.list.templates','.checkbox');
		
		let testSheck = HWS.getElem('.list.templates .one-elem .one_template_check.active');
		let btnBox = HWS.getElem('#block-menu-action-btn');
		
		if(testSheck){btnBox.classList.add('open');}
		else {btnBox.classList.remove('open');}
	});
	
	//Нажатие на чекбокс
	HW.on('click','one_template_check',function(e,el){
		let btnBox = HWS.getElem('#block-menu-action-btn');
		
		if(el.classList.contains('active')){
			btnBox.classList.add('open');
		}else {
			let allAct = document.querySelectorAll('.list.templates .one-elem .one_template_check.active');
			if (allAct.length == 0){btnBox.classList.remove('open');}
		}
	});	
	
		
	//Кнопка удоления шаблонов
	HW.on('click','btn_delete_equp',function(e,el){
		let allAct = document.querySelectorAll('.list.templates .one-elem .one_template_check.active');
		let bufAr = [];
		if (allAct.length != 0){
			let infoText = 'Выбранные шаблоны будут удолены.';
			HWF.recount(allAct,function(elem){
				let li = HWS.upElem(elem,'li')
				bufAr.push(li);
			},'on');
			HWS.buffer.deleteEqupArray  = bufAr;
			HWF.modal.confirm('Удалить шаблоны ?',infoText,'','Удалить');
			HW.on('click','btn-yes-modal',function(e,el){
				let allId = [];
				HWF.recount(HWS.buffer.deleteEqupArray,function(equpLi){
					allId.push(equpLi.dataset.id);
					equpLi.remove();
				});
				HWF.modal.loader();
				HWF.post({url:'patterns/delete',data:{token:token,ids:allId},fcn:function(info){
					console.log(info);
					HWF.modal.of();
				}});
			});
		}
	});
}



