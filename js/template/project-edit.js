"use strict";
HWS.templateAdd(function(HW,B,H){
	let infoBlock = HWS.buffer.infoBlock;
	let GodToken = localStorage.getItem('PITn0Hxg') || false;
	let print = '';
	let token = HWS.pageAccess;
	let info = HWS.buffer.elemLoadInfo;
	let projectID = H.hash[1];
	let objSet = {}; 
		
	B.title.append('<h1>Редактировать Проект</h1>');
	let menu = '<div><span class="btn btn_dash_click active" data-fcn="equpPageSets">Настройки</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageObject">Объекты</span>';
		menu+= '<span class="btn btn_dash_click" data-fcn="equpPageAccesses">Доступ</span></div>';
		//menu+='<div><a title="Смотреть проект" href="#'+HWD.page.proj+'/'+projectID+'" class="btn_text grey block_p_8">В проект</a></div>'
	let Sprint ='';
	let proj = info; console.log(proj);
	let body = document.createElement('div');
		body.classList.add('project-edit');
		body.classList.add('main-block');
		body.classList.add('max-1200');
		body.classList.add('no-p-top');
	let bodyTop = document.createElement('div');
		bodyTop.classList.add('project-edit');
		bodyTop.classList.add('main-block');
		bodyTop.classList.add('max-1200');
		bodyTop.classList.add('no-p-bot');
	B.content.append('');
	B.content.append(bodyTop);
	B.content.append(body);
	
	objSet.img = false;
	objSet.type = proj.project_type;
	objSet.name = false;
	objSet.zoom = proj.project_data.zoom || false;
	
	bodyTop.innerHTML = printInfoTop(proj);
	
	// Зфгрузка настройки
	HWS.buffer.equpPageSets = function(){
		body.innerHTML = printInfo(proj);
		if (objSet.type == 2){create_map()}
	};
	
	// Зфгрузка подстраници "ОБЪЕКТЫ"
	HWS.buffer.equpPageObject = function(){
		let listEqup = proj.project_data.equipments;
		let listProj = proj.project_data.projects;
		let allEqup = [];
		let allProj = [];
		let allObj = [];
		let iter = 0;
		let lisrPrint = '';
		// Массивы для запроса
		HWF.recount(listEqup,function(el){allEqup.push(el.id)});
		HWF.recount(listProj,function(el){allProj.push(el.id)});

		Sprint = '<div id="project-content-object-block" class="one-block nop ob-top conections">';
		Sprint+= HWhtml.loader();
		Sprint+= '</div>';
		body.innerHTML = Sprint
		
		// если есть "ОБОРУДОВАНИ"
		if (allEqup.length > 0){
			let dataGet = {token:token,list_start:0,list_amount:100,id_object:allEqup};
			HWF.post({url:HWD.api.gets.list,data:dataGet,fcn:function(inf){
				let listEq = inf.list;
				HWF.recount(listEq,function(el){
					let et = (el.info.type)? HWD.equp[el.info.type.type] || false : false;
					let id = el.id;
					let name = el.config.name || '';
					let type = et.type || 'equp';
					let icon = el.info.type.type || '';
					allObj.push({
						id:id,
						name:name,
						type:type,
						icon:'icon_equip_'+icon,
					});
				});
				load_list_object();
			}});
		} else {load_list_object();}
		
		// если есть "ПОЕКТЫ"
		if (allProj.length > 0){
			let data = {token:token,id_project:allProj}
			HWF.post({url:HWD.api.gets.projectsGet,data:data,fcn:function(inf){
				let listPr = inf.list;
				HWF.recount(listPr,function(el){
					let id = el.id;
					let name = el.name || '';
					let type = 'proj';
					let icon = el.project_type || '';

					allObj.push({
						id:id,
						name:name,
						type:type,
						icon:'icon_projects_'+icon,
					});
				});
				load_list_object();
			}});
			
		} else {load_list_object();}
		
		// обработка всех собранных данных
		function load_list_object(){
			iter++
			if(iter == 2){
				console.log(allObj);
				let blockObj = document.body.querySelector('#project-content-object-block');
				blockObj.innerHTML = '';
				
				lisrPrint = '<ul id="project-list-object" class="list-equipments">';
				lisrPrint+= '<li class="one-head sticky">\
					<div class="equ_n"><span class="block_mr_10 block_w_40p">Тип</span><span>Имя</span></div>\
					<div class="block_w_220p">Вид</div>\
					<div class="equ_b object_select_all user-none">Всё</div>\
				</li>';
				HWF.recount(allObj,function(elem){
					let tName = '';
					if (elem.type == 'proj'){tName = 'Проект'}
					if (elem.type == 'prog'){tName = 'Программа'}
					if (elem.type == 'equp'){tName = 'Оборудование'}
					
					lisrPrint+= '<li class="one-equipment" data-id="'+elem.id+'" data-type="'+elem.type+'">';
					lisrPrint+= '<div class="equ_n">\
						<span class="icon '+elem.icon+' block_mr_10"></span>\
						<span class="text_name vertical-middle">'+elem.name+'</span>\
					</div>';
					lisrPrint+= '<div class="block_w_220p">'+tName+'</div>';
					lisrPrint+= '<div class="equ_b"><div class="settings-butt"><div class="btn-active checkbox checkbox_list"></div></div></div>';
					lisrPrint+= '</li>';
				});
				lisrPrint+= '</ul>';
				lisrPrint+= '<div class="flex_between block_w_100 block_p_10 box-sizing-box">\
					<div></div>\
					<div id="btn-all-bot-object" class="open-clouse"><span class="style_btn p4 grey block_ml_6 btn_delete_acces">Удалить</span></div>\
				</div>';
				
				blockObj.innerHTML = lisrPrint;
				
				// выбор сазу всех элементов
				HWS.on('click','object_select_all',function(event,elem){
					HWF.allActive('.list-equipments','.checkbox');
					let testSheck = HWS.getElem('#project-list-object .one-equipment .checkbox_list.active');
					let btnBox = HWS.getElem('#btn-all-bot-object');
					if(testSheck){btnBox.classList.add('open');} else {btnBox.classList.remove('open');}
				});
				HWS.on('click','checkbox_list',function(e,el){
					let testSheck = HWS.getElem('#project-list-object .one-equipment .checkbox_list.active');
					let btnBox = HWS.getElem('#btn-all-bot-object');
					if(testSheck){btnBox.classList.add('open');} else {btnBox.classList.remove('open');}
				});
				HW.on('click','btn_delete_acces',function(e,el){HWF.modal.confirm('Удалить выбранные ?','Удаление','delete_object_from_project');});
				HW.on('click','delete_object_from_project',function(e,el){
					let ul = document.body.querySelector('#project-list-object');
					let allAct = ul.querySelectorAll('.one-equipment .equ_b .checkbox.active');
					let listEqup = [], listEqupNew = {};
					let listProj = [], listProjNew = {};
					let prda = info.project_data;
					let goGet = false;
					
					if(allAct.length > 0){ HWF.recount(allAct,function(elem){
						let li = HWS.upElem(elem,'.one-equipment');
						let id = li.dataset.id;
						let type = li.dataset.type;
						if(type == 'equp'){listEqup.push({id:id,html:li});}
						if(type == 'proj'){listProj.push({id:id,html:li});}
						li.remove();
					},'on'); }
					
					if(listEqup.length > 0){goGet = true; HWF.recount(listEqup,function(elem){
						if(prda.equipments[elem.id]){elem.html.remove(); delete prda.equipments[elem.id]}
					})}
					
					if(listProj.length > 0){goGet = true; HWF.recount(listProj,function(elem){
						if(prda.projects[elem.id]){elem.html.remove(); delete prda.projects[elem.id]}
					})}
					
					if(goGet) {HWF.post({url:HWD.api.gets.projChange,data:{token:token,id_project:projectID,project_data:prda}});}
				});
				
			}
		}
	};
	// Зфгрузка подстраници "ДОСТУПЫ"
	HWS.buffer.equpPageAccesses = function(){
		Sprint = '<div class="one-block nop ob-top conections">';
			Sprint+= '<div class="mobile-100 flex_center_left block_w_100 block_p_10">\
				<div title="Добавить" class="icon icon-add hover add_new_fr block_mr_10"></div>\
				<h5>Доступ</h5>\
			</div>';
			Sprint+= '<div id="list-proj-friends" class="block_w_100"></div>';
		Sprint+= '</div>';
		body.innerHTML = Sprint;
		
		HWF.printList({name:'printListMyEqupBuffer',url:HWD.api.gets.projListUser,target:'#list-proj-friends',
		data:{id_project:projectID},
		fcn:function(inf){
			let list = inf.list;
			let html = '<ul class="list block_pt_20">';
			
			if(list.length > 0){
				html+= '<li class="one-head sticky">';
				html+= '<div class="l-mail">Емайл</div>';
				html+= '<div class="l-name ta_left">Имя</div>';
				html+= '<div class="l-btn block_w_100p flex_between">\
					<div class="btn_text block_w_20p ta_center fr_select_r">r</div>\
					<div class="btn_text block_w_20p ta_center fr_select_w">w</div>\
					<div class="btn_text block_w_20p ta_center fr_select_x">x</div>\
					</div>';
				html+= '<div class="l-btn"></div>';
				html+= '</li>';
				HWF.recount(list,function(el){
					let security = el.security;
					let actR = (security[0] == 'r')? 'active' : '';
					let actW = (security[1] == 'w')? 'active' : '';
					let actX = (security[2] == 'x')? 'active' : '';

					html+= '<li class="one_fr" data-id="'+el.id_user+'">';
					html+= '<div class="l-mail">'+el.email+'</div>';
					html+= '<div class="l-name ta_left">'+el.name+'</div>';
					html+= '<div class="l-btn block_w_100p flex_between">\
					<div class="btn-active '+actR+' checkbox-empty click_acse click_acse_r">r</div>\
					<div class="btn-active '+actW+' checkbox-empty click_acse click_acse_w">w</div>\
					<div class="btn-active '+actX+' checkbox-empty click_acse click_acse_x">x</div>\
					</div>';
					html+= '<div class="l-btn ta_right"><div class="text-icon btn_fr_delete">✕</div></div>';
					html+= '</li>';
				});
				html+='</ul>';
			}
			else {
				html = '<div class="ta_center block_pt_20">Доступ к этому проекту есть только у Вас. Если хотите, поделитесь им с доверенными пользователями.</div>';
			}
			
			return html;
		}});
		
		// ** FUNCTIONS **
		function mass_rwx(){
			let allLi = document.body.querySelectorAll('#list-proj-friends .print-list section ul .one_fr');
			let dataD =[];
			if(allLi.length > 0){
				HWF.recount(allLi,function(li){
					let id = li.dataset.id;
					let r = li.querySelector('.click_acse_r');
					let w = li.querySelector('.click_acse_w');
					let x = li.querySelector('.click_acse_x');
					
					let security = '';
					(r.classList.contains('active'))? security+= 'r' : security+= '-';
					(w.classList.contains('active'))? security+= 'w' : security+= '-';
					(x.classList.contains('active'))? security+= 'x' : security+= '-';
					dataD.push({id_user:id,id_project:projectID,security:security});
				},'on');
				
				let data = {token:token,data:dataD}
				HWF.post({url:HWD.api.gets.projSetUser,data:data});
			}
		}
		
		// **АКТИВНОСТЬ**
		HWS.on('click','add_new_fr',function(){
			let print = '<div id="modal-fr-list" class="table-content block_w_100 block_h_90"></div>';
				print+= '<div class="style_btn orange block_w_200p add_more_fr_btn">Добавить</div><br><br>';
			HWF.modal.print('Добавить Пользователя',print);
			
			HWF.printList({name:'printListMyFrBuffer',url:HWD.api.gets.userFriendGet,target:'#modal-fr-list',
			clas:'block_w_100',
			fcn:function(inf){
				let list = inf.list;
				let html = '<ul class="list block_pt_20">';
				
				html+= '<li class="one-head sticky">';
				html+= '<div class="l-mail">Емайл</div>';
				html+= '<div class="l-name ta_left">Имя</div>';
				html+= '<div class="l-btn"><span class="btn_text fr_select_all">Всё</span></div>';
				html+= '</li>';
				HWF.recount(list,function(el){
					html+= '<li data-id="'+el.id+'">';
					html+= '<div class="l-mail">'+el.email+'</div>';
					html+= '<div class="l-name ta_left">'+el.name+'</div>';
					html+= '<div class="l-btn ta_right"><div class="btn-active checkbox"></div></div>';
					html+= '<span class="click click_one_fr_user"></span>';
					html+= '</li>';
				});
				html+='</ul>';
				
				return html;
			}});
		});
		
		// нажатие на 1го друга
		HWS.on('click','click_one_fr_user',function(e,el){
			let li = HWS.upElem(el,'li');
			let check = li.querySelector('.checkbox');
			if(check.classList.contains('active')){check.classList.remove('active')}
			else{check.classList.add('active')} 
		});
		// Нажатие на кнпку добавить
		HWS.on('click','add_more_fr_btn',function(e,el){
			let allActive = document.body.querySelectorAll('.modal-section .table-content .print-list .checkbox.active');
			let idList = []
			
			if(allActive.length > 0){HWF.recount(allActive,function(elem){
				let li = HWS.upElem(elem,'li');
				let id = li.dataset.id;
				idList.push({id_project:projectID,id_user:id,security:'r--'});
			},'on');}
			// если выбран хотябы 1 пользователь
			if(idList.length > 0){
				HWS.buffer.userAddFrProject = idList;
				HWF.modal.print('Добавить Пользователя',HWhtml.loader());

				let list =  info.project_data.equipments;
				let allEqup = [];
				let allProj = [];
				// РАспределение между "ОБОРУДОВАНИЕМ" и "ПРОЕКТАМИ"
				HWF.recount(list,function(el){if(el.type != "project"){allEqup.push(el.id);}});
				allEqup = HWF.unique(allEqup);
				// если есть "ОБОРУДОВАНИ"
				if (allEqup.length > 0){
					let dataGet = {token:token,list_start:0,list_amount:1000,id_object:allEqup};
					HWF.post({url:HWD.api.gets.list,data:dataGet,fcn:function(inf){
						let print = '<div id="modal-fr-list" class="table-content block_w_100 block_h_90">';
						let ul = '<ul id="add-equp-fr-list" class="list-equipments">'
							ul+= '<li class="one-head sticky">\
							<div class="equ_n"><span class="block_mr_10">Тип</span><span>Имя</span></div>\
							<div class="equ_d">Вид</div>\
							<div class="equ_b object_select_all user-none">Всё</div>\
						</li>';
						let listEq = inf.list;
						HWF.recount(listEq,function(el){
							let et = (el.info.type)? HWD.equp[el.info.type.type] || false : false;
							let id = el.id;
							let name = el.config.name || '';
							let type = et.type || 'equp';
							
							if (type == 'proj'){type = 'Проект'}
							if (type == 'prog'){type = 'Программа'}
							if (type == 'equp'){type = 'Оборудование'}
							
							ul+= '<li class="one-equipment" data-id="'+id+'">';
							ul+= '<div class="equ_n">\
								<span class="icon icon_equip_'+id+' block_mr_10"></span>\
								<span class="text_name vertical-middle">'+name+'</span>\
							</div>';
							ul+= '<div class="equ_d">'+type+'</div>';
							ul+= '<div class="equ_b"><div class="settings-butt"><div class="btn-active checkbox active"></div></div></div>';
							ul+= '<span class="click click_one_fr_user"></span>';
							ul+= '</li>';
						});
						ul+= '</ul>'
						print+= ul+'</div>'
						print+= '<div class="button share_equp_fr_btn">Добавить</div><br><br>';
						
						HWF.modal.print('Добавление доступа к оборудованию проекта',print);
					}});
				} else {
					HWF.modal.message('В проекте еще нет оборудования.','Права на оборудование');
					HWS.on('click','btn_confir_accept',function(e,el){HWS.buffer.printListMyEqupBuffer();});
				}
			}
		});
		//Добавление оборудования пользователю другу 
		HWS.on('click','share_equp_fr_btn',function(e,el){
			let allActive = document.body.querySelectorAll('#add-equp-fr-list .checkbox.active');
			let idList = [];
			let UserList = HWS.buffer.userAddFrProject;
			let spIter = 0;
			HWF.modal.print('Права на оборудование',HWhtml.loader());
			// ДОбавление друга
			HWF.post({url:HWD.api.gets.projSetUser,data:{token:token,data:UserList},fcn:load_complide});
			if(allActive.length > 0){
				HWF.recount(UserList,function(user){
					let idUser = user.id_user;
					HWF.recount(allActive,function(elem){
						let li = HWS.upElem(elem,'li');
						let id = li.dataset.id;
						idList.push({id_user:idUser,id_object:id,security:'r--'});
					},'on');
				});
				// ДОбавление Оборудования другу
				HWF.post({url:HWD.api.gets.objectsSecurity,data:{token:token,data:idList},fcn:load_complide});
			} else {load_complide();}
			
			function load_complide(){
				spIter++;
				if (spIter == 2){
					HWF.modal.message('Добавление выполнено','Права на оборудование');
					HWS.on('click','btn_confir_accept',function(e,el){HWS.buffer.printListMyEqupBuffer();});
				}
			}
		});
		
		//Удаление пользователя нажатие
		HWS.on('click','btn_fr_delete',function(e,el){
			let li = HWS.upElem(el,'li');
			let email = li.querySelector('.l-mail').innerHTML;
			let print = '';
			HWS.buffer.deleteLiFr = li;
			print = '<div class="confirm-modal"><div class="confirm-modal-block">'+
			'<div class="confirm-top">Удалить: <b>'+email+'</b>  ?</div>'+
			'<div class="btn-active style_btn delete_fr_accept">ДА</div>'+
			'<div data-target="modal-window" class="btn-no-modal btn-active style_btn">НЕТ</div>'+
			'</div></div>';
			HWF.modal.print('Удаление',print);
		});
		//Подтверждение удоления
		HWS.on('click','delete_fr_accept',function(e,el){
			let id = HWS.buffer.deleteLiFr.dataset.id;
			let list =  info.project_data.elements;
			let allEqup = [];
			// РАспределение между "ОБОРУДОВАНИЕМ" и "ПРОЕКТАМИ"
			HWF.recount(list,function(el){if(el.type != "project"){allEqup.push(el.id);}});
			allEqup = HWF.unique(allEqup);
			
			let data = {
				token:token,
				list_start:0,
				list_amount:1000,
				share_to_id_user:id,
				id_object:allEqup
			}
			HWF.modal.print('Права на оборудование',HWhtml.loader());
			HWF.post({url:HWD.api.gets.list,data:data,fcn:function(inf){
				let print = '<div id="modal-fr-list" class="table-content block_w_100 block_h_90">';
				let ul = '<ul id="add-equp-fr-list" class="list-equipments">'
					ul+= '<li class="one-head sticky">\
					<div class="equ_n"><span class="block_mr_10">Тип</span><span>Имя</span></div>\
					<div class="equ_d">Вид</div>\
					<div class="equ_b object_select_all user-none">Всё</div>\
				</li>';
				let listEq = inf.list;
				HWF.recount(listEq,function(el){
					let et = (el.info.type)? HWD.equp[el.info.type.type] || false : false;
					let id = el.id;
					let name = el.config.name || '';
					let type = et.type || 'equp';
					
					if (type == 'proj'){type = 'Проект'}
					if (type == 'prog'){type = 'Программа'}
					if (type == 'equp'){type = 'Оборудование'}
					
					ul+= '<li class="one-equipment" data-id="'+id+'">';
					ul+= '<div class="equ_n">\
						<span class="icon icon_equip_'+id+' block_mr_10"></span>\
						<span class="text_name vertical-middle">'+name+'</span>\
					</div>';
					ul+= '<div class="equ_d">'+type+'</div>';
					ul+= '<div class="equ_b"><div class="settings-butt"><div class="btn-active checkbox active"></div></div></div>';
					ul+= '<span class="click click_one_fr_user"></span>';
					ul+= '</li>';
				});
				ul+= '</ul>'
				print+= ul+'</div>'
				print+= '<div class="button del_equp_fr_btn">Удалить</div>';
				
				HWF.modal.print('Удаление доступа оборудования пректа',print);
			}});

			//HWS.buffer.deleteLiFr.remove();
		});
		HWS.on('click','del_equp_fr_btn',function(e,el){
			let allActive = document.body.querySelectorAll('#add-equp-fr-list .checkbox.active');
			let idUser = HWS.buffer.deleteLiFr.dataset.id;
			let spIter = 0;
			let delData = []
			
			function load_complide(){
				spIter++;
				if (spIter == 2){
					HWF.modal.message('Удаление выполнено','Удаление');
				}
			}
			
			if(allActive.length > 0){
				HWF.recount(allActive,function(elem){
					let li = HWS.upElem(elem,'li');
					let idObj = li.dataset.id;
					delData.push({id_user:idUser,id_object:idObj});
				},'on');
				HWF.post({url:HWD.api.gets.objectsSecurityDel,data:{token:token,data:delData},fcn:load_complide});
			} else {load_complide();}

			HWF.post({url:HWD.api.gets.projDelUser,data:{token:token,id_user:idUser,id_project:projectID},fcn:load_complide});

			HWS.buffer.deleteLiFr.remove();
		});
		
		// Нажатие на 1 вид прав
		HWS.on('click','click_acse',function(e,el){
			let li = HWS.upElem(el,'li');
			let id = li.dataset.id;
			let r = li.querySelector('.click_acse_r');
			let w = li.querySelector('.click_acse_w');
			let x = li.querySelector('.click_acse_x');
			
			let security = '';
			(r.classList.contains('active'))? security+= 'r' : security+= '-';
			(w.classList.contains('active'))? security+= 'w' : security+= '-';
			(x.classList.contains('active'))? security+= 'x' : security+= '-';
			
			let Ddata = [{id_user:id,id_project:projectID,security:security}];
			let data = {token:token,data:Ddata}
			HWF.post({url:HWD.api.gets.projSetUser,data:data});
		});
		
		// выбор сазу всех элементов
		HWS.on('click','object_select_all',function(){HWF.allActive('#add-equp-fr-list','.checkbox');});
		HWS.on('click','fr_select_all',function(){HWF.allActive('#modal-fr-list .print-list ul','.checkbox');});
		HWS.on('click','fr_select_r',function(){HWF.allActive('#list-proj-friends .print-list ul','.click_acse_r'); mass_rwx();});
		HWS.on('click','fr_select_w',function(){HWF.allActive('#list-proj-friends .print-list ul','.click_acse_w'); mass_rwx();});
		HWS.on('click','fr_select_x',function(){HWF.allActive('#list-proj-friends .print-list ul','.click_acse_x'); mass_rwx();});
	};
	
	HWS.buffer.equpPageSets();
	
	HW.on('click','form-radio',function(event,elem){
		let activeOff = document.querySelector('.form-radio.active');
		let data = elem.dataset.type;
		let img  = document.querySelector('.proj_img');
		if (activeOff){
			activeOff.classList.remove('active');
			activeOff.querySelector('.checkbox').classList.remove('active');
		}

		elem.classList.add('active');
		elem.querySelector('.checkbox').classList.add('active');
		
		if (data == 1){img.style.width = '264px';}
		else {img.style.width = '0px';}
	},{method:'m'});
	
	HW.on('click','checkbox_clouse_name',function(e,el){
		if (el.classList.contains('active')){proj.project_data.clouse_name = 0}
		else {proj.project_data.clouse_name = 1}
		
		let data = {token:HWS.pageAccess,id_project:proj.id,project_data:proj.project_data}
		HWF.post({url:HWD.api.gets.projChange,data:data});
	});
	
	function printInfoTop (proj){
		let id = proj.id;
		let name = proj.name;
		let pData = proj.project_data.clouse_name || 0;
			pData = (pData == 1)? '' : 'active' ;

		let active = {};
			active.main = '';
		
		if(proj.main_project){active.main = '<span class="tc_green">Главный</span>';}
		else {active.main = '<span class="set_main_proj btn-active on-off"></span>'}
		
		let html = '\
		<div class="one-block-col">\
			<div class="name-page">\
				<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
				<input id="name-project" class="btn_name_project input-name-page" type="text" value="'+name+'" placeholder="Название проекта">\
				<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
			</div>\
			<div id="error_name" class="name-page tc_red">Неверное имя</div>\
			<div class="block_w_100 ta_right">\
				<div class="block_w_260p display_inline ta_left">\
					<p class="block_pb_8">Имена объектов '+HWhtml.helper('Включить отображение имен объектов (датчиков, программ и т.д.)')+'</p>\
					<span class="checkbox_clouse_name btn-active on-off '+pData+'"></span>\
				</div>\
				<div class="block_w_260p display_inline ta_left">\
					<p class="block_pb_8">Сделать главным '+HWhtml.helper('Включить отображение этого проекта на главной странице')+'</p>\
					<p id="proj-main-btn-onof">'+active.main+'</p>\
				</div>\
			</div>\
		</div>';
		
		return html;
	}
	
	function printInfo (proj){
		let type = proj.project_type;
		let pData = proj.project_data.clouse_name || 0;
			pData = (pData == 1)? '' : 'active' ;
		let html = '';
		let active = {};
			active.type = {1:'',2:'',3:''};
			active.type[type] = 'active';
		let img = '';
		let imgLoader = '0px';
		
		
		
		if (type == 1){
			if(proj.project_data && proj.project_data.img && proj.project_data.img.src){img = '<img src="'+serverURL+HWD.api.imgDir+proj.project_data.img.src+'"/>';}
		}
		/*
		
		html = '\
			<div class="one-block ob-mid">\
				<h4>Вид</h4>\
				<div class="dop-info">\
					<div class="project-type">\
						<p data-type="1" class="form-radio '+active.type[1]+'"><span class="icon icon_projects_1"></span>Картинка<span class="checkbox '+active.type[1]+'"></span></p>\
						<p data-type="2" class="form-radio '+active.type[2]+'"><span class="icon icon_projects_2"></span>Карта<span class="checkbox '+active.type[2]+'"></span></p>\
						<p data-type="3" class="form-radio '+active.type[3]+'"><span class="icon icon_projects_3"></span>Доска<span class="checkbox '+active.type[3]+'"></span></p>\
						<p style="width:'+imgLoader+'" class="proj_img"><input class="new-file-project block_w_250p" title="Загрузите новую картинку" name="proj_img" type="file" id="filetoupload" accept=".jpg, .jpeg, .png, .gif"></p>\
					</div>\
					<div class="project-mini-img '+active.imgB+'">'+img+'</div>\
					<span class="tc_orange ts_08">*При изменении вида, все объекты внутри обнулятся</span>\
				</div>\
			</div>\
		';
		*/
		
		html = '<div class="one-block-col no-p-lr block-type-project">\
			<div class="title"><h4>Тип проекта '+HWhtml.helper(' Элементы проекта могут быть привязаны к координатам рисунка (фото), к географическим координатам карты или абстрактной сетке.')+'</h4></div>\
			<ul class="big-btn-type-proj">\
				<li class="'+active.type[1]+'" data-id="btn-content-img" data-type="1">\
					<span class="icon p20 icon_projects_1 block_mr_10"></span>\
					<span>Изображение</span>\
					<span class="click btn_type_proj"></span>\
				</li>\
				<li class="'+active.type[2]+'" data-id="btn-content-map" data-type="2">\
					<span class="icon p20 icon_projects_2 block_mr_10"></span>\
					<span>Карта</span>\
					<span class="click btn_type_proj btn_spec_type_map"></span>\
				</li>\
				<li class="'+active.type[3]+'" data-id="btn-content-grid" data-type="3">\
					<span class="icon p20 icon_projects_3 block_mr_10"></span>\
					<span>Доска</span>\
					<span class="click btn_type_proj"></span>\
				</li>\
			</ul>\
			<div id="btns-content-type" class="big-btn-conten-block">\
				<div id="btn-content-img" class="content-btn '+active.type[1]+'">\
					<div class="block_w_100 block_pb_20" id="img-container">'+img+'</div>\
					<div id="error_img" class="name-page tc_red">Изображение не выбрано </div>\
					<div class="btn-select-file">\
						<span class="btn">Заменить <input class="btn_load_img" name="proj_img" type="file" id="filetoupload"></span>\
						<span class="name"></span>\
					</div>\
					<p class="ts_14p tc_greyA tl_21p block_pt_20">\
						Тип проекта «Изображение» позволяет установить в качестве <br />\
						фона любое ваше изображение, например проект дома <br />\
						или схему участка.<br />\
						<br /> \
					</p>\
				</div>\
				<div id="btn-content-map" class="content-btn '+active.type[2]+'">\
					<div id="ecto-map"></div>\
					<p class="ts_14p tc_greyA tl_21p block_pt_20">\
						Тип проекта «Карта» позволяет установить в качестве <br />\
						фона выбранную зону на карте, например, ваш <br />\
						коттеджный поселок<br />\
					</p>\
				</div>\
				<div id="btn-content-grid" class="content-btn '+active.type[3]+'">\
					<div class="block_w_100 block_pb_20" id="img-container"><img src="/img/mmBack.jpg"></div>\
					<p class="ts_14p tc_greyA tl_21p block_pt_20">\
						Тип проекта «Доска» позволяет установить в качестве <br />\
						фона разлинованный фон, похожий на поля в тетради. <br />\
						На нем можно удобно расположить элементы. <br />\
					</p>\
				</div>\
			</div>\
			<div class="block_w_100 flex_between block_plr_20 tc_greyA ">\
				<p>\
					<b>ВАЖНО:</b> при изменении типа проекта, все объекты <br />\
					внутри него пропадут<br />\
				</p>\
				<!--span class="style_btn grey btn_save_project">Изменить</span-->\
			</div>\
		</div>';
		
		return html;
	}
	
		//Спец нажатие "СОЗДАТЬ КАРТУ"
	function create_map(){
		if(!HWS.buffer.createObjectMap){
			// Создание карты
			let wievMap = {lat:60.539811,lng:102.2617187,zoom:4};
			
			if (objSet.zoom){
				wievMap.lat = objSet.zoom.lat || 60.539811;
				wievMap.lng = objSet.zoom.lng || 102.2617187;
				wievMap.zoom = objSet.zoom.zoom || 4;
			}
			
			let map = L.map('ecto-map');
				map.options.crs = L.CRS.EPSG3395;
				map.setView([wievMap.lat,wievMap.lng],wievMap.zoom);
				HWS.buffer.createObjectMap = map;
			
			L.tileLayer(
			  'http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
				subdomains:['mt0','mt1','mt2','mt3'],
				reuseTiles:true,
				updateWhenIdle:false,
			  }
			).addTo(map);
		}
	}
	
	//Заполнение блоков контентом
	B.menud.append(menu);
	
	//ACTIVITI
	//Кнопка сделать главным
	HW.on('click','set_main_proj',function(e,el){
		let block = HWS.getElem('#proj-main-btn-onof');
			block.innerHTML = '<span class="tc_green">Главный</span>'
			
		HWF.post({url:HWD.api.gets.projChangeMain,data:{token:token,id_project:projectID}});
	});
	
	//Нажатие на кнопку ТИП проекта
	HW.on('click','btn_type_proj',function(e,el){
		let li = HWS.upElem(el,'li');
		let ul = HWS.upElem(li,'ul');
		let idOn = li.dataset.id;
		let BlockCont = HWS.getElem('#btns-content-type');
		let blockOn = BlockCont.querySelector('#'+idOn);
		let liActive = ul.querySelector('li.active');
		let BlockActive = BlockCont.querySelector('.content-btn.active');
		
		liActive.classList.remove('active');
		BlockActive.classList.remove('active');
		li.classList.add('active');
		blockOn.classList.add('active');
		objSet.type = li.dataset.type;
		
		if(idOn == 'btn-content-map'){create_map();}
		
		proj.project_data.equipments = {};
		HWF.post({url:HWD.api.gets.projChange,data:{token:token,id_project:projectID,project_data:proj.project_data},fcn:function(inf){console.log(inf)}});
		HWF.post({url:HWD.api.gets.projChangeType,data:{token:token,id_project:projectID,type:objSet.type},fcn:function(inf){console.log(inf)}});
	});
	
	// Изменение кнопки "ВЫБРАТЬ КАРТИНКУ"
	HW.on('change','btn_load_img',function(e,el){
		if(e.target.files.length > 0){
			let block = HWS.upElem(el,'.btn-select-file');
			let blockName = block.querySelector('.name');
			let imgBlock = HWS.getElem('#img-container'); imgBlock.innerHTML = '';
			let file = e.target.files[0];
			let error = HWS.getElem('#error_img'); error.classList.remove('active');
			
			URL.revokeObjectURL(HWS.buffer.urlImgObjectFile);
			HWS.buffer.urlImgObjectFile = URL.createObjectURL(file);
			objSet.img = file;
			blockName.innerHTML = file.name;
			imgBlock.innerHTML = '<img src="'+HWS.buffer.urlImgObjectFile+'">';
			
			HWF.form({url:'upload/image',file:objSet.img,data:{token:token},fcn:function(info){
				let newData = proj.project_data || {};
				newData.img = {src:info.path_with_file,id:info.id_image}; proj.project_data = newData;				
				HWF.post({url:HWD.api.gets.projChange,data:{token:token,id_project:projectID,project_data:newData}});
			}});
		}
	});
	// Изменение имени
	HW.on('focusin','btn_name_project',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"};
	});
	HW.on('focusout','btn_name_project',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"};
	});

	HW.on('change','btn_name_project',function(e,el){ 
		objSet.name = el.value 
		//let nameRegex = new RegExp(HWD.reg.name);
		let name = el;
		
		//if (nameRegex.test(name.value)){
		if (name.value){
			objSet.name = el.value; name.classList.remove('red');
			HWF.post({url:HWD.api.gets.projChangeName,data:{token:token,id_project:projectID,name:objSet.name}});
		}
		else {name.classList.add('red');}
		
		
		el.blur();
	});
	
	// 
	HW.on('click','btn_save_project',function(e,el){
		console.log(objSet);
	});
	
});
