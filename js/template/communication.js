"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1>Связь</h1>';
	let content = '<div id="communication" class="main-block ob-center max-1200 info-page"></div>';
	let token = HWS.pageAccess;
	let pageA = H.hash[1] || 'list';
	let menu = '<div>';
	menu += '<a class="btn active" href="#communication/list">Список</a>';
	menu += '</div>';
	menu += '<span class="add_new_system btnIcon">\
			<span class="select-none">Добавить систему</span>\
			<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>\
		</span>';
	HWS.buffer.listSystem = {}

	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);

	let contentB = document.body.querySelector('#communication');
	
	//** lOGICS **
	if(pageA == 'list') {load_list()}
	if(pageA == 'not') {load_not()}
	//** -lOGICS- **
	
	//** FCN **
	//Загрузка
	function load_list(){
		let print = HWhtml.tile('hw-list-info-block system-list');
		contentB.innerHTML = '\
			<div id="all-info-content-top" class="one-block-col">'+
				'<div class="flex_between block_w_100">'+
					'<div>Период: '+
						HWhtml.select({1:'1 час',3:'3 часа',12:'12 часов',24:'24 часа',168:'7 дней',336:'14 дней'},'selector_time_graph block_w_100p ')+
						' '+HWhtml.helper(HWD.helpText.pages.communicationTime)+
					'</div>'+
					HWhtml.search()+
					'<div class="block_w_150p"></div>'+
				'</div>'+
			'</div>'+
			'<div id="all-info-content" class="one-block nop"></div>';
			
		//		blcTop.innerHTML = ;
			
		let blcTop = document.body.querySelector('#all-info-content-top');
		let blcMid = document.body.querySelector('#all-info-content');
		
		print.top.innerHTML = '\
			<span class="name"><span class="block_mr_10 block_w_30p">Тип</span><span>Имя '+HWhtml.helper(HWD.helpText.pages.communicationName)+'</span></span>\
			<span class="equp">id '+HWhtml.helper(HWD.helpText.pages.communicationID)+' </span>\
			<span class="conect">Связь '+HWhtml.helper(HWD.helpText.pages.communicationConnection)+'</span>\
			<!--span class="send">Уведомлять, если связь с ЛК '+HWhtml.helper(HWD.helpText.pages.communicationMesConnect)+'</span-->\
			<span class="btns">График '+HWhtml.helper(HWD.helpText.pages.communicationGraph)+'</span>\
			<span class="btns"></span>';
		print.mid.innerHTML = '<ul class="list"><li>'+HWhtml.loader()+'</li></ul>'
		print.bot.innerHTML = '<div class="flex_between">\
			<div id="pagin-list-system" class="display_inline"></div>\
			<!--div class="block_w_240p">\
				<div class="display_inline">\
					<span class="icon icon-email block_pl_10" title="Email"></span>\
					<span class="vertical-middle">Email</span>\
				</div>\
				<div class="display_inline block_pl_10">\
					<span class="icon icon-push block_pl_10" title="Email"></span>\
					<span class="vertical-middle">Push на телефон</span>\
				</div>\
			</div>--\
		</div>';
		blcMid.append(print.b);
		
		HWS.buffer.listSystem = function(num){ num = num || 1;
			let pagin = document.body.querySelector('#pagin-list-system');
			let lAmount = localStorage.getItem('AllPageAmount') || 10;	lAmount = parseInt(lAmount);
			let lStart = num.start || 0;
			
			print.mid.innerHTML = HWhtml.loader();
			
			function print_gsmwifi_icon(elemWG,cfgLk){	
				console.log(elemWG);
				let type = elemWG.type;
				let clasStatus = elemWG.use_inet;
				let signal = elemWG.level;
				let roam = (elemWG.roam)?'-roam':'';

				if(clasStatus == 'connect'){clasStatus = 'green';}
				if(clasStatus == 'disconnect'){clasStatus = 'red';}
				if(clasStatus == 'not_use'){clasStatus = 'of';}
				let circle = '<span class="spec-circle-icon-state '+clasStatus+'"></span>';
				let iconColor = '';
				
				if(cfgLk == 1 && type == 'wifi'){circle=''; iconColor = 'grey'}
				if(cfgLk == 2 && type == 'gsm'){circle=''; iconColor = 'grey';}
				
				let print = ''+
					'<span class="select-none position_relative block_mr_10">'+
						circle+
						'<span class="icon icon-'+type+roam+'-'+signal+' '+iconColor+'"></span>'+
					'</span>';
				return print;
			};
			
			let data = {token:token,list_start:lStart,list_amount:lAmount}
			HWF.post({url:HWD.api.gets.objectsList3xSystem,data:data,fcn:function(info){
				let lCount = info.item_count || 0;	
				let list = info.list || false; console.log(list);
				let echo = '<ul class="list">'	
				HWF.recount(list,function(obj){
					HWS.buffer.listSystem[obj.id] = obj;
					let email = (obj.alert_to_mail)?'active':'';
					let push = (obj.push_alert)?'active':'';
					let telega = (obj.alert_to_telegram)?'active':'';
					let alerti = obj.alert_type || 0;
					let selectInf = {
						0:{name:'не уведомлять'},
						1:{name:'Потеряна'},
						2:{name:'Восстановлена'},
						3:{name:'Любой статус'},
					}
					selectInf[alerti].selected = 'selected';
					let connection = obj.connection;
					let wifiM = {} ; wifiM.id = obj.id_wifi || '';
					let gsmM = {}; gsmM.id = obj.id_gsm || '';
					let printIcon = '';
					let S = {};
					
					HWF.recount(connection,function(eleC){
						if(eleC.type == "wifi"){
							wifiM.id = eleC.id;
							wifiM.signal_level = eleC.level || 0;
							printIcon+= print_gsmwifi_icon(eleC,obj.config.cfg_lk);
							S.wifiUse = (eleC.use_inet == 'connect')?true:false;
						}
						if(eleC.type == "gsm"){
							gsmM.id = eleC.id;
							gsmM.signal_level = eleC.level || 0;
							printIcon+= print_gsmwifi_icon(eleC,obj.config.cfg_lk);
							S.gsmUse = true;//(eleC.use_inet != 'not_use')?true:false;
						}
					});
					
					if(alerti == 0) {S.btnsIcon = 'of';}
					else {S.btnsIcon = '';}
					let share_status = (obj.lk)?obj.lk.share_status || '':'';
					if (share_status &&  share_status  == 1){share_status = '';}
					if (share_status &&  share_status  == 2){share_status = '<span class="icon icon-share-down btn_helper block_mr_10" data-text="'+HWD.helpText.pages.shareStatusIconDown+'"></span>';}
					if (share_status &&  share_status  == 3){share_status = '<span class="icon icon-share-up btn_helper block_mr_10" data-text="'+HWD.helpText.pages.shareStatusIconUp+'"></span>';}
						
					S.type = obj.cloud_type;
					S.nameHtml = '<a class="tc_black" href="#equipment/'+obj.id+'">'+obj.config.name+'</a>';
					S.idHtml = '<a class="tc_black" href="#equipment/'+obj.id+'">'+obj.info.ident+'</a>';
					S.btnGraphHtml = '<div class="icon icon-graph hover btn_system_graph"></div>';
					S.btnSetsHtml = '<a class="icon icon-settings" href="#equipment/'+obj.id+'"></a>';
					S.btnSendHtml = ''+
						HWhtml.select(selectInf,'select_alert_type block_w_140p block_p_4 block_mr_8')+': '+
						'<div class="btns-icons display_inline '+S.btnsIcon+'">'+
						'<div class="icon icon-email hover block_pl_10 btn_system_mail '+email+'" title="Email"></div>'+
						'<div class="icon icon-push hover block_pl_10 btn_system_push '+push+'" title="Push"></div>'+
						'</div>';
					
					if(S.type == 14719){S.btnGraphHtml = ''; S.btnSendHtml = ''; printIcon = obj.lk.state_name || '';}

					echo+= '<li class="one-elem" data-id="'+obj.id_system+'" data-iddel="'+obj.id+'">';
					echo+= '<div class="info flex_between block_w_100">';
					echo+= '<div class="name">\
						<span class="icon p30 icon_equip_'+S.type+' block_mr_10"></span>\
						'+share_status+'\
						<span class="text-name tw_700">'+S.nameHtml+'</span>\
					</div>';
					echo+= '<div class="equp tw_700">'+S.idHtml+'</div>';
					echo+= '<div class="conect">'+
						printIcon+
					'</div>';

					echo+= '<!--div class="send">'+S.btnSendHtml+'</div-->';
					echo+= '<div class="btns ta_center">'+S.btnGraphHtml+'</div>';
					echo+= '<div class="btns ta_right">'+
						S.btnSetsHtml+
						'<span class="icon icon-delete hover btn_system_delete block_ml_8"></span>'+
					'</div>';
					echo+= '</div>';
					
					if(wifiM.id && S.wifiUse){
						S.wifi = '<div id="miniGraph'+wifiM.id+'" class="graph-wifi mini-graph" data-id="'+wifiM.id+'">'+
							'<div class="graph-top ta_left block_p_8"> <span class="icon block_mr_10 icon-wifi-'+wifiM.signal_level+'"></span> Wi-Fi:</div>'+
							'<div class="graph"></div>'+
						'</div>';
						S.gsmWidth = '';
					} else {S.wifi = ''; }
					
					if (gsmM.id && S.gsmUse){
						S.gsm = ''+
							'<div id="miniGraph'+gsmM.id+'" class="graph-gsm mini-graph" data-id="'+gsmM.id+'">'+
								'<div class="graph-top ta_left block_p_8"><span class="icon block_mr_10 icon-gsm-'+gsmM.signal_level+'"></span> GSM:</div>'+
								'<div class="graph"></div>'+
							'</div>';
					} else {S.gsm = '';}
					
					echo+= '<div class="graph-info">'+
						S.gsm+
						S.wifi+
					'</div>';
					echo+= '</li>';
				});
				echo+= '</ul>';
				
				print.mid.innerHTML = echo;
				
				if(num == 1){pagin.innerHTML = HWhtml.pagination({number:num,amount:lAmount,all:lCount,type:'buffer',load:'listSystem'});}
			}});
		}
		
		HWS.buffer.listSystem();
			
		HW.on('click','btn_system_graph',function(e,el){
			let timeR = document.body.querySelector('.selector_time_graph').value;
			let time = parseInt(timeR);
			let ul = HWS.upElem(el,'ul');
			let openLi = ul.querySelector('.one-elem.graph');
			let oneElem = HWS.upElem(el,'.one-elem');
			let system = HWS.buffer.listSystem[oneElem.dataset.iddel] || '';
			let wifiM = oneElem.querySelector('.graph-wifi');
			let gsmM = oneElem.querySelector('.graph-gsm');
			let gTime = system.system_time || Date.now();
			let sTime = gTime - (time*3600000);
			let eTime = gTime;
			let send = {};
			send.token = token;
			send.timestamp_start = sTime,
			send.timestamp_end = eTime
			send.charts = [];

			if(oneElem.classList.contains('graph')){clouseLi(oneElem);}
			else {
				if(openLi){clouseLi(openLi);}
				oneElem.classList.add('graph');
				
				if(gsmM){
					gsmM.querySelector('.graph').innerHTML = HWhtml.loader();
					send.object_id = gsmM.dataset.id;
					HWF.post({url:"history/chart_value_data",data:send,fcn:load_graph_conect_system});
				}
				
				if(wifiM){
					wifiM.querySelector('.graph').innerHTML = HWhtml.loader();
					send.object_id = wifiM.dataset.id;
					HWF.post({url:"history/chart_value_data",data:send,fcn:load_graph_conect_system});
				}
			}
			
			function clouseLi(OneLi){
				let wifi = OneLi.querySelector('.graph-info .graph-wifi');
				let gsm = OneLi.querySelector('.graph-info .graph-gsm');
				let idWifi = (wifi)?wifi.dataset.id:0;// miniGraph4225450
				let idGsm = (gsm)?gsm.dataset.id:0;// miniGraph4225450
				if(!HWS.buffer.mini_analog_graph){HWS.buffer.mini_analog_graph = {}}; 
				if(HWS.buffer.mini_analog_graph[idWifi]){
					HWS.buffer.mini_analog_graph[idWifi].dispose();
					HWS.buffer.mini_analog_graph[idWifi] = null;
				}
				if(HWS.buffer.mini_analog_graph[idGsm]){
					HWS.buffer.mini_analog_graph[idGsm].dispose();
					HWS.buffer.mini_analog_graph[idGsm] = null;
				}
				OneLi.classList.remove('graph');
			}
		});
		
		//Изменение вида оповещения 
		HW.on('change','select_alert_type',function(e,el){
			let oneElem = HWS.upElem(el,'.one-elem');
			let id = oneElem.dataset.id;
			let val = parseInt(el.value);
			let btnsIcon = oneElem.querySelector('.send .btns-icons');
			let data = {token:token,list:[{id_system:id,alert_type:val}]}
			HWF.post({url:HWD.api.gets.changeMesag3xSystem,data:data,fcnE:HWF.getServMesErr,fcn:function(info){}});
			
			if(val == 0){btnsIcon.classList.add('of');} 
			else {btnsIcon.classList.remove('of');}
		});
		
		// Email 
		HW.on('click','btn_system_mail',function(e,el){
			let oneElem = HWS.upElem(el,'.one-elem');
			let id = oneElem.dataset.id;
			if(el.classList.contains('active')){
				let data = {token:token,list:[{id_system:id,alert_to_mail:0}]}
				HWF.post({url:HWD.api.gets.changeMesag3xSystem,data:data,fcnE:HWF.getServMesErr,fcn:function(info){}});
				el.classList.remove('active');
			}
			else {
				let data = {token:token,list:[{id_system:id,alert_to_mail:1}]}
				HWF.post({url:HWD.api.gets.changeMesag3xSystem,data:data,fcnE:HWF.getServMesErr,fcn:function(info){}});
				el.classList.add('active');
			}
		});
		// Push
		HW.on('click','btn_system_push',function(e,el){
			let oneElem = HWS.upElem(el,'.one-elem');
			let id = oneElem.dataset.id;
			if(el.classList.contains('active')){
				let data = {token:token,list:[{id_system:id,push_alert:0}]}
				HWF.post({url:HWD.api.gets.changeMesag3xSystem,data:data,fcnE:HWF.getServMesErr,fcn:function(info){}});
				el.classList.remove('active');
			}
			else {
				let data = {token:token,list:[{id_system:id,push_alert:1}]}
				HWF.post({url:HWD.api.gets.changeMesag3xSystem,data:data,fcnE:HWF.getServMesErr,fcn:function(info){}});
				el.classList.add('active');
			}
		});
		// Telega
		HW.on('click','btn_system_telegram',function(e,el){
			let oneElem = HWS.upElem(el,'.one-elem');
			let id = oneElem.dataset.id;
			if(el.classList.contains('active')){
				let data = {token:token,list:[{id_system:id,alert_to_telegram:0}]}
				HWF.post({url:HWD.api.gets.changeMesag3xSystem,data:data,fcnE:HWF.getServMesErr,fcn:function(info){}});
				el.classList.remove('active');
			}
			else {
				let data = {token:token,list:[{id_system:id,alert_to_telegram:1}]}
				HWF.post({url:HWD.api.gets.changeMesag3xSystem,data:data,fcnE:HWF.getServMesErr,fcn:function(info){}});
				el.classList.add('active');
			}
		});
	}
	//Загрузка Помощьи
	function load_not(){contentB.innerHTML = ''; contentB.innerHTML = 'Уведомления'}
	
	// Постороит график по указаной ДАТЕ в указанный ТАРГЕТ
	function load_graph_conect_system(info){
		let id = info.id_object;
		let data = info.list;
		if(!HWS.buffer.mini_analog_graph){HWS.buffer.mini_analog_graph = {}}; 
		let dataG = [], graph = [];
		let block = document.body.querySelector('#miniGraph'+id+' .graph');
		HWF.recount(data,function(t){
			dataG.push({x:t.timestamp,y:t.value});
		});
		if(HWS.buffer.mini_analog_graph[id]){
			HWS.buffer.mini_analog_graph[id].dispose();
			HWS.buffer.mini_analog_graph[id] = null;
		}
		
		// Create chart instance
		am4core.useTheme(am4themes_animated);
		let chart = am4core.create(block, am4charts.XYChart);
			chart.cursor = new am4charts.XYCursor();	
			chart.data  = dataG;
			chart.dateFormatter.utc = true;
			
		let xAxes = chart.xAxes.push(new am4charts.DateAxis());

		// Create value axis
		let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
			yAxis.min  = 0;
			yAxis.max  = 100;
			yAxis.renderer.labels.template.adapter.add("text", function(text) {
				return parseFloat(text).toFixed(0)+'%';
			});

		// Create series
		let series = chart.series.push(new am4charts.LineSeries());
		series.dataFields.valueY = "y";
		series.dataFields.dateX = "x";
		series.stroke = am4core.color(HWD.color.green);
		series.fill = am4core.color(HWD.color.green);
		series.strokeWidth = 1;
		series.tooltipText = "{valueY}";
		series.fillOpacity = 0.2;
		
		chart.scrollbarX = new am4core.Scrollbar();
		HWS.buffer.mini_analog_graph[id] = chart;
	}
	
	//** -FCN- **
	
	//** ACTION **
	//Нажатие на удолить систему
	HW.on('click','btn_system_delete',function(e,el){
		HWS.buffer.liDeleteSystem = HWS.upElem(el,'li');
		let infoText = 'Ситстема и все ее оборудование <br> будут удалены из Вашего  аккаунта. Вы уверены?';
		HWF.modal.confirm('Удалить систему ?',infoText,'btn-delete-system-modal','Удалить');
		console.log(HWS.buffer.liDeleteSystem);
	});
	// Нажатие "да" в модальном окне
	HW.on('click','btn-delete-system-modal',function(e,el){
		let id = HWS.buffer.liDeleteSystem.dataset.iddel;
		console.log('click');
		HWF.post({url:HWD.api.gets.objectsDelete,data:{token:token,id_objects:[id]},fcn:function(info){
			console.log(info);
			HWF.modal.of();
		}});
		HWS.buffer.liDeleteSystem.remove();
	});
	
	//Общее изменение графиков
	HW.on('change','selector_time_graph',function(e,el){		
		let allOpen = document.querySelector('.one-elem.graph')
		if(allOpen){
			let Idsystem = allOpen.dataset.iddel || '';
			let system = HWS.buffer.listSystem[Idsystem] || '';
			let time = parseInt(el.value);
			let gTime = system.system_time || Date.now();
			let sTime = gTime - (time*3600000);
			let eTime = gTime;
			let allId = [];
			let send = {};
			send.token = token;
			send.timestamp_start = sTime,
			send.timestamp_end = eTime
			send.charts = [];
			
			//HWF.recount(allOpen,function(elem){
			let wifi = allOpen.querySelector('.graph .graph-wifi');
			let wifiID = false, wifiG = false;
			if(wifi){wifiID = wifi.dataset.id; wifiG = wifi.querySelector('.graph');}
			
			let gsm = allOpen.querySelector('.graph .graph-gsm');			
			let gsmG = gsm.querySelector('.graph');
			let gsmID = gsm.dataset.id;
			
			if(!HWS.buffer.mini_analog_graph){HWS.buffer.mini_analog_graph = {}}; 
			if(gsmID){
				if(HWS.buffer.mini_analog_graph[gsmID]){
					HWS.buffer.mini_analog_graph[gsmID].dispose();
					HWS.buffer.mini_analog_graph[gsmID] = null;
				}
				gsmG.innerHTML = HWhtml.loader();
				send.object_id = gsmID;
				HWF.post({url:"history/chart_value_data",data:send,fcn:load_graph_conect_system});
			}
			
			if(wifiID){
				if(HWS.buffer.mini_analog_graph[wifiID]){
					HWS.buffer.mini_analog_graph[wifiID].dispose();
					HWS.buffer.mini_analog_graph[wifiID] = null;
				}
				wifiG.innerHTML = HWhtml.loader();
				send.object_id = wifiID;
				HWF.post({url:"history/chart_value_data",data:send,fcn:load_graph_conect_system});
			}
		}
	});
	
	// Нажатие на добавить систему
	HW.on('click','add_new_system',function(e,el){HWF.printModalAddSystem();});	

	// Выделяет активный пукт дашбоардного меню, присваевает класс "актив" если 2й тег соответствует 2му тегу ссылки
	HWF.activeDasboartMenu();
	//** -ACTION- **
});