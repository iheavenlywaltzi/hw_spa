"use strict";
HWS.templateAdd(function(HW,B,H){
	let title = '<h1 class="test_page">Доверенные пользователи</h1>';
	let token = HWS.pageAccess;
	let dopTop = HWhtml.tile('one-block list-friends-top');
	let dopB = HWhtml.tile('one-block nop list-friends');
	let content = document.body.querySelector('.user-page');
	let idUser = H.hash[2] || false;
	
	dopTop.mid.classList.add('flex_between');
	//dopTop.mid.innerHTML = HWhtml.search();
	
	/*dopTop.mid.innerHTML = '\
	<div class="flex_center_left">\
		<span class="btnIcon new_user_add">\
			<span class="select-none">Добавить пользователя</span>\
			<span class="spec-img-add select-none">\
				<span class="icon icon-add select-none" title="Добавить"></span>\
			</span>\
		</span>\
	</div>'+
	HWhtml.search();
	*/
	
	dopB.top.innerHTML = '';
	dopB.mid.innerHTML = '';
	dopB.bot.innerHTML = '';
	
	content.innerHTML = '';
	content.appendChild(dopTop.b);
	content.appendChild(dopB.b);

	B.title.append(title);
	//B.content.append(content);
	
	//load_page
	HWS.buffer.printListBuffer = function(num){ num = num || 1;
		let listB = document.body.querySelector('.list-friends section');
		let pagin = document.body.querySelector('.list-friends footer');
		let lAmount = localStorage.getItem('AllPageAmount') || 10;
		let lStart = num.start || 0;
		let data = {token:token,list_start:lStart,list_amount:lAmount}
		
		listB.innerHTML = HWhtml.loader();
		
		HWF.post({url:HWD.api.gets.userFriendGet,data:data,fcn:function(info){
			let lCount = info.item_count || 0;
			let list = info.list;
			let listI = '<ul class="list">';
			
			listI+= '<li class="one-head sticky">';
			listI+= '<div class="l-mail">Электронная почта</div>';
			listI+= '<div class="l-name ta_left">Имя</div>';
			listI+= '<div class="l-btn"></div>';
			listI+= '</li>';
			HWF.recount(list,function(el){
				listI+= '<li data-id="'+el.id+'">';
				listI+= '<div class="l-mail">'+el.email+'</div>';
				listI+= '<div class="l-name ta_left">'+el.name+'</div>';
				listI+= '<div class="l-btn ta_right"><div class="text-icon ib-clouse delete_friends block_plr_8">✕</div></div>';
				//listI+= '<a href="/#user/friends/'+el.id+'" class="click"></a>';
				listI+= '</li>';
			});
			listB.innerHTML = listI+'</ul>';

			if(num == 1){pagin.innerHTML = HWhtml.pagination({number:num,amount:lAmount,all:lCount,type:'buffer',load:'printListBuffer'});}
		}});
	}
	
	if(idUser) {HW.load('friend');}
	else {HWS.buffer.printListBuffer();}
	
	HW.on('click','new_user_add',function(){
		
		let inpSum = '<div class="mini-modal-wiev">'+
			'<div class="str nop">'+
				'<div>Email</div>'+
				'<div><input id="user_mail_send" type="text"></div>'+
			'</div>'+
			'<span id="info-mesage-er-add-friend" class="block_w_100 ts_08 tc_red"></span>'+
			'<span id="info-mesage-ok-add-friend" class="block_w_100 ts_08 tc_green"></span>'+
			'<div class="str top">'+
				'<div class="block_pt_8">Сообщение</div>'+
				'<div><textarea id="user_mesaje_send" rows="5"></textarea></div>'+
			'</div>'+
		'</div>';
		
		HWF.modal.confirmCust({
			title:'Пригласить пользователя',
			print:inpSum,
			yes:'Пригласить',
			classYes:'btn_add_user_modal',
			clouseYes:'no'
		});
	});
	
	//отправка запроса на добавление в друзья
	HW.on('click','btn_add_user_modal',function(){
		let mail = document.body.querySelector('#user_mail_send');
		let mesage = document.body.querySelector('#user_mesaje_send');
		let error = document.body.querySelector('#info-mesage-er-add-friend');
		let oksend = document.body.querySelector('#info-mesage-ok-add-friend');
		let pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
		let controlSend = true;
		
		error.innerHTML = '';
		oksend.innerHTML = '';
		
		if (!pattern.test(mail.value)){error.innerHTML = 'Неверно введён Емаил'; controlSend = false;}
		
		if (controlSend){
			oksend.innerHTML = HWhtml.loader();
			let data = {token:token,email:mail.value,message:mesage.value}
			console.log(data);
			HWF.post({url:'user/set_request_for_add_friendly_user',data:data,fcn:function(inf){
				HWF.push('Приглашение новому пользователю отправлено','green');
				HWF.modal.of();
				error.innerHTML = '';
				console.log(inf);
			},fcnE:function(inf){
				oksend.innerHTML = '';
				error.innerHTML = HWF.getServMesErr(inf);
			}});
		}
		
	});
	
	//Удаление дуга
	HW.on('click','delete_friends',function(e,el){
		let li = HWS.upElem(el,'li'); console.log(li)
		let name = li.querySelector('.l-mail').innerHTML
		HWS.buffer.liDelUser = li;
		HWF.modal.confirm('Удаление пользователя из доверенных','Удалить пользователя <b>'+name+'</b> из списка доверенных?','acept_delete_user');
		//HWF.modal.confirm('<span class="tc_grey8">Удалить</span> '+name+' ?','Удаление','acept_delete_user');
	});
	HW.on('click','acept_delete_user',function(){
		let id = HWS.buffer.liDelUser.dataset.id;
		HWF.post({url:HWD.api.gets.userFriendDel,data:{token:token,id_user:id},fcn:function(info){}})
		HWS.buffer.liDelUser.remove();
	});
});