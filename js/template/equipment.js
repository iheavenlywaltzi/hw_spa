 "use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1>Элемент Оборудования</h1>';
	let sbc = document.createElement('div'); // spec block content
	let contentTop = document.createElement('div');
		contentTop.classList.add('main-block'); contentTop.classList.add('max-1200');
		contentTop.classList.add('no-P-bot');
		contentTop.id = 'equp-content-top';
	let contentBot = document.createElement('div');
		contentBot.classList.add('main-block'); 
		contentBot.classList.add('max-1200');
		contentBot.classList.add('no-p-top');
		contentBot.id = 'equp-content-mid';
		contentBot.innerHTML = HWhtml.loader();
	let noConnect = document.createElement('div');
		noConnect.classList.add('no-connect-block');
		noConnect.id = 'no-connect-block';
	let id = H.hash[1];
	let token = HWS.pageAccess;
	let G={},print={};
	HWS.buffer.timeValueGraph = 1;
	//Изменение имени
	HWS.buffer.nameFocus = false;
	
	sbc.append(contentTop);
	sbc.append(contentBot);
	sbc.append(noConnect);
	B.content.append(sbc);
	B.title.append(title);

	// запросы информации
	HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:load_page});
	
	// функции для работы
	function load_page(info){ console.log(info);
		G.elem = info;
		HWS.buffer.elem = info;
		HWS.buffer.system_time = info.system_time || false;
		if(info.web_config){HWS.buffer.timeValueGraph = info.web_config.time_per_graph || 1;}
		let type =  info.cloud_type || info.info.type.type;
		let connect = false; 
		
		if(HWF.objKeyTest('system_connection_status',info)){connect = info.system_connection_status;}
		HW.load('equipment/'+HWD.equp[type].tmp); // вызов соответствующего шаблона
		if(!connect && G.elem.cloud_type != 14719) {noConnect.classList.add('on')}
		if(
			G.elem.cloud_type == 14720 || 
			G.elem.cloud_type == 14721 || 
			G.elem.cloud_type == 14722 || 
			G.elem.cloud_type == 14723 || 
			G.elem.cloud_type == 14724
		){noConnect.innerHTML = '';}
		else {noConnect.innerHTML = '<div class="btn_no_connect tf_fr900 ts_24p btn_text black">Система этого объекта не на связи с ЛК. Почему???</div>';}
	}
	
	// ОБЩИЕ Функции
	HWS.buffer.share_stick = function(info){ //показывает стикер расшарки
		let blockShare =  document.createElement('div');
			blockShare.classList.add('block_share_sticer');
		let share_status = info.lk.share_status || '';
		
		if (share_status &&  share_status  == 1){share_status = '';}
		if (share_status &&  share_status  == 3){share_status = '<span class="sticer-share-up btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconUp+'"></span>';}
		if (share_status &&  share_status  == 2){share_status = '<span class="sticer-share-down btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconDown+'"></span>';}
		blockShare.innerHTML = share_status;
		
		contentTop.querySelector('.one-block').append(blockShare);
	}
	HWS.buffer.load_analog_graph = function(data){ //загрузка и отрисовка аналогово графика
		if(!HWS.buffer.analog_graph){HWS.buffer.analog_graph = {}}; 
		let time = data.time || 1;
		let elId = data.elId || id;
		let target = data.target || '#equp-statistic-graf .graph-windiw-statistic';
		let gTime = HWS.buffer.system_time || Date.now();
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		HWF.post({url:HWD.api.gets.historyСharts,data:{token:token,timestamp_start:sTime,timestamp_end:eTime,charts:[{object_id:id}]},fcn:function(infGraph){
			let list = infGraph[id].charts[0].values;
			let blockGraph = HWS.getElem(target);
			let dataChart = [];
			
			HWF.recount(list,function(elInf){
				let statColor = HWF.getStateColor(elInf.state);
				dataChart.push({
					"date":new Date(elInf.timestamp),
					"value":elInf.value,
					"color":am4core.color(statColor),
				});
			});
			//console.log(dataChart);
			if(HWS.buffer.analog_graph[elId]){
				let chart = HWS.buffer.analog_graph[elId];
				let lastList = chart.data;
				let newList = dataChart;
				let startPoitn = newList[0].date
				let endPoitn = lastList[lastList.length-1].date;
				let iter = 0,shiftDel = 0,shiftAdd = 0;
				let sumRenderList = lastList.length, testSum = 0;
				
				HWF.recount(lastList,function(pointInfo){if(pointInfo.date < startPoitn){shiftDel++;}});
				HWF.recount(newList,function(pointInfo){if(pointInfo.date > endPoitn){shiftAdd++}});
				
				testSum = sumRenderList-shiftDel;
				if (shiftDel && testSum > 1) {for (let i = 0; i < shiftDel; i++) {chart.addData([],1);}}
				
				if (shiftAdd) {
					let newAdd = newList.slice(-shiftAdd);
					chart.addData(newAdd);
				}
			}
			else {
				// Create chart instance
				console.log('HI');
				am4core.useTheme(am4themes_animated);
				let chart = am4core.create(blockGraph, am4charts.XYChart);
					chart.cursor = new am4charts.XYCursor();
					chart.data  = dataChart;
					chart.dateFormatter.utc = true;
					chart.language.locale = am4lang_ru_RU;
					
				let xAxes = chart.xAxes.push(new am4charts.DateAxis());
					//xAxes.startLocation = 0.49;
					//xAxes.endLocation = 0.51;
					
				// Create value axis
				let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
					yAxis.extraMin  = 0.1;
					yAxis.extraMax  = 0.1;
					yAxis.baseValue = -100;
					yAxis.renderer.labels.template.adapter.add("text", function(str) {
						if(str){ let numb = str.replace(',','.'); return parseFloat(numb).toFixed(1);}
					})

				// Create series
				let series = chart.series.push(new am4charts.LineSeries());
				series.dataFields.valueY = "value";
				series.dataFields.dateX = "date";
				series.propertyFields.stroke = "color";
				series.propertyFields.fill = "color";
				series.strokeWidth = 2;
				series.tooltipText = "{value}";
				//series.tensionX = 0.1;
				series.fillOpacity = 0.2;

				let scrollbarX = new am4charts.XYChartScrollbar();
				scrollbarX.series.push(series);
				//scrollbarX.minHeight = 60; // размер скрола
				chart.scrollbarX = scrollbarX;
				HWS.buffer.analog_graph[elId] = chart;
				
				chart.cursor.xAxis = xAxes;
				chart.cursor.snapToSeries = series;

			}
			HWS.buffer.controlAutoReoladStoper = false;
		}});
	}

	HWS.buffer.load_discret_graph = function (data){ //загрузка и отрисовка дискретного графика 
		if(!HWS.buffer.discret_graph){HWS.buffer.discret_graph = {}}; 
		let time = data.time || 1; time = parseInt(time);
		let elId = data.elId || id;
		let target = data.target || '#equp-statistic-graf .graph-windiw-statistic';
		let gTime = HWS.buffer.system_time || Date.now();
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		
		HWF.post({url:HWD.api.gets.historyСharts,data:{token:token,timestamp_start:sTime,timestamp_end:eTime,charts:[{object_id:id}]},fcn:function(infGraph){console.log(infGraph);
			let list = infGraph[id].charts[0].values;
			let blockGraph = HWS.getElem(target);
			let dataChart = [];
			let nameData = HWS.buffer.elem.config.state_name || ['no','yes'];
			
			HWF.recount(list,function(elInf){
				let statColor = HWF.getStateColor(elInf.state);
				dataChart.push({
					"date":elInf.timestamp,
					"value":1,
					"color":am4core.color(statColor),
				});
			});
			
			if(HWS.buffer.discret_graph[elId]){
				let chart = HWS.buffer.discret_graph[elId];
				let lastList = chart.data;
				let newList = dataChart;
				let startPoitn = newList[0].date
				let endPoitn = lastList[lastList.length-1].date;
				let iter = 0,shiftDel = 0,shiftAdd = 0;
				let sumRenderList = lastList.length, testSum = 0;
				
				HWF.recount(lastList,function(pointInfo){ if(pointInfo.date < startPoitn){shiftDel++;}});
				HWF.recount(newList,function(pointInfo){ if(pointInfo.date > endPoitn){shiftAdd++} });
				
				testSum = sumRenderList-shiftDel;//sumRenderList
				if (shiftDel && testSum > 1) { for (let i = 0; i < shiftDel; i++) {chart.addData([],1);}}
				if (shiftAdd) {let newAdd = newList.slice(-shiftAdd);chart.addData(newAdd);}
			}
			else {
				var chart = am4core.create(blockGraph, am4charts.XYChart);
					chart.data = dataChart;
					chart.dateFormatter.utc = true;
					chart.language.locale = am4lang_ru_RU;
					chart.leftAxesContainer.width = 0;
					chart.plotBorderWidth = 1;

				var xAxes = chart.xAxes.push(new am4charts.DateAxis());
					//xAxes.categories = dataChart;
					xAxes.startLocation = 0.49;
					xAxes.endLocation = 0.51;
					//xAxes.min = 0.5;
					//xAxes.max = dataChart.length - 1.5;
					//xAxes.max  = dataChart[1].data;
					//xAxes.min  = dataChart[0].data;
					//xAxes.strictMinMax = true;
					//xAxes.renderer.minGridDistance = 0;
					//xAxes.logarithmic = true;
					//xAxes.adjustLabelPrecision = false;
					//xAxes.autoMargins = false;
					//xAxes.marginLeft =0;
					//xAxes.marginRight =0;
					
				var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
					yAxis.max  = 1;
					yAxis.min  = -1;
					yAxis.baseValue = -10;
					yAxis.renderer.baseGrid.disabled = true;
					yAxis.tooltip.disabled = true;
					yAxis.renderer.labels.template.disabled = true;

				var series = chart.series.push(new am4charts.LineSeries());
				series.dataFields.dateX = "date";
				series.dataFields.valueY = "value";
				series.propertyFields.stroke = 'color';
				series.propertyFields.fill = "color";
				series.strokeWidth = 0;
				series.fillOpacity = 0.5;
				series.tooltip.getFillFromObject = false;

				chart.cursor = new am4charts.XYCursor();
				//chart.cursor.lineY.disabled = true; // отключает  показ линии по оси Y
				
				chart.cursor.snapToSeries = series;
				chart.cursor.xAxis = xAxes;
				
				chart.scrollbarX = new am4charts.XYChartScrollbar();
				//chart.scrollbarX.minHeight = 40;
				chart.scrollbarX.series.push(series);
				
				HWS.buffer.discret_graph[elId] = chart;
			}
			HWS.buffer.controlAutoReoladStoper = false;
		}});
	}

	function correct_lagrand (orig,corr) {
		let x = 0.0;
		for (let i in corr) {
			let calc = 1.0;
			for (let j in corr) {
				if (i == j) continue;
				let mul = orig - corr[j]["origin"];
				calc *= mul;
				let div = corr[i]["origin"] - corr[j]["origin"];
				calc /= div ;
			}
			x += corr[i]["correction"] * calc;
		}
		x = x;
		x = ((x * 10) | 0) / 10;
		return x;
	}
	
	function load_history(H){
		// История
		let systemID = G.elem.id_system;
		let block = document.body.querySelector('#history-block');
		let setsB = document.createElement('div'); setsB.id = 'history-block_sets';
		let listB = document.createElement('div'); listB.id = 'history-block_list';
		if(!block){
			block = document.createElement('div');
			block.id = 'history-block'; block.classList.add('one-block');
			//block.innerHTML = '<h4>История:</h4>'+ HWhtml.loader();
			contentBot.append(block);
		}
		print = HWhtml.timeControler(H,'timeControlFCNJurnal','timeControlLoadFCNJurnal');
	
		setsB.innerHTML = print;
		block.innerHTML = '';
		block.append(setsB);
		block.append(listB);

		let time = H || 1; // 1 hour
		let gTime = Date.now() ;
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		let data = {
			timestamp_start:sTime,
			timestamp_end:eTime,
			system_id:systemID,
			device_ids:[id]
		}

		HWF.printList({name:'listJurnal',url:HWD.api.gets.eventsHistory,target:listB,
		data:data,
		clas:'block_w_100',
		fcn:function(info){ console.log(info)
			let list = info.events;
			let html = '<ul class="list jurnal">';
				html+= '<li class="one-head sticky">\
					<span class="time">Дата и время</span>\
					<span class="info">Событие</span>\
				</li>';
			
			if(list.length > 0){
				HWF.recount(list,function(inf){
					let time = HWF.unixTime(inf.timestamp).full;
					let texte = inf.text;
					html+= '<li>\
						<span class="time">'+time+'</span>\
						<span class="info">'+texte+'</span>\
					</li>';
				});
			} else {
				html+= '<li>\
						<p class="block_w_100 tw_600 ta_center block_p_20">За указанный период события не найдены.</p>\
					</li>';
			}
			html+= '</ul>'
			
			return html;
		}});
	} HWS.buffer.load_history = load_history;
	
	//Изменение времени показаний ЖУРНАЛА
	HWS.buffer.timeControlFCNJurnal = function(H) {
		HWS.buffer.load_history(H);
	}		
	
	//Загрузка истории по журналу
	HWS.buffer.timeControlLoadFCNJurnal = function(H,type) {
		let time = parseInt(H) || 1; // 1 hour
		let gTime = Date.now() ;
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		let data = {
			token:token,
			timestamp_start:sTime,
			timestamp_end:eTime,
			system_id:G.elem.id_system,
			device_ids:[id]
		}
		
		if(type == 'CSV'){
			HWF.post({url:"history/events_history_csv",data:data,fcn:function(inf){
				window.open(serverURL+'/uploads/'+inf.path_and_filename_to_upload_file);
			}});
		}
		
		if(type == 'PDF'){
			HWF.post({url:"history/events_history_pdf",data:data,fcn:function(inf){
				window.open(serverURL+'/uploads/'+inf.path_and_filename_to_upload_file);
			}});
		}
	}	
	
	
	// Загрузка графиков
	HWS.buffer.timeControlLoadFCN = function(H,type){
		let time = parseInt(H) || 1;
		let gTime = HWS.buffer.system_time || Date.now();
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		if(type == 'CSV'){
			HWF.post({url:"history/chart_value_csv",data:{token:token,object_id:id,timestamp_start:sTime,timestamp_end:eTime},fcn:function(inf){
				window.open(serverURL+'/uploads/'+inf.path_and_filename_to_upload_file);
			}});
		}
		
		if(type == 'PDF'){
			HWF.post({url:"history/chart_value_pdf",data:{token:token,object_id:id,timestamp_start:sTime,timestamp_end:eTime},fcn:function(inf){
				window.open(serverURL+'/uploads/'+inf.path_and_filename_to_upload_file);
			}});
		}
	}
	
	// Нажатие на кнопку если оборудование не на связи
	HWS.on('click','btn_no_connect',function(e,el){
		let gosystemId = HWS.buffer.elem.system_object_id || '';
		if(gosystemId){gosystemId = 'equipment/'+gosystemId}
		else {gosystemId = '';}
		HWF.modal.confirmCust({
			title:'Нет связи с системой',
			print:'Система, с которой связано данное устройство или программа,<br> ушла со связи с ЛК. Когда она снова выйдет на связь,<br> все настройки снова будут доступны.<br><br> Проверьте связь с системой',
			no:'ЗАКРЫТЬ',
			yes:'ПРОВЕРИТЬ',
			yesHref:gosystemId,
		});
	});
		
	// Загрузка Доступов
	HWS.buffer.equpAccessesFCN = function(){
		B.title.append('Доступы');
		HWequip.accessUsers(id,contentBot)
	}
	
	HWS.buffer.notYouElemInfoFCN = function(infoEl){if(infoEl && !infoEl.owner_status && infoEl.security == 'r'){
		let ovner = infoEl.owner_info.email;
		let divInf =  document.createElement('div');
		divInf.classList.add('one-block-col');
		divInf.innerHTML = '<div class="not-you-elem">Вам доступен только просмотр, любые изменения настроек не сохранятся. Для изменения прав обратитесь к: '+ovner+'</div>';
		contentBot.prepend(divInf);
	}}
	
	// Кнопка Удалить программу/оборудование
	HW.on('click','btn_delete_equp',function(e,el){
		let title = 'Удалить устройство?';
		let infotext = 'Данное устройство будет удалено из личного кабинета. Вы уверены?';
		HWF.modal.confirm(title,infotext,'btn_delete_equp_yes','Удалить');
	});
	HW.on('click','btn_delete_equp_yes',function(e,el){
		let idprog = id || H.hash[1] || false;;
		HWF.modal.loader('Удаление');
		HWF.post({url:HWD.api.gets.objectsDelete,data:{token:token,id_objects:[idprog]},fcn:function(info){
			HWF.modal.of();
			HWS.go('equipments');
			HWF.push('Оборудование удалено');
		}});
	});
	
	//Изменение имени
	HW.on('focusin','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"}; HWS.buffer.nameFocus = 1;
	});
	HW.on('focusout','equp_name',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"}; HWS.buffer.nameFocus = 1;
	});
	HW.on('input','equp_name',function(e,el){HWS.buffer.nameFocus = 1}); HW.on('click','equp_name',function(e,el){HWS.buffer.nameFocus = 1});
	
	HW.on('change','equp_name',function(e,el){
		HWS.buffer.nameFocus = false;
		let name = document.querySelector('.name-page input');
		//let nameRegex = new RegExp(HWD.reg.name);
		
		if(HWS.buffer.elem.config.name != name.value){
			//if(nameRegex.test(name.value)){
			if(name.value){
				let dataP = {token:HWS.pageAccess,id_object:id,name:name.value}
				HWF.post({url:HWD.api.gets.taskName3xDevice,data:dataP,fcn:function(nInf){console.log(nInf)}});
				HWS.buffer.elem.config.name = name.value;
				name.classList.remove('red');
			}
			else {
				name.classList.add('red');
			}
		}
		el.blur();
	});
	
	//Нажатие на чекбокс в разделе ( доступы )
	HW.on('click','one_equipment_acsses_check',function(e,el){
		let btnBox = HWS.getElem('#block-menu-action-btn-acsses');
		
		if(el.classList.contains('active')){
			btnBox.classList.add('open');
		}else {
			let allAct = document.querySelectorAll('.list-equipments li .equ_b .one_equipment_check.active');
			if (allAct.length == 0){btnBox.classList.remove('open');}
		}
	});	
});