"use strict";
HWspa.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1>Наборы доступа</h1>';
	let menu = '<div class="btn_access_add" title="Добавить набор">+</div>';
	let content = document.createElement('div');
		content.classList.add('main-block'); content.classList.add('max-1200');
		content.classList.add('access-page');
		content.innerHTML = HWhtml.loader();
		
	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);
	
	HWF.post("access",load_page);
	
	function load_page(info){
		let block = HWhtml.tile('one-block');
		let btn = document.createElement('div'); btn.classList.add('btn-page'); btn.classList.add('flex_between');
			btn.innerHTML = HWhtml.btn.back();
		let fragment = document.createDocumentFragment();
		
		block.b.appendChild(btn);
		
		for (let key in info) {
			let elem = info[key];
			let one = HWhtml.tile('one-tile');
			
			one.b.dataset.id = key;
			one.top.classList.add('flex_between');
			one.top.innerHTML = '\
			<h3 class="pointer">'+elem.name+'</h3>\
			<div class="click btn_change_acces"></div>\
			<div class="control-btn">\
				<div class="text_btn btn_change_acces">✎</div>\
				<div class="text_btn btn_clouse_acces">✕</div>\
			</div>';
			let list = [{html:'<div>Тип</div><div>Имя</div>',class:'sticky one-head'}];
			for (let lk in elem.list) {
				let li = elem.list[lk];
				let type = '';
				if(li.uid) {type = 'icon_equip_'+HWF.getTypeUid(li.uid)}
				else if(li.group && li.group == 'projects') {type = 'icon_projects_'+li.type}
				else if(li.group && li.group == 'programs') {type = 'icon-programs'}
				let icon =  '<span class="icon '+type+'">';
				list.push('<div>'+icon+'</div><div>'+li.name+'</div>');
			}
			
			one.mid.innerHTML = HWhtml.ul(list);
			fragment.appendChild(one.b);
		}
		block.top.innerHTML = '<h5>Мои наборы</h5>';
		block.mid.classList.add('flex_center_top');
		block.mid.appendChild(fragment);
		content.innerHTML = '';
		content.appendChild(block.b);
	}
	
	HW.on('click','btn_change_acces',function(e,elem){
		let id = HW.parentSearchClass(elem,'one-tile').dataset.id;
		HW.go('acces/'+id);
	});
	
	//Добавить Новый набор
	HW.on('click','btn_access_add',function(e,elem){HW.go('acces');});
	//Удалить набор
	HW.on('click','btn_clouse_acces',function(e,elem){
		let block = HW.parentSearchClass(elem,'one-tile');
		let name = block.querySelector('h3').innerHTML;
		HW.data.elemDelete = block;
		HWF.modal.confirm('Удалить <b>'+name+'</b> ? ');
	});
	HW.on('click','btn-yes-modal',function(e,elem){
		HW.data.elemDelete.parentNode.removeChild(HW.data.elemDelete);
	});
});