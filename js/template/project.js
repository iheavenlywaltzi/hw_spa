"use strict";
HWS.templateAdd(function(HW,B,H){  // ШАБЛОН и его функции. для вызываемой страници
	HWF.clearBlocks();//очистим блоки контена
	let token = HWS.pageAccess;
	let listD = {}; // для синхронизации по сокетам
	let project = H.hash[1] || 'main_page';
	let edit = H.hash[2] || ''; // edit
	let content = '<span>'+HWhtml.loader()+'</span>';
	let	menu = '<div></div>';
		menu+= '<div id="spec-dop-mini-menu"></div>';
	
	B.menud.append(menu);
	B.content.append(content);	
	//запросы информации
	HWF.post({url:'projects/get_project',data:{token:token,id_project:project},fcn:load_project});
	
	// Загрузка Проекта
	function load_project(info){
		console.log(info);
		HWS.buffer.elemLoadInfo = info;
		if (edit == 'edit'){HW.load('project-edit');}
		else{
			let type = info.project_type;
			if(type == '1'){HWmain.loadImg(info)};
			if(type == '2'){HWmain.loadMap(info)};
			if(type == '3'){HWmain.loadGrid(info)};
		}
	}
	
	HW.on('click','rm-add-obj',HWmain.modal.obj);
	HW.on('click','rm-add-equip',HWmain.modal.equip);
	HW.on('click','rm-add-prog',HWmain.modal.prog);
	HW.on('click','rm-add-grup',HWmain.modal.grup);
	HW.on('pointerdown','container',HWmain.ofRMenu,{method:'m'});
});