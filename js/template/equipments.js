"use strict";
HWS.template.equipments = function(HW,B,H){
	HWF.clearBlocks();//очистим блоки контена
	HWS.buffer.dopFilter = {}//  Дополнительный фильтр. для оборудования
	let viewTable = localStorage.getItem('viewEquipments') || 'list';
	let vieeControl = {}; vieeControl[viewTable] = 'active';
	let localBuffer = {}
	let token = HWS.pageAccess;
	let title = '<h1>Оборудование</h1>';
	let hashGet = HWS.getHash().get;
	let menu = '<div>';
		menu += '<a class="btn active" href="#equipments">Оборудование</a>';
		menu += '<a class="btn" href="#templates">Шаблоны настроек</a>';
		menu += '</div>';
		menu += '<span class="add_new_equp btnIcon">\
				<span class="select-none">Добавить оборудование</span>\
				<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>\
			</span>';
			
	HWS.buffer.btn_dop_sort_system = 1;
	HWS.buffer.dopFilter.system = 1;
	
	if(JSON.stringify(hashGet) != "{}"){hashGet = 'active';} else {hashGet = '';}
	let content = '<div class="main-block ob-center max-1200 equipments-page">\
		<div class="filter-equipments one-block-col">\
			<div class="flex_between block_w_100">\
				<div class="btn-open-filter btn-active pointer block_w_210p" data-onof="#filter-window" data-targetdown="#icon-filter">\
					<div class="display_inline select-none">\
						<span id="icon-filter" class="icon icon-filter hover_orange p20"></span>\
						<span class="spec-circle-icon '+hashGet+'"></span>\
					</div>\
					<span class="vertical-middle select-none ts_12p block_pl_5">Фильтр</span>\
				</div>\
				'+HWhtml.search('block_mr_8')+'\
				<div id="block-menu-action-btn" class="open-clouse block_w_305p">\
					<span class="style_btn p3 grey block_ml_6 btn_set_tmp">Шаблон</span>\
					<span class="style_btn p3 grey block_ml_6 btn_go_vijet">В виджеты</span>\
					<span class="style_btn p3 grey block_ml_6 btn_delete_equp">Удалить</span>\
				</div>\
			</div>\
			'+HWfilter.equipments()+'\
			<div class="dop-sort-filter">\
				<div>\
					<div class="checkbox-text btn-active btn_dop_sort_filter_system">\
						<span class="checkbox select-none"></span>\
						<span class="name select-none">Сначала системы</span>\
					</div>\
					<span class="block_p_8"></span>\
					<div class="checkbox-text btn-active btn_dop_sort_filter_alarm_sensor">\
						<span class="checkbox select-none"></span>\
						<span class="name select-none">Сначала датчики в тревоге</span>\
					</div>\
					<span class="block_p_8"></span>\
					<div class="checkbox-text btn-active btn_dop_sort_filter_no_connect">\
						<span class="checkbox select-none"></span>\
						<span class="name select-none">Сначала устройства без связи</span>\
					</div>\
				</div>\
				<div>\
					<div class="checkbox-text btn-active btn_dop_sort_system active">\
						<span class="name select-none">Сортировать по системам</span>\
						<span class="on-off select-none"></span>\
					</div>\
				</div>\
			</div>\
		</div>\
		<div class="one-block nop all-equipments"></div>\
	</div>'+
	'<div class="right-click-menu">'+HWhtml.rMenuEqup()+'</div>';
	
	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);

	load_list();
	
	//функция загрузки страници;
	function load_list(){
		let get = HWS.getHash().get;
		let controlQuery = true;
		let system = get.equp_system;

		if(system == 2){HWF.recount(get,function(val){if (val == "no_select"){controlQuery = false}});}
		 
		if(controlQuery){dop_filter();}
		else {
			let list = HWS.getElem('.all-equipments');
			list.innerHTML = '<p class="tw_600 ta_center block_p_20">Оборудование не найдено. Попробуйте изменить настройки фильтра.</p>';
		}
	}
	
	function dop_filter(){
		let arrGet = [];
		let objControl = HWS.buffer.dopFilter;

		HWF.recount(objControl,function(elf,keyf){
			if(elf){
				if(keyf == 'name'){keyf = '-name';}
				arrGet.push(keyf);
			}
		});
		
		HWS.data.apiGet.equipments.data.sort = arrGet;
		HWF.print.equipments('.all-equipments',{a:'on'});
	}
	
	function print_modal_list_sys_add_equp(e,el){
		let print = '<div class="spec-line-add-equp">';
			print+= '<div class="spec-test">';
				print+= '<span class="text-btn left tf_fr900">Выбор<br>системы</span>';
				print+= '<span class="text-btn mid tc_grey">Выбор типа<br>оборудования</span>';
				print+= '<span class="text-btn right tc_grey">Добавление<br>оборудования</span>';
			print+= '</div>';
			print+= '<div class="big-line">';
				print+= '<span class="point active"><span class="cir"></span></span>';
				print+= '<span class="line"></span>';
				print+= '<span class="point"><span class="cir"></span></span>';
				print+= '<span class="line"></span>';
				print+= '<span class="point"><span class="cir"></span></span>';
			print+= '</div>';
		print+= '</div>';
		print+= '<div id="modal-print-list">'+HWhtml.loader()+'</div>';
		
		HWF.modal.print('Добавление оборудования',print);
		HWF.print.system('#modal-print-list',{a:'no',type:'nobtn system-list',click:'click_modal_one_system'});
	}
	
	function print_modal_list_port_add_equp(e,el){
		let elemLi = HW.upElem(el,'.one-equipment');
		if(elemLi) {
			HWS.buffer.idSystem = elemLi.dataset.id;
			HWS.buffer.idSystemD = elemLi.dataset.idsystem;
		}

		let print = '<div class="spec-line-add-equp">';
			print+= '<div class="spec-test">';
				print+= '<span class="text-btn left rm-add-equip btn_text grey">Выбор<br>системы</span>';
				print+= '<span class="text-btn mid tf_fr900">Выбор типа<br>оборудования</span>';
				print+= '<span class="text-btn right tc_grey">Добавление<br>оборудования</span>';
			print+= '</div>';
			print+= '<div class="big-line">';
				print+= '<span class="point passive"><span class="cir"></span></span>';
				print+= '<span class="line active"></span>';
				print+= '<span class="point active"><span class="cir"></span></span>';
				print+= '<span class="line"></span>';
				print+= '<span class="point"><span class="cir"></span></span>';
			print+= '</div>';
		print+= '</div>';
		print+= '<div class="modal-type-equp-add">';
			print+= '<div class="one-block-t">';
				print+= '<div class="block-btn">';
					print+= '<p class="tf_fr900">Порты «Д1… Д5»</p>';
					print+= '<p>Движение, дым, газ манометр, наличия сети, протечка, дверь, уровень жидкости</p>';
					print+= '<span class="button grey noback btn_modal_window_port_d1_d5">Выбрать</span>';
				print+= '</div>';
				print+= '<div class="block-btn">';
					print+= '<p class="tf_fr900">Порт «Радио»</p>';
					print+= '<p>Движение, дым, беспроводная розетка, протечка, брелок, привод крана, температура, дверь</p>';
					print+= '<span class="button grey noback btn_modal_window_port_radio">Выбрать</span>';
				print+= '</div>';
			print+= '</div><div class="one-block-t">';
				print+= '<div class="block-btn">';
					print+= '<p class="tf_fr900">Порт «ДОП»</p>';
					print+= '<p>Температура RS-485, блок 10 реле, блок 2-х розеток, влажность, адаптеры eBus/OpenTherm</p>';
					print+= '<span class="button grey noback btn_modal_window_port_dop">Выбрать</span>';
				print+= '</div>';
				print+= '<div class="block-btn">';
					print+= '<p class="tf_fr900">Ключ «TouchMemory»</p>';
					print+= '<p>Считыватель подключается к порту Т3/КЛ <br> и через него программируется ключ</p>';
					print+= '<span class="button grey noback btn_modal_window_key">Выбрать</span>';
				print+= '</div>';
			print+= '</div>';
			/*	
			print+= '</div><div class="one-block-t">';

			print+= '</div>';
			*/
		print+= '</div>';
		
		HWF.modal.print('Добавление оборудования',print);
	}
	
	function print_modal_port_d1_d5(e,el){
		function print_sensor_d1_d5 (){
			let block = HWS.getElem('#modal-block-d1-d5');
			let idSystemD = HWS.buffer.idSystemD;
			let dataP = {token:HWS.pageAccess,list_start:0,list_amount:100,id_system:idSystemD,iface_type:0,iface_port:3}
			HWF.post({url:HWD.api.gets.list,data:dataP,fcn:function(inf){
				let allD = {};
				for (let k in inf.list){allD[inf.list[k].info.channel_index] = inf.list[k].info.type.type;}
				let specSelect = {
					0:'Не настроен',
					1441:{name:'Датчик открытия двери'},
					1857:{name:'Датчик дыма'},
					1537:{name:'Датчик газа'},
					1665:{name:'Датчик протечки воды'},
					738:{name:'Датчик наличия напряжения сети'},
					1697:{name:'Датчик движения'},
					417:{name:'Универсальный датчик'},
					1761:{name:'Датчик давления'},
					1473:{name:'Датчик уровня'}
				}
				let selec1 = (allD[1])?allD[1]:false; let selec2 = (allD[2])?allD[2]:false; let selec3 = (allD[3])?allD[3]:false;
				let selec4 = (allD[4])?allD[4]:false; let selec5 = (allD[5])?allD[5]:false;
				let html = ''; 
				html += '<p id="set_d1"><span class="block_pr_10 vertical-middle">Д1</span>'+HWhtml.select(specSelect,'block_w_260p',selec1)+'</p>';
				html += '<p id="set_d2"><span class="block_pr_10 vertical-middle">Д2</span>'+HWhtml.select(specSelect,'block_w_260p',selec2)+'</p>';
				html += '<p id="set_d3"><span class="block_pr_10 vertical-middle">Д3</span>'+HWhtml.select(specSelect,'block_w_260p',selec3)+'</p>';
				html += '<p id="set_d4"><span class="block_pr_10 vertical-middle">Д4</span>'+HWhtml.select(specSelect,'block_w_260p',selec4)+'</p>';
				html += '<p id="set_d5"><span class="block_pr_10 vertical-middle">Д5</span>'+HWhtml.select(specSelect,'block_w_260p',selec5)+'</p>';
				
				block.innerHTML = html;
			}});
		}
		
		let print = '<div class="spec-line-add-equp">';
			print+= '<div class="spec-test">';
				print+= '<span class="text-btn left rm-add-equip btn_text grey">Выбор<br>системы</span>';
				print+= '<span class="text-btn mid click_modal_one_system btn_text grey">Выбор типа<br>оборудования</span>';
				print+= '<span class="text-btn right tf_fr900">Добавление<br>оборудования</span>';
			print+= '</div>';
			print+= '<div class="big-line">';
				print+= '<span class="point passive"><span class="cir"></span></span>';
				print+= '<span class="line active"></span>';
				print+= '<span class="point passive"><span class="cir"></span></span>';
				print+= '<span class="line active"></span>';
				print+= '<span class="point active"><span class="cir"></span></span>';
			print+= '</div>';
		print+= '</div>';
		print+= '<div class="modal-type-equp-add">';
			print+= '<div class="one-block-t2">\
				<div class="number">1</div>\
				<div class="info">\
					<p>Подключите нужные датчики к любым <br> не занятым портам Д1 … Д5</p><br>\
					<img src="img/modalequpd1d5.svg"><br><br>\
					<p class="tf_fr900">Поддерживаемые датчики</p>\
					<p>Движение, дым, газ манометр, наличия <br> сети, протечка, дверь, уровень жидкости</p>\
				</div>\
			</div>';
			print+= '<div class="one-block-t2">\
				<div class="number">2</div>\
				<div class="info">\
					<p>Выберете какой датчик к какому порту вы<br>подключили и нажмите «Добавить»</p><br>\
					<div id="modal-block-d1-d5">'+HWhtml.loader()+'</div>\
				</div>\
			</div>';
		print+= '</div>';
		print+= '<div class="modal-type-equp-add-bot-btn">\
			<span class="block_w_120p"></span>\
			<span class="block_w_170p style_btn orange btn_add_new_equp_d1_d5">Добавить</span>\
			<a href=""><span><img src="img/play.svg"></span><span>Видео-<br>инструкция</span></a>\
		</div>';
		
		HWF.modal.print('Порты «Д1… Д5»',print);
		print_sensor_d1_d5();
	}
	function print_modal_port_dop(e,el){
		let print = '<div class="spec-line-add-equp">';
			print+= '<div class="spec-test">';
				print+= '<span class="text-btn left rm-add-equip btn_text grey">Выбор<br>системы</span>';
				print+= '<span class="text-btn mid click_modal_one_system btn_text grey">Выбор типа<br>оборудования</span>';
				print+= '<span class="text-btn right tf_fr900">Добавление<br>оборудования</span>';
			print+= '</div>';
			print+= '<div class="big-line">';
				print+= '<span class="point passive"><span class="cir"></span></span>';
				print+= '<span class="line active"></span>';
				print+= '<span class="point passive"><span class="cir"></span></span>';
				print+= '<span class="line active"></span>';
				print+= '<span class="point active"><span class="cir"></span></span>';
			print+= '</div>';
		print+= '</div>';
		print+= '<div class="modal-type-equp-add">';
			print+= '<div class="one-block-t2">\
				<div class="number">1</div>\
				<div class="info">\
					<p>Отключите все устройства от порта ДОП <br>и подключите в него добавляемый датчик</p><br>\
					<img src="img/modalequpdop.svg"><br><br>\
					<p class="tf_fr900">Поддерживаемые датчики</p>\
					<p>Температура RS-485, блок 10 реле, блок 2-х<br>розеток, влажность, адаптеры eBus/OpenTherm</p>\
				</div>\
			</div>';
			print+= '<div class="one-block-t2">\
				<div class="flex_center_top"><div class="number">2</div>\
				<div class="info">\
					<p>Нажмите на системе кнопку «УСТ», либо<br>нажмите кнопку «УСТ» ниже</p>\
					<img src="img/modalequpdop2.svg"><br><br>\
					<span class="style_btn grey block_w_120p btn_add_new_equp_ust">УСТ</span>\
				</div></div><br>\
				<div class="flex_center_top block_pt_30"><div class="number">3</div>\
				<div class="info">\
					<p>Система издаст три звуковых сигнала.<br>Датчик добавлен в систему.</p><br>\
				</div></div>\
			</div>';
		print+= '</div>';
		print+= '<div class="modal-type-equp-add-bot-btn">\
			<span class="block_w_120p"></span>\
			<span class="block_w_170p style_btn orange btn_clouse_modal">Готово</span>\
			<a href="https://youtu.be/Hl-9ua5VsQg?t=195" target="_blank"><span><img src="img/play.svg"></span><span>Видео-<br>инструкция</span></a>\
		</div>';
		print+= '<div class="modal-type-equp-add-bot-btn">\
			<span class="block_w_120p"></span>\
			<span data-text="1. Проверьте, включен ли порт ДОП в настройках вашей системы. <br>2. Находится ли датчик в списке поддерживаемых для порта ДОП? <br>3. Корректно ли подключен датчик и провода к нему?" class="btn_helper btn_text grey">Не получилось?</span>\
			<span class="block_w_120p"></span>\
		</div>';
		
		HWF.modal.print('Порт «ДОП»',print);
	}
	function print_modal_port_radio(e,el){
		let print = '<div class="spec-line-add-equp">';
			print+= '<div class="spec-test">';
				print+= '<span class="text-btn left rm-add-equip btn_text grey">Выбор<br>системы</span>';
				print+= '<span class="text-btn mid click_modal_one_system btn_text grey">Выбор типа<br>оборудования</span>';
				print+= '<span class="text-btn right tf_fr900">Добавление<br>оборудования</span>';
			print+= '</div>';
			print+= '<div class="big-line">';
				print+= '<span class="point passive"><span class="cir"></span></span>';
				print+= '<span class="line active"></span>';
				print+= '<span class="point passive"><span class="cir"></span></span>';
				print+= '<span class="line active"></span>';
				print+= '<span class="point active"><span class="cir"></span></span>';
			print+= '</div>';
		print+= '</div>';
		print+= '<div class="modal-type-equp-add">';
			print+= '<div class="one-block-t2">\
				<div class="flex_center_top"><div class="number">1</div>\
				<div class="info">\
					<p>По инструкции установите батарейку<br>в датчик, соблюдая полярность</p><br>\
					<p class="tf_fr900">Поддерживаемые датчики</p>\
					<p>Движение, дым, беспроводная розетка, протечка,<br>брелок, привод крана, температура, дверь</p>\
				</div>\</div>\
				<div class="flex_center_top block_pt_30"><div class="number">2</div>\
				<div class="info">\
					<p>Нажмите на системе кнопку «УСТ» (либо <br>нажмите кнопку «УСТ» ниже) и дождитесь, <br>пока индикатор «УСТ» на системе загорится</p><br>\
					<img src="img/modalequpradio1.svg"><br><br>\
					<span class="style_btn grey block_w_120p btn_add_new_equp_ust">УСТ</span>\
				</div>\</div>\
			</div>';
			print+= '<div class="one-block-t2">\
				<div class="flex_center_top"><div class="number">3</div>\
				<div class="info">\
					<p>Поднесите магнит на 0.5 секунды в место,<br>указанное в иснтрукции к датчику</p>\
					<img src="img/modalequpradio2.svg"><br><br>\
				</div></div>\
				<div class="flex_center_top block_pt_30"><div class="number">4</div>\
				<div class="info">\
					<p>При правильном программировании <br>индикатор на датчике мигнет 1 раз, а затем <br>мигнет 3 раза. Система издаст три звуковых <br>сигнала. Готово!</p><br>\
				</div></div>\
			</div>';
		print+= '</div>';
		print+= '<div class="modal-type-equp-add-bot-btn">\
			<span class="block_w_120p"></span>\
			<span class="block_w_170p style_btn orange btn_clouse_modal">Готово</span>\
			<a href="https://youtu.be/Hl-9ua5VsQg?t=364" target="_blank"><span><img src="img/play.svg"></span><span>Видео-<br>инструкция</span></a>\
		</div>';
		print+= '<div class="modal-type-equp-add-bot-btn">\
			<span class="block_w_120p"></span>\
			<span data-text="1. Проверьте, включен ли порт ДОП в настройках вашей системы. <br>2. Находится ли датчик в списке поддерживаемых для порта ДОП? <br>3. Корректно ли подключен датчик и провода к нему?" class="btn_helper btn_text grey">Не получилось?</span>\
			<span class="block_w_120p"></span>\
		</div>';
		
		HWF.modal.print('Порт «Радио»',print);
	}
	function print_modal_port_key(){
		let print = '<div class="spec-line-add-equp">';
			print+= '<div class="spec-test">';
				print+= '<span class="text-btn left rm-add-equip btn_text grey">Выбор<br>системы</span>';
				print+= '<span class="text-btn mid click_modal_one_system btn_text grey">Выбор типа<br>оборудования</span>';
				print+= '<span class="text-btn right tf_fr900">Добавление<br>оборудования</span>';
			print+= '</div>';
			print+= '<div class="big-line">';
				print+= '<span class="point passive"><span class="cir"></span></span>';
				print+= '<span class="line active"></span>';
				print+= '<span class="point passive"><span class="cir"></span></span>';
				print+= '<span class="line active"></span>';
				print+= '<span class="point active"><span class="cir"></span></span>';
			print+= '</div>';
		print+= '</div>';
		print+= '<div class="modal-type-equp-add">';
			print+= '<div class="one-block-t2">\
				<div class="number">1</div>\
				<div class="info">\
					<p>Убедитесь, что считыватель ключей<br>подключен к порту Т3/КЛ</p><br>\
					<img src="img/modalequpkey.svg"><br><br>\
				</div>\
			</div>';
			print+= '<div class="one-block-t2">\
				<div class="flex_center_top"><div class="number">2</div>\
				<div class="info">\
					<p>Нажмите на системе кнопку «УСТ», либо<br>нажмите кнопку «УСТ» ниже</p>\
					<img src="img/modalequpdop2.svg"><br><br>\
					<span class="style_btn grey block_w_120p btn_add_new_equp_ust">УСТ</span>\
				</div></div><br>\
				<div class="flex_center_top block_pt_30"><div class="number">3</div>\
				<div class="info">\
					<p>Индикатор считывателя начнет быстро <br>мигать. Приложетие ключ к считывателю.Код ключа будет сохранен в памяти <br>системы как мастер ключ.</p><br>\
				</div></div>\
			</div>';
		print+= '</div>';
		print+= '<div class="modal-type-equp-add-bot-btn">\
			<span class="block_w_120p"></span>\
			<span class="block_w_170p style_btn orange btn_clouse_modal">Готово</span>\
			<a href="https://youtu.be/youCs27kkOM?t=693" target="_blank"><span><img src="img/play.svg"></span><span>Видео-<br>инструкция</span></a>\
		</div>';
		print+= '<div class="modal-type-equp-add-bot-btn">\
			<span class="block_w_120p"></span>\
			<span data-text="1. Проверьте, включен ли порт ДОП в настройках вашей системы. <br>2. Находится ли датчик в списке поддерживаемых для порта ДОП? <br>3. Корректно ли подключен датчик и провода к нему?" class="btn_helper btn_text grey">Не получилось?</span>\
			<span class="block_w_120p"></span>\
		</div>';
		
		HWF.modal.print('Ключ «TouchMemory»',print);
	}
	
	//Нажатие на чекбокс
	HW.on('click','one_equipment_check',function(e,el){
		let btnBox = HWS.getElem('#block-menu-action-btn');
		
		if(el.classList.contains('active')){
			btnBox.classList.add('open');
		}else {
			let allAct = document.querySelectorAll('.list-equipments li .equ_b .one_equipment_check.active');
			if (allAct.length == 0){btnBox.classList.remove('open');}
		}
	});	
	
	//Кнопка Шаблон
	HW.on('click','btn_set_tmp',function(e,el){
		let allAct = document.querySelectorAll('.list-equipments li .equ_b .one_equipment_check.active');
		let bufAr = [], html='';
		if (allAct.length != 0){
			HWF.recount(allAct,function(elem){
				let li = HWS.upElem(elem,'li');
				bufAr.push(li.dataset.id);
			},'on');
			HWS.buffer.goTemplateArray  = bufAr;
			
			html+= '';
			
			HWF.modal.print('Выбор шаблона',html);
			HWF.print.templates('.modal-window .modal-section',{a:'off'});
		}
	});	
	// Выбор одного шаблона
	HW.on('click','click_one_temp',function(e,el){
		let id = HWS.upElem(el,'li').dataset.id;
		HWF.modal.print('Установка шаблона',HWhtml.loader());
		HWF.post({url:'patterns/apply',data:{token:token,id:id,id_objects:HWS.buffer.goTemplateArray},fcn:function(info){
			let Print = 'Шаблон применен к '+info.success_apply+' элементу(ам) из '+(info.fail_apply + info.success_apply)+'.';
			HWF.modal.message(Print,'Установка шаблона',);
		}});
	});
	
	//Кнопка в виджеты оборудования
	HW.on('click','btn_go_vijet',function(e,el){
		let allAct = document.querySelectorAll('.list-equipments li .equ_b .one_equipment_check.active');
		let bufAr = [];
		if (allAct.length != 0){
			HWF.recount(allAct,function(elem){
				let li = HWS.upElem(elem,'li');
				bufAr.push(li.dataset.id);
			},'on');
			HWS.buffer.goVijetArray  = bufAr;
			HWF.modal.confirm('Добавление виджетов','Добавить выбранное оборудование на страницу "Виджеты"?','','Добавить');
			HW.on('click','btn-yes-modal',function(e,el){
				HWF.push('Выбранное оборудование добавлено на страницу "Виджеты"');
				HWF.post({url:HWD.api.gets.vijetAdd,data:{token:token,id_objects:HWS.buffer.goVijetArray},fcn:function(info){
					
				}});
			});
		}
	});	
	
	//Кнопка удоления оборудования
	HW.on('click','btn_delete_equp',function(e,el){
		let allAct = document.querySelectorAll('.list-equipments li .equ_b .one_equipment_check.active');
		let bufAr = [];
		if (allAct.length != 0){
			//let infoText = 'Оборудование, подключенное к системе <br> (кроме встроенных в систему устройств), <br> будет удалено из системы.<br> <b>Системы</b> будут удалены из аккаунта.';
			let infoText = 'Выбранные устройства, кроме встроенных,<br> будут удалены из систем, которые их содержат.<br><br> При удалении систем сами системы будут удалены из аккаунта<br> со всеми устройствами, которые к ним подключены,<br> в том числе встроенными.';
			HWF.recount(allAct,function(elem){
				let li = HWS.upElem(elem,'li')
				bufAr.push(li);
			},'on');
			HWS.buffer.deleteEqupArray  = bufAr;
			HWF.modal.confirm('Удалить оборудование?',infoText,'','Удалить');
			HW.on('click','btn-yes-modal',function(e,el){
				let allId = [];
				//let dopModal = false;
				HWF.recount(HWS.buffer.deleteEqupArray,function(equpLi){
					allId.push(equpLi.dataset.id);
					let group = equpLi.dataset.group || 0; group = parseInt(group);
					let clType = equpLi.dataset.typetype || 0; clType = parseInt(clType);
					console.log(clType);
					if(clType != 14719 && clType != 14720 && clType != 14721 && clType != 14722 && clType != 14723 && clType != 14724){	
						if(group){equpLi.remove();}
						else {equpLi.querySelector('.equ_b .settings-butt .checkbox').classList.remove('active');}
					} else {equpLi.remove();}
				});
				HWF.modal.loader();

				HWF.post({url:HWD.api.gets.objectsDelete,data:{token:token,id_objects:allId},fcn:function(info){
					HWF.modal.of();
					HWF.push('Выбранное оборудование, кроме встроенного, удалено');
				}});
			});
		}
	});
	// доп для очистки кнопок
	HWS.on('change','filter_page',function(){let btnBox = HWS.getElem('#block-menu-action-btn'); btnBox.classList.remove('open');});	
	HWS.on('change','paginator-wiev-page',function(){let btnBox = HWS.getElem('#block-menu-action-btn'); btnBox.classList.remove('open');});	
	HWS.on('click','btn-paginator-right',function(){let btnBox = HWS.getElem('#block-menu-action-btn'); btnBox.classList.remove('open');});	
	HWS.on('click','btn-paginator-page',function(){let btnBox = HWS.getElem('#block-menu-action-btn'); btnBox.classList.remove('open');});	
	
	//select all
	HW.on('click','equp_select_all',function(event,elem){
		HWF.allActive('.list-equipments','.checkbox');
		
		let testSheck = HWS.getElem('.list-equipments .one-equipment .one_equipment_check.active');
		let btnBox = HWS.getElem('#block-menu-action-btn');
		
		if(testSheck){btnBox.classList.add('open');}
		else {btnBox.classList.remove('open');}
	});
	
	// Нажатие на + ( добавить )
	HW.on('click','add_new_equp',function(e,el){
		let menu = document.querySelector('.right-click-menu');
		if(menu.classList.contains('active')){menu.classList.remove('active')}
		else{
			menu.style.top = e.clientY+'px';
			menu.style.left = (e.clientX-200)+'px';
			menu.classList.add('active');
		}
	});
	
	// Нажатие на добавить систему
	HW.on('click','rm-add-system',function(e,el){HWF.printModalAddSystem();});	
	
	// Нажатие на добавить Оборудование
	HW.on('click','rm-add-equip',print_modal_list_sys_add_equp);
	// Выбор системы для добавления
	HW.on('click','click_modal_one_system',print_modal_list_port_add_equp);
	HW.on('click','btn_modal_window_port_d1_d5',print_modal_port_d1_d5);
	HW.on('click','btn_modal_window_port_dop',print_modal_port_dop);
	HW.on('click','btn_modal_window_port_radio',print_modal_port_radio);
	HW.on('click','btn_modal_window_key',print_modal_port_key);
	// НАжатие на кнопку Добавить д1-д5
	HW.on('click','btn_add_new_equp_d1_d5',function(e,el){
		let idSystemD = HWS.buffer.idSystemD;
		let d1 = document.body.querySelector('#set_d1 select');
		let d2 = document.body.querySelector('#set_d2 select');
		let d3 = document.body.querySelector('#set_d3 select');
		let d4 = document.body.querySelector('#set_d4 select');
		let d5 = document.body.querySelector('#set_d5 select');
		let allD = [
			{channel_index:1,type:d1.value},
			{channel_index:2,type:d2.value},
			{channel_index:3,type:d3.value},
			{channel_index:4,type:d4.value},
			{channel_index:5,type:d5.value}
		];
		let dataP = {token:HWS.pageAccess,id_system:idSystemD,devices:allD}
		HWF.post({url:HWD.api.gets.sendFor3xSystemSensorTypes,data:dataP,});
		
		HWF.push('Команда отправлена');
		HWF.modal.of();
	});
	// НАжатие на кнопку УСТ
	HW.on('click','btn_add_new_equp_ust',function(e,elem){ 
		let idSystemD = HWS.buffer.idSystemD;
		let dataP = {token:HWS.pageAccess,id_system:idSystemD,command:'ust'}
		HWF.post({url:HWD.api.gets.taskCommandTo3xSystem,data:dataP});
		HWF.push('Команда на добавление устройств отправлена системе.');
	});
	
	//ФИЛЬТР системы. btn_dop_sort_filter_system
	HW.on('click','btn_dop_sort_filter_system',function(e,el){
		if(el.classList.contains('active')){HWS.buffer.dopFilter.system_first = 1;}
		else {HWS.buffer.dopFilter.system_first = 0;}
		dop_filter();
	});
	
	//ФИЛЬТР датчики в тревоге. btn_dop_sort_filter_alarm_sensor 
	HW.on('click','btn_dop_sort_filter_alarm_sensor',function(e,el){
		if(el.classList.contains('active')){HWS.buffer.dopFilter.alarm_first = 1;}
		else {HWS.buffer.dopFilter.alarm_first = 0;}
		dop_filter();
	});
	
	//ФИЛЬТР сначала устройство без связи. btn_dop_sort_filter_no_connect
	HW.on('click','btn_dop_sort_filter_no_connect',function(e,el){
		if(el.classList.contains('active')){HWS.buffer.dopFilter.no_connection_first = 1;}
		else {HWS.buffer.dopFilter.no_connection_first = 0;}
		dop_filter();
	});	
	
	//ФИЛЬТР сортировать во системам. btn_dop_sort_system
	HW.on('click','btn_dop_sort_system',function(e,el){
		//HWS.buffer.btn_dop_sort_system = 0;
		if(el.classList.contains('active')){HWS.buffer.dopFilter.system = 1; HWS.buffer.btn_dop_sort_system = 1;}
		else {HWS.buffer.dopFilter.system = 0; HWS.buffer.btn_dop_sort_system = 0;}
		dop_filter();
	});	
	
	//ФИЛЬТР сортировать ПО имени. btn_dop_sort_system
	HWS.buffer.fcnSornName = function(){dop_filter();}
	
	
	
	/*
	
		//Уникальные элементы массива ( уберёт все похожие элементы )
		this.unique = function(arr) {
		var obj = Object.assign(o1, o2, o3);
	*/
}



