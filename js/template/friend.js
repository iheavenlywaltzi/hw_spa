"use strict";
HWS.templateAdd(function(HW,B,H){		
	let token = HWS.pageAccess;
	let idUser = H.hash[2] || false;
	let content = document.body.querySelector('.user-page');
	let myListBlock = HWhtml.tile('one-block nop my_list_block');
	let frListBlock = HWhtml.tile('one-block nop fr_list_block');
	
	content.innerHTML = '';
	myListBlock.top.innerHTML = '<p class="tw_700 block_p_10">Моё оборудование<p>';
	frListBlock.top.innerHTML = '<p class="tw_700 block_p_10">Мне доверили</p>';
	content.appendChild(myListBlock.b);
	content.appendChild(frListBlock.b);

	// ** FCN ** 
	// я доверил
	HWF.printList({name:'printListMyEqupBuffer',url:HWD.api.gets.list,target:myListBlock.mid,
	data:{share_to_id_user:idUser},
	fcn:function(inf){
		let list = inf.list;
		let html = '<ul id="list-acsses-equp" class="list">';
		
		if(list.length > 0){
			html+= '<li class="one-head sticky">';
			html+= '<div class="l-name ta_left"><span class="l-icon">Тип</span><span class="vertical-middle">Имя</span></div>';
			html+= '<div class="l-btn block_w_100p flex_between">\
				<div class="btn_text block_w_20p ta_center fr_select_r">r</div>\
				<div class="btn_text block_w_20p ta_center fr_select_w">w</div>\
				<div class="btn_text block_w_20p ta_center fr_select_x">x</div>\
				</div>';
			html+= '<div class="l-btn"></div>';
			html+= '</li>';
			HWF.recount(list,function(el){
				console.log(el)
				let Dinfo = {}
				let security = el.security;
				let actR = (security[0] == 'r')? 'active' : '';
				let actW = (security[1] == 'w')? 'active' : '';
				let actX = (security[2] == 'x')? 'active' : '';
				Dinfo.id = el.id;
				Dinfo.icon = '<span class="icon icon_equip_'+el.info.type.type+' block_mr_10"></span>';
				Dinfo.name = el.config.name || '';

				html+= '<li class="one_fr" data-id="'+Dinfo.id+'">';
				html+= '<div class="l-name ta_left">\
					<span class="l-icon">'+Dinfo.icon+'</span>\
					<span class="vertical-middle text-name">'+Dinfo.name+'</span>\
				</div>';
				html+= '<div class="block_w_100p flex_between">\
				<div class="btn-active '+actR+' checkbox-empty click_acse click_acse_r">r</div>\
				<div class="btn-active '+actW+' checkbox-empty click_acse click_acse_w">w</div>\
				<div class="btn-active '+actX+' checkbox-empty click_acse click_acse_x">x</div>\
				</div>';
				html+= '<div class="l-btn ta_right"><div class="text-icon btn_fr_delete">✕</div></div>';
				html+= '</li>';
			});
			html+='</ul>';
		}
		else {
			html = '<div class="ta_center block_pt_20">Отсутствует.</div>';
		}
		
		return html;
	}});
	
	// Мне доверели
	HWF.printList({name:'printListFrEqupBuffer',url:HWD.api.gets.list,target:frListBlock.mid,
	data:{shared_me_id_user:idUser},
	fcn:function(inf){
		let list = inf.list;
		let html = '<ul id="list-acsses-equp" class="list">';
		
		if(list.length > 0){
			html+= '<li class="one-head sticky">';
			html+= '<div class="l-name ta_left"><span class="l-icon">Тип</span><span class="vertical-middle">Имя</span></div>';
			html+= '<div class="l-btn block_w_100p flex_between">\
				<div class="block_w_20p ta_center">r</div>\
				<div class="block_w_20p ta_center">w</div>\
				<div class="block_w_20p ta_center">x</div>\
				</div>';
			html+= '<div class="l-btn"></div>';
			html+= '</li>';
			HWF.recount(list,function(el){
				let Dinfo = {}
				let security = el.security;
				let actR = (security[0] == 'r')? 'active' : '';
				let actW = (security[1] == 'w')? 'active' : '';
				let actX = (security[2] == 'x')? 'active' : '';
				Dinfo.id = el.id;
				Dinfo.icon = '<span class="icon icon_equip_'+el.info.type.type+' block_mr_10"></span>';
				Dinfo.name = el.config.name;

				html+= '<li class="one_fr" data-id="'+Dinfo.id+'">';
				html+= '<div class="l-name ta_left">\
					<span class="l-icon">'+Dinfo.icon+'</span>\
					<span class="vertical-middle">'+Dinfo.name+'</span>\
				</div>';
				html+= '<div class="l-btn block_w_100p flex_between">\
				<div class="'+actR+' checkbox-empty noactive">r</div>\
				<div class="'+actW+' checkbox-empty noactive">w</div>\
				<div class="'+actX+' checkbox-empty noactive">x</div>\
				</div>';
				html+= '<div class="l-btn ta_right"></div>';
				html+= '</li>';
			});
			html+='</ul>';
		}
		else {
			html = '<div class="ta_center block_pt_20">Отсутствует.</div>';
		}
		return html;
	}});

	/* FCN */
	function mass_rwx(){
		let allLi = document.body.querySelectorAll('#list-acsses-equp .one_fr');
		let dataD =[];
		if(allLi.length > 0){
			HWF.recount(allLi,function(li){
				let id = li.dataset.id;
				let r = li.querySelector('.click_acse_r');
				let w = li.querySelector('.click_acse_w');
				let x = li.querySelector('.click_acse_x');
				
				let security = '';
				(r.classList.contains('active'))? security+= 'r' : security+= '-';
				(w.classList.contains('active'))? security+= 'w' : security+= '-';
				(x.classList.contains('active'))? security+= 'x' : security+= '-';
				dataD.push({id_user:idUser,id_object:id,security:security});
			},'on');
			
			let data = {token:token,data:dataD}
			HWF.post({url:HWD.api.gets.objectsSecurity,data:data});
		}
	}

	// ** ACTIVE **
	// Нажатие на 1 вид прав
	HWS.on('click','click_acse',function(e,el){
		let li = HWS.upElem(el,'li');
		let id = li.dataset.id;
		let r = li.querySelector('.click_acse_r');
		let w = li.querySelector('.click_acse_w');
		let x = li.querySelector('.click_acse_x');
		
		let security = '';
		(r.classList.contains('active'))? security+= 'r' : security+= '-';
		(w.classList.contains('active'))? security+= 'w' : security+= '-';
		(x.classList.contains('active'))? security+= 'x' : security+= '-';
		
		let Ddata = [{id_user:idUser,id_object:id,security:security}]; 
		let data = {token:token,data:Ddata};
		HWF.post({url:HWD.api.gets.objectsSecurity,data:data});
	});
	//Удаление пользователя нажатие
	HWS.on('click','btn_fr_delete',function(e,el){
		let li = HWS.upElem(el,'li');
		let email = li.querySelector('.l-name .text-name').innerHTML;
		let print = 'Удалить доступы: <b>'+email+'</b>  ?';
		HWS.buffer.deleteLiFr = li;
		HWF.modal.confirm('Удаление',print,'delete_fr_accept');
	});
	//Подтверждение удоления
	HWS.on('click','delete_fr_accept',function(e,el){
		let idEqup = HWS.buffer.deleteLiFr.dataset.id;
		let delData = [{id_user:idUser,id_object:idEqup}];
		HWF.post({url:HWD.api.gets.objectsSecurityDel,data:{token:token,data:delData}});
		HWS.buffer.deleteLiFr.remove();
		HWS.buffer.deleteLiFr = false;
	});
	
	
	// выбор сазу всех элементов
	HWS.on('click','fr_select_r',function(){HWF.allActive('#list-acsses-equp','.click_acse_r'); mass_rwx();});
	HWS.on('click','fr_select_w',function(){HWF.allActive('#list-acsses-equp','.click_acse_w'); mass_rwx();});
	HWS.on('click','fr_select_x',function(){HWF.allActive('#list-acsses-equp','.click_acse_x'); mass_rwx();});
});
