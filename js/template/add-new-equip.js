"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let idSystem = HWS.data.idSystem;
	let idSystemD = HWS.data.idSystemD;
	let title = '<h1>Добавление нового оборудование</h1>';
	let content = '<div class="main-block ob-center max-1200 add-new-equip"><div class="one-block spec-print-block"></div></div>';
	console.log(HWS.data);
	B.title.append(title);
	B.content.append(content);
	
	let printB = document.body.querySelector('.main-block .spec-print-block');
	start_print();
	
	
	function start_print(){
		printB.innerHTML = '\
		<h4>Что добовляем ?</h4>\
		<div>\
			<div class="button_text btn_port_dop_or_radio">Порт доп или Радио</div>\
			</br>\
			<div class="button_text btn_conection_d1_d5">Встроенный датчик Д1…Д5</div>\
			</br>\
			<a href="/#equipments">Вернутся к оборудованию</div>\
		</div>'
	}
	
	function print_dop_or_radio(){
		let html = '<div><div class="button btn_back_start"> <- Назад</div></div>'; 
		html += '<div>';
		html += '<h2 class="ta_center">Для порта ДОП</h2>';
		html += '<p class="ta_center">Отключите все устройства от порта ДОП,</p>';
		html += '<p class="ta_center"> подключите только добавляемый датчик и нажмите кнопку«УСТ».</p>';
		html += '<p class="ta_center">Желтый индикатор загорится, а затем просигнализирует состояние добавляемого датчика:</p>';
		html += '<p class="ta_center">1 вспышка – датчик успешно добавлен,</p>';
		html += '<p class="ta_center">2 вспышки – датчик уже существует,</p>';
		html += '<p class="ta_center">3 вспышки – ошибка подключения датчика.</p>';
		html += '<br>';
		html += '<br>';
		html += '<h2 class="ta_center">Для порта Радио</h2>';
		html += '<p class="ta_center">Убедитесь, что датчик подключено к питанию, батарея вставлена правильно.</p>';
		html += '<p class="ta_center">Нажмите кнопку «УСТ», дождитесь загорания желтого индикатора,</p>';
		html += '<p class="ta_center">а затем кратковременно приложите магнит к датчику согласно инструкции для него.</p>';
		html += '<p class="ta_center">Убедитесь, что светодиод подключаемого датчика мигнул при поднесении магнита.</p>';
		html += '<p class="ta_center">Если светодиод мигнул три раза, программирование удалось. Если 2 раза – ошибка подключения</p>';	
		html += '<br>';
		html += '<br>';
		html += '<div class="ta_center"><div class="style_btn orange block_w_200p btn_port_dop_or_radio_add">УСТ</div></div>';
		html += '</div>';
		html += '<div></div>';
		printB.innerHTML = html;
	}
	
	function print_sensor_d1_d5 (){
		printB.innerHTML = '<div></div><div>'+HWhtml.loader()+'</div><div></div>';
		let dataP = {token:HWS.pageAccess,list_start:0,list_amount:100,id_system:idSystemD,iface_type:0,iface_port:3}
		HWF.post({url:HWD.api.gets.list,data:dataP,fcn:function(inf){
			let allD = {};
			for (let k in inf.list){allD[inf.list[k].info.channel_index] = inf.list[k].info.type.type;}
			let specSelect = {
				0:'Не настроен',
				1441:{name:'Датчик открытия двери'},
				1857:{name:'Датчик дыма'},
				1537:{name:'Датчик газа'},
				1665:{name:'Датчик протечки воды'},
				738:{name:'Датчик наличия напряжения сети'},
				1697:{name:'Датчик движения'},
				417:{name:'Универсальный датчик'},
				1761:{name:'Датчик давления'},
				1473:{name:'Датчик уровня'}
			}
			let selec1 = (allD[1])?allD[1]:false; let selec2 = (allD[2])?allD[2]:false; let selec3 = (allD[3])?allD[3]:false;
			let selec4 = (allD[4])?allD[4]:false; let selec5 = (allD[5])?allD[5]:false;
			let html = ''; 
			html += '<p id="set_d1" class="ta_center">Д1: '+HWhtml.select(specSelect,'',selec1)+'</p>';
			html += '</br>';
			html += '<p id="set_d2" class="ta_center">Д2: '+HWhtml.select(specSelect,'',selec2)+'</p>';
			html += '</br>';
			html += '<p id="set_d3" class="ta_center">Д3: '+HWhtml.select(specSelect,'',selec3)+'</p>';
			html += '</br>';
			html += '<p id="set_d4" class="ta_center">Д4: '+HWhtml.select(specSelect,'',selec4)+'</p>';
			html += '</br>';
			html += '<p id="set_d5" class="ta_center">Д5: '+HWhtml.select(specSelect,'',selec5)+'</p>';
			html += '</br></br>';
			html += '<div class="ta_center"><div class="style_btn orange block_w_200p btn_d1_d5_save">Сохранить</div></div>';
			
			printB.innerHTML = '<div><div class="button btn_back_start"> <- Назад</div></div><div>'+html+'</div><div></div>';
		}});
	}
	
	
	// Выбор Добовления через порт доп или радио
	HW.on('click','btn_port_dop_or_radio',function(e,elem){ print_dop_or_radio() });	
	// Назад на главную
	HW.on('click','btn_back_start',function(e,elem){ start_print() });	
	// НАжатие на кнопку УСТ
	HW.on('click','btn_port_dop_or_radio_add',function(e,elem){ 
		let dataP = {token:HWS.pageAccess,id_system:idSystemD,command:'ust'}
		console.log(dataP);
		HWF.post({url:HWD.api.gets.taskCommandTo3xSystem,data:dataP});
	});
	
	// Выбор Добовления Д1...Д5
	HW.on('click','btn_conection_d1_d5',function(e,elem){ print_sensor_d1_d5();	});
	
	// Сохранить Д1...Д5
	HW.on('click','btn_d1_d5_save',function(e,elem){
		let d1 = document.body.querySelector('#set_d1 select');
		let d2 = document.body.querySelector('#set_d2 select');
		let d3 = document.body.querySelector('#set_d3 select');
		let d4 = document.body.querySelector('#set_d4 select');
		let d5 = document.body.querySelector('#set_d5 select');
		let allD = [
			{channel_index:1,type:d1.value},
			{channel_index:2,type:d2.value},
			{channel_index:3,type:d3.value},
			{channel_index:4,type:d4.value},
			{channel_index:5,type:d5.value}
		];
		console.log(allD);
		let dataP = {token:HWS.pageAccess,id_system:idSystemD,devices:allD}
		HWF.post({url:HWD.api.gets.sendFor3xSystemSensorTypes,data:dataP,});
		
		printB.innerHTML = '<div><div class="button btn_back_start"> <- Назад</div></div><div>Данные сохранены</div><div><a href="/">На главную</a></div>';
	});
});