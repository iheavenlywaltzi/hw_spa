"use strict";
HWS.templateAdd(function(HW,B,H){
	let title = '<h1>Информация</h1>';
	let menu = '<div><a class="btn active" href="#message/list">Уведомления</a>';
		menu+= '<a class="btn active" href="#message/sets">Настройки</a> </div>';
		
	let content = '<div id="all-info-content-message" class="main-block ob-center max-1200 info-page">';
		content+= '</div>';
	let pageA = H.hash[1] || 'list';
	let LocalIdSys = H.hash[2] || false;
	let token = HWS.pageAccess;
	let M = {};

	B.title.append(title);
	B.menud.append(menu);
	B.content.append(content);

	let contentB = document.body.querySelector('#all-info-content-message');
	
	HWS.buffer.pageMessage = function(){
		contentB.innerHTML = HWhtml.loader();
		HWF.printList({name:'listNews',url:HWD.api.gets.newsList,target:contentB,
		data:{news_for_all:1},clas:'block_w_100 one-block nop',
		fcn:function(info){ //console.log(info);
			let html = '<ul class="news-list-page block_pt_20 block_pl_10">';
			HWF.recount(info.list,function(inf){
				let tim = HWF.unixTime(inf.news_time);
				
				html+= '<li>';
				if (inf.type_news == 3){
					console.log(inf);
					html+= '<div class="title ts_12"><b>'+inf.theme+'</b></div>';
					html+= '<div class="time tc_grey ts_08"> '+tim.fullDate+' </div>';
					html+= '<div class="desc">\
						<p><b>'+inf.data.email+'</b> хочет иметь возможность обмениваться с Вами правами доступа на оборудование. Приняв этот запрос, вы всего лишь подтверждаете такую возможность. Чтобы делиться своим оборудованием или управлять чужим, необходимо выполнить дополнительные настройки прав доступа.</p>\
						<p class="block_pt_4">'+inf.data.message+'</p>\
						<p class="block_pt_10" data-id="'+inf.data.id_user+'">\
						<span class="btn_text acept_friend_user">Принять<span>\
						<span class="btn_text block_ml_10 block_friend_user" href="">Отклонить</span>\
						</p>\
					</div>';
				}
				else{
					html+= '<div class="title ts_12"><a href="#info/news/'+inf.id+'"> '+inf.theme+' </a></div>';
					html+= '<div class="time tc_grey ts_08"> '+tim.fullDate+' </div>';
					html+= '<div class="desc"> '+inf.description+' </div>';
				}
				html+= '</li>';
			});
			html+= '</ul>'
			return html;
		}});
	}	
	
	HWS.buffer.pageSets = function(){
		contentB.innerHTML = HWhtml.loader();
		let topHtml = '<div class="flex_between block_w_100 block_p_20 box-sizing-box">'+
			HWhtml.search()+
			'<div class="block_w_150p"></div>'+
		'</div>';

		HWF.printList({url:HWD.api.gets.objectsList3xSystem,target:contentB,
		clas:'block_w_100 one-block nop',
		topHtml:topHtml,
		fcnOne:function(oneEl){let idSys = oneEl.list[0].id; HWS.go('message/sets/'+idSys); console.log(oneEl)},
		fcn:function(info){
			let html = '<ul class="list left">';
			html+='<li class="one-head">\
				<span class="l-name"><span class="block_mr_10 block_w_30p">Тип</span><span>Имя '+HWhtml.helper(HWD.helpText.pages.communicationName)+'</span></span>\
				<span class="l-name">id '+HWhtml.helper(HWD.helpText.pages.communicationID)+' </span>\
				<span class="l-btn">Связь '+HWhtml.helper(HWD.helpText.pages.communicationConnection)+'</span>\
				<span class="l-btn"></span>\
			</li>';
			HWF.recount(info.list,function(obj){ //console.log(obj);
				let S = {};
				S.connection = obj.connection;
				S.type = obj.cloud_type;
				S.nameHtml = '<span class="tc_black" >'+obj.config.name+'</span>';
				S.idHtml = '<span class="tc_black" >'+obj.info.ident+'</span>';
				S.btnSetsHtml = '<a class="icon icon-settings" href="#equipment/'+obj.id+'"></a>';
				S.printIcon = '';
				
				HWF.recount(S.connection,function(eleC){
					if(eleC.type == "wifi"){
						S.printIcon+= print_gsmwifi_icon('wifi',eleC.level,eleC.use_inet,obj.config.cfg_lk);
					}
					if(eleC.type == "gsm"){
						S.printIcon+= print_gsmwifi_icon('gsm',eleC.level,eleC.use_inet,obj.config.cfg_lk);
					}
					if(eleC.type == "gsm_roam"){
						S.printIcon+= print_gsmwifi_icon('gsm',eleC.level,eleC.use_inet,obj.config.cfg_lk);
					}
				});
				
				if(S.type == 14719){S.btnGraphHtml = ''; S.btnSendHtml = ''; S.printIcon = obj.lk.state_name || '';}
				
				html+= '<li class="one-elem" data-idsys="'+obj.id_system+'" data-id="'+obj.id+'">';
				html+= '<div class="l-name">\
					<span class="vertical-middle icon p30 icon_equip_'+S.type+' block_mr_10"></span>\
					<span class="vertical-middle text-name tw_700">'+S.nameHtml+'</span>\
				</div>';
				html+= '<div class="l-name tw_700">'+S.idHtml+'</div>';
				html+= '<div class="l-btn">'+
					S.printIcon+
				'</div>';

				html+= '<div class="l-btn ta_right">'+
					//S.btnSetsHtml+
					//'<span class="icon icon-delete hover btn_system_delete block_ml_8"></span>'+
				'</div>';
				if(S.type != 14719){html+= '<div class="click click_one_system"></div>';}
				
				html+= '</li>';
				
			});
			html+= '</ul>'
			return html;
		}});
		
	}
	
	HWS.buffer.pageSetsId = function(idSys){
		contentB.innerHTML = HWhtml.loader();
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:idSys},fcn:function(infSys){
			console.log(infSys);
			HWS.buffer.systelLocalId = infSys.id_system;
			HWS.buffer.system_object_id = infSys.system_object_id;
			let data = {token:token,id_system:infSys.id_system,local_id:655361}
			HWF.post({url:HWD.api.gets.objectLocalId,data:data,fcn:function(elem){
				HWS.buffer.notificId = elem.id;
				let print = '';
				let S = {};
					M.userTel = elem.config.usr_tel;
					M.gnotif = elem.config.cfg_gnotif;
					M.alive = elem.config.cfg_alive;
					M.recal = elem.config.cfg_recal;
					M.retnorm = elem.config.cfg_retnorm;
					M.hours = elem.config.cfg_atime[0];
					M.mins = elem.config.cfg_atime[1];
					M.connect = (elem.system_connection_status)?'':'on';

				S.users = [];
				HWF.recount(M.userTel,function(el,key){
					S.users.push({
						id:key,
						name:el.name || '',
						tel: el.phone_number || '',
						btn: {
							sms: el.sms_alarm ,
							auto: el.period_sms ,
							voice: el.call_alarm ,
							security: el.scurity_sms ,
						}
					});
				});
				
				S.state = (M.gnotif)?'active':'';
				S.auto = {}; S.auto[M.alive] = 'selected';
				S.callB = {}; S.callB[M.recal] = 'selected';
				S.alertsOpen = (M.alive)?'open':'';
				S.stateNorm = (M.retnorm)?'active':'';
				S.userEmail = localStorage.getItem('UserEmail') || '';
				
				let alerti = infSys.alert_type || 0;
				let selectInf = {
					0:{name:'не уведомлять'},
					1:{name:'Потеряна'},
					2:{name:'Восстановлена'},
					3:{name:'Любой статус'},
				}
				selectInf[alerti].selected = 'selected';
				
				print += '<div class="one-block-col">\
					<div class="title">\
						<h4><a href="#message/list" class="icon icon-back hover pointer block_mr_10" title="назад"></a> SMS и звонки '+HWhtml.helper(HWD.helpText.pages.messageSmsAndCool)+'</h4>\
					</div>\
					<div class="ltft"></div>\
					<div class="right">\
						<div class="colum">\
							<p>Оповещения системы</p>\
							<br>\
							<p><span class="on-off btn-active '+S.state+' btn_alerts_all_user"></span></p>\
						</div>\
						<div class="colum">\
							<p>Режим автооповещений</p>\
							<br>\
							<p>\
							<select class="open_control btn_auto_alerts block_w_170p">\
							<option '+S.auto[0]+' data-of="#time-auto-alerts" value="0">Нет оповещений</option>\
							<option '+S.auto[1]+' data-on="#time-auto-alerts" value="1">Раз в 1 день</option>\
							<option '+S.auto[2]+' data-on="#time-auto-alerts" value="2">Раз в 2 дня</option>\
							<option '+S.auto[3]+' data-on="#time-auto-alerts" value="3">Раз в 3 дня</option>\
							<option '+S.auto[4]+' data-on="#time-auto-alerts" value="4">Раз в 4 дня</option>\
							<option '+S.auto[5]+' data-on="#time-auto-alerts" value="5">Раз в 5 дней</option>\
							<option '+S.auto[6]+' data-on="#time-auto-alerts" value="6">Раз в 6 дней</option>\
							<option '+S.auto[7]+' data-on="#time-auto-alerts" value="7">Раз в неделю</option>\
							<option '+S.auto[9]+' data-on="#time-auto-alerts" value="9">2 раза в день</option>\
							</select>\
							</p>\
						</div>\
						<div class="colum">\
							<p>Если нет ответа на звонок</p>\
							<br>\
							<select class="btn_recal_tell block_w_170p">\
							<option '+S.callB[0]+' value="0">Не перезванивать</option>\
							<option '+S.callB[1]+' value="1">Перезвонить 1 раз</option>\
							<option '+S.callB[2]+' value="2">Перезвонить 2 раза</option>\
							<option '+S.callB[3]+' value="3">Перезвонить 3 раза</option>\
							<option '+S.callB[4]+' value="4">Перезвонить 4 раза</option>\
							<option '+S.callB[5]+' value="5">Перезвонить 5 раз</option>\
							</select>\
						</div>\
						<div class="colum">\
							<p>Оповещать об окончании тревоги</p>\
							<br>\
							<p><span class="on-off btn-active '+S.stateNorm+' btn_alerts_return_norm"></span></p>\
						</div>\
						<div id="time-auto-alerts" class="block-onof '+S.alertsOpen+'">\
							<div class="colum">\
								<p>Время автооповещений, ч</p>\
								<br>\
								<p>'+HWhtml.btn.numb(M.hours,{fcn:'setTime',id:'noti-time-h',min:'null',max:23})+'</p>\
							</div>\
							<div class="colum">\
								<p>Время автооповещений, мин</p>\
								<br>\
								<p>'+HWhtml.btn.numb(M.mins,{fcn:'setTime',id:'noti-time-m',min:'null',max:59})+'</p>\
							</div>\
						</div>\
					</div>\
				';
				
				print += '\
					<div class="title"><h4>Пользователи для SMS и звонков '+HWhtml.helper(HWD.helpText.pages.messageUserSmsAndCool)+'</h4></div>\
					<div class="ltft"></div>\
					<div class="right"></div>\
					<div class="title">\
						'+printUserSystem3x(S.users)+'\
					</div>\
					<div class="title">\
						<div class="block-pr-10 block-w-100 all-span-midle ta_right">\
							<span class="block-p-10">\
								<span class="icon icon-send-voice"></span>\
								<span class="text-grey">голосом</span>\
							</span>\
							<span class="block-p-10">\
								<span class="icon icon-send-sms"></span>\
								<span class="text-grey">по SMS</span>\
							</span>\
							<span class="block-p-10">\
								<span class="icon icon-send-sms-auto"></span>\
								<span class="text-grey">автоотчеты в SMS</span>\
							</span>\
							<span class="block-p-10">\
								<span class="icon icon-send-sms-secure"></span>\
								<span class="text-grey">SMS о состоянии охраны</span>\
							</span>\
						</div>\
					</div>\
					<div id="no-connect-block" class="no-connect-block '+M.connect+'"><div class="btn_no_connect tf_fr900 ts_24p btn_text black">Система этого объекта не на связи с ЛК. Почему???</div></div>\
				</div>';
				
				print += '<div class="one-block">\
				<div class="title"><h4>Уведомления на e-mail '+HWhtml.helper(HWD.helpText.pages.messageMesageEmail)+'</h4></div>\
				<div>Отправка происходит на e-mail: <b>'+S.userEmail+'</b></div>\
				<br>\
				<br>\
				<div class="title"><h4>Связь с системой</h4></div>\
				<div>'+HWhtml.select(selectInf,'select_alert_type block_w_140p block_p_4 block_mr_8')+'</div>\
				</div>';

				contentB.innerHTML = print
			}});
		}});
	}
	
	if(pageA == 'list' && !LocalIdSys){HWS.buffer.pageMessage();}
	if(pageA == 'sets' && !LocalIdSys){HWS.buffer.pageSets();}
	if(LocalIdSys){HWS.buffer.pageSetsId(LocalIdSys);}
	
	
	//** FCN **
	// распечатка иконок
	function print_gsmwifi_icon(type,signal,clasStatus,cfgLk){			
		if(clasStatus == 'connect'){clasStatus = 'green';}
		if(clasStatus == 'disconnect'){clasStatus = 'red';}
		if(clasStatus == 'not_use'){clasStatus = 'of';}
		let circle = '<span class="spec-circle-icon-state '+clasStatus+'"></span>';
		let iconColor = '';
		
		if(cfgLk == 1 && type == 'wifi'){circle=''; iconColor = 'grey'}
		if(cfgLk == 2 && type == 'gsm'){circle=''; iconColor = 'grey'}
		//wifi gsm
		let print = ''+
			'<span class="select-none position_relative block_mr_10">'+
				circle+
				'<span class="icon icon-'+type+'-'+signal+' '+iconColor+'"></span>'+
			'</span>';
		return print;
	};
	
		// спец лист пользывателей для старых систем  ☎ ✉ ◴ ☖
	function printUserSystem3x(list){ list = list || {};
		let S = {};
		S.print = '<ul id="user-tel-list" class="list no-hover block-w-100 list-user-system-3x">';
		S.startLi = '<li class="one-head">'+
			'<div class="block_w_40p">Ячейка</div>'+
			'<div class="sys3x-n"><span class="name">Имя</span></div>'+
			'<div class="sys3x-t">Номер</div>'+
			'<div class="sys3x-o">Типы оповещений</div>'+
			'<div class="sys3x-b"></div>'+
		'</li>';
		S.AllUser = '';
		for (let key in list){
			let user = list[key];
			let btn = {}
			let tel = user.tel; //.slice(1);
			
			btn.voice = ''; if(user.btn.voice){btn.voice = 'active'}
			btn.sms = ''; if(user.btn.sms){btn.sms = 'active'}
			btn.auto = ''; if(user.btn.auto){btn.auto = 'active'}
			btn.security = ''; if(user.btn.security){btn.security = 'active'}
			
			S.AllUser += ''+
			'<li id="one-user-'+user.id+'" class="one-user" data-num="'+user.id+'">'+
				'<div class="block_w_40p"><p class="ta_center">'+user.id+'</p></div>'+
				'<div class="sys3x-n">'+
					'<span class="name"><input class="enter_click_list_user" type="text" value="'+user.name+'" disabled ></span>'+
				'</div>'+
				'<div class="sys3x-t">\
					<input class="enter_click_list_user phone_inp_text block_w_130p" type="text" value="'+tel+'" disabled maxlength="20">\
				</div>'+
				'<div class="sys3x-o elements-around">'+
					'<span class="btn-active icon hover btn_onof_check icon-send-voice onof_voice '+btn.voice+'"></span>'+
					'<span class="btn-active icon hover btn_onof_check icon-send-sms onof_sms '+btn.sms+'"></span>'+
					'<span class="btn-active btn_onof_check icon hover icon-send-sms-auto onof_auto '+btn.auto+'"></span>'+
					'<span class="btn-active btn_onof_check icon hover icon-send-sms-secure onof_security '+btn.security+'"></span>'+
				'</div>'+
				'<div class="sys3x-b">'+
					'<span class="icon icon-clouse hover btn_user_tell_delete"></span>'+
				'</div>'+
			'</li>';
		}
		S.dopBtnPrint = '<div class="block_w_100 block_pt_20">'+
			'<span class="btn_text grey btn_add_user_system_3x block_pl_10">Добавить</span>'+
		'</div>';
		
		S.print = S.print + S.startLi + S.AllUser + '</ul>'+S.dopBtnPrint;
		return S.print;
	}
	
	
	//** ACTION **
	//Нажатие на удолить систему
	HW.on('click','btn_system_delete',function(e,el){
		HWS.buffer.liDeleteSystem = HWS.upElem(el,'li');
		let infoText = 'Ситстема и все ее оборудование <br> будут удалены из Вашего  аккаунта. Вы уверены?';
		HWF.modal.confirm('Удалить систему ?',infoText,'btn-delete-system-modal','Удалить');
		console.log(HWS.buffer.liDeleteSystem);
	});
	
	//Нажатие на 1 систему
	HW.on('click','click_one_system',function(e,el){
		let li = HWS.upElem(el,'.one-elem');
		let idsys = parseInt(li.dataset.id);
		contentB.innerHTML = HWhtml.loader();
		HWS.go('message/sets/'+idsys);
	});
	
	
	//** FCN **
	//Загрузка страници новостей
	function load_page_news(id){
		let data = {token:token,news_id:id}
		
		contentB.innerHTML = HWhtml.loader();
		
		HWF.post({url:HWD.api.gets.getNews,data:data,fcn:function(info){
			let time = HWF.unixTime(info.news_time);
			
			contentB.innerHTML = '';
			contentB.innerHTML = '\
				<div class="one-news-top block_w_100"><h2>'+info.theme+'</h2></div>\
				<div class="one-news-mid block_w_100">'+info.news_text+'</div>\
				<div class="one-news-bot block_w_100 tc_grey ts_08">'+time.full+'</div>\
			';
		}});
	}	
	//Загрузка Помощьи
	function load_faq(){
		contentB.innerHTML = '';
		contentB.innerHTML = 'Помощь'
	}
	//** -FCN- **
	
	//** ACTION **
	// Принят дружбу
	HWS.on('click','acept_friend_user',function(e,el){
		let pBlock = HWS.upElem(el,'p');
		let id = pBlock.dataset.id;
		HWF.push('Запрос принят');
		pBlock.innerHTML = HWhtml.loader();
		HWF.post({url:HWD.api.gets.userFriendCon,data:{token:token,id_user:id},fcn:function(info){
			console.log(info);
			if(info.success){pBlock.innerHTML = '<span class="tc_green">Запрос принят</span>'}
		}});
		
	});	
	// Отклонить дружбу
	HWS.on('click','block_friend_user',function(e,el){
		let pBlock = HWS.upElem(el,'p');
		let id = pBlock.dataset.id;
		HWF.push('Запрос отклонен','red');
		pBlock.innerHTML = HWhtml.loader();
		HWF.post({url:HWD.api.gets.userFriendAvo,data:{token:token,id_user:id},fcn:function(info){
			console.log(info);
			if(info.success){pBlock.innerHTML = '<span class="tc_red">Запрос отклонен</span>'}
		}});
	});
	
	// изменение оповещений для всех
	HW.on('click','btn_alerts_all_user',function(e,el){
		if(HWS.buffer.notificId){
			if(el.classList.contains('active')){M.gnotif = 1}
			else{M.gnotif = 0}
			let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.notificId,cfg_gnotif:M.gnotif}
			HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
			//change_state();
		}
	});
	
	// Оповещения о возврате в норму
	HW.on('click','btn_alerts_return_norm',function(e,el){
		if(HWS.buffer.notificId){
			if(el.classList.contains('active')){M.retnorm = 1;}
			else{ M.retnorm = 0; }
			let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.notificId,cfg_retnorm:M.retnorm}
			HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
		}
	});
	
	// изменение Режим автооповещений
	HW.on('change','btn_auto_alerts',function(e,el){
		if(HWS.buffer.notificId){
			M.alive = el.value;
			let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.notificId,cfg_alive:M.alive}
			HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
		}
	});
	
	// изменение Если нет ответа на звонок
	HW.on('change','btn_recal_tell',function(e,el){
		if(HWS.buffer.notificId){
			M.recal = el.value;
			let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.notificId,cfg_recal:M.recal}
			HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
		}
	});
	
	//Изменение Минут и часов автооповещений
	HWS.buffer.setTime = function(n){
		if(HWS.buffer.notificId){
			let H = document.body.querySelector('#noti-time-h input').value;
			let M = document.body.querySelector('#noti-time-m input').value;
			clearTimeout(HWS.buffer.setMinsTimer);
			HWS.buffer.setMinsTimer = setTimeout(function(){
				let time = [H,M];
				let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.notificId,cfg_atime:time}
				HWF.post({url:HWD.api.gets.taskNotificationProgram3xSystem,data:dataP});
			},2000);
		}
	}
	
	// ВКЛ\ОТКЛ оповещение
	HW.on('click','btn_onof_check',function(e,el){
		//C.users = 1;
		let li = HWS.parentSearchClass(el,'one-user');
		let n = li.dataset.num;
		let tel = li.querySelector('.sys3x-t input').value;
		let voice = (li.querySelector('.onof_voice').classList.contains('active'))?1:0;
		let sms = (li.querySelector('.onof_sms').classList.contains('active'))?1:0;
		let auto = (li.querySelector('.onof_auto').classList.contains('active'))?1:0;
		let def = (li.querySelector('.onof_security').classList.contains('active'))?1:0;

		let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.notificId,
			number:n,
			phone:tel,
			sms_alarm:sms,
			call_alarm:voice,
			period_sms:auto,
			scurity_sms:def
		}
		HWF.post({url:HWD.api.gets.taskPhoneNotificationProgram3xSystem,data:dataP});
	});
	
	// Кнопка удалить пользователя
	HW.on('click','btn_user_tell_delete',function(e,el){
		let li = HWS.parentSearchClass(el,'one-user');
		let liN = li.dataset.num;
		let tel = li.querySelector('.sys3x-t input').value;
		let name = li.querySelector('.sys3x-n .name input').value;
		HWS.buffer.idDEletePhone = liN;
		//HWF.modal.confirm('Удалить ?<br>'+name+'  '+tel,'Удаление номера','btn_delete_user');
		HWF.modal.confirm('Удаление номера','<p>Удалить номер <b>'+tel+'</b>?</p> Звонки и SMS на него от системы приходить не будут.','btn_delete_user');
	});
	HW.on('click','btn_delete_user',function(e,el){ //
		document.querySelector('#one-user-'+HWS.buffer.idDEletePhone).remove();
		let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.notificId,number:HWS.buffer.idDEletePhone,}
		HWF.post({url:HWD.api.gets.taskDelPhoneNotificationProgram3xSystem,data:dataP});
	});
	
	// Кнопка добавить нового пользователя
	HW.on('click','btn_add_user_system_3x',function(e,el){
		let html = '';
		
		html += '<ul class="dop-inp-info ta_center">'
		html += '<li><span>Имя:</span><input id="noti-new-name" type="text" value=""></li>'
		html += '<li><p class="block_pt_10 ta_center"></p></li>'
		html += '<li>\
			<span>Телефон: 7 123 45 67 890</span> \
			<div>\
				<input class="phone_inp_text" id="noti-new-tel" type="text" maxlength="20" value="">\
			</div>\
		</li>'
		html += '<li><p class="block_pt_10 ta_center"></p></li>'
		html += '<li><span>Оповещения:</span><div class="sys3x-o elements-around">'+
			'<span id="noti-new-voice" class="btn-active icon hover block_m_4 icon-send-voice"></span>'+
			'<span id="noti-new-sms"   class="btn-active icon hover block_m_4 icon-send-sms"></span>'+
			'<span id="noti-new-auto"  class="btn-active icon hover block_m_4 icon-send-sms-auto"></span>'+
			'<span id="noti-new-def"   class="btn-active icon hover block_m_4 icon-send-sms-secure"></span>'+
		'</div></li>';
		html += '</ul>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<p id="error-mesage-modal" class="tc_red block_h_18p"></p>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<div class="style_btn orange block_w_200p btn_add_new_tel">Добавить</div>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<p class="block_pt_10 ta_center"></p>'
		html += '<div class="block-pr-10 block-w-100 all-span-midle ta_center">\
			<span class="block-p-10">\
				<span class="icon icon-send-voice"></span>\
				<span class="text-grey">голосом</span>\
			</span>\
			<span class="block-p-10">\
				<span class="icon icon-send-sms"></span>\
				<span class="text-grey">по SMS</span>\
			</span>\
			<span class="block-p-10">\
				<span class="icon icon-send-sms-auto"></span>\
				<span class="text-grey">автоотчеты в SMS</span>\
			</span>\
			<span class="block-p-10">\
				<span class="icon icon-send-sms-secure"></span>\
				<span class="text-grey">SMS о состоянии охраны</span>\
			</span>\
		</div>';
		
		HWF.modal.print('Добавить пользователя',html);
	})//Добавление нового номера
	HW.on('click','btn_add_new_tel',function(e,el){
		let errorB = document.body.querySelector('#error-mesage-modal');
		let name = document.body.querySelector('#noti-new-name').value;
		let voice = (document.body.querySelector('#noti-new-voice').classList.contains('active'))?1:0;
		let sms = (document.body.querySelector('#noti-new-sms').classList.contains('active'))?1:0;
		let auto = (document.body.querySelector('#noti-new-auto').classList.contains('active'))?1:0;
		let def = (document.body.querySelector('#noti-new-def').classList.contains('active'))?1:0;
			errorB.innerHTML = '';
		let number = document.body.querySelector('#noti-new-tel').value;

		let n=0;
		while(M.userTel[n]){n++;}
		
		if (n <= 9) {
			let list = document.body.querySelector('.list-user-system-3x');
			let newLi = document.createElement('li');
				newLi.classList.add('one-user')
				newLi.id = 'one-user-'+n;
				newLi.dataset.num = n;
			let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.notificId,
				number:n,
				name:name,
				phone:number,
				sms_alarm:sms,
				call_alarm:voice,
				period_sms:auto,
				scurity_sms:def
			}
			HWF.post({url:HWD.api.gets.taskPhoneNotificationProgram3xSystem,data:dataP,fcnE:HWF.getServMesErr});
			
			voice = (voice)?'active':''; sms = (sms)?'active':''; auto = (auto)?'active':''; def = (def)?'active':'';
			newLi.innerHTML = ''+
			'<div class="block_w_40p"><p class="ta_center">'+n+'</p></div>'+
			'<div class="sys3x-n">'+
				'<span class="name"><input class="enter_click_list_user " type="text" value="'+name+'" disabled ></span>'+
			'</div>'+
			'<div class="sys3x-t">\
				<input class="block_w_130p phone_inp_text enter_click_list_user" type="text" value="'+number+'" disabled maxlength="20">\
			</div>'+
			'<div class="sys3x-o elements-around">'+
				'<span class="btn-active icon hover btn_onof_check icon-send-voice onof_voice '+voice+'"></span>'+
				'<span class="btn-active icon hover btn_onof_check icon-send-sms onof_sms '+sms+'"></span>'+
				'<span class="btn-active btn_onof_check icon hover icon-send-sms-auto onof_auto '+auto+'"></span>'+
				'<span class="btn-active btn_onof_check icon hover icon-send-sms-secure onof_security '+def+'"></span>'+
			'</div>'+
			'<div class="sys3x-b">'+
				//'<span class="icon icon-change hover btn_user_tell_cange block_mr_8"></span>'+
				'<span class="icon icon-clouse hover btn_user_tell_delete"></span>'+
			'</div>';
			
			list.appendChild(newLi);
			HWF.modal.off();
		}else {
			errorB.innerHTML = 'Все ячейки для номеров заняты';
			setTimeout(function(){errorB.innerHTML = '';},3000);
		}
	});
	
	// Нажатие на кнопку если оборудование не на связи
	HWS.on('click','btn_no_connect',function(e,el){
		let gosystemId = HWS.buffer.system_object_id || '';
		if(gosystemId){gosystemId = 'equipment/'+gosystemId}
		else {gosystemId = '';}
		HWF.modal.confirmCust({
			title:'Нет связи с системой',
			print:'Система, с которой связано данное устройство или программа,<br> ушла со связи с ЛК. Когда она снова выйдет на связь,<br> все настройки снова будут доступны.<br><br> Проверьте связь с системой',
			no:'ЗАКРЫТЬ',
			yes:'ПРОВЕРИТЬ',
			yesHref:gosystemId,
		});
	});
	
	//Изменение вида оповещения 
	HW.on('change','select_alert_type',function(e,el){
		let val = parseInt(el.value);
		let data = {token:token,list:[{id_system:HWS.buffer.systelLocalId,alert_type:val}]}
		HWF.post({url:HWD.api.gets.changeMesag3xSystem,data:data,fcn:function(info){}});
	});
	
		// Редактирование пользователя
	function clouse_active(liActive){let ret = 'of'; if(liActive){
		let tel = liActive.querySelector('.sys3x-t input');
		let name = liActive.querySelector('.sys3x-n .name input');
		let number = tel.value || '';

		liActive.classList.remove('redact');
		tel.classList.remove('red');
		name.disabled = true;
		tel.disabled = true;
		ret = 'on';
		
		let n = liActive.dataset.num;
		let voice = (liActive.querySelector('.onof_voice').classList.contains('active'))?1:0;
		let sms = (liActive.querySelector('.onof_sms').classList.contains('active'))?1:0;
		let auto = (liActive.querySelector('.onof_auto').classList.contains('active'))?1:0;
		let def = (liActive.querySelector('.onof_security').classList.contains('active'))?1:0;
		let dataP = {token:HWS.pageAccess,id_object:HWS.buffer.notificId,
			number:n,
			name:name.value,
			phone:number,
			sms_alarm:sms,
			call_alarm:voice,
			period_sms:auto,
			scurity_sms:def
		}
		HWF.post({url:HWD.api.gets.taskPhoneNotificationProgram3xSystem,data:dataP});
		
	} return ret; }
	
	HWS.buffer.globalClick = function(e){
		let liActive = HWS.getElem('#user-tel-list .one-user.redact');
		
		if(e.target.classList.contains('enter_click_list_user')){
			let li = HWS.upElem(e.target,'.one-user');
			
			if(!li.classList.contains('redact')){
				let tel = li.querySelector('.sys3x-t input');
				let name = li.querySelector('.sys3x-n .name input');
				let controller = '';
				li.dataset.nb = tel.value;
				controller = clouse_active(liActive);

				if(controller != 'error'){
					li.classList.add('redact');
					name.disabled = false;
					tel.disabled = false;
				}
			}
		} else {
			clouse_active(liActive);
		}
	}
	// Ввод тел номеров 
	HWS.on('click','phone_inp_text',function(e,el){if(!el.value){el.value = '+'}});
	
	// Выделяет активный пукт дашбоардного меню, присваевает класс "актив" если 2й тег соответствует 2му тегу ссылки
	HWF.activeDasboartMenu();
});