"use strict";
HWS.template.main = function(HW,B,H){ // ШАБЛОН и его функции. для вызываемой страници
	HWF.clearBlocks();//очистим блоки контена
	let listD = {}; // для синхронизации по сокетам
	let token = HWS.pageAccess;
	let mainP = localStorage.getItem('PageMain') || 'proj';
	
	B.content.append('<span>'+HWhtml.loader()+'</span>');
	
	let	menu = '<div>';
		menu+= 		'<span class="btn btn_dash_click project_btn" data-fcn="pageProj">Проекты</span>';
		menu+= 		'<span class="btn btn_dash_click vijet_btn" data-fcn="pageVijet">Виджеты</span>';
		menu+= '</div>';
		menu+= '<div id="spec-dop-mini-menu"></div>';
	
	B.menud.append(menu);

	// Загрузка Проекта
	function load_project(info){
		let type = info.project_type;
		let name = info.name;
		let id = info.id;
		HWS.buffer.elemLoadInfo = info;
		B.title.append('<h1>'+name+'</h1>');

		if(id != null){
			if(type == 1){HWmain.loadImg(info)};
			if(type == 2){HWmain.loadMap(info)};
			if(type == 3){HWmain.loadGrid(info)};
		}
		else {
			HWS.blocks.title.append('<h1>Проекты</h1>');
			let RTmenu = document.querySelector('#spec-dop-mini-menu');
			RTmenu.innerHTML = ''+
				'<a href="#project-add" class="btnIcon">				<span class="select-none">Добавить проект</span>				<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>			</a>'+
			'';
			
			let print = '<div id="no-tile-proj" class="flex_center">\
				<div>\
					<p class="block_pb_20"><img src="img/add_project_new.png"></p>\
					<p class="block_pb_10 ts_24p tf_mb tc_greyС9">Добавьте проект!</p>\
					<p class="tc_greyС9">У вас пока нет проектов, добавьте <br> ваш первый проект, нажав на кнопку <br> в правом верхнем углу экрана.</p>\
				</div>\
			</div>';
			HWS.blocks.content.append(print);
		};
	}
	
	
	HWS.buffer.pageProj = function(type){
		localStorage.setItem('PageMain','proj')
		let RTmenu = document.querySelector('#spec-dop-mini-menu');
		RTmenu.innerHTML = '';
		B.content.append('<span>'+HWhtml.loader()+'</span>');
		if (localStorage.getItem('PITn0Hxg') && !token){HW.go('god');}
		else{HWF.post({url:'projects/get_main_project',data:{token:token},fcn:load_project});}
		
		HW.on('click','rm-add-obj',HWmain.modal.obj);
		HW.on('click','rm-add-equip',HWmain.modal.equip);
		HW.on('click','rm-add-prog',HWmain.modal.prog);
		HW.on('click','rm-add-grup',HWmain.modal.grup);
		HW.on('pointerdown','container',HWmain.ofRMenu,{method:'m'});
	}
		
	HWS.buffer.pageVijet = function(type){
		localStorage.setItem('PageMain','fast')
		let RTmenu = document.querySelector('#spec-dop-mini-menu');
		RTmenu.innerHTML = '';
		clearTimeout(HWS.buffer.specTimeControllerId);
		HWS.load('vijets');
	}
	
	if (mainP == 'proj'){
		let btnProj = B.menud.elem.querySelector('.project_btn');
		let btnVije = B.menud.elem.querySelector('.vijet_btn');
			btnProj.classList.add('active');
			btnVije.classList.remove('active');
		HWS.buffer.pageProj();
	}
	
	if (mainP == 'fast'){
		let btnProj = B.menud.elem.querySelector('.project_btn');
		let btnVije = B.menud.elem.querySelector('.vijet_btn');
			btnProj.classList.remove('active');
			btnVije.classList.add('active');
		HWS.buffer.pageVijet();
	}
}
// Добавим шаблон на 404 ( несуществование страници )
HWS.template["404"] = function(){
	let title = '<h1>Страница не найдена</h1>';
	let menu = '<a href="#main" title="Назад">&#8617;</a>';
	let content = '<h2>Запрашиваемая вами страница, не найдена!</h2>';
	
	HWS.blocks.title.append(title);
	HWS.blocks.menud.append(menu);
	HWS.blocks.content.append(content);
}