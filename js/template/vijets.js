"use strict";
HWS.templateAdd(function(HW,B,H){
	//HWF.clearBlocks();//очистим блоки контена 
	let title = '<h1>Быстрый Доступ</h1>';
	let content = '<div class="main-block ob-left-top fast-menu">'+HWhtml.loader()+'</div>';
	let token = HWS.pageAccess;
	let tileId = 0;
	let RTmenu = document.querySelector('#spec-dop-mini-menu');
	let fcnGraph = false;
	HW.load('vijets-create'); // функции создания виджета

	B.title.append(title);
	B.content.append(content);
	
	if(!RTmenu){RTmenu = document.querySelector('.dasboart-menu');}
	RTmenu.innerHTML = ''+
		'<span class="btn_add_fast_vijet btnIcon">\
				<span class="select-none">Добавить виджет</span>\
				<span class="spec-img-add select-none"><span class="icon icon-add select-none" title="Добавить"></span></span>\
		</span>'+
	'';
	
	//'<span class="btn_add_fast_tile btnIcon"> Добавить проект <span class="icon icon-add select-none" title="Добавить"></span></span>'+
	HWF.post({url:'objects/list_favorite_objects',data:{token:token},fcn:controller});
	
	function controller(info){
		if(HWS.buffer.createTileCheck) {
			load_page(info); 
			HW.load('vijets-reload'); // Авто обновление виджетов.
		}
		else {setTimeout(function(){controller(info)},200);}
	}
	
	function load_page(info){ console.log(info);
		HWS.buffer.stopReolad = true; // отменяем автообновление на 1 шаг.
		HWS.buffer.analog_vijet_graph = {};
		let list = info.list
		if(list.length > 0){
			let mainBlock = document.querySelector('.fast-menu'); mainBlock.innerHTML = '';
			let mainPage = document.querySelector('.main-page');
			let fragment = document.createDocumentFragment();
			
			mainBlock.innerHTML = '';
			HWS.buffer.tileGraph = {};
			HWF.recount(list,function(info){ fragment.appendChild(HWS.buffer.createTile(info)) });
			mainBlock.appendChild(fragment);
			$('.fast-menu').sortable({  // для перемещения
				handle: '.left',
				beforeStop: function(e,ui) {
					let token = HWS.pageAccess;
					let newPos = [];
					let allVij = document.body.querySelectorAll('.global-main .main-block div.one-tile');
					HWF.recount(allVij,function(oneVij){
						let id = oneVij.dataset.id || false;
						if(id){newPos.push(parseInt(id));}
					},'on')
					
					if(newPos.length > 0){
						let data = {
							token:token,
							new_sort_order:newPos,
						}
						HWF.post({url:"objects/change_sort_number_favorite_object",data:data});
					}
				},
			});	
			//setTimeout(function(){HWS.buffer.loaderGraph(HWS.buffer.tileGraph)},10000);
			HWS.buffer.loaderGraph(HWS.buffer.tileGraph);
		}else {
			let mainBlock = document.querySelector('.fast-menu');
			//let mainPage = document.querySelector('.main-page');
			
			mainBlock.innerHTML = '<div id="no-tile-vijet" class="flex_center">\
				<div>\
					<p class="block_pb_20"><img src="img/add_vijet_new.png"></p>\
					<p class="block_pb_10 ts_24p tf_mb tc_greyС9">Добавьте виджет!</p>\
					<p class="tc_greyС9">У вас пока нет виджетов, добавьте <br> ваш первый виджет, нажав на кнопку <br> в правом верхнем углу экрана.</p>\
				</div>\
			</div>';
		}
		HWF.modal.of();
	}

	HW.on('click','btn_add_fast_vijet',function(e,el){
		HWS.data.apiGet.listVijet.data.search_text = '';
		let getServer = {}
		getServer.search_text = '';
		HWS.buffer.programListDopGetServer = getServer;
		HWS.data.apiGet.systemVijet.data.search_text = '';
		
		let rMenu = document.querySelector('.right-click-menu');

		if(rMenu){
			rMenu.innerHTML = HWhtml.rMenuVijet();
		}
		else {
			rMenu = document.createElement('div');
			rMenu.classList.add('right-click-menu');
			rMenu.innerHTML = HWhtml.rMenuVijet();
			document.body.appendChild(rMenu);
		}
		
		let xr = (e.clientX-200);
		let yr = e.clientY;

		if (rMenu.style.top == yr+'px' && rMenu.style.left == xr+'px'){
			if (rMenu.classList.contains('active')){rMenu.classList.remove('active');}
			else {rMenu.classList.add('active');}
		}else {
			rMenu.classList.add('active');
			rMenu.style.top = yr+'px';
			rMenu.style.left = xr+'px';
		} 
		
	});
	
	function reolad_info_relay(block,id){
		HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:id},fcn:function(infoEl){
			block.innerHTML = infoEl.lk.state_name || '';
		}});
	}
	
	HW.on('click','blamba-btn',function(e,el){
		let block = HWS.upElem(el,'.one-tile');
		let id = block.dataset.id;
		let textBlock = block.querySelector('section .state .state-info');
		if(el.classList.contains('btn-active')){
			if(el.classList.contains('active')){
				let dataP = {token:HWS.pageAccess,id_object:id,state:1}
				HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP,fcn:function(){reolad_info_relay(textBlock,id)}});
			}
			else{
				let dataP = {token:HWS.pageAccess,id_object:id,state:0}
				HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP,fcn:function(){reolad_info_relay(textBlock,id)}});
			}
		}
	});
	HW.on('click','btn_add_fast_tile',HWmain.btnAddElem); //Кнопка Добавить виджет
	// add click
	HW.on('click','vjet-add-equip',function(){
		let print = ''+
		'<div class="filter block_w_100 ta_center">'+HWhtml.search('','search_equp_vijet')+'</div>'+
		'<div class="table-content block_w_100 block_h_85 all-equipments"></div>'+
		'<div class="style_btn orange add_more_equp_btn block_w_168p">Добавить</div>'+
		'<div class="block_p_15"></div>'+
		'';
		HWF.modal.print('Добавить Оборудование',print);
		HWF.print.listVijet('.modal-window .modal-section .table-content');
		HWmain.ofRMenu();
				
		// выбор сазу всех элементов
		HWS.on('click','equp_select_all',function(event,elem){
			HWF.allActive('.list-equipments','.checkbox');
		});
	}); // Кнопка ( в малом меню) добавить Оборудование
	// Тык в 1 ли
	HWS.on('click','btn-one-equip',function(e,el){
		let li = HWS.upElem(el,'li');
		let check = li.querySelector('.checkbox');
		if(check.classList.contains('active')){check.classList.remove('active')}
		else{check.classList.add('active')}
	});
	
	HW.on('click','add_more_equp_btn',function(){ // Добавить МНОГО оборудования
		let glushka = document.querySelector('#no-tile-vijet');
		let allActive = document.body.querySelectorAll('.modal-section .table-content .list-equipments .checkbox.active');
		let addElem = [];
		if(allActive.length > 0){
			if (glushka){glushka.remove();}
			HWF.recount(allActive,function(elem){
				let elemLi = HWS.upElem(elem,'.one-equipment');
				let id = elemLi.dataset.id;
				addElem.push(id);
			},'on');
			HWF.modal.print('Добавление...',HWhtml.loader())
			HWF.post({url:HWD.api.gets.vijetAdd,data:{token:token,id_objects:addElem},fcn:function(info){
				HWF.post({url:'objects/list_favorite_objects',data:{token:token},fcn:load_page});
			}});
		}
	});
	

	// добавить МНОГО проектов
	HW.on('click','add_more_proj_btn',function(){
		console.log('add PROJECT');
	});	
	
	// Кнопка добавить программу
	HW.on('click','vjet-add-prog',function(){
		let print = ''+
		'<div class="filter block_w_100 ta_center">'+HWhtml.search('','search_prog_vijet')+'</div>'+
		'<div class="table-content block_w_100 block_h_85 modal-programs"></div>'+
		'<div class="style_btn orange add_more_prog_btn block_w_168p">Добавить</div>'+
		'<div class="block_p_15"></div>'+
		'';
		
		HWF.modal.print('Добавить Программу',print);
		HWprog.list('.modal-window .modal-section .table-content','of','checkbox','objects/list_programs_for_add_to_favorite');
		HWF.modal.on();HWmain.ofRMenu();
		
		// выбор сазу всех элементов
		HWS.on('click','prog_select_all',function(event,elem){
			HWF.allActive('.list-programs','.checkbox');
		});
	});
	// добавить МНОГО Программ
	HW.on('click','add_more_prog_btn',function(){
		let allActive = document.body.querySelectorAll('.modal-section .table-content .list-programs .checkbox.active');
		let addElem = [];
		if(allActive.length > 0){
			//if (glushka){glushka.remove();}
			HWF.recount(allActive,function(elem){
				let elemLi = HWS.upElem(elem,'.one-program');
				let id = elemLi.dataset.id;
				addElem.push(id);
			},'on');
			HWF.modal.print('добавление...',HWhtml.loader())

			HWF.post({url:HWD.api.gets.vijetAdd,data:{token:token,id_objects:addElem},fcn:function(info){
				HWF.post({url:'objects/list_favorite_objects',data:{token:token},fcn:load_page});
			}});
		}
	});
	
	// Кнопка добавить Систему
	HW.on('click','vjet-add-system',function(){
		let print = ''+
		'<div class="filter block_w_100 ta_center">'+HWhtml.search('','search_system_vijet')+'</div>'+
		'<div class="table-content block_w_100 block_h_85 modal-programs"></div>'+
		'<div class="style_btn orange add_more_system_btn block_w_168p">Добавить</div>'+
		'<div class="block_p_15"></div>'+
		'';
		
		HWF.modal.print('Добавить систему',print);
		HWF.print.systemVijet('.modal-window .modal-section .table-content');
		HWmain.ofRMenu();
		
		// выбор сазу всех элементов
		HWS.on('click','equp_select_all',function(event,elem){
			HWF.allActive('.list-equipments','.checkbox');
		});
	});
	// добавить МНОГО Систем
	HW.on('click','add_more_system_btn',function(){
		let allActive = document.body.querySelectorAll('.modal-section .table-content .list-equipments .checkbox.active');
		let addElem = [];
		if(allActive.length > 0){
			HWF.recount(allActive,function(elem){
				let elemLi = HWS.upElem(elem,'.one-equipment');
				let id = elemLi.dataset.id;
				addElem.push(id);
			},'on');
			HWF.modal.print('добавление...',HWhtml.loader())

			HWF.post({url:HWD.api.gets.vijetAdd,data:{token:token,id_objects:addElem},fcn:function(info){
				HWF.post({url:'objects/list_favorite_objects',data:{token:token},fcn:load_page});
			}});
		}
	});
	
	/* PROGRAMS Click */
	HW.on('click','btn_security_vijet_click',function(e,el){
		let vijBlock = HWS.upElem(el,'.one-tile');
		let textPole = vijBlock.querySelector('.mid-info .state-info');
		let icon = vijBlock.querySelector('.mid-info .icon'); 
		let idProg = vijBlock.dataset.id;
		let textInfo = HWD.equp[32769].state;
		
		if(el.classList.contains('active')){
			//textPole.innerHTML = textInfo[0].name;
			icon.classList.remove('green');
			el.classList.remove('active');
			el.innerHTML = 'Поставить на охрану';
			
			let dataP = {token:token,id_object:idProg,security_active:0}
			HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP,fcn:reoladText});
		}
		else {
			//textPole.innerHTML = textInfo[1].name;
			icon.classList.add('green');
			el.classList.add('active');
			el.innerHTML = 'Снять с охраны';
			
			let dataP = {token:token,id_object:idProg,security_active:1}
			HWF.post({url:HWD.api.gets.taskSecurityProgram3xSystem,data:dataP,fcn:reoladText});
		}
		
		function reoladText(){
			HWF.post({url:HWD.api.gets.object,data:{token:token,id_object:idProg},fcn:function(elem){
				textPole.innerHTML = elem.lk.state_name;
			}});
		}
	});
	// нажатие на Отопление ( эконом стандарт комфорт )
	HW.on('click','btn_heating_click_tmp',function(e,el){
		let vijBlock = HWS.upElem(el,'.one-tile');
		let block = HWS.upElem(el,'.state');
		let val = el.dataset.val;
		let active = block.querySelector('.style_btn.active');
		let idProg = vijBlock.dataset.id;
		let custom = vijBlock.querySelector('section .state .btn-int');
		let tmpCor = vijBlock.querySelector('.tmp_correction');
		
		custom.classList.remove('green');
		custom.classList.add('grey');
		
		if(active){active.classList.remove('active');}
		el.classList.add('active');
		
		tmpCor.innerHTML = parseFloat(val).toFixed(1);
		
		let dataP = {token:token,id_object:idProg,pau_therm_value:val,control_from_schedule:0}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	}); 
	// нажатие на Отопление ( расписание )
	HW.on('click','btn_heating_click_timetable',function(e,el){
		let vijBlock = HWS.upElem(el,'.one-tile');
		let block = HWS.upElem(el,'.state');
		let active = block.querySelector('.style_btn.active');
		let idProg = vijBlock.dataset.id;
		let custom = vijBlock.querySelector('section .state .btn-int');
		let tmpCor = vijBlock.querySelector('.tmp_correction');
		
		custom.classList.remove('green');
		custom.classList.add('grey');
		
		if(active){active.classList.remove('active');}
		el.classList.add('active');
		
		tmpCor.innerHTML = '--';
		
		let dataP = {token:token,id_object:idProg,control_from_schedule:1}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	});
	// нажатие на Отопление ( Своё значение )
	HW.on('click','btn_click_custom',function(e,el){
		let vijBlock = HWS.upElem(el,'.one-tile');
		let block = HWS.upElem(el,'.state');
		let active = block.querySelector('.style_btn.active');
		let custom = vijBlock.querySelector('section .state .btn-int');
		let idProg = vijBlock.dataset.id;
		let val = el.value;
		let tmpCor = vijBlock.querySelector('.tmp_correction');
		
		if(active){active.classList.remove('active');}
		el.classList.add('active');
		
		custom.classList.add('green');
		custom.classList.remove('grey');
		
		tmpCor.innerHTML = parseFloat(val).toFixed(1);;
		
		let dataP = {token:token,id_object:idProg,pau_therm_value:val,control_from_schedule:0}
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	});
	HWS.buffer.btnClickCustom = function(inf,el){
		let vijBlock = HWS.upElem(el,'.one-tile');
		let block = HWS.upElem(el,'.state');
		let active = block.querySelector('.style_btn.active');
		let custom = vijBlock.querySelector('section .state .btn-int');
		let idProg = vijBlock.dataset.id;
		let val = inf;
		let tmpCor = vijBlock.querySelector('.tmp_correction');
		
		if(active){active.classList.remove('active');}
		el.classList.add('active');
		custom.classList.add('green');
		custom.classList.remove('grey');
		
		tmpCor.innerHTML = parseFloat(val).toFixed(1);;
		
		let dataP = {token:token,id_object:idProg,pau_therm_value:val,pau_therm_pre_usr:val,control_from_schedule:0}
		console.log(dataP);
		HWF.post({url:HWD.api.gets.taskHeatingProgram3xSystem,data:dataP});
	}
		
	// delete click
	HW.on('click','btn_ib_clouse',function(e,el){
		let block = HWS.upElem(el,'.one-tile');
		HWS.buffer.deleteFaseElem = block;
		let name = block.querySelector('header .left .name').innerHTML;
		
		HWF.modal.confirm('Удалить виджет?','Вы действительно хотите удалить <b>'+name+'</b> из виджетов?','del_one_vijet_for_page');
	});
	HW.on('click','del_one_vijet_for_page',function(){
		HWS.buffer.stopReolad = true;
		let block = HWS.buffer.deleteFaseElem;
		let id = block.dataset.id;
		let mainBlock = document.querySelector('.fast-menu');
		HWF.post({url:'objects/delete_favorite_objects',data:{token:token,id_objects:[id]}});
		block.remove();

		let testBlock = mainBlock.querySelector('.one-tile');
		if(!testBlock){
			mainBlock.innerHTML = '<div id="no-tile-vijet" class="flex_center">\
				<div>\
					<p class="block_pb_20"><img src="img/add_vijet_new.png"></p>\
					<p class="block_pb_10 ts_24p tf_mb tc_greyС9">Добавьте виджет!</p>\
					<p class="tc_greyС9">У вас пока нет виджетов, добавьте <br> ваш первый виджет, нажав на кнопку <br> в правом верхнем углу экрана.</p>\
				</div>\
			</div>';
		}
	});
	
	// prog alarm click rebout relay
	HW.on('click','btn_rebout_relay_prog_alarm',function(e,el){
		let block = HWS.upElem(el,'.one-tile');
		let invers = block.dataset.invers;
		let idDivice = block.dataset.iddivice;
		
		if(idDivice){
			let clickVal = 0;
			if(invers != 0) {clickVal = 1}
			
			let dataP = {token:HWS.pageAccess,id_object:idDivice,state:clickVal}
			HWF.post({url:HWD.api.gets.sendDeviceState,data:dataP});
			HWF.push('Отправлен запрос на cброс тревоги');
		}
	});	
	// Сделать снимок фотокамера
	HW.on('click','btn_shot_camera',function(e,el){
		let block = HWS.upElem(el,'.one-tile');
		let id = block.dataset.id;
		let dataP = {token:HWS.pageAccess,id_object:id}
		
		HWF.post({url:'task/photo_from_3x_camera',data:dataP});
		HWF.push('Отправлен запрос на получение фото снимка');
	});	
	// Обновить баланц в системе
	HW.on('click','click_system_retyrn_balance',function(e,el){
		let block = HWS.upElem(el,'.one-tile');
		let id = block.dataset.id;
		let dataP = {token:HWS.pageAccess,id_object:id,command:'balance'}
		
		HWF.post({url:HWD.api.gets.taskCommandTo3xSystem,data:dataP});
		HWF.push('Отправлен запрос на обновление баланса');
	});
	// right click
	//HW.on('contextmenu','main-page',HWmain.imgClickR,{method:'m'});
	
	
	// Текстовый поиск Оборудование
	HWS.on('change','search_equp_vijet',function(e,el){
		HWS.data.apiGet.listVijet.data.search_text = el.value;
		HWF.print.listVijet('.modal-window .modal-section .table-content');
	});	
	
	// Текстовый поиск Программ
	HWS.on('change','search_prog_vijet',function(e,el){
		let getServer = {}
		getServer.search_text =  el.value
		HWS.buffer.programListDopGetServer = getServer;
		HWprog.list('.modal-window .modal-section .table-content','of','checkbox');
	});
	
	// Текстовый поиск система
	HWS.on('change','search_system_vijet',function(e,el){
		HWS.data.apiGet.systemVijet.data.search_text = el.value;
		HWF.print.systemVijet('.modal-window .modal-section .table-content');
	});	
		
	// Нажатие  адаптере бойлера на кнопку подробнее с ошибкой
	HWS.on('click','btn_modal_adapter_boiler',function(e,el){
		let block = HWS.upElem(el,'.one-tile');
		let id = block.dataset.id;
		let dataText = HWS.buffer.adapterBoiler[id];
		
		HWF.modal.message('Описание ошибки','');
		console.log(dataText);
	});
});
