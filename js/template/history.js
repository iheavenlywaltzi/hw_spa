"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1 class="test_page">История</h1>';
	let content = document.createElement('div');
		content.classList.add('main-block'); content.classList.add('max-1200');
		content.classList.add('settings-page');
	let token = HWS.pageAccess;
	let getSer = {};
	let selectEqup = [];
	let dopGet = {};
	
	content.innerHTML = HWhtml.loader();
	B.title.append(title);
	B.content.append(content);

	// проверяем сколько систем у пользователя
	HWF.post({url:HWD.api.gets.objectsList3xSystem,data:{token:token,list_start:0,list_amount:1000},fcn:function(info){
		if (info.list.length == 0){load_page(false,'on');}
		else if (info.list.length == 1){load_page(info.list[0]);}
		else {load_page();}
	}});

	function load_page(system,noSys){ system = system || false;
		let print = '';
		let act = {};
		let data = HWS.getHash().get;
		
		dopGet.system_id = data.system_id || false;
		dopGet.time = data.time || 1;
		if(system) {dopGet.system_id = system.id_system}

		function create_date(d1,dFull){ //console.log(d1);
			let getInf = data[d1] || false;
		
			getInf = (getInf)?getInf.split(','):'';
			let d2={};
			if(getInf && getInf.length > 0){ HWF.recount(getInf,function(el){
				el = parseInt(el); d2[el] = 'active';
				if(!getSer[d1]) {getSer[d1] = []}
				getSer[d1].push(el);
			});} else { HWF.recount(dFull,function(el,key){
				key = parseInt(key); d2[key] = 'active';
				if(!getSer[d1]) {getSer[d1] = []}
				getSer[d1].push(key);
			});}
			return d2;
		}
		
		act.callSms = create_date('call_sms',{1:'',2:'',3:'',4:''});
		act.mail = create_date('mail',{1:''});
		act.systemEvent = create_date('system_event',{1:'',2:''});
		
		act.device_ids = data.device_ids || false;
		if(act.device_ids){
			act.device_ids = act.device_ids.split(',')
			selectEqup = act.device_ids;
			act.device_ids = 'Выбрано оборудования: '+act.device_ids.length+' шт';
		}
		else {act.device_ids = 'Оборудование не выбрано'}
		
		if(system){
			act.systemP = '\
			<div id="system-name-block" class="tf_mb block_plr_20">'+system.config.name+'</div>';
		} else {
			act.systemP = '\
			<div id="system-name-block" class="tf_mb block_plr_20"></div>\
			<div class="">\
				<span class="btn_text grey btn_filter_select_system">Выбрать систему</span> <br>\
				<span id="error-message-system" class="tc_red open-clouse">Система не выбрана</span>\
			</div>';
		}

		print += '<div class="one-block-col">\
		<div class="block_pb_10 block_w_100 display_flex">\
			<div class="btn-open-filter btn-active pointer" data-onof="#filter-window" data-targetdown="#icon-filter">\
				<div class="display_inline select-none">\
					<span class="spec-circle-icon "></span>\
					<span id="icon-filter" class="icon icon-filter hover_orange p16 active"></span>\
				</div>\
				<span class="vertical-middle tc_grey6 select-none">Фильтр</span>\
			</div>\
			'+act.systemP+'\
		</div>';
		print += '<div id="filter-window" class="filter-window flex_between_top history open">';
		print += '<div class="colum">\
			<p class="title">Оборудование</p>\
			<div class="block_mt_8"><span class="btn_select_equp_filter btn_text grey">Выбрать</span></div>\
			<div id="select_equp_info" class="block_mt_8 tw_700">'+act.device_ids+'</div>\
		</div>';
		print += '<div class="colum">\
			<p class="title">Звонки и SMS</p>\
			<ul id="filter-typeProj-ul" data-type="call_sms">\
				<li><span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				<li class="filter-li '+(act.callSms[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Входящие SMS</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.callSms[2] || '')+'" data-val="2">\
					<span class="checkbox"></span><span class="name">Исходящие SMS</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.callSms[3] || '')+'" data-val="3">\
					<span class="checkbox"></span><span class="name">Входящие звонки</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.callSms[4] || '')+'" data-val="4">\
					<span class="checkbox"></span><span class="name">Исходящие звонки</span><div class="click btn_one_filter"></div></li>\
			</ul>\
		</div>';
		print += '<div class="colum">\
			<p class="title">Почтовые рассылки</p>\
			<ul id="filter-typeProj-ul" data-type="mail">\
				<li><span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				<!--li class="filter-li '+(act.mail[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Периодические отчеты</span><div class="click btn_one_filter"></div></li-->\
				<li class="filter-li '+(act.mail[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Уведомление о связи</span><div class="click btn_one_filter"></div></li>\
				<!--li class="filter-li '+(act.mail[3] || '')+'" data-val="3">\
					<span class="checkbox"></span><span class="name">Прочие</span><div class="click btn_one_filter"></div></li-->\
			</ul>\
		</div>';
		print += '<div class="colum">\
			<p class="title">Системные события</p>\
			<ul id="filter-typeProj-ul" data-type="system_event">\
				<li><span class="btn-all btn_filter_select_all btn_text_2">Всё</span></li>\
				<li class="filter-li '+(act.systemEvent[1] || '')+'" data-val="1">\
					<span class="checkbox"></span><span class="name">Охрана</span><div class="click btn_one_filter"></div></li>\
				<li class="filter-li '+(act.systemEvent[2] || '')+'" data-val="2">\
					<span class="checkbox"></span><span class="name">Прочие</span><div class="click btn_one_filter"></div></li>\
			</ul>\
		</div>';
		let selT = parseInt(dopGet.time);
		
		
		print += '<div class="block_w_100">\
				<span class="style_btn grey btn_clear_equp_filter block_w_80p">Очистить</span>\
				<span class="block_pt_10"></span>\
				<span class="style_btn grey btn_accept_new_filter block_w_80p">Применить</span>\
		</div>';
		
		print += '</div>';
		print += '</div>';
		
		act.noSysText = '';
		if(noSys){act.noSysText = '<div class="block_p_10 ta_center tf_fr900">В Вашем аккаунте еще нет систем. Добавьте их.</div>';}
		print += '<div class="one-block-col">\
			<div class="block_w_100">'+HWhtml.timeControler(1,'timeControlFCN','timeControlLoadFCN')+'</div>\
			<div class="block_w_100" id="list-history-block" >'+act.noSysText+'</div>\
		</div>';
		
		content.innerHTML = print;
		
		// Запрос системы
		if(dopGet.system_id){
			HWS.getElem('#system-name-block').innerHTML = HWhtml.miniLoader();
			HWF.post({url:HWD.api.gets.list,data:{list_start:0,list_amount:100,token:token,id_system:dopGet.system_id,local_id:0},fcn:function(inf){
				console.log(inf);
				let name = inf.list[0].config.name;
				HWS.getElem('#system-name-block').innerHTML = name;
			}});
		}
		// печать списка если это возможно
		print_list();
	}

	// FUNCTION
	function print_list(){
		if(dopGet.system_id){
			let time = dopGet.time || 1; // 1 hour
			let gTime = Date.now() ;
			let sTime = gTime - (time*3600000);
			let eTime = gTime;
			let data = {
				timestamp_start:sTime,
				timestamp_end:eTime,
				system_id:dopGet.system_id
			}
			
			function anti_null(arr){
				let newArr = []
				if(arr.length > 0){HWF.recount(arr,function(elAr){
					if(elAr != 255){
						let newInex = parseInt(elAr);		
						newArr.push(newInex);
					}
				});}
				return newArr;
			}
			data.call_sms = anti_null(getSer.call_sms);
			data.mail = anti_null(getSer.mail);
			data.system_event = anti_null(getSer.system_event);
			if(selectEqup.length > 0){data.device_ids = selectEqup;}

			HWF.printList({name:'listNews',url:HWD.api.gets.eventsHistory,target:'#list-history-block',
			data:data,
			clas:'block_w_100',
			fcn:function(info){ //console.log(info)
				let list = info.events;
				let html = '<ul class="list jurnal">';
					html+= '<li class="one-head sticky">\
						<span class="time">Дата и время</span>\
						<span class="info">Событие</span>\
					</li>';
					
				HWF.recount(list,function(inf){
					let time = HWF.unixTime(inf.timestamp).full;
					let texte = inf.text;
					html+= '<li>\
						<span class="time">'+time+'</span>\
						<span class="info">'+texte+'</span>\
					</li>';
				});
				html+= '</ul>'
				
				return html;
			}});
		}
	}
	
	
	// ACTIVITI
	// Кнопка применить фильтр
	HWS.on('click','btn_accept_new_filter',function(e,el){
		if(dopGet.system_id){
			let getstr = '?system_id='+dopGet.system_id+'&time='+dopGet.time+'&';
			HWF.recount(getSer,function(oneGet,key){
				if(oneGet.length > 0){
					if ( oneGet[255] ) {delete oneGet[255]}
					getstr+= key;
					getstr+= '=';
					getstr+= oneGet.join(',');
					getstr+= '&';
				} else {
					getstr+= key; getstr+= '='; getstr+= '255'; getstr+= '&'
				}
			});
			
			//console.log(getSer);
			if(selectEqup.length > 0){getstr+= 'device_ids='+selectEqup.join(',');}
			
			let go = 'history'+getstr;	
			HWS.go(go,'no_reboot');
			print_list();
		} else {
			HWS.getElem('#error-message-system').classList.add('open');
		}
	});
	
	// Кнопка Выбрать оборудование ( модальное окно )
	HWS.on('click','btn_select_equp_filter',function(e,el){
		if(dopGet.system_id){
			let getData = {}
				getData.token = token;
				getData.list_start = 0;
				getData.list_amount = 1000;
				getData.id_system = dopGet.system_id;
				getData.local_id_object_type = 2;
				//getData.iface_group = [0,1,2]; //  iface_group отвечает за группы (встроенные, проводные, радио)
			
			HWF.modal.print('Выбор оборудования',HWhtml.loader());
			
			HWF.post({url:"objects/list",data:getData,fcn:function(inf){
				let list = inf.list;
				let print = '';
				
				//console.log(selectEqup);
				print+= '<div class="table-mid">';
				print+= HWhtml.equipments(list,{check:selectEqup});
				print+= '</div>';
				print+= '<div class="style_btn orange block_w_200p block_mtb_20">Добавить</div>';
				
				HWF.modal.print('Выбор оборудования',print);
			}});
		} else {
			HWS.getElem('#error-message-system').classList.add('open');
		}
	});
	// выбор одно оборудование
	HWS.on('click','btn-one-equip',function(e,el){
		let li = HWS.upElem(el,'li');
		let check = li.querySelector('.equ_b .settings-butt .checkbox ');
		
		if(check.classList.contains('active')){check.classList.remove('active');}
		else{check.classList.add('active');}
	});
	
	// выбор сазу всех элементов
	HWS.on('click','equp_select_all',function(e,el){
		HWF.allActive('.list-equipments ','.checkbox');
	});
	
	// Rкнопка добваить оборудование ( внутри модального окна )
	HWS.on('click','add_more_equp_btn',function(e,el){
		selectEqup = [];
		let modal = HWS.upElem(el,'.modal-section');
		let allLi = modal.querySelectorAll('.table-mid .list-equipments .one-equipment .equ_b .checkbox.active');
		
		if(allLi.length > 0){
			HWF.recount(allLi,function(eli){
				let li = HWS.upElem(eli,'.one-equipment');
				let id = li.dataset.id;
				selectEqup.push(id);
			},'on');
			
			let infoText = HWS.getElem('#select_equp_info');
				infoText.innerHTML = 'Выбрано оборудования: '+allLi.length+' шт';
		}
		
		HWF.modal.of();
	});
	
	// Кнопка очистить фильтр
	HWS.on('click','btn_clear_equp_filter',function(e,el){
		HWS.go('history');
	});

	// Выбор системы (Модальное окно)
	HWS.on('click','btn_filter_select_system',function(e,el){
		let topPrint = '<div class="block_w_100 ta_center">'+HWhtml.search()+'</div>';// на будущее текстовый поиск
		HWF.modal.print('Выберите систему','');
		HWF.print.system('.modal-section',{top:'',a:'no',type:'nobtn system-list',click:'btn_selected_one_system'});
	});
	// нажатие на 1 систему
	HWS.on('click','btn_selected_one_system',function(e,el){
		let li = HWS.upElem(el,'li');
		let name = li.querySelector('.equ_n .text_name').innerHTML;
		
		selectEqup = [];
		dopGet.system_id = li.dataset.idsystem;
		HWS.getElem('#system-name-block').innerHTML = name;
		HWS.getElem('#select_equp_info').innerHTML = 'Оборудование не выбрано';
		HWS.getElem('#error-message-system').classList.remove('open');
		HWF.modal.of();
	});
	
	// Нажатие кнопки СОХРАНИТЬ историю
	HWS.buffer.timeControlLoadFCN = function(H,type){
		if(dopGet.system_id){
			let time = dopGet.time || 1; // 1 hour
			let gTime = Date.now() ;
			let sTime = gTime - (time*3600000);
			let eTime = gTime;
			let data = {
				token:token,
				timestamp_start:sTime,
				timestamp_end:eTime,
				system_id:dopGet.system_id
			}
			
			function anti_null(arr){
				let newArr = []
				if(arr && arr.length > 0){HWF.recount(arr,function(elAr){
					let newInex = parseInt(elAr);		
					newArr.push(newInex);
				});}
				return newArr;
			}
			console.log(getSer);
			data.device_type = anti_null(getSer.device_type);
			data.call_sms = anti_null(getSer.call_sms);
			data.mail = anti_null(getSer.mail);
			data.system_event = anti_null(getSer.system_event);
			if(selectEqup.length > 0){data.device_ids = selectEqup;}
			
			if(type == 'CSV'){
				HWF.post({url:"history/events_history_csv",data:data,fcn:function(inf){
					window.open(serverURL+'uploads/'+inf.path_and_filename_to_upload_file);
				}});
			}
			
			if(type == 'PDF'){
				HWF.post({url:"history/events_history_pdf",data:data,fcn:function(inf){
					window.open(serverURL+'uploads/'+inf.path_and_filename_to_upload_file);
				}});
			}
			
		} else {
			HWS.getElem('#error-message-system').classList.add('open');
		}
	}
	HWS.on('click','btn_load_info_format',function(e,el){

	});
	// Нажатие на 1 фильтр	
	HWS.on('click','btn_one_filter',function(e,el){
		let ul = HWS.upElem(el,'ul');
		let li = HWS.upElem(el,'li');
		let type = ul.dataset.type;
		let val = parseInt(li.dataset.val);
		
		if(!getSer[type]){getSer[type] = [];}
		
		if(li.classList.contains('active')){li.classList.remove('active'); getSer[type].splice(getSer[type].indexOf(val), 1);} 
		else {li.classList.add('active'); getSer[type].push(val);}
	});
	
	// Выбрать Время
	HWS.buffer.timeControlFCN = function(H){
		dopGet.time = parseInt(H);
		print_list();
	}
	
	// Выбрать всё		
	HWS.on('click','btn_filter_select_all',function(e,el){
		let ul = HWS.upElem(el,'ul');
		let type = ul.dataset.type;
		HWF.allActive(ul,'.filter-li');
		let allLi = ul.querySelectorAll('.filter-li.active');
		let newArr = [];
		
		if(allLi.length > 1){
			HWF.recount(allLi,function(eli){
				newArr.push( parseInt(eli.dataset.val));
			},'on')
		}
		getSer[type] = newArr;
	});
});