"use strict";
HWS.templateAdd(function(HW,B,H){ HWF.clearBlocks();//очистим блоки контена
	let title = '<h1>Программа</h1>';
	let progId = H.hash[1] || 0;
	let token = HWS.pageAccess;
	HWS.buffer.PrintData = {}
	
	let sbc = document.createElement('div'); // spec block content
	let contentTop = document.createElement('div');
		contentTop.classList.add('main-block'); contentTop.classList.add('max-1200');
		contentTop.classList.add('no-P-bot');
		contentTop.id = 'prog-content-top';
	let contentBot = document.createElement('div');
		contentBot.classList.add('main-block'); 
		contentBot.classList.add('max-1200');
		contentBot.classList.add('no-p-top');
		contentBot.id = 'prog-content-mid';
		contentBot.innerHTML = HWhtml.loader();
	let noConnect = document.createElement('div');
		noConnect.classList.add('no-connect-block');
		noConnect.innerHTML = '<div class="btn_no_connect tf_fr900 ts_24p btn_text black">Система этого объекта не на связи с ЛК. Почему???</div>';
	HWS.buffer.timeValueGraph = 1;	
	
	sbc.append(contentTop);
	sbc.append(contentBot);
	sbc.append(noConnect);
	B.content.append(sbc);
	B.title.append(title);
	
	let dataP = {token:token,id_object:progId}
	HWF.post({url:HWD.api.gets.object,data:dataP,fcnE:HWF.getServMesErr,fcn:load_page});

	function load_page(info){
		let elem = info;
		let type = elem.cloud_type || elem.info.type.type;
		let et = HWD.equp[type] || false;
		let content = HWS.getElem('.main-block');
		let connect = false; 
		if(HWF.objKeyTest('system_connection_status',info)){connect = info.system_connection_status;}
		HWS.buffer.system_time = elem.system_time;
		HWS.buffer.elem = elem;
		HWS.buffer.elemEt = et;
		if(info.web_config){HWS.buffer.timeValueGraph = info.web_config.time_per_graph || 1;}
		HWS.load('prog/'+et.tmp); // вызов нужного шаблона
		if(!connect) {noConnect.classList.add('on')}
	}
	
	HWS.buffer.load_jurnal = function(H){
		// История
		let progID = HWS.buffer.elem.id;
		let block = document.body.querySelector('#history-block');
		let setsB = document.createElement('div'); setsB.id = 'history-block_sets';
		let listB = document.createElement('div'); listB.id = 'history-block_list';
		if(!block){
			block = document.createElement('div');
			block.id = 'history-block'; block.classList.add('one-block');
			//block.innerHTML = '<h4>История:</h4>'+ HWhtml.loader();
			contentBot.append(block);
		}
		print = HWhtml.timeControler(H,'timeControlFCNJurnal','timeControlLoadFCNJurnal');
	
		setsB.innerHTML = print;
		block.innerHTML = '';
		block.append(setsB);
		block.append(listB);

		let time = H || 1; // 1 hour
		let gTime = Date.now() ;
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		let data = {
			timestamp_start:sTime,
			timestamp_end:eTime,
			program_id:progID
		}

		HWF.printList({name:'listJurnal',url:HWD.api.gets.eventsProgHistory,target:listB,
		data:data,
		clas:'block_w_100',
		fcn:function(info){ console.log(info)
			let html = '';
			if(!info.exceptions){
				let list = info.events;
					html = '<ul class="list jurnal">';
					html+= '<li class="one-head sticky">\
						<span class="time">Дата и время</span>\
						<span class="info">Событие</span>\
					</li>';
				
				if(list.length > 0){
					HWF.recount(list,function(inf){
						let time = HWF.unixTime(inf.timestamp).full;
						let texte = inf.text;
						html+= '<li>\
							<span class="time">'+time+'</span>\
							<span class="info">'+texte+'</span>\
						</li>';
					});
				} else {
					html+= '<li>\
							<p class="block_w_100 tw_600 ta_center block_p_20">За указанный период события не найдены.</p>\
						</li>';
				}
				html+= '</ul>'
			}
			else {html = '';}
			
			return html;
		}});
	}
	//Изменение времени показаний ЖУРНАЛА
	HWS.buffer.timeControlFCNJurnal = function(H) {
		HWS.buffer.load_jurnal(H);
	}		
	
	//Загрузка истории по журналу
	HWS.buffer.timeControlLoadFCNJurnal = function(H,type) {
		let time = H || 1; // 1 hour
		let gTime = Date.now() ;
		let sTime = gTime - (time*3600000);
		let eTime = gTime;
		let data = {
			token:token,
			timestamp_start:sTime,
			timestamp_end:eTime,
			program_id:HWS.buffer.elem.id,
		}
		console.log(HWS.buffer.elem)
		if(type == 'CSV'){
			HWF.post({url:"history/program_events_csv",data:data,fcn:function(inf){
				window.open(serverURL+'/uploads/'+inf.path_and_filename_to_upload_file);
			}});
		}
		
		if(type == 'PDF'){
			HWF.post({url:"history/program_events_pdf",data:data,fcn:function(inf){
				window.open(serverURL+'/uploads/'+inf.path_and_filename_to_upload_file);
			}});
		}
	}
	
	// Нажатие на кнопку если оборудование не на связи
	HWS.on('click','btn_no_connect',function(e,el){
		let gosystemId = HWS.buffer.elem.system_object_id || '';
		if(gosystemId){gosystemId = 'equipment/'+gosystemId}
		HWF.modal.confirmCust({
			title:'Нет связи с системой',
			print:'Система, с которой связано данное устройство или программа,<br> ушла со связи с ЛК. Когда она снова выйдет на связь,<br> все настройки снова будут доступны.<br><br> Проверьте связь с системой',
			no:'ЗАКРЫТЬ',
			yes:'ПРОВЕРИТЬ',
			yesHref:gosystemId,
		});
		console.log(HWS.buffer.elem);
	});
	
		// Загрузка Доступов
	HWS.buffer.equpAccessesFCN = function(){
		B.title.append('Доступы');
		HWequip.accessUsers(progId,contentBot)
	}
	
	HWS.buffer.share_stick = function(info){ //показывает стикер расшарки
		let blockShare =  document.createElement('div');
			blockShare.classList.add('block_share_sticer');
		let share_status = (info.lk)?info.lk.share_status || '':'';
		
		if (share_status &&  share_status  == 1){share_status = '';}
		if (share_status &&  share_status  == 3){share_status = '<span class="sticer-share-up btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconUp+'"></span>';}
		if (share_status &&  share_status  == 2){share_status = '<span class="sticer-share-down btn_helper" data-text="'+HWD.helpText.pages.shareStatusIconDown+'"></span>';}
		blockShare.innerHTML = share_status;
		
		contentTop.querySelector('.one-block').append(blockShare);
	}
	
		//Нажатие на чекбокс в разделе ( доступы )
	HW.on('click','one_equipment_acsses_check',function(e,el){
		let btnBox = HWS.getElem('#block-menu-action-btn-acsses');
		
		if(el.classList.contains('active')){
			btnBox.classList.add('open');
		}else {
			let allAct = document.querySelectorAll('.list-equipments li .equ_b .one_equipment_check.active');
			if (allAct.length == 0){btnBox.classList.remove('open');}
		}
	});
	
	HWS.buffer.notYouElemInfoFCN = function(){if(!HWS.buffer.elem .owner_status && HWS.buffer.elem .security == 'r'){
		let ovner = HWS.buffer.elem .owner_info.email;
		let divInf =  document.createElement('div');
		divInf.classList.add('one-block-col');
		divInf.innerHTML = '<div class="not-you-elem">Вам доступен только просмотр, любые изменения настроек не сохранятся. Для изменения прав обратитесь к: '+ovner+'</div>';
		contentBot.prepend(divInf);
	}}
});