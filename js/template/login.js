"use strict";
HWS.templateAdd('login',function(HW,B,H){ 
	HWF.clearBlocks();//очистим блоки контена

	let verification = false;
	let recovery = false
		// /#login?verification=c4c29f8d2a7146599e6abe28ded17cae
		if(H){verification = H.get.verification || false;} // регистрация прошла успешно
		// /#login?recovery=051001c9406940319b0c63edce3bbe8d
		if(H){recovery = H.get.recovery || false;}
	let title = '<h1>Авторизация</h1>';
	let container = document.createElement('div');
		container.classList.add('login-page-form');
	let btnPlay = document.createElement('div');
		btnPlay.classList.add('btn-play');
	let forma = document.createElement('form');
	let imgLogo = '<img src="img/logo_black.svg">';
	let containerForm = document.createElement('div');
		containerForm.classList.add('login-cont-form');
	let containerSpecTex = document.createElement('div');
		containerSpecTex.classList.add('login-dop-cpec-text');

	
	let HtmlLogin = function(mail){mail = mail || ''; let s = '\
		<p class="logo">'+imgLogo+'</p>\
		<p class="title">Вход в личный кабинет</p>\
		<p id="error-login"></p>\
		<input type="hidden" id="specHidden" value="login">\
		<p><input name="email" type="email" id="user_mail" value="'+mail+'" placeholder="Электронная почта" required></p>\
		<p><span class="spec-password">\
			<input name="password" type="password" id="user_pass" placeholder="Пароль" required>\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>\
		<p class="dop-btn">\
			<span class="btn_text grey btn_register_user">Регистрация</span>\
			<span class="btn_text grey btn_bad_memory">Забыли пароль?</span>\
		</p>\
		<p class="block_pt_10"><button class="btn-form-click">Войти</button></p>\
		<p class="demo"><span class="btn_text grey btn_demo_user">Демо-вход</span></p>\
		<p class="store">\
			<a href="https://apps.apple.com/mg/app/ectocontrol/id1520421408" target="_blanck"><img src="img/a_store.svg"></a>\
			<a href="https://play.google.com/store/apps/details?id=ru.ectocontrol.app&hl=ru&gl=US" target="_blanck"><img src="img/g_play.svg"></a>\
		</p>\
		<!--p class="info-text">\
			При входе вы подтверждаете свое согласие с <span class="btn_text grey">условиями использования</span> <br>\
			ectoControl и <span class="btn_text grey">политикой о данных пользователей.</span> \
		</p-->\
		<p class="block_p_10"></p>\
	';
		return s;
	}
	let HtmlRegistr = function(mail){mail = mail || ''; let s ='\
		<p class="logo">'+imgLogo+'</p>\
		<p class="title">Регистрация в личном кабинете</p>\
		<input type="hidden" id="specHidden" value="registr">\
		<p><input name="email" type="email" id="user_mail" value="'+mail+'" placeholder="Электронная почта" required></p>\
		<p><span class="spec-password">\
			<input name="password" type="password" id="user_pass" placeholder="Пароль" required maxlength="25">\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>\
		<p><span class="spec-password">\
			<input name="password2" type="password" id="user_pass2" placeholder="Пароль еще раз" required maxlength="25">\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>\
		<p id="error-login"></p>\
		<p></p>\
		<p class="dop-btn">\
			<span class="btn_text grey btn_login_user">Вход</span>\
			<span><input name="rules" type="checkbox" id="rules" required>согласен с <span class="btn_text grey btn_info_user">правилами</span>.</span>\
		</p>\
		<p class="block_pt_10"><button class="btn-form-click">Регистрация</button></p>\
		<p class="demo"><span class="btn_text grey btn_demo_user">Демо-вход</span></p>\
		<p class="store">\
			<a href="https://apps.apple.com/mg/app/ectocontrol/id1520421408" target="_blanck"><img src="img/a_store.svg"></a>\
			<a href="https://play.google.com/store/apps/details?id=ru.ectocontrol.app&hl=ru&gl=US" target="_blanck"><img src="img/g_play.svg"></a>\
		</p>\
		<!--p class="info-text">\
			При входе вы подтверждаете свое согласие с <span class="btn_text grey">условиями использования</span> <br>\
			ectoControl и <span class="btn_text grey">политикой о данных пользователей.</span> \
		</p>-->\
		<p class="block_p_10"></p>\
		';
		return s;
	}
	let HtmlRecovery = function(mail){mail = mail || ''; let s ='\
		<p class="logo">'+imgLogo+'</p>\
		<p class="title">Восстанавление пароля</p>\
		<p id="error-login"></p>\
		<input type="hidden" id="specHidden" value="recovery">\
		<p><input name="email" type="email" id="user_mail"  value="'+mail+'" placeholder="Электронная почта" required></p>\
		<p class="tc_grey block_pl_10">*Мы вышлем вам инструкции на Email</p>\
		<p class="dop-btn">\
			<span class="btn_text grey btn_login_user">Вход</span>\
			<span class="btn_text grey btn_register_user">Регистрация</span>\
		</p>\
		<p class="block_pt_10"><button class="btn-form-click">Выслать</button></p>\
		<p class="demo"><span class="btn_text grey btn_demo_user">Демо-вход</span></p>\
		<p class="store">\
			<a href="https://apps.apple.com/mg/app/ectocontrol/id1520421408" target="_blanck"><img src="img/a_store.svg"></a>\
			<a href="https://play.google.com/store/apps/details?id=ru.ectocontrol.app&hl=ru&gl=US" target="_blanck"><img src="img/g_play.svg"></a>\
		</p>\
		<p class="block_p_10"></p>\
		';
		return s;
	}
	let HtmlRecoveryNewPass = function(){let s =''+
		'<p class="logo">'+imgLogo+'</p>'+
		'<p class="title no-p-bot">Введите новый пароль</p>'+
		'<p id="error-login"></p>'+
		'<input type="hidden" id="specHidden" value="newpass">'+
		'<p><span class="spec-password">\
			<input name="password" type="password" id="user_pass" placeholder="Пароль" required>\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>'+
		'<p class="title no-p-bot">Повторите пароль</p>'+
		'<p><span class="spec-password">\
			<input name="passwordto" type="password" id="user_pass2" placeholder="Пароль" required>\
			<span class="btn_view_pass icon hover_orange icon-read"></span>\
		</span></p>'+
		'<p class="block_pt_10"><button class="btn-form-click">Готово!</button></p>'+
		'<p class="block_pt_15">\
			<a href="/" class="btn_text grey">Отмена</a>\
		</p>'+
		'<br> <br> <br>';
		return s;
	}
	
	btnPlay.innerHTML = '\
		<p><span><img src="img/play.svg"><span>Как пользоваться<br>личным кабинетом?</span></span></p>\
	';

	forma.innerHTML = HtmlLogin();
	
	if (verification){
		forma.innerHTML = ''+
			'<p class="logo">'+imgLogo+'</p>'+
			'<p class="title">Авторизация</p>'+
			'<p>Авторизация...</p>'+HWhtml.loader()+
			'<br> <br> <br> <br>'+
		'';
		
		let data = {code:verification};
		
		HWF.post({url:'registration/check',data:data,fcn:verification_ok,fcnE:modal_mes});
		
		function verification_ok(info){
			if(info.success){ console.log(info);
				let connect = info.success;
				let token = info.token;

				localStorage.setItem('PageAccess',token)
				localStorage.setItem('UserEmail','');
				forma.innerHTML = ''+
					'<p class="logo">'+imgLogo+'</p>'+
					'<p class="title">Регистрация прошла успешно!</p>'+
					'<p><a href="/">На главную</a></p>'+
					'<br> <br> <br> <br>'+
				'';
			}
		}
	}	
	
	if (recovery){ forma.innerHTML = HtmlRecoveryNewPass(); }
	
	containerSpecTex.innerHTML = '\
	<p class="ts_18p tf_mb block_pb_20">Инструкция по переходу на новый <br> личный кабинет со старого:<p>\
	<ul class="list-numder">\
		<li>Введите ваши email и пароль от старого личного кабинета и нажмите «Войти».</li>\
		<li>Мы отправим письмо на ваш email со ссылкой для смены пароля (это необходимо для обеспечения безопасности).</li>\
		<li>Перейдите по этой ссылке и придумайте новый надежный пароль.</li>\
		<li>Через 3-5 минут ваша система со всем оборудованием и программами автоматически будет доступна в новом кабинете (при этом в старом она станет не доступна).</li>\
	</ul>\
	<p class="block_ptb_20"><a class="tc_black" href="https://www.youtube.com/watch?v=goBK_lGFWUg" target="_blanck"><img class="vertical-middle" src="img/play.svg"><span class="vertical-middle block_pl_10">Видеоинструкция<br>по переходу</span></a></p>\
	<p class="block_pb_20 tc_grey8">Если у вас возникли сложности или вопросы, напишите нам <br> в WhatsApp <a class="tc_grey8 text_line" href="https://wa.me/74951202269" target="_blanck">+7 (495) 120-22-69</a> или на почту <a class="tc_grey8 text_line" href="mailto:support@ectostroy.ru">support@ectostroy.ru</a></p>\
	<p>\
		<span class="ts_18p tf_mb">Старый личный кабинет<br>доступен по ссылке:</span>\
		<a class="style_btn p12 grey block_ml_20" href="https://old.ectostroy.ru/" target="_blanck">Старый личный кабинет</a>\
	</p>';
		
	// функции для этой страници
	forma.onsubmit = function(){
		let spec = document.getElementById('specHidden').value;
		let email = document.getElementById('user_mail'); email = (email)?email.value:false; HWS.buffer.mail = email;
		let name = document.getElementById('user_name'); name = (name)?name.value:false;
		let pass = document.getElementById('user_pass'); pass = (pass)?pass.value:false;
		let pass2 = document.getElementById('user_pass2'); pass2 = (pass2)?pass2.value:false;
		let rules = document.getElementById('rules'); rules = (rules)?rules.value:false;
		let errorMesText = '<span class="tc_red"><span data-text="1. Длина пароля от 8 до 25 символов <br> 2. Латиниские буквы, цифры и разрешенные символы: # ! $ % & ( ) * + , - . / : ; < = > ? @ _" class="btn_helper">Пароль не соответствует требованиям ?</span></span>';
		
		if (spec == 'login'){
			forma.innerHTML = ''+
				'<p class="logo">'+imgLogo+'</p>'+
				'<p class="title">Вход в личный кабинет</p>'+
				'<p>Вход...</p>'+HWhtml.loader()+
				'<br> <br> <br> <br>'+
			'';
			localStorage.setItem('UserEmail',email);
			let data = {
				email:email,
				password:pass
			}
			HWF.post({url:'user/login',data:data,fcn:load_project,fcnE:error_login});
		}
		
		if (spec == 'registr'){
			let pass1 = HWS.getElem('#user_pass');
			let pass2 = HWS.getElem('#user_pass2');
			let errorMes = HWS.getElem('#error-login'); errorMes.innerHTML='';
			
			if (pass1.value.length > 7){if(pass1.value == pass2.value){
				
				let regex = new RegExp("^[a-zA-Z0-9#!$%&()*+,-./:;<=>?@_ ]+$");
				let control = false;
				if(regex.test(pass1.value)){control = true}
				if(control){
					forma.innerHTML = ''+
						'<p class="logo">'+imgLogo+'</p>'+
						'<p class="title">Регистрация в личный кабинет</p>'+
						'<p>Регистрация...</p>'+HWhtml.loader()+
						'<br> <br> <br> <br>'+
					'';
					if (rules == 'on') {rules = true;}
					let data = {
						email:email,
						password:pass,
						password_confirmation:pass2,
						rules:rules
					}
					HWF.post({url:'registration/registrate',data:data,fcn:ok_registr,fcnE:error_registr});
				}
				else {errorMes.innerHTML = errorMesText;}
			}
			else {errorMes.innerHTML = '<span class="tc_red">Пароли не совпадают</span>';}}
			else {errorMes.innerHTML = errorMesText;}
		}
		
		if (spec == 'recovery'){	
			forma.innerHTML = ''+
				'<p class="logo">'+imgLogo+'</p>'+
				'<p class="title">Восстанавление</p>'+
				'<p>Восстанавливаем...</p>'+HWhtml.loader()+
				'<br> <br> <br> <br>'+
			'';
			
			let data = {email:email}
			HWF.post({url:'user/send-code-for-restore-password',data:data,fcn:recovery_ok,fcnE:error_recovery});
		}
		
		if (spec == 'newpass'){	
			let mes = document.body.querySelector('#error-login');
				mes.innerHTML = '';
			
			if (pass && pass.length > 7){ if(pass == pass2){
				
				let regex = new RegExp("^[a-zA-Z0-9#!$%&()*+,-./:;<=>?@_ ]+$");
				let control = false;
				if(regex.test(pass)){control = true}
				if(control){
					forma.innerHTML = ''+
						'<p class="logo">'+imgLogo+'</p>'+
						'<p class="title">Восстановление</p>'+
						'<p>Восстанавливаем...</p>'+HWhtml.loader()+
						'<br> <br> <br> <br>'+
					'';

					let data = {link_key:recovery,password:pass};
					HWF.post({url:'user/change_password_from_link_key',data:data,fcn:function(info){
						if(info.success){
							forma.innerHTML = ''+
								'<p class="logo">'+imgLogo+'</p>'+
								'<p class="title">Восстановление пароля</p>'+
								'<p>Пароль успешно изменен</p>'+ 
								'<br>'+
								'<p><a class="btn-form-login" href="/">Войти</a></p>'+
								'<br> <br>'+
							'';
						} else {forma.innerHTML = HtmlRecoveryNewPass();}
					},fcnE:function(info,key){
						modal_mes(info,key);
						forma.innerHTML = HtmlRecoveryNewPass();
					}});
					mes.innerHTML = '';
				}
			}
			else {mes.innerHTML = '<span class="message">Пароли не совпадают</span>';}}
			else {mes.innerHTML = errorMesText;}
		}
		return false;
	};
	// успешный вход
	function load_project(info){ console.log(info);
		let connect = info.success;
		let token = info.token;
		let email = localStorage.getItem('UserEmail') || 'none';
		let uNumber = localStorage.getItem('UserNumber') || false;
		if (connect){
			HWS.data.token = token;
			HWS.pageAccess = token;
			
			if(info.admin==true){
				//HWS.blocks.menul.append(HWhtml.lMenu('admin')); 
				//HWS.blocks.menu.append(HWhtml.tRMenu('admin'));
				localStorage.setItem('AdminEmail',email);
				localStorage.setItem('PITn0Hxg',token);
				document.body.classList.add('admin');
				HWS.go('god');
			}
			else {
				if(info.mail){recovery_ok()}
				else{
					localStorage.setItem('PageAccess',token);
					localStorage.setItem('PITn0Hxg','');
					localStorage.setItem('UserEmail',email);
					document.body.classList.remove('admin');
					HWS.go('main');
					HWF.setInfoTopMenu();
				}
			}
			HWF.globalReolad();
		}
	}
	// регистрация успешна
	function ok_registr(info){
		forma.innerHTML = ''+
			'<p class="logo">'+imgLogo+'</p>'+
			'<p class="title">Регистрация</p>'+
			'<p>Ссылка подтверждения регистрации</p>'+ 
			'<p>отправлена на <b>ваш e-mail</b>.</p>'+
			'<p>Следуйте инструкциям в письме.</p>'+
			'<br>'+
			'<p><span class="btn-form-login btn_login_user">Войти</span></p>'+
			'<br> <br>'+
		'';
	}
	
	// Востановления удачно
	function recovery_ok(info){
		HWS.data.token = false;
		HWS.pageAccess = false;
		localStorage.setItem('PageAccess','');
		
		forma.innerHTML = ''+
			'<p class="logo">'+imgLogo+'</p>'+
			'<p class="title">Восстановление пароля</p>'+
			'<p>На указанный адрес e-mail отправлено письмо</p>'+
			'<p>с инструкцией по восстановлению пароля.</p>'+
			'<p>Если письмо не пришло, проверьте папку "Спам".</p>'+
			'<br>'+
			'<p><span class="btn-form-login btn_login_user">Ок</span></p>'+
			'<br> <br> <br>'+
		'';
	}

	// печатает сообщение от сервера
	function modal_mes(info,key){
		let mes = HWF.getServMesErr(info) || 'Что-то пошло не так, попробуйте еще раз';
		HWF.modal.message(mes,'Ошибка '+key);
		//forma.innerHTML = HtmlLogin();
	}
	
	//Ошибки во время логоизации
	function error_login(info){
		forma.innerHTML = HtmlLogin(HWS.buffer.mail);
		let mes = document.body.querySelector('#error-login');
		let eror = HWF.getServMesErr(info) || 'Что-то пошло не так, попробуйте еще раз';
		let dopBtn = '';
		if(info.exceptions && info.exceptions[0] && info.exceptions[0].code == 5000010092){dopBtn = '<span class="btn_text blue btn_send_mesage_email">отправить</span>';}
		console.log(info);
		// 5000010092
		mes.innerHTML = '<span class="message">'+eror+' '+dopBtn+'</span>';
	}	
	//Ошибки во время регистрации
	function error_registr(info){
		forma.innerHTML = HtmlRegistr(HWS.buffer.mail);
		let mes = document.body.querySelector('#error-login');
		let eror = HWF.getServMesErr(info) || 'Что-то пошло не так, попробуйте еще раз';
		mes.innerHTML = '<span class="message">'+eror+'</span>';
	}	
	//Ошибки во время востановления
	function error_recovery(info){
		forma.innerHTML = HtmlRecovery(HWS.buffer.mail);
		let mes = document.body.querySelector('#error-login');
		let eror = HWF.getServMesErr(info) || 'Что-то пошло не так, попробуйте еще раз';
		mes.innerHTML = '<span class="message">'+eror+'</span>';
	}
	
	containerForm.appendChild(forma);
	//containerForm.appendChild(btnPlay);
	container.appendChild(containerForm);
	container.appendChild(containerSpecTex);
	
	B.title.append(title);
	B.content.append(container);
	 
	HW.on('click','btn_register_user',function(){B.title.append('<h1>Регистрация</h1>'); forma.innerHTML = HtmlRegistr()});
	HW.on('click','btn_login_user',function(){B.title.append('<h1>Авторизация</h1>'); forma.innerHTML = HtmlLogin()});
	HW.on('click','btn_bad_memory',function(){B.title.append('<h1>Восстановление</h1>'); forma.innerHTML = HtmlRecovery()});
	HW.on('click','btn_info_user',function(){HWF.modal.print('Правила',HWD.regRegulations);});
	HW.on('click','btn_demo_user',function(){
		HWS.getElem('.login-page-form').innerHTML = HWhtml.loader();
		//forma.innerHTML = '<p>Вход...</p>'+HWhtml.loader();
		localStorage.setItem('UserEmail','demolk@ectostroy.ru');
		HWF.post({url:'user/demo_login',fcn:function(info){
			HWS.pageAccess = info.token;
			localStorage.setItem('PageAccess',info.token);
			localStorage.setItem('PITn0Hxg','');
			localStorage.setItem('UserEmail','demolk@ectostroy.ru');
			document.body.classList.remove('admin');
			HWS.go('main');
			HWF.setInfoTopMenu();
		}});
	});
	// Отправить емайл повторно
	HW.on('click','btn_send_mesage_email',function(){
		let mes = document.body.querySelector('#error-login');
			mes.innerHTML = '';
		let email = document.body.querySelector('#user_mail').value || 'xxxxx@yyyy.zzz'; 
		let title = 'Активация аккаунта';
		let mesage = 'Письмо для активации аккаунта '+email+' отправлено. <br> Следуйте инструкциям в письме. Если письмо не приходит проверьте папку "Спам"';
		let data = {email:email}
		
		HWF.modal.message(title,mesage);
		HWF.post({url:'registration/resend_mail',data:data,fcn:function(info){}});
	});
});