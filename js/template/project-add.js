"use strict";
HWS.templateAdd(function(HW,B,H){
	HWF.clearBlocks();//очистим блоки контена main-block max-1200 no-p-top
	let token = HWS.pageAccess;
	let menu = '';
	let name = '<h1>Создайте проект</h1>';
	let containerTop = document.createElement('div');
		containerTop.classList.add('main-block');
		containerTop.classList.add('max-1200');
		containerTop.classList.add('no-P-bot');
	let container = document.createElement('div');
		container.classList.add('main-block');
		container.classList.add('max-1200');
		container.classList.add('no-p-top');
	let printTop = '';
	let printMid = '';
	let objSet = {} // Объект контроля для отправки на сервер
		objSet.name = 'Новый проект';
		objSet.nameObj = '';
		objSet.main = '';
		objSet.type = '1';
		objSet.img = '';
	
	printTop = '<div class="one-block-col">\
		<div class="name-page">\
			<span class="icon icon-back hover pointer btn_back_page block_mr_10" title="назад"></span>\
			<input id="name-project" class="btn_name_project input-name-page" type="text" value="Новый проект" placeholder="Название проекта">\
			<span id="icont_redact_name" class="icon icon-change select-none" title="Редактировать"></span>\
		</div>\
		<div id="error_name" class="name-page tc_red">Неверное имя</div>\
		<div class="block_w_100 ta_right">\
			<div class="block_w_260p display_inline ta_left">\
				<p class="block_pb_8">Имена объектов '+HWhtml.helper('Включить отображение имен объектов (датчиков, программ и т.д.)')+'</p>\
				<span class="btn_name_object btn-active on-off"></span>\
			</div>\
			<div class="block_w_260p display_inline ta_left">\
				<p class="block_pb_8">Сделать главным '+HWhtml.helper('Включить отображение этого проекта на главной странице')+'</p>\
				<span class="btn_main_object btn-active on-off"></span>\
			</div>\
		</div>\
	</div>';
	
	printMid = '<div class="one-block-col no-p-lr block-type-project">\
		<div class="title"><h4>Тип проекта '+HWhtml.helper(' Элементы проекта могут быть привязаны к координатам рисунка (фото), к географическим координатам карты или абстрактной сетке.')+'</h4></div>\
		<ul class="big-btn-type-proj">\
			<li class="active" data-id="btn-content-img" data-type="1">\
				<span class="icon p20 icon_projects_1 block_mr_10"></span>\
				<span>Изображение</span>\
				<span class="click btn_type_proj"></span>\
			</li>\
			<li class="" data-id="btn-content-map" data-type="2">\
				<span class="icon p20 icon_projects_2 block_mr_10"></span>\
				<span>Карта</span>\
				<span class="click btn_type_proj btn_spec_type_map"></span>\
			</li>\
			<li class="" data-id="btn-content-grid" data-type="3">\
				<span class="icon p20 icon_projects_3 block_mr_10"></span>\
				<span>Доска</span>\
				<span class="click btn_type_proj"></span>\
			</li>\
		</ul>\
		<div id="btns-content-type" class="big-btn-conten-block">\
			<div id="btn-content-img" class="content-btn active">\
				<div class="block_w_100 block_pb_20" id="img-container"></div>\
				<div id="error_img" class="name-page tc_red">Изображение не выбрано или неверный формат. (.jpg, .jpeg, .png)</div>\
				<div class="btn-select-file">\
					<span class="btn">Выберите файл <input class="btn_load_img" name="proj_img" type="file" id="filetoupload" accept=".jpg, .jpeg, .png"></span>\
					<span class="name">Файл не выбран</span>\
				</div>\
				<p class="ts_14p tc_greyA tl_21p block_pt_20">\
					Тип проекта «Изображение» позволяет установить в качестве <br />\
					фона любое ваше изображение, например проект дома <br />\
					или схему участка.<br />\
				</p>\
			</div>\
			<div id="btn-content-map" class="content-btn">\
				<div id="ecto-map"></div>\
				<p class="ts_14p tc_greyA tl_21p block_pt_20">\
					Тип проекта «Карта» позволяет установить в качестве <br />\
					фона выбранную зону на карте, например, ваш <br />\
					коттеджный поселок<br />\
				</p>\
			</div>\
			<div id="btn-content-grid" class="content-btn">\
				<div class="block_w_100 block_pb_20" id="img-container"><img src="/img/mmBack.jpg"></div>\
				<p class="ts_14p tc_greyA tl_21p block_pt_20">\
					Тип проекта «Доска» позволяет установить в качестве <br />\
					фона разлинованный фон, похожий на поля в тетради. <br />\
					На нем можно удобно расположить элементы. <br />\
				</p>\
			</div>\
		</div>\
		<div class="block_w_100 ta_right block_plr_20"><span class="style_btn grey btn_send_new_proj">Добавить</span></div>\
	</div>';

	containerTop.innerHTML = printTop;
	container.innerHTML = printMid;
	
// функции для этой страници
	// Изменение "ИМЕНИ"
		HW.on('focusin','btn_name_project',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"};
	});
	HW.on('focusout','btn_name_project',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "inline-block"};
	});
	HW.on('change','btn_name_project',function(e,el){
		let iconName = HWS.getElem('#icont_redact_name'); if(iconName){iconName.style.display = "none"};		
		HWS.getElem('#error_name').classList.remove('active');
		let name = el.value;
		
		if(HWF.nameControl(name)){objSet.name = name}
		else {objSet.name = false;}

		el.blur();
	});
	// Изменение кнопки "Имена объектов"
	HW.on('click','btn_name_object',function(e,el){
		if(el.classList.contains('active')){objSet.nameObj = 0}
		else {objSet.nameObj = 1}
	})	
	// Изменение кнопки "Имена объектов"
	HW.on('click','btn_main_object',function(e,el){
		if(el.classList.contains('active')){objSet.main = 1}
		else {objSet.main = 0}
	})
	// Изменение кнопки "ВЫБРАТЬ КАРТИНКУ"
	HW.on('change','btn_load_img',function(e,el){
		if(e.target.files.length > 0){
			let block = HWS.upElem(el,'.btn-select-file');
			let blockName = block.querySelector('.name');
			let imgBlock = HWS.getElem('#img-container'); imgBlock.innerHTML = '';
			let file = e.target.files[0];
			let error = HWS.getElem('#error_img'); error.classList.remove('active');
			
			if (file.type == 'image/jpeg' || file.type == 'image/jpg' || file.type == 'image/png') {
				URL.revokeObjectURL(HWS.buffer.urlImgObjectFile);
				HWS.buffer.urlImgObjectFile = URL.createObjectURL(file);
				objSet.img = file;
				blockName.innerHTML = file.name;
				imgBlock.innerHTML = '<img src="'+HWS.buffer.urlImgObjectFile+'">';
			} else {
				error.classList.add('active');
			}
			

		}
	});
	//Нажатие на кнопку ТИП проекта
	HW.on('click','btn_type_proj',function(e,el){
		let li = HWS.upElem(el,'li');
		let ul = HWS.upElem(li,'ul');
		let idOn = li.dataset.id;
		let BlockCont = HWS.getElem('#btns-content-type');
		let blockOn = BlockCont.querySelector('#'+idOn);
		let liActive = ul.querySelector('li.active');
		let BlockActive = BlockCont.querySelector('.content-btn.active');
		
		liActive.classList.remove('active');
		BlockActive.classList.remove('active');
		li.classList.add('active');
		blockOn.classList.add('active');
		objSet.type = li.dataset.type;
		
		if(idOn == 'btn-content-map'){create_map();}
	});
	//Спец нажатие "СОЗДАТЬ КАРТУ"
	function create_map(){
		if(!HWS.buffer.createObjectMap){
			// Создание карты
			let wievMap = {lat:60.539811,lng:102.2617187,zoom:4.5};
			let map = L.map('ecto-map');
				map.options.crs = L.CRS.EPSG3395;
				map.setView([wievMap.lat,wievMap.lng],wievMap.zoom);
				HWS.buffer.createObjectMap = map;
			
			L.tileLayer(
			  'http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
				subdomains:['mt0','mt1','mt2','mt3'],
				reuseTiles:true,
				updateWhenIdle:false,
			  }
			).addTo(map);
		}
	}
	
	// Нажатие на кнопку "СОЗДАТЬ" оноже отправка на сервер
	HW.on('click','btn_send_new_proj',function(event,elem){
		if(objSet.name) {
			// IMG
			if(objSet.type == 1){
				if(objSet.img){
					loader_proj();
					HWF.form({url:'upload/image',file:objSet.img,data:{token:token},fcn:function(info){
						let newData = {img:{src:info.path_with_file,id:info.id_image},clouse_name:objSet.nameObj}
						HWF.post({url:'projects/set_project',data:{
							token:token,
							type:objSet.type,
							name:objSet.name,
							set_main:objSet.main,
							project_data:newData
						},fcn:load_project});
					}});
					
				} else {let error = HWS.getElem('#error_img'); error.classList.add('active');}
			} 
			// MAP
			else if(objSet.type == 2) {
				loader_proj();
				let newData = {}; newData.zoom = {
					lat:HWS.buffer.createObjectMap.getCenter().lat,
					lng:HWS.buffer.createObjectMap.getCenter().lng,
					zoom:HWS.buffer.createObjectMap.getZoom(),
				}
				HWF.post({url:'projects/set_project',data:{
					token:token,
					type:objSet.type,
					name:objSet.name,
					set_main:objSet.main,
					project_data:newData
				},fcn:load_project});
			}
			// ALL
			else {
				loader_proj();
				let newData = {clouse_name:objSet.nameObj}
				HWF.post({url:'projects/set_project',data:{
					token:token,
					type:objSet.type,
					name:objSet.name,
					set_main:objSet.main,
					project_data:newData,
				},fcn:load_project});
			}
		}
		else {HWS.getElem('#error_name').classList.add('active');}
	});
	
	function load_project(info){
		let id = parseInt(info.id_project);
		HW.go(HWS.data.page.proj+'/'+id);
	}
	
	function loader_proj(){
		containerTop.innerHTML = '<div class="one-block-col">Идёт создание проекта, пожалуйста подождите...</div>';
		container.innerHTML = '<div class="one-block-col">'+HWhtml.loader()+'</div>';
	}

	// заполним информацией блоки
	B.content.append(containerTop);
	B.content.append(container);
	B.title.append(name);
	B.menud.append(menu);
});
