"use strict";
function hw_spa(){
	var dirT = "js/template"; // папка где лежат шаблоны
	var bindBlock = 'hwspa-bind'; // селектор для создание блоков
	var websocketUrl = 'ws://localhost:8000/'; //путь для сокета
	var blocks = {}; // блоки (контейнеры) в которых перерисовываем инфу
	var blocksFcn = {}; // Функции которые будут выполнятся каждый раз
	var template = {}; // тут все шаблоны живут
	var dataI = {};
	var websocket;
	var G = this;
	
	G.buffer = {};
	G.page = {} // страници и их функции
	G.page.main = 'main'; // главная страница
	G.page.login = 'login'; // страница авторизации
	G.page.error = '404'; // страница ошибки загрузки
	G.page.access = true; // Доступность страниц
	G.page.version = {}; // хранит версии для страниц шаблонов
	G.hashLoad = true;

	// получение текущего хеша
	function get_hash(){return window.location.hash || false;}
	// старт всей системы
	function start(fcn){
		fcn = fcn || function(){}; fcn(G);
		hash_change(); window.onhashchange = hash_change;
	}
	// при каждом изменение строки браузере
	function hash_change(){
		let hash = get_hash(); 
		if(G.hashLoad){load_page(hash);}
		else {G.hashLoad = true;}
	}
	// загрузка/подгрузка/изменение страници.
	function load_page(hash){
		let hInfo = norm_hash(hash) || {hash:[G.page.main]};
		G.hash = hInfo.hash;
		G.get = hInfo.get;
		let nameP = hInfo.hash[0] || G.page.main;
		dataI.hash = hInfo;

		if (websocket){websocket.close();}
		if (blocksFcn){for (let key in blocksFcn){if (typeof blocksFcn[key] == 'function'){blocksFcn[key](blocks[key],hInfo);}}}
		
		G.hashChange(hInfo); // пользовательская функция при каждой загрузки новой страници
		G.buffer = {}; // Очистим буфер для новой страници
		clear_event_type();// очистим все локальные евенты
		// загрузка шаблона
		if (G.page.access){load_template(nameP)}
		else {load_template(G.page.login)}
		
		function load_template(nameT){
			if(template[nameT]){init_template(nameT)}
			else {
				let script = document.createElement('script');
				let ver = G.page.version[nameT] || 1;
				script.dataset.name = nameT;
				script.dataset.v = ver;
				script.src = dirT+"/"+nameT+".js?v="+ver;
				document.body.appendChild(script);
				script.onload = function(){init_template(nameT);}
				script.onerror = function(){console.log( "Ошибка Загрузки: " + this.src ); init_template(G.page.error);}
			}
		}
		
		function init_template(nameT){
			let dopFcn;
			if (typeof template[nameT] == "function"){just_do_it(template[nameT]);}
			else if (typeof template[nameT] == "object"){
				let hashl = hInfo.hash;
				let obj = template;

				hashl.every(function(helem){
					if (obj[helem]) {obj = obj[helem]; return true;}
					else {return false;}
				});

				if (typeof obj == "function"){just_do_it(obj);}
				if (typeof obj == "object"){if(obj.init){just_do_it(obj.init)}}
			}
			
			function just_do_it(fcn){
				let fc = fcn || function(){}; let dopFcn;
				let specHesh = norm_hash(window.location.hash);
				if(typeof fc == "function"){dopFcn = fc(G,blocks,specHesh);}
				if(typeof dopFcn == "function"){dopFcn(G,blocks,specHesh);}
			}
		}
	}
	//загрузка шаблона
	function load_tmp(url){
		if(template[url]){init_template(url)}
		else {
			G.newTmpName = url;
			let script = document.createElement('script');
			let ver = G.page.version[url] || 1;
			script.dataset.name = url;
			script.dataset.v = ver;
			script.src = dirT+"/"+url+".js?v="+ver;
			document.body.appendChild(script);
			script.onload = function(){init_template(url);}
			script.onerror = function(){console.log( "Ошибка Загрузки: " + this.src ); init_template(G.page.error);}
		}

		function init_template(nameT){
			let dopFcn;
			if (typeof template[nameT] == "function"){just_do_it(template[nameT]);}
			else if (typeof template[nameT] == "object"){
				let hashl = hInfo.hash;
				let obj = template;

				hashl.every(function(helem){
					if (obj[helem]) {obj = obj[helem]; return true;}
					else {return false;}
				});

				if (typeof obj == "function"){just_do_it(obj);}
				if (typeof obj == "object"){if(obj.init){just_do_it(obj.init)}}
			}
			
			function just_do_it(fcn){
				let fc = fcn || function(){}; let dopFcn;
				let specHesh = norm_hash(window.location.hash);
				if(typeof fc == "function"){dopFcn = fc(G,blocks,specHesh);}
				if(typeof dopFcn == "function"){dopFcn(G,blocks,specHesh);}
			}
		}
	}
	// Добовляет шаблон
	function template_add(name,obj){
		obj = obj || function(){}
		let hash = get_hash();
		let hInfo = norm_hash(hash);
		let nam = hInfo.hash[0];
		if(G.newTmpName){nam = G.newTmpName; obj = name;}
		else {
			if (typeof name == 'string'){nam = name;}
			if (typeof name == 'function'){obj = name;}
		}
		G.newTmpName = false;
		template[nam] = obj;
	}
	// Преобразует строку в нужный формат
	function norm_hash(hash){
		let data={}; data.get={}; data.hash={};
		if (hash){
			hash = hash.slice(1);
			let info = hash.split('?');
			if (info[0]){data.hash = info[0].split('/');}
			if (info[1]){info[1].split('&').forEach(function(e){e = e.split('='); data.get[e[0]] = e[1];});}
			return data;
		}
		return data;
	}
	// Добавления блоков управления страницей
	function block_add(data){
		let name = data.name
		let fcn = data.fcn || false;
		
		blocks[data.name] = create_block(data.query);
		if (fcn) {blocksFcn[data.name] = fcn; blocks[data.name].fcn = fcn}
						
		let binds = blocks[data.name].elem.querySelectorAll('['+bindBlock+']');
		if (binds.length > 0){
			let Allobj = {};
			binds = Array.apply(null, binds);
			binds.forEach(function(el){
				let name = el.getAttribute(bindBlock);
				Allobj[name] = create_block(el);
			});
			blocks[data.name].data = Allobj;
		}
				
		function create_block(selec){
			let obj = {};
				if (typeof selec == 'string'){obj.elem = document.querySelector(selec);}
				if (selec instanceof HTMLElement) {obj.elem = selec;}
				obj.clear = function(){obj.elem.innerHTML = "";}
				obj.addClass = function(clas){obj.elem.classList.add(clas);}
				obj.removeClass = function(clas){obj.elem.classList.remove(clas);}
				obj.append = function(e){
					if (typeof e == 'string'){obj.elem.innerHTML = e;}
					if (e instanceof HTMLElement){obj.elem.appendChild(e);}
				}
			return obj;
		}
		
	}
	// функция обыкновенного AJAX  запроса
	function hw_ajax (option){
		//var statusBar
		var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
		var url = option.url || '';
		var type = option.type || 'GET'; // POST , FORM
		var convert = option.convert || 'JSON';
		var fc = option.fc || function(){};
		var fcb = option.fcb || function(){};
		//var fcbb = option.fcbb || function(){};
		var dataInfo = 'None date!!';
		var body = '';
		var request = new XHR();
			request.onreadystatechange = on_ready;
			
		if (type == 'POST'){
			var data = option.data || {};
			var boundary = String(Math.random()).slice(2);
			var boundaryMiddle = '--' + boundary + '\r\n';
			var boundaryLast = '--' + boundary + '--\r\n'

			body = ['\r\n'];
			for (let key in data) {
				body.push('Content-Disposition: form-data; name="' + key + '"\r\n\r\n' + data[key] + '\r\n');
			}
			body = body.join(boundaryMiddle) + boundaryLast;
			request.open(type,url,true);
			request.setRequestHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
		}	
		if (type == 'GET'){
			request.open(type,url,true);
		}
		if (type == 'FORM'){
			body = option.data || new FormData();
			request.open('post',url,true);
		}
		
		request.send(body);
		
		function on_ready(){
			if(request.readyState === 4) {
				if(request.status === 200) {
					dataInfo = request.responseText
					if (convert == 'JSON') {
						if(IsJsonString(dataInfo)){dataInfo = JSON.parse(dataInfo);fc(dataInfo);}
						else {fc(dataInfo);}
					}else {fc(dataInfo);}
				}else {
					dataInfo = request.responseText || '';
					if(IsJsonString(dataInfo)){dataInfo = JSON.parse(dataInfo);}
					fcb(dataInfo,request.status);
				}
			}
		}

		function IsJsonString(str) {
			try {JSON.parse(str);} catch (e) {return false;}
			return true;
		}
	}
	// подключение к сокету
	function socket(data){
		let Url = data.url || websocketUrl;
		let message = data.onmessage || function(){};
		
		if (websocket){websocket.close();}
		websocket = new WebSocket(Url);
		websocket.onopen = function(evt) {};
		websocket.onmessage = function(evt) {message(evt);};
		websocket.onclose = function(evt) {websocket.close();};//соеденение Закрылось
		websocket.onerror = function(evt) {console.log(evt); websocket.close();};//Ошибка
	}
	// установка функции на эвент по классу
	var events = {}
	var сlearFcn = {}; // Функции которые очищены при обновлении страници ( или принудительно )
	function add_event(evenp,obj,fcn,datap){
		let clas = obj || false;
		fcn = fcn || function(){};
		let data = datap || {};
		let typef = data.type || 'l'; // g
		let method = data.method || 'n'; // m
		
		if(typeof clas == 'string'){
			let even = evenp+'HW'+method;
			if (!events[even] && method=='n'){events[even]={}; document.body.addEventListener(evenp,lisner,false);}
			if (!events[even] && method=='m'){events[even]={}; document.body.addEventListener(evenp,lisner_m,false);}
			if(!events[even][typef]){events[even][typef]={}}
			events[even][typef][clas] = {f:fcn};
			
			function lisner(event){
				let evenl = event.type+'HWn';
				let elem = event.target || event.srcElement;
				control_click(evenl,elem,event,'one')
			}
			
			function lisner_m(event){
				let evenl = event.type+'HWm';
				let elem = event.target || event.srcElement;
				control_click(evenl,elem,event)
			}
		}
		
		if(clas instanceof HTMLElement){
			if (!сlearFcn[typef]){сlearFcn[typef] = {};}
			let numb = Object.keys(сlearFcn[typef]).length + 1;
			сlearFcn[typef][numb] = {o:clas,e:evenp,f:fcn};
			clas.addEventListener(evenp,сlearFcn[typef][numb].f,false);
		}

		function control_click(evenl,elem,event,type){
			let ty = type || 'up';
			let allName = elem.getAttribute("class") || false;
			let fcCtr = false;
			if(allName){
				allName = allName.split(' ');
				for (let typekey in events[evenl]){
					/*
					allName.forEach(function(cn){
						if (events[evenl][typekey][cn]){
							events[evenl][typekey][cn].f(event,elem);
							fcCtr=true;
						}
					});
					*/
					allName.every(function(cn){
						if (events[evenl][typekey][cn]){
							events[evenl][typekey][cn].f(event,elem);
							fcCtr=true;
							return false;
						}
						else {return true;}
					});
				}
			}
			if(!fcCtr && ty=="up"){elem = elem.parentElement;if(elem){control_click(evenl,elem,event);}}
		}
	}
	// удаление функции с эвент по типу
	function clear_event_type(etype){
		let type = etype || 'l'; 
		for (let keye in events){delete events[keye][type];}
		for (let keyc in сlearFcn[type]){
			сlearFcn[type][keyc].o.removeEventListener(сlearFcn[type][keyc].e,сlearFcn[type][keyc].f,false);
			delete сlearFcn[type][keyc];
		}
	}
	// Поиск элемента по селектору
	function get_element(sel){
		let block = '';
		if (typeof sel == 'string'){block =  document.querySelector(sel)}
		else if (sel instanceof HTMLElement) {block = sel;}
		return block;
	}
	// поиск родителя у элемента по классу
	function parent_search_class (el,cls) {while ((el = el.parentElement) && !el.classList.contains(cls));return el;}
	// поиск родителя у элемента по class,id,tag
	function parent_search (el,sel) {
		if(sel[0] == '#'){while ((el = el.parentElement) && el.id != sel.slice(1));return el;}
		else if(sel[0] == '.'){while ((el = el.parentElement) && !el.classList.contains(sel.slice(1)));return el;}
		else {while ((el = el.parentElement) && el.localName != sel);return el;}
	}
	// переход на другую страницу
	function go_page(page,load){ 
		if(load == 'no_reboot'){G.hashLoad = false; setTimeout(function(){G.hashLoad = true;},200);}
		else {G.hashLoad = true;}
		window.location.hash = '#'+page;
	}
	

	this.start = start; // запустить всю систему
	this.go = go_page; // перейти на указанную страницу
	this.templateAdd = template_add; // Добавить новый шаблон
	this.template = template; // тут все шаблоны
	this.load = load_tmp; // подгрузит указанный шаблон.
	this.blocks = blocks; // тут все блоки
	this.blockAdd = block_add; // добавить новый блок
	this.ajax = hw_ajax; // функция для запросов
	this.socket = socket; // подключение к сокету
	this.allEvent = events; // все эвенты (не, ну малоли зачем )
	this.on = add_event; // повешать функцию, на класс или HTML элемент
	this.clearEventType = clear_event_type; // удалисть все эвенты по типу
	this.parentSearchClass = parent_search_class; // найти родителя по классу
	this.hashChange = function(){}; // при изменение страници выполнит функцию
	this.hash = norm_hash(get_hash()).hash; // текущий хешь страници
	this.get = norm_hash(get_hash()).get; // текущий GET страници
	this.getHash = function(){return norm_hash(get_hash())}; // текущий hash страници
	this.getHashStr = function(){return window.location.hash}; // текущий hash страници в вид строки
	this.getElem = get_element; // Получит элемент со страници
	this.upElem = parent_search; // найти родителя по class,id,tag
	this.data = dataI; // любая информация что потребуется для работы
	this.buffer = G.buffer; // Буфер. очищается прикаждой смене страници.
}
var HWS = new hw_spa();