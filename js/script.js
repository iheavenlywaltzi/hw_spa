"use strict";
// Загрузки Языков , подсказок, ошибок  и т.д
HWF.loadlang('ru','events.json','events'); // Эвенты
HWF.loadlang('ru','units.json','unit'); // Юниты
HWF.loadlang('ru','errors.json','errors'); // Ошибки
HWF.loadlang('ru','interface.json','interface'); // Язык интерфейса
document.addEventListener("DOMContentLoaded", function(){
	HWSW.reg('/sw.js'); HWSW.btn(); //HWSW.del();
	document.oncontextmenu = function(){return false;}; // отключим правый клик
	HWS.hashChange = HWF.hashChange; // функция при каждой смене страници
	// Инициализация ( создание/добавление ) блоков/контейнеров. которые будут менятся в шаблонах
	HWS.blockAdd({name:"title",query:".main-title"});
	//HWS.blockAdd({name:"dopMenu",query:".dop-menu"});
	HWS.blockAdd({name:"content",query:".main-page"});
	HWS.blockAdd({name:"menur",query:".right-menu"});
	HWS.blockAdd({name:"menul",query:".left-menu"});
	HWS.blockAdd({name:"menud",query:".dasboart-menu"});
	HWS.blockAdd({name:"modal",query:".modal-window",fcn:HWF.modal.off});
	// версии шаблонов ( для разработки  и дебага )
	HWS.templateVersion = HWD.v;
	// Страници
	HWS.data.page = HWD.page;
	HWS.mainPage = HWD.page.main; // главная страница
	HWS.loginPage = HWD.page.login; // страница авторизации
	HWS.pageAccess = localStorage.getItem('PageAccess') || false; // Доступность страниц
	//api
	HWS.data.apiUrl = HWD.api.url;
	HWF.createrList(HWD.api.queries); // Списки
	// запуск системы
	HWS.start(HWF.start);
	
	//подключение функций для работы кнопок и т.д в системе
	//HWspa.addEvent('click','btn-active',global.active,{type:'g',method:'m'});
		// active
	HWS.on('click','btn-active',HWF.active,{type:'g'}); // Кнопка включения выключения класса "active"
	HWS.on('click','btn-active-one',HWF.activeOne,{type:'g'}); // Открывает 1 закрывая другой.
	// для select, добовляет/удоляет класс active элементам в параметре data-on/of. для моножеста указать через |  data-on="#id|.class li|"
	HWS.on('change','active_control',HWF.activeControl,{type:'g'}); // добовляет/удоляет класс active
	HWS.on('change','open_control',HWF.openControl,{type:'g'}); // как и собрат выше, но добовляет/удоляет класс open
		// Button standart
	document.addEventListener('keydown',HWF.btnClickRandom); // Нажатие на любую кнопку клавиатуры
	document.addEventListener('fullscreenchange',HWF.clickEscOfFullScrin);
	document.addEventListener('webkitfullscreenchange',HWF.clickEscOfFullScrin);
	document.addEventListener('mozfullscreenchange',HWF.clickEscOfFullScrin);
	document.addEventListener('MSFullscreenChange',HWF.clickEscOfFullScrin);

	HWS.on('click','btn_open_full_mainw',HWF.btnOpenFullMainw,{type:'g'}); // Раскрыть навесь экран главный блок
	HWS.on('click','btn_clouse_full_mainw',HWF.btnClouseFullMainw,{type:'g'}); // Закрыть весь экран главный блок
	HWS.on('click','btn_open_control',HWF.btnOpenControl,{type:'g'}); // добовляет/удоляет класс active
	HWS.on('click','btn_back_page',HWF.pageBack,{type:'g'}); // назад на 1 страницу
	HWS.on('click','btn_int_click',HWF.clickBtnInt,{type:'g'}); //функция для нажатий на элемент btn-int , btn_int_click ( с кнопочками + и -  );
	HWS.on('change','inp_num_val',HWF.changeBtnInt,{type:'g'}); //функция если изменилось значение в поле
	HWS.on('click','btn_clouse_modal',HWF.modal.of,{type:'g'}); //Закрытие модального окна
		// pagination
	HWS.on('click','btn-paginator-left-end',HWF.pagination.leftEnd,{type:'p'});
	HWS.on('click','btn-paginator-left',HWF.pagination.left,{type:'p'});
	HWS.on('click','btn-paginator-right',HWF.pagination.right,{type:'p'});
	HWS.on('click','btn-paginator-right-end',HWF.pagination.rightEnd,{type:'p'});
	HWS.on('click','btn-paginator-page',HWF.pagination.page,{type:'p'});
	HWS.on('change','paginator-wiev-page',HWF.pagination.amount,{type:'p'});
		// helper
	HWS.on('click','helper',HWF.helper,{type:'h'});
	HWS.on('mouseover','helper',HWF.helper,{type:'h'});
	HWS.on('mouseout','helper',HWF.helperEnd,{type:'h'});
	HWS.on('click','btn_helper',HWF.helper,{type:'h'});
	HWS.on('mouseover','btn_helper',HWF.helper,{type:'h'});
	HWS.on('mouseout','btn_helper',HWF.helperEnd,{type:'h'});
		// Filter
	HWS.on('change','filter_page',HWF.filter,{type:'g'});	
		// Push
	HWS.on('click','clouse-one-push-message',HWF.clousePush,{type:'g'}); // Свернуть Окно пушей
	//HWS.on('click','btn_push_windiw_clear',HWF.clearPush,{type:'g'}); // Полная очистка пуш окна
	//HWS.on('click','clouse-one-push-message',HWF.clouseOnePush,{type:'g'}); // Закрыть одно сообщение
		// dasboart-menu
	HWS.on('click','btn_dash_click',HWF.clickDasboartMenu,{type:'g'});
		// input
	HWS.on('keyup','inp_only_numb',HWF.inpOnlyNumb,{type:'g'});
	HWS.on('click','btn_view_pass',HWF.eyeRead,{type:'g'}); // глазик в спец полях паролей
		// Time controller
	HWS.on('click','statistic_time_wiev',HWF.clickTimeChange,{type:'g'});
	HWS.on('click','btn_load_info_format',HWF.clickLoadTypeInfo,{type:'g'});
	
	// Нажатие на сообщение 
	HWS.on('pointerdown','info-mesage',function(event,elem){clearTimeout(HWS.data.infoMesageTimer);elem.style.display = 'none';},{type:'g',method:'m'});
	// Глобальное нажатие
	document.body.addEventListener('click',HWF.global);
	// Глабольное обновлние всей страници
	HWF.globalReolad();
	
	//выйти из пользователя
	HWS.on('click','btn_exit_admin',function(){
		let token = localStorage.getItem('PageAccess') || false;
		HWS.pageAccess = false;
		localStorage.setItem('PageAccess','');
		localStorage.setItem('PITn0Hxg','');
		HWS.go("login");
		if (token){HWF.post({url:'user/logout',data:{token:token}});}
	},{type:'g'});
	HWS.on('click','btn_exit_user',function(){
		HWS.pageAccess = false;
		localStorage.setItem('PageAccess','');
		HWS.blocks.menul.addClass('admin');
		HWS.blocks.menur.addClass('admin');
		HWS.go("god");
	},{type:'g'});
});